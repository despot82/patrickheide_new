<?php 
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mod_eurocalc.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}
		
	require_once($aENV['path']['global_service']['unix'].'class.CurrencyConverter.php');
	$oCurrencyConverter = new CurrencyConverter($aENV);
	
	$aRate = $oCurrencyConverter->getRate();
	// Fuer die Ausgabe...
	$aCur['EUR'] = 'EUR';
	$aCur['GBP'] = 'GBP';
	$aCur['USD'] = 'USD';
	$aCur['CHF'] = 'CHF';
?>
<script language="JavaScript" type="text/javascript">
// waehrungsrechner javascript //
	var aRate = new Array();
	aRate['EUREUR'] = 1;
	aRate['GBPGBP'] = 1;
	aRate['USDUSD'] = 1;
	aRate['CHFCHF'] = 1;
	<?	echo "\n";
		foreach($aRate as $cur => $val) {
			echo "\taRate['".$cur."'] = ".$val['rate'].";\n";
		}
	?>

function calc_curr(fieldName) {
	frm = document.forms['waehrungsrechner'];
	field = frm["elements"][fieldName];
	
	if (fieldName == "currtxt_1") {
		selCurrencyFrom = frm.currencies_1.options[frm.currencies_1.selectedIndex].value;
		selCurrencyTo = frm.currencies_2.options[frm.currencies_2.selectedIndex].value;
	}
	else if (fieldName == "currtxt_2") {
		selCurrencyFrom = frm.currencies_2.options[frm.currencies_2.selectedIndex].value;
		selCurrencyTo = frm.currencies_1.options[frm.currencies_1.selectedIndex].value;
	}
	else { return; }
	
	nRate = aRate[selCurrencyFrom + selCurrencyTo];
	if (fieldName == "currtxt_1") {
		frm.elements["currtxt_2"].value = (Math.round((conv(field.value) * nRate) * 100) / 100);
		document.getElementById("waehrungskurs").innerHTML = "[ 1 : " + nRate + " ]";
	}
	else if (fieldName == "currtxt_2") {
		frm.elements["currtxt_1"].value = (Math.round((conv(field.value) * nRate) * 100) / 100);
		document.getElementById("waehrungskurs").innerHTML = "[ " + nRate + " : 1 ]";
	}
}

function dummy() {	// Anker auf Schalter laesst bei gescrollter Seite zum Seitenanfang springen; blur wiederum zwingt manche browser in den Hintergrund => Dummy-Funktion
	return;
}
</script>

<form name="waehrungsrechner">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabelle">
<tr><td class="th" colspan="3"><b><?php echo $aMSG['portal']['currencycalculator'][$syslang]; ?></b> <span id="waehrungskurs"></span></td></tr>
<tr>
	<td width="20%" class="sub3">
	<select name="currencies_1" size="1" style="width:70px" onChange="calc_curr('currtxt_1')">
	<?
	foreach($aCur as $currency => $countryid) {
		$sel = ($countryid == "EUR") ? " selected" : "";
	?>
	<option value="<?=$countryid?>"<?=$sel?>><?=$currency?></option>
	<? } ?>
	</select>
	</td>
	<td class="sub3"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td>
	<td width="80%" class="sub3">
	<select name="currencies_2" size="1" style="width:70px" onChange="calc_curr('currtxt_2')">
	<?
	foreach($aCur as $currency => $countryid) {
		$sel = ($countryid == "GBP") ? " selected" : "";
	?>
	<option value="<?=$countryid?>"<?=$sel?>><?=$currency?></option>
	<? } ?></td>
	</select></td>
</tr>
<tr>
	<td class="sub2"><input type="text" name="currtxt_1" size="11" value="" onBlur="calc_curr('currtxt_1')"></td>
	<td class="sub2"><a href="javascript:dummy()" onMouseOver="window.status=''; return true;" title="<?php echo $aMSG['form']['calculate'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_equal.gif" alt="<?php echo $aMSG['form']['calculate'][$syslang]; ?>" class="btn"></a></td>
	<td class="sub2"><input type="text" name="currtxt_2" size="11" value="" onBlur="calc_curr('currtxt_2')"></td>
</tr>
<tr>
	<td colspan="3" class="sub3"><p><a href="http://finance.yahoo.com/currency/convert?amt=75&from=EUR&to=GBP&submit=Convert" target="_blank">Source: finance.yahoo.com</a></p></td>
</tr>
</table>
</form>

<script language="javascript" type="text/javascript">
document.getElementById("waehrungskurs").innerHTML = "[ 1 : " + aRate['EURGBP'] + " ]";
</script>
		