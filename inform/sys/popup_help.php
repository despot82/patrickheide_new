<?php
/**
* popup_help.php
*
* Popup, um bei Fragen oder Problemen eine E-Mail an design aspekt schicken zu koennen. 
* -> 2sprachig und kopierbar.
*
* NEU: Diese Seite wird aus der popup_faq.php aufgerufen
* (ALT: Diese Seite wird aus der JS-function helpWin([referrer]) aufgerufen)
*
* @param	string	$status		[um zu wissen: Formular oder Danke-Seite anzeigen] (optional -> default = Formular)
* @param	string	$referrer	[zum richtigen zurueckspringen & wird mit uebermittelt!]
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.0 / 2004-05-04 (new_intranet)
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.popup_help.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// init
	require_once("../sys/php/_include_all.php");
	// NUR login wenn eingeloggt!
	if (!isset($oSess) || !is_object($oSess)) {
		$oSess =& new session($aENV['client_shortname']."_inform");  // params: $sName
		$Userdata	= $oSess->get_var('Userdata'); // params: $sName[,$default=null]
		if (is_array($Userdata)) {
			require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
			$bLoggedIn = true;
		}
		else {
			// syslang (if empty, use default from $aENV)
			$syslang	= $oSess->get_var('syslang', $aENV['system_language']['default']); // params: $sName[,$default=null]
			$bLoggedIn = false;
		}
	}
	else {
		$bLoggedIn = false;
	}

// 2a. GET-params abholen
	$action		= (isset($_GET['action'])) ? $_GET['action'] : '';
	$referrer	= (isset($_GET['referrer'])) ? $_GET['referrer'] : '';
// 2b. POST-params abholen
	$aData		= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn		= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars

// Formularversand
if (isset($btn['SAVE'])) {
	// mail vars
	$mailTo			= "inform@design-aspekt.com";
	$mailSubject 	= "INFORM - ".$aENV['web_url'];
	$mailBody 		= "-------------------------------------------- \r\n";
	$mailBody 		.= "Absender Name: ".trim(strip_tags($aData['name']))."\r\n";
	$mailBody 		.= "Absender Email: ".trim(strip_tags($aData['email']))."\r\n";
	$mailBody 		.= "Referrer: ".trim(strip_tags($aData['referrer']))."\r\n";
	$mailBody 		.= "----------------- \r\n";
	$mailBody 		.= "Nachricht: \r\n";
	$mailBody 		.= trim(strip_tags($aData['nachricht']))."\r\n";
	$mailBody 		.= "-------------------------------------------- \r\n";
	$mailBody		= implode("\r\n", preg_split("/\r?\n/", $mailBody));
	$mailFrom 		= $aData['email'];
	if (@mail($mailTo, $mailSubject, $mailBody, "From: ".$mailFrom."\r\n")) {
		header("Location: ".$aENV['PHP_SELF']."?action=ok");
	} else {
		header("Location: ".$aENV['PHP_SELF']."?action=ko");
	}
}

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
?>

<script language="javascript" type="text/javascript">
<!--
function checkForm() {
	with(document.forms['kontaktformular']) {
		obj=elements['aData[nachricht]'];	
		if (obj.value =="") {
			alert("<?php echo $aMSG['help']['alert'][$syslang]; ?>");
			obj.focus();
			return false;
		}
		return true;
	}
}
//-->
</script>

<div id="contentPopup">
<span class="title"><?php echo $aMSG['topnavi']['help'][$syslang]; ?><br></span>
<br>

<?php
if ($action != 'ok') { 
	// Action: Formular
	$oForm = new form_web($aData, $syslang); // params: $aData[,$sLang='de']
	echo $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions, 'POST', 'kontaktformular', 'onSubmit="return checkForm()"'); // params: [$sAction=''][,$sUrlParams=''][,$sMethod=''][,$sName=''][,$sExtra='']
	echo $oForm->hidden("referrer", $referrer); // params: $sFieldname[,$sDefault=''][,$sExtra='']
	if ($bLoggedIn) {
		echo $oForm->hidden("name", $Userdata['firstname'].' '.$Userdata['surname']);
		echo $oForm->hidden("email", $Userdata['email']);
	}
?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php if (!$bLoggedIn) { ?>
<tr>
	<th><p><b><?php echo $aMSG['help']['sender_hl'][$syslang]; ?></b></p></th>
</tr>
<tr>
	<td><p><label for="name"><?php echo $aMSG['help']['name_form'][$syslang]; ?></label><br>
		<?php echo $oForm->textfield("name");
		#echo $oForm->textfield("name", 150, 57, $aMSG['help']['name_form'][$syslang], 'onFocus="this.value=\'\'"'); ?><br>
		<label for="email"><?php echo $aMSG['help']['email_form'][$syslang]; ?></label><br>
		<?php echo $oForm->textfield("email"); ?><br></p>
	</td>
</tr>
<?php } ?>
<tr>
	<th><p><b><?php echo $aMSG['help']['text_form'][$syslang]; ?></b></p></th>
</tr>
<tr>
	<td>
		<?php echo $oForm->textarea("nachricht", 10, 60); ?><br>
		<?php echo $oForm->button('SAVE', $aMSG['btn']['send'][$syslang]); ?>
	</td>
</tr>
</table>
<br>
<?php 
	echo $oForm->end_tag();

} else { // Action: Danke-Seite
?>

<form>
<table width="100%" height="300" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<tr valign="top"><td><p><?php echo $aMSG['help']['text_ok'][$syslang]; ?></p></td></tr>
</table>
<br>
<?php 
} // END Action

?>

</div>

</body>
</html>