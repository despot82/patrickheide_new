<?php
/**
* sys_usergroup.php
*
* Overview-page: usergroups
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2005-11-14 (NEUES RECHTEMANAGEMENT - NICHT MEHR ABWAERTSKOMPATIBEL!)
* #history	1.0 / 2005-01-12
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_usergroup.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// diese seite ist nur was fuer admins ;-)
	if ($oPerm->hasPriv('admin') == false) { echo js_history_back(); }

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= $aENV['table']['sys_usergroup'];
	$sEditPage		= $aENV['SELF_PATH']."sys_usergroup_detail.php";	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;							// fuer NEW-button

// OBJECTS
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	// Alle Userdaten in  einem assioziativen Array vorhalten, damit keine subsubquery noetig wird
	$aUser = $oUser->getAllUserData(false);

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>

<form>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><span class="title"><?php echo $aMSG['topnavi']['users'][$syslang]; ?></span></td>
		<td align="right"><?php if ($oPerm->hasPriv('admin')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"] ?></td>
	</tr>
</table>

<?php echo HR; ?>

<?php include_once("inc.sys_detnavi.php"); 
	echo getSysUserSubnavi(); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<th width="30%"><?php echo $aMSG['form']['usergroup'][$syslang]; ?></th>
		<th width="60%"><?php echo $aMSG['form']['username'][$syslang]; ?></th>
		<th width="10%">&nbsp;</th>
	</tr>
	
<?php // DB
	$aUsergroup = $oUser->getAllUsergroups(); // params: [$bWithUsers=false] 
	$entries = count($aUsergroup);	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
	foreach ($aUsergroup as $ug_id => $aData) {
		// GET-options
		$sGEToptions = "?id=".$aData['id'];
		// user dieser usergroup ermitteln
		$aUgUserName = array(); // container
		$aUID = $oUser->getAllUserIdsByUsergroup($aData['id']); // params: $ugid 
		foreach($aUID as $uid){ 
			// vars
			if (!is_array($aUser[$uid])) continue;
			// DA-Service-Login nur für DA-Service-Login anzeigen
			if ($aUser[$uid]['flag_da_service'] == '1' && !$oPerm->isDaService()) continue;
			// NAME
			$username = $aUser[$uid]['firstname'].' '.$aUser[$uid]['surname'];
			if ($aUser[$uid]['flag_deleted'] == '1') {
				// geloeschte user markieren und nicht verlinken
				$username = '<span style="text-decoration:line-through">'.$username.'</span>';
			} elseif ($oPerm->hasPriv('admin')) {
				// sonst - NUR wenn admin! - verlinken
				$username = '<a href="'.$aENV['SELF_PATH'].'sys_user_detail.php?id='.$uid.'">'.$username.'</a>';
			}
			// sammeln
			$aUgUserName[] = $username;
		}
		
?>
	<tr><td colspan="3" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td height="20"><p><?php // title(-link)
			echo view_link($aData['title'], $sEditPage.$sGEToptions); // params: $sText[,$sHref=''][,$nStatus=0][,$nLength=80][,$bFallback=true]
		?><br></p></td>
		<td><p><?php // usernames ausgeben
			echo implode(', ', $aUgUserName); 
		?>&nbsp;</p></td>
		<td class="sub2" align="right" nowrap><p><?php // edit-button (NUR admin!)
			if ($oPerm->hasPriv('admin')) {
				?><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['std']['edit'][$syslang]; ?>" class="btn"></a><?php
			} 
		?></p></td>
	</tr>
	<tr>
		<td colspan="3" class="sub2"><small><?php // UG-description
			echo nl2br_cms($aData['description']);
		?></small></td>
	</tr>
	
<?php
	}  // END while and 'no-data'-string
	if ($entries == 0) { echo '	<tr><td colspan="3" align="center"><span class="text">'.$aMSG['std']['no_entries'][$syslang].'</span></td></tr>'; }
	unset($aData); // wegen footer loeschen
?>
</table>
<form>
<br>


<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
