<?

 /*
  * Created on 07.11.2005 
  * 
  */

/**
  *
  * @author Frederic Hoppenstock <fh@design-aspekt.com>
  *
  */

class AdrCompanyBean {

	var $id;
	var $referenceid;
	var $cat;
	var $street;
	var $building;
	var $region;
	var $town;
	var $pobox;
	var $poboxtown;
	var $country;
	var $phonemob;
	var $phonetel;
	var $phonefax;
	var $email;
	var $web;
	var $companyid;
	var $companydep;
	var $companypos;
	var $recipient;
	var $created;
	var $createdby;
	var $lastmodified;
	var $lastmodby;
	var $companyname;
	
	function setId($id) {
		
		 $this->id = $id;

	}

	function getId() {

		 return $this->id;

	} 


	function setReferenceid($referenceid) {

		 $this->referenceid = $referenceid;

	}

	function getReferenceid() {

		 return $this->referenceid;

	} 


	function setCat($cat) {

		 $this->cat = $cat;

	}

	function getCat() {

		 return $this->cat;

	} 


	function setStreet($street) {

		 $this->street = $street;

	}

	function getStreet() {

		 return $this->street;

	} 


	function setBuilding($building) {

		 $this->building = $building;

	}

	function getBuilding() {

		 return $this->building;

	} 


	function setRegion($region) {

		 $this->region = $region;

	}

	function getRegion() {

		 return $this->region;

	} 


	function setTown($town) {

		 $this->town = $town;

	}

	function getTown() {

		 return $this->town;

	} 


	function setPobox($pobox) {

		 $this->pobox = $pobox;

	}

	function getPobox() {

		 return $this->pobox;

	} 


	function setPoboxtown($poboxtown) {

		 $this->poboxtown = $poboxtown;

	}

	function getPoboxtown() {

		 return $this->poboxtown;

	} 


	function setCountry($country) {

		 $this->country = $country;

	}

	function getCountry() {

		 return $this->country;

	} 


	function setPhonemob($phonemob) {

		 $this->phonemob = $phonemob;

	}

	function getPhonemob() {

		 return $this->phonemob;

	} 


	function setPhonetel($phonetel) {

		 $this->phonetel = $phonetel;

	}

	function getPhonetel() {

		 return $this->phonetel;

	} 


	function setPhonefax($phonefax) {

		 $this->phonefax = $phonefax;

	}

	function getPhonefax() {

		 return $this->phonefax;

	} 


	function setEmail($email) {

		 $this->email = $email;

	}

	function getEmail() {

		 return $this->email;

	} 


	function setWeb($web) {

		 $this->web = $web;

	}

	function getWeb() {

		 return $this->web;

	} 


	function setCompanyid($companyid) {

		 $this->companyid = $companyid;

	}

	function getCompanyid() {

		 return $this->companyid;

	} 


	function setCompanydep($companydep) {

		 $this->companydep = $companydep;

	}

	function getCompanydep() {

		 return $this->companydep;

	} 


	function setCompanypos($companypos) {

		 $this->companypos = $companypos;

	}

	function getCompanypos() {

		 return $this->companypos;

	} 


	function setRecipient($recipient) {

		 $this->recipient = $recipient;

	}

	function getRecipient() {

		 return $this->recipient;

	} 


	function setCreated($created) {

		 $this->created = $created;

	}

	function getCreated() {

		 return $this->created;

	} 


	function setCreatedby($createdby) {

		 $this->createdby = $createdby;

	}

	function getCreatedby() {

		 return $this->createdby;

	} 


	function setLastmodified($lastmodified) {

		 $this->lastmodified = $lastmodified;

	}

	function getLastmodified() {

		 return $this->lastmodified;

	} 


	function setLastmodby($lastmodby) {

		 $this->lastmodby = $lastmodby;

	}

	function getLastmodby() {

		 return $this->lastmodby;

	} 

	function setCompanyname($companyname) {

		 $this->companyname = $companyname;

	}

	function getCompanyname() {

		 return $this->companyname;

	} 
}

?>