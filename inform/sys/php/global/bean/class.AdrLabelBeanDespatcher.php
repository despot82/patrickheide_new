<?php
/*
 * Created on 29.09.2005
 *
 */
/**
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * 
 */
 
  class AdrLabelBeanDespatcher {
  	
	var $abstrasse;
	var $abhnr;
	var $abplz;
	var $abort;
	var $abname;
	var $abcountry;
	var $abcountrycode;

	function setAbstrasse($abstrasse) {

		 $this->abstrasse = $abstrasse;

	 }

	function getAbstrasse() {

		 return $this->abstrasse;

	 } 


	function setAbhnr($abhnr) {

		 $this->abhnr = $abhnr;

	 }

	function getAbhnr() {

		 return $this->abhnr;

	 } 


	function setAbplz($abplz) {

		 $this->abplz = $abplz;

	 }

	function getAbplz() {

		 return $this->abplz;

	 } 


	function setAbort($abort) {

		 $this->abort = $abort;

	 }

	function getAbort() {

		 return $this->abort;

	 } 


	function setAbname($abname) {

		 $this->abname = $abname;

	 }

	function getAbname() {

		 return $this->abname;

	 } 


	function setAbcountry($abcountry) {

		 $this->abcountry = $abcountry;

	 }

	function getAbcountry() {

		 return $this->abcountry;

	 } 
  	
	function setAbcountrycode($abcountrycode) {

		 $this->abcountrycode = $abcountrycode;

	 }

	function getAbcountrycode() {

		 return $this->abcountrycode;

	 } 
  }
?>
