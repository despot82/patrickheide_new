<?php

 /*
  * Created on 03.11.2005 
  * 
  */

/**
  *
  * @author Frederic Hoppenstock <fh@design-aspekt.com>
  *
  */

class TeamAccessControllBean {

	var $id;
	var $flagtype;
	var $ugid;
	var $fkid;
	var $flagshow;
	var $flagtree;

	function setId($id) {

		 $this->id = $id;

	}

	function getId() {

		 return $this->id;

	} 


	function setFlagtype($flagtype) {

		 $this->flagtype = $flagtype;

	}

	function getFlagtype() {

		 return $this->flagtype;

	} 


	function setUgid($ugid) {

		 $this->ugid = $ugid;

	}

	function getUgid() {

		 return $this->ugid;

	} 


	function setFkid($fkid) {

		 $this->fkid = $fkid;

	}

	function getFkid() {

		 return $this->fkid;

	} 


	function setFlagshow($flagshow) {

		 $this->flagshow = $flagshow;

	}

	function getFlagshow() {

		 return $this->flagshow;

	} 


	function setFlagtree($flagtree) {

		 $this->flagtree = $flagtree;

	}

	function getFlagtree() {

		 return $this->flagtree;

	} 

}

?>