<?

 /*
  * 
  * Created on 29.09.2005 
  * 
  */
/**
  *
  * @author Frederic Hoppenstock <fh@design-aspekt.com>
  *
  */
class AdrLabelBeanEmpfaenger {

	var $emstrasse;
	var $emhnr;
	var $emplz;
	var $emort;
	var $emname;
	var $ememail;
	var $emcountry;
	var $empobox;
	var $empoboxtown;
	var $embuilding;
	var $emregion;
	var $emrecipient;
	var $emadresstype;
	var $emcompany;
	var $emcompanyid;

	function setEmstrasse($emstrasse) {

		 $this->emstrasse = $emstrasse;

	 }

	function getEmstrasse() {

		 return $this->emstrasse;

	 } 


	function setEmhnr($emhnr) {

		 $this->emhnr = $emhnr;

	 }

	function getEmhnr() {

		 return $this->emhnr;

	 } 


	function setEmplz($emplz) {

		 $this->emplz = $emplz;

	 }

	function getEmplz() {

		 return $this->emplz;

	 } 


	function setEmort($emort) {

		 $this->emort = $emort;

	 }

	function getEmort() {

		 return $this->emort;

	 } 


	function setEmname($emname) {

		 $this->emname = $emname;

	 }

	function getEmname() {

		 return $this->emname;

	 } 


	function setEmemail($ememail) {

		 $this->ememail = $ememail;

	 }

	function getEmemail() {

		 return $this->ememail;

	 } 

	function setEmcountry($emcountry) {

		 $this->emcountry = $emcountry;

	 }

	function getEmcountry() {

		 return $this->emcountry;

	 } 


	function setEmpobox($empobox) {

		 $this->empobox = $empobox;

	 }

	function getEmpobox() {

		 return $this->empobox;

	 } 


	function setEmpoboxtown($empoboxtown) {

		 $this->empoboxtown = $empoboxtown;

	 }

	function getEmpoboxtown() {

		 return $this->empoboxtown;

	 } 


	function setEmbuilding($embuilding) {

		 $this->embuilding = $embuilding;

	 }

	function getEmbuilding() {

		 return $this->embuilding;

	 } 


	function setEmregion($emregion) {

		 $this->emregion = $emregion;

	 }

	function getEmregion() {

		 return $this->emregion;

	 } 
	 
	function setEmrecipient($emrecipient) {

		 $this->emrecipient = $emrecipient;

	 }

	function getEmrecipient() {

		 return $this->emrecipient;

	 } 
	function setEmadresstype($emadresstype) {

		 $this->emadresstype = $emadresstype;

	 }

	function getEmadresstype() {

		 return $this->emadresstype;

	 } 
	function setEmcompany($emcompany) {

		 $this->emcompany = $emcompany;

	 }

	function getEmcompany() {

		 return $this->emcompany;

	 } 
	function setEmcompanyid($emcompanyid) {

		 $this->emcompanyid = $emcompanyid;

	 }

	function getEmcompanyid() {

		 return $this->emcompanyid;

	 } 

}

?>