<?php

 /*
  * Created on 03.11.2005 
  * 
  */

/**
  *
  * @author Frederic Hoppenstock <fh@design-aspekt.com>
  *
  */

class FolderBean {

	var $prio;
	var $parentid;
	var $flagnavi;
	var $title;
	var $id;

	function setPrio($prio) {

		 $this->prio = $prio;

	}

	function getPrio() {

		 return $this->prio;

	} 


	function setParentid($parentid) {

		 $this->parentid = $parentid;

	}

	function getParentid() {

		 return $this->parentid;

	} 


	function setFlagnavi($flagnavi) {

		 $this->flagnavi = $flagnavi;

	}

	function getFlagnavi() {

		 return $this->flagnavi;

	} 


	function setTitle($title) {

		 $this->title = $title;

	}

	function getTitle() {

		 return $this->title;

	} 


	function setId($id) {

		 $this->id = $id;

	}

	function getId() {

		 return $this->id;

	} 

}

?>