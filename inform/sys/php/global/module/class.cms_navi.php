<?php // Class cms_navi
/**
* Diese Klasse vereinfacht das Ausgeben der CMS-Navigation.
* Es stehen Methoden zur Verfuegung, die z.B. die Topnavi oder die aktuelle Subnavi als "behandeltes" Array
* zurueckgeben. D.h. nur freigegebene und mit aufbereiteten Parqametern wie href ({FIRST_SUB} ist aufgeloest)
* plus dem zusaetzlichen parameter "highlight", des bei dem gerade aktiven Menuepunkt auf 1, sonst auf 0 steht.
* Ausserdem gibt es eine Methode, die den fertigen HTML-Path der aktuellen Ausgabeseite liefert.
* Um schnellere Ergebnisse zu produzieren, wird das aus der Datenbank gelesene komplette Navi-Array gecacht.
* Dieses Cache-File kann mit der Methode "clearCache()" geloescht werden. Bei der naechsten Anfrage wird das
* Cache-File automatisch wieder erzeugt.
* NOTE: benoetigt die Klasse "class.db_mysql.php", das Environment-Array $aENV, sowie das Array $aPageInfo aus der aktuellen Container-Datei!
* NOTE: Diese Klasse holt sich ALLE verwendeten HTML-Tags aus der Klasse "class.HtmlFolder.php" (wie auch die Klassen "cms_navi" und "pms_navi").
*
* Example: 
* <pre><code>
* require_once("php/global/class.cms_navi.php");
* $oNav =& new cms_navi(); // params: -
* $oNav->loadNavi($navi_id, $weblang, false); // params: $nNaviId[,$sLang='de'][,$bValidOnly=true]
* echo "gefundene Datensaetze in Navi-Table: ".$oNav->getCountItems();
* $aMainNavi = $oNav->getMainNavi(); // params: -
* foreach($aMainNavi as $key => $aNaviItem) { echo $aNaviItem[href]; ... }
* $aSubNavi = $oNav->getSubNavi($navi_id); // params: [$nParentId=null][,$nTopnaviId=null]
* foreach($aSubNavi as $key => $aNaviItem) { echo $aNaviItem[href]; ... }
* $aServiceNavi = $oNav->getServiceNavi(); // params: [$nTopnaviId='1']
* foreach($aServiceNavi as $key => $aNaviItem) { echo $aNaviItem[href]; ... }
* echo $oNav->getPathHTML(); // params: [$sTrenner=' &raquo; ']
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <af@design-aspekt.com>
* @version	1.48 / 2006-04-25 (NEU: Mehrsprachige Containerfiles)
* @uses		class.db_mysql.php
* @uses		class.HtmlFolder.php
* @see		class.PrintFolder.php
* @see		class.pms_navi.php
*/

class cms_navi {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse cms_navi:
		----------------------------------------------------------------------------
		constructor cms_navi()
		private _initTree()
		private _buildFlatTree()
		private _checkTable()
		private _createTable()
		private _moveChildren($completeArray, &$childArray)
		
		function loadNavi($nNaviId, $sLang='de', $bValidOnly=true)
		function clearCache()
		
		function getCountItems()
		function getCountLevel()
		function getMainNavi()
		function getSubNavi($nParentId=null, $nTopnaviId=null)
		function getServiceNavi($nTopnaviId='1')
		private _getAllChildren($parentId='0', $nTopnaviId='0', $arr='')
		private _getFirstSubHref($parentId)
		function getAllChildrenId($parentId='0', $arr='')
		function getAllParentId($id, $firstrun=true)
		private _getCurrentPosition($id)
		function getPrev($id)
		function getNext($id)
		function getParent($id)
		function getItemArray($id)
		
		function getPathArray()
		function getPathHTML($sTrenner=' &raquo; ')
		----------------------------------------------------------------------------
		HISTORY:
		1.46 / 2006-02-23 (NEU: zeitsteuerung; "$bWeb" wieder entfernt]
		1.45 / 2006-02-16 (NEU: "getSubNavi()" ermoeglicht "$bWeb" zu uebergeben]
		1.44 / 2005-06-07 (NEU: "getSubNavi()" ermoeglicht "$nTopnaviId" zu uebergeben]
		1.43 / 2005-05-17 (NEU: "_getFirstSubHref()" + diese eingebaut in "getPathArray()"]
		1.42 / 2005-04-20 ($aENV global statt per konstruktor-value importieren]
		1.41 / 2005-04-19 ($lang in $weblang umbenannt]
		1.4 / 2005-04-14 (BUGFIX: in "getSubNavi()" + "getPathArray()" + "cms_navi()"]
		1.34 / 2005-04-11 (BUGFIX: sprachabhaengige felder von "$aENV['content_language']" abhaengig gemacht]
		1.33 / 2005-03-31 (BUGFIX: rekursives weiterhangeln i.d. Kinder bei "getAllChildrenId()"]
		1.32 / 2005-01-18 [BUGFIX: highlight-handling bei "_getHighlightId()" (+Aufruf bei "loadNavi()")]
		1.31 / 2005-01-05 [BUGFIX: "this->MSG" bei _buildFlatTree()]
		1.3 / 2004-12-01 [KOMPLETT NEU +Service-Navi +Tree +rekursiv]
		1.23 / 2004-10-28 [BUGFIX: Bedingung zum Neuschreiben des Cachefiles entfernt bei "clearCache()"]
		1.22 / 2004-10-14 [BUGFIX: flag_navi-Behandlung bei "getSubNavi()"]
		1.21 / 2004-09-21 [BUGFIX: Highlight bei "getMainNavi()"]
		1.2 / 2004-07-30 [Konstruktor aufgesplittet (+ in diesem Zusammenhang "load_navi()" als 2. Init-Methode erstellt]
		1.1 / 2004-07-28 ["getSubNavi()" ermoeglicht "nParentId" zu uebergeben + "bValidOnly" i.d. Konstruktor verlagert]
		1.0 / 2004-06-18 [entwickelt auf basis von "class.tree.php"]
	*/
	
	// Class Vars
	var $oDb;				// DB-Objekt
	var $aENV;				// importiertes globales CMS-Array
	var $sCachefile;		// Cachefile (inkl. Path)
	var $sTable;			// Navi-DB-Tabelle
	var $aDbTitleFields;	// Title-Felder der Navi-DB-Tabelle
	var $sOrderBy;			// ORDER-BY Feldname (UND Laufrichtung)
	var $nNaviId;			// aktuelle Datensatz-Id (der navi-table)
	var $sLang;				// aktuelle Ausgabesprache
	var $bValidOnly;		// Schalter fuer Web- oder CMS-Ansicht
	var $flatArr;			// flaches Array, das so aus der DB (bzw. dem Cachefile) gelesen wird
	var $treeArr;			// verschachteltes Array, das die Baumstruktur abbildet
	var $countFlatArr;		// Gesamtanzahl Eintraege
	var $countLevel;		// Gesamtanzahl Ebenen (Verschachtelungstiefe)
	var $curPos;			// Aktuelle Position des Zeigers im flatArr
	var $MSG;				// Text fuer das Cache-File
/**
* Alle IDs die gehighlightet(/ausgeklappt) werden muessen
* @access   private
* @var	 	array	
*/
	var $aHighlightId = array();
/**
* @access   private
* @var	 	object	HtmlFolder-Object
*/
	var $oHtmlFolder = NULL;
/**
* Einrueckung in Pixeln
* @access   private
* @var	 	integer	
*/
	var $nIndent = 0;
/**
* @access   private
* @var	 	boolean	Debug-Status (false = off, true = on)
*/
	var $debug = false;

/**
* Konstruktor -> Initialisiert das navi-object
*
* Beispiel: 
* <pre><code> 
* require_once("php/class.cms_navi.php"); 
* $oNav =& new cms_navi(); // params: -
* </code></pre>
*
* @access   public
* @global	global	$aENV			HOME Environment
* @global	object	$oDb			"class.db_mysql.php"
* @return	void
*/
	function cms_navi() {
		// import globals
		global $aENV;
		$this->aENV				=& $aENV;
		if (!is_array($this->aENV)) die("ERROR: NO aENV!");
		
		// init HtmlFolder
		require_once($aENV['path']['global_service']['unix'].'class.HtmlFolder.php');
		$this->oHtmlFolder =& new HtmlFolder();
		$this->nIndent = 17; // Einrueckung in Pixeln
		
		// DB-Objekt instanzieren
		$this->oDb				=& new dbconnect($this->aENV['db']);
		
		// Content-Tabelle definieren
		$this->sTable			= $this->aENV['table']['cms_navi'];
		
		// Title-Felder i.d. Content-Tabelle definieren
		$this->aDbTitleFields	= array();
		foreach ($this->aENV['content_language'] as $k => $v) {
			if ($k == 'default') continue;
			$this->aDbTitleFields[] = 'title_'.$k;
		}
		
		// OrderBy definieren
		$this->sOrderBy			= "prio DESC";
		
		// ggf. Cachefile definieren
		$this->sCachefile		= $this->aENV['path']['data']['unix']."array.navi_data.php";
		
		// set default ( wird von "loadNavi()" ueberschrieben!)
		$this->sLang			= $this->aENV['content_language']['default'];
		
		// Texte
		$this->MSG = array();
		$this->MSG['intro']['de'] = "Diese Seite wird vom design aspekt CMS automatisch erstellt. Manuelle Aenderungen gehen verloren!";
		$this->MSG['intro']['en'] = "This page was created automatically by design aspekt CMS. Do not make any changes!";
		
		// pruefe ob navi-table vorhanden
		$this->_checkTable();
		
		// baue und lade Navi-Array
		$this->_initTree();
	}


# --------------------------------------------------------------------------------

/** Erstell-Funktion: Aufbau des Baumes und aller dazugehoerigen Variablen. 
* (kann zum reseten genommen werden!)
*
* @access   private
*/
	function _initTree() {
		// reset
		$this->flatArr			= array();
		$this->treeArr			= array();
		$this->countFlatArray	= 0;
		$this->countLevel		= 0;
		
		$this->flatArr			= $this->_buildFlatTree();		
		$this->countFlatArr		= count($this->flatArr);
		// kopiere array fuer tree
		$this->treeArr			= $this->flatArr;
		// baue baum-array
		$this->_moveChildren($this->treeArr, $this->treeArr);	#print_r($this->treeArr);
	}

/** Erstell-Funktion: Lese DB in ein flaches Array (schon in der richtigen Reihenfolge)
* 
* @access   private
* @param	string	[optional] wird von der Funktion selbst benoetigt
* @param	string	[optional] wird von der Funktion selbst benoetigt
* @param	int		[optional] wird von der Funktion selbst benoetigt
* @param	int		[optional] wird von der Funktion selbst benoetigt
* @return	array
*/
	function _buildFlatTree($db='', $aNavi='', $parentId=0, $ebene=0) {
		// Ebene 0 Init
		if ($ebene == 0) {
			$aNavi = Array();
			if (!$db) { $db = &$this->oDb; }
		}
		// schaue nach ob evtl. cachefile existiert
		if ($ebene == 0 
		&& !empty($this->sCachefile) 
		&& file_exists($this->sCachefile)) {

			unset($aNavi);
			$aNavi	= Array();
			require($this->sCachefile); // nur bei bedarf inkludieren!
			
		} else {
			
		// ansonsten array via db-abfrage erstellen
			$aFieldlist = array('id','parent_id','reference_id','image','image_hi','template','layout','link','authorisation','flag_navi','flag_topnavi');
			foreach ($this->aENV['content_language'] as $k => $v) { // sprachabhaengige felder
				if ($k == 'default') continue;
				$aFieldlist[] = 'title_'.$k;
				$aFieldlist[] = 'flag_online_'.$k;
				$aFieldlist[] = 'online_from_'.$k;
				$aFieldlist[] = 'online_to_'.$k;
				$aFieldlist[] = 'filename_'.$k;
			}
			$sQuery = "SELECT ".implode(',', $aFieldlist)." 
						FROM ".$this->sTable." 
						WHERE parent_id=".$parentId." 
						".$this->sSqlConstraint." 
						ORDER BY ".$this->sOrderBy;
			
			$lines = $db->fetch_in_Aarray($sQuery);
			foreach ($lines as $aData) {
				$tmp = array(	'id'				=> $aData['id'],
								'parent_id'			=> $aData['parent_id'],
								'reference_id'		=> $aData['reference_id'],
								'image'				=> $aData['image'],
								'image_hi'			=> $aData['image_hi'],
								'template'			=> $aData['template'],
								'layout'			=> $aData['layout'],
								'authorisation'		=> $aData['authorisation'],
								'flag_navi'			=> $aData['flag_navi'],
								'flag_topnavi'		=> $aData['flag_topnavi']
							);
				// sprachabhaengige felder
				foreach ($this->aENV['content_language'] as $k => $v) {
					if ($k == 'default') continue;
					$tmp['title_'.$k]		= $aData['title_'.$k];
					$tmp['flag_online_'.$k]	= $aData['flag_online_'.$k];
					$tmp['online_from_'.$k]	= $aData['online_from_'.$k];
					$tmp['online_to_'.$k]	= $aData['online_to_'.$k];
					$tmp['filename_'.$k]	= $aData['filename_'.$k];
						
					// Feature: zusaetzlicher Parameter 'href' (= 'link' oder 'filename')!
					// -> kann direkt in den a href des navi-punktes geschrieben werden
					if (empty($aData['link'])) { 
						$tmp['href_'.$k] = $aData['filename_'.$k]; 
					}
					else {
						$tmp['href_'.$k] = $aData['link'];
					}
				}
				// zusaetzliches feld 'children'
				$tmp['children'] = array();
				$aNavi[] = $tmp;
				
				$this->_buildFlatTree(&$db, &$aNavi, $aData['id'], ++$ebene); $ebene--;
				
			}
			// ggf. abfrage als array in cachedatei speichern
			if ($ebene == 0 && $this->sCachefile && count($aNavi)) {
				$fp = fopen($this->sCachefile, "w");
				fputs($fp, '<?php /* '.$this->MSG['intro'][$this->aENV['system_language']['default']].' */'."\n");
				fputs($fp, '$aNavi = array();'."\n");
				for ($i = 0; $i < count($aNavi); $i++) {
					fputs($fp, '$aNavi['.$i.'] = array('."\n");
					foreach($aNavi[$i] as $k => $v) {
						if ($k == "children") {
							fputs($fp,'	"'.$k.'" => array()'."\n");
						} else {
							fputs($fp,'	"'.$k.'" => "'.str_replace('"','&quot;',$v).'",'."\n");
						}
					}
					fputs($fp,');'."\n");
				}
				fputs($fp,'?>'."\n");
				fclose($fp);
			}
		}
		// Ebene 0, Wert zurueckgeben
		if ($ebene == 0) {
			return $aNavi; #print_r($aNavi);
		}
	}

/** Erstell-Funktion: checke ob DB-Table vorhanden, wenn nicht, starte Funktion zum Erstellen
* 
* @access   private
*/
	function _checkTable() {
		$tables = $this->oDb->get_tables();
		if (!empty($this->sTable) && !in_array($this->sTable, $tables)) {
			$this->_createTable();
		}
	}

/** Erstell-Funktion: erstelle DB-Table (minimum required fields)
* 
* @access   private
*/
	function _createTable() {
		if (empty($this->sTable)) {
			if ($this->debug) { die("ERROR: empty var this->sTable!"); } else { return false; }
		}
		$sQuery	=	"CREATE TABLE `".$this->sTable."` ( 
						`id` INT(11) NOT NULL auto_increment, 
						`parent_id` INT(11) NOT NULL default '0', 
						`reference_id` INT(11) NOT NULL default '0', ";
		foreach ($this->aENV['content_language'] as $k => $v) { // sprachabhaengige felder
			if ($k == 'default') continue;
			$sQuery	.=	"`title_".$k."` VARCHAR(150) default NULL, ";
			$sQuery	.=	"`flag_online_".$k."` ENUM('0','1') NOT NULL default '0', ";
		}
		$sQuery	.=		"`flag_topnavi` ENUM('0','1') NOT NULL default '0', 
						`link` VARCHAR(150) default NULL, 
						`image` VARCHAR(150) default NULL,
						`image_hi` VARCHAR(150) default NULL, 
						`template` VARCHAR(32) default NULL, 
						`layout` ENUM('A','B','C','D') NOT NULL default 'A', 
						`filename` VARCHAR(150) default NULL, 
						`prio` INT(11) NOT NULL default '0', 
						`flag_navi` ENUM('0','1') NOT NULL default '1', ";
		foreach ($this->aENV['content_language'] as $k => $v) { // sprachabhaengige felder
			if ($k == 'default') continue;
			$sQuery	.=	"`online_from_".$k."` DATE NOT NULL default '0000-00-00', ";
			$sQuery	.=	"`online_to_".$k."` DATE NOT NULL default '0000-00-00', ";
		}
		$sQuery	.=		"`created` VARCHAR(14) default NULL, 
						`created_by` SMALLINT(5) default NULL, 
						`last_modified` TIMESTAMP(14) NOT NULL, 
						`last_mod_by` SMALLINT(5) default NULL, 
						PRIMARY KEY  (`id`) 
					) TYPE=MyISAM;";
		return $this->oDb->query($sQuery);
	}

/** Erstell-Funktion: Ermittelt rekursiv Kinder und verschiebt sie in Unter-Arrays
* (Re-strukturiert das Array neu, indem Kinder an ihre Eltern geknuepft werden)
* 
* @access   private
* @param	array	&$completeArray	Array als Referenz (das komplette flache Array, das von "buildFlatTree()" zurueckgeliefert wird!)
* @param	array	&$childArray	Array als Referenz (der Zweig des Arrays, an den Kinder angehaengt werden - kann auch das ganze sein!)
* @return	void
*/
	function _moveChildren($completeArray, &$childArray) {
		if ($this->countFlatArr == 0) return;
		foreach ($childArray as $key => $val) {
			$temp = array();
			// Kinder ermitteln
			foreach ($completeArray as $k => $v) {
				if ($v['parent_id'] == $val['id']) {
					$temp[$k] = $v; // gefunden! -> in $temp zwischenspeichern
					unset($this->treeArr[$k]); // loesche Original
				}
			}
			$childArray[$key]['children'] = $temp; // Kinder neu zuweisen
			// und rekursiv weiterhangeln...
			$this->_moveChildren($completeArray, $childArray[$key]['children']);
		}
	}

# --------------------------------------------------------------------------------

/**
* Erstell-Funktion: zieht die Navi-Daten aus dem Cache-File oder wenn noch nicht vorhanden, aus der DB und bereitet sie auf. 
* (NOTE: Wenn diese Methode nicht aufgerufen wird, kann man sonst eigentlich nur die Methode "clearCache()" benutzen, 
* da alle anderen Ausgabe-Methoden nur "false" zurueckgeben wuerden!)
*
* Beispiel: 
* <pre><code> 
* $oNav->loadNavi($navi_id, $weblang); // params: $nNaviId[,$sLang='de'][,$bValidOnly=true]
* </code></pre>
*
* @access   public
* @param	string	$nNaviId		aktuelle id des Datensatzes (der navi-table)
* @param	string	[$sLang]		ggf. aktuelle Sprache angeben (default: 'de')
* @param	boolean	[$bValidOnly]	ggf. angeben ob Web- (true) oder CMS-Ansicht (false) (default: true)
* @return	void
*/
	function loadNavi($nNaviId, $sLang='de', $bValidOnly=true) {
		// init vars
		if (empty($nNaviId)) { $nNaviId = 0; }
		$this->nNaviId		= ($nNaviId + 0);
		if (empty($sLang))   { $sLang = 'de'; }
		$this->sLang		= $sLang;
		$this->bValidOnly	= $bValidOnly;
		
		// ggf. baue und lade Navi-Array neu
		if (count($this->flatArr) == 0) $this->_initTree();
		
		// ermittle alle parent_ids des aktuellen navi-punktes (zum spaeteren ausklappen/highlighten)
		if ($this->bValidOnly) {
			$this->aHighlightId		= $this->_getHighlightId($this->nNaviId);
		} else {
			$this->aHighlightId		= $this->getAllParentId($this->nNaviId);
			$this->aHighlightId[]	= $this->nNaviId; // sich selbst dazu
		}	#print_r($this->aHighlightId); // DEBUG
		// fallback
		if (!is_array($this->aHighlightId)) { $this->aHighlightId = array(0); }
		
		// Verschachtelungstiefe tracken
		$this->countLevel = count($this->aHighlightId);
	}

/** Hilfs-Funktion: Gebe die eigene ID, sowie die IDs der Eltern (und Grosseltern -> rekursiv!) einer $Id 
* unter Beruecksichtigung der alternativen Highlight-Einstellungen als flaches (numerisches) Array zurueck. 
* 
* @access   private
* @param	string	[$id]		Navigationspunkt-ID, dessen Ahnen ermittelt werden sollen
* @param	array	[$firstrun]	-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function _getHighlightId($id, $firstrun=true) {
		if ($this->countFlatArr == 0 || empty($id)) return array();
		if ($firstrun == true) {
			$this->temp = array(); // reset buffer
		}
		foreach ($this->flatArr as $key => $val) {
			if ($val['id'] == $id) { // gefunden!
				// highlight-ID-handling
				$HighlightId = ($val['reference_id'] > 0) ? $val['reference_id'] : $val['id'];
				if (!in_array($HighlightId, $this->temp)) { // wenn noch nicht (z.B. als parent_id) im array-buffer...
					$this->temp[] = $HighlightId; // ... sich selbst (oder alternativ die highlight-ID) dem buffer hinzufuegen
				}
				// oben angekommen: schleife beenden...
				if ($val['parent_id'] == '0') { break; }
				// ... ansonsten uebergeordnete ID merken ...
				$this->temp[] = $val['parent_id'];
				// ... und rekursiv weiterhangeln.
				$this->_getHighlightId($val['parent_id'], false);
			}
		}
		// am schluss gesammelte ids zurueckgeben
		return $this->temp;
	}

/** Funktion zum Loeschen des Navi-Cache-Files
* 
* @access   public
*/
	function clearCache() {
		if (empty($this->sCachefile)) {
			if ($this->debug) { die("ERROR: empty var sCachefile!"); } else { return; }
		}
		if (!file_exists($this->sCachefile)) {
			if ($this->debug) { die("ERROR: sCachefile does not exist!"); } else { return; }
		}
		// delete file
		@unlink($this->sCachefile);
		
		// baue und lade Navi-Array neu
		$this->_initTree();
	}

# --------------------------------------------------------------------------------

/** Ausgabe-Funktion: Gebe Gesamtanzahl Eintraege aus
* Beispiel: 
* <pre><code> 
* echo "gefundene Datensaetze: ".$oTree->getCountItems(); 
* </code></pre>
* 
* @access   public
* @return	array
*/
	function getCountItems() {
		return $this->countFlatArr;
	}

/** Ausgabe-Funktion: Gebe Verschachtelungstiefe aus
* 
* @access   public
* @return	int
*/
	function getCountLevel() {
		return $this->countLevel;
	}

/** Ausgabe-Funktion: Gebe Haupt-Navigationspunkte als Array aus.
*
* Beispiel: 
* <pre><code> 
* $aMainNavi = $oNav->getMainNavi(); // params: -
* foreach($aMainNavi as $key => $aNaviItem) { echo $aNaviItem[href]; ... } 
* </code></pre>
*
* @access   public
* @return	array
*/
	function getMainNavi() {
		return $this->_getAllChildren('0', '0');
	}

/** Ausgabe-Funktion: Gebe Unter-Navigationspunkte eines Navigationspunktes als Array aus.
*
* Wird der optionale Parameter "nParentId" uebergeben, werden die Unterpunkte 
* dieses Hauptpunktes zurueckgegeben, sonst wird der "aktuelle" Hauptpunkt ermittelt 
* und seine Unterpunkte zurueckgegeben.
*
* Beispiel: 
* <pre><code> 
* $aSubNavi = $oNav->getSubNavi(); // params: [$nParentId=null][,$nTopnaviId=null]
* foreach($aSubNavi as $key => $aNaviItem) { echo $aNaviItem[href]; ... } 
* </code></pre>
*
* @access   public
* @param	string	[$parentId]		(optional) "needle in a haystack" - default: null
* @param	string	[$nTopnaviId]	(optional) "needle in a haystack" - default: null
* @return	array
*/
	function getSubNavi($nParentId=null, $nTopnaviId=null) {
		// check ggf. uebergebene $nParentId
		if (!is_null($nParentId)) $nParentId = ($nParentId + 0);
		if (!is_null($nParentId) && $nParentId == 0) $nParentId = null;
		// "richtige" parent-id ermitteln wenn keine uebergeben wurde
		if ($nParentId == null) {
			$nParentId = (isset($this->aHighlightId[$this->countLevel-1])) 
				? $this->aHighlightId[$this->countLevel-1] 
				: $this->nNaviId;
			if ($nParentId == null) $nParentId = 0; // fallback
		}
		// "richtige" $nFlagTopnavi ermitteln wenn keine uebergeben wurde
		if (is_null($nTopnaviId)) {
			// finde aktuellen Array-Eintrag
			$curr = $this->_getCurrentPosition($nParentId);
			$nTopnaviId = $this->flatArr[$curr]['flag_topnavi'];
		}
		// output
		return $this->_getAllChildren($nParentId, $nTopnaviId);
	}

/** Ausgabe-Funktion: Gebe Top-/Service-Navigationspunkte (inkl. aller Unterpunkte) als Array aus.
*
* Ermittelt fuer Webseite freigegebene Navi-Punkte, die nicht in der Hauptnavi erscheinen sollen - 
* z.B. Servicenavi mit Kontakt, Impressum etc...
* Optional kann die Topnavi-Nummer uebergeben werden (z.B. bei mehr als einer ServiceNavi).
*
* Beispiel: 
* <pre><code> 
* $aServiceNavi = $oNav->getServiceNavi(); // params: [$nTopnaviId='1']
* foreach($aServiceNavi as $key => $aNaviItem) { echo $aNaviItem[href]; ... } 
* </code></pre>
*
* @access   public
* @param	string	[$nTopnaviId]	(optional) Welcher Service-Navigations-Branch - default: 1
* @return	array
*/
	function getServiceNavi($nTopnaviId='1') {
		// output
		return $this->_getAllChildren('0', $nTopnaviId);
	}

/** Hilfs-Funktion: Gebe die Kinder (und Kindeskinder) einer $parentId als Array zurueck ("{FIRST_SUB}" ist aufgeloest!)
* 
* @access   private
* @uses		function "compareOnlineFromToWithToday()" [file: func.admin_common.php]
* @param	string	[$parentId]	(optional) "needle in a haystack" - default: 0
* @param	array	[$arr]		-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function _getAllChildren($parentId='0', $nTopnaviId='0', $arr='') {
		// check vars
		#if ($this->countFlatArr == 0) return array();
		if (!function_exists('compareOnlineFromToWithToday')) die("function 'compareOnlineFromToWithToday()' does not exist!");
		// init
		if ($arr == '') {
			$arr = $this->treeArr;
			$this->temp = array();
		}
		//print_r($arr);
		foreach ($arr as $key => $val) {
			if ($val['parent_id'] == $parentId) {// gefunden!
				// "valid"-check
				if ($this->bValidOnly) { // (->"continue" ueberspringt den rest der schleife)
					if ($val['flag_online_'.$this->sLang] == "0") { continue; }	// nur freigegebene navi-punkte anzeigen
					if ($val['flag_navi'] != "1") { continue; }	// keine versteckten navi-punkte anzeigen
				}
				$val['flag_online'] = $val['flag_online_'.$this->sLang];
				// nur "richtigen" navi-branch anzeigen
				if ($val['flag_topnavi'] != $nTopnaviId) { continue; } // andere navi-aeste (z.b. topnavi) ueberspringen
				
				// zeitgesteuerte anzeige (nur webansicht, nicht im CMS)
				if ($this->bValidOnly) {
					$isValid = compareOnlineFromToWithToday($val['online_from_'.$this->sLang], $val['online_to_'.$this->sLang]);
					if (!$isValid) continue;
				}
				/*foreach ($this->aENV['content_language'] as $k => $v) {
					if ($k == 'default') continue;
					// zusaetliches web-feature: wenn {FIRST_SUB} angegeben wurde -> verlinke auf ersten freigegebenen(!) unterpunkt
					if ($val['href_'.$k] == '{FIRST_SUB}') {
						$val['href_'.$k] = $this->_getFirstSubHref($val['id'],$k); // ueberschreibe "href"
					}*
				}*/
				// zusaetliches web-feature: wenn {FIRST_SUB} angegeben wurde -> verlinke auf ersten freigegebenen(!) unterpunkt
				if ($val['href_'.$this->sLang] == '{FIRST_SUB}') {
					$val['href_'.$this->sLang] = $this->_getFirstSubHref($val['id'],$this->sLang);	 // ueberschreibe "href"
				}
				// zur abwaertskompatibilitaet die defaultsprache mit kuerzel...
				$val['href'] = $val['href_'.$this->sLang];
				// Feature: zusaetzlicher Parameter 'highlight'
				$val['highlight'] = (is_array($this->aHighlightId) && in_array($val['id'], $this->aHighlightId)) ? 1 : 0;
				// merken und am schluss zurueckgeben
				$this->temp[$key] = $val;
			} else {
				$this->_getAllChildren($parentId, $nTopnaviId, $val['children']);
			}
		}
		return $this->temp;
	}

/** Hilfs-Funktion: Gebe den Filename des ersten Kindes einer $parentId (als String-Replacement fuer "{FIRST_SUB}") zurueck.
* 
* @access   private
* @param	string	$parentId	ID des Datensatzes, dessen FirstSub-Href ermittelt werden soll
* @param	string  $lang
* @return	string
*/
	function _getFirstSubHref($parentId,$lang) {
		$this->oDb->query("SELECT id, filename_".$lang.", link   
							FROM ".$this->sTable." 
							WHERE parent_id = '".$parentId."' 
								AND flag_online_".$this->sLang." <> '0' 
								AND flag_navi <> '0' 
							ORDER BY prio DESC 
							LIMIT 0, 1");
		$tmp = $this->oDb->fetch_array();
		if ($tmp['link'] == '{FIRST_SUB}') {
			return $this->_getFirstSubHref($tmp['id'],$lang); 
		}
		else {
			return (!empty($tmp['filename_'.$lang])) ? $tmp['filename_'.$lang] : '#'; // ueberschreibe "href"
		}
	}

/** Hilfs-Funktion: Gebe nur die IDs der Kinder (und Kindeskinder -> rekursiv!) 
* einer $parentId als flaches (numerisches) Array zurueck. 
* Beispiel: 
* <pre><code> 
* $aChildrenId = $oTree->getAllChildrenId(); 
* </code></pre>
* 
* @access   public
* @param	string	[$parentId]	(optional) "needle in a haystack" - default: 0
* @param	array	[$arr]		-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function getAllChildrenId($parentId='0', $arr='') {
		if ($this->countFlatArr == 0) return array();
		if ($arr == '') {
			$arr = $this->treeArr;
			$this->temp = array(); // reset
		}
		foreach ($arr as $key => $val) {
			if ($val['parent_id'] == $parentId) {
				$this->temp[] = $val['id']; // gefunden! -> ID merken und am schluss zurueckgeben
				$this->getAllChildrenId($val['id'], $val['children']); // auch die Kinder der gefundenen ID sammeln!
			} else {
				$this->getAllChildrenId($parentId, $val['children']); // ... ansonsten weiterhangeln...
			}
		}
		return $this->temp;
	}

/** Hilfs-Funktion: Gebe nur die IDs der Eltern (und Grosseltern -> rekursiv!) 
* einer $Id als flaches (numerisches) Array zurueck. 
* Beispiel: 
* <pre><code> 
* $aParentId = $oTree->getAllParentId($navi_id); 
* </code></pre>
* 
* @access   public
* @param	string	[$id]		Navigationspunkt-ID, dessen Ahnen ermittelt werden sollen
* @param	array	[$firstrun]		-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function getAllParentId($id, $firstrun=true) {
		if ($this->countFlatArr == 0 || empty($id)) return;
		if ($firstrun == true) {
			$this->temp = array(); // reset
		}
		foreach ($this->flatArr as $key => $val) {
			if ($val['id'] == $id && $val['parent_id'] == '0') { break; } // oben angekommen... schleife beenden
			if ($val['id'] == $id) { // gefunden!
				array_unshift($this->temp, $val['parent_id']); // -> ID an den Anfang haengen
				$this->getAllParentId($val['parent_id'], false);
			}
		}
		return $this->temp; // alle gesammelten IDs zurueckgeben
	}

/** Erstell-Funktion Funktion: Gebe die aktuelle Position des aktuellen Datensatzes zurueck
* 
* @access   private
* @param	int	$id		ID des aktuellen Datensatzes
* @return	int			Position im flatArr
*/
	function _getCurrentPosition($id) {
		if ($this->countFlatArr == 0) return;
		// ueberpruefe Zeiger und gebe ihn ggf. direkt zurueck
		if ($this->flatArr[$this->curPos]['id'] == $id) {
			return $this->curPos;
		} else {
		// ansonsten finde aktuellen Array-Eintrag
			reset($this->flatArr);
			while ($v = current($this->flatArr)) {
				if ($v['id'] == $id) {
					break;
				} else {
					next($this->flatArr);
				}
			}
			$this->curPos = key($this->flatArr); // aktuelle Position speichern...
			return $this->curPos; // ... und zurueckgeben
		}
	}

/** Ausgabe-Funktion: Gebe die ID des vorigen Datensatzes zurueck
* 
* @access   public
* @param	int	$id		ID des aktuellen Datensatzes
* @return	int			ID des vorigen Datensatzes
*/
	function getPrev($id) {
		if ($this->countFlatArr == 0) return;
		// finde aktuellen Array-Eintrag
		$curr = $this->_getCurrentPosition($id);
		// gehe einen Schritt zurueck
		if ($curr-1 < 0) {
			return false; // wenn schon erster Eintrag
		} else {
			return $this->flatArr[$curr-1]['id']; // id zurueckgeben
		}
	}

/** Ausgabe-Funktion: Gebe die ID des naechsten Datensatzes zurueck
* 
* @access   public
* @param	int		$id		ID des aktuellen Datensatzes
* @return	mixed			ID des naechsten Datensatzes(int) || false (boolean)
*/
	function getNext($id) {
		if ($this->countFlatArr == 0) return;
		// finde aktuellen Array-Eintrag
		$curr = $this->_getCurrentPosition($id);
		// gehe einen Schritt weiter
		if ($curr+1 >= $this->countFlatArr) {
			return false; // wenn schon letzter Eintrag
		} else {
			return $this->flatArr[$curr+1]['id']; // id zurueckgeben
		}
	}

/** Ausgabe-Funktion: Gebe die ID des Eltern-Datensatzes zurueck
* 
* @access   public
* @param	int		$id		ID des aktuellen Datensatzes
* @return	mixed			ID des Eltern-Datensatzes(int) || false (boolean)
*/
	function getParent($id) {
		if ($this->countFlatArr == 0) return;
		// finde aktuellen Array-Eintrag
		$curr = $this->_getCurrentPosition($id);
		return $this->flatArr[$curr]['parent_id']; // id zurueckgeben
	}

/** Ausgabe-Funktion: Gebe die Array-Informationen des gesuchten Navigationspunktes zurueck
* 
* @access   public
* @param	int		$id		ID des aktuellen Datensatzes
* @return	mixed			ID des Eltern-Datensatzes(int) || false (boolean)
*/
	function getItemArray($id) {
		if ($this->countFlatArr == 0) return;
		// finde aktuellen Array-Eintrag
		$curr = $this->_getCurrentPosition($id);
		return $this->flatArr[$curr]; // array zurueckgeben
	}

#----------------------------------------------------------------------------- PATH

/** WEB-Ausgabe-Funktion: Gebe den kompletten Pfad als Array zurueck ({FIRST_SUB} ist aufgeloeest!)
*
* @access   public
* @return	array
*/
	function getPathArray() {
		// vars
		$aBuffer = array();
		if ($this->countLevel == 0) return $aBuffer;
		// durchlaufe aHighlightId-array (top->down, also aktuelle Seite zuerst!)
		
		for ($i = 0; $i < $this->countLevel; $i++) {
			$tmp = $this->getItemArray($this->aHighlightId[$i]);
			// zusaetliches web-feature: wenn {FIRST_SUB} angegeben wurde -> verlinke auf ersten freigegebenen(!) unterpunkt
			if ($tmp['href'] == '{FIRST_SUB}') {
				$tmp['href'] = $this->_getFirstSubHref($tmp['id']); // ueberschreibe "href"
			}
			if ($i == 0) { // => aktueller menupunkt
				$aBuffer[$i] = array( // "href" nur wenn detailseite (identifikator = $_GET['id'])
					'href' => (isset($_GET['id'])) ? $tmp['href'] : '', 
					'title' => $tmp['title_'.$this->sLang]
				);
			} else { // => alle parent menupunkte
				$aBuffer[$i] = array(
					'href' => $tmp['href'], 
					'title' => $tmp['title_'.$this->sLang]
				);
			}
		}
		
		return $aBuffer;
	}

/** WEB-HTML-Ausgabe-Funktion: Gebe den kompletten Pfad als HTML-String zurueck
*
* Beispiel: 
* <pre><code> 
* echo $oNav->getPathHTML(); // params: [$sTrenner=' &raquo; ']
* </code></pre>
*
* @access   public
* @param	string	$sTrenner	Trennzeichen, das zwischen den Pfadpunkten erscheinen soll
* @return	array
*/
	function getPathHTML($sTrenner=' &raquo; ') {
		$aPath = $this->getPathArray(); // path-array holen
		$sPath = '';
		// array durchlaufen und HTML bauen
		foreach($aPath as $aItem) {
			if (!empty($sPath)) $sPath .= $sTrenner;
			$sPath .= (!empty($aItem['href'])) 
				? '<a href="'.$this->aENV['path']['root']['http'].$aItem['href'].'" title="'.$aItem['title'].'" class="path" onfocus="this.blur()">'.$aItem['title'].'</a>' 
				: $aItem['title'];
		}
		// output
		return $sPath;
	}

#----------------------------------------------------------------------------- FOLDER

// schreibe die komplette navi-struktur rekursiv als html-links mit dhtml und icons
	function writeNavi($flag_topnavi=0, $divider='hDivider') { 
		$aStructure = $this->_getAllChildren('0', $flag_topnavi);
		$this->_writeNavi($aStructure, 0, 0, $divider);
	}
	// sub von "writeNavi()"
	function _writeNavi(&$data, $depth=0, $id=0, $divider='hDivider') { 

		if (!is_array($data)) return; 
		// umschliessender layer ist nicht noetig im level 0, da immer aufgeklappt
		if ($depth > 0) { // alle tieferen level erst mal geschlossen
			###echo '<div id="nLayer_'.$id.'" style="visibility:visible;display:none">'."\n";
			echo $this->oHtmlFolder->getLayerStart($id);
		}
		// ebene durchlaufen
		foreach ($data as $key => $val) {
			
			// get sub-navi array
			$aSubNavi = $this->getSubNavi($val['id']); // params: [$nParentId=null]
			$nEntriesSub = count($aSubNavi); // get total sub-entries
			
			// navi entry
			echo $this->getNaviItem($val, $nEntriesSub, $depth);	#print_r($val); // DEBUG

			// rekursiv weiterhangeln
			if ($nEntriesSub > 0) { $this->_writeNavi($aSubNavi, ++$depth, $val['id']); --$depth; }
			
			// divider fuer level 0 (drueber)
			if ($depth == 0) {
				###echo '<div class="'.$divider.'"><img src="'.$this->aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"></div>'."\n";
				echo $this->getDivider($divider);
			}
		}
		// umschliessenden layer schliessen
		if ($depth > 0) {
			###echo '</div>'."\n";
			echo $this->oHtmlFolder->getLayerEnd();
		}
	}
// sub von "_writeNavi()": gebe den Divider zurueck
	function getDivider($class) {
		// output
		return $this->oHtmlFolder->getDivider($class);
	}
// sub von "_writeNavi()": gebe den aktuellen navi-item formatiert zurueck
	function getNaviItem(&$aItem, $nEntriesSub=0, $depth=0) { 
		global $weblang;
			
		// einrueckung
		$sStr = $this->getEinrueckung($depth);
		
		// toggle icon
		$sStr .= $this->getIcon($aItem['id'], $nEntriesSub);
		
		// highlight
		$class	= ($aItem['highlight'] == 1) ? 'cnavihi' : 'cnavi';
		
		// wenn nicht fuer Navi freigegeben wird Eintrag eingerueckt
		if ($aItem['flag_navi'] == 0) { $class .= "off"; }
		
		// build href
		$href	= $this->aENV['SELF_PATH'].'template.'.$aItem['template'].'.php?navi_id='.$aItem['id'];
		
		// wenn nicht fuer Webseite freigegeben erscheint Eintrag kursiv
		$kursiv = ($aItem['flag_online_'.$weblang] == "0") ? true : false;
		$sStr .= $this->getLink($href, $class, $aItem['title_'.$weblang], $kursiv);
		// output
		return $sStr;
	}
// sub von "getNaviItem()": gebe die richtigen einrueckung zurueck
	function getEinrueckung($depth) {
		$spacer_width = 0;
		for ($i = 0; $i < $depth; $i++) { $spacer_width += $this->nIndent; }
		// output
		return ($spacer_width > 0) ? $this->oHtmlFolder->getSpacer($spacer_width) : '';
	}
// sub von "getNaviItem()": gebe das richtige icon zurueck
	function getIcon($id, $nEntriesSub) {
		// output
		return ($nEntriesSub > 0) 
			? $this->oHtmlFolder->getIconFolder($id) 
			: $this->oHtmlFolder->getIconFile();
	}
// sub von "getNaviItem()": gebe den kompletten Link zurueck
	function getLink($href, $class, $link, $kursiv=false) {
		global $syslang, $aMSG;
		
		if(empty($link))
			$link	= $aMSG['std']['no_title_lang'][$syslang];
		else
			$link	=  shorten_text($link, 20);
		
		// ggf. kursiv
		if ($kursiv) { $link = '<i>'.$link.'</i>'; }
		
		// output
		return $this->oHtmlFolder->getLink($href, $class, $link);
	}
	
// schreibe ein JavaScript fuer den Vorlader fuer Toggle-Icon-tausch
	function getJsIconPreloader() {
		// output
		return $this->oHtmlFolder->getJsIconPreloader();
	}
// schreibe ein JavaScript, mit dem defaultmaessig die richtigen unterpunkte aufgeklappt werden
	function getJsOpenDefault() {
		$highlight_ids = implode('","', $this->aHighlightId);
		// output
		return $this->oHtmlFolder->getJsOpenDefault($highlight_ids);
	}


#-----------------------------------------------------------------------------
} // END of class

?>