<?php
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung einen kleinen 
* HTML-Kalender zu erstellen, in dem "Heute" und ggf. Termine markiert sind. 
*
* Example: 
* <pre><code> 
* $oCal =& new calendar(); // params: [$lang (default: de)]
* $oCal->_html['td'] = '<td class="{CLASS}" title="{TITLE}">{TD}</td>'; // ueberschreibe default...
* $oCal->addDate(array("2004-07-18"=>"Andy's Geburtstag!")); // params: $aDate 
* echo $oCal->getMinicalendarHtml(); // params: [$sHref=''][,$sButAddGetVars=''][,$bWithCw=false] 
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.4 / 2006-03-13 [NEU: franz. Sprachversion]
*/
class calendar {
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse archive:
		----------------------------------------------------------------------------
		konstruktor calendar($lang = 'de')
		function setFilter($aFilter)
		function getDayEvents(&$oDb, $timestamp=NULL, $today=NULL)
		function addDate($aDate)
		function getMinicalendarHtml($sHref='', $sButAddGetVars='', $bWithCw=false, $sCwHref='')
		function getMinicalendarWap($sHref='', $sButAddGetVars='', $bWithCw=false, $sCwHref='')
		function getMonthcalendarHtml(&$oDb, $bWithCw=false, $sEditPage='', $sDayViewPage='', $sWeekViewPage='', $bWithNewButton=true)
		
		function getCustomLabel()
		function getCustomLabelColour()
		function setCustomLabels()
		----------------------------------------------------------------------------
		HISTORY:
		1.4 / 2006-03-13 [NEU: franz. Sprachversion]
		1.32 / 2006-01-19 [NEU: "$bWithNewButton" bei "getMonthcalendarHtml()"]
		1.31 / 2006-01-17 [NEU: Kalenderwoche verlinkt bei "getMinicalendarHtml()" + "getMonthcalendarHtml()"]
		1.3 / 2005-07-04 [NEU: Kalenderwoche bei "getMinicalendarHtml()" + "getMonthcalendarHtml()"]
		1.23 / 2005-04-01 [BUGFIX: GET-Vars filtern bei "getMonthcalendarHtml()"]
		1.22 / 2005-03-30 [NEU: "office"-Filter bei "getDayEvents()"]
		1.21 / 2005-03-29 [NEU: private events fuer andere ausblenden b. "getDayEvents()"]
		1.2 / 2005-03-16 [NEU: Geburtstage auslesen b. "getDayEvents()" + Month-Array v. HTML getrennt + div. Umbenennungen]
		1.1 / 2005-03-10 [NEU: "getDayEvents()" + "getMonthcalendarHtml()" + Hilfsfunktionen]
		1.0 / 2004-07-22
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	array	Namen aller Wochentage (Abkuerzungen!)
*/
    var $_daynames;
/**
* @access   private
* @var	 	array	Namen aller Monatstage
*/
    var $_monthnames;
/**
* @access   private
* @var	 	string	Ausgabesprache der Wochentage/-monate
*/
    var $_lang;
/**
* @access   private
* @var	 	array	HTML-Template-Geruest der Kalender-Tabelle
*/
    var $_html;
/**
* @access   private
* @var	 	array	Termine (mysql-date-format) die einfach nur ge-highlightet werden
*/
    var $_dates;
/**
* @access   private
* @var	 	array	Termine eines Tages (aehnlich wie _dates, nur ausfuehrlicher, mit mehr Informationen als komplexes Array)
*/
    var $_events;
/**
* @access   private
* @var	 	array	Filter fuer Termine eines Tages
*/
    var $_filter;
/**
* @access   private
* @var	 	array	Monats-Kalender als Array
*/
    var $_monthcalendar;
/**
* @access   private
* @var	 	object	Rechteverwaltung fuer User und Teams
*/
    var $_oTACEA;
#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das calendar-Objekt und setzt die Ausgabesprache
* und initialisiert die Klassenvariablen.
*
* Beispiel: 
* <pre><code> 
* $oCal =& new calendar(); // params: [$lang (default: 'de')] 
* </code></pre>
*
* @access   public
* @param 	string	$lang	Ausgabesprache der Monats- und Tage-Namen (default: 'de')
* @param 	object	$oTACEA	Rechteverwaltung fuer User/Teams (Object ref.)
* @return   void
*/
	function calendar($lang = 'de', $oTACEA=null) {
		// cal vars
		$this->_calendarweek['de']	= 'KW';
		$this->_calendarweek['en']	= 'WK';
		$this->_calendarweek['fr']	= 'Semaine';
		$this->_daynames['de']		= array("Mo", "Di", "Mi", "Do", "Fr", "Sa", "So");
		$this->_daynames['en']		= array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
		$this->_daynames['fr']		= array("Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim");
		$this->_monthnames['de']	= array("", "Januar", "Februar", "März", 
										"April",   "Mai",      "Juni", 
										"Juli",    "August",   "September", 
										"Oktober", "November", "Dezember");
		$this->_monthnames['en']	= array("", "January", "February", "March", 
										"April",   "May",      "June", 
										"July",    "August",   "September", 
										"October", "November", "December");
		$this->_monthnames['fr']	= array("", "Janvier", "Février", "Mars", 
										"Avril",   "Mai",      "Juin", 
										"Juillet", "Août",     "Septembre", 
										"Octobre", "Novembre", "Décembre");
		// lang
		$this->_lang = $lang;
		// html (minicalendar)
		$this->_html['header']	= '
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="calTableHead">
			<tr>
				<th width="60%" class="calHeadlineTD">{MONTHNAME} {YEAR}</th>
				<th width="40%" class="calButtonTD" align="right" nowrap><input type="button" value="&lt;" class="smallbut" onclick="location.href=\'{PREV_LINK}\'" title="{PREV_TITLE}"><input type="button" value="{TODAY_TITLE}" class="smallbut" onclick="location.href=\'{TODAY_LINK}\'" title="{TODAY_TITLE}"><input type="button" value="&gt;" class="smallbut" onclick="location.href=\'{NEXT_LINK}\'" title="{NEXT_TITLE}"></th>
			</tr>
		</table>';
		$this->_html['table_start']	= '<table width="100%" border="0" cellspacing="1" cellpadding="2" class="calTableBody">';
		$this->_html['table_end']	= '</table>';
		$this->_html['tr_start']	= '<tr>';
		$this->_html['tr_end']		= '</tr>';
		$this->_html['th']			= '<th class="calDaynames">{TH}</th>';
		$this->_html['td']			= '<td class="{CLASS}" title="{TITLE}">{TD}</td>';
		// dates / events
		$this->_dates				= array();
		$this->_events				= array();
		$this->_filter				= array('show_birthdays' => true); // zeige die Geburtstage bei "getDayEvents()" an.
		// array (monthcalendar)
		$this->_buildMonthCalendarArray(); // -> $this->_monthcalendar
		// Rechte
		$this->_oTACEA =& $oTACEA;
	}

#----------------------------------------------------------------------------- DAY EVENTS

/**
* Ermittelt alle deutschen Feiertage.
* @access   public
* @return   array	Feiertage als assioziatives Array (key:name, val:timestamp)
*/
	function getGermanFreeDays() {

		$easter = $this->getOrthodoxEaster(date('U'));
		$freedays['neujahr'] = mktime(0,0,0,1,1,date('Y'));
		// Ostern
		//karfreitag liegt 2 Tage zurueck
		$freedays['karfreitag'] = $easter-172800;
		$freedays['ostersonntag'] = $easter;
		$freedays['ostermontag'] = $easter+86400;
		$freedays['maifeiertag'] = mktime(0,0,0,5,1,date('Y'));
		$freedays['tagderdteinheit'] = mktime(0,0,0,10,3,date('Y'));
		// Weihnachten
		$freedays['heiligabend'] = mktime(0,0,0,12,24,date('Y'));
		$freedays['1weihnachtstag'] = mktime(0,0,0,12,25,date('Y'));
		$freedays['2weihnachtstag'] = mktime(0,0,0,12,26,date('Y'));

		return $freedays;
	}
/**
* Takes any Gregorian date and returns the Gregorian date of Orthodox Easter for that year.
* @access   public
* @param	array	$date		timestamp
* @return   array	Ostern als timestamps
*/
	function getOrthodoxEaster($date){
		
		$year = date("Y", $date);
		$r1 = $year % 19;
		$r2 = $year % 4;
		$r3 = $year % 7;
		$ra = 19 * $r1 + 16;
		$r4 = $ra % 30;
		$rb = 2 * $r2 + 4 * $r3 + 6 * $r4;
		$r5 = $rb % 7;
		$rc = $r4 + $r5;
		//Orthodox Easter for this year will fall $rc days after April 3
		return strtotime("3 April $year + $rc days");
	}

/**
* Setzt Filter, die die Ausgabe der Funktion "getDayEvents()" einschraenkt.
* Z.B. kann mit array("show_birthdays" => false) abgeschaltet werden, dass Geburtstage ermittelt werden.
* Die anderen moeglichen Filter sind: label (@see setup.php), participant (eine user_id), project_id (eine project_id).
*
* Beispiel: 
* <pre><code> // zeige nur meine privaten Termine
* $aFilter = array(
* 	"show_birthdays" => false, 
* 	"label" => 'private', 
* 	"office" => $Userdata['office_id'], 
* 	"participant" => $Userdata['id'] 
* );
* $oCal->setFilter($aFilter); // params: $aFilter 
* </code></pre>
*
* @access   public
* @param	array	$aFilter		Filter
* @return   array	$aFilter
*/
	function setFilter($aFilter) {
		if (!$aFilter || !is_array($aFilter)) return; // check vars
		// add
		$this->_filter = $aFilter;
		return $this->_filter;
	}

/**
* ermittelt Termine an einem optional als Timestamp oder ISO-Date uebergebenen Datum (default=heute)
* und gibt die ermittelten Informationen als Array zurueck.
*
* Beispiel: 
* <pre><code> 
* $aEvents = $oCal->getDayEvents($oDb); // params: &$oDb[,$day=NULL][,$media='']
* </code></pre>
*
* @access   public
* @param	object	$oDb		Datenbank-Objekt (by reference)
* @param	string	$day		UNIX-timestamp oder ISO-Date (optional - default: heute)
* @param	string	$media      Mediaparam (WAP o. HTTP) passt die jeweiligen Links an
* @return   string	HTML Kalender
* @see		setFilter()
*/
	function getDayEvents(&$oDb, $day=NULL, $media='') {
		global $aENV, $aMSG, $Userdata;
		
		require_once($aENV['path']['global_service']['unix']."class.team.php");
		require_once($aENV['path']['global_service']['unix']."class.user.php");
				
		$oTeam =& new team($oDb);
		$oUser =& new user($oDb);
		
		$aUsergroup = $oTeam->getAllTeamNames(); // params: [$bWithUsers=false] 
 		$aLabels = Tools::getLabels($oDb,$aENV,'cal','business');
		// check vars
		if (!isset($aENV['table']['cal_event']) || empty($aENV['table']['cal_event'])) die("class.calendar.php -> ERROR: aENV['table']['cal_event'] nicht definiert!");
		if (!is_null($day)) { // es wurde ein datum uebergeben
			$isodate = (strpos($day, '-') === false) ? date("Y-m-d", $day): $day; // timestamp oder iso-date?
		} else {
			$isodate = date("Y-m-d");// kein datum -> default: heute
		}
		$this->_events = array();
		$i = 0; // key (counter)
		
		// ADR: Geburtstage auslesen ----------------------------------------------------------------------------
		if (isset($aENV['module']['adr']) && $this->_filter['show_birthdays'] == true) {
			// get birthdays of today
			$oDb->query("SELECT `id`,`title`,`firstname`,`surname`,`birthday`, DAYOFYEAR(birthday) as `dayofyear` 
						FROM `adr_contact` 
						WHERE DAYOFMONTH(birthday) = DAYOFMONTH('".$isodate."')
							AND MONTH(birthday) = MONTH('".$isodate."')");
			while ($aData = $oDb->fetch_array()) {
				$this->_events[$i] = array();
				$this->_events[$i]['icon']			= '<img src="'.$aENV['path']['pix']['http'].'icn_birthday.gif" border="0" alt=""> ';
				$this->_events[$i]['flag_allday']	= 1; // ganztagestermin
				$this->_events[$i]['time_start']	= '00:00';
				$this->_events[$i]['time_end']		= '24:00';
				$this->_events[$i]['date_start']	= $isodate;
				$this->_events[$i]['date_end']		= $isodate;
				// name -> subject
				$name = ($media == "wap") 
					? '<a href="'.$aENV['path']['wap']['http'].'mbl_adr_detail.php?id='.$aData['id'].'&type=contact" title="'.$aMSG['form']['details'][$this->_lang].'" class="textLink">' 
					: '<a href="'.$aENV['path']['adr']['http'].'adr_detail.php?id='.$aData['id'].'&type=contact" title="'.$aMSG['form']['details'][$this->_lang].'">';
				if ($aData['title']) {
					$name .= ' '.$aData['title'];
				}
				$name .= $aData['firstname'].' '.$aData['surname'];
				$name .= '</a>';
				// geburtsjahr an namen haengen
				#$year = substr($aData['birthday'], 0, 4);
				#if ($year != "0000") $name .= " (".$year.")";
				$this->_events[$i]['subject']		= $name;
				
				// dauer in stunden (insgesamt $event['hours_total'] + pro tag $event['hours_today'])
				$this->_events[$i] = $this->_calculateHours($this->_events[$i], $isodate);
				// ausgabe start- und end-zeit
				$this->_events[$i] = $this->_getFormattedTime($this->_events[$i], $isodate);
				// counter hochzaehlen
				$i++;
			}
		} // END ADR ------------------------------------------------------------------------------------------
		
		// SQL
		$oDb->query("SELECT `id`,`subject`,`location`,`event_start`,`event_end`,`event_arrival`,`event_return`,`flag_allday`,`flag_confirmed`,`label`,`flag_private_event`,`office`,`project_id`,`label_id`,`comment` 
					FROM `".$aENV['table']['cal_event']."` 
					WHERE DATE_FORMAT(event_start, '%Y-%m-%d') = '".$isodate."' 
						OR DATE_FORMAT(event_end, '%Y-%m-%d') = '".$isodate."' 
						OR (DATE_FORMAT(event_start, '%Y-%m-%d') < '".$isodate."' 
							AND DATE_FORMAT(event_end, '%Y-%m-%d') > '".$isodate."') 
					ORDER BY `flag_allday` DESC, `event_start` ASC, `subject` ASC");
		
		while ($tmp = $oDb->fetch_array()) {
			if (count($this->_filter) > 0) { // es gibt mindestens einen Filter
				// filter: label
				if (isset($this->_filter['label']) && isset($aLabels) && is_array($aLabels)) {
					foreach ($aLabels as $key => $val) {
						if($key == $this->_filter['label']) {
							$id = $key;
							break;	
						}
					}
					if($this->_filter['label'] != $tmp['label'] && $this->_filter['label'] != $tmp['label_id']) {
				 		continue;
					}
				}
				// filter: project
				if (isset($this->_filter['project_id']) && $this->_filter['project_id'] != $tmp['project_id']) continue;
				// filter: participant
				if (isset($this->_filter['participant'])) {
					$uright = $this->_oTACEA->getRightForID($tmp['id'],'cal');
					if(!is_array($uright[$this->_filter['participant']])) {
						continue;
					}
				} 
			}
			// Teilnehmer ermitteln...
			$uright = $this->_oTACEA->getRightForID($tmp['id'],'cal');
			$aURight = array();
			if(is_array($uright)) {
				foreach($uright as $key => $val) {
					$aURight[] = $key;
				}
			}
			// ... und ggf. private events fuer andere ausblenden
			if (isset($Userdata['id']) && $tmp['flag_private_event'] == 1) {
				if($Userdata['flag_da_service'] == 0) {
					if (!in_array($Userdata['id'], $aURight)) {
						continue;
					}
				}
			}
			
			// ggf. nur events fuer dieses office einblenden
			if (isset($this->_filter['office']) && !empty($tmp['office'])) {
				$aOffice = explode(',', $tmp['office']);
				if (!in_array($this->_filter['office'], $aOffice)) continue;
			}
			list($date_start, $time_start)	= explode(' ', $tmp['event_start']);
			list($date_end, $time_end)		= explode(' ', $tmp['event_end']);
			// vars in array zwischenpeichern
			$this->_events[$i]['id']			= $tmp['id'];
			$this->_events[$i]['subject']		= $tmp['subject'];
			$this->_events[$i]['location']		= $tmp['location'];
			$this->_events[$i]['time_start']	= substr($time_start, 0, 5);
			$this->_events[$i]['time_end']		= str_replace('23:59', '24:00', substr($time_end, 0, 5));
			$this->_events[$i]['date_start']	= $date_start;
			$this->_events[$i]['date_end']		= $date_end;
			$this->_events[$i]['arrival']		= $tmp['event_arrival'];
			$this->_events[$i]['return']		= $tmp['event_return'];
			$this->_events[$i]['flag_allday']	= $tmp['flag_allday']; // ganztagestermin
			$this->_events[$i]['flag_confirmed']= $tmp['flag_confirmed'];
			$this->_events[$i]['label']			= $tmp['label'];
			$this->_events[$i]['labelid']		= $tmp['label_id'];
			$this->_events[$i]['project_id']	= $tmp['project_id'];
			$this->_events[$i]['comment']		= $tmp['comment'];
			
			// Participants ermitteln
			$participants = $this->_oTACEA->getParticipants($oTeam,$oUser,'cal',$tmp['id']);
				
			if(is_array($participants)) {
					
				$this->_events[$i]['participant'] = implode(', ',$participants);
			}
			
			unset($participants);
			
		// zusaetzliche virtuelle felder
			// mehrtaegig (flag + pfeile)
			$this->_events[$i]['flag_multidays']= ($date_start == $isodate && $date_end == $isodate) ? 0 : 1;
			$this->_events[$i]['sign_multidays']=  '';
			if ($this->_events[$i]['flag_multidays'] == 1 && $date_start != $isodate && $date_end != $isodate) {
				$this->_events[$i]['sign_multidays'] .= '<img src="'.$aENV['path']['pix']['http'].'icn_mdmiddle.gif" border="0" alt="" hspace="4">';
			}
			else if ($this->_events[$i]['flag_multidays'] == 1 && $date_start < $isodate) {
				$this->_events[$i]['sign_multidays'] .= '<img src="'.$aENV['path']['pix']['http'].'icn_mdstop.gif" border="0" alt="" hspace="4">';
			}
			else if ($this->_events[$i]['flag_multidays'] == 1 && $date_end > $isodate) {
				$this->_events[$i]['sign_multidays'] .= '<img src="'.$aENV['path']['pix']['http'].'icn_mdstart.gif" border="0" alt="" hspace="4">';
			}
			// icon
			$this->_events[$i]['icon']				= ''; // default = kein icon
			if ($this->_events[$i]['label'] != "business") {
				$Lbl = $this->_events[$i]['label'];
				$this->_events[$i]['icon']			= '<img src="'.$aENV['path']['pix']['http'].'icn_'.$Lbl.'.gif" border="0" alt=""> ';
			}
			// dauer in stunden (insgesamt $event['hours_total'] + pro tag $event['hours_today'])
			$this->_events[$i] = $this->_calculateHours($this->_events[$i], $isodate);
			// ausgabe start- und end-zeit
			$this->_events[$i] = $this->_getFormattedTime($this->_events[$i], $isodate);
			// counter hochzaehlen
			$i++;
		}
		// output
		return $this->_events;
	}
/**
* Hilfsfunktion fuer getDayEvents(). Berechnet die Anzahl Stunden die ein Termin andauert (insgesamt und nur heute)
*
* @access   private
* @param	array	$event		Termin-Datensatz (by reference)
* @param	string	$date_today	ISO-Datum von heute
* @return	array	$event		modifizierten Termin-Datensatz
* @see   	getDayEvents()
*/
	function _calculateHours(&$event, $date_today) {
		// calculate total duration time in hours
		list($start_hr, $start_min)		= explode(':', $event['time_start']);
		list($end_hr, $end_min)			= explode(':', $event['time_end']);
		list($start_year, $start_month, $start_day)	= explode('-', $event['date_start']);
		list($end_year, $end_month, $end_day)		= explode('-', $event['date_end']);
		$delta = mktime($end_hr, $end_min, 0, $end_month, $end_day, $end_year) - mktime($start_hr, $start_min, 0, $start_month, $start_day, $start_year); 
		$event['hours_total'] = $delta / 60 / 60;
		// make integers
		$start = substr($event['time_start'], 0, 2) + (substr($event['time_start'], 3, 5) / 60);
		$stop = substr($event['time_end'], 0, 2) + (substr($event['time_end'], 3, 5) / 60);
		if ($event['date_start'] < $date_today && $event['date_end'] == $date_today) {
		// mehrtaegiger termin (vergangenheit bis heute)
			$stop = substr($event['time_end'], 0, 2) + (substr($event['time_end'], 3, 5) / 60);
			$event['hours_today'] = $stop;
		} elseif ($event['date_start'] == $date_today && $event['date_end'] > $date_today) {
		// mehrtaegiger termin (heute bis zukunft)
			$start = substr($event['time_start'], 0, 2) + (substr($event['time_start'], 3, 5) / 60);
			$event['hours_today'] = (24 - $start);
		} elseif ($event['date_start'] < $date_today && $event['date_end'] > $date_today) {
		// mehrtaegiger termin (verganenheit bis zukunft)
			$event['hours_today'] = 24;
		} else {#if ($event['date_start'] == $date_today && $event['date_end'] == $date_today) {}
		// eintaegiger termin
			$event['hours_today'] = $event['hours_total'];
		}
		return $event;
	}
/**
* Hilfsfunktion fuer getDayEvents(). Berechnet die Anzahl Stunden die ein Termin andauert (insgesamt und nur heute)
*
* @access   private
* @param	array	$event		Termin-Datensatz (by reference)
* @param	string	$date_today	ISO-Datum von heute
* @return	array	$event		modifizierten Termin-Datensatz
* @see   	getDayEvents()
*/
	function _getFormattedTime(&$event, $date_today) {
		
		// keine zeit anzeigen bei ganztags-terminen
		if ($event['flag_allday'] == 1) return $event;
		
		global $aMSG, $aENV;
		if (!isset($aMSG['form']['date_start'][$this->_lang])) { // ggf. MSG-file nachladen
			require_once($aENV['path']['php']['unix']."cal_messages.php");
		}
		
		// formatted time ...................................................................
		$event['show_time'] = '';
		// start time
		if ($event['date_start'] == $date_today) {
			if ($event['date_end'] != $date_today) {
				#$event['show_time'] .= $aMSG['form']['date_start'][$this->_lang].': ';
			}
			$event['show_time'] .= $event['time_start'];
		}
		// end time
		if ($event['date_end'] == $date_today) {
			if ($event['date_start'] != $date_today) {
				#$event['show_time'] .= $aMSG['form']['date_end'][$this->_lang].': ';
			} else {
				$event['show_time'] .= ' - '; // "bis" zeichen nur wenn termin heute anfaengt UND aufhoert!
			}
			$event['show_time'] .= $event['time_end'];
		}
		if (!empty($event['show_time'])) { $event['show_time'] .= '<br>'; }
		
		// formatted time incl. arrival/return .............................................
		$event['show_total_time'] = '';
		if ($event['arrival'] != '0.0'  || $event['return'] != '0.0') {
			// arrival
			if ($event['date_start'] == $date_today) {
				if ($event['arrival'] != '0.0') {
					list($start_h, $start_m) = explode(':', $event['time_start']);
					list($arrival_h, $arrival_m) = explode('.', $event['arrival']);
					$total_h = $start_h - $arrival_h;
					$total_m = $start_m;
					if ($arrival_m == '5') { // eine halbe stunde abziehen
						$total_m = ($start_m == '30') ? '00' : '30';
						if ($start_m == '00') $total_h --; // ggf. h eins runtersetzen
					}
					if (strlen($total_h) == 1 && $total_h < 10) { $total_h = '0'.$total_h; }
					$event['show_total_time'] .= $total_h.':'.$total_m;
				} else {
					$event['show_total_time'] .= $event['time_start'];
				}
			}
			// return
			if ($event['date_end'] == $date_today) {
				if ($event['date_start'] == $date_today) {
					$event['show_total_time'] .= ' - '; // "bis" zeichen nur wenn termin heute anfaengt UND aufhoert!
				}
				if ($event['return'] != '0.0') {
					list($end_h, $end_m) = explode(':', $event['time_end']);
					list($return_h, $return_m) = explode('.', $event['return']);
					$total_h = $end_h + $return_h;
					$total_m = $end_m;
					if ($return_m == '5') { // eine halbe stunde dazu
						$total_m = ($end_m == '30') ? '00' : '30';
						if ($total_m == '00') $total_h++; // ggf. h eins hochsetzen
					}
					if (strlen($total_h) == 1 && $total_h < 10) { $total_h = '0'.$total_h; }
					$event['show_total_time'] .= $total_h.':'.$total_m;
				} else {
					$event['show_total_time'] .= $event['time_end'];
				}
			}
		}
		if (!empty($event['show_total_time'])) {
			$event['show_total_time'] = ''.$event['show_total_time'].'<br>';
		}
		
		// output
		return $event;
	}

#----------------------------------------------------------------------------- MINICALENDAR

/**
* addiert Termine zu einem internen Array, die dann in der Ausgabe
* des Mini-Kalenders gehighlightet werden.
* Die Termine koennen als einzelner Termin (als einfaches Array) ODER 
* als mehrere Termine (als verschachteltes Array) uebergeben werden.
* Der Termin besteht immer aus Datum (array-key) und Titel (array-value)!
* Es koennen auch mehrere Termine an einem Tag sein.
*
* Beispiel: 
* <pre><code> 
* $dates_to_highlight = array(
* 	"2004-07-13" => "Martina's Geburtstag", 
* 	"2004-07-18" => array("Andy's Geburtstag", "noch ein Geb.")
* );
* $oCal->addDate($dates_to_highlight); // params: $aDate 
* </code></pre>
*
* @access   public
* @param	array	$aDate		Termin-Datensatz/Datensaetze
* @return   string	HTML Kalender
*/
	function addDate($aDate) {
		
		if (!$aDate) return; // check vars
		if (!is_array($aDate)) { $aDate = array($aDate => ''); }
		
		// add
		foreach ($aDate as $date => $title) {
			if (is_array($title)) {	// wenn mehrere Termine an diesem Datum
				foreach ($title as $k => $t) {
					$this->_dates[$date][] = $t;
				}
			} else {	// wenn nur ein Termin an diesem Datum
				$this->_dates[$date][] = $title;
			}
		}
		
		return $this->_dates;
	}

/**
* ermittelt alle Termine des aktuellen Users des aktuellen Monats 
* und gibt die ermittelten Informationen als Array an den minicalendar weiter.
*
* Beispiel: 
* <pre><code> 
* $oCal->addMyDates($oDb); // params: &$oDb 
* </code></pre>
*
* @access   public
* @param	object	$oDb		Datenbank-Objekt (by reference)
* @param	string	$month		UNIX-timestamp oder ISO-Date (optional - default: aktueller Monat)
* @return   void
* @see		_add_day()
*/
	function addMyDates(&$oDb) {
		global $aENV, $Userdata;
		
		require_once($aENV['path']['global_service']['unix']."class.team.php");
		require_once($aENV['path']['global_service']['unix']."class.user.php");

		$oTeam =& new Team($oDb);
		
		// check vars
		if (!isset($aENV['table']['cal_event']) || empty($aENV['table']['cal_event'])) die("class.calendar.php -> ERROR: aENV['table']['cal_event'] nicht definiert!");
		// get current month & year
		$month	= (!isset($_GET['calMonth']) || empty($_GET['calMonth'])) ? date('n') : $_GET['calMonth'];
		$year	= (!isset($_GET['calYear']) || empty($_GET['calYear'])) ? date('Y') : $_GET['calYear'];
		if (isset($_GET['date_ts']) && !empty($_GET['date_ts'])) {
			$month	= date('n', $_GET['date_ts']);
			$year	= date('Y', $_GET['date_ts']);
		}
		// SQL
		$oDb->query("SELECT DISTINCT `id`,`subject`,`event_start`,`event_end`,`flag_allday` 
					FROM `".$aENV['table']['cal_event']."` 
					WHERE (MONTH(event_start) = '".$month."' AND YEAR(event_start) = '".$year."') 
						OR (MONTH(event_end) < '".$month."' AND YEAR(event_end) > '".$year."') 
					ORDER BY `flag_allday` DESC, `event_start` ASC, `subject` ASC");
		$buffer = array();
		while ($tmp = $oDb->fetch_array()) {
			// nur Termine des aktuellen Nutzers anzeigen || --------------------aus TACEA holen!!!-------
			#OLD:if (!is_array($aParticipants) || !in_array($Userdata['id'], $aParticipants)) continue;
			$uright = $this->_oTACEA->getRightForID($tmp['id'],'cal');
			if(!is_array($uright[$Userdata['id']])) {
				continue;
			}
			// vars formatieren
			list($date_start, $time_start)	= explode(' ', $tmp['event_start']);
			$time_start						= substr($time_start, 0, 5);
			list($date_end, $time_end)		= explode(' ', $tmp['event_end']);
			$time_end						= substr($time_end, 0, 5);
			$subject						= $tmp['subject'];
			if ($tmp['flag_allday'] != '1') {
				$subject					.= ' ('.$time_start.')';
			}
			// vars in array uebergeben
			$this->addDate(array($date_start => $subject)); // params: $aDate
			
			// mehrtaegige events uebergeben
			while ($date_start != $date_end) {
				$date_start = $this->_add_day($date_start);
				$this->addDate(array($date_start => $tmp['subject'])); // params: $aDate
			}
		}
	}
/**
* Hilfsfunktion: addiert einen Tag zu einem ISO-Datum.
*
* @access	private
* @param	string	Iso-Datum
* @return	string	Iso-Datum plus einen Tag
* @see		addMyDates()
*/
	function _add_day($iso_date) {
		$ts_year = substr($iso_date, 0, 4);
		$ts_month = substr($iso_date, 5, 2);
		$ts_day = substr($iso_date, 8, 2);
		$timestamp_tomorrow = mktime(0, 0, 0, $ts_month, ($ts_day + 1), $ts_year);
		return date("Y-m-d", $timestamp_tomorrow);
	}

/**
* baut ein kleines Kalender-Modul (z.B. fuer die rechte Spalte ...). 
* Es werden nur die Tage des gewaehlten Monats angezeigt. Wenn man 
* sich im aktuellen Monat befindet, wird der aktuelle Tag gekennzeichnet.
* Ausserdem koennen mit der Methode "addDate()" Tage des aktuellen Monats gehighlightet werden.
* Wenn als 3. Parameter "true" uebergeben wird, werden die Kalenderwochen als erste TD jeder TR angezeigt.
*
* Beispiel: 
* <pre><code> 
* echo $oCal->getMinicalendarHtml(); // params: [$sHref=''][,$sButAddGetVars=''][,$bWithCw=false][,$sCwHref=''] 
* </code></pre>
*
* @access   public
* @param	string	$sHref			(optional: um jeden Tag zu verlinken - default: '')
* @param	string	$sButAddGetVars	(optional: zusaetzliche Parameter, die beim Blaettern nicht verloren gehen duerfen - default: '')
* @param	boolean	$bWithCw		(optional: um die Kalenderwoche anzuzeigen - default: false)
* @param	string	$sCwHref		(optional: wenn die Kalenderwoche angezeigt wird, kann man einen Link zur Wochenansicht uebergeben - default: '')
* @return   string	HTML Kalender
* @see		_buildMonthCalendarArray()
*/
	function getMinicalendarHtml($sHref='', $sButAddGetVars='', $bWithCw=false, $sCwHref='',$sMonthHref='') {
	// Create HTML-table header
		
		if(!empty($sMonthHref)) {
			$iMonth = array_search($this->_monthcalendar['header']['monthname'],$this->_monthnames[$this->_lang]);
			$iTS = mktime(0,0,0,$iMonth,1,$this->_monthcalendar['header']['year']);
			$sMonthname = '<a href="'.str_replace("{TS}", $iTS, $sMonthHref).'">'.$this->_monthcalendar['header']['monthname'].'</a>';
			$sYearname = '<a href="'.str_replace("{TS}", $iTS, $sMonthHref).'">'.$this->_monthcalendar['header']['year'].'</a>';
		}
		else {
			$sMonthname = $this->_monthcalendar['header']['monthname'];
			$sYearname = $this->_monthcalendar['header']['year'];
		}
		$header_search	= array("{MONTHNAME}", "{YEAR}", "{PREV_LINK}", "{TODAY_LINK}", "{NEXT_LINK}", "{PREV_TITLE}", "{TODAY_TITLE}", "{NEXT_TITLE}");
		$header_replace	= array(
			$sMonthname, 
			$sYearname, 
			$this->_monthcalendar['header']['prev_link'].$sButAddGetVars, 
			$this->_monthcalendar['header']['today_link'].$sButAddGetVars, 
			$this->_monthcalendar['header']['next_link'].$sButAddGetVars, 
			$this->_monthcalendar['header']['prev_title'], 
			$this->_monthcalendar['header']['today_title'], 
			$this->_monthcalendar['header']['next_title']
		);
		$content  = str_replace($header_search, $header_replace, $this->_html['header'])."\n";
			
	// Create HTML-table calendar
		$content .= $this->_html['table_start'].$this->_html['tr_start']."\n";
		// calendar week
		if ($bWithCw == true) {
			$cw_search = array("{CLASS}", "{TITLE}", "{TD}");
			$cw_replace = array('calWeek', $this->_calendarweek[$this->_lang], $this->_calendarweek[$this->_lang]);
			$content .= str_replace($cw_search, $cw_replace, $this->_html['td']);
			//$content .= str_replace("{TH}", $this->_calendarweek[$this->_lang], $this->_html['th']);
		}
		// daynames
		foreach ($this->_monthcalendar['dayname'] as $dayname) {
			$content .= str_replace("{TH}", $dayname, $this->_html['th']);
		}
		$content .= $this->_html['tr_end']."\n";
		// weeks
		foreach ($this->_monthcalendar['day'] as $row => $day) {
			// start row
			$content .= $this->_html['tr_start']."\n";
			// calendar week
			if ($bWithCw == true) {
				list($cw_y, $cw_m, $cw_d) = explode('-', $day[$row]['isodate']);
				$cw_ts = mktime(0, 0, 0, $cw_m, $cw_d, $cw_y);
				$cw	= date('W', $cw_ts);
				$title = $this->_calendarweek[$this->_lang].' '.$cw;
				// wenn ein Link uebergeben wurde
				if ($sCwHref != '' && !empty($cw_ts)) {
					$cw	= '<a href="'.str_replace("{TS}", $cw_ts, $sCwHref).'">'.$cw.'</a>';
				}
				$cw_search = array("{CLASS}", "{TITLE}", "{TD}");
				$cw_replace = array('calWeek', $title, $cw);
				$content .= str_replace($cw_search, $cw_replace, $this->_html['td']);
			}
			// days
			foreach ($day as $key => $val) {
				if (count($this->_dates) > 0 && array_key_exists($val['isodate'], $this->_dates)) {
					$val['class'] = "calHighlight";
					$val['title'] = implode(", ", $this->_dates[$val['isodate']]);
				}
				else {
					$val['title'] = '';
				}
				// highlight today
				$today_highlight = ($val['isodate'] == date("Y-m-d")) ? '" id="calToday' : '';
				
				// TD
				$TD = substr($val['isodate'], 8, 2);
				if (!empty($sHref)) {
					$ts_year = substr($val['isodate'], 0, 4);
					$ts_month = substr($val['isodate'], 5, 2);
					$ts_day = substr($val['isodate'], 8, 2);
					$date_ts = mktime(0, 0, 0, $ts_month, $ts_day, $ts_year);
					$link_search	= array("{Y}", "{M}", "{D}", "{TS}");
					$link_replace	= array($ts_year, $ts_month, $ts_day, $date_ts);
					$TD = '<a href="'.str_replace($link_search, $link_replace, $sHref).'" class="'.$val['class'].'">'.$TD.'</a>';
				}
				// replace
				$content_search		= array("{CLASS}", "{TITLE}", "{TD}");
				$content_replace	= array($val['class'].$today_highlight, $val['title'], $TD);
				$content .= str_replace($content_search, $content_replace, $this->_html['td']);
			}
			// end row
			$content .= $this->_html['tr_end']."\n";
		}
		// end table
		$content .= $this->_html['table_end']."\n";
	// return calendar html
		return $content;
	} // END function

/**
* baut ein kleines Kalender-Modul fuer Handheld-Ausgabe. 
* Es werden nur die Tage des gewaehlten Monats angezeigt. Wenn man 
* sich im aktuellen Monat befindet, wird der aktuelle Tag gekennzeichnet.
* Ausserdem koennen mit der Methode "addDate()" Tage des aktuellen Monats gehighlightet werden.
* Wenn als 3. Parameter "true" uebergeben wird, werden die Kalenderwochen als erste TD jeder TR angezeigt.
*
* Beispiel: 
* <pre><code> 
* echo $oCal->getMinicalendarWap(); // params: [$sHref=''][,$sButAddGetVars=''][,$bWithCw=false][,$sCwHref=''] 
* </code></pre>
*
* @access   public
* @param	string	$sHref			(optional: um jeden Tag zu verlinken - default: '')
* @param	string	$sButAddGetVars	(optional: zusaetzliche Parameter, die beim Blaettern nicht verloren gehen duerfen - default: '')
* @param	string	$bWithCw		(optional: um die Kalenderwoche anzuzeigen - default: false)
* @param	string	$sCwHref		(optional: wenn die Kalenderwoche angezeigt wird, kann man einen Link zur Wochenansicht uebergeben - default: '')
* @return   string	HTML Kalender
* @see		_buildMonthCalendarArray()
*/
	function getMinicalendarWap($sHref='', $sButAddGetVars='', $bWithCw=false, $sCwHref='') {
	// Create HTML-table header

		$header_search	= array('{NEW}',"{MONTHNAME}", "{YEAR}", "{PREV_LINK}", "{TODAY_LINK}", "{NEXT_LINK}", "{PREV_TITLE}", "{TODAY_TITLE}", "{NEXT_TITLE}");
		$header_replace	= array(
			'<a href="mbl_cal_detail.php">'.$this->_monthcalendar['header']['new'].'</a>',
			$this->_monthcalendar['header']['monthname'], 
			$this->_monthcalendar['header']['year'], 
			$this->_monthcalendar['header']['prev_link'].$sButAddGetVars, 
			$this->_monthcalendar['header']['today_link'].$sButAddGetVars, 
			$this->_monthcalendar['header']['next_link'].$sButAddGetVars, 
			$this->_monthcalendar['header']['prev_title'], 
			$this->_monthcalendar['header']['today_title'], 
			$this->_monthcalendar['header']['next_title']
		);
		
	// Create HTML-table calendar
		$content = '<table cellpadding="0" cellspacing="0" class="calTable">'."\n<tr>";
		$sHeader = '<td class="calTableHead" colspan="8">&nbsp;<b>{MONTHNAME} {YEAR} | {NEW}</b></td></tr>'."\n<tr>\n";
		$content  .= str_replace($header_search, $header_replace, $sHeader)."\n";
		// calendar week
		if ($bWithCw == true) {
			$content .= '<td class="calWeek"></td>';
		}
		// daynames
		foreach ($this->_monthcalendar['dayname'] as $dayname) {
			if (strlen($dayname) > 2) {
				$dayname = substr($dayname, 0, 2);
			}
			$content .= str_replace("{TH}", $dayname, '<td class="calDaynames">{TH}</td>');
		}
		$content .= "</tr>\n";
		// weeks
		foreach ($this->_monthcalendar['day'] as $row => $day) {
			// start row
			$content .= "<tr>";
			// calendar week
			if ($bWithCw == true) {
				list($cw_y, $cw_m, $cw_d) = explode('-', $day[$row]['isodate']);
				$cw_ts = mktime(0, 0, 0, $cw_m, $cw_d, $cw_y);
				
				$cw	= date('W', $cw_ts);
				// wenn ein Link uebergeben wurde
				if ($sCwHref != '' && !empty($cw_ts)) {
					$cw	= '<a href="'.str_replace("{TS}", $cw_ts, $sCwHref).'">'.$cw.'</a>';
				}
				$content .= '<td class="calWeek">'.$cw.'</td>';
			}
			// days
			foreach ($day as $key => $val) {
				if (count($this->_dates) > 0 && array_key_exists($val['isodate'], $this->_dates)) {
					$val['class'] = "calHighlight";
					$val['title'] = implode(", ", $this->_dates[$val['isodate']]);
				}
				// highlight today
				$today_highlight = ($val['isodate'] == date("Y-m-d")) ? 'Td" id="calToday' : '';
				
				// TD
				$TD = substr($val['isodate'], 8, 2);
				if (!empty($sHref)) {
					$ts_year = substr($val['isodate'], 0, 4);
					$ts_month = substr($val['isodate'], 5, 2);
					$ts_day = substr($val['isodate'], 8, 2);
					$date_ts = mktime(0, 0, 0, $ts_month, $ts_day, $ts_year);
					$link_search	= array("{Y}", "{M}", "{D}", "{TS}");
					$link_replace	= array($ts_year, $ts_month, $ts_day, $date_ts);
					$TD = '<a href="'.str_replace($link_search, $link_replace, $sHref).'" class="'.$val['class'].'">'.$TD.'</a>';
				}
				// replace
				$content_search		= array("{CLASS}", "{TITLE}", "{TD}");
				$content_replace	= array($val['class'].$today_highlight, $val['title'], $TD);
				$content .= str_replace($content_search, $content_replace, '<td class="{CLASS}Td">{TD}</td>');
			}
			// end row
			$content .= "</tr>\n";
		}
		// end table
		$sFooter = "</table>\n<br />\n".'<a href="{PREV_LINK}" title="{PREV_TITLE}" class="calButton">&lt;</a><a href="{TODAY_LINK}" title="{TODAY_TITLE}" class="calButton">{TODAY_TITLE}</a><a href="{NEXT_LINK}" title="{NEXT_TITLE}" class="calButton">&gt;</a>'."\n";
		$content .= str_replace($header_search, $header_replace, $sFooter);
	// return calendar html
		return $content;
	} // END function

#----------------------------------------------------------------------------- MONTH CALENDAR

/**
* baut ein (grosses) Monats-Kalender-Modul. 
* Wenn man sich im aktuellen Monat befindet, wird der aktuelle Tag gekennzeichnet.
* Ausserdem wird jeden Tag die Methode "getDayEvents()" aufgerufen und die entsprechenden Termine angezeigt 
* (Die Termine koennen mit der Methode "setFilter()" gefiltert werden). 
* Wenn als 2. Parameter "true" uebergeben wird, werden die Kalenderwochen als erste TD jeder TR angezeigt.
* Der 3. Parameter wird benoetigt, um einen Link zur Edit-Seite des Termins zu bauen.
* Der 4. Parameter wird benoetigt, um einen Link zur Day-View-Seite des Kalenders zu bauen.
*
* Beispiel: 
* <pre><code> 
* echo $oCal->getMonthcalendarHtml(); // params: $oDb[,$bWithCw=false][,$sEditPage=''][,$sDayViewPage=''][,$sWeekViewPage=''][,$bWithNewButton=true] 
* </code></pre>
*
* @access   public
* @return   string	HTML Kalender
* @param	boolean	$bWithCw		(optional: um die Kalenderwoche anzuzeigen - default: false)
* @param	string	$sEditPage		(optional: Link zur Edit-Seite - default: '')
* @param	string	$sDayViewPage	(optional: Link zur Day-View-Seite - default: '')
* @param	string	$sWeekViewPage	(optional: Link zur Week-View-Seite - default: '')
* @param	boolean	$bWithNewButton	(optional: um den New-Button anzuzeigen - default: true)
* @see		_buildMonthCalendarArray()
*/
	function getMonthcalendarHtml(&$oDb, $bWithCw=false, $sEditPage='', $sDayViewPage='', $sWeekViewPage='', $bWithNewButton=true) {
	// vars
		global $aMSG, $aENV;
		$aLabels = Tools::getLabels($oDb,$aENV,'cal','business');
		// Links/Get-Vars filtern
		// edit-page
		if (!empty($sEditPage)) { // aus link ggf. parameter rausfiltern
			$sEditPage = preg_replace('/(\?|\&)date_ts=([0-9])*/', '', $sEditPage); // date_ts wird individuell angehaengt
			$sEditPage = preg_replace('/(\?|\&)id=([0-9])*/', '', $sEditPage); // id darf nicht vorkommen!
		}
		if (!strpos($sEditPage, '?')) {
			$sEditPage .= '?';
		}
		// day-view-page
		$sDayViewPage = preg_replace('/(\?|\&)(date_ts|id)*=([0-9])*/', '', $sDayViewPage);
		$sDayViewPage = preg_replace('/(\?|\&)view=([a-z])*/', '', $sDayViewPage); // view darf nicht vorkommen!
		if (empty($sDayViewPage)) {
			$q = (!strpos($sDayViewPage, '?')) ? '?' : '&';
			$sDayViewPage = $aENV['PHP_SELF'].$q.'view=day';
		}
		if (!strpos($sDayViewPage, '?')) {
			$sDayViewPage .= '?';
		}
		// week-view-page
		$sWeekViewPage = preg_replace('/(\?|\&)(date_ts|id)*=([0-9])*/', '', $sWeekViewPage);
		$sWeekViewPage = preg_replace('/(\?|\&)view=([a-z])*/', '', $sWeekViewPage); // view darf nicht vorkommen!
		if (empty($sWeekViewPage)) {
			$q = (!strpos($sWeekViewPage, '?')) ? '?' : '&';
			$sWeekViewPage = $aENV['PHP_SELF'].$q.'view=week&date_ts={TS}';
		}
		if (!strpos($sWeekViewPage, '?')) {
			$sWeekViewPage .= '?';
		}
		
	// Create HTML-table header
		$content  = "\n";
		
	// Create HTML-table calendar
		$content .= $this->_html['table_start'].$this->_html['tr_start']."\n";
		// calendar week
		if ($bWithCw == true) {
			$cw_search = array("{CLASS}", "{TITLE}", "{TD}");
			$cw_replace = array('calWeek', $this->_calendarweek[$this->_lang], '');
			$content .= str_replace($cw_search, $cw_replace, $this->_html['td']);
			#$content .= str_replace("{TH}", $this->_cw[$this->_lang], $this->_html['th']);
		}
		// daynames
		foreach ($this->_monthcalendar['dayname'] as $dayname) {
			$content .= str_replace("{TH}", $dayname, $this->_html['th']);
		}
		// weeks
		foreach ($this->_monthcalendar['day'] as $row => $day) {
			// start row
			$content .= $this->_html['tr_start']."\n";
			// calendar week
			if ($bWithCw == true) {
				list($cw_y, $cw_m, $cw_d) = explode('-', $day[$row]['isodate']);
				$cw_ts = mktime(0, 0, 0, $cw_m, $cw_d, $cw_y);
				$cw	= date('W', $cw_ts);
				$cw_title = $this->_calendarweek[$this->_lang].' '.$cw;
				// wenn ein Link uebergeben wurde
				if ($sWeekViewPage != '' && !empty($cw_ts)) {
					$cw	= '<a href="'.str_replace("{TS}", $cw_ts, $sWeekViewPage).'">'.$cw.'</a>';
				}
				$cw_search = array("{CLASS}", "{TITLE}", "{TD}");
				$cw_replace = array('calWeek', $cw_title, $cw);
				$content .= str_replace($cw_search, $cw_replace, $this->_html['td']);
			}
			// days
			foreach ($day as $key => $val) {
				if (count($this->_dates) > 0 && array_key_exists($val['isodate'], $this->_dates)) {
					$val['class'] = "calHighlight";
					$val['title'] = implode(", ", $this->_dates[$val['isodate']]);
				}
				// highlight today
				if ($val['isodate'] == date("Y-m-d")) {
					$val['class'] .= '" id="calToday';
				}
			// TD
				// vars
				$ts_year = substr($val['isodate'], 0, 4);
				$ts_month = substr($val['isodate'], 5, 2);
				$ts_day = substr($val['isodate'], 8, 2);
				$date_ts = mktime(0, 0, 0, $ts_month, $ts_day, $ts_year);
				// Datum + NEW-Button
				$TD = '<span class="text">';
				if ($bWithNewButton) { // NEW-Button
					$TD .= '<a href="'.$sEditPage.'&date_ts='.$date_ts.'" title="'.$aMSG['mode']['new'][$this->_lang].'">';
					$TD .= '<img src="'.$aENV['path']['pix']['http'].'btn_add_small.gif" alt="'.$aMSG['std']['new'][$this->_lang].'" class="btnsmall"></a>';
				}
				if (!empty($sEditPage)) { $TD .= '<a href="'.$sDayViewPage.'&date_ts='.$date_ts.'" title="'.$aMSG['cal']['view_day'][$this->_lang].'" class="calMonthDay">'; }
				$TD .= ''.$ts_day.'';// Datum
				if ($val['class'] == 'calNotthismonth') {
					$TD .= '.'.$ts_month.'.';
					if ($ts_month == '12' && $row == 0) { $TD .= ''.$ts_year; }
					if ($ts_month == '01' && $row > 3) { $TD .= ''.$ts_year; }
				}
				if (!empty($sEditPage)) { $TD .= '</a>'; }
				$TD .= '<br></span>';
				// Termine Divs
				$aEvents = $this->getDayEvents($oDb, $date_ts); // params: &$oDb[,$day=NULL][,$media=''] 
				if (count($aEvents) > 0) {
					foreach ($aEvents as $k => $event) {
						if ($event['flag_allday']==1) { $classDay="allday"; } else { $classDay="notallday"; }
						$TD .= '<div class="'.$classDay.'">';
						if($event['label'] != 'business' || empty($event['labelid'])) {
							$event_class = 'eventBoxWeek'.$event['label'];
							$TD .= '<div class="'.$event_class.'"><span class="text"><small>';
						} else {
							if($aLabels[$event['labelid']]['color'] != '') $bgcolor = "background-color: ".$aLabels[$event['labelid']]['color'].';'; else $bgcolor ='';	
							$TD .= '<div style="'.$bgcolor.' border-left:1px solid #f7f7f7; border-bottom:1px solid #ffffff; padding:2px;"><span class="text"><small>';
						}
						if (!empty($sEditPage)) { $TD .= '<a href="'.$sEditPage.'&id='.$event['id'].'&date_ts='.$date_ts.'" title="'.$aMSG['mode']['edit'][$this->_lang].'">'; }
						$TD .= $event['icon'];
						if($aENV['flag_confirmed'] && $event['flag_confirmed'] == 0) {
							//TODO icon?
				 			$TD .= '[tbc] '; 
						}
						$TD .= $event['subject'];
						if (!empty($sEditPage)) { $TD .= '</a>'; }
						$TD .= ''.$event['sign_multidays'].'<br></small></span></div>';
						$TD .= '</div>';
					}
				}
				$content_search		= array("{CLASS}", "{TITLE}", "{TD}");
				$content_replace	= array($val['class'], $val['title'], $TD);
				$content .= str_replace($content_search, $content_replace, $this->_html['td']);
			}
			// end row
			$content .= $this->_html['tr_end']."\n";
		}
		// end table
		$content .= $this->_html['table_end']."\n";
	// return calendar html
		return $content;
	} // END function

/**
* Hilfsfunktion: baut ein Monats-Kalender-Array.
*
* @access   private
* @return   void
* @see		getMinicalendarHtml()
* @see		getMonthcalendarHtml()
*/
	function _buildMonthCalendarArray() {
	// vars
		global $aMSG, $aENV;
		$buffer	= array();
		// current month & year
		$month	= (!isset($_GET['calMonth']) || empty($_GET['calMonth'])) ? date('n') : $_GET['calMonth'];
		$year	= (!isset($_GET['calYear']) || empty($_GET['calYear'])) ? date('Y') : $_GET['calYear'];
		if (isset($_GET['date_ts']) && !empty($_GET['date_ts'])) {
			$month	= date('n', $_GET['date_ts']);
			$year	= date('Y', $_GET['date_ts']);
		}
		$today_link = $_SERVER['PHP_SELF'].'?calMonth='.date('n').'&amp;calYear='.date('Y');
		$count_days_this_month = date('t', mktime(0, 0, 0, $month, 1, $year));
		$month_name = $this->_monthnames[$this->_lang][$month];
		// previous month & year
		$previousyear = $year;
		$previousmonth = $month - 1;
		if ($previousmonth < 1) {
			$previousmonth = 12;
			$previousyear = $year - 1;
		}
		$prev_link = $_SERVER['PHP_SELF'].'?calMonth='.$previousmonth.'&amp;calYear='.$previousyear;
		$prev_monthname_en = $this->_monthnames['en'][$previousmonth];
		$count_days_prev_month = date("t", strtotime("01 $prev_monthname_en $previousyear"));
		$prev_title = $this->_monthnames[$this->_lang][$previousmonth].' '.$previousyear;
		// next month & year
		$nextyear = $year;
		$nextmonth = $month + 1;
		if ($nextmonth > 12) {
			$nextmonth = 1;
			$nextyear = $year + 1;	
		}
		$next_link = $_SERVER['PHP_SELF'].'?calMonth='.$nextmonth.'&amp;calYear='.$nextyear;
		$next_title = $this->_monthnames[$this->_lang][$nextmonth].' '.$nextyear;
		// first day of current month? (0 = Sunday, 6 = Saturday)
		$first_day = date('w', mktime(0, 0, 0, $month, 1, $year));
		$first_day = $first_day - 1; // since week starts from monday, subtract one
		if ($first_day < 0) { $first_day = 6; }
		// how many rows are in current month
		$count_calendar_rows = (int) ceil(($first_day + 1 + $count_days_this_month) / 7);
		
	// collect header
		$buffer['header'] = array();
		$buffer['header']['new']		= $aMSG['std']['new'][$this->_lang];
		$buffer['header']['monthname']	= $month_name;
		$buffer['header']['year']		= $year;
		$buffer['header']['prev_link']	= $prev_link;
		$buffer['header']['today_link']	= $today_link;
		$buffer['header']['next_link']	= $next_link;
		$buffer['header']['prev_title']	= $prev_title;
		$buffer['header']['today_title']= $aMSG['btn']['today'][$this->_lang];
		$buffer['header']['next_title']	= $next_title;
		
	// collect daynames
		$buffer['dayname'] = array();
		foreach ($this->_daynames[$this->_lang] as $dayname) {
			$buffer['dayname'][]	= $dayname;
		}
		
	// collect days (in rows)
		$row = 0;
		$buffer['day'] = array();
		// days
		for ($i = 0; $i < ($count_calendar_rows * 7); $i++) {
			// zeilenwechsel
			if (($i % 7) == 0) { $row ++; }
			// iso_date-elemente / classes
			$day = (1 + $i - $first_day);
			if ($i < $first_day) {
				// Tag ist in VORIGEM Monat
				$iso_day = $count_days_prev_month + $day;
				$iso_month = $previousmonth;
				$iso_year = ($previousmonth == '12') ? ($year - 1) : $year;
				$class = "calNotthismonth";
			} elseif ($i > ($count_days_this_month + $first_day - 1)) {
				// Tag ist in NAECHSTEM Monat
				$iso_day = $day - $count_days_this_month;
				$iso_month = $nextmonth;
				$iso_year = ($nextmonth == '01') ? ($year + 1) : $year;
				$class = "calNotthismonth";
			} else {
				// Tag ist in DIESEM Monat
				$iso_day = $day;
				$iso_month = $month;
				$iso_year = $year;
				$class = "calWeekday";							// Wochentag
				if (($i % 7) == 5) { $class = "calSaturday"; }	// Samstag
				if (($i % 7) == 6) { $class = "calSunday"; }	// Sonntag
			}
			// leading zero
			if (strlen($iso_day) == 1) { $iso_day = '0'.$iso_day; }
			if (strlen($iso_month) == 1) { $iso_month = '0'.$iso_month; }
			// highlight specified date(s)
			$iso_date = $iso_year.'-'.$iso_month.'-'.$iso_day;
			if (count($this->_dates) > 0 && array_key_exists($iso_date, $this->_dates)) {
				$class = "calHighlight";
				$title = implode(", ", $this->_dates[$iso_date]);
			}
			// collect
			$buffer['day'][$row-1][] = array(
				'class'			=> $class,
				'isodate'		=> $iso_date
			);
		}
	// calendar array zwischenspeichern
		$this->_monthcalendar = $buffer;
	} // END function

	/**
	 * Diese Methode gibt alle Urlaubs und Kranktage aller im CMS vorhandenen User mit berücksichtigung des Office zurück.
	 * @param int $year
	 * @param object $oDb
	 * @param array $aUsername
	 * @return array
	 */

	function getUserEvents($year,&$oDb,&$aUsername) {
		
		$oUser =& new user($oDb);
		
		$t1 = 'cal_event';
		$t2 = 'sys_team_access_right';
		
		$aOffices = $oUser->getAllOffices(true);
		$aFreedays = array();
		
		// TODO: ermitteln gegen die adm timesheets ob der user an einem krankheits oder holiday tag gearbeitet hat. die stunden abziehen bzw. den tag.
		for($i=0;list($id,$name) = each($aOffices);$i++) {
			$sql = "SELECT UNIX_TIMESTAMP(event_start) as `event_start` FROM $t1 WHERE office='$id' AND label='holiday'";
			$oDb->query($sql);
			if ($oDb->num_rows() > 0) {
				for($j=0;$row = $oDb->fetch_object();$j++) {
					$aFreedays[$id][] = $row->event_start;
				}
			}
			else { $aFreedays[$id][] = ""; }
		}
		
		foreach($aUsername as $uid => $name) {
			$sql = "SELECT `$t1`.`id`,UNIX_TIMESTAMP(`$t1`.`event_end`) as `end`, UNIX_TIMESTAMP(`$t1`.`event_start`) as `start`,`$t1`.`flag_allday`,`$t1`.`label` 
					FROM `$t1` INNER JOIN `$t2` 
					ON `$t1`.`id` = `$t2`.`fk_id` 
					WHERE (`$t1`.`label` = 'sick' OR `$t1`.`label` = 'leave' OR `$t1`.`label` = 'lieu') 
						AND `$t2`.`flag_type` = 'uid' 
						AND `$t2`.`flag_tree` = 'cal' 
						AND `$t2`.`flag_show` = '1'
						AND YEAR(`$t1`.`event_start`) = $year 
						AND `$t2`.`ug_id` = '$uid'";
			$oDb->query($sql);
			$aEvents[$uid]['sick']['day'] = 0;
			$aEvents[$uid]['leave']['day'] = 0;
			$aEvents[$uid]['lieu']['day'] = 0;
			$aEvents[$uid]['sick']['hour'] = 0;
			$aEvents[$uid]['leave']['hour'] = 0;
			$aEvents[$uid]['lieu']['hour'] = 0;
			
			$aUserdata = $oUser->getUserdata($uid);
			$iOfficeid = $aUserdata['office_id'];
			
			for ($i=0;$row = $oDb->fetch_object();$i++) {
				$sub = 0;
				$ts = 0;
				if ($row->flag_allday == 1) {
					for ($j=$row->start; $j<=$row->end; $j=$j+86400) {
						if (!in_array($j,$aFreedays[$iOfficeid]) && date('D',$j) != 'Sun' && date('D',$j) != 'Sat') {
							$ts++;	
						}
					}
					$aEvents[$uid][$row->label]['day'] += $ts;
				}
				else {
				// DEBUG ausgaben sind deaktiviert, aber sollten noch vorhanden sein, wenn fehler auftreten sollten.
//					echo "<!-- day: ".date('d.m.Y',$row->start)."-->\n";

					$ts = ($row->end - $row->start)/3600;

					// durchläuft jeden Tag des Events und filtert nach Feier/Wochenendtagen
					for ($n=$row->start; $n<=$row->end; $n=$n+86400) {
						if (in_array($n,$aFreedays[$iOfficeid]) 
						|| date('D',$n) == 'Sun' 
						|| date('D',$n) == 'Sat') {
							$ts -= 24;
						}
					} 
					
//					echo "<!-- ts: $ts -->\n\n";

					$aEvents[$uid][$row->label]['hour'] += $ts;
				}
			}
		}
		return $aEvents;
	}
} // END class

?>