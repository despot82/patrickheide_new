<?php
/**
* Diese Klasse kapselt die Datenbankzugriffe im ADM/Projects Bereich. 
*
* @access   public
* @package  module
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2006-03-17
*/
class AdmProjectEA {
	
	/**
	 * @access	private
	 * @var		object	Datenbank-Objekt
	 */
	var $oDb = null;
	
	/**
	 * @access	private
	 * @var		array	globales Inform Environment
	 */
	var $aENV = array();
	
	/**
	 * @access	private
	 * @var		int		Anzahl der bei einer Abfrage ermittelten Datensaetze
	 */
	var $entries = 0;
	
	/**
	 * Konstruktor
	 * 
	 * @access	public
	 * @param	string	$mode 	[optional, default: '']
	 */
	function AdmProjectEA(&$oDb, &$aENV) {
		$this->oDb	=& $oDb;
		$this->aENV	=& $aENV;
	}

// TEAM ////////////////////////////////////////////////////////////////////////

	/**
	 * Ermittle die aktuelle Rate des uebergebenen Users.
	 * 
	 * @access	public
	 * @param	int		$user_id
	 * @param	string	$currentValDate	[optional, default: '']
	 * @return	array[key] = value
	 */
	function getCurrentRate($user_id, $currentValDate='') {
		// vars
		if (empty($currentValDate)) $currentValDate = date("Y-m-d");
		if (empty($user_id)) return;
		$buffer = array();
		// select
		$sql = "SELECT `id`,`rate_euro`,`rate_int_euro`,`rate_pound`,`rate_int_pound`,`valid_from` 
				FROM `adm_employee_rate` 
				WHERE (('".$currentValDate."' BETWEEN `valid_from` AND `valid_to`) 
						OR ('".$currentValDate."' >= `valid_from` AND `valid_to` = '0000-00-00'))
					AND `user_id` = ".$user_id." 
				GROUP BY `id`,`rate_euro`,`rate_int_euro`,`rate_pound`,`rate_int_pound` 
				ORDER BY `valid_from` DESC";
		$this->oDb->query($sql);
		$this->entries = $this->oDb->num_rows();
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer['euro']		= $tmp['rate_euro'];
			$buffer['euro_int']	= $tmp['rate_int_euro'];
			$buffer['pound']	= $tmp['rate_pound'];
			$buffer['pound_int']= $tmp['rate_int_pound'];
		}
		// output
		return $buffer;
	}

	/**
	 * Ermittle alle Raten des uebergebenen Users.
	 * 
	 * @access	public
	 * @param	int		$user_id
	 * @return	array[user_id][key] = value
	 */
	function getRates($user_id) {
		// vars
		if (empty($user_id)) return;
		$buffer = array();
		// select
		$sql = "SELECT `id`,`rate_euro`,`rate_int_euro`,`rate_pound`,`rate_int_pound`,`valid_from`,`valid_to` 
				FROM `adm_employee_rate` 
				WHERE `user_id` = '".$user_id."'  
				ORDER BY `valid_from` DESC";
		$this->oDb->query($sql);
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']]	= $tmp;
		}
		// output
		return $buffer;
	}

	/**
	 * Ermittle alle beteiligten User-Namen des uebergebenen Projekts.
	 * 
	 * @access	public
	 * @param	int		$project_id
	 * @param	string	$sConstraint	[optional, default: '']
	 * @return	array[user_id] = user_firstname
	 */
	function getProjectuser($project_id='', $sConstraint='') {
		// vars
		$buffer = array();
		// select
		$sql = "SELECT `u`.`id`,`u`.`firstname`,`u`.`surname` 
				FROM `".$this->aENV['table']['sys_user']."` `u`, `adm_timesheet` `t`
				WHERE `t`.`user_id` = `u`.`id` 
					AND `t`.`project_id` = '".$project_id."' 
					".$sConstraint."
				ORDER BY `u`.`id` ASC";
		$this->oDb->query($sql);
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']]	= $tmp['firstname'];
		}
		// output
		return $buffer;
	}

	/**
	 * Ermittle alle Employee-Namen (User mit mindestens einer Rate).
	 * 
	 * @access	public
	 * @param	string	$sConstraint	[optional, default: '']
	 * @return	array[user_id] = user_name
	 */
	function getEmployees($sConstraint='') {
		// vars
		$buffer = array();
		// select
		$sql = "SELECT DISTINCT `t`.`user_id`,`u`.`firstname`,`u`.`surname`
				FROM `adm_employee_rate` `t`, `".$this->aENV['table']['sys_user']."` `u`
				WHERE `t`.`user_id` = `u`.`id` 
					AND `u`.`flag_deleted` = '0' 
					AND `u`.`flag_da_service` = '0'
					".$sConstraint."
				ORDER BY `u`.`firstname` ASC, `u`.`surname` ASC";
		$this->oDb->query($sql);
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['user_id']]	= $tmp['firstname'].' '.$tmp['surname'];
		}
		// output
		return $buffer;
	}
	
	/**
	 * Ermittle alle deaktivierten Employee-Namen.
	 * 
	 * @access	public
	 * @return	array[user_id] = user_name
	 */
	function getDeletedEmployees() {
		// vars
		$buffer = array();
		// select
		$sql = "SELECT `id`,`firstname`,`surname`
				FROM `".$this->aENV['table']['sys_user']."`
				WHERE `flag_deleted` = '1' 
					AND `flag_da_service` = '0'
				ORDER BY `firstname` ASC, `surname` ASC";
		$this->oDb->query($sql);
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']]	= $tmp['firstname'].' '.$tmp['surname'];
		}
		// output
		return $buffer;
	}

// PROJECTS ////////////////////////////////////////////////////////////////////////

	/**
	 * Ermittle alle aktuellen Projekte + Kunden.
	 * 
	 * @access	public
	 * @return	array[project_id] = (string) project_details
	 */
	function getCurrentProjects() {
		// vars
		$buffer = array();
		// select
		$sql = "SELECT `p`.`client_id`,`p`.`id`,`p`.`projectname`,`p`.`prj_nr`,`c`.`client_nr`,`c`.`client_shortname` 
				FROM `adm_project` `p`, `adr_contact` `c` 
				WHERE `c`.`id` = `p`.`client_id` 
					AND `p`.`prj_status` IN (0,1)
				ORDER BY `c`.`client_shortname` ASC, `p`.`projectname` ASC";
		$this->oDb->query($sql);
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['client_shortname']." - ".$tmp['projectname']." [ ".$tmp['client_nr'].'.'.$tmp['prj_nr']." ]";
		}
		// output
		return $buffer;
	}

	/**
	 * Ermittle alle Details der uebergebenen Projekt-ID.
	 * 
	 * @access	public
	 * @param	int		$project_id
	 * @return	array[key] = value
	 */
	function getProjectdetails($project_id) {
		// vars
		if (empty($project_id)) return;
		
		$aPrj = array();
		
		// select
		$sql = "SELECT `projectname`,`prj_nr`,`office_id`,`client_id`,`contact_id`,
						`currency`,`tax`,`handling`,`discount`,`prj_status`,
						`prj_date0`,`prj_date1`,`prj_date2`,`prj_date3`,
						`po_number`,`comment`,`proposal_introduction`,`proposal_resume`
				FROM `adm_project` 
				WHERE `id` = '".$project_id."'";
		$this->oDb->query($sql);
		
		// output
		if ($this->oDb->num_rows() > 0) {
			$aPrj = $this->oDb->fetch_array();
		}
		else {
			$aPrj = array();
		}
		
		// Projects raddress_id ermitteln
		$sql = "SELECT `id` as `r_id` FROM `adr_address` WHERE `reference_id`='".$aPrj['client_id']."'";
		$this->oDb->query($sql);
		$aDet = $this->oDb->fetch_array();
		
		if (is_array($aDet) && is_array($aPrj)) {
			$aPrj = array_merge($aPrj,$aDet);
		}
		
		return $aPrj;
	}

	/**
	 * Ermittle alle Client-Details.
	 * bei Uebergabe einer Client-ID als einfaches assioziatives Array, sonst als mehrdimendionales Array (key=ID).
	 * 
	 * @access	public
	 * @param	int		$client_id
	 * @return	array[key] = value oder array[id][key] = value
	 */
	function getClientdetails($client_id=0) {
		// vars
		$buffer = array();
		// select
		if ($client_id > 0) {
			$sql = "SELECT `id`,`client_nr`,`client_shortname`,`company`, 
							concat(`title`,' ',`firstname`,' ',`surname`) AS `name` 
					FROM `adr_contact`
					WHERE `id` = '".$client_id."'";
			$this->oDb->query($sql);
			$buffer = $this->oDb->fetch_array();
		} else {
			$sql = "SELECT `id`,`client_nr`,`client_shortname`
					FROM `adr_contact`
					WHERE `client_nr` <> '000' 
						AND `client_nr` IS NOT NULL 
						AND `client_shortname` IS NOT NULL 
						AND `client_shortname` <> '' 
					ORDER BY `id` ASC";
			$this->oDb->query($sql);
			while ($tmp = $this->oDb->fetch_array()) {
				$buffer[$tmp['id']]['client_nr']		= $tmp['client_nr'];
				$buffer[$tmp['id']]['client_shortname']	= $tmp['client_shortname'];
			}
		}
		// output
		return $buffer;
	}
		
	/**
	 * Diese Methode ermittelt nur die Client_id mittels Project-ID
	 * @param	int		$projectid 
	 * @return	int		adm_project.client_id 
	 */
	function getClientIdByProjectId($projectid) {
		$sql = "SELECT `client_id` 
				FROM `adm_project` 
				WHERE `id` = '$projectid'";
		$this->oDb->query($sql);
		$buffer = $this->oDb->fetch_array();
		return $buffer['client_id'];
	}

	/**
	 * Aktuelle Projekte als verschachteltes Client/Projects DHTML-DropDown
	 * 
	 * @access	public
	 * @param	int		$sSelectedPrjId
	 * @param	int		$fieldnameext
	 * @return	HTML
	 */
	function getClientprojectsDropdown($sSelectedPrjId='',$fieldnameext='') {
		// init vars
		$buffer = ''; // HTML
		$eol	= "\n"; // Zeilenumbruch fuer bessere Lesbarkeit des HTML-Quelltextes
		$aProject = array();
		$aProject[0][0] = '--'; // wichtig -> erstes JS-array fuer KEIN Client/Prj ($aClient[0])!
		$aClient = array('0' => '--'); // erster Eintrag im Clients-DropDown
		$selected_client_id = 0;
		$aStatus = array('0' => array());
		// alle noetigen informationen fuer die verschachtelten dropdowns mit einer query ermitteln!
		$sql = "SELECT `p`.`client_id`,`p`.`id`,`p`.`projectname`,`p`.`prj_nr`,`p`.`prj_status`,`c`.`client_nr`,`c`.`client_shortname` 
				FROM `adm_project` `p`, `adr_contact` `c` 
				WHERE `c`.`id` = `p`.`client_id` 
					AND `p`.`prj_status` IN (0,1)
				ORDER BY `c`.`client_shortname` ASC, `p`.`projectname` ASC";
		$this->oDb->query($sql);
		// daten sammeln
		while ($tmp = $this->oDb->fetch_array()) {
			// verschachteltes Projekte-DropDown (key=client_id, val=Prj-array)
			$aProject[$tmp['client_id']][$tmp['id']] = addslashes($tmp['projectname'].' [ '.$tmp['client_nr'].'.'.$tmp['prj_nr'].' ]');
			$aStatus[$tmp['client_id']][$tmp['id']] = $tmp['prj_status'];
			
			// Clients-DropDown (key=id, val=name)
			$aClient[$tmp['client_id']] = $tmp['client_shortname'];
			// ggf. client_id aus project_id ermitteln
			if (!empty($sSelectedPrjId) && $sSelectedPrjId == $tmp['id']) {
				$selected_client_id = $tmp['client_id'];
			}
		}
	// HTML Dropdowns
		// Clients-DropDown (wird nicht gespeichert, sondern das dynamisch gefuellte Projekte-DropDown!)
		$buffer = '<select name="clients" id="clients" size="1" onChange="fillProjectSelection'.$fieldnameext.'(this, \'aData[project_id'.$fieldnameext.']\')">'.$eol;
		foreach ($aClient as $client_id => $client_name) {
			$sel = ($selected_client_id == $client_id) ? ' selected' : '';
			$buffer .= '<option value="'.$client_id.'"'.$sel.'>'.$client_name.'</option>'.$eol;
		}
		$buffer .= '</select>'.$eol;
		// Projekte-DropDown (wird wenn ein Client ausgewaehlt wird per JS aufgebaut; wird in DB gespeichert)
		$buffer .= '<select name="aData[project_id'.$fieldnameext.']">'.$eol;
		
		// wenn "edit"-mode: -> den DB-Eintrag selectieren, dazu das dazugehoerige DropDown per PHP aufbauen
		$buffer .= '<option value="0">--</option>'.$eol;
		foreach ($aProject[$selected_client_id] as $prj_id => $prj_name) {
			$sel = ($sSelectedPrjId == $prj_id) ? ' selected' : '';
			$sClass = ($aStatus[$selected_client_id][$prj_id] == "0") ? "status0" : "status1";
			$buffer .= '<option value="'.$prj_id.'"'.$sel.' class="'.$sClass.'">'.stripslashes($prj_name).'</option>'.$eol;
		}
		$buffer .= '</select>'.$eol;
	// JavaScript
		$buffer .= '<script language="JavaScript" type="text/javascript"><!-- '.$eol;
		$buffer .= 'clientProjectId = new Array();'.$eol;
		$buffer .= 'clientProjectText = new Array();'.$eol;
		$buffer .= 'clientProjectStatus = new Array();'.$eol;
		// Projekte-DropDown Arrays
		$i = 0; // num. counter f. JS-Array
		foreach ($aProject as $client_id => $aPrj) {
			if (!is_array($aPrj)) { continue; }
			// DropDown Values
			$buffer .= 'clientProjectId['.$i.'] =  new Array(';
			$buffer .= "'".implode("','", array_keys($aPrj))."'";
			$buffer .= ');'."\n";
			// DropDown Texte
			$buffer .= 'clientProjectText['.$i.'] =  new Array(';
			$buffer .= "'".implode("','", $aPrj)."'";
			$buffer .= ');'."\n";
			// Projekt-Status
			$buffer .= 'clientProjectStatus['.$i.'] =  new Array(';
			$buffer .= "'".implode("','", $aStatus[$client_id])."'";
			$buffer .= ');'."\n";
			$i++;
		}
		$buffer .= 'function fillProjectSelection'.$fieldnameext.'(obj,tgt) {
			with(document.forms["editForm'.$fieldnameext.'"]) {
				// benoetigt die arrays "clientProjectId" + "clientProjectText"!
				elements[tgt].length = clientProjectId[obj.selectedIndex].length+1;
				elements[tgt].options[0].value = "0";
				elements[tgt].options[0].text = "--";
				elements[tgt].options[0].className = "status1";
				
				for (p=1; p<=clientProjectId[obj.selectedIndex].length; p++) {
					elements[tgt].options[p].selected = false;
					elements[tgt].options[p].value = clientProjectId[obj.selectedIndex][p-1];
					elements[tgt].options[p].text = clientProjectText[obj.selectedIndex][p-1];
					if (clientProjectStatus[obj.selectedIndex][p-1] == "0") { elements[tgt].options[p].className = "status0"; }
					else { elements[tgt].options[p].className = "status1"; }
				}
			}
		}'.$eol;
		$buffer .= '// -->'.$eol;
		$buffer .= '</script>'.$eol;
		
		// output
		return $buffer;
	}

	/**
	 * ALLE Projekte als verschachteltes Client/Projects DHTML-DropDown
	 * 
	 * @access	public
	 * @param	int		$sSelectedPrjId
	 * @param	int		$sSelectedClientId		[optional, default: 0]
	 * @return	HTML
	 */
	function getAllClientprojectsDropdown($sSelectedPrjId, $sSelectedClientId=0) {
		// init vars
		global $aMSG,$syslang;
		$buffer = ''; // HTML
		$eol	= "\n"; // Zeilenumbruch fuer bessere Lesbarkeit des HTML-Quelltextes
		$aProject = array();
		$aProject[0][0] = '--'; // wichtig -> erstes JS-array fuer KEIN Client/Prj ($aClient[0])!
		$aClient = array('0' => $aMSG['form']['fremdkostenclients'][$syslang]); // erster Eintrag im Clients-DropDown
		$selected_client_id = ($sSelectedClientId != 0) ? $sSelectedClientId : 0;
		$aStatus = array('0' => array());
		// alle noetigen informationen fuer die verschachtelten dropdowns mit einer query ermitteln!
		$sql = "SELECT `p`.`client_id`,`p`.`id`,`p`.`projectname`,`p`.`prj_nr`,`p`.`prj_status`,`c`.`client_nr`,`c`.`client_shortname` 
				FROM `adm_project` `p`, `adr_contact` `c` 
				WHERE `c`.`id` = `p`.`client_id` 
				ORDER BY `c`.`client_shortname` ASC, `p`.`projectname` ASC";
		$this->oDb->query($sql);
		// daten sammeln
		while ($tmp = $this->oDb->fetch_array()) {
			// verschachteltes Projekte-DropDown (key=client_id, val=Prj-array)
			$aProject[$tmp['client_id']][$tmp['id']] = addslashes($tmp['projectname'].' [ '.$tmp['client_nr'].'.'.$tmp['prj_nr'].' ]');
			$aStatus[$tmp['client_id']][$tmp['id']] = $tmp['prj_status'];
			
			// Clients-DropDown (key=id, val=name)
			$aClient[$tmp['client_id']] = $tmp['client_shortname'];
			// ggf. client_id aus project_id ermitteln
			if (!empty($sSelectedPrjId) && $sSelectedPrjId == $tmp['id']) {
				$selected_client_id = $tmp['client_id'];
			}
		}
	// HTML Dropdowns
		// Clients-DropDown (wird nicht gespeichert, sondern das dynamisch gefuellte Projekte-DropDown!)
		$buffer = '<select name="clients" id="clients" size="1" onChange="fillProjectSelection(this, \'aData[project_id]\')">'.$eol;
		foreach ($aClient as $client_id => $client_name) {
			$sel = ($selected_client_id == $client_id) ? ' selected' : '';
			$buffer .= '<option value="'.$client_id.'"'.$sel.'>'.$client_name.'</option>'.$eol;
		}
		$buffer .= '</select>'.$eol;
		// Projekte-DropDown (wird wenn ein Client ausgewaehlt wird per JS aufgebaut; wird in DB gespeichert)
		$buffer .= '<select name="aData[project_id]">'.$eol;
		
		// wenn "edit"-mode: -> den DB-Eintrag selectieren, dazu das dazugehoerige DropDown per PHP aufbauen
		$buffer .= '<option value="0">--</option>'.$eol;
		foreach ($aProject[$selected_client_id] as $prj_id => $prj_name) {
			$sel = ($sSelectedPrjId == $prj_id) ? ' selected' : '';
			$sClass = ($aStatus[$selected_client_id][$prj_id] == "0") ? "status0" : "status1";
			$buffer .= '<option value="'.$prj_id.'"'.$sel.' class="'.$sClass.'">'.$prj_name.'</option>'.$eol;
		}
		$buffer .= '</select>'.$eol;
	// JavaScript
		$buffer .= '<script language="JavaScript" type="text/javascript"><!-- '.$eol;
		$buffer .= 'clientProjectId = new Array();'.$eol;
		$buffer .= 'clientProjectText = new Array();'.$eol;
		$buffer .= 'clientProjectStatus = new Array();'.$eol;
		// Projekte-DropDown Arrays
		$i = 0; // num. counter f. JS-Array
		foreach ($aProject as $client_id => $aPrj) {
			if (!is_array($aPrj)) { continue; }
			// DropDown Values
			$buffer .= 'clientProjectId['.$i.'] =  new Array(';
			$buffer .= "'".implode("','", array_keys($aPrj))."'";
			$buffer .= ');'."\n";
			// DropDown Texte
			$buffer .= 'clientProjectText['.$i.'] =  new Array(';
			$buffer .= "'".implode("','", $aPrj)."'";
			$buffer .= ');'."\n";
			// Projekt-Status
			$buffer .= 'clientProjectStatus['.$i.'] =  new Array(';
			$buffer .= "'".implode("','", $aStatus[$client_id])."'";
			$buffer .= ');'."\n";
			$i++;
		}
		$buffer .= 'function fillProjectSelection(obj,tgt) {
			with(document.forms["filterForm"]) {
				// benoetigt die arrays "clientProjectId" + "clientProjectText"!
				elements[tgt].length = clientProjectId[obj.selectedIndex].length+1;
				elements[tgt].options[0].value = "0";
				elements[tgt].options[0].text = "--";
				elements[tgt].options[0].className = "status1";
				
				for (p=1; p<=clientProjectId[obj.selectedIndex].length; p++) {
					elements[tgt].options[p].selected = false;
					elements[tgt].options[p].value = clientProjectId[obj.selectedIndex][p-1];
					elements[tgt].options[p].text = clientProjectText[obj.selectedIndex][p-1];
					if (clientProjectStatus[obj.selectedIndex][p-1] == "0") { elements[tgt].options[p].className = "status0"; }
					else { elements[tgt].options[p].className = "status1"; }
				}
			}
		}'.$eol;
		$buffer .= '// -->'.$eol;
		$buffer .= '</script>'.$eol;
		
		// output
		return $buffer;
	}
	
	/**
	 * Ermittle Ausgewählten Datensatz als Read-Only Version des Client/Projects DHTML-DropDown
	 * 
	 * @access	public
	 * @param	int		$sSelectedPrjId
	 * @return	HTML
	 */
	function getClientproject($sSelectedPrjId='') {
		// vars
		if (empty($sSelectedPrjId)) return;
		// select
		$sql = "SELECT p.client_id, p.id, p.projectname, p.prj_nr, c.client_nr, c.client_shortname
				FROM adm_project p, adr_contact c 
				WHERE c.id = p.client_id 
				AND p.id = '".$sSelectedPrjId."'";
		$this->oDb->query($sql);
		// output
		return $this->oDb->fetch_array();
	}

	function setProjectStatus($prjid,$status) {
		$sql = "UPDATE adm_project SET prj_status='$status',prj_date".$status."='".date('Y-m-d')."'";
		$sql .= " WHERE id=$prjid";
		return $this->oDb->query($sql);
	}

// TIMESHEETS ////////////////////////////////////////////////////////////////////////

	/**
	 * Diese Methode ermittelt, ob es mindestens einen Timesheet-Datensatz zu einer Project-ID gibt.
	 * @param	int		$project_id
	 * @return	boolean
	 */
	function hasTimesheets($project_id) {
		if (!$project_id) return false; // TODO: error?
		// update data
		$sql = "SELECT `id` 
				FROM `adm_timesheet` 
				WHERE `project_id` = '$project_id' 
					AND `invoice_id` = 0 
					AND `flag_bill` = '0'";
		$this->oDb->query($sql);
		return ($this->oDb->num_rows() > 0) ? true : false;
	}

	/**
	 * Diese Methode markiert einen oder mehrere Timesheet-Datensaetze als berechnet.
	 * @param	mixed	$timesheet_id
	 * @param	int		$invoice_id
	 * @return	boolean
	 */
	function markTimesheetAsCleared($timesheet_id, $invoice_id) {
		// check vars
		if (is_array($timesheet_id)) {
			$timesheet_id = implode(',', $timesheet_id);
		}
		if (empty($timesheet_id)) return false; // TODO: error?
		
		// darf eintreten, damit die invoice auf n.a. gesetzt werden kann.
		// if (!$invoice_id) return false; // TODO: error?
		// update data
		$sql = "UPDATE `adm_timesheet` 
				SET `flag_bill`='1',`invoice_id`='".$invoice_id."' 
				WHERE `id` IN(".$timesheet_id.")";
		return $this->oDb->query($sql);
	}

	/**
	 * Diese Methode markiert einen oder mehrere Timesheet-Datensaetze als NICHT-berechnet.
	 * @param	mixed	$timesheet_id
	 * @return	boolean
	 */
	function markTimesheetAsUncleared($timesheet_id) {
		// check vars
		if (is_array($timesheet_id)) {
			$timesheet_id = implode(',', $timesheet_id);
		}
		if (empty($timesheet_id)) return false; // TODO: error?
		// update data
		$sql = "UPDATE `adm_timesheet` 
				SET `flag_bill`='0',`invoice_id`='0' 
				WHERE `id` IN(".$timesheet_id.")";
		return $this->oDb->query($sql);
	}

// EXPENSES ////////////////////////////////////////////////////////////////////////

	/**
	 * Diese Methode ermittelt, ob es mindestens einen Timesheet-Datensatz zu einer Project-ID gibt.
	 * @param	int		$project_id
	 * @return	boolean
	 */
	function hasExpenses($project_id) {
		if (!$project_id) return false; // TODO: error?
		// update data
		$sql = "SELECT `id` 
				FROM `adm_expense` 
				WHERE `project_id` = '$project_id' 
					AND `invoice_id` = 0 
					AND `flag_bill` = '0'";
		$this->oDb->query($sql);
		return ($this->oDb->num_rows() > 0) ? true : false;
	}

	/**
	 * Diese Methode markiert einen oder mehrere Expense-Datensaetze als berechnet.
	 * @param	mixed	$timesheet_id
	 * @param	int		$invoice_id
	 * @return	boolean
	 */
	function markExpenseAsCleared($expense_id, $invoice_id) {
		// check vars
		if (is_array($expense_id)) {
			$expense_id = implode(',', $expense_id);
		}
		if (empty($expense_id)) return false; // TODO: error?
		// darf eintreten, damit die invoice auf n.a. gesetzt werden kann.
		//if (!$invoice_id) return false; // TODO: error?
		// update data
		$sql = "UPDATE `adm_expense` 
				SET `flag_bill`='1',`invoice_id`='".$invoice_id."' 
				WHERE `id` IN(".$expense_id.")";
		return $this->oDb->query($sql);
	}

	/**
	 * Diese Methode markiert einen oder mehrere Expense-Datensaetze als NICHT-berechnet.
	 * @param	mixed	$expense_id
	 * @return	boolean
	 */
	function markExpenseAsUncleared($expense_id) {
		// check vars
		if (is_array($expense_id)) {
			$expense_id = implode(',', $expense_id);
		}
		if (empty($expense_id)) return false; // TODO: error?
		// update data
		$sql = "UPDATE `adm_expense` 
				SET `flag_bill`='0',`invoice_id`='0' 
				WHERE `id` IN(".$expense_id.")";
		return $this->oDb->query($sql);
	}

} // END class
?>