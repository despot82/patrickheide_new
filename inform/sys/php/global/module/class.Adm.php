<?php
/**
 * Diese Klasse stellt allgemeine Methoden für den Adm-Bereich zur Verfügung.
 *
 * @access   public
 * @package  adm
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @author	Andy Fehn <af@design-aspekt.com>
 * @version	1.0 / 2006-03-17
 * 
 */
class Adm {
	
	/**
	 * @access	private
	 * @var		object	EasyAccess-Objekt
	 */
	var $oAdmEA = null;
	/**
	 * @access	private
	 * @var		string	[ Invoice | Proposal ] 
	 */
	var $mode = '';
	/**
	 * @access	private
	 * @var		int		User-ID
	 */
	var $uid = 0;
	
	/**
	 * Konstruktor
	 * 
	 */
	function adm() {

	}
		
	/** 
	 * format team rate
	 * 
	 * @param	int		$rate
	 * @param	string	$syslang
	 * @return	string	formatierte rate
	 * @uses	function format_money()
	 */
	function formatRate($rate, $syslang) {
		if (empty($rate)) return;
		$rate = format_money($rate, $syslang); // formatiere money
		$rate = str_replace(array(',00', '.00'), '', $rate); // ggf. ".00" nicht ausgeben
		// output
		return $rate;
	}

	/**
	 * format team rate-date
	 * 
	 * @param	string	$date
	 * @param	object	$oDate
	 * @return	string	formatiertes rate-datum
	 * @uses	function date_mysql2trad() der uebergebenen Date-Klasse
	 */
	function formatValidFrom($date, $oDate) {
		if (empty($date)) return;
		// output
		return ($date != '0000-00-00') ? $oDate->date_mysql2trad($date) : '00.00.0000';
	}
	
	/**
	 * Setter-Methode fuer AdmInvoice Easy-Access-Klasse
	 * 
	 * @param object $oAdmInvoiceEA
	 * @return void
	 */
	function setAdmEA(&$oAdmEA) {
		$this->oAdmEA =& $oAdmEA;
	}
	
	/**
	 * Setter-Methode fuer User-ID
	 * 
	 * @param array $uid
	 * @return void
	 */
	function setUid($uid) {
		$this->uid = $uid;	
	}

	/**
	 * Bereite die Informationen im Adata Array für den Update vor.
	 * @param object $oPrj
	 * @param array $aData
	 * @return array
	 */
	function prepareInvoiceUpdate(&$oPrj,$aData) {

		list($id,$aData2) = each($aData);
		unset($aData);
		$aPrj = $oPrj->getProjectdetails($aData2['project_id']);
		$aData['prj_nr']		= $aPrj['prj_nr']; 
		$aData['currency']		= $aPrj['currency']; 
		$aData['tax']			= $aPrj['tax']; 
		$aData['handling']		= $aPrj['handling']; 
		$aData['po_number']		= $aPrj['po_number']; 
		$aData['projectname']	= $aPrj['projectname'];
		return $aData;
	}

	/**
	 * Speichert die Daten für das durch $mode angebebene Modul im Adm
	 * 
	 * @param array $aData
	 * @return bool
	 */
	function save($aData) {
		// UPDATE
		if(isset($aData['id']) && !empty($aData['id'])) {
			$aData['last_mod_by'] = $this->uid;
			// Invoice
			if($this->mode == 'invoice') {
				$aData2 = $this->oAdmEA->getInvoice(0,0," id='".$aData['id']."'");
				if($aData2[$aData['id']]['flag_closed'] == 0) {
					
					return $this->oAdmEA->update($aData);
				}
				else {
					return false;
				}
			}
			// Proposal
			if($this->mode == 'proposal') {
				// filter data-array
				foreach($aData as $field => $val) {
					if($field != 'proposal_introduction' && $field != 'proposal_resume' && $field != 'discount' && $field != 'id') {
						unset($aData[$field]);
					}
				}
				return $this->oAdmEA->saveProposal($aData);
			}
		}
		// INSERT
		else {
			$aData['created'] = date('dmYHis');
			$aData['created_by'] = $this->uid;
			// Invoice
			if($this->mode == 'invoice') {
				$pid = $aData['projectid'];
				$aIds = $aData['aIds'];
				unset($aData['projectid']);
				unset($aData['aIds']);
				return $this->oAdmEA->insert($pid,$aIds,$aData);
			}
			// Proposal
			if($this->mode == 'proposal') {
				// brauchen wir z.zt. nicht, da die proposal stammdaten i.d. adm_project gespeichert werden
				
			}
		}
	}
	/**
	 * 
	 * Diese Methode storniert eine Rechnung
	 * @param array $aData
	 * @return bool
	 * 
	 */
	function storno($aData) {
		$ret		= $aData['id'];
		$aInvoice	= $this->oAdmEA->getInvoice(0, 0, " WHERE id='".$aData['id']."'");
		$aData['last_mod_by']	= $this->uid;
		$this->oAdmEA->update($aData, 'adm_invoice');
		if($aInvoice[$aData['id']]['flag_storno'] == 0) {
			$ret	= $this->duplicate($aData['id'], 1);
		}
		return $ret;
	}
	
	/**
	 * 
	 * Diese Methode dupliziert eine Rechnung.
	 * @param int $invoiceid
	 * @return mixed
	 * 
	 */
	function duplicate($invoiceid, $storno=0, $dunning=0, $client_id=0, $aAdm=array(), $aMSG=array(),$syslang='') {
		
		$aData 		= $this->oAdmEA->getInvoiceData(0, $invoiceid, 'prio', 'DESC');	// JavaScripts + oeffnendes Form-Tag
		$aInvoice	= array();
		
		for($i=0; is_array($aData[$i]); $i++) {

			if(count($aInvoice) == 0) {

				$aInvoice['project_id']		= $aData[$i]['project_id'];
				$aInvoice['introduction']	= $aData[$i]['introduction'];
				$aInvoice['resume']			= $aData[$i]['resume'];
				$aInvoice['date']			= $aData[$i]['date'];
				$aInvoice['discount']		= $aData[$i]['discount'];
				$aInvoice['tax']			= $aData[$i]['tax'];
				$aInvoice['prj_nr']			= $aData[$i]['prj_nr'];
				$aInvoice['currency']		= $aData[$i]['currency'];
				$aInvoice['po_number']		= $aData[$i]['po_number'];
				$aInvoice['projectname']	= $aData[$i]['projectname'];
				$aInvoice['handling']		= $aData[$i]['handling'];
				$aInvoice['raddress_id']	= $aData[$i]['raddress_id'];
				$aInvoice['rcontact_id']	= $aData[$i]['rcontact_id'];

				if($storno==1) {
					$aInvoice['parent_id']	= $invoiceid;
					$aInvoice['flag_storno']=1;
					$aInvoice['storno_text']=$aData[$i]['storno_text'];
					$aInvoice['date']			= date('Y-m-d');
				}
			
				if($dunning==1) {
					$aInvoice['date']		= date('Y-m-d');
					$aInvoice['parent_id']	= $invoiceid;
					$aInvoice['flag_mahnung'] = 1;
				}
			
				$aInvoice['created_by'] = $this->uid;
				$aInvoice['created']	= date('dmYHis');
			}
			
			if($dunning != 1) {
				$aInvoiceDatasets[$i]['prio']			= $aData[$i]['prio'];
				$aInvoiceDatasets[$i]['job_cat']		= $aData[$i]['job_cat'];
				$aInvoiceDatasets[$i]['alt_jobcat']		= $aData[$i]['alt_jobcat'];
				$aInvoiceDatasets[$i]['description']	= $aData[$i]['description'];
				$aInvoiceDatasets[$i]['hours']			= $aData[$i]['hours'];
				$aInvoiceDatasets[$i]['quantity']		= $aData[$i]['quantity'];
				$aInvoiceDatasets[$i]['quantity_cat']	= $aData[$i]['quantity_cat'];
				$aInvoiceDatasets[$i]['price_net']		= $aData[$i]['price_net'];
				$aInvoiceDatasets[$i]['flag_extern']	= $aData[$i]['flag_extern'];
				$aInvoiceDatasets[$i]['created_by']		= $this->uid;
				$aInvoiceDatasets[$i]['created']		= date('dmYHis');
			} else {
				// [nh] invoice dataset now in dunning_dataset 22/02/07
				$aInvoiceDatasets[0]['price_net']		+= $aData[$i]['price_net'];
				$aInvoiceDatasets[0]['created']			= date('dmYHis');
				$aInvoiceDatasets[0]['created_by']		= $this->uid;
				$aInvoiceDatasets[0]['hours']			+= $aData[$i]['hours'];
				// Projectclient => nötig für die richtige Projectnb
				$aClientNb = $this->oAdmEA->getAdrContact($client_id);
				$aInvoiceDatasets[0]['alt_jobcat']		= $aMSG['adm']['dunning_letterto'][$syslang].' '.str_replace(array('{clientNumber}','{projectNumber}','{invoiceNumber}'),array($aClientNb['client_nr'],$aData[$i]['prj_nr'],$aData[$i]['invoice_id']),$aAdm[$this->mode]['followpage']['projectNumber']['format']);
			}
		}
		$nId	= ($dunning==1) ? $this->oAdmEA->dunning($invoiceid, $aInvoice, $aInvoiceDatasets):$this->oAdmEA->duplicate($aInvoice, $aInvoiceDatasets);
		
		return $nId;
	}

	/**
	 * 
	 * Setzt den Mode bsp. Invoice oder Proposal
	 * @param String $mode
	 * 
	 */
	function setMode($mode) {
		
		$this->mode = strtolower($mode);
	}
	
	/**
	 * Diese Methode generiert die Daten für die Proposal oder Invoice PDFs.
	 * 
	 * @param int $invoiceid
	 * @param String $syslang
	 * @param array $aENV
	 * @param array $aJobcat
	 * @param array $aAdm
	 * @param int $client_id
	 * @return array
	 */
	function getPDFData($id,$syslang,$aENV,$aJobcat,$aJobPrice,&$aAdm,$client_id) {
		$aData = ($this->mode=='invoice' || $this->mode=='proposal') ? $this->oAdmEA->getPDFData($id,'prio','DESC'):$this->oAdmEA->getPDFDunningData($id,'prio','DESC');
		
		for($i=0;is_array($aData[$i]);$i++) {
			
			// summen-vars sammeln
			if(isset($aData[$i]['flag_extern']) && !empty($aData[$i]['flag_extern'])) {
				$aPrj['extprice'] += $aData[$i]['price_net'];
			}
			else {
				$aPrj['sum'] += $aData[$i]['price_net'];
			}
			$aPrj['price'][$i] = format_money($aData[$i]['price_net'],$syslang);
			$aPrj['id'][$i] = $aData[$i]['id'];
			$aPrj['project_id'] = $aData[$i]['project_id'];
			
			if(empty($aData[$i]['alt_jobcat'])) {
				if($aAdm[$this->mode]['linenumbers'] && is_array($aData[1])) {
					$aPrj['hd'][$i] = Tools::convertFromUTF8(($i+1).'. '.$aJobcat[$syslang][$aData[$i]['job_cat']]);
				} else {
					$aPrj['hd'][$i] = Tools::convertFromUTF8($aJobcat[$syslang][$aData[$i]['job_cat']]);
				}
			}
			else {
				if($aAdm[$this->mode]['linenumbers'] && is_array($aData[1])) {
					$aPrj['hd'][$i] = Tools::convertFromUTF8(($i+1).'. '.$aData[$i]['alt_jobcat']);
				} else {
					$aPrj['hd'][$i] = Tools::convertFromUTF8($aData[$i]['alt_jobcat']);
				}
			}			
			$aPrj['text'][$i] = Tools::convertFromUTF8($aData[$i]['description']);
			$aPrj['hours'][$i] = $aData[$i]['hours'];

			if($aData[$i]['quantity'] != 0) {
				$aPrj['mt'][$i] = format_hours($aData[$i]['quantity'],$syslang);
				$aPrj['mt'][$i] .= ($aData[$i]['quantity_cat'] == 'day') ? $aAdm[$this->mode]['days'][$syslang] : $aAdm[$this->mode]['hours'][$syslang];
				if(!empty($aAdm[$this->mode]['text']['per'])) {
					$aPrj['mt'][$i] .= $aAdm[$this->mode]['text']['per'];
					
					if($aAdm[$this->mode]['bHourlyrate']) {
						if($aData[$i]['quantity_cat'] != 'day') {
							$aPrj['mt'][$i] .= format_money($aJobPrice[$aData[0]['currency']][$aData[$i]['job_cat']]);
						}
						else {
							$aPrj['mt'][$i] .= format_money(($aJobPrice[$aData[0]['currency']][$aData[$i]['job_cat']]*8));
						}
					}
					else {
						$aPrj['mt'][$i] .= format_money(($aJobPrice[$aData[0]['currency']][$aData[$i]['job_cat']]*8));
					}
				}
			}
			$aPrj['quantity_cat'][$i] = $aData[$i]['quantity_cat'];
			$aPrj['flag_extern'][$i] = $aData[$i]['flag_extern'];
		}
		$aPrj['currency'] = $aData[0]['currency'];
		$aPrj['handling'] = $aData[0]['handling'];
		$aPrj['date'] = $aData[0]['date'];
		$aPrj['tax'] = $aData[0]['tax'];
		$aPrj['parent_id'] = $aData[0]['parent_id'];
		
		$aPrj['dunning_letter'] = $aData[0]['flag_mahnung'];
		$aPrj['projectNumber'] = $aData[0]['prj_nr'];
		$aPrj['projectName'] = Tools::convertFromUTF8($aData[0]['projectname']);
		$aPrj['introduction'] = Tools::convertFromUTF8($aData[0]['introduction']);
		$aPrj['resume'] = Tools::convertFromUTF8($aData[0]['resume']);
		$aPrj['discount'] = $aData[0]['discount'];
		$aPrj['contactid'] = $aData[0]['contact_id'];
		$aPrj['poNumber'] = $aData[0]['po_number'];

		if($this->mode == 'invoice') {
			$aPrj['invoiceNB'] = $aData[0]['invoice_id'];

			// Projectclient => nötig für die richtige Projectnb
			$aClientNb = $this->oAdmEA->getAdrContact($client_id);
			
			$rs2 = $this->oAdmEA->getAdrContact($aPrj['contactid'],'adr_contact');
			$aData3 = $this->oAdmEA->getAdrRefId($aData[0]['raddress_id']);
			$rs = $this->oAdmEA->getAdrContact($aData3[0]['reference_id']);
			$aData2 = $this->oAdmEA->getAdrDetails($aData3[0]['reference_id']);
			
			$aArray = array($aClientNb['client_nr'],$aPrj['projectNumber'],$aPrj['invoiceNB']);
			$str = str_replace(array('{clientNumber}','{projectNumber}','{invoiceNumber}'),$aArray,$aAdm[$this->mode]['startpage']['projectNumber']['format']);
			$aPrj['projectNumber'] = Tools::convertFromUTF8($str);
			unset($str);
			$str = str_replace(array('{clientNumber}','{projectNumber}','{invoiceNumber}'),$aArray,$aAdm[$this->mode]['followpage']['projectNumber']['format']);
			$aPrj['projectNumberfollow'] = Tools::convertFromUTF8($str);
			
		}
		
		if($this->mode == 'dunning') {
		
			$oDb	= new dbconnect($aENV['db']);
			$aTemp	= $oDb->fetch_by_id($aData[0]['dunning_id'], 'adm_dunning');
			$sSql	= 'SELECT id FROM adm_dunning WHERE project_id = '.$aTemp['project_id'].' ORDER BY date ASC';
			$oDb->query($sSql);			
			while($aTemp = $oDb->fetch_array()) {
				$aPrj['invoiceNB']++; 	
				if($aData[0]['dunning_id']==$aTemp['id']) { break; }
			}
			
			// Projectclient => nötig für die richtige Projectnb
			$aClientNb = $this->oAdmEA->getAdrContact($client_id);
			
			$rs2 = $this->oAdmEA->getAdrContact($aPrj['contactid'],'adr_contact');
			$aData3 = $this->oAdmEA->getAdrRefId($aData[0]['raddress_id']);
			$rs = $this->oAdmEA->getAdrContact($aData3[0]['reference_id']);
			$aData2 = $this->oAdmEA->getAdrDetails($aData3[0]['reference_id']);
			
			$aArray = array($aClientNb['client_nr'],$aPrj['projectNumber'],$aPrj['invoiceNB']);
			$str = str_replace(array('{clientNumber}','{projectNumber}','{invoiceNumber}'),$aArray,$aAdm[$this->mode]['startpage']['projectNumber']['format']);
			$aPrj['projectNumber'] = Tools::convertFromUTF8($str);
			unset($str);
			$str = str_replace(array('{clientNumber}','{projectNumber}','{invoiceNumber}'),$aArray,$aAdm[$this->mode]['followpage']['projectNumber']['format']);
			$aPrj['projectNumberfollow'] = Tools::convertFromUTF8($str);
			
		}		
		
		if($this->mode == 'proposal') {
			// Projectclient => nötig für die richtige Projectnb
			$rs = $this->oAdmEA->getAdrContact($client_id);
			$rs2 = $this->oAdmEA->getAdrContact($aPrj['contactid'],'adr_contact');
			$aData2 = $this->oAdmEA->getAdrDetails($client_id);
			$str = str_replace(array('{clientNumber}','{projectNumber}'),array($rs['client_nr'],$aPrj['projectNumber']),$aAdm[$this->mode]['startpage']['projectNumber']['format']);
			$aPrj['projectNumber'] = Tools::convertFromUTF8($str);
			$str = str_replace(array('{clientNumber}','{projectNumber}'),array($rs['client_nr'],$aPrj['projectNumber']),$aAdm[$this->mode]['followpage']['projectNumber']['format']);
			$aPrj['projectNumberfollow'] = Tools::convertFromUTF8($str);
		}
		$aPrj['clientname'] = Tools::convertFromUTF8($rs['company']);
		
		global $aENV;
		
		$aPattern	= array('{COMPANY}', '{TITLE}', '{FIRSTNAME}', '{SURNAME}', '{STREET}', '{BUILDING}', '{REGION}', '{TOWN}', '{COUNTRY}', '{PO_BOX}', '{PO_BOX_TOWN}');
		$aReplace	= array(Tools::convertFromUTF8($rs['company']),
							Tools::convertFromUTF8(!empty($aPrj['contactid']) ? $rs2['title']:$rs['title']),
							Tools::convertFromUTF8(!empty($aPrj['contactid']) ? $rs2['firstname']:$rs['firstname']),
							Tools::convertFromUTF8(!empty($aPrj['contactid']) ? $rs2['surname']:$rs['surname']),
							Tools::convertFromUTF8($aData2[0]['street']),
							Tools::convertFromUTF8($aData2[0]['building']),
							Tools::convertFromUTF8($aData2[0]['region']),
							Tools::convertFromUTF8($aData2[0]['town']),
							Tools::convertFromUTF8($aData2[0]['country']),
							Tools::convertFromUTF8($aData2[0]['po_box']), 
							Tools::convertFromUTF8($aData2[0]['po_box_town']));
		foreach($aReplace as $nKey => $sValue) {
			$aReplace[$nKey]	= (empty($sValue) ? '':"$sValue ");
		}		
		$aPrj['address']	= str_replace($aPattern, $aReplace, $aENV['address']['format']);
					
		return $aPrj;
	}
	
	/**
	 * Prepariert Money-Daten vor dem Speichern (komma/punkt/tausenderstellen problematik)
	 * 
	 * @param	string	$money
	 * @return	string	$money bearbeitet
	 */
	function prepareMoneyBeforeSave($money) {
		// 1. leerzeichen entfernen
		$money	= implode('', explode(' ', trim($money)));
		// 2. kommas gegen punkte tauschen
		$money	= str_replace(',', '.', $money);
		// 3. Anzahl Punkte ermitteln
		$aMoney = explode('.', $money);
		if (count($aMoney) > 2) {
			// wenn mehr als 1 Punkt
			$search		= "/(\d+)(\.)(\d+)(\.)(\d+)/i";
			$replace	= "\$1\$3.\$5";
			$money		= preg_replace($search, $replace, $money);
		} else {
			// 0 oder 1 punkt
			if (strlen($aMoney[1]) > 2) {
			// wenn hinter dem einen punkt mehr als 2 zahlen stehen -> punkt entfernen
				$money	= str_replace('.', '', $money);
			}
		}
		// output
		return $money;
	}
	
	/**
	 * Diese Methode ermittelt alle Summen/Zwischensummen/Mwst-Anteil/Rabattanteil etc zu einer Invoice (oder einem Proposal)
	 * 
	 * @param	int		$sum_fees 				[optional, default: 0]
	 * @param	int		$sum_extern 			[optional, default: 0]
	 * @param	int		$nTax					[optional, default: 0]
	 * @param	int		$nExpenseExtraCharge	[optional, default: 0]
	 * @param	int		$nDiscount				[optional, default: 0]
	 * @return	array[key] = value (unbehandelte float-werte)
	 */
	function calcSum($sum_fees=0, $sum_extern=0, $nTax=0, $nHandling=0, $nDiscount=0) {
		// check vars
		if ($sum_fees == 0 && $sum_extern == 0) return false; // TODO: error?
		// init vars
		$buffer		= array(
			'rate_tax'			=> $nTax,
			'rate_handling'		=> $nHandling,
			'rate_discount'		=> $nDiscount,
			'fees_subtotal'		=> 0,
			'fees_discount'		=> 0,
			'fees_total'		=> 0,
			'extern_subtotal'	=> 0,
			'extern_handling'	=> 0,
			'extern_total'		=> 0,
			'sum_net'			=> 0,
			'sum_tax'			=> 0,
			'sum_gross'			=> 0
		);
		
		// wenn es honorar-kosten gibt
		if ($sum_fees > 0) {
			$buffer['fees_total']			= $sum_fees;
			// wenn es rabatt gibt
			if ($nDiscount > 0) {
				$buffer['fees_subtotal']	= $sum_fees;
				$buffer['fees_discount']	= ($sum_fees * $nDiscount / 100);
				$buffer['fees_total']		= ($sum_fees - $buffer['fees_discount']);
			}
		}
		// wenn es externe kosten gibt
		if ($sum_extern > 0) {
			$buffer['extern_total']			= $sum_extern;
			// wenn es einen Aufschlag fuer externe Kosten gibt (setup.php)
			if ($nHandling > 0) {
				$buffer['extern_subtotal']	= $sum_extern;
				$buffer['extern_handling']	= ($sum_extern / 100 * $nHandling);
				$buffer['extern_total']		= ($sum_extern + $buffer['extern_handling']);
			}
		}
		// netto summe
		$buffer['sum_net'] = 0;
		if ($buffer['fees_total'] || $buffer['extern_total']) $buffer['sum_net']	+= $buffer['fees_total'];
		if ($buffer['fees_total'] || $buffer['extern_total']) $buffer['sum_net']	+= $buffer['extern_total'];
		
		// ggf. mwst + bruttosumme
		if (!empty($nTax)) {
			$buffer['sum_tax']		= $this->calcTax($buffer['sum_net'], $nTax);
			$buffer['sum_gross']	= ($buffer['sum_tax'] + $buffer['sum_net']);
		}
		
		// output
		return $buffer;
	}
	
	/**
	 * Ermittele den MwSt.-Anteil eines Netto-Betrages
	 * 
	 * @param	int		$nSum 			Ausgangsbetrag
	 * @param	int		$nTax 			[optional, default: 0]
	 * @return	int		Mwst. Anteil
	 */
	function calcTax($nSum, $nTax=0) {
		// vars
		if ($nSum == '-' || $nSum == 0 || $nTax == 0) return 0;
		// calculate
		$buffer		= ($nSum * $nTax / 100);
		// output
		return $buffer;
	}

	/**
	 * Diese Methode ermittelt im uebergeben Invoice-Data-Array die Nummer mittels uebergebener Invoice-ID.
	 * Die Invoice-ID kann als kommaseparierter string oder als array uebergeben werden.
	 * 
	 * @param	array	$aInvoiceData
	 * @param	mixed	$invoice_id	[string|array]
	 * @return	int		Proposal-ID
	 */
	function getInvoiceNb(&$aInvoiceData,$invoice_id) {
		$aInvoiceId = (!is_array($invoice_id)) ? explode(',', $invoice_id) : $invoice_id;
		$aInvoiceNb = array();
		foreach ($aInvoiceData as $i_id => $i_data) {
			if(in_array($i_id, $aInvoiceId)) {
				$aInvoiceNb[$i_id] = $i_data['invoice_nb'];
			}
		}
		return $aInvoiceNb;
	}
	/**
	 * Diese Methode ermittelt alle Jahre in denen es Rechnungen gibt.
	 * Wenn es keine Jahre gibt, wird ein leeres Array zurückgegeben (sicherheitsgründe für foreach)
	 * @return array
	 * @access public
	 */
	function getYear() {
		$aYear = $this->oAdmEA->getInvoiceYear();
		for($i=0;is_array($aYear[$i]);$i++) {
			$aYears[$aYear[$i]['year']] = $aYear[$i]['year'];
		}
		if(!is_array($aYears)) {
			$aYears = array();
		}
		return $aYears;
	}

} // END class
?>