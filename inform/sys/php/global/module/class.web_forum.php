<?php // Class forum
/**
* This class provides common forum functionalities.
*
* benoetigt eine tabelle mit folgenden feldern: id, article_id, parent_id, headline, content, datetimestamp, homepage_active
*
* Example: 
* <pre><code>
* // init
* $oForum =& new forum($aDBvars);
* // print gesamtanzahl eintraege
* echo $oForum->get_count_threads(); // params: --
* // print complete tree
* $oForum->html_threads_list($article_id, $current_id); // params: $article_id[,$current_id=''][,$detail_uri='']
* // ermittle aktuellen eintrag
* $data = $oForum->get_thread(127); // params: $id
* print_r($data);
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-03-30
*/

#require_once("service/class.db_mysql.php");

class forum extends dbconnect {
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse forum:
		----------------------------------------------------------------------------
		constructor forum()
		private  get_article_threads($article_id)
		function get_count_threads()
		
		function get_thread($id)
		function add_thread($data, $filename='')
		function quote_string($string, $split_length=40)
		function quote_headline($string)
		
		function html_threads_shortlist($article_id, $detail_uri='')
		function html_threads_list($article_id, $current_id='', $detail_uri='')
		
		TODO: check_table() + create_table, variable db-fields...
		----------------------------------------------------------------------------
		HISTORY:
		1.0 / 2004-03-30
	*/
	
	// global vars
	var $count_threads = 0;
	var $table = '';
	var $db; // DB-Object
	var $count_level = 0; // tatsaechliche verschachtelungs-tiefe
	var $max_level = 0; // maximale verschachtelungs-tiefe
	// icons
	var $spacer = ''; // zum einruecken
	var $icon_empty = ''; // eintrag ohne kinder
	var $icon_plus = ''; // eintrag mit kinder


/**
* Konstruktor -> Initialisiert das forum-object
*
* Beispiel:
* <pre><code>
* $oForum =& new forum($aDBvars);
* </code></pre>
*
* @param	array		$aDBvars		DB-Zugangsdaten (dbconnect-konform)
* @return	void
* @access   public
*/
	function forum($aDBvars) {
		// create db-object
		$this->db = new dbconnect($aDBvars);
		// set global defauts
		$this->table		= "forum";
		$this->max_level	= 20; // nur zur sicherheit!
		$this->sSearchtext		= '';
		// icons
		$this->spacer		= '<img src="onepix.gif" width="12" height="12" border="0" alt="">';
		$this->icon_empty	= '<img src="btn_here.gif" alt="" class="btnsmall">';
		$this->icon_plus	= '<img src="btn_show.gif" alt="" class="btnsmall">';
		$this->icon_minus	= '<img src="btn_hide.gif" alt="" class="btnsmall">';
		// pruefe ob table vorhanden
		#$this->_checkTable();
	}

/** Erstell-Funktion: checke ob DB-Table vorhanden, wenn nicht, starte Funktion zum Erstellen
* 
* @access   private
*/
	function _checkTable() {
		if (!$this->oDb) return;
		$tables = $this->oDb->get_tables();
		if (!empty($this->sTable) && !in_array($this->sTable, $tables)) {
			$this->_createTable();
		}
	}

/** Erstell-Funktion: erstelle DB-Table(s)
* 
* @access   private
*/
	function _createTable() {
		if (empty($this->sTable)) return;
		$q = "CREATE TABLE `".$this->sTable."` ( 
			  `id` int(11) NOT NULL auto_increment,
			  `article_id` int(11) default NULL,
			  `parent_id` int(11) default NULL,
			  `headline` varchar(150) default NULL,
			  `content` text,
			  `keywords` varchar(250) default NULL,
			  `downloads` text,
			  `searchtext` text,
			  `is_active` enum('0','1') NOT NULL default '1',
			  `created` varchar(14) default NULL,
			  `created_by` smallint(5) default NULL,
			  `last_modified` timestamp(14) NOT NULL,
			  `last_mod_by` smallint(5) default NULL,
			  PRIMARY KEY  (`id`)
			) TYPE=MyISAM;";
		$this->oDb->query($q);
	}


//------------------------------------------------------- methoden


/** 
* INTERN: ermittle alle eintraege dieses artikels (select by article_id [,parent_id])
* 
* @param	int		$article_id		ID des Artikels
* @param	int		$parent_id		parent-ID des kommentars
* @param	int		$level			verschachtelungstiefe
* @return	array
* @access   private
*/
	function get_article_threads($article_id, $parent_id=0, $level=0) {
		// vars
		if (!$article_id) return;
		if ($level == 0) { $buffer = array(); }
		// Verschachtelungstiefe tracken
		if ($level > $this->count_level) { $this->count_level = $level; }
		// query
		$this->db->query("SELECT * FROM `".$this->table."` 
						WHERE `article_id`=".$article_id." 
							AND `parent_id`=".$parent_id." 
							AND `is_active`='1' 
						ORDER BY `created` DESC");
		while ($tmp = $this->db->fetch_array()) {
			$buffer[] = $tmp;
		}
		// und rekursiv weiterhangeln...
		for ($i = 0; $i< count($buffer); $i++) {
			// ... aber nur wenn noch nicht die maximale verschachtelungstiefe erreicht ist
			if ($this->count_level < $this->max_level) {
				$buffer[$i]['children'] = $this->get_article_threads($article_id, $buffer[$i]['id'], ++$level); $level--;
			}
		}
		return $buffer;
	}


/** 
* ermittle gesamtanzahl freigegebener eintraege
* 
* <pre><code> 
* echo $oForum->get_count_threads(); // params: --
* </code></pre>
* 
* @return	int		gesamt-anzahl eintraege
* @access   public
*/
	function count_all_threads() {
		$this->db->query("SELECT `id` FROM `".$this->table."` WHERE `is_active`='1'");
		$this->count_threads = $this->db->num_rows();
		return $this->count_threads;
	}


//------------------------------------------------------- Single Entry


/** 
* ermittle aktuellen eintrag (select by id)
* 
* <pre><code> 
* $data = $oForum->get_entry(127); // params: $id
* print_r($data);
* </code></pre>
* 
* @param	int		$id		ID des kommentars
* @return	array
* @access   public
*/
	function get_entry($id) {
		// vars
		if (!$id) return;
		$buffer = array();
		$this->db->query("SELECT * FROM `".$this->table."` WHERE `id`=".$id);
		if ($this->db->num_rows() < 1) return;
		$row = $this->db->fetch_array();
		// buffer nur fuellen wenn freigegeben
		if ($row['is_active'] == 1) {
			$buffer['id'] = $id;
			$buffer['article_id'] = $row['article_id'];
			$buffer['parent_id'] = $row['parent_id'];
			$buffer['headline'] = $row['headline'];
			$buffer['content'] = $row['content'];
			$buffer['created'] = $row['created'];
			$buffer['is_active'] = $row['is_active'];
			// zusatzvariable: has children?
			$this->db->query("SELECT `id` FROM `".$this->table."` WHERE `parent_id`=".$id);
			$buffer['has_children'] = ($this->db->num_rows()) ? 1 : 0;
		}
		return $buffer;
	}

/** 
* fuer forum abstrahierter make insert (by article_id [,parent_id])
* 
* Beispiel: 
* <pre><code> 
* $data = array();
* $data['article_id'] = 1;
* $data['parent_id'] = 3;
* $data['headline'] = "test-comment 2.1";
* $data['content'] = "test kommentar test text 2.1";
* $id = $oForum->add_entry($data, 'forum_overview.php');
* echo "kommentar mit der ID $id eingefuegt.";
* </code></pre>
* 
* @param	array	$data			zu speichernder datensatz (mindestens: $data['article_id']!)
* @param	string	[$filename]		filename(s) der im cache zu loeschenden seiten (optional - default: '')
* @return	int		ID des eingefuegten datensatzes
* @access   public
*/
	function add_entry($data, $filename='') {
		if ($data['article_id']) {
			if (!$data['parent_id']) { $data['parent_id'] = 0; } #''
			// set datetime
			$data['created'] = date("YmdHis");
			// set online by default
			if (!$data['is_active']) { $data['is_active'] = "1"; }
			$this->db->make_insert($data, $this->table);
			
			return $this->db->insert_id();
		} else {
			echo "<script>alert('Keine article_id! Kein insert moeglich!');</script>";
		}
	}

/** 
* gebe einen text "zitiert" zurueck (also nach $split_length-Zeichen (+ nach dem naechsten 
* leerzeichen) umgebrochen und mit '>'-Zitatzeichen am Anfang jeder Zeile).
* 
* <pre><code> 
* echo "&lt;textarea>".$oForum->quote_string($data['content'])."</textarea>";
* </code></pre>
* 
* @param	string		$string
* @param	int			[$split_length]		anzahl zeichen nach denen umgebrochen wird (optional - default: 40)
* @return	string
* @access   public
*/
	function quote_string($string, $split_length=40) {
		// vars
		if (!$string) return;
		$nl = "\n";
		$quote_chr = '>';
		$newstring = '';
		$tmp = array();
		$str_array = explode($nl, $string);
		foreach ($str_array as $key => $val) {
			if (substr($val, 0, 1) == $quote_chr) {
				// schon zitiert -> also nur $quote_chr davor!
				$tmp[] = $quote_chr.$val;
			} else {
				// generate wordwrap
				$newstring = wordwrap($val, $split_length, $nl);
				$str_array = explode($nl, $newstring);
				$newstring = implode($nl.$quote_chr.' ', $str_array);
				$tmp[] = $quote_chr.' '.$newstring;
			}
		}
		return implode($nl, $tmp);
	}

/** 
* gebe eine headline "zitiert" zurueck (also mit 'Re:' davor).
* 
* <pre><code> 
* echo '&lt;inut value="'.$oForum->quote_headline($data['headline']).'">';
* </code></pre>
* 
* @param	string		$string
* @return	string
* @access   public
*/
	function quote_headline($string) {
		// vars
		if (!$string) return;
		return 'Re: '.$string;
	}


//------------------------------------------------------- HTML-Listen


/** 
* ermittle nur erstes level eintraege dieses artikels (aber finde heraus ob jeder eintrag threads hat -> icon!)
* und gebe sie direkt in HTML aus.
* 
* <pre><code> 
* $oForum->html_threads_shortlist(127, 'forum_detail.php?navi_id=12'); // params: $article_id[,$detail_uri='']
* </code></pre>
* 
* @param	int		$article_id
* @param	string	[$detail_uri]		href zur detailseite (optional - default: $_SERVER['PHP_SELF'])
* @return	HTML
* @access   public
*/
	function html_threads_shortlist($article_id, $detail_uri='') {
		// vars
		if (!$article_id) return;
		if (empty($detail_uri)) { $detail_uri = $_SERVER['PHP_SELF']; }
		$detail_uri .= (strstr($detail_uri,'?')) ? "&" : "?";
		// get tree data
		$this->max_level = 2;
		$arr = $this->get_article_threads($article_id);
		// generate output
		foreach ($arr as $key => $val) {
			// link
			$tmp = '<a href="'.$detail_uri.'article_id='.$val['article_id'].'&id='.$val['id'].'">';
			// icon
			$tmp .= (isset($val['children']) && count($val['children']) > 0) ? $this->icon_plus : $this->icon_empty;
			// headline
			$tmp .= $val['headline'].'</a>';
			// output
			echo $tmp.'<br>'."\n";
		}
	}

/** 
* ermittle alle level eintraege dieses artikels -> komplette baumstruktur
* und gebe sie direkt in HTML aus.
* 
* <pre><code> 
* $oForum->html_threads_list(127); // params: $article_id[,$current_id=''][,$detail_uri='']
* </code></pre>
* 
* @param	int		$article_id
* @param	int		[$current_id]		id eines ggf. aktuell ausgewaehlten eintrags (optional - default: '')
* @param	string	[$detail_uri]		href zur detailseite (optional - default: $_SERVER['PHP_SELF'])
* @param	array	[$arr]				INTERNE VARIABLE - nicht benutzen!
* @param	int		[$level]			INTERNE VARIABLE - nicht benutzen!
* @return	HTML
* @access   public
*/
	function html_threads_list($article_id, $current_id='', $detail_uri='', $arr='', $level=0) {
		// vars
		if (!$article_id) return;
		if (empty($detail_uri)) { $detail_uri = $_SERVER['PHP_SELF']; }
		$class = (!empty($css_class)) ? ' class="'.$css_class.'"' : '';
		if ($level == 0) { $detail_uri .= (strstr($detail_uri,'?')) ? "&" : "?"; }
		// get tree data
		if ($arr == '') { $arr = $this->get_article_threads($article_id); }
		// generate output
		foreach ($arr as $key => $val) {
			// einrueckung
			$tmp = str_repeat($this->spacer, $level); 
			// link
			$tmp .= '<a href="'.$detail_uri.'article_id='.$val['article_id'].'&id='.$val['id'].'">';
			// icon
			$tmp .= (isset($val['children']) && count($val['children']) > 0) ? $this->icon_minus : $this->icon_empty;
			// headline
			$tmp .= $val['headline'].'</a>';
			// highlight
			if (!empty($current_id) && $current_id == $val['id']) {
				$tmp = '<b>'.$tmp.'</b>'; // aktiver eintrag wird bold!
			}
			// output
			echo $tmp.'<br>'."\n";
			
			// und rekursiv weiterhangeln...
			if (count($val['children'])) {
				$this->html_threads_list($article_id, $current_id, $detail_uri, $val['children'], ++$level); $level--;
			}
		}
	}

} // END of class
?>