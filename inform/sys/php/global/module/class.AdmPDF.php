<?php
	class AdmPDF {
		
		// arrays
		var $aAdm		= null;
		var $Userdata	= null;
		var $aPrj		= null;
		var $aMsg		= '';
		var $aCurrency	= null;
		var $jobcat		= null;
		var $aCurrent	= null;
		
		// int
		var $nSitebreak	= null;
		var $nCurrentY	= null;
		
		// string
		var $syslang	= 'de';
		var $savename	= '';
		var $mode		= 'proposal';
		var $sPageType	= '';
		
		// object
		var $oDate		= null;
		
		// boolean
		var $saveonserver	= true;
		var $bSum	= false;
		var $bTotal	= false;
		var $bResume= false;
		var $bIntroduction= false;
		
		function admPDF($aENV, $Userdata, $syslang, $aMSG, $mode='proposal') {
		
			$this->aENV		= $aENV;
			$this->Userdata	= $Userdata;
			$this->syslang	= $syslang;
			$this->aMSG		= $aMSG;
			$this->mode		= $mode;
			
			// initialise datetime class
			$this->oDate	= new datetime_db();
			$method			= 'set_lang_to_'.$this->syslang;
			$this->oDate->$method();
		}
		
		function initialize($jobcat, $aPrj) {
			$this->aCurrency	= array();
			$this->aCurrency['de']['euro']	= chr(128); //chr(128)
			$this->aCurrency['de']['pound']	= chr(163);
			$this->aCurrency['de']['dollar']	= chr(36);
			$this->aCurrency['en']['euro']	= chr(128); //chr(128)
			$this->aCurrency['en']['pound']	= chr(163);
			$this->aCurrency['en']['dollar']	= chr(36);
			
			$this->jobcat	= $jobcat;
			$this->aPrj		= $this->setProjectData($aPrj);
		}
		
		function setProjectData($aPrj) {
			$aTemp	= array();
			
			switch(true) {
				case ($aTemp['discount'] == 0 
				&& $aTemp['extprice'] == 0):
					$this->nSitebreak = 30;
				break;
					
				case ($aTemp['discount'] != 0 
				&& $aTemp['extprice'] != 0):
					$this->nSitebreak = 80;
				break;
					
				case ($aTemp['discount'] != 0):
					$this->nSitebreak = 50;
				break;
					
				case ($aTemp['extprice'] != 0):
					$this->nSitebreak = 50;
				break;
			}
			
			foreach($aPrj as $sKey => $mValue) {
			
				// if mValue is an array do magic
				if(is_array($mValue)) {
			
					switch($sKey) {
						case 'price':
							foreach($mValue as $nKey => $sValue) {
								$price	= utf8_decode($this->aCurrency[$this->syslang][$aPrj['currency']]);
			
								if($aPrj['parent_id'] != 0 
								&& $aPrj['dunning_letter'] == 0) 
									$price .= ' -';
								$price	.= ' '.$sValue;

								if(!empty($sValue) 
								&& $sValue != '0,00'  
								&& $sValue != '0.00') 
									$aTemp['priceBlock'][$sKey][$nKey]	= $price;
							}
						break;
						default:
							$aTemp['priceBlock'][$sKey]	= $mValue;
					} // end switch sKey
					
				} else { // else if mValue is not an array
						
					switch($sKey) {
						case 'sum':
							$sum	= utf8_decode($this->aCurrency[$this->syslang][$aPrj['currency']]);
							if($aPrj['parent_id'] != 0 
							&& $aPrj['dunning_letter'] == 0) 
								$sum	.= ' -';
							
							$sum	.= ' '.format_money($aPrj['sum'], $this->syslang);
							$aTemp['sum_format']	= $sum;
							$aTemp['sum']	= $aPrj['sum'];
						break;
													
						default:
							$aTemp[$sKey]	= $mValue;
					} // end switch

				} // end if
				
			} // end foreach
//			$this->debug($aTemp);
			return $aTemp;
		}
		
		/**
		 * Soll ein Deckblatt mit generiert werden?
		 * @param boolean $bWithDeckblatt
		 * @access public
		 * @return void
		 */
		function setDeckblatt($bWithDeckblatt) {
			$this->bWithDeckblatt	= $bWithDeckblatt;
		}
		
		/**
		 * Diese Methode setzt die Konfigurationsparameter
		 * @param array $aAdm
		 * @return void
		 * @access public
		 */
		function setAdm($aAdm) {
			$this->aAdm	= $aAdm;
		}
		
		/**
		 * Diese Methode setzt die Konfigurationsparameter
		 * @param array $aAdm
		 * @return void
		 * @access public
		 */
		function setMode($sMode) {
			$this->mode	= $sMode;
		}		
		
		function printPdf() {
			$pdf = &File_PDF::factory(array('orientation' => 'P','unit' => 'mm','format' => 'A4'));

  			$pdf->open();
			$pdf->setAutoPageBreak(false, 0);
			  			
  //			$cfonts	= $pdf->_getCorefonts();
	  		$cfonts	= $pdf->_core_fonts;
  			$iscore = false;

			foreach ($cfonts as $key) {
  			
  				if($key == $this->aAdm['master']['fontface']['normal']) {
  					$iscore = true;
  					break;
  				}
  				
  			} // end foreach
  			
  			// if the choosen font isnt standard, it is included 
  			if(!$iscore) {
  				$pdf->path = $this->aENV['path']['pear']['unix'].'PDF/fonts/';
  			
  				$pdf->addFont($this->aAdm['master']['fontface']['normal'], '', $this->aENV['path']['pear']['unix'].'PDF/fonts/'.$this->aAdm['master']['fontface']['normal'].'.php');
  				$pdf->addFont($this->aAdm['master']['fontface']['normal'], 'B', $this->aENV['path']['pear']['unix'].'PDF/fonts/'.$this->aAdm['master']['fontface']['bold'].'.php');
  			}
  			
  			$fontsize	= $this->aAdm['master']['fontsize']['text'];
			$fontweight	= "";
			$fontfamily	= $this->aAdm['master']['fontface']['normal'];
			
			$pdf->setFont($fontfamily, $fontweight, $fontsize);
 			
			$pdf	=& $this->printTemplate($pdf);

			// if save pdf on server is true
  			if($this->saveonserver) {
  				$sPrjnr	= str_replace('.', '', $this->aPrj['projectNumber']);
  				
  				$oFile	= new filesystem();

  				$sFilename	= utf8_encode($sPrjnr."_".$this->aPrj['projectName'].".pdf");
				
				// array with replace tokens
				$aTranslate	= array('ä'	=> "ae", 
									'ö'	=> "oe",
									'ü'	=> "ue",
									'Ä'	=> "ae", 
									'Ö'	=> "oe",
									'Ü'	=> "ue",									
									'ß'	=> "ss",
									" "	=> "_",
									"..."	=> "");
						
				// replace action						
				$sFilename	= strtr($sFilename, $aTranslate);
				
  				$this->savename	= $oFile->return_good_filename($sFilename);

				if(!is_dir($this->aENV['path']['adm_data']['unix'].$this->mode)) { 
					mkdir($this->aENV['path']['adm_data']['unix'].$this->mode, 0777);
				}

  				$pdf->save($this->aENV['path']['adm_data']['unix'].$this->mode.'/'.$this->savename);
  			} // end if

		} // end method
			
		function debug($aArray, $sString='') {
			echo (!empty($sString)) ? "<b>$sString</b>":'';
			echo "<pre>";
			print_r($aArray);
			echo "</pre>";
		}
		
		function Header($pdf) {
			$aTempAdm	= $this->aCurrent;
			$aPrj		= $this->aPrj;
			$aMaster	= $this->aAdm['master'];
			
			if(!empty($aTempAdm['headerImg']['url']))					
				$pdf->image($aTempAdm['headerImg']['url'], $aTempAdm['headerImg']['x'], $aTempAdm['headerImg']['y'], $aTempAdm['headerImg']['width'], $aTempAdm['headerImg']['height']);
			
			// if there is a footer image, write it to the pdf
			if(!empty($aTempAdm['footerImg']['url']))					
				$pdf->image($aTempAdm['footerImg']['url'], $aTempAdm['footerImg']['x'], $aTempAdm['footerImg']['y'], $aTempAdm['footerImg']['width'], $aTempAdm['footerImg']['height']);			
			
			$aPrj['paging']	= $pdf->getPageNo(); //$this->aMSG['adm']['site'][$this->syslang].' '.
			
			switch($aTempAdm['date']['format']) { 
				case 'short':
					$sMethod	= date_mysql2trad;
				break;
									
				case 'shortshort':
					$sMethod	= date_mysql2tradshort;
				break;
				
				default:
					$sMethod	= date_long_from_isodate;
			}
		
			$aPrj['date']			= $this->oDate->$sMethod($aPrj['date']);				
			
			foreach($aTempAdm['header'] as $sKey2) {
													
				if($aTempAdm[$sKey2]['display'] == false) continue;

				$pdf->setX($aTempAdm[$sKey2]['x']);
				
				if(!empty($aTempAdm[$sKey2]['y']))
					$pdf->setY($aTempAdm[$sKey2]['y']);
		
				$fontsize	= (empty($aTempAdm[$sKey2]['fontsize'])) ? $aMaster['fontsize']['text']:$aTempAdm[$sKey2]['fontsize'];
				$fontweight	= (empty($aTempAdm[$sKey2]['fontweight'])) ? "":($aTempAdm[$sKey2]['fontweight']=='bold') ? "B":"";
				$fontfamily	= $aMaster['fontface']['normal'];
				
				$pdf->setFont($fontfamily, $fontweight, $fontsize);

				if(isset($aTempAdm[$sKey2]['method']) 
				&& !empty($aTempAdm[$sKey2]['method'])) {
					$pdf	= $this->$aTempAdm[$sKey2]['method']($pdf);
					continue;	
				}
				
				$height	= ($aTempAdm[$sKey2]['lineheight']!=0)	? $aTempAdm[$sKey2]['lineheight']:$aMaster['lineheight']['text'];
				$valign	= (empty($aTempAdm[$sKey2]['align']))	? 'L':$aTempAdm[$sKey2]['align'];
				
				$text	= trim($aPrj[$sKey2]);
																										
				if(isset($aTempAdm[$sKey2]['prefix']) 
				&& !empty($aTempAdm[$sKey2]['prefix']))
					$text	= $aTempAdm[$sKey2]['prefix'].$text;
					
				if(isset($aTempAdm[$sKey2]['suffix']) 
				&& !empty($aTempAdm[$sKey2]['suffix']))
					$text	.= $aTempAdm[$sKey2]['suffix'];

				$maxwidth	= $pdf->fw-($aTempAdm['margin']['right']+$aTempAdm['margin']['left']);
				$width		= ceil($pdf->getStringWidth($text))+2;
				if($maxwidth<$width)	$width	= $maxwidth;
				
				$pdf->multiCell($width, $height, $text, 0, $valign);
					
			} // end foreach
			
			// return the pdf object
			return $pdf; 
		}
		
		function Footer($pdf) {
			// if there is no footer, return the pdf object
			if(!$this->aAdm['displayfooter']) return $pdf;
			
			// set temp array to current format array
			$aTempAdm	= $this->aCurrent;
			
			// set x position for the cursor
			$pdf->setX($aTempAdm['footer']['x']);
			// set y position for the cursor
			$pdf->setY($pdf->fh-$aTempAdm['margin']['bottom']);
		
			// set font to normal
			$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
			$pdf->setFont($this->aAdm['master']['fontface']['normal'], '', $fontsize);
		
			// write the author to the footer
			$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text']; 
			$pdf->Write($lh, $this->aMSG['adm'][$this->mode.'author'][$this->syslang].$this->Userdata['firstname'].' '.$this->Userdata['surname'].'.');
			
			// return the pdf object
			return $pdf;
		}
 		
 		/**
 		 * add's a new page to the PDF
 		 * 
 		 * @param object the PDF Object
 		 * @return object the PDF Object
 		 */
 		
 		function addPage($pdf) {
 			// make a new page in fpdf
			$pdf->addPage();
			
			// if this is the first page in pdf, set sPageType to startpage
			if(empty($this->sPageType)) {
				$this->sPageType	= 'startpage';
			} elseif($this->sPageType=='startpage') {
				$this->sPageType	= 'followpage';
			}
			
			// set the current format array to the matching page format array
			$this->aCurrent	= $this->aAdm[$this->sPageType];
			
			$fontsize	= $this->aAdm['master']['fontsize']['text'];
			$fontweight	= "";
			$fontfamily	= $this->aAdm['master']['fontface']['normal'];
			
			$pdf->setFont($fontfamily, $fontweight, $fontsize);	
			
			// set the header and the footer for the page
			$pdf	= $this->Header($pdf);			
			$pdf	= $this->Footer($pdf);
			
			// set left & right margin			
			$pdf->setRightMargin($this->aCurrent['margin']['right']);
			$pdf->setLeftMargin($this->aCurrent['margin']['left']);
			
			// set the cursor to x & y position
			$pdf->setX($this->aCurrent['margin']['left']);
			$pdf->setY($this->aCurrent['margin']['top']);			
			
			// return the pdf object
			return $pdf; 			
 		}
 		
		function printTemplate($pdf) {
			
			$aTempA	= array('startpage'		=> $this->aAdm['startpage'],
							'followpage'	=> $this->aAdm['followpage']);
							
			$this->aCurrent		= $this->aAdm['startpage'];
			
			$pdf		= $this->addPage($pdf);
					
			$aMaster	= $this->aAdm['master'];
					
			foreach($aTempA as $sKey => $aValue) {
				$aPrj	= $this->aPrj;
				$aAdm	= $this->aAdm[$sKey];
				
				$this->sPageType	= $sKey;
				$this->aCurrent		= $this->aAdm[$sKey];
				
				foreach($aValue['content'] as $sKey2) {
							
					if($aValue[$sKey2]['display'] == false) continue;

					$pdf->setX($aValue[$sKey2]['x']);
					
					if(!empty($aValue[$sKey2]['y']))
						$pdf->setY($aValue[$sKey2]['y']);
						
					if(isset($aValue[$sKey2]['method']) 
					&& !empty($aValue[$sKey2]['method'])) {
						$pdf	= $this->$aValue[$sKey2]['method']($pdf);
						continue;	
					}
								
					$height	= ($aValue[$sKey2]['lineheight']!=0)	? $aValue[$sKey2]['lineheight']:$aMaster['lineheight']['text'];
					$valign	= (empty($aValue[$sKey2]['align']))		? 'L':$aValue[$sKey2]['align'];
					
					$text	= trim($aPrj[$sKey2]);
					$text	= str_replace("\t", "", $text);

					if(isset($aValue[$sKey2]['prefix']) 
					&& !empty($aValue[$sKey2]['prefix']))
						$text	= $aValue[$sKey2]['prefix'].$text;
						
					if(isset($aValue[$sKey2]['suffix']) 
					&& !empty($aValue[$sKey2]['suffix']))
						$text	.= $aValue[$sKey2]['suffix'];
						
					if(empty($text)) continue;
					
					$bTemp	= "b".strtolower($sKey2);
					
					if(isset($this->$bTemp) 
					&& $this->$bTemp==true) continue;

					$fontsize	= (empty($aValue[$sKey2]['fontsize'])) ? $aMaster['fontsize']['text']:$aValue[$sKey2]['fontsize'];
					$fontweight	= (empty($aValue[$sKey2]['fontweight'])) ? "":($aValue[$sKey2]['fontweight']=='bold') ? "B":"";
					$fontfamily	= $aMaster['fontface']['normal'];

					$pdf->setFont($fontfamily, $fontweight, $fontsize);
														
					$nStringHeight	= count(explode("\n", $text));
					$maxheight		= ceil($pdf->getY() 
									+ ($this->nSitebreak 
									+ ($nStringHeight*$height)));
					
					if($maxheight>$pdf->fh)
						$pdf	= $this->addPage($pdf);
						
					$maxwidth	= $pdf->fw-($aValue['margin']['right']+$aValue['margin']['left']);
					$width		= $pdf->getStringWidth($text)+5;

					if($maxwidth<$width)	$width	= $maxwidth;
					
					$pdf->multiCell($width, $height, $text, 0, $valign);
					$pdf->newLine();
					
					$this->$bTemp	= true;	
				} // end foreach
				
			} // end foreach

			// return the object
			return $pdf;
		}		
		
		/**
		 * writes the introduction to the pdf, calculates lineheight, and makes
		 * new pages if needed
		 * 
		 * @param object $pdf the PDF Object
		 * @return object $pdf the PDF Object
		 */
		function getIntroduction($pdf) {
			
			// if boolean flag is true, return pdf object
			if($this->bIntroduction) return $pdf;

			// get current format array
			$aTempAdm	= $this->aCurrent;
			// get introduction format array from current format array
			$aTempB		= $aTempAdm['introduction'];
			// get introduction text from project array
			$sTempI		= $this->aPrj['introduction'];
			
			if(empty($this->aPrj['introduction'])) { 
				// if there is a seperate firstpage, make a new page
				if($this->aAdm['seperatefirstpage']) {
					$pdf	= $this->addPage($pdf);
				}
			
				return $pdf; 
			}
			
			$fontsize	= (!empty($aTempB['fontsize'])) ? $aTempB['fontsize']:$this->aAdm['master']['fontsize']['text'];
			$fontweight	= (!empty($aTempB['fontweight'])) ? $aTempB['fontweight']:"";
			$fontfamily	= $this->aAdm['master']['fontface']['normal'];
			
			$height	= ($aTempB['lineheight']!=0)	? $aTempB['lineheight']:$aTempAdm['lineheight']['text'];
			$valign	= (empty($aTempB['align']))		? 'L':$aTempB['align'];

			//$width	= $pdf->fw-$aTempAdm['margin']['right']; nh 12.11.07
			$width	= $pdf->fw-($aTempAdm['margin']['right']+$aTempAdm['margin']['left']);
			// wir berechnen die maximale anzahl zeichen die in die Zeile passen könnte, anhand von leerzeichen 
			// kann dazu führen das der Text nicht korrekt umbricht, da m breiter ist als " " und dem entsprechend
			// die Zeile evtl.doch nicht reinpasst.
			$letter	= ceil($width / ($pdf->_current_font['cw'][' '] * $pdf->_font_size / 1000));
			$sTempI	= wordwrap( $sTempI, $letter, "\n", true );
			$aTempI	= explode("\n",$sTempI);
			// foreach line in introduction, write the line		
			foreach($aTempI as $sText) {			
				
				// calculate estimate lineheight
				$sText	= trim($sText);
				$sText	= str_replace("\t", "", $sText);
				
				$maxheight		= ceil($pdf->getY() 
								+ ($aTempAdm['margin']['bottom']
								+ ($height*3)));

				// if estimated lineheight greater than total page height, make a new page
				if($maxheight>$pdf->fh) {
					$pdf	= $this->addPage($pdf);
				}
					
				// write the line	
				$pdf->setFont($fontfamily, "", $fontsize);					
				$pdf->setX($aTempAdm['margin']['left']);

				$pdf->multiCell($width, $height, $sText, 0, $valign);
			}
			
			$pdf->newLine();
			
			// if there is a seperate firstpage, make a new page
			if($this->aAdm['seperatefirstpage'])
				$pdf	= $this->addPage($pdf);
				
			// return pdf object	
			return $pdf;
		}
		
		function getPriceBlock($pdf) {
				if($this->bSum) return $pdf;
				
				// read current format array
				$aTempAdm	= $this->aCurrent;
				// read priceBlock array from format array
				$aTempB		= $aTempAdm['priceBlock'];
				// read priceBlock array from project array
				$aTempP		= $this->aPrj['priceBlock'];

				// set fontsize/fontweight & fontfamily from master format array
				$fontsize	= $this->aAdm['master']['fontsize']['text'];
				$fontweight	= "";
				$fontfamily	= $this->aAdm['master']['fontface']['normal'];
				
				$height	= ($aTempB['lineheight']!=0)	? $aTempB['lineheight']:$aTempAdm['lineheight']['text'];
				$valign	= (empty($aTempB['align']))		? 'L':$aTempB['align'];
				
				// foreach price Block entrie in project array, write the block
				foreach($aTempP['hd'] as $nKey => $aText) {
					
					// calculate estimate stringheight for price block
					$width	= $pdf->fw-$aTempAdm['margin']['right']-50;
					$width	= ceil($width / ($pdf->_current_font['cw'][' '] * $pdf->_font_size / 1000));

					$text	= wordwrap( $aTempP['text'][$nKey], $width, "\n" );
					$nStringHeight	= count(explode("\n",$text));
					
					$maxheight		= ceil($pdf->getY() 
									+ ($aTempAdm['margin']['bottom']
									+ ($height*3) 
									+ ($nStringHeight*$height)));
					
					// if estimated height greater than total page height, make a new page
					if($maxheight>$pdf->fh)
						$pdf	= $this->addPage($pdf);
					
					// write title
					$x		= $pdf->getX();
					$valign	= (empty($aTempAdm['align']))	? 'L':$aTempB['align'];
					$text	= $aTempP['hd'][$nKey];
					$width	= ceil($pdf->getStringWidth($text))+20;
				
					$pdf->setFont($fontfamily, "B", $fontsize);
					$pdf->multiCell($width, $height, $text, 0, $valign);
						
					// write Text
					if (!empty($aTempP['text'][$nKey])) {
						$pdf->setX($x);
						$text	= $aTempP['text'][$nKey];
						$width	= $pdf->fw-$aTempAdm['margin']['right']-50;
						
						$pdf->setFont($fontfamily, "", $fontsize);
						$pdf->multiCell($width, $height, $text, 0, $valign);
						$y	= $pdf->getY()-$height;
					}
					
					$afterTextY	= $pdf->getY();
					$y	= $pdf->getY()-$height;
					// write mandays/hours
					if (!empty($aTempP['mt'][$nKey])) {
						$pdf->setX($x);						
						$text	= $aTempP['mt'][$nKey];
						$y		= $pdf->getY();	
						$width	= 0;
						$valign	= "L";
						$pdf->multiCell($width, $height, $text, 0, $valign);
					}
					
					// write price	
					$pdf->setY($y);	
					
					$text	= $aTempP['price'][$nKey];
					$width	= 0;
					$valign	= "R";
					$pdf->multiCell($width, $height, $text, 0, $valign);
			
					if (empty($aTempP['mt'][$nKey])) {
						$pdf->setY($afterTextY);
					}

					if(count($aTempP['hd']) > $nKey+1) {
						// make linebreak					
						$pdf->newLine($aTempB['lineheight']);
					}
					
				}

				if(!empty($aTempB['y_spacing'])
				|| $aTempB['y_spacing']>0) {
					$pdf->setY($pdf->getY()+$aTempB['y_spacing']);
				}

				// set boolean flag to true
				$this->bSum	= true;
				
				// return pdf object
				return $pdf;
		}
		/**
		 * Diese Methode generiert die Abrechnung unter der Rechnung bzw. dem Angebot
		 * @param object $pdf
		 * @return $pdf
		 * @access private
		 */
		function getSum($pdf) {
			if($this->bTotal) return $pdf;
			
			$aTempAdm	= $this->aCurrent;
			$aTempBlock	= $this->aCurrent['total'];
			
			if(empty($aTempAdm['lineheight']['text'])) {
				$aTempAdm['lineheight']['text']		= $this->aAdm['master']['lineheight']['text'];
			}
				
			if(empty($aTempBlock['lineheight']['text'])) {
				$aTempBlock['lineheight']['text']	= $this->aAdm['master']['lineheight']['text'];
			}
				
			$sitelen	= $pdf->getY();
			$sitelen	+= ($this->aPrj['discount']!=0 && $this->aPrj['extprice']!=0) ? $aTempBlock['lineheight']['text']*3:0;
			$sitelen	+= ($this->aPrj['discount']!=0 && $this->aPrj['sum']!=0) ? $aTempBlock['lineheight']['text']*3:0;
			$sitelen	+= ($this->aPrj['extprice']!=0) ? $aTempBlock['lineheight']['text']*3:0;			
			$sitelen	+= (($this->mode == 'invoice' || $this->mode == 'dunning')&& $this->aPrj['tax'] != 0) ? $aTempBlock['lineheight']['text']*4:0;
			$sitelen	+= $this->nSitebreak;
			
			if($pdf->fh<$sitelen) {
				$pdf	= $this->addPage($pdf);
			}
			
			if(((!empty($this->aPrj['discount']) || $this->aPrj['discount'] != 0)  
			&& (!empty($this->aPrj['sum']) && $this->aPrj['sum'] != 0)) 
			|| (!empty($this->aPrj['extprice']) 
			|| $this->aPrj['extprice'] != 0)) {
				
				$pdf->setX($aTempAdm['margin']['left']);
				$pdf->setY($pdf->getY()+($aTempAdm['lineheight']['text']));

				// set font bold
				$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
				$height		= (!empty($aTempAdm['lineheight']['text']))? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
				$pdf->setFont($this->aAdm['master']['fontface']['normal'], 'B', $fontsize);
				$pdf->write($height, $this->aMSG['adm']['fees'][$this->syslang]);
				
				$pdf->setX($aTempAdm['margin']['left']);
				$pdf->setY($pdf->getY() + $aTempAdm['lineheight']['text']);
		
				$sum	= utf8_decode($this->aCurrency[$this->syslang][$this->aPrj['currency']]);
				if($this->aPrj['parent_id'] != 0 
				&& $this->aPrj['dunning_letter'] == 0) 
					$sum	.= ' -';
				
				$sum	.= ' '.format_money($this->aPrj['sum'], $this->syslang);
				
				// write the sum 
				$pdf->multiCell(0, -$aTempAdm['lineheight']['text'], $sum, 0, 'R');
				
				// if bWithLine is true, write a line
				if($this->aAdm['bWithLine']) {
					$pdf->line($aTempAdm['margin']['left'], ($pdf->getY()-2), ($pdf->fw-$aTempAdm['margin']['right']), ($pdf->getY()-2));
				} // end if
				
			} // end if
			
			// if discount is true and sum is true
			if((!empty($this->aPrj['discount']) || $this->aPrj['discount'] != 0) 
			&& (!empty($this->aPrj['sum']) && $this->aPrj['sum'] != 0)) {
				
				$pdf->setX($aTempAdm['margin']['left']);
				$pdf->setY($pdf->getY() + $aTempAdm['lineheight']['text']);

				$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
				$height		= (!empty($aTempAdm['lineheight']['text']))? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
				$pdf->setFont($this->aAdm['master']['fontface']['normal'], '', $fontsize);

				// write discount to pdf
				$pdf->write($height, '- '.$this->aPrj['discount'].'% '.$this->aMSG['adm']['discount'][$this->syslang]);
				
				$this->aPrj['sum']	= $this->aPrj['sum'] - ($this->aPrj['sum'] * ($this->aPrj['discount'] / 100));
				
				$pdf->setX($aTempAdm['margin']['left']);
				$pdf->setY($pdf->getY()+$aTempAdm['lineheight']['text']);
				
				$sum	= utf8_decode($this->aCurrency[$this->syslang][$this->aPrj['currency']]);
				if($this->aPrj['parent_id'] != 0 
				&& $this->aPrj['dunning_letter'] == 0) 
					$sum	.= ' -';
				
				$sum	.= ' '.format_money($this->aPrj['sum'], $this->syslang);
				
				// write the sum to pdf, align right
				$pdf->multiCell(0, -$height, $sum, 0, 'R');
				$pdf->setY($pdf->getY() + $aTempAdm['lineheight']['text']);
			
			} // end if
			
			if(!empty($this->aPrj['extprice']) 
			|| $this->aPrj['extprice'] != 0) {
							
				$pdf->setX($aTempAdm['margin']['left']);
				
				// if discount is true && sum is true do a linebreak
				if((!empty($this->aPrj['discount']) || $this->aPrj['discount'] != 0) 
				&& (!empty($this->aPrj['sum']) && $this->aPrj['sum'] != 0)) {
					$pdf->setY($pdf->getY()+($aTempAdm['lineheight']['text']));
				} else { // else do two linebreaks
					$pdf->setY($pdf->getY()+($aTempAdm['lineheight']['text']*2));
				} // end if discount is true && sum is true
				
				//set font to bold
				$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
				$height		= (!empty($aTempAdm['lineheight']['text']))? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
				$pdf->setFont($this->aAdm['master']['fontface']['normal'],'B',$fontsize);

				// write extern to pdf
				$pdf->write($height,$this->aMSG['adm']['flag_extern'][$this->syslang]);

				// set font to normal
				$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];								
				$pdf->setFont($this->aAdm['master']['fontface']['normal'],'',$fontsize);
				
				$pdf->setY($pdf->getY());
				
				$extcost	= $this->aPrj['handling']/100*$this->aPrj['extprice'];
				$sum	= utf8_decode($this->aCurrency[$this->syslang][$this->aPrj['currency']]);
				
				if($this->aPrj['parent_id'] != 0 
				&& $this->aPrj['dunning_letter'] == 0) 
					$sum	.= ' -';
					
				$sum	.= ' '.format_money($this->aPrj['extprice'],$this->syslang);
				
				// write the extern cost to the pdf, align right
				$pdf->multiCell(0,$aTempAdm['lineheight']['text'], $sum, 0, 'R');

				$pdf->setX($aTempAdm['margin']['left']);

				// write our handling fee to pdf
				$pdf->write($aTempAdm['lineheight']['text'],'+ '.$this->aPrj['handling'].'% '.$this->aMSG['adm']['handling'][$this->syslang]);
				
				$pdf->setY($pdf->getY());

				// set font to bold
				$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
				$pdf->setFont($this->aAdm['master']['fontface']['normal'],'B',$fontsize);
				
				$sum	= utf8_decode($this->aCurrency[$this->syslang][$this->aPrj['currency']]);
				
				if($this->aPrj['parent_id'] != 0 
				&& $this->aPrj['dunning_letter'] == 0) 
					$sum	.= ' -';
					
				$sum	.= ' '.format_money(($extcost+$this->aPrj['extprice']),$this->syslang);

				// write 				
				$pdf->multiCell(0, $aTempAdm['lineheight']['text'], $sum, 0, 'R');
				
				// add external cost to the sum
				$this->aPrj['sum']	+= $extcost + $this->aPrj['extprice'];
			} // end if
			
			$pdf->setX($aTempAdm['margin']['left']);

			// make a linebreak
			$pdf->setY($pdf->getY() + $aTempAdm['lineheight']['text']);
			
			// set font to bold			
			$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
			$pdf->setFont($this->aAdm['master']['fontface']['normal'], 'B', $fontsize);

			// if mode is invoice make estimate total
			switch($this->mode) {
				case 'dunning':
				case 'invoice':
					$sMode	= 'total';
				break;
				case 'proposal':
					$sMode	= 'estimate';
				break;
			}

			// write estimate to pdf
			$pdf->write($aTempAdm['lineheight']['text'], $this->aMSG['adm'][$sMode][$this->syslang]);
			$pdf->setY($pdf->getY());

			$sum	= utf8_decode($this->aCurrency[$this->syslang][$this->aPrj['currency']]);
			
			if($this->aPrj['parent_id'] != 0 
			&& $this->aPrj['dunning_letter'] == 0) 
				$sum	.= ' -';
				
			$sum	.= ' '.format_money($this->aPrj['sum'], $this->syslang);
			
			// write sum to pdf
			$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
			$pdf->multiCell(0, $lh, $sum, 0, 'R');

			// if bWithLine write a line
			if($this->aAdm['bWithLine']) {
				$pdf->line($aTempAdm['margin']['left'], ($pdf->getY()-($aTempAdm['lineheight']['text']*1.5)),($pdf->fw-$aTempAdm['margin']['right']),($pdf->getY()-($aTempAdm['lineheight']['text']*1.5)));
			} // end if
			
			// if mode is invoice && tax is not 0
			if(($this->mode == 'invoice'
			|| $this->mode == 'dunning') 
			&& $this->aPrj['tax'] != 0) {
				
				// set font to normal
				$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
				$pdf->setFont($this->aAdm['master']['fontface']['normal'], '', $fontsize);
				
				// {tax},{taxMsg}
				$sTax	= str_replace(array('{tax}','{taxMsg}'), array($this->aPrj['tax'], $this->aMSG['adm']['tax'][$this->syslang]), $this->aAdm['text']['tax']);
				
				$pdf->setX($aTempAdm['margin']['left']);

				// write taxmessage to pdf
				$pdf->write($aTempAdm['lineheight']['text'], $sTax);
				
				$pdf->setY($pdf->getY());
				
				$taxedsum	= $this->aPrj['sum']*($this->aPrj['tax'] / 100);
				$sum		= utf8_decode($this->aCurrency[$this->syslang][$this->aPrj['currency']]);
				
				if($this->aPrj['parent_id'] != 0 
				&& $this->aPrj['dunning_letter'] == 0) 
					$sum	.= ' -';
					
				$sum	.= ' '.format_money($taxedsum, $this->syslang);

				// write taxsum to pdf
				$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];				
				$pdf->multiCell(0, $lh, $sum, 0, 'R');
			
				// make a linebreak
				$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
				$pdf->setY($pdf->getY()+$lh);
				
				// add taxsum to total sum
				$this->aPrj['sum']	+= $taxedsum;
				
				// set font to bold
				$fontsize	= (!empty($aTempAdm['fontsize']['text'])) ? $aTempAdm['fontsize']['text']:$this->aAdm['master']['fontsize']['text'];
				$pdf->setFont($this->aAdm['master']['fontface']['normal'], 'B', $fontsize);
				
				$pdf->setX($aTempAdm['margin']['left']);

				// write label for total to pdf				
				$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
				$pdf->write($lh, $this->aMSG['adm']['sum'][$this->syslang]);
				
				$pdf->setY($pdf->getY());
				
				$sum	= ($this->aPrj['currency']!='dollar') ? utf8_decode($this->aCurrency[$this->syslang][$this->aPrj['currency']]):$this->aCurrency[$this->syslang][$this->aPrj['currency']];

				if($this->aPrj['parent_id'] != 0 
				&& $this->aPrj['dunning_letter'] == 0) 
					$sum	.= ' -';
					
				$sum	.= ' '.format_money($this->aPrj['sum'], $this->syslang);
				
				// write total to pdf
				$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
				$pdf->multiCell(0, $lh, $sum, 0, 'R');
				
				// make a linebreak
				$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
				$pdf->setY($pdf->getY()+$lh);
				
				// if bWithLine is true, write a line
				if($this->aAdm['bWithLine']) {
					$lh	= (!empty($aTempAdm['lineheight']['text'])) ? $aTempAdm['lineheight']['text']:$this->aAdm['master']['lineheight']['text'];
					$pdf->line($aTempAdm['margin']['left'], ($pdf->getY()-($lh*2.5)), ($pdf->fw-$aTempAdm['margin']['right']),($pdf->getY()-($lh*2.5)));
				} // end if with line
				
			} // end if
			
			if(!empty($aTempBlock['y_spacing'])
			|| $aTempBlock['y_spacing']>0) {
				$pdf->setY($pdf->getY()+$aTempBlock['y_spacing']);
			}			
			
			$this->bTotal	= true;
			
			// return pdf object
			return $pdf;
		}
		
		/**
		 * Gibt den Savename zurück. Also den neuen PDF Namen.
		 * @return string
		 * @access public
		 */
		function getSavename() {
			return $this->savename;	
		}
	}
?>