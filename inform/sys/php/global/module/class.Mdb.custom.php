<?php
/**
* Diese Klasse stellt allgemeine Methoden für den MDB-Bereich zur Verfügung.
*
* @access	public
* @package	Content
* @uses		class.download.php
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.2 / 2006-03-27 [NEU: public function setMsieDownloadsRedirect()]
*/
class Mdb {
	
	var $aENV = null;
	var $oDb = null;
	
	var $oSess = null;
	var $oFile = null;
	var $oDate = null;
	var $oTree = null;
	
	var $Userdata = null;
	var $searchMDir = 0;
	var $searchMType = '';
	var $searchMTerm = '';
	
	var $aAuthedTreeIds = null;
	
	var $entries = 0;
	
/**
* Konstruktor
*
* @access	public
* @param	array	$aENV		(als Referenz)
* @param	object	$oDb		(als Referenz)
* @return	void
*/
	function Mdb(&$aENV,&$oDb) {
		$this->oDb =& $oDb;
		$this->aENV =& $aENV;
	}

# SETTER-Methoden -----------------------------------------------------------------------------

	function setDate($oDate) {
		$this->oDate=&$oDate;
	}

	function setFile($oFile) {
		$this->oFile = $oFile;
	}

	function setSess($oSess) {
		$this->oSess = $oSess;
	}

	function setTree($oTree) {
		$this->oTree = $oTree;
	}

	function setUserdata($Userdata) {
		$this->Userdata = $Userdata;
	}

	function setSearchMDir($searchMDir) {
		$this->searchMDir = $searchMDir;
	}

	function setSearchMType($searchMType) {
		$this->searchMType = $searchMType;
	}

	function setSearchMTerm($searchMTerm) {
		$this->searchMTerm = $searchMTerm;
	}

# GETTER-Methoden -----------------------------------------------------------------------------

	function getEntries() {
		return $this->entries;
	}

# OVERVIEW-PAGE -----------------------------------------------------------------------------

/**
* Baut komplettes Dropdown der zur Verfuegung stehenden mediatypes 
* (kann mittels Setter-Methode setSearchMType() gehilightet werden).
*
* Beispiel:
* <pre><code>
* echo $oMdb->getMediatypesDropdown($aMSG['mdb']['all_filetypes'][$syslang]); // params: [$sAllTxt='']
* </code></pre>
*
* @access	public
* @param	string	$sAllTxt	wenn uebergeben, wird als erste option "all" angezeigt. (optional => default: '')
* @return	html-code
*/
	function getMediaTypesDropdown($sAllTxt='', $sShow='all', $bWithCount=true, $onlyAuthed=false) {
		// vars
		$buffer = '';
		$aMediatype = array();
		// Ausgabe ggf. einschraenken
		if ($sShow != 'all') {
			$aShow = explode(',', $sShow);
			if (count($aShow) == 1) return $buffer; // wenn nur EIN mediatype uebergeben wurde -> KEINE AUSGABE!
		}
		// get mimetype->filetype array
		$aMimetype = mediadb::getMimetypeFiletypeArray();

		// verwendete mimetypes ermitteln
		$sql = "SELECT `filetype` 
				FROM `".$this->aENV['table']['mdb_media']."`";
		// ggf. auf only-authed einschraenken
		if ($onlyAuthed) {
			$aAuthedFolder = $this->oTree->getAllAuthedIds();
			if (is_array($aAuthedFolder) && count($aAuthedFolder) > 0)  {
				$sAuthedFolder = implode(',',$aAuthedFolder);			
				$sAuthedFolder .= ",0"; // -1 = upload, 0 = unsorted
				if (basename($_SERVER['PHP_SELF']) == 'mdb.php') {
					$sAuthedFolder .= ",-1";	// "upload" NUR in der MDB-overview "mdb.php"
				}
				$sql .= " WHERE `tree_id` IN(".$sAuthedFolder.")";
			}			
		}

		$this->oDb->query($sql);
		while ($tmp = $this->oDb->fetch_array()) {
			// mimetype -> filetype (mediatype) wandeln
			$mediatype = $aMimetype[$tmp['filetype']];
			if (empty($mediatype)) $mediatype = 'file';
			// zeige ggf. nur die uebergebenen
			if (is_array($aShow) && !in_array($mediatype, $aShow)) { continue; }
			// sammeln
			if (!array_key_exists($mediatype, $aMediatype)) {
				$aMediatype[$mediatype] = 1;
			} else {
				$aMediatype[$mediatype] ++;
			}
		}
		// wenn nur EIN mediatype ermittelt wurde -> KEINE AUSGABE!
		if (count($aMediatype) == 1) return $buffer;
		
		// build select-field
		$buffer = '<select name="searchMType" size="1" class="smallForm">';
		if (!empty($sAllTxt)) {
			$sel = ($this->searchMType == "all") ? ' selected' : '';
			$buffer .= '<option value="all"'.$sel.'>'.$sAllTxt.'</option>';
		}
		foreach ($aMediatype as $key => $val) {
			if ($key == 'default' || $key == '') continue;
			$sel = ($this->searchMType == $key) ? ' selected' : '';
			$buffer .= '<option value="'.$key.'"'.$sel.'>'.$key;
			if ($bWithCount) $buffer .= ' ('.$val.')';
			$buffer .= '</option>';
		}
		$buffer .= '</select>'."\n";
		
		return $buffer;
	}

/**
* Baut komplettes Dropdown der zur Verfuegung Folder 
* (kann mittels Setter-Methode setSearchMDir() gehilightet werden).
*
* Beispiel:
* <pre><code>
* echo $oMdb->getMediadirsDropdown(true);
* </code></pre>
*
* @access	public
* @param	boolean	$bWithUploadFolder	wenn true, wird als erste option "upload_folder" angezeigt. (optional => default: false)
* @param	string	$sName		(optional, default: 'searchMDir')
* @return	html-code
*/
	function getMediaDirsDropdown($bWithUploadFolder=false, $bWithAllFolder=true, $sName='searchMDir') {
		// vars
		global $aMSG, $syslang, $oPerm;
		
		// set tree vars
		if ($bWithUploadFolder) {
			if ($this->searchMDir == -1 || $oPerm->hasPriv('admin')) {
				$this->oTree->addOption(array('-1' => $aMSG['media']['upload_folder'][$syslang]), false);
				$this->oTree->addOption(array('0' => '---------------------'), false); // Trenner
			}
		} else {
			$this->oTree->removeOption(array('-1' => $aMSG['media']['upload_folder'][$syslang]));
			$this->oTree->removeOption(array('0' => '---------------------'));
		}
		if ($bWithAllFolder) {
			$this->oTree->setAllName($aMSG['media']['all_folders'][$syslang]);
		} else {
			$this->oTree->unsetAllName();
		}
		$this->oTree->setRootOff();
		$this->oTree->addOption(array('0' => $aMSG['media']['root_folder'][$syslang]), false);
		
		// output
		return $this->oTree->treeFormSelect($sName, $this->searchMDir);
	}

/** 
* Empfaengt alle Thread-IDs, die fuer den aktuellen User freigeschaltet sind.
* 
* <pre><code> 
* $oMdb->setAuthedThreads($aTreeId); // params: $aTreeId
* </code></pre>
* 
* @access   public
* @param	array	$aTreeId		IDs der fuer den aktuellen User autorisierten Tree-Datensaetze
* @return	array
*/
	function authThreads($aTreeId) {
		if (is_array($aTreeId)) {
			$this->aAuthedTreeIds = $aTreeId;
			array_unshift($this->aAuthedTreeIds, '0');
		}
		return $this->aAuthedTreeIds;
	}
	
/**
* Selectiert alle gewuenschten mediafiles.
*
* Beispiel:
* <pre><code>
* echo $oMdb->select($start, $limit, $mustfit, $width, $height);
* </code></pre>
*
* @access	public
* @param	int		$start		(optional, default: 0)
* @param	int		$limit		(optional, default: 0)
* @param	int		$width		(optional, default: '')
* @param	int		$height		(optional, default: '')
* @param	int		$mustfit	([0|1], default: 0)
* @return	array (key: ID; val: assioz.-array(fieldname->value))
*/
	function select($start=0, $limit=0, $bWithUploadDir=false, $width='', $height='', $mustfit=0) {
		// vars
		global $aMSG, $syslang;

		$buffer = array();
		// set tree vars
		$this->oTree->setRootName($aMSG['media']['root_folder'][$syslang]); // f.d. path!
		
		// build query
		$sql = 'SELECT `m`.`id`,`m`.`name`,`m`.`filename`,`m`.`filetype`,`m`.`description`,`m`.`width`,`m`.`height`,`m`.`filesize`,`m`.`tree_id` 
				FROM `'.$this->aENV['table']['mdb_media'].'` AS `m` 
				LEFT JOIN `mdb_media_data`AS `d` ON `m`.`id`=`d`.`media_id`
				WHERE ';
		// wenn tree-dir eingeschraenkt wurde
		if (isset($this->searchMDir) && !empty($this->searchMDir)) {
			
			if ($this->searchMDir == 'all') {
				// bei 'all' ggf. nur die fuer den aktuellen user erlaubten datensaetze anzeigen
				if (count($this->aAuthedTreeIds) > 0) {
					$sql .= ' `m`.`tree_id` IN ('.implode(',', $this->aAuthedTreeIds).')';
				}
				else {
					// ALLE folder ODER ALLE folder ausser "upload"
					$sql .= ($bWithUploadDir) ? '1' : " `m`.`tree_id` != '-1' ";
				}
			} else {
				// alle Unterordner ermitteln
				$aChildrenId = $this->oTree->getAllChildrenId($this->searchMDir);
				$aChildrenId[] = $this->searchMDir; // + sich selbst auch dazu
				// einen bestimmten folder (inkl. aller unterordner)
				$sql .= " `m`.`tree_id` IN (".implode(",", $aChildrenId).") ";
			}
		} else {
			$sql .= " `m`.`tree_id` = '0' ";
		}

		// wenn filetype eingeschraenkt wurde
		if ($this->searchMType != 'all' && $this->searchMType != '') {
			if ($this->searchMType != 'file') {
				$aFiletype = mediadb::getFiletypeMimetypeArray();#debug($aFiletype);
				$sql .= " AND ( ";
				$c = count($aFiletype[$this->searchMType]['mimetype']);
				for ($i=0; $i < $c; $i++) {
					if ($i > 0 && $i < $c) $sql .= " OR ";
					$sql .= " `m`.`filetype` = '".$aFiletype[$this->searchMType]['mimetype'][$i]."' ";
				}
				$sql .= " ) ";
			} else {
				$sql .= " AND (`m`.`filetype` = '' OR `m`.`filetype` is NULL OR `m`.`filetype` = 'application/octet-stream') ";
			}
		}

		// wenn width uebergegeben wurde
		if (!empty($width) && $width > 0) {
			$operator = ($mustfit == 1) ? '=' : '<='; // width = maxwidth oder fixwidth?
			$sql .= ' AND `m`.`width` '.$operator.' '.$width.' ';
		}

		// wenn height uebergegeben wurde
		if (!empty($height) && $height > 0) {
			$operator = ($mustfit == 1) ? '=' : '<=';// height = maxheight oder fixheight?
			$sql .= ' AND `m`.`height` '.$operator.' '.$height.' ';
		}

		// wenn suchbegriff eingegeben wurde
		if (isset($this->searchMTerm) && $this->searchMTerm != '') {
			$sql .= " AND ( (`m`.`description` LIKE '%".$this->searchMTerm."%') 
						OR (`m`.`keywords` LIKE '%".$this->searchMTerm."%') 
						OR (`m`.`name` LIKE '%".$this->searchMTerm."%') 
						OR (`d`.`work_title` LIKE '%".$this->searchMTerm."%') 
						OR (`d`.`work_medium` LIKE '%".$this->searchMTerm."%') 
						OR (`d`.`work_year` LIKE '%".$this->searchMTerm."%')
						OR (`d`.`work_size` LIKE '%".$this->searchMTerm."%') 
						OR (`d`.`work_conditions` LIKE '%".$this->searchMTerm."%') 
						OR (`d`.`work_owner` LIKE '%".$this->searchMTerm."%') 
						OR (`d`.`work_price` LIKE '%".$this->searchMTerm."%') 
						OR (`d`.`work_comment` LIKE '%".$this->searchMTerm."%')
					 ) ";
			$sql .= ' ORDER BY `m`.`name` ASC';
		} else {
			$sql .= ' ORDER BY `m`.`last_modified` DESC';
		}
		
		// doit
		$this->oDb->query($sql);
		$this->entries = floor($this->oDb->num_rows());	// noetig fuer DB-Navi und PRIO!
		if ($limit > 0 && $this->entries > $limit) {
			$this->oDb->limit($sql, $start, $limit);
		}
		while ($aData = $this->oDb->fetch_array()) {
		
			// sammle data
			$buffer[$aData['id']] = $aData;
		
			// zusaetzliche info: path
			$path = '';
			$aParentId = $this->oTree->getAllParentId($aData['tree_id']); #print_r($aParentId); // DEBUG
			$aParentId[] = $aData['tree_id']; // + sich selbst auch dazu
		
			foreach ($aParentId as $pId) {
				if ($pId == -1) {
					$path .= ''.$aMSG['media']['upload_folder'][$syslang].'/';
				} elseif ($pId > 0) {
					$path .= ''.$this->oTree->getValueById($pId, "title").'/';
				} else {
					$path .= ''.$this->oTree->sRootName.'/';
				}
			}
			$buffer[$aData['id']]['path'] = $path;
		}

		// output
		return $buffer;
	}

/**
* Hilfsfunktion fuer Checkbox/Sammel-Aktionen
*
* @access	private
* @param	array	$aData		(Checkboxen)
* @return	array	Aufbereitetes Array
*/
	// Hilfsfunktion fuer Checkbox/Sammel-Aktionen
	function _getPostVals($aData) {
		foreach($aData as $key => $val) {
			if(strstr($key,"mvdel")) {
				if(!empty($val)) {
					$kname = str_replace("mvdel","",$key);
					$vals[] = $kname;
				}
			}
		}
		return $vals;
	}
/**
* Checkbox/Sammel-Aktion: delete
*
* @access	public
* @param	array	$aData		(Checkboxen)
* @return	void
*/
	function doBatchDelete($aData) {
		
		$vals = $this->_getPostVals($aData);
		$iCountvals = count($vals);
		
		for($i=0; $iCountvals > $i; $i++) {
			// vars aufbereiten
			$aDataToDelete = array();
			$aDataToDelete['id'] = $vals[$i];
			$rs = $this->oDb->fetch_by_id($vals[$i], $this->aENV['table']['mdb_media']);
			$aDataToDelete['filename'] = $rs['filename'];
			// delete
			$this->delete($aDataToDelete);
		}
	}
/**
* Checkbox/Sammel-Aktion: update
*
* @access	public
* @param	array	$aData		(Checkboxen)
* @param	string	$movetoMDir	(Tree-ID)
* @return	void
*/
	function doBatchUpdate($aData, $movetoMDir) {
		
		// MOVE TO DIR -> $moveToDir
		$vals = $this->_getPostVals($aData);
		$iCountvals = count($vals);
		
		for($i=0; $iCountvals > $i; $i++) {
			$sql = "UPDATE ".$this->aENV['table']['mdb_media']." 
					SET tree_id='".$movetoMDir."' 
					WHERE id='".$vals[$i]."'";
			$this->oDb->query($sql);
		}
	}


# DETAIL-PAGE -----------------------------------------------------------------------------

/**
* Loescht ein Mediafile aus der DB und aus dem Filesystem (ggf. inkl. Thumb).
*
* @access	public
* @param	array	$aData (mit min. 'id' + 'filename'!)
* @return	void
*/
	function delete($aData) {
		if (empty($aData['id'])) return false;
		// delete file -> ...'unix' muss genommen werden, weil sonst kein file geschrieben werden kann
		$this->oFile->delete_files($this->aENV['path']['mdb_upload']['unix'].$aData['filename']);
		// ggf. delete thumb -> bei automatischer thumb-generierung muss auch dieses geloescht werden!
		$this->oFile->delete_files($this->aENV['path']['mdb_upload_cache']['unix'].$aData['filename']);
		// delete data in db
		$this->oDb->make_delete($aData['id'], $this->aENV['table']['mdb_media']);
	}

/**
* Speichert ein Mediafile in die DB und in das Filesystem.
*
* @access	public
* @param	array	$aData
* @param	array	$_FILES
* @param	array	$syslang
* @param	array	$compare_filetype (optional, default: )
* @return	void
*/
	function save($aData,$_FILES,$syslang,$compare_filetype='', $unique_filename=true) {
		// ggf. file hochladen
		if (is_array($_FILES) && $_FILES['uploadfile']['name'] != '') {
			$aData = $this->uploadFileInMDB($aData,$_FILES,$syslang,$compare_filetype, $unique_filename);
		}
		if(is_array($aData)) {
			// ggf. DB action
			if (!$aData['bCloseAfterSave']) {
				$bCloseAfterSave = $aData['bCloseAfterSave'];
				unset($aData['bCloseAfterSave']);
				
				// Update eines bestehenden Eintrags
				if ($aData['id']) { 
					unset($aData['extract']);
					$aData['last_mod_by'] = $this->Userdata['id'];
					$aUpdate['id'] = $aData['id'];
					$this->oDb->make_update($aData, $this->aENV['table']['mdb_media'], $aUpdate);
				}
				// Insert eines neuen Eintrags 
				else { 
					
					if (!$aData['filename']) {
						echo js_alert("Please upload a file!"); $alert=true;
						if(isset($aData['filename'])) unset($aData['filename']);
						if(isset($aData['id']) && empty($aData['id'])) unset($aData['id']);
					} else {
						$aData['created'] = $this->oDate->get_mysql_timestamp();
						$aData['created_by'] = $this->Userdata['id'];
						$this->oDb->make_insert($aData, $this->aENV['table']['mdb_media']);
						$aData['id'] = $this->oDb->insert_id();
						
						// SPEZIAL: aendere ggf. SESSION-einstellung
						if ($this->searchMDir != $aData['tree_id']) {
							$this->oSess->set_var('searchMDir', $aData['tree_id']);
						}
					}
				}
				$aData['bCloseAfterSave'] = $bCloseAfterSave;
			}
			return $aData;
		}
		else {
			return false;
		}
	}

/**
* Hilfsfunktion fuer save(): Kapselt den Upload.
*
* @access	public
* @param	array	$aData
* @param	array	$_FILES
* @param	array	$syslang
* @param	array	$compare_filetype (optional, default: )
* @return	void
*/
	function uploadFileInMDB($aData, $_FILES, $syslang, $compare_filetype='', $unique_filename=true) {
		// make good filename bugfix nh 18.01.07
		$_FILES['uploadfile']['name']	= preg_replace('/[^a-z0-9_\-\.]/i', '_', $_FILES['uploadfile']['name']);
		
		// upload new file
		if (isset($_FILES['uploadfile']['name']) 
		&& !empty($_FILES['uploadfile']['name'])) {
			// register file to delete
			if ($aData['id']) { $sFileToDelete	= $aData['filename']; }
			
			// get additional file infos
			$aData['name']		= $_FILES['uploadfile']['name'];
			$aData['filesize']	= $_FILES['uploadfile']['size'];
			$aData['filetype']	= $_FILES['uploadfile']['type'];
			
			// ggf. mimetype korrigieren
			if ($aData['filetype'] == 'image/pjpeg') 		{ $aData['filetype'] = 'image/jpeg'; } // NO progressive JPEGs [... IE-Bug (PC only): always sets filetype to progressive... ]
			if ($aData['filetype'] == 'image/x-png') 		{ $aData['filetype'] = 'image/png'; }
			if ($aData['filetype'] == 'application/x-gzip') { $aData['filetype'] = 'application/gzip'; }
			if ($aData['filetype'] == 'application/x-zip-compressed') { $aData['filetype'] = 'application/zip'; }
			if (strstr($aData['name'], ".flv")) { $aData['filetype'] = 'video/x-flv'; }
			if (strstr($aData['name'], ".wmv")) { $aData['filetype'] = 'video/x-ms-wmv'; }
			if ($aData['filetype'] == 'audio/mpg') { $aData['filetype'] = 'audio/mpeg'; }
		
			// upload
			$oUpload	=& new upload("uploadfile", $unique_filename); // params: $name[,$unique_filename=true]
			$oUpload->set_syslang($syslang); // params: $syslang
			
			if (!($tmp = $oUpload->make_upload($this->aENV['path']['mdb_upload']['unix']))) {

				$oUpload->script_alert();
				$alert	= true;
			}
			// error: kein File!
			if (empty($tmp)) {
				echo js_alert("Please upload a file!");
				$alert	= true;
			}
			
			// alles weitere NUR nach erfolgreichem upload!
			if ($alert) { // deshalb hier ggf. aussteigen!
				$aData['bCloseAfterSave']	= false;
				return $aData;
			}
			
			// ggf. add. image-infos erst nach upload möglich, wegen neuer PHP Uploadspeichervariante
			if (strstr($aData['filetype'], "image") || strstr($aData['filetype'], "flash") || strstr($aData['filetype'], "flv")) {
				$imgsize	= GetImageSize($this->aENV['path']['mdb_upload']['unix'].$tmp);
				
				$aData['width']		= $imgsize[0];
				$aData['height']	= $imgsize[1];
			}			
		}
		
		// virtuelle (i.d. DB nicht existierende) vars umschichten und loeschen
		$bExtract	= (isset($aData['extract']) && $aData['extract'] == 1) ? true : false;
		if (isset($aData['extract'])) unset($aData['extract']);
		
		// ggf. Zip extrahieren
		if ($bExtract && strtolower(substr($aData['name'],-3)) == 'zip') {
			// entpacke zip
			if($this->extractZip($oUpload->filename,$aData)) {
				// action-anweisung an aufrufende funktion uebergeben
				$aData['bCloseAfterSave'] = true;
			}
			else {
				return false;
			}
			// loesche zip
			$this->oFile->delete_files($this->aENV['path']['mdb_upload']['unix'].$oUpload->filename);
			
		} else { // sonst upload fortsetzen
			// ggf. delete old file nur NACH nach erfolgreichem upload!
			if (isset($sFileToDelete) 
			&& !empty($sFileToDelete)) {
				$this->oFile->delete_files($this->aENV['path']['mdb_upload']['unix'].$sFileToDelete); // ...'unix' muss genommen werden, weil sonst kein file geschrieben werden kann
				$this->oFile->delete_files($this->aENV['path']['mdb_upload_cache']['unix'].$sFileToDelete); // bei automatischer thumb-generierung muss auch dieses geloescht werden!
			}
			// filename beibehalten, wenn "replace" && filetype ist gleich
			if (isset($tmp) && !empty($tmp) && isset($aData['filename']) && $compare_filetype == $aData['filetype']) {
				rename($this->aENV['path']['mdb_upload']['unix'].$tmp, $this->aENV['path']['mdb_upload']['unix'].$aData['filename']);
			} else {
				$aData['filename'] = $tmp;
			}
			// action-anweisung an aufrufende funktion uebergeben
			$aData['bCloseAfterSave'] = false;
		}
		return $aData;
	}

/**
* Hilfsfunktion fuer uploadFileInMDB(): Extrahiert ein ZIP mittels PCLZIP-Klasse.
*
* @access	public
* @param	string	$sFilename	(zu extrahierendes File)
* @param	array	$aData
* @return	void
*/
	function extractZip($sFilename,$aData) {
		
		// neue instanz der Zip lib mit hinweis auf unser Zipfile
		$zip =& new PclZip($this->aENV['path']['mdb_upload']['unix'].$sFilename);
			
		// extract-directory -> temp fuer jeden User unabhaengig
		$exdir = $this->aENV['path']['mdb_upload']['unix'].'temp'.session_id().'/';

		// ok, extrahieren und alle Pfadangaben rauswerfen!
		$extract = $zip->extract($exdir, PCLZIP_OPT_REMOVE_ALL_PATH);

		if ($extract > 0) { 
			// Temp Verzeichnis auslesen
			$handle	= opendir($exdir);
			while ($file = readdir ($handle)) {
				// Ordner ueberspringen
				if ($file == "." || $file == "..") continue;
				
				if ($file == '__MACOSX') {
					rmdir($exdir.'/'.$file);
					continue;
				}

				// Wenn ein Verzeichnis in dem Zip existiert, wird das Tempverzeichnis gelöscht und die darin enthaltenen Dateien (security)
				if (is_dir($exdir.'/'.$file)) {
					$this->oFile->recursivFolderDel($exdir.'/'.$file);
					echo js_alert("Zip contains folder! Upload aborted.");
					return false;
				}
				// make good filename
				$tmpfile	= $file; // original name merken!
				$file		= preg_replace('/[^a-z0-9_\-\.]/i', '_', $file);

				// extension extrahieren
				$fileext	= substr($file,-3);
				
				// file-details ermitteln
				$aData['name']		= $file;
				$aData['filesize']	= filesize($exdir.'/'.$tmpfile);
				$aData['filetype']	= download::getMimeType($tmpfile);
					
				$imgsize	= getimagesize($exdir.'/'.$tmpfile);
				if (!empty($imgsize[2])) {
					$aData['width']		= $imgsize[0];
					$aData['height']	= $imgsize[1];
				}
							
				// Wenn die Datei nicht existiert koennen wir den Namen beibehalten... 
	   			// ansonsten wird ein Zufallsgeneriertes Kuerzel angehaengt (bsp. test.jpg vorhanden => test6egr5v5.jpg)
				if (file_exists($this->aENV['path']['mdb_upload']['unix'].$tmpfile)) {
					$md5		= substr(md5(microtime()), 0, 8);
					$namepart	= str_replace('.'.$fileext, '', $file);
					$file		= $namepart.'.'.$md5.'.'.$fileext;
				}
				
				// Verschiebt die Datei aus dem Tempdir in das Media-Verzeichnis
				$chk	= rename($exdir.'/'.$tmpfile, $this->aENV['path']['mdb_upload']['unix'].$file);
				$aData['filename']	= $file;
				
				// Insert eines neuen Eintrags
				$aData['created']		= $this->oDate->get_mysql_timestamp();
				$aData['created_by']	= $this->Userdata['id'];
				$this->oDb->make_insert($aData, $this->aENV['table']['mdb_media']);
				
				// SPEZIAL: aendere ggf. SESSION-einstellung
				if ($this->searchMDir != $aData['tree_id']) { 
					$this->oSess->set_var('searchMDir', $aData['tree_id']); 
				}
			} // END while
			
			closedir($handle); 
			rmdir($exdir);
		}

		return true;
	}

/**
* Hilfsfunktion die vor delete() aufgerufen wird: Prueft auf Verwendung des uebergebenen mediafiles.
*
* @access	public
* @param	int		$media_id
* @global	string	$syslang
* @global	array	$aMSG
* @return	string	leer, wenn ok; sonst fehlermeldung!
*/
	function checkMedia($media_id) {
		global $syslang, $aMSG;
		
		// tabellen + felder, die durchsucht werden sollen, wenn ein bild aus der mediendb geloescht wird
		// (ACHTUNG: mit einer variable zusammengestzte feldbezeichner (z.B.'title_'.$weblang) gehen hier nicht!!!)
		// key: tablename, val: fieldname (mehrere kommasepariert)
		// NOTE: die Tabellen/Felder des CMS muessen in der setup konfiguriert werden!!!
		
		// Felder in denen nur eine EINZELNE id vorkommt:
		#$aENV['check_single_media'] - array ergaenzen!
		$this->aENV['check_single_media']['pms_content_1']	= "image";
		$this->aENV['check_single_media']['pms_content_2']	= "image";
		$this->aENV['check_single_media']['pms_content_3']	= "image";
		$this->aENV['check_single_media']['pms_content_4']	= "image";
		$this->aENV['check_single_media']['pms_content_5']	= "image";
		$this->aENV['check_single_media']['pms_content_6']	= "image";
		$this->aENV['check_single_media']['pms_content_7']	= "image";
		$this->aENV['check_single_media']['pms_content_8']	= "image";
		$this->aENV['check_single_media']['pms_content_9']	= "image";
		$this->aENV['check_single_media']['pms_content_10']	= "image";
		
		// felder in denen MEHRERE kommagetrennte ids vorkommen:
		#$aENV['check_multiple_media'] - array ergaenzen!
		$this->aENV['check_multiple_media']['pms_content_1']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_2']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_3']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_4']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_5']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_6']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_7']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_8']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_9']	= "slideshow, multipix";
		$this->aENV['check_multiple_media']['pms_content_10']	= "slideshow, multipix";
		#this->$aENV['check_multiple_media']['pms_archive']		= "used_media"; // <-- muesste noch gespeichert werden - Feld gibt es noch nicht!
		#this->$aENV['check_multiple_media']['cms_projectsite']	= "downloads"; // <-- muesste noch getrennt werden!
	
		#echo $oMdb->check_media(15); // TEST!!!
		
		
		// check tables
		$buffer1 = $this->checkSingleMedia($media_id);
		#$buffer2 = _check_multiple_media($media_id);
		#$buffer = array_merge($buffer1, $buffer2);	#print_r($buffer);
		
		// wenn NUR eine unestimmte Fehlermeldung kommt, reicht es wenn das script abbricht, wenn eines! gefunden wurde!
		// "_check_multiple_media()" muss also wenn schon ein fehler gefunden wurde gar nicht mehr ausgefuehrt werden.
		if (count($buffer1) == 0) {
			$buffer2 = $this->checkMultipleMedia($media_id);
			$buffer = array_merge($buffer1, $buffer2);	#print_r($buffer);
		} else {
			$buffer = $buffer1;
		}
		// wenn kein match
		if (count($buffer) == 0) return;
		// wenn match
		$alert = $aMSG['err']['delete_media'][$syslang]."\\n"; // ZWEI slashes!!!
		
		/* TODO: hier evtl. noch verstaendliche suchergebnisse produzieren?
		foreach ($buffer as $table => $val) {
			$template = (isset($this->aENV['template'][$table][$syslang]) && !empty($this->aENV['int_linkman'][$table][$syslang])) 
					? $this->aENV['template'][$table][$syslang] 
					: $aMSG['template'][$table][$syslang];
			$field = (isset($this->aENV['int_linkman'][$table]['fieldname']) && !empty($this->aENV['int_linkman'][$table]['fieldname'])) 
					? $this->aENV['int_linkman'][$table]['fieldname'] 
					: "title_".$syslang;
			$this->oDb->query("SELECT id, ".$field." FROM ".$table." WHERE id='".key($val)."'");
			$title = ($this->oDb->num_rows()) ? $this->oDb->fetch_field($field) : key($val); // title oder id als fallback
			$alert .= "Template ".$template.": ".$title."\n";
		}*/
		
		return $alert;
	}
/**
* Hilfsfunktion fuer checkMedia(): Prueft DB-Felder mit nur einem Wert.
*
* @access	public
* @param	int		$media_id
* @return	array	leer, wenn ok; sonst felder, in denen das file verwendet wird!
*/
	function checkSingleMedia($media_id) {
	
		if (!is_array($this->aENV['check_single_media'])) return;
		$buffer = array();
		
		// gehe durch alle tables
		foreach ($this->aENV['check_single_media'] as $table => $fields) {
			// baue WHERE bedingung
			$aFields = explode(',', $fields);
			$aWhere = array();
			foreach ($aFields as $k => $field) { // mit = alle datensetze holen die exakt in frage kommen
				$aWhere[] = trim($field)." = '".trim($media_id)."'";
			}
			// DB query
			$this->oDb->query("SELECT id, ".$fields." FROM ".$table." WHERE ".implode(' OR ', $aWhere));
			while ($tmp = $this->oDb->fetch_array()) {
				// hier nochmal alle felder durchlaufen
				foreach ($aFields as $k => $field) {
					$field = trim($field);
					if (!empty($tmp[$field])) {
					// und nur die matchenden suchergebnisse speichern: 
					// -> array[tablename][datensatz_id][fieldname] = media_id
						$buffer[$table][$tmp['id']][$field] = $tmp[$field];
					}
				}
			}
		}
		return $buffer;	#print_r($buffer);
	}
/**
* Hilfsfunktion fuer checkMedia(): Prueft DB-Felder mit mehreren Werten.
*
* @access	public
* @param	int		$media_id
* @return	array	leer, wenn ok; sonst felder, in denen das file verwendet wird!
*/
	function checkMultipleMedia($media_id) {
		
		if (!is_array($this->aENV['check_multiple_media'])) return;
		$buffer = array();
		
		// gehe durch alle tables
		foreach ($this->aENV['check_multiple_media'] as $table => $fields) {
			$aFields = explode(',', $fields);
			$this->oDb->query("SELECT id, ".$fields." FROM ".$table); // query (ohne WHERE bedingung)
			while ($tmp = $this->oDb->fetch_array()) {
				// gehe durch alle felder
				foreach ($aFields as $nix => $field) {
					$field = trim($field);
					$aValue = explode(',', $tmp[$field]); // teile values am komma
					// gehe durch alle values
					foreach ($aValue as $key => $val) {
						if (!empty($val) && $val == $media_id) {
						// und nur die matchenden suchergebnisse speichern: 
						// -> array[tablename][datensatz_id][fieldname] = media_id
							$buffer[$table][$tmp['id']][$field] = $val;
						}
					}
				}
			}
		}
		return $buffer;	#print_r($buffer);
	}


// NICHT IN BENUTZUNG ///////////////////////////////////////////////////////////////////////////

	// DB-action: delete_all_unused 
	// braucht zwingend die vollstaendige(!) Konfiguration in welcher Table in welchen Feldern welche Media-Files benutzt werden!!!
	// -> TODO: nicht loeschen, sondern in Ordner verschieben?!
	function delete_all_unused() {
		/*********************/
		$DEBUG = true; // <-- 
		/*********************/
		$error = array();
		$log = array();
		$aToDelete = array();
		$oDb2 =& new dbconnect($this->aENV['db']);
		$oDb2->query("SELECT id, filename FROM ".$this->aENV['table']['mdb_media']." ORDER BY id");
		while ($aData = $oDb2->fetch_array()) {
			// checken ob dieses file noch irgendwo verwendet wird
			$check = _check_media($aData['id']);
			if (empty($check)) { // wenn nicht:
				if ($DEBUG) { // zum DEBUG array hinzufuegen
					$aToDelete[$aData['id']] = $aData['filename'];
				} else {
					// move unused file in Backup-dir
					$src_file	= $this->aENV['path']['upload']['unix'].$aData['filename'];  // ...'unix' muss genommen werden, weil sonst kein file geschrieben werden kann
					$dest_file	= $this->aENV['path']['upload']['unix'].'UNUSED/'.$aData['filename'];  // ...'unix' muss genommen werden, weil sonst kein file geschrieben werden kann
					if (file_exists($src_file)) {
						$delete = rename($src_file, $dest_file);
						if (!$delete) { // error
							$error['rename'][$aData['id']] = $aData['filename'];
						} else { // alles klar!
							$log[$aData['id']] = $aData['filename'];
							// delete data in DB
							$aData2delete['id'] = $aData['id']; // Delete data from editview
							#$oDb->make_delete($aData2delete,$aENV['table']['mdb_media']);
						}
					} else { // error
						$error['exist'][$aData['id']] = $aData['filename'];
					}
				}
			}
		} // END while
		if ($DEBUG) { // zum DEBUG array hinzufuegen
			echo count($aToDelete);
			print_r($aToDelete);
		}
		if (count($error['exist'])) {
			echo "ERROR: ".count($error['exist'])." files do not exist:<br>\n";
			print_r($error['exist']);
		}
		if (count($error['rename'])) {
			echo "ERROR: ".count($error['rename'])." files can not be moved:<br>\n";
			print_r($error['rename']);
		}
		if (count($log)) {
			echo "LOG: ".count($log)." files successfully moved:<br>\n";
			print_r($log);
		}
	}

/**
* Gibt die IDs aller Unterseiten eines Datensatzes zurueck (nur sub & subsub & subsubsub!).
*
* @access   public
* @global	array	$aENV
* @global	object	$oDb
* @param	string	$id		(ID des Datensatzes in der Table)
* @param	string	$sTable (Name der Content-Table)
* @return	array	IDs aller Unterpunkte (max. 3 Unterebenen!)
*/
	function returnAllSubIds($id, $sTable) {
		
		// vars
		$aID = array();
		$aSubID = array();
		
		// get subs
		$sql = "SELECT id FROM ".$sTable." WHERE parent_id='".$id."' AND flag_tree='mdb'";

		$this->oDb->query($sql);
		while ($aData = $this->oDb->fetch_array()) {
			$aID[] = $aData['id'];
		}
		// get subsubs
		$sIDs = implode(",", $aID);
		if (!empty($sIDs)) {
			$this->oDb->query("SELECT id FROM ".$sTable." WHERE parent_id IN (".$sIDs.") AND flag_tree='mdb'");
			while ($aData = $this->oDb->fetch_array()) {
				$aSubID[] = $aData['id'];
			}
		}
		// get subsubsubs
		$sSubIDs = implode(",", $aSubID);
		if (!empty($sSubIDs)) {
			$this->oDb->query("SELECT id FROM ".$sTable." WHERE parent_id IN (".$sSubIDs.") AND flag_tree='mdb'");
			while ($aData = $this->oDb->fetch_array()) {
				$aSubID[] = $aData['id'];
			}
		}
		$aID = array_merge($aID, $aSubID);
		return $aID;
	}

} // END class
?>