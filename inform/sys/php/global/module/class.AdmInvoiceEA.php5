<?php
/**
 * Diese Klasse kapselt die Datenbankzugriffe im ADM/Invoice Bereich. 
 *
 * @access   public
 * @package  adm
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @author	Andy Fehn <af@design-aspekt.com>
 * @version	1.0 / 2006-03-17
 * 
 * @param object $oDb
 */
	class AdmInvoiceEA {
		private $oDb = null;
		private  $entries = 0;
		
		public function AdmInvoiceEA($oDb) {
			$this->oDb = clone $oDb;
		}
		
		/**
		 * Diese Methode ermittelt, ob es mindestens einen Invoice-Datensatz zu einer Project-ID gibt.
		 * @param	int		$project_id
		 * @return	boolean
		 */
		public function hasInvoice($project_id) {
			if (!$project_id) return false; // TODO: error?
			// update data
			$this->oDb->query("SELECT `id` FROM `adm_invoice` WHERE `project_id` = '$project_id'");
			return ($this->oDb->num_rows() > 0) ? true : false;
		}
		
		/**
		 * Diese Methode ermittelt nur alle Invoice-Stammdatensätze zu einer Projectid
		 * @param	int		$projectid 
		 * @param	string	$orderby	[optional, default: 'invoice_nb ASC']
		 * @return	array[invoiceid][fieldname] = fieldvalue
		 */
		public function getInvoiceByProjectId($projectid, $start=0, $limit=0) {
			// constraint
			$sConstraint = "`project_id` = '$projectid'";
			
			// output
			return $this->getInvoice($start, $limit, $sConstraint);
		}
		
		/**
		 * Diese Methode ermittelt nur alle Invoice-Stammdatensätze zu einer Projectid
		 * @param	int		$start 			[optional, default: '']
		 * @param	int		$limit 			[optional, default: '']
		 * @param	string	$sConstraint 	[optional, default: '']
		 * @param	string	$orderby		[optional, default: 'invoice_nb ASC']
		 * @return	array[invoiceid][fieldname] = fieldvalue
		 */
		public function getInvoice($start=0, $limit=0, $sConstraint='', $orderby='') {
			// orderby
			if (empty($orderby)) $orderby = '`id` DESC'; // set default
			// constraint
			if (!empty($sConstraint) && !strstr($sConstraint, 'WHERE')) {
				$sConstraint = 'WHERE '.$sConstraint;
			}
			
			// select invoices
			$sql = "SELECT `id`,`parent_id`,`project_id`,`introduction`,`resume`,`date`,`discount`,`tax`,`handling`,
							`raddress_id`,`flag_mahnung`,`flag_storno`,`storno_text`,`flag_closed`,`rcontact_id`,`invoice_nb`,`created`,`created_by`,`last_modified`,`last_mod_by` 
					FROM `adm_invoice` 
					".$sConstraint." 
					ORDER BY ".$orderby;
			$this->oDb->query($sql);
			$this->entries = $this->oDb->num_rows();
			// ggf. limit
			if (($limit > 0 && $limit < $this->entries) || $start > 0) {
				$this->oDb->limit($sql, $start, $limit);
			}
			
			// get data
			$buffer = array();
			while ($row = $this->oDb->fetch_array()) {
				$buffer[$row['id']] = $row;	
			}
			$this->oDb->free_result();
			
			// output
			return $buffer;
		}
		
		/**
		 * Diese Methode ermittelt alle Summen zu einem Invoice-Stammdatensatz
		 * @param	int		$invoiceid 
		 * @return	array[key] = value (unbehandelte float-werte)
		 */
		public function getInvoiceSum($invoiceid=0) {
			// check vars
			if (empty($invoiceid)) return false; // TODO: error?
			// init vars
			$buffer				= array();
			$buffer['fees']		= 0;
			$buffer['extern']	= 0;
			
			// betraege jedes datasets sammeln
			$sql = "SELECT `price_net`,`flag_extern`
					FROM `adm_invoice_datasets` 
					WHERE `invoice_id` = '$invoiceid' 
					ORDER BY `prio` DESC;";
			$this->oDb->query($sql);
			if ($this->oDb->num_rows() != 0) { 
				while ($row = $this->oDb->fetch_array()) {
					if (isset($row['flag_extern']) && !empty($row['flag_extern'])) {
						$buffer['extern']	+= $row['price_net'];	
					} else {
						$buffer['fees']		+= $row['price_net'];
					}
				}
			
			}
			// summenrelevante informationen aus der stammdatensatz tabelle sammeln
			$sql = "SELECT `discount`,`tax`,`handling`
					FROM `adm_invoice` 
					WHERE `id` = '$invoiceid';";
			$this->oDb->query($sql);
			$row = $this->oDb->fetch_array();
			$buffer['discount']	= (isset($row['discount']) && !empty($row['discount'])) ? $row['discount'] : 0;
			$buffer['tax']		= (isset($row['tax']) && !empty($row['tax'])) ? $row['tax'] : 0;
			$buffer['handling']	= (isset($row['handling']) && !empty($row['handling'])) ? $row['handling'] : 0;
			
			$this->oDb->free_result();
			
			// output
			return $buffer;
		}
		
		public function duplicate($aInvoice,$aInvoiceDatasets) {
			
			$invoiceid = $this->insert($aInvoice['project_id'],null,$aInvoice);
			for($i=0;is_array($aInvoiceDatasets[$i]);$i++) {
				$aInvoiceDatasets[$i]['invoice_id'] = $invoiceid;
				$this->oDb->make_insert($aInvoiceDatasets[$i],'adm_invoice_datasets');
			}
			return $invoiceid;
		}
		
		/**
		 * Diese Methode ermittelt entweder alle Invoicedatensätze zu einer Projectid oder zu einer Invoiceid
		 * @param int $projectid [optional]
		 * @param int $invoiceid [optional]
		 * @param String $orderby [optional]
		 * @param String $orderway [optional]
		 * @param bool $bAssoc [optional]
		 * @return array
		 */
		public function getInvoiceData($projectid=0,$invoiceid=0,$orderby='',$orderway='',$bAssoc=false) {
			$t1 = 'adm_invoice';
			$t2 = 'adm_invoice_datasets';

			$sql = "SELECT " .
					"$t1.`id` as invoice_id,$t1.`prj_nr`,$t1.`po_number`,$t1.`projectname`,$t1.`handling`,$t1.`currency`,$t1.`introduction`,$t1.`resume`,$t1.`date`,$t1.`discount`,$t1.`tax`,$t1.`handling`,$t1.`flag_mahnung`," .
					"$t1.`raddress_id`,$t1.`rcontact_id`,$t1.`invoice_nb`,$t1.`parent_id`,$t1.`created`,$t1.`created_by`,$t1.`last_modified`,$t1.`last_mod_by`," .
					"$t2.`prio`,$t2.`id`,$t2.`job_cat`,$t2.`description`,$t2.`hours`,$t2.`quantity`,$t2.`quantity_cat`," .
					"$t2.`alt_jobcat`, $t2.`price_net`,$t2.`flag_extern`,$t1.`flag_closed`,$t1.`flag_storno`,$t1.`storno_text`,$t1.`project_id`,$t1.`flag_storno`,$t1.`storno_text` " .
					"FROM $t1 LEFT JOIN $t2 " .
					"ON $t1.`id`=$t2.`invoice_id` " .
					"WHERE ";
			
			if(empty($orderway) && empty($orderby)) {
				$order = ' ORDER BY id ASC';
			}
			else {
				$order = ' ORDER BY `'.$orderby.'` '.$orderway;	
			}
			if($projectid != 0) {
				$sql .= "$t1.`project_id`='$projectid'".$order;
			}
			else {
				$sql .= "$t1.`id`='$invoiceid'".$order;
			}
			if($bAssoc) {
				return $this->generateAssozData($sql);
			}
			else {
				return $this->generateData($sql);
			}
		}
		/**
		 * 
		 * Diese Methode liefert alle Invoice Informationen.
		 * @return Array
		 * 
		 */
		public function getAllInvoiceData() {
			$t1 = 'adm_invoice';
			$t2 = 'adm_invoice_datasets';

			$sql = "SELECT " .
					"$t1.`id` as `invoice_id`,$t1.`flag_mahnung`,$t1.`introduction`,$t1.`resume`,$t1.`date`,$t1.`discount`,$t1.`raddress_id`," .
					"$t1.`rcontact_id`,$t1.`invoice_nb`,$t1.`created`,$t1.`created_by`,$t1.`last_modified`,$t1.`last_mod_by`," .
					"$t2.`prio`,$t2.`id`,$t2.`job_cat`,$t2.`description`,$t2.`hours,$t2.`quantity`,$t2.`quantity_cat`," .
					"$t2.`alt_jobcat`, $t2.`price_net`,$t1.`flag_closed`,$t1.`flag_storno`,$t1.`storno_text`,$t2.`flag_extern` " .
					"FROM $t1 INNER JOIN $t2 " .
					"ON $t1.`id`=$t2.`invoice_id` " .
					"ORDER BY `invoice_id` ASC";
			
			return $this->generateData($sql);
		}
		
		/**
		 * 
		 * Führt einen Copy Insert durch.
		 * @param int $projectid
		 * @param array $aIds
		 * @param array $aData
		 * @return int $invoiceid
		 * 
		 */
		public function insert($projectid,$aIds,$aData) {
			/*
			$sql = "SELECT max(`invoice_nb`) as `inb` 
					FROM `adm_invoice` 
					WHERE `project_id`='$projectid'";
			$this->oDb->query($sql);
			if($this->oDb->num_rows() != 0) {
				$row = $this->oDb->fetch_object();
				$inb = ++$row->inb;
			}
			else {
				$inb = 1;
			}*/
			//$aData['invoice_nb'] = $inb;
			$aData['project_id'] = $projectid;
			$this->oDb->make_insert($aData,'adm_invoice');
			$invoice_id = $this->oDb->insert_id();
			if(is_array($aIds) && count($aIds) > 0) {
				
				$oProposalEA = new AdmProposalEA(clone $this->oDb);
				
				$sql = "SELECT * 
						FROM `adm_proposal` 
						WHERE `project_id`='$projectid' 
							AND `id` IN(".implode(',',$aIds).")";
				$aData2 = $this->generateData($sql);
				unset($aData);
				
				for($i=0;!empty($aData2[$i]['id']);$i++) {
					foreach($aData2[$i] as $fieldname => $val) {
						if($fieldname != 'id' && $fieldname != 'project_id') {
							$aData[$fieldname] = $val;
						}
						if($fieldname == 'id') {
							$oProposalEA->saveProposalInvoices($val, $invoice_id);	
						}
					}
					$aData['invoice_id'] = $invoice_id;
					$this->oDb->make_insert($aData,'adm_invoice_datasets');
				
				}
			}
			return $invoice_id;
		}
		
		/**
		 * 
		 * Liefert die Daten, die die Adm Klasse für das Aufbereiten zum Pdf benötigt
		 * @param int $projectid
		 * @param String $orderby
		 * @param String $orderway
		 * @return array
		 * @access public
		 * 
		 */
		public function getPDFData($invoiceid,$orderby,$orderway) {
			$t1 = 'adm_invoice';
			$t2 = 'adm_invoice_datasets';

			$sql = "SELECT " .
					"$t1.`id` as `invoice_id`,$t1.`flag_mahnung`,$t1.`introduction`,$t1.`resume`,$t1.`date`,$t1.`discount`,$t1.`raddress_id`,$t1.`parent_id`," .
					"$t1.`rcontact_id` as contact_id,$t1.`invoice_nb`,$t1.`created`,$t1.`created_by`,$t1.`last_modified`,$t1.`last_mod_by`," .
					"$t2.`prio`,$t2.`id`,$t2.`job_cat`,$t2.`description`,$t2.`hours`,$t2.`quantity`,$t2.`quantity_cat`," .
					"$t2.`alt_jobcat`, $t2.`price_net`,$t2.`flag_extern`,$t1.`currency`,$t1.`prj_nr`,$t1.`po_number`,$t1.`projectname`,$t1.`po_number`," .
					"$t1.`flag_closed`,$t1.`handling`,$t1.`tax` " .
					"FROM $t1 LEFT JOIN $t2 ON $t1.`id`=$t2.`invoice_id` " .
					"WHERE $t1.`id`='$invoiceid'";

			if(empty($orderway) && empty($orderby)) {
				$sql .= ' ORDER BY `id` ASC';
			}
			else {
				$sql .= ' ORDER BY `'.$orderby.'` '.$orderway;	
			}

			return $this->generateData($sql);
		}
		/**
		 * Liefert die Entries dieser Query zurück.
		 * @return int
		 * 
		 */
		public function getEntries() {
			return $this->entries;	
		}
		/**
		 * 
		 * Diese Methode updated einen bestehenden Rechnungsdatensatz.
		 * @param array $aData
		 * @return bool
		 * 
		 */
		public function update($aData,$sTable='adm_invoice_datasets') {
			return $this->oDb->make_update($aData,$sTable,$aData['id']);
		}
		
		/**
		 * @access protected
		 * Diese Methode generiert die Daten für diese EA. Dabei ist der erste Schlüssel numerisch und der Zweite der jeweilige Feldname.
		 * @param String $sql
		 * @param boolean $arraykey (falls der arraykey mit dem wert eines der felder gefuellt werden soll)
		 * @return array
		 * 
		 */
		protected function generateData($sql, $arraykey=false) {
			$aInvoiceData = $this->oDb->fetch_in_aarray($sql, $arraykey);
			$this->entries = $this->oDb->num_rows();
			$this->oDb->free_result();
			return $aInvoiceData;	
		}
		
		/**
		 * @access protected
		 * @param String $sql
		 * @return assoziatives Array [invoiceid][datasetid][keys]
		 * 
		 */
		protected function generateAssozData($sql) {
			$this->oDb->query($sql);
			$this->entries = $this->oDb->num_rows();
			for($i=0;$row = $this->oDb->fetch_object();$i++) {
				foreach($row as $key => $val) {
					$aInvoiceData[$row->invoice_id][$row->id][$key] = $val;	
				}
			}
			$this->oDb->free_result();
			return ksort($aInvoiceData);	
		}

		/**
		 * Diese Methode ermittelt alle Timesheet-Eintraege zu einer Invoice-ID
		 * @param	int		$invoiceid 
		 * @return	array[timesheet_id][key] = value
		 */
		public function getInvoiceTimesheets($invoiceid=0,$project_id=0) {
			// vars
		//	if (empty($invoiceid)) return false; // TODO: error?
			$buffer	= array();
			// select
			$sql = "SELECT `id`,`hours`,`description`,`date_iso`,`job_cat`,`user_id`,`flag_bill`,`project_id`,`flag_bill` 
					FROM `adm_timesheet` 
					WHERE `invoice_id` = '".$invoiceid."' AND `flag_bill`='1'";
			if($project_id!=0) $sql .= " AND project_id='$project_id' ";
			$sql .= " ORDER BY `date_iso` ASC;";
			// output
			return $this->generateData($sql, 'id');
		}
		
		/**
		 * Diese Methode ermittelt alle Expenses zu einer Invoice-ID
		 * @param	int		$invoiceid 
		 * @return	array[expence_id][key] = value
		 */
		public function getInvoiceExpenses($invoiceid=0,$project_id=0) {
			// vars
		//	if (empty($invoiceid)) return false; // TODO: error?
			$buffer	= array();
			// select
			$sql = "SELECT `id`,`project_id`,`cat`,`job_cat`,`description`,`price_net`,`currency`,`tax`,`date_iso`,`flag_bill`,`created_by` 
					FROM `adm_expense` 
					WHERE `invoice_id` = '".$invoiceid."' AND `flag_bill`='1'";
			if($project_id!=0) $sql .= " AND project_id='$project_id' ";
			$sql .= " ORDER BY `job_cat` ASC, `date_iso` ASC;";
			// output
			return $this->generateData($sql, 'id');
		}
		
		public function deleteInvoice($invoiceid) {
			// alle notwendigen daten loeschen
			$sql = "DELETE FROM `adm_invoice_datasets` WHERE `invoice_id`='$invoiceid'";
			$this->oDb->query($sql);
			$sql = "DELETE FROM `adm_invoice` WHERE `id`='$invoiceid'";
			$this->oDb->query($sql);
			// alle abhaengigen daten aufraeumen
			$sql = "UPDATE `adm_timesheet` SET `flag_bill`='0',`invoice_id`='0' WHERE `invoice_id`='$invoiceid' AND `flag_bill`='1'";
			$this->oDb->query($sql);
			$sql = "UPDATE `adm_expense` SET `flag_bill`='0',`invoice_id`='0' WHERE `invoice_id`='$invoiceid' AND `flag_bill`='1'";
			$this->oDb->query($sql);
		}

	/**
	 * Diese Methode markiert einen oder mehrere Timesheet-Datensaetze als berechnet.
	 * @param	mixed	$timesheet_id
	 * @param	int		$invoice_id
	 * @return	boolean
	 */
	public function markInvoiceAsClosed($invoice_id) {
		// check vars
		if (is_array($invoice_id)) {
			$invoice_id = implode(',', $invoice_id);
		}
		if (empty($invoice_id)) return false; // TODO: error?
		// update data
		$sql = "UPDATE `adm_invoice` 
				SET `flag_closed`='1' 
				WHERE `id` IN(".$invoice_id.")";
		return $this->oDb->query($sql);
	}
		
	public function getInvoiceYear() {
		$sql = 'SELECT YEAR(date) as year FROM adm_invoice';
		return $this->oDb->fetch_in_aarray($sql);
	}
### Ab hier sind vorruebergehend ADR EAs... 
### TODO: in eigene EA-Klasse auslagern (bzw. nach Umbau auf ADR v2 anpassen!)

		/**
		 * @deprecated 1.0 - 04.07.2006
		 */
		public function getCompanyContacts($cid) {
			$sql = "SELECT firstname,surname,c.id as contact_id FROM adr_contact c INNER JOIN adr_address a ON c.id=a.reference_id WHERE (cat='o1' OR CAT='o2') AND a.company_id=".$cid;
			$Data = $this->oDb->fetch_in_aarray($sql);
			$aFill = array();
			for($i=0;is_array($Data[$i]);$i++) {
				$aFill[$Data[$i]['contact_id']] = trim($Data[$i]['title'].' '.$Data[$i]['firstname'].' '.$Data[$i]['surname']);
			}
			return $aFill;
		}
		/**
		 * @deprecated 1.0 - 17.03.2006
		 */
		public function getAdrContact($rcontact_id) {
			return $this->oDb->fetch_by_id($rcontact_id,'adr_contact');	
		}
		
		/**
		 * @deprecated 1.0 - 17.03.2006
		 */
		public function getAdrCountries() {
			$aCountry = array();
			$this->oDb->query("SELECT code, country, phone_prefix FROM adr_country ORDER BY country");
			while ($tmp = $this->oDb->fetch_array()) {
				$aCountry[$tmp['code']]['name']		= $tmp['country'];
				$aCountry[$tmp['code']]['phone']	= (!empty($tmp['phone_prefix'])) ? '+'.$tmp['phone_prefix'] : '';
			}
			return $aCountry;
		}
		
		/**
		 * @deprecated 1.0 - 17.03.2006
		 */
		
		public function getAdrRefId($raddress_id) {
			$sql = "SELECT reference_id 
					FROM adr_address 
					WHERE id='$raddress_id'";	
			return $this->generateData($sql);
		}
		
		/**
		 * @deprecated 1.0 - 17.03.2006
		 */
		public function getAdrDetails($clientid) {
			$t1 = 'adr_address';
			$t2 = 'adr_country';
			
			$sql = "SELECT $t1.`po_box`,$t1.`po_box_town`,$t1.`street`,$t1.`building`,$t1.`region`,$t1.`town`,$t2.`country` 
					FROM $t1 INNER JOIN $t2 
					ON $t1.`country`=$t2.`code` 
					WHERE $t1.`reference_id`='".$clientid."'";
			
			return $this->generateData($sql);
		}
		
	}
?>