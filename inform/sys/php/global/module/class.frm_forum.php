<?php // Class frm_forum
/**
* This class provides all FRM-forum functionalities.
*
* Example: 
* <pre><code>
* // init
* $oForum =& new frm_forum($oDb);
* // print gesamtanzahl eintraege
* echo $oForum->countAll(); // params: -
* // ermittle aktuellen eintrag
* $aData = $oForum->getEntry(127); // params: $id
* print_r($data);
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.3 / 2006-02-06 ["countComments()" umbenannt + unbenutztes geloescht + NEU: "authThreads()"]
*/

class frm_forum {
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse frm_forum:
		----------------------------------------------------------------------------
		constructor frm_forum(&$oDb)
		function createTable()
		function authThreads($aTreeId)
		
		function getPostings($nTreeId)
		function getComments($nPostingId)
		function getRecent($nLimit=10, $nPostingOnly=false)
		function getSearchResult($sSearchtext, $nTreeId=NULL)
		
		function setMaxLevel($nMaxLevel)
		function countLevel()
		function countAll()
		function countPostings($nTreeId)
		function countComments($nParentId)
		function getCommentIds($nParentId)
		
		function getEntry($id)
		function saveEntry($aData, $user_id=0)
		function deleteEntry($id)
		
		function quoteHeadline($sHeadline)
		function quoteContent($sContent, $nSplitLength=40)
		----------------------------------------------------------------------------
		HISTORY:
		1.3 / 2006-02-06 ["countComments()" umbenannt + unbenutztes geloescht + NEU: "authThreads()"]
		1.2 / 2005-04-28 [Aufgeraeumt + einiges UMBENANNT + unbenutztes DEAKTIVIERT + NEU: "getCommentIds()"]
		1.11 / 2005-04-26 [NEU: check empty($sSearchtext) bei "getSearchResult()"]
		1.1 / 2005-04-18 [sys->frm]
		1.0 / 2005-04-15
	*/
	
	// global vars
	var $oDb;				// DB-Object
	var $sTable;			// Tablename
	var $nMaxLevel;			// maximale verschachtelungs-tiefe
	var $nCountLevel;		// tatsaechliche verschachtelungs-tiefe
	var $sSearchtext;		// Suchtext
	var $temp;				// teporaere variable fuer rekursive methoden...
	var $aAuthedTreeIds;	// wenn Threads authentifiziert wurden, sind hier alle gueltigen drin
	
/**
* @access   private
* @var	 	array	Zeichen, die aus dem Flash-Texteditor rauskommen und vor dem speichern in der DB ersetzt werden muessen
*/
	var $aFlashSearch;
/**
* @access   private
* @see      $aFlashSearch
* @var	 	array	Zeichen, die eingesetzt werden
*/
	var $aFlashReplace;


/**
* Konstruktor -> Initialisiert das forum-object
*
* Beispiel:
* <pre><code>
* $oForum =& new frm_forum($oDb); // params: &$oDb
* </code></pre>
*
* @access   public
* @param	object		$oDb		DB-Objekt
* @return	void
*/
	function frm_forum(&$oDb) {
		// import db-object
		$this->oDb				= $oDb;
		// set defauts
		global $aENV;
		$this->sTable			= (isset($aENV['table']['frm_forum'])) ? $aENV['table']['frm_forum'] : 'frm_forum';
		$this->nCountLevel		= 0;
		$this->nMaxLevel		= 20; // nur zur Sicherheit!
		$this->sSearchtext		= '';
		$this->aAuthedTreeIds	= array();
		
		// Flash-Texteditor
		$this->aFlashFieldname	= array('content'); // default: KEIN Flash-Texteditor
		$this->aFlashSearch		= array('&lt;', '&gt;', '&amp;', '&quot;', '&apos;');
		$apos = (!get_magic_quotes_gpc()) ? "'" : "\'";
		$this->aFlashReplace	= array('<', '>', '&', '"', $apos);
	}

/**
* Erstell-Funktion: erstelle DB-Table
* @access   public
* @return	void
*/
	function createTable() {
		if (empty($this->sTable)) return;
		$q = "CREATE TABLE `".$this->sTable."` ( 
			  `id` int(11) NOT NULL auto_increment,
			  `tree_id` int(11) default NULL,
			  `parent_id` int(11) default NULL,
			  `headline` varchar(150) default NULL,
			  `content` text,
			  `keywords` varchar(250) default NULL,
			  `img_id` int(11) default NULL,
			  `downloads` text,
			  `searchtext` text,
			  `created` varchar(14) default NULL,
			  `created_by` smallint(5) default NULL,
			  `last_modified` timestamp(14) NOT NULL,
			  `last_mod_by` smallint(5) default NULL,
			  PRIMARY KEY  (`id`)
			) TYPE=MyISAM;";
		$this->oDb->query($q);
	}

# -----------------------------------------------------------------------------------

/** 
* Empfaengt alle Thread-IDs, die fuer den aktuellen User freigeschaltet sind.
* 
* <pre><code> 
* $oForum->setAuthedThreads($aTreeId); // params: $aTreeId
* </code></pre>
* 
* @access   public
* @param	array	$aTreeId		IDs der fuer den aktuellen User autorisierten Tree-Datensaetze
* @return	array
*/
	function authThreads($aTreeId) {
		if (is_array($aTreeId)) {
			$this->aAuthedTreeIds = $aTreeId;
			array_unshift($this->aAuthedTreeIds, '0');
		}
		return $this->aAuthedTreeIds;
	}

/** 
* ermittelt alle Postings (nur die wesentlichen Infos fuer eine Uebersicht) 
* (nur 1. ebene!) und ermittelt vieviele Antworten zum jew. Eintrag existieren. 
* Optional kann auf einen Thread (=tree_id) eingeschraenkt werden.
* 
* <pre><code> 
* $aData = $oForum->getPostings($tree_id); // params: $nTreeId=NULL
* </code></pre>
* 
* @access   public
* @param	int		$nTreeId		ID des Verzeichnisses (optional -> default:NULL)
* @return	array
*/
	function getPostings($nTreeId=NULL) {
		// vars
		$buffer = array();
		if (is_null($nTreeId)) $nTreeId = 'all'; // Postings aus allen Threads
		// ggf. einschraenkung auf einen thread
		$sAddConstraint = ($nTreeId != 'all') ?  'AND tree_id = '.$nTreeId : '';
		// bei 'all' ggf. nur die fuer den aktuellen user erlaubten datensaetze anzeigen
		if ($nTreeId == 'all' && count($this->aAuthedTreeIds) > 0) {
			$sAddConstraint = 'AND tree_id IN ('.implode(',', $this->aAuthedTreeIds).')';
		}
		// query
		$sQuery = 'SELECT `id`,`tree_id`,`headline`,`keywords`,`img_id`,`flag_with_comments`,`created`,`created_by` 
					FROM `'.$this->sTable.'` 
					WHERE `parent_id` = 0 '.$sAddConstraint.' 
					ORDER BY `created` DESC';
		$this->oDb->query($sQuery);
		while ($row = $this->oDb->fetch_array()) {
			$buffer[] = array(
				'id'			=> $row['id'],
				'tree_id'		=> $row['tree_id'],
				'headline'		=> $row['headline'],
				'keywords'		=> $row['keywords'],
				'img_id'		=> $row['img_id'],
				'flag_with_comments'		=> $row['flag_with_comments'],
				'created'		=> $row['created'],
				'created_by'	=> $row['created_by']
			);
		}
		// Zusatzvariable: Anzahl Kommentare
		for ($i = 0; $i< count($buffer); $i++) {
			$buffer[$i]['comments'] = $this->countComments($buffer[$i]['id']);
		}
		return $buffer;
	}

/** 
* ermittelt alle Kommentare (nur 1 ebene!) dieser nPostingId (Artikel/Root-Posting). 
* 
* <pre><code> 
* $aData = $oForum->getComments($id); // params: $nPostingId
* </code></pre>
* 
* @access   public
* @param	int		$nPostingId		parent-ID der Kommentare
* @return	array
*/
	function getComments($nPostingId) {
		// vars
		$buffer = array();
		if (!$nPostingId) return $buffer;
		// query
		$sQuery = 'SELECT `id`,`parent_id`,`headline`,`content`,`keywords`,`img_id`,`downloads`,`created`,`created_by`,`last_modified`,`last_mod_by` 
					FROM `'.$this->sTable.'` 
					WHERE `parent_id` = '.$nPostingId.' 
					ORDER BY `created` ASC';
		$this->oDb->query($sQuery);
		while ($row = $this->oDb->fetch_array()) {
			$buffer[] = array(
				'id'			=> $row['id'],
				'parent_id'		=> $row['parent_id'],
				'headline'		=> $row['headline'],
				'content'		=> $row['content'],
				'keywords'		=> $row['keywords'],
				'img_id'		=> $row['img_id'],
				'downloads'		=> $row['downloads'],
				'created'		=> $row['created'],
				'created_by'	=> $row['created_by'],
				'last_modified'	=> $row['last_modified'],
				'last_mod_by'	=> $row['last_mod_by']
			);
		}
		return $buffer;
	}

/** 
* ermittelt die aktuellsten $nLimit Eintraege.
* Wenn ein Eintrag einen neuen Kommentar hinzubekam, wird dieser Eintrag auch als neu behandelt, 
* ausser man uebergibt als zweiten optionalen Parameter false. Dann werden nur neue Postings (Erst-Eintraege) angezeigt.
* Ist unahbhaengig von Tree.
*
* <pre><code> 
* $aData = $oForum->getRecent($limit); // params: [$nLimit=10][,$nPostingOnly=false]
* </code></pre>
* 
* @access   public
* @param	int		$nLimit			Anzahl angezeigter Threads
* @param	boolean	$nPostingOnly	sollen nur neue (Erst-)Eintraege ermittelt werden (optional - default: false)
* @return	array
*/
	function getRecent($nLimit=10, $nPostingOnly=false) {
		// vars
		global $aENV;
		$buffer = array();
		$sAddConstraint = '';
		// ggf. nur die fuer den aktuellen user erlaubten datensaetze anzeigen
		if (count($this->aAuthedTreeIds) > 0) {
			$sAddConstraint = 'tree_id IN ('.implode(',', $this->aAuthedTreeIds).')';
		}

		if ($nPostingOnly == true) {
			// query
			if ($sAddConstraint != '') $sAddConstraint = 'AND '.$sAddConstraint;
			$sQuery = 'SELECT `id`,`tree_id`,`parent_id`,`headline`,`keywords`,`img_id`,`flag_with_comments`,`created`,`created_by`,`last_modified`,`last_mod_by` 
						FROM `'.$this->sTable.'` 
						WHERE `parent_id` = 0 '.$sAddConstraint.' 
						ORDER BY `created` DESC';
			$this->oDb->limit($sQuery, 0, $nLimit); // sql, start, limit
			while ($row = $this->oDb->fetch_array()) {
				$buffer[] = array(
					'id'			=> $row['id'],
					'tree_id'		=> $row['tree_id'],
					'parent_id'		=> $row['parent_id'],
					'headline'		=> $row['headline'],
					'keywords'		=> $row['keywords'],
					'img_id'		=> $row['img_id'],
					'flag_with_comments'		=> $row['flag_with_comments'],
					'created'		=> $row['created'],
					'created_by'	=> $row['created_by'],
					'last_modified'	=> $row['last_modified'],
					'last_mod_by'	=> $row['last_mod_by']
				);
			}
		} else {
			$aPostingId = array();
			$i = 0;
			// query
			if ($sAddConstraint != '') $sAddConstraint = 'WHERE '.$sAddConstraint;
			$sQuery = 'SELECT `id`,`tree_id`,`parent_id`,`headline`,`keywords`,`img_id`,`flag_with_comments`,`created`,`created_by`,`last_modified`,`last_mod_by` 
						FROM `'.$this->sTable.'` '.$sAddConstraint.' 
						ORDER BY `created` DESC';
			$this->oDb->query($sQuery); // hier KEIN limit
			while ($row = $this->oDb->fetch_array()) {
				// posting-IDs sammeln
				if ($row['parent_id'] == 0) {
					// Beitrag nur hinzufuegen wenn noch nicht dabei
					if (!in_array($row['id'], $aPostingId)) {
						$aPostingId[] = $row['id'];
					} else {
						continue;
					}
				} else {
					// Kommentar nur hinzufuegen wenn posting noch nicht dabei ist
					if (!in_array($row['parent_id'], $aPostingId)) {
						$aPostingId[] = $row['parent_id'];
					} else {
						continue;
					}
				}
				$buffer[] = array(
					'id'			=> $row['id'],
					'tree_id'		=> $row['tree_id'],
					'parent_id'		=> $row['parent_id'],
					'headline'		=> $row['headline'],
					'keywords'		=> $row['keywords'],
					'img_id'		=> $row['img_id'],
					'flag_with_comments'	=> $row['flag_with_comments'],
					'created'		=> $row['created'],
					'created_by'	=> $row['created_by'],
					'last_modified'	=> $row['last_modified'],
					'last_mod_by'	=> $row['last_mod_by']
				);
				// Anzahl Postings hochzaehlen und bei Limit aufhoeren
				if (++$i >= $nLimit) {
					break(0);
				}
			}
		}
		// Zusatzaktionen
		for ($i = 0; $i< count($buffer); $i++) {
			// Kommentar gegen seinen Originalbeitrag ersetzen
			if ($nPostingOnly == false && $buffer[$i]['parent_id'] > 0) {
				$buffer[$i] = $this->getEntry($buffer[$i]['parent_id']); // params: $id
			}
			// Zusatzvariable: Anzahl Kommentare
			$buffer[$i]['comments'] = $this->countComments($buffer[$i]['id']);
		}
		return $buffer;
	}

/** 
* ermittelt die aktuellsten $nLimit abonnierten Eintraege.
* Wenn ein Eintrag einen neuen Kommentar hinzubekam, wird dieser Eintrag auch als neu behandelt, 
* ausser man uebergibt als zweiten optionalen Parameter false. Dann werden nur neue Postings (Erst-Eintraege) angezeigt.
* Ist unahbhaengig von Tree.
*
* <pre><code> 
* $aData = $oForum->getRecent($limit); // params: [$nLimit=10][,$nPostingOnly=false]
* </code></pre>
* 
* @access   public
* @param	int		$nLimit			Anzahl angezeigter Threads
* @param	boolean	$nPostingOnly	sollen nur neue (Erst-)Eintraege ermittelt werden (optional - default: false)
* @return	array
*/
	function getRecentSubscriptions($uid, $nLimit=10, $nPostingOnly=false) {
		// vars
		global $aENV;
		$buffer = array();
		$sAddConstraint = '';
		// ggf. nur die fuer den aktuellen user erlaubten datensaetze anzeigen
		if (count($this->aAuthedTreeIds) > 0) {
			$sAddConstraint = 'tree_id IN ('.implode(',', $this->aAuthedTreeIds).')';
		}

		if ($nPostingOnly == true) {
			// query
			if ($sAddConstraint != '') $sAddConstraint = ' AND '.$sAddConstraint;
			$sQuery = 'SELECT `'.$this->sTable.'`.`id`,`tree_id`,`parent_id`,`headline`,`keywords`,`img_id`,`created`,`created_by`,`last_modified`,`last_mod_by` 
						FROM `'.$this->sTable.'`
						INNER JOIN frm_subscription ON `'.$this->sTable.'`.`id`=`frm_subscription`.`fkid`		
						WHERE `parent_id` = 0 AND uid='.$uid.' '.$sAddConstraint.' 
						ORDER BY `created` DESC';
			$this->oDb->limit($sQuery, 0, $nLimit); // sql, start, limit
			while ($row = $this->oDb->fetch_array()) {
				$buffer[] = array(
					'id'			=> $row['id'],
					'tree_id'		=> $row['tree_id'],
					'parent_id'		=> $row['parent_id'],
					'headline'		=> $row['headline'],
					'keywords'		=> $row['keywords'],
					'img_id'		=> $row['img_id'],
					'created'		=> $row['created'],
					'created_by'	=> $row['created_by'],
					'last_modified'	=> $row['last_modified'],
					'last_mod_by'	=> $row['last_mod_by']
				);
			}
		} else {
			$aPostingId = array();
			$i = 0;
			// query
			if ($sAddConstraint != '') $sAddConstraint = ' AND '.$sAddConstraint;
			$sQuery = 'SELECT `'.$this->sTable.'`.`id`,`tree_id`,`parent_id`,`headline`,`keywords`,`img_id`,`created`,`created_by`,`last_modified`,`last_mod_by` 
						FROM `'.$this->sTable.'` 
						INNER JOIN frm_subscription ON `'.$this->sTable.'`.`id`=`frm_subscription`.`fkid`		
						WHERE `parent_id` = 0 AND uid='.$uid.' '.$sAddConstraint.' 
						ORDER BY `created` DESC';

			$this->oDb->query($sQuery); // hier KEIN limit
			while ($row = $this->oDb->fetch_array()) {
				// posting-IDs sammeln
				if ($row['parent_id'] == 0) {
					// Beitrag nur hinzufuegen wenn noch nicht dabei
					if (!in_array($row['id'], $aPostingId)) {
						$aPostingId[] = $row['id'];
					} else {
						continue;
					}
				} else {
					// Kommentar nur hinzufuegen wenn posting noch nicht dabei ist
					if (!in_array($row['parent_id'], $aPostingId)) {
						$aPostingId[] = $row['parent_id'];
					} else {
						continue;
					}
				}
				$buffer[] = array(
					'id'			=> $row['id'],
					'tree_id'		=> $row['tree_id'],
					'parent_id'		=> $row['parent_id'],
					'headline'		=> $row['headline'],
					'keywords'		=> $row['keywords'],
					'img_id'		=> $row['img_id'],
					'created'		=> $row['created'],
					'created_by'	=> $row['created_by'],
					'last_modified'	=> $row['last_modified'],
					'last_mod_by'	=> $row['last_mod_by']
				);
				// Anzahl Postings hochzaehlen und bei Limit aufhoeren
				if (++$i >= $nLimit) {
					break(0);
				}
			}
		}
		// Zusatzaktionen
		for ($i = 0; $i< count($buffer); $i++) {
			// Kommentar gegen seinen Originalbeitrag ersetzen
			if ($nPostingOnly == false && $buffer[$i]['parent_id'] > 0) {
				$buffer[$i] = $this->getEntry($buffer[$i]['parent_id']); // params: $id
			}
			// Zusatzvariable: Anzahl Kommentare
			$buffer[$i]['comments'] = $this->countComments($buffer[$i]['id']);
		}
		return $buffer;
	}

/** 
* ermittelt alle Postings (nur 1. ebene + nur die wesentlichen Infos fuer eine Uebersicht) mit diesem Suchbegriff.
* Optional kann auf eine nTreeId (Thread) eingeschraenkt werden.
* und ermittelt vieviele Antworten zum jew. Eintrag existieren.
* 
* <pre><code> 
* $aData = $oForum->getSearchResult($frmSearchterm, $frmThread); // params: $sSearchtext[,$nTreeId]
* </code></pre>
* 
* @access   public
* @param	string	$sSearchtext	Suchbegriff
* @param	int		$nTreeId		ID des Verzeichnisses (optional - default: NULL)
* @return	array
*/
	function getSearchResult($sSearchtext, $nTreeId=NULL) {
		// vars
		if (is_null($nTreeId)) $nTreeId = 'all'; // Postings aus allen Threads
		$buffer = array();
		$aPostingId = array();
		// wenn kein suchbegriff -> direkte query (ist schneller!)
		if (empty($sSearchtext)) {
			if ($nTreeId == 'new') {
				return $this->getRecent(); // params: [$nLimit=10][,$nPostingOnly=false]
			} else {
				return $this->getPostings($nTreeId); // params: $nTreeId
			}
		}
		// SEARCH
		$this->oDb->soptions['select']	= "id, tree_id, parent_id, headline, keywords, flag_with_comments, created, created_by";
		if ($nTreeId != 'all') {
			// wenn auf einen Thread eingeschraenkt wurde
			global $oTree;
			if (is_object($oTree)) {
				// alle Unterordner ermitteln
				$aTreeId = $oTree->getAllChildrenId($nTreeId);
				if (count($this->aAuthedTreeIds) > 0) {
					// ggf. die rausschmeissen, die f.d. aktuellen user nicht authed sind
					$tmp = array();
					foreach ($aTreeId as $tid) {
						if (in_array($tid, $this->aAuthedTreeIds)) $tmp[] = $tid;
					}
					$aTreeId = $tmp;
				}
				$aTreeId[] = $nTreeId; // + sich selbst auch dazu
				// einen bestimmten folder (inkl. aller unterordner)
				$this->oDb->soptions['extra']	= 'tree_id IN ('.implode(',', $aTreeId).')';
			} else {
				// sonst einfach nur diesen ordner (fallback, wenn $oTree kein objekt ist)
				$this->oDb->soptions['extra']	= 'tree_id = '.$nTreeId;
			}
		} else {
			// wenn alles durchsucht werden soll, aber aAuthedTreeIds gefuellt ist
			if (count($this->aAuthedTreeIds) > 0) {
				// ggf. nur die f.d. user erlaubten folder
				$this->oDb->soptions['extra']	= 'tree_id IN ('.implode(',', $this->aAuthedTreeIds).')';
			}
		}
		#$this->oDb->soptions['goperator']		= "OR"; // default: AND
		#$this->oDb->soptions['search_umlauts']	= true;
		#$this->oDb->soptions['passive_mode']	= true;
		$this->oDb->make_search("searchtext", $sSearchtext, $this->sTable, 'created DESC');
		while ($row = $this->oDb->fetch_array()) {
			if ($row['parent_id'] == 0) {
				if(!in_array($row['id'], $aPostingId)) {
					$aPostingId[] = $row['id'];
				} else {
					continue;
				}
			} else {
				if (!in_array($row['parent_id'], $aPostingId)) {
					$aPostingId[] = $row['parent_id'];
				} else {
					continue;
				}
			}
			$buffer[] = array(
				'id'			=> $row['id'],
				'tree_id'		=> $row['tree_id'],
				'parent_id'		=> $row['parent_id'],
				'headline'		=> $row['headline'],
				'keywords'		=> $row['keywords'],
				'flag_with_comments'		=> $row['flag_with_comments'],
				'created'		=> $row['created'],
				'created_by'	=> $row['created_by']
			);
		}
		// Zusatzvariable: Anzahl Threads ermitteln (has children?)
		for ($i = 0; $i< count($buffer); $i++) {
			// wenn suchtreffer ein kommentar -> hier parent posting ermitteln
			if ($buffer[$i]['parent_id'] > 0) {
				$buffer[$i] = $this->getEntry($buffer[$i]['parent_id']); // params: $id
			}
			// Zusatzvariable: Anzahl Kommentare
			$buffer[$i]['comments'] = $this->countComments($buffer[$i]['id']);
		}
		return $buffer;
	}

# -----------------------------------------------------------------------------------

/** 
* setzt anzahl verschachtelungstiefe
* 
* @access   public
* @param	int		verschachtelungstiefe
*/
	function setMaxLevel($nMaxLevel) {
		if (empty($nMaxLevel)) return false;
		return $this->nMaxLevel = $nMaxLevel;
	}

/** 
* ermittelt insgesamte anzahl verschachtelungstiefe.
* 
* @access   public
* @return	int		verschachtelungstiefe
*/
	function countLevel() {
		return $this->nCountLevel;
	}

/** 
* ermittle Gesamtanzahl Eintraege.
* 
* <pre><code> 
* echo $oForum->countAll(); // params: -
* </code></pre>
* 
* @access   public
* @return	int		Gesamtanzahl Eintraege
*/
	function countAll() {
		$this->oDb->query('SELECT `id` FROM `'.$this->sTable.'`');
		return $this->oDb->num_rows();
	}

/** 
* ermittle Anzahl Eintraege einer Kategorie (nur Eintraege, ohne Kommentare!).
* 
* <pre><code> 
* echo $oForum->countPostings($tree_id); // params: $nTreeId
* </code></pre>
* 
* @access   public
* @param	int		$nTreeId		nTreeId
* @return	int		Anzahl Eintraege einer Kategorie
*/
	function countPostings($nTreeId) {
		if (empty($nTreeId)) return false;
		$this->oDb->query('SELECT `id` FROM `'.$this->sTable.'` WHERE `tree_id` = '.$nTreeId);
		return $this->oDb->num_rows();
	}

/** 
* ermittle Anzahl Kommentare eines Beitrages.
* 
* <pre><code> 
* echo $oForum->countComments($parent_id); // params: $nParentId
* </code></pre>
* 
* @access   public
* @param	int		$nParentId		ID des Beitrages, dessen Kommentare gezaehlt werden sollen
* @return	int		anzahl eintraege
*/
	function countComments($nParentId) {
		if (empty($nParentId)) return false;
		$this->oDb->query('SELECT `id` FROM `'.$this->sTable.'` WHERE `parent_id` = '.$nParentId);
		return $this->oDb->num_rows();
	}

/**
* Hilfs-Funktion: Gebe nur die IDs der Kommentare eines Beitrages als flaches (numerisches) Array zurueck. 
* 
* @access   private
* @param	int		$parentId	ID dessen Kinder-IDs gesucht werden
* @return	array
*/
	function getCommentIds($nParentId) {
		if (empty($nParentId)) return false;
		// init vars
		$buffer = array();
		// query
		$this->oDb->query("SELECT `id` FROM `".$this->sTable."` WHERE `parent_id` = ".$nParentId." ORDER BY `id` ASC");
		while ($row = $this->oDb->fetch_array()) {
			$buffer[] = $row['id'];
		}
		// output
		return $buffer;
	}

# -----------------------------------------------------------------------------------

/** 
* ermittle aktuellen eintrag (select by id)
* 
* <pre><code> 
* $aData = $oForum->getEntry(127); // params: $id
* print_r($data);
* </code></pre>
* 
* @access   public
* @param	int		$id		ID des Datensatzes
* @return	array
*/
	function getEntry($id) {
		// vars
		if (!$id) return false;
		$buffer = array();
		$row = $this->oDb->fetch_by_id($id, $this->sTable);
		if ($this->oDb->num_rows() < 1) return $buffer;
		$buffer['id']			= $id;
		$buffer['tree_id']		= $row['tree_id'];
		$buffer['parent_id']	= $row['parent_id'];
		$buffer['headline']		= $row['headline'];
		$buffer['content']		= $row['content'];
		$buffer['keywords']		= $row['keywords'];
		$buffer['img_id']		= $row['img_id'];
		$buffer['downloads']	= $row['downloads'];
		$buffer['flag_edit_all']= $row['flag_edit_all'];
		$buffer['flag_with_comments']	= $row['flag_with_comments'];
		$buffer['created']		= $row['created'];
		$buffer['created_by']	= $row['created_by'];
		$buffer['last_modified']= $row['last_modified'];
		$buffer['last_mod_by']	= $row['last_mod_by'];
		// zusatzvariable: Anzahl Kommentare
		$buffer['comments'] = $this->countComments($id);
		// output
		return $buffer;
	}

/** 
* fuer Forum abstrahierter "make_insert()" bzw. "make_update()" (kuemmert sich selbst darum).
* 
* Beispiel: 
* <pre><code> 
* $aData = array();
* $aData['nTreeId'] = 1;
* $aData['parent_id'] = 3;
* $aData['headline'] = "test-comment 2.1";
* $aData['content'] = "test kommentar test text 2.1";
* $aData = $oForum->saveEntry($aData, $Userdata['id']); // params: $aData[,user_id=0]
* echo "kommentar mit der ID ".$aData['id']." eingefuegt.";
* </code></pre>
* 
* @access   public
* @param	array	$aData		zu speichernder datensatz (mindestens: $aData['nTreeId']!)
* @param	int		$user_id	ID des Users
* @return	int		ID des eingefuegten Datensatzes
*/
	function saveEntry($aData, $user_id=0) {
		// ggf. Sonderzeichen- und TAG-Behandlung des Flash-Editors mittels Ersetzungen ausgleichen
		$this->_replace_flasheditor_specials($aData);
		
		// check vars
		if (!isset($aData['tree_id']))		{ $aData['tree_id'] = 0; }
		if (!isset($aData['parent_id']))	{ $aData['parent_id'] = 0; }
		// set searchtext
		$aData['searchtext']	= $aData['headline'].' '.$aData['keywords'].' '.$aData['content'];
		if (isset($aData['id']) && !empty($aData['id'])) {
		// Update
			// set last_mod_by
			$aData['last_mod_by'] = $user_id;
			// make update
			$this->oDb->make_update($aData, $this->sTable, $aData['id']);
		} else {
		// Insert
			// set created
			$aData['created']		= date("YmdHis");
			$aData['created_by']	= $user_id;
			// make insert
			$this->oDb->make_insert($aData, $this->sTable);
			// get last inserted id
			$aData['id']			= $this->oDb->insert_id();
		}
		// return aData
		return $aData;
	}

/** 
* fuer Forum abstrahierter "make_delete()".
* 
* Beispiel: 
* <pre><code> 
* $oForum->deleteEntry($id); // params: $id
* </code></pre>
* 
* @access   public
* @param	int		$id		ID des zu loeschenden Datensatzes
* @return	void
*/
	function deleteEntry($id) {
		// check vars
		if (empty($id))	return;
		// make delete
		$this->oDb->make_delete($id, $this->sTable);
		// alle Kommentare ermitteln
		$aIdsToDelete = $this->getCommentIds($id);
		if (count($aIdsToDelete) > 0) {
			// wenn Kommentare -> dann auch diese loeschen!
			$this->oDb->query('DELETE FROM `'.$this->sTable.'` WHERE `id` IN('.implode(',', $aIdsToDelete).')');
		}
	}

# -----------------------------------------------------------------------------------

/** 
* gebe eine Headline "zitiert" zurueck (also mit 'Re:' davor).
* 
* <pre><code> 
* $sQuotedHeadline = $oForum->quoteHeadline($aData['headline']); // params: $sHeadline
* </code></pre>
* 
* @access   public
* @param	string		$string
* @return	string
*/
	function quoteHeadline($sHeadline) {
		// vars
		if (empty($sHeadline)) return false;
		return 'Re: '.$sHeadline;
	}

/** 
* gebe einen Text "zitiert" zurueck (also nach $nSplitLength-Zeichen (+ nach dem naechsten 
* leerzeichen) umgebrochen und mit '>'-Zitatzeichen am Anfang jeder Zeile).
* 
* <pre><code> 
* $sQuotedContent = $oForum->quoteContent($aData['content']); // params: $sContent[,$nSplitLength=40][,$bStripTags=true]
* </code></pre>
* 
* @access   public
* @param	string		$sContent
* @param	int			[$nSplitLength]		anzahl zeichen nach denen umgebrochen wird (optional - default: 40)
* @param	boolean		[$bStripTags]		sollen HTML-Tags entfernt werden (optional - default: true)
* @return	string
*/
	function quoteContent($sContent, $nSplitLength=40, $bStripTags=true) {
		// vars
		if (!$sContent) return false;
		if ($bStripTags == true) {
			$sContent = strip_tags($sContent);
		}
		$nl = "\n";
		$quote_chr = '>';
		$newstring = '';
		$tmp = array();
		$str_array = explode($nl, $sContent);
		foreach ($str_array as $val) {
			if (substr($val, 0, 1) == $quote_chr) {
				// schon zitiert -> also nur $quote_chr davor!
				$tmp[] = $quote_chr.$val;
			} else {
				// generate wordwrap
				$newstring = wordwrap($val, $nSplitLength, $nl);
				$str_array = explode($nl, $newstring);
				$newstring = implode($nl.$quote_chr.' ', $str_array);
				$tmp[] = $quote_chr.' '.$newstring;
			}
		}
		return implode($nl, $tmp);
	}
/**
* Gibt die IDs aller Unterseiten eines Datensatzes zurueck (nur sub & subsub & subsubsub!).
*
* @access   public
* @global	array	$aENV
* @global	object	$oDb
* @param	string	$id		(ID des Datensatzes in der Table)
* @param	string	$sTable (Name der Content-Table)
* @return	array	IDs aller Unterpunkte (max. 3 Unterebenen!)
*/
	function returnAllSubIds($id, $sTable,&$aENV) {
		
		// vars
		$aID = array();
		$aSubID = array();
		
		// get subs
		$this->oDb->query("SELECT id FROM ".$sTable." WHERE parent_id='".$id."' AND flag_tree='frm'");
		while ($aData = $this->oDb->fetch_array()) {
			$aID[] = $aData['id'];
		}
		// get subsubs
		$sIDs = implode(",", $aID);
		if (!empty($sIDs)) {
			$this->oDb->query("SELECT id FROM ".$sTable." WHERE parent_id IN (".$sIDs.") AND flag_tree='frm'");
			while ($aData = $this->oDb->fetch_array()) {
				$aSubID[] = $aData['id'];
			}
		}
		// get subsubsubs
		$sSubIDs = implode(",", $aSubID);
		if (!empty($sSubIDs)) {
			$this->oDb->query("SELECT id FROM ".$sTable." WHERE parent_id IN (".$sSubIDs.") AND flag_tree='frm'");
			while ($aData = $this->oDb->fetch_array()) {
				$aSubID[] = $aData['id'];
			}
		}
		$aID = array_merge($aID, $aSubID);
		return $aID;
	}
# -----------------------------------------------------------------------------------

	/**
	 * 
	 * Speichert eine Subscription ab oder Löscht diese. Dies ist abhängig davon ob sie bereits vorhanden ist in der Tabelle.
	 * @param array $aData
	 * @return bool
	 * @access public
	 */
	function saveSubscribtion($aData) {
		
		$sTable = 'frm_subscription';
		$sql = "SELECT id FROM ".$sTable." WHERE fkid='".$aData['id']."' AND uid='".$aData['uid']."'";
		$this->oDb->query($sql);
		if($this->oDb->num_rows() != 0) {
			return $this->oDb->make_delete(array('fkid' => $aData['id'],'uid' => $aData['uid']),$sTable);
		}
		else {
			$sql = "INSERT INTO $sTable (`fkid`,`uid`) VALUES ('".$aData['id']."','".$aData['uid']."')";
			return $this->oDb->query($sql);
		}
	}

	/**
	 * 
	 * Liefert den Abonnementstatus eines Beitrags
	 * @param int $fkid
	 * @param int $uid
	 * @return bool
	 * @access public
	 * 
	 */
	function getSubscribeStatus($fkid,$uid) {
		$sql = "SELECT * FROM frm_subscription WHERE fkid='$fkid' AND uid='$uid'";
		$this->oDb->query($sql);
		$num = $this->oDb->num_rows();
		return ($num == 0) ? false : true;
	}
	
/**
* Private Funktion, die ggf. Ersetzungen direkt in den Daten vornimmt, die aus dem Flash-Texteditor kommen.
*
* @access   private
* @global	string	$weblang			Ausgabesprache
* @param	array	$aData			Datensatz (value by reference)
* @return	void
*/
	function _replace_flasheditor_specials(&$aData) {
		$anz = count($this->aFlashFieldname);
		if ($anz == 0) return; // funktioniert komischerweise nicht in einer Zeile!?!
		
		for ($i = 0; $i < $anz; $i++) {
			$aData[$this->aFlashFieldname[$i]] = str_replace(	$this->aFlashSearch, 
																$this->aFlashReplace, 
																$aData[$this->aFlashFieldname[$i]]);
		}
	}
} // END of class
?>