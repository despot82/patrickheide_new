<?php
/**
 * Memo Klasse: Diese Klasse stellt einen vereinfachten Zugriff auf die Memos bereit.
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @version 1.0
 * @param dbconnect $oDb
 */

class Memo {

	private $oDb = null;
	private $mod = '';
	private $debug = false;
	
/**
* @access   private
* @var	 	array	Zeichen, die aus dem Flash-Texteditor rauskommen und vor dem speichern in der DB ersetzt werden muessen
*/
	var $aFlashSearch;
/**
* @access   private
* @see      $aFlashSearch
* @var	 	array	Zeichen, die eingesetzt werden
*/
	var $aFlashReplace;
	
	
/**
 * Konstruktor initialisiert das DB Object und setzt eine defaulteinstellung
 * @access public
 * @param db_connect $oDb
 */
	public function Memo($oDb) {
		$this->oDb = clone $oDb;
		$this->mod = 'memo';
		
		// Flash-Texteditor
		$this->aFlashFieldname	= array('memo'); // default: KEIN Flash-Texteditor
		$this->aFlashSearch		= array('&lt;', '&gt;', '&amp;', '&quot;', '&apos;');
		$apos = (!get_magic_quotes_gpc()) ? "'" : "\'";
		$this->aFlashReplace	= array('<', '>', '&', '"', $apos);
	}

	private function getDelete() {
		if(is_null($this->aDeleted)) {
			$sql = "SELECT * FROM `sys_memo_options` WHERE `flag_deleted`='1'";
			$this->oDb->query($sql);
			$aDel = array();
			for($i=0;$row = $this->oDb->fetch_object();$i++) {
				$aDel[$row->fkid][] = $row->uid;
			}
		}
		return $aDel;
	}

	/**
	 * Diese Methode sortiert die Participants neu und versieht die User die die Memo bereits gelöscht haben mit einem (-)
	 * 
	 * @param int $fkid
	 * @param object $oTeam
	 * @param object $oUser
	 * @param object $oTACEA
	 * @return String
	 * @access public
	 */
	public function getFormatedParticipants($fkid,$oTeam,$oUser,$oTACEA) {
		$aParticipant = $oTACEA->getParticipants($oTeam,$oUser,'memo',$fkid,true);
		$aDel = $this->getDelete();
		foreach($aParticipant as $ident => $ut) {
			foreach($ut as $id => $name) {
				if($ident == 'uid') {
					if(is_array($aDel[$fkid])) {
						if(in_array($id,$aDel[$fkid])) {
							$aParti[] = '(-)'.$name;
						}
						else {
							$aParti[] = $name;
						}
					}
					else {						
						$aParti[] = $name;
					}
				}
				else {
					$aParti[] = $name;
				}
			}
		}
		if(is_array($aParti)) {
			$sReturn = implode(', ',$aParti);
		}
		else {
			$sReturn = false;
		}
		return $sReturn;
	}
		
/**
 * Diese Methode setzt ein Loeschflag in der sys_memo_options Tabelle. 
 * Diese Flags werden dann erst zurueckgesetzt wenn alle Memoteilnehmer dieses Flag erhalten haben.
 * @access public
 * @param int $uid
 * @param int $fkid
 * @param int $prio
 * @return Boolean
 */
	public function setDelete($uid,$fkid,$prio=0) {
	
		$sql = "INSERT INTO `sys_memo_options` (`uid`,`fkid`,`mod`,`prio`,`flag_deleted`) ";
		$sql .= "VALUES ('".$uid."','".$fkid."','".$this->mod."','".$prio."','1')";

		return $this->oDb->query($sql);
	}

/**
 * Diese Methode ueberprueft ob ein User in einem Authorisiertem Team ist.
 * @access public
 * @param int $uid
 * @param int $fkid
 * @param TeamAccessControllEA $oTACEA
 * @param team $oTeam
 * @return Boolean
 */
	public function isUserInAuthedTeam($uid,$fkid,$oTACEA,$oTeam) {
		$this->oDb->debug = $this->debug;
		$user = $oTACEA->getRightForID($fkid,$this->mod);
		$ug = array();

		if(is_array($user)) {
			foreach($user as $userid => $val) {
				if($uid == $userid) {
					$ug[] = $userid;
					$users[$userid] = $val;
				}
			}
		}
		
		$sql = "SELECT `id` 
				FROM `sys_memo_options` 
				WHERE `fkid` = '".$fkid."' 
					AND `mod` = '".$this->mod."' 
					AND `uid` = '".$uid."'";
		$this->oDb->query($sql);
		$num = $this->oDb->num_rows();
		//echo "num:$num<br>ug: $uid = ".$ug[0]; // DEBUG
		
		if($num == 0 && isset($ug[0]) && $ug[0] == $uid) {
			return true;
		}
		else {
			return false;	
		}
	}

/**
 * Speichert eine Memo mit allen Werten und Rechten.
 * @access public
 * @param array $aData
 * @param array $aENV
 * @param array $Userdata
 * @param TeamAccessControllEA $oTACEA
 * @return array
 */
	public function save($aData,$aENV,$Userdata,$oTACEA) {
		// ggf. Sonderzeichen- und TAG-Behandlung des Flash-Editors mittels Ersetzungen ausgleichen
		$this->_replace_flasheditor_specials($aData);
		
		$group = $aData['group'];
		$user = $aData['user'];
		unset($aData['group']);
		unset($aData['user']);
		
		if(empty($group) && empty($user)) $user = $Userdata['id'];

		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$this->oDb->make_update($aData, $aENV['table']['sys_memo'], $aData['id']);
			$this->oDb->make_delete(array('fkid' => $aData['id'], 'mod' => 'memo'),'sys_memo_options');
		} else { // Insert eines neuen Eintrags
			global $oDate;
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			// make insert
			$this->oDb->make_insert($aData, $aENV['table']['sys_memo']);
			$aData['id'] = $this->oDb->insert_id();
		}
			
		$oTACEA->addRights($group, $user,$aData['id'],'memo',false,$Userdata['id']);
		return $aData;	
	}

/**
 * Löscht eine Memo
 * @access public
 * @param int $delete_id
 * @param TeamAccessControllEA $oTACEA
 * @param array $aENV
 * @param array $Userdata
 */
	public function doDelete($delete_id,$oTACEA,$aENV,$Userdata) {
		$user = $oTACEA->getRightForID($delete_id,'memo');
		$ug = array();
		
		if(is_array($user)) {
			foreach($user as $uid => $val) {
				if($uid != $Userdata['id']) {
					$ug[] = $uid;
					$users[$uid] = $val;
				}
			}
		}
		$this->setDelete($Userdata['id'],$delete_id);
		
		$sql = "SELECT `uid` 
				FROM `sys_memo_options` 
				WHERE `fkid` = '".$delete_id."' 
					AND `mod` = 'memo' 
					AND `flag_deleted` = '1'";
		$this->oDb->query($sql);
		
		for($i=0;$row = $this->oDb->fetch_object();$i++) {
			if(in_array($row->uid,$ug)) {
				$key = array_search($row->uid,$ug);
				if($key !== false) {
					unset($ug[$key]);
				}
			}
		}
		
		$aData['authorisation'] = implode(',',$ug);
		if (empty($aData['authorisation'])) {
		// wenn keiner mehr drin -> datensatz loeschen
			$oTACEA->onDeleteAction($delete_id,'memo');
			$this->oDb->make_delete($delete_id, $aENV['table']['sys_memo']);
			// Raeumen wir in dem Saustall mal auf...
			$this->clearMemo($delete_id);
			$oTACEA->delRightForTree($delete_id,'memo');
		} 	
	}
	
/**
 * Diese Methode loescht Eintraege aus der sys_memo_options. 
 * @access public
 * @param int $fkid
 * @return Boolean
 */
	public function clearMemo($fkid) {
	
		$sql = "DELETE 
				FROM `sys_memo_options` 
				WHERE `fkid` = '".$fkid."' 
					AND `mod` = '".$this->mod."'";
		
		return $this->oDb->query($sql);
	}

/**
 * Diese Methode fragt alle Informationen ueber eine Memo ab und liefert ein Array zurueck.
 * @access public
 * @return array
 */
	public function getMemos() {
		// DB: get memos
		$sql = "SELECT `id`,`memo`,`prio`,`last_modified`,`last_mod_by`,`created`,`created_by` 
				FROM `sys_memo` 
				ORDER BY `last_modified` DESC";
		$this->oDb->query($sql);
		
		for($i=0; $row = $this->oDb->fetch_object(); $i++) {
			foreach($row as $field => $val) {
				$memos[$i][$field] = $val;
			}	
		}
		
		return $memos;
	}

/**
* Private Funktion, die ggf. Ersetzungen direkt in den Daten vornimmt, die aus dem Flash-Texteditor kommen.
*
* @access   private
* @global	string	$weblang			Ausgabesprache
* @param	array	$aData			Datensatz (value by reference)
* @return	void
*/
	function _replace_flasheditor_specials(&$aData) {
		$anz = count($this->aFlashFieldname);
		if ($anz == 0) return; // funktioniert komischerweise nicht in einer Zeile!?!
		
		for ($i = 0; $i < $anz; $i++) {
			$aData[$this->aFlashFieldname[$i]] = str_replace(	$this->aFlashSearch, 
																$this->aFlashReplace, 
																$aData[$this->aFlashFieldname[$i]]);
		}
	}
} // END class
?>