<?
/**
* Diese Klasse vereinfacht die Funktionalitaeten, die in dieser Seite gebraucht werden. 
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.02 / 2006-05-24 [NEU: Mehrsprachige Containerfiles]
* #history	1.0 / 2004-10-20
*/
class CmsNaviAdmin {
	// Class Vars
	var $aENV		= array();		// System-Variable
	var $aMSG		= array();		// System-Variable
	var $syslang	= '';			// System-Variable
	var $oNav		= NULL;			// System-Variable

/**
* Konstruktor -> CMS-spezialisiert: Initialisiert das object
*
* @global	object	$oDb				Datenbank-Objekt, welches fuer die Klasse "class.db_mysql.php" benoetigt wird
* @access   public
*/
	function cmsNaviAdmin() {
		// system-vars importieren
		global $aENV, $aMSG, $syslang, $weblang, $oNav;
		$this->aENV				= $aENV;
		$this->aMSG				= $aMSG;
		$this->syslang			= $syslang;
		$this->lang				= $weblang;
		$this->oNav				= $oNav;
	}

# -------------------------------------------------------------------------------------------- FORMULAR

/** Ausgabe-Funktion: Tree-Array (ersten 2 Ebenen) automatisiert als Drop-Down-Formfeld zurueckgeben
* 
* @access   public
* @param	array	$aData			aktueller Datensatz
* @param	int		$maxLevel		(optional) Anzahl Ebenen (defaut: 2)
* @return	HTML
*/
	function getAssignParentSelect(&$aData, $maxLevel=2) {
		global $id;
		// NAVI-KLASSE initialisieren
		$this->oNav->loadNavi($aData['id'], $this->syslang, false); // params: $nNaviId[,$sLang='de'][,$bValidOnly=true]
		// welche navi
		$flag_topnavi = (isset($_GET['flag_topnavi']) && !empty($_GET['flag_topnavi'])) ? strip_tags($_GET['flag_topnavi']) : $aData['flag_topnavi'];
		if (empty($flag_topnavi)) $flag_topnavi = 0; // fallback
		// parent
		$aData['parent_id'] = (isset($_GET['parent_id']) && !empty($_GET['parent_id'])) ? strip_tags($_GET['parent_id']) : $aData['parent_id'];
		if (empty($aData['parent_id'])) $aData['parent_id'] = 0; // fallback
		
		// get NAVI
		$aStructure = $this->oNav->_getAllChildren('0', $flag_topnavi); // params: $parentId,$flag_topnavi
		#if (count($aStructure) == 0) { return; }
		$buffer	= array();
		
		// start tag
		$buffer[] = '<select name="aData[parent_id]" size="1">';
		// first option
		$sel = ($aData['parent_id'] == '0') ? ' selected' : ''; // default highlight
		$buffer[] = '<option value="0"'.$sel.'>--- '.$this->aMSG['navi']['new_section'][$this->syslang].' ---'."</option>";
		// tree options
		if ($maxLevel > 0) {
			foreach($aStructure as $kMain => $aData2) {
				if ($id == $aData2['id']) continue; // nicht noch mal sich selbst anzeigen!
				// highlight
				$sel = ($aData['parent_id'] == $aData2['id']) ? ' selected' : '';
				$buffer[] = '<option value="'.$aData2['id'].'"'.$sel.'>'.$aData2['title_'.$this->lang]."</option>";
				if ($maxLevel > 1) {
					foreach($aData2['children'] as $kSub => $aData3) {
						if ($id == $aData3['id']) continue; // nicht noch mal sich selbst anzeigen!
						// highlight
						$sel = ($aData['parent_id'] == $aData3['id']) ? ' selected' : '';
						$buffer[] = '<option value="'.$aData3['id'].'"'.$sel.'>- '.$aData3['title_'.$this->lang]."</option>";
						if ($maxLevel > 2) {
							foreach($aData3['children'] as $kSubSub => $aData4) {
								// highlight
								$sel = ($aData['parent_id'] == $aData4['id']) ? ' selected' : '';
								$buffer[] = '<option value="'.$aData4['id'].'"'.$sel.'>- - '.$aData4['title_'.$this->lang]."</option>";
							} // END foreach
						} // END if $maxLevel > 2
					} // END foreach
				} // END if $maxLevel > 1
			} // END foreach
		} // END if $maxLevel > 0
		// end tag
		$buffer[] = '</select>';
		// output
		return implode("\n", $buffer)."\n";
	}

/** Ausgabe-Funktion: Die veschiedenen Navi-Bereiche automatisiert als Drop-Down-Formfeld zurueckgeben
* 
* @access   public
* @param	array	$aData			aktueller Datensatz
* @param	string	$_GET['flag_topnavi']	(optional)
* @return	HTML
*/
	function getAssignNaviSelect(&$aData) {
		// start tag
		$buffer = '<select name="aData[flag_topnavi]" size="1">';
		// options
		foreach($this->aENV['content_navi'] as $key => $val) {
			$sel = ($key == $aData['flag_topnavi'] || $key == $_GET['flag_topnavi']) ? ' selected' : '';
			$buffer .= '	<option value="'.$key.'"'.$sel.'>'.$val[$this->syslang].'</option>'."\n";
		}
		// end tag
		$buffer .= '</select>'."\n";
		// output
		return $buffer;
	}

/** Ausgabe-Funktion: build select-field for subsections, if page is not shown on the navigation
* 
* @access   public
* @global	object	$oDb			DB-Object
* @param	array	$aData			aktueller Datensatz
* @return	HTML
*/
	function getAssignReferenceSelect(&$aData) {
		global $oDb;
		// start tag
		$buffer = '<select name="aData[reference_id]" size="1">';
		// options
		$sql	=  "SELECT id, title_".$this->lang." 
					FROM ".$this->aENV['table']['cms_navi']." 
					WHERE parent_id='".$aData['parent_id']."' 
						AND flag_navi='1' 
						AND template != 'no_page' 
					ORDER BY prio DESC";
		$oDb->query($sql);
		while ($aData2 = $oDb->fetch_array()) {
			$sel = ($aData2['id'] == $aData['reference_id']) ? ' selected' : '';
			$buffer .= '<option value="'.$aData2['id'].'"'.$sel.'>'.$aData2['title_'.$this->lang].'</option>'."\n";
		}
		$sel = ('0' == $aData['reference_id']) ? ' selected' : '';
		$buffer .= '<option value="0"'.$sel.'>-- no highlighting --</option>'."\n";
		// end tag
		$buffer .= '</select>'."\n";
		// output
		return $buffer;
	}

/** Hilfs-Funktion: Das Template des Parent-Navipunktes ermitteln und zurueckgeben
* 
* @access   public
* @global	object	$oDb			DB-Object
* @param	array	$aData			aktueller Datensatz
* @return	HTML
*/
	function getParentTemplate(&$aData) {
		global $oDb;
		if (!is_object($oDb)) { return; }
		if (!is_array($aData) || empty($aData['parent_id'])) { return; }
		// get
		$sql	=  "SELECT template 
					FROM ".$this->aENV['table']['cms_navi']." 
					WHERE id='".$aData['parent_id']."'";
		$oDb->query($sql);
		$parent_template = ($oDb->num_rows()) ? $oDb->fetch_field('template') : '';
		
		// output
		return $parent_template;
	}

/** Hilfs-Funktion: Den Template-Namen ermitteln und zurueckgeben
* 
* @access   public
* @param	array	$aData			aktueller Datensatz
* @return	HTML
*/
	function getTemplateName(&$aData) {
		if (isset($this->aMSG['template'][$aData['template']][$this->syslang])) {
			// std-cms templates
			return $this->aMSG['template'][$aData['template']][$this->syslang];
		} elseif (isset($this->aENV['template'][$aData['template']][$this->syslang])) {
			// user-def templates
			return $this->aENV['template'][$aData['template']][$this->syslang];
		} else {
			// templatename der DB (fallback)
			return $aData['template'];
		}
	}

/** Hilfs-Funktion: Einen "guten" Datei-Namen fuer den PHP-Container ermitteln und zurueckgeben
* 
* @access   public
* @global	object	$oDb			DB-Object
* @global	object	$oFile			Filesystem-Object
* @param	array	$aData			aktueller Datensatz
* @return	string	Filename
*/
	function getContainerFilename($aData, $aCompare = array()) {
		global $oDb, $oFile;
		
		if ($aData['template'] == 'home') { // if container is "home"

			$aData['filename_'.$this->aENV['content_language']['default']]	= $this->aENV['web_home'][$this->aENV['content_language']['default']];
			
			foreach($this->aENV['content_language'] as $sKey => $sValue) {
				
				if($sKey != 'default' 
				&& $sKey != $this->aENV['content_language']['default']) {
					$aData['filename_'.$sKey] = $this->aENV['web_home'][$sKey];
				} // end if sKey != default && sKey != aENV default
				
			} // end foreach aENV content language
			
		} else { // else if template != home 
			// initialise array
			$aLastlang	= array();

			// foreach aENV content language
			foreach($this->aENV['content_language'] as $sKey => $sValue) {
				// don't need a filename if title is empty
				if(empty($aData['title_'.$sKey]))	continue;

				// dont't need the default language container
				if($sKey == 'default')				continue;

				// don't need to change the filename if isn't changed by user
				if(!empty($aCompare)
				&& !empty($aData['filename_'.$sKey])
				&& $aData['filename_'.$sKey].".php" == $aCompare['filename_'.$sKey])	continue;
				
				// initialise string
				$sContainerFilename	= '';
				if(empty($aData['filename_'.$sKey]))
					$sContainerFilename	.= strtok(trim($oFile->return_good_filename(strtolower($aData['title_'.$sKey]))), ' '); // erstes wort aus titel holen
				else
					$sContainerFilename	.= strtok(trim($oFile->return_good_filename(strtolower($aData['filename_'.$sKey]))), ' '); // erstes wort aus titel holen 

				$sContainerFilename	= str_replace('.', '', $sContainerFilename);

				// if sContainerFilename is in array aLastLang, add language key 
				if(in_array($sContainerFilename.'.php',$aLastlang)) { 
					$sContainerFilename .= '_'.$sKey;
				}
	
				$aData['filename_'.$sKey]	= $sContainerFilename.'.php';

				// if filename allready exists
				if($this->_filename_exists($aData['filename_'.$sKey],$sKey)) {
					$sFilename	= substr($aData['filename_'.$sKey], 0, -4);

					// if filename allready exists || length < 8
					if($this->_filename_exists($sFilename.'_'.$sKey.'.php', $sKey)
					|| empty($sFilename)) {
						
						for($i	= 1; $this->_filename_exists($aData['filename_'.$sKey], $sKey); $i++) {
							// if filename != unique, increment and add a counter
							$aData['filename_'.$sKey]	= strtok(strtok($aData['filename_'.$sKey], '_'), '."').'_'.$sKey.'_'.$i.'.php';
						} // end for
						
					} // end if filename == exists
					// else if filename != exists 
					else {
						$aData['filename_'.$sKey]	= strtok(strtok($aData['filename_'.$sKey], '_'), '."').'_'.$sKey.'.php';
					} // end else filename != exists

				} // end if _filename_exists
				
				$aLastlang[]	= $aData['filename_'.$sKey];
				
			} // end foreach aENV content language
			
		} // end else template != home
		
		// foreach aENV content language 
		foreach($this->aENV['content_language'] as $sKey => $sValue) {
			if($sKey == 'default')	continue;
			if(empty($aData['filename_'.$sKey]))	continue;
			
			// eregi no .php then add a .php to all
			$aData['filename_'.$sKey]	.= eregi('.php', $aData['filename_'.$sKey]) ? '':'.php';
		} 

		// output
		return $aData;
	}
	
	// check if filename already exists
	function _filename_exists($sFilename, $lang) {
		global $oDb;

		// if sFilename == empty return false
		if (empty($sFilename)) return;

		// if file exists return true
		if(file_exists('../../'.$sFilename))
			return true;
		
		// foreach aENV content language
		foreach($this->aENV['content_language'] as $sKey => $sValue) {
			if($sKey == 'default') continue;
			if($sKey == $lang) continue;
			$where[]	= "filename_".$sKey."='".trim($sFilename)."'";
			$fields[]	= "filename_".$sKey;
		} // end foreach aENV content language
		if(empty($fields))
			$fields[]	= "filename_".$lang;
		if(empty($where))
			$where[]	= "filename_".$lang."='".trim($sFilename)."'";
				
		$sql	= "SELECT ".implode(',',$fields)." 
				   FROM ".$this->aENV['table']['cms_navi']." 
				   WHERE ".implode(' OR ',$where);
		$oDb->query($sql);
		$nNum	= $oDb->num_rows();
		
		// return true || false
		return ($nNum != 0) ? true : false;
	}

	function writeAllContainer(&$oDb,$sContainerPath) {
		$sql = "SELECT * FROM ".$this->aENV['table']['cms_navi'];
		$oDb->query($sql);
		
		for($i=0;$aData[] = $oDb->fetch_array();$i++);
		$size = count($aData);
		unset($aData[($size-1)]); // den letzten eintrag killen!
		for($i=0;is_array($aData[$i]);$i++) {
			$this->createContainer($aData[$i],$sContainerPath,$oDb);
		}
	}
	
	function createAllContainer($sContainerPath) {
		global $oDb;
		$oDb2	= $oDb;
		
		if (isset($this->aENV['create_php_container']) 
		&& $this->aENV['create_php_container'] == false) {
			return;
		}

		// initialise filesystem object
		$oFile	= new filesystem();
		
		$sql = "SELECT * FROM ".$this->aENV['table']['cms_navi'];
		$oDb2->query($sql);
		
		while($aData	= $oDb2->fetch_array()) {
			$aResult[]	= $aData;
		}	
		
		foreach($aResult as $aData) {	
			if ($aData['template'] == 'no_page') continue;
			
			foreach ($this->aENV['content_language'] as $sKey => $sValue) {
				if ($sKey == 'default')	continue;
				if (empty($aData['filename_'.$sKey])) continue;
				
				$oFile->delete_files($sContainerPath.$aData['filename_'.$sKey]);
			}
			
			$this->createContainer($aData, $sContainerPath, $oDb);
		}
	}	

/** Hilfs-Funktion: Den Inhalt (PHP-Array) fuer den PHP-Container ermitteln und zurueckgeben
* 
* @access   public
* @global	object	$oDb				DB-Object
* @param	array	$aData				aktueller Datensatz
* @param	string	$sContainerPath		Pfad zum PHP-Container
* @return	string	PHP-Array-String
*/
	function getContainerString(&$aData) {
		global $oDb;
		
		if ($aData['template'] == 'no_page') { // if template == no page
			return; 
		}
		 
		$aTitleFields	= array();
		
		// reset aENV content language array pointer to 0
		reset($this->aENV['content_language']);

		foreach ($this->aENV['content_language'] as $sKey => $sValue) {
			if ($sKey == 'default') continue;
			$aTitleFields[]	= 'title_'.$sKey;
		} // end foreach aENV content language
		
		// build string
		$sContainerString	= '<?php // this page was created automatically by design aspekt Inform. Do not make any changes!'."\r\n";

		// navi id
		$sContainerString	.= '$sNaviId = "'.$aData['id'].'";'."\r\n";
		
		// foreach page array
		foreach($aData as $sKey => $val) {
			$sContainerString .= '$aPageInfo["'.$sKey.'"] = "'.htmlspecialchars($aData[$sKey], ENT_QUOTES).'";'."\r\n";
		} // end foreach page array
		
		// get title of parent section to create correct title-tag in inc.header
		$sql	= "SELECT ".implode(',', $aTitleFields)." 
				   FROM ".$this->aENV['table']['cms_navi']." 
				   WHERE id='".$aData['parent_id']."'";
		$oDb->query($sql);
		$aTemp	= $oDb->fetch_array();
		
		// foreach 
		foreach ($aTitleFields	as $sValue) {
			$sContainerString	.= '$aPageInfo["parent_'.$sValue.'"]	= "'.htmlspecialchars($aTemp[$sValue], ENT_QUOTES).'";'."\r\n"; // is left empty if parent_id=0
		} // end foreach title fields
		
		// include template/static-page
		if ($aData['template'] == 'static_page') {
			$sContainerString .= 'require_once ("static.'.$aData['filename_'.$this->aENV['content_language']['default']].'");'."\r\n";
		} else {
			$sContainerString .= 'require_once("template.'.$aData['template'].'.php");'."\r\n";
		}
		$sContainerString .= '?>';
		
		//output
		return $sContainerString;
	}

	/*
	 * 
	 */
	function createContainer($aData, $sContainerPath, $oDb) {

		// if aENV create php container != true return empty
		if (isset($this->aENV['create_php_container']) 
		&& $this->aENV['create_php_container'] != true) {
			return $aData;
		} // end if aENV create php container
		
		// if template != no page && aENV filename content language default != empty
		if ($aData['template'] != 'no_page' 
		 && $aData['filename_'.$this->aENV['content_language']['default']] != '') {

			// build container string
			$aData['weblang'] = $this->aENV['content_language']['default'];

			// get data for default language container
			$sContainerString	= $this->getContainerString($aData);
		
			// initialise file object
			$oFile	= new filesystem();
			
			// write container for default language
			if(!empty($aData['filename_'.$this->aENV['content_language']['default']]))			
				$oFile->write_str_in_file($sContainerPath.$aData['filename_'.$this->aENV['content_language']['default']], $sContainerString);

			// foreach aENV content language
			foreach($this->aENV['content_language'] as $sKey => $sVal) {

				$sTempName	= str_replace('.php', '', $aData['filename_'.$sKey]);
				
				// if lang == default && lang != aENV content language default && filename != empty				
				if($sKey != 'default' 
				&& $sKey != $this->aENV['content_language']['default']
				&& !empty($sTempName)) {
					$aData['weblang']	= $sKey;
					$sContainerString	= $this->getContainerString($aData);

					$oFile->write_str_in_file($sContainerPath.$aData['filename_'.$sKey], $sContainerString);
				} // end if 
				
			} // end foreach aENV content language

			// reset aENV content language to array pointer 0			
			reset($this->aENV['content_language']);
			
			// get child elements
			$sql	= "SELECT * FROM ".$this->aENV['table']['cms_navi']." WHERE parent_id='".$aData['id']."'";
			$oDb->query($sql);
			
			// while mysql result
			while($aRow	= $oDb->fetch_array()) {
				unset($sContainerString);
						
				foreach($this->aENV['content_language'] as $sKey => $sVal) {
					$sTempName	= str_replace('.php', '', $aData['filename_'.$sKey]);
					
					if(empty($aRow['title_'.$sKey]))	continue;
					if(empty($sTempName))	continue;
					if($sKey	!= 'default') {
						// make file
						$aRow['weblang']	= $sKey;
						$sContainerString	= $this->getContainerString($aRow);
						
						$oFile->write_str_in_file($sContainerPath.$aRow['filename_'.$sKey], $sContainerString);
					} // if key != default
					
				} // end foreach aENV content language
				 
			} // end while mysql result
			
			// free mysql result
			$oDb->free_result();
		} // end  template != no page && aENV filename content language default != empty

		// return output
		return $aData;
	}
	
	/*
	 * 
	 */
	function insert(&$aData,&$oDb,&$Userdata,$sTable) {
		global $oDate;

		// set timestamp + userid
		$aData['created']		= $oDate->get_mysql_timestamp();
		$aData['created_by']	= $Userdata['id'];
			
		// build filename out of title(s)
		if (isset($this->aENV['create_php_container']) && $this->aENV['create_php_container'] == true) { // bei einer reinen Flash-Website muessen keine PHP-Container geschrieben werden
			$aData	= $this->getContainerFilename($aData);
		}
		
		// find highest prio
		$sql	= "SELECT MAX(prio) FROM ".$sTable;
		$oDb->query($sql);
		$newPrio = $oDb->fetch_row();
		$aData['prio'] = ($newPrio[0] + 1);
			
		// make insert in NAVI-table
		$oDb->make_insert($aData, $sTable);
		$aData['id'] = $oDb->insert_id(); // get last inserted id
			
		// make insert in CONTENT-table
		if ($aData['template'] != 'no_page') { // no page if no_page ;-)
			$aContentData['navi_id']	= $aData['id'];
			$aContentData['created']	= $aData['created'];
			$aContentData['created_by']	= $aData['created_by'];
			$oDb->make_insert($aContentData, $this->aENV['table']['cms_standard']);
		}
		return $aData;
	}

	/**
	 *	@access private
	 *
	 *	@param array $aData
	 *	@param object $oDb
	 *	@param object $Userdata
	 *	@param string $sTable 
	 *
	 *	@return array 
	 */	
	function update(&$aData, &$oDb, &$Userdata, $sTable) {
		
		$this->updateExistingLinks($oDb, $aData);

		// set userid
		$aData['last_mod_by']	= $Userdata['id'];
			
		// once the page has been chosen for display in the navigation, all references to other navigation pages are deleted
		if ($aData['flag_navi'] == 1) { $aData['reference_id'] = 0; }
		
		// if parent_id has been changed, all references to other navigation pages are deleted
		if ($_POST['parent_id'] != $aData['parent_id']) { $aData['reference_id'] = 0; }
			
		// make update
		$aUpdate['id']	= $aData['id'];
		$oDb->make_update($aData, $sTable, $aUpdate);
		
		$this->updateChildNode($aData['id'], $aData, $oDb, $sTable);
		
		return $aData;
	}
	
	function updateChildNode($nId, $aData, $oDb, $sTable) {
		$aChild['flag_topnavi']		= $aData['flag_topnavi'];
		$aChildUpdate['parent_id']	= $nId;
		
		$oDb->make_update($aChild, $sTable, $aChildUpdate);
		
		$sSql	= "SELECT id FROM $sTable WHERE parent_id = $nId";
		$oDb->query($sSql);
		
		while($aChildNode = $oDb->fetch_array()) {
			$this->updateChildNode($aChildNode['id'], $aData, $oDb, $sTable);
		}
		
		return true;		
	}
	
	/**
	 *	@access public
	 *
	 *	@param array $aData
	 *	@param object $oDb
	 *	@param string $sContainerPath
	 *	@param string $sTable	
	 *	@param object $Userdata
	 *	@param array $_POST 
	 *
	 *	@return array  
	 */	
	function save(&$aData, &$oDb, $sContainerPath, $sTable, &$Userdata, &$_POST) {
		
		// time control
		// foreach aENV content language
		foreach ($this->aENV['content_language'] as $sKey => $sVal) {

			// if default language continue
			if ($sKey == 'default') { continue; }
			if ($this->aENV['cms_navi_onlinefromto_dateselect'] == 'popup') {

				// wenn date mittels "$oForm->date_popup()" gesetzt wurde:
				if (isset($aData['online_from_'.$sKey]) && !empty($aData['online_from_'.$sKey])) {
					list($d, $m, $y)	= explode('.', $aData['online_from_'.$sKey]);
					$aData['online_from_'.$sKey]	= $y.'-'.$m.'-'.$d;
				} // end if

				if (isset($aData['online_to_'.$sKey]) && !empty($aData['online_to_'.$sKey])) {
					list($d, $m, $y)	= explode('.', $aData['online_to_'.$sKey]);
					$aData['online_to_'.$sKey]	= $y.'-'.$m.'-'.$d;
				} // end if popup

			} else {

				// wenn date mittels "$oForm->date_dropdown()" gesetzt wurde:
				if (isset($_POST['online_from_'.$sKey]['year'])) {
					$aData['online_from_'.$sKey]	= $_POST['online_from_'.$sKey]['year'].'-'.$_POST['online_from_'.$sKey]['month'].'-'.$_POST['online_from_'.$sKey]['day'];
				} // end if

				if (isset($_POST['online_to_'.$sKey]['year'])) {
					$aData['online_to_'.$sKey]	= $_POST['online_to_'.$sKey]['year'].'-'.$_POST['online_to_'.$sKey]['month'].'-'.$_POST['online_to_'.$sKey]['day'];
				} // end if

			} // end else dropdown

		} // end foreach aENV content language
		
		// if aData id != empty make update
		if ($aData['id']) { 				
			$aData2	= $oDb->fetch_by_id($aData['id'], $this->aENV['table']['cms_navi']);

			// initialise filesystem object
			$oFile	= new filesystem();

			// foreach aENV content language 	
			foreach ($this->aENV['content_language'] as $sKey => $val) {
				if($sKey=='default') { continue; }
				if(empty($aData['filename_'.$sKey])
				&& empty($aData['title_'.$sKey])) 
					$aData['flag_online_'.$sKey]	= 0;
				if(empty($aData2['filename_'.$sKey])) { continue; }
				if($aData['filename_'.$sKey]==$aData2['filename_'.$sKey]) { continue; }
				
				// delete all old container files
				$oFile->delete_files($sContainerPath.$aData2['filename_'.$sKey]);
				
			} // end foreach aENV content language

			$aData	= $this->getContainerFilename($aData, $aData2);
			
			// make an update on the database
			$this->update($aData, $oDb, $Userdata, $sTable);
			
		} // end if aData id != empty
		// else if aData id == empty make insert
		else { 
			$this->insert($aData,$oDb,$Userdata,$sTable);
		} // end else aData id == empty

		// create the container files if filename is set
		$aData	= $this->createContainer($aData, $sContainerPath, $oDb);
		
		$this->writeSitemaps($oDb);
		
		// return output
		return $aData;
	}
	
	function delete ($aData,&$oDb,$sTable,$sContainerPath,$oSearch) {
		
		global $oFile;
		
		// ggf. loesche PHP_Container
		if (isset($this->aENV['create_php_container']) 
		&& $this->aENV['create_php_container'] == true) {
			
			if ($aData['template'] != 'no_page') { // no delete if no_page
			 
				foreach ($this->aENV['content_language'] as $k => $val) {
					if ($k == 'default') { continue; }
					$oFile->delete_files($sContainerPath.$aData['filename_'.$k]);
				} // end foreach
				
			} // end if
			
		} // end if
			
		// ggf. loesche abhaengiges XML-Content-File
		if (isset($this->aENV['create_xml_structure']) && $this->aENV['create_xml_structure'] == true) {
			
			if ($aData['template'] != 'no_page' 
			&& $aData['template'] != 'standard' 
			&& $aData['template'] != 'home') { // kein delete bei "no_page" oder "standard"
				$oFile->delete_files($this->aENV['path']['xml']['unix'].$aData['template'].'_'.$aData['id'].'.xml');
			}
		}

		// ggf. make delete in standard + search table
		$sql	= "SELECT id FROM ".$this->aENV['table']['cms_standard']." WHERE navi_id=".$aData['id'];
		$oDb->query($sql);
		if ($oDb->num_rows() > 0) {
			// make delete (search table (alle sprachversionen))
			$aSearchData2delete['ref_table']	= $this->aENV['table']['cms_standard'];
			$aSearchData2delete['ref_id']		= $oDb->fetch_field('id');
			$oSearch->delete($aSearchData2delete['ref_id'],$aSearchData2delete['ref_table']);
			// make delete (standard table)
			$aContentData2delete['navi_id']		= $aData['id'];	// get delete-id
			$oDb->make_delete($aContentData2delete, $this->aENV['table']['cms_standard']);
		}
			
		// make delete in navi table
		$oDb->make_delete($aData['id'], $sTable);
		
		$this->writeSitemaps($oDb);
		
		return $aData;
	}
	
	function writeSitemaps($oDb) {
		global $aENV, $oDate;
		
		$oFile	= new filesystem();
		
		$aLanguageFlag	= $oDb->fetch_by_id(1, 'cms_meta');
		$aSitemaps	= array();
		
		// foreach aENV contentlanguage
		foreach ($this->aENV['content_language'] as $sKey => $sValue) {
			// create file and write sitemap xml
			$sFilename	= "google_sitemap_".$sKey.".xml";
			
			if ($sKey == "default") { continue; } 
			if ($aLanguageFlag['flag_online_'.$sKey] == 0) { if(file_exists($this->aENV['path']['xml']['unix'].$sFilename)) { unlink($this->aENV['path']['xml']['unix'].$sFilename); } continue; }
			
			$sXml	=  '<?xml version="1.0" encoding="UTF-8"?>'."\n";
			$sXml	.= '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">'."\n";
			
			if($this->aENV['cug']['writeNotInSitemap']==true) { $aCugRel	= $oDb->fetch_in_flatarray('SELECT rel_id FROM cug_usergroup_rel WHERE rel_table="cms_navi"'); }
			
			$sql	= "SELECT * FROM `".$this->aENV['table']['cms_navi']."` WHERE flag_online_".$sKey." = '1' ORDER BY parent_id ASC , prio DESC";
			$oDb->query($sql);
			
			$aResult	= array();
			while($aData = $oDb->fetch_array()) {
				if($this->aENV['cug']['writeNotInSitemap'] == true
				   && in_array($aData['id'], $aCugRel)) { continue; }
				if ($aData['template'] != 'no_page') {
					$aResult[]	= $aData;
				}
			}
			
			foreach($aResult as $aData) {
				$sUrl	= 'http://'.$aENV['web_url'].'/'.$aData['filename_'.$sKey];
				
				if(strlen($aData['last_modified'])==14) $aData['last_modified']	= $oDate->get_isodatetime($oDate->convert_timestamp($aData['last_modified']));
				
				$sXml	.= "<url>\n";
				$sXml	.= "	<loc>$sUrl</loc>\n";
				$sXml	.= "	<lastmod>".str_replace(" ", "T", $aData['last_modified'])."+01:00</lastmod>\n";
				$sXml	.= "</url>\n";

				if(!empty($this->aENV['int_linkman'][$aData['template']])) {
					$sql	= "SELECT * FROM `".$this->aENV['int_linkman'][$aData['template']]['table']."` WHERE flag_online_".$sKey." = '1' ";
					$sql	.= (!empty($this->aENV['int_linkman'][$aData['template']]['orderby'])) ? "ORDER BY ".$this->aENV['int_linkman'][$aData['template']]['orderby']:"";
					$oDb->query($sql);

					while($aData2 = $oDb->fetch_array()) {
						if(strlen($aData2['last_modified'])==14) $aData2['last_modified']	= $oDate->get_isodatetime($oDate->convert_timestamp($aData2['last_modified']));
						$sXml	.= "<url>\n";
						$sXml	.= "	<loc>$sUrl?id=".$aData2['id']."</loc>\n";
						$sXml	.= "	<lastmod>".str_replace(" ", "T", $aData2['last_modified'])."+01:00</lastmod>\n";
						$sXml	.= "</url>\n";						
					}					
				}
			}
			
			$sXml .= "</urlset>\n";			

			$oFile->write_str_in_file($this->aENV['path']['xml']['unix'].$sFilename, $sXml);
			$aSitemaps[]	= 'http://'.$aENV['web_url'].'/data/cms/xml/'.$sFilename;
		}// END foreach aENV contentlanguage

		$sXml	= '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		$sXml	.= '<sitemapindex xmlns="http://www.google.com/schemas/sitemap/0.84">'."\n";
	
		foreach($aSitemaps as $nKey => $sFilename) {
			$sXml	.= "<sitemap>\n";
		  	$sXml	.= "	<loc>".$sFilename."</loc>\n";
			$sXml	.= "	<lastmod>".str_replace(" ", "T", $oDate->get_isodatetime(time()))."+01:00</lastmod>\n";
			$sXml	.= "</sitemap>\n";
		}
		
		$sXml .= "</sitemapindex>\n";
		
		// write Sitemap Index
		$oFile->write_str_in_file($this->aENV['path']['root']['unix'].$this->aENV['sitemap']['index']['name'], $sXml);
	}
	
	// Links finden ab hier ...
	/**
	 * 
	 * Bestimmte Felder in einer Tabelle finden ... in unserem Fall diesmal Copytexte... aber: die Methode findet auch alles andere bzw. alle Felder, sofern dies gewünscht ist.
	 * @param object $oDb
	 * @param string $sTable
	 * @param string $pattern [optional]
	 * @access public
	 * 
	 */
	function columInfo(&$oDb,$sTable,$pattern='') {
		$like = '';
		if(!empty($pattern)) $like = " LIKE '%$pattern%'";
		$sql = "SHOW COLUMNS FROM `$sTable`".$like;
		
		$oDb->query($sql);
		
		for($i=0;$row = $oDb->fetch_object();$i++) {
			if($row->Type == 'text' || strstr($row->Type,'varchar') || $row->Type == 'longtext') {
				$aData[] = $row->Field;
			}
		}
		return $aData;
	}
	/**
	 * 
	 * Diese Methode findet die Links in den Web-Tabellen. Diese Methode lässt sich auch von 'aussen' nutzen.
	 * @param object $oDb
	 * @param array $aData
	 * @return array
	 * @access public
	 * 
	 */
	function findLinks(&$oDb,&$aData) {
		$aFilename = array();
		$aTables[] = 'cms_standard';
		$aData2 = $oDb->fetch_by_id($aData['id'],'cms_navi');
		foreach($this->aENV['int_linkman'] as $template => $keys) {
			$aTables[] = $keys['table'];
		}
		$bNotnull = false;
		foreach ($this->aENV['content_language'] as $k => $val) {
			if ($k == 'default') { continue; }
			if($aData['filename_'.$k] != $aData2['filename_'.$k]) {
				$aFilename[$k] = $aData['filename_'.$k];
			}
			else {
				$bNotnull = true;
			}
			
		}
		if(!$bNotnull) return; // Ausgang damit die restlichen Schleifen nicht durchlaufen werden müssen.
		for($i=0;!empty($aTables[$i]);$i++) {
			$aFields = $this->columInfo($oDb,$aTables[$i]);
			foreach($aFilename as $lang => $val) {
				if($aData2['filename_'.$lang] != $aData['filename_'.$lang]) {
					
					$oDb->make_search($aFields,$aData2['filename_'.$lang],$aTables[$i]);
					for($j=0;$row = $oDb->fetch_array();$j++) {
						$row['table'] = $aTables[$i];
						$row['filenametochange'] = $aData2['filename_'.$lang];
						$row['foundinlang'] = $lang;
						$rs[] = $row;
					}
					$oDb->free_result();
				}
			}
			reset($aFilename);
		}
		return $rs;
	}
	/**
	 * 
	 * Diese Methode stößt die Update Methoden für die anderen CMS Methoden an. Sofern ein Container umbenannt wurde, wird dieser im gesamten Auftritt ersetzt.
	 * @param object $oDb
	 * @param array $aData
	 * @access private
	 * @return void
	 */
	function updateExistingLinks(&$oDb,&$aData) {
		
		$rs = $this->findLinks($oDb,$aData);
		for($i=0;is_array($rs[$i]);$i++) {
			
			$sTable = $rs[$i]['table'];
			$aFields = $this->columInfo($oDb,$sTable);
			
			for($j=0;!empty($aFields[$j]);$j++) {
				$rs[$i][$aFields[$j]] = str_replace($rs[$i]['filenametochange'],$aData['filename_'.$rs[$i]['foundinlang']],$rs[$i][$aFields[$j]]);
			}

			unset($rs[$i]['table']);
			unset($rs[$i]['filenametochange']);
			unset($rs[$i]['foundinlang']);
			$oDb->make_update($rs[$i],$sTable,array('id' => $rs[$i]['id']));
		}
	}
} // END of class -------------------------------------------------------------------------
?>