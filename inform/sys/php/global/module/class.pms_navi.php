<?php // Class tree
/**
* Diese Klasse stellt die Tree-Funktionalitaeten spezialisiert fuer die PMS-Navi zur Verfuegung. 
* (erwartet "class.tree.php" + "class.db_mysql.php" + "class.filesystem.php"!!!)
*
* Example: 
* <pre><code>
* require_once("php/class.pms_navi.php");
* $oPmsNavi =& new pms_navi($oDb); // params: $oDb
* echo "gefundene Datensaetze: ".$oPmsNavi->getCount();
* $aStructure = $oPmsNavi->getAllChildren();
* foreach($aStructure as $k => $aData) { 
* 	echo $oPmsNavi->getToggleIcon($aData, $main, 'main', "&id=".$id); // params: $aData,$sActiveId,$sActiveVarname[,$sAddGetVars='']
* ... }
* // Zuordnungs-DropDown
* echo $oPmsNavi->getAssignSelect($aData); // params: $aData
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.31 / 2006-01-17 [NEU: Safari-Versionbei "get***Button()"]
*/

require_once($aENV['path']['global_service']['unix']."class.tree.php");

class pms_navi extends tree {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse pms_navi:
		----------------------------------------------------------------------------
		constructor pms_navi(&$oDb)
		
		function setToggleVars($aToggleIcon)
		function getToggleIcon(&$aData, $sActiveId, $sActiveVarname, $sAddGetVars='')
		function openToggle($currentId)
		
		function getPrevButton($sCurrentId)
		function getNextButton($sCurrentId)
		function getNewButton($nInsertPrio='', $nParentId='')
		function getAssignSelect(&$aData)
		
		function makeDelete($sDeleteId, $sTable)
		function makeUpdate(&$aData, $sTable)
		function makeInsert(&$aData, $sTable)
		
		function clearPause($sTable)
		function savePause(&$aData, $sTable)
		function loadPause(&$sTable)
		private _createPause($sTable)
		----------------------------------------------------------------------------
		HISTORY:
		1.31 / 2006-01-17 [NEU: Safari-Versionbei "get***Button()"]
		1.3 / 2005-04-11 [NEU: angepasst an neuen tree-Konstruktor]
		1.2 / 2005-04-07 [NEU: ueberpruefung, wie viele level angeboten werden bei "getAssignSelect()"]
		1.11 / 2005-04-07 [NEU: image statt form-button bei "getNewButton()"]
		1.1 / 2004-12-22 [NEU: toggle (wie CMS) komplett umgebaut!]
		1.0 / 2004-12-02
	*/
	
	// Class Vars
	var $aENV			= array();		// System-Variable
	var $aMSG			= array();		// System-Variable
	var $syslang		= 'de';			// System-Variable
	var $slot			= NULL;			// System-Variable
	var $aToggleIcon	= array();		// Icons/Texte fuer Toggle-Icons (zum Aufklappen der Subnavi)
	var $nToggleId		= '';			// private Variable (aktuelle id) fuers toggeln
	var $aToggleId		= array();		// private Variable (parents of id -> alle ids bei denen aufgeklappt werden muss)

/**
* Konstruktor -> PMS-spezialisiert: Initialisiert das tree-object und zieht die Daten aus der DB
*
* Beispiel: 
* <pre><code> 
* require_once("php/class.pms_navi.php"); 
* $oPmsNavi =& new pms_navi($oDb); // params: $oDb
* </code></pre>
*
* @global	object	$oDb				Datenbank-Objekt, welches fuer die Klasse "class.db_mysql.php" benoetigt wird
* @access   public
*/
	function pms_navi(&$oDb) {
		// system-vars importieren
		global $aENV, $aMSG, $slot, $syslang;
		$this->aENV			= $aENV;
		$this->aMSG			= $aMSG;
		$this->syslang		= $syslang;
		$this->slot			= $slot;
		// set tree-defaults fuer PMS
		$sTable			= $this->aENV['table']['pms_content'][$this->slot];
		$sCachefile		= $this->aENV['path']['pms_data']['unix'].$this->slot."/array.structure.php";
		$aOptions['sSqlConstraint']	= "AND flag_pause='0'";
		$aOptions['sOrderBy']		= "prio DESC";
		
		// tree-Objekt generieren
		parent::tree($oDb, $sTable, $sCachefile, '', $aOptions); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
		parent::buildTree();
		
		// toggle-icon vars setzen
		$this->aToggleIcon	= array();
		$this->aToggleIcon['hide']	= '<img src="'.$this->aENV['path']['pix']['http'].'btn_hide.gif" alt="'.$this->aMSG['content']['hide_slides'][$this->syslang].'" class="btnsmall">';
		$this->aToggleIcon['show']	= '<img src="'.$this->aENV['path']['pix']['http'].'btn_show.gif" alt="'.$this->aMSG['content']['show_slides'][$this->syslang].'" class="btnsmall">';
		$this->aToggleIcon['here']	= '<img src="'.$this->aENV['path']['pix']['http'].'btn_here.gif" alt="" class="btnsmall">';
		
		// toggle-highlight-vars
		$this->nToggleId	= (isset($_GET['nToggleId'])) ? strip_tags($_GET['nToggleId']) : '';
		if (isset($_GET['id'])) $this->nToggleId = strip_tags($_GET['id']);
		$this->aToggleId	= array();
		// ahnen zum toggeln bestimmen
		if (!empty($this->nToggleId)) {
			$aParentId = $this->getAllParentId($this->nToggleId);
			foreach ($aParentId as $pid) {
				if ($pid > 0) { $this->aToggleId[] = $pid; } // wenn nicht root -> dazu
			}
			$this->aToggleId[] = $this->nToggleId; // + sich selbst auch dazu
		}
	}

# --------------------------------------------------------------------------------

/** Config-Funktion zum Ueberschreiben der benoetigten Icons zum "toggeln" (Aufklappen von Subnavigationen)
* 
* @access   public
* @param	array	$aToggleIcon	Vars fuer die Toggle-Icons
*/
	function setToggleVars($aToggleIcon) {
		if (is_array($aToggleIcon)) $this->aToggleIcon = $aToggleIcon;
	}

/** Config-Funktion: Gebe das richtige Icon zum "toggeln" (Aufklappen von Subnavigationen) zurueck.
* Beispiel: 
* <pre><code> 
* echo $oPmsNavi->getToggleIcon($aData, $nEntriesSub); // params: $aData,$nEntriesSub[,$sAddGetVars='']
* </code></pre>
* 
* @access   public
* @param	array	$id				Aktueller Datensatz f.d. ein Toggle-Icon ermittelt werden soll
* @param	int		$nEntriesSub	Anzahl Subnavigationspunkte dieses Navigationspunktes
* @param	string	$sAddGetVars	(optional) Zusatzliche GET-Vars, die beim Aufklappen nicht verloren gehen duerfen
* @return	HTML
*/
	function getToggleIcon($id, $nEntriesSub='', $sAddGetVars='') {
		// get total sub-entries
		if (empty($nEntriesSub)) $nEntriesSub = count($aData['children']);
		if ($nEntriesSub < 1) {
			// kein toggle-icon, sondern platzhalter
			return $this->aToggleIcon['here'];
		} else {
			// auf-/zu-klapp link
			$href = $this->aENV['PHP_SELF']."?slot=".$this->slot."&nToggleId=";
			if (!in_array($id, $this->aToggleId)) {
			// click to open branch
				$href .=  $id;
				$status = 'show';
			} else {
			// click to close branch
				$href .= $this->getParent($id);
				$status = 'hide';
			}
			if (!empty($sAddGetVars)) { $href .= '&'.str_replace('?', '&', $sAddGetVars); }
			// output
			return '<a href="'.$href.'">'.$this->aToggleIcon[$status].'</a>';
		}
	}

/** Ausgabe-Funktion zum Ermitteln, ob die aktuelle Subnavigation aufklappen muss.
* Beispiel: 
* <pre><code> 
* echo ($oPmsNavi->openToggle($aData['id'])) ? "true" : "false"; // params: $currentId
* </code></pre>
* 
* @access   public
* @return	boolean		true, wenn die Subnavi ausgeklappt werden soll
*/
	function openToggle($currentId) {
		// output
		return (!empty($this->nToggleId) && in_array($currentId, $this->aToggleId)) ? true : false; // zeige subnavi oder nicht
	}

# --------------------------------------------------------------------------------

/** Funktion zum Erstellen eines Blaetter-Butons
* 
* @access   public
* @param	string	$sCurrentId		ID des aktuellen Datensatzes
* @return	HTML
*/
	function getPrevButton($sCurrentId) {
		if (empty($sCurrentId)) return;
		// -> safari (vor v1.3) leert die formularfelder nicht beim direkten springen zur naechsten seite!!!
		global $oBrowser;
		if ($oBrowser->isSafari() && $oBrowser->getVersion() < 1.3) { return; }
		// prev button
		if ($sPrevId = $this->getPrev($sCurrentId)) {
			return '<input type="button" value="&lt;" title="'.$this->aMSG['content']['edit_prev'][$this->syslang].'" name="btn[prev]" id="prev" class="smallbut" onClick="location.replace(\''.$this->aENV['PHP_SELF']."?slot=".$this->slot."&id=".$sPrevId.'\');">';
		} else {
			return '<input type="button" value="&lt;" title="'.$this->aMSG['content']['edit_prev'][$this->syslang].'" name="btn[prev]" id="prev" class="smallbutDisabled" disabled>';
		}
	}

/** Funktion zum Erstellen eines Blaetter-Butons
* 
* @access   public
* @param	string	$sCurrentId		ID des aktuellen Datensatzes
* @return	HTML
*/
	function getNextButton($sCurrentId) {
		if (empty($sCurrentId)) return;
		// -> safari (vor v1.3) leert die formularfelder nicht beim direkten springen zur naechsten seite!!!
		global $oBrowser;
		if ($oBrowser->isSafari() && $oBrowser->getVersion() < 1.3) { return; }
		// next button
		if ($sNextId = $this->getNext($sCurrentId)) {
			return '<input type="button" value="&gt;" title="'.$this->aMSG['content']['edit_next'][$this->syslang].'" name="btn[prev]" id="prev" class="smallbut" onClick="location.replace(\''.$this->aENV['PHP_SELF']."?slot=".$this->slot."&id=".$sNextId.'\');">';
		} else {
			return '<input type="button" value="&gt;" title="'.$this->aMSG['content']['edit_next'][$this->syslang].'" name="btn[next]" id="next" class="smallbutDisabled" disabled>';
		}
	}

/** Funktion zum Erstellen eines New-Butons
* 
* @access   public
* @param	$nInsertPrio	(optional) Value des Feldes 'prio' des aktuellen Datensatzes
* @param	$nParentId		(optional) Value des Feldes 'parent_id' des aktuellen Datensatzes
* @return	HTML
*/
	function getNewButton($nInsertPrio='', $nParentId='') {
		// -> safari (vor v1.3) leert die formularfelder nicht beim direkten springen zur naechsten seite!!!
		global $oBrowser;
		if ($oBrowser->isSafari() && $oBrowser->getVersion() < 1.3) { return; }
		// link zusammenbauen
		$sLinkNew = $this->aENV['PHP_SELF'].'?slot='.$this->slot.''; // f.d. aufklapp-navi!
		// neue Seite genau an dieser Stelle einfuegen!
		if (!empty($nInsertPrio)) {
			$sLinkNew .= '&nInsertPrio='.$nInsertPrio;
		}
		// fuer aufklapp-navi
		if (!empty($nParentId)) {
			$sLinkNew .= '&nToggleId='.$nParentId;
		}
		// new button
		###return '<input type="button" value="'.$this->aMSG['btn']['new'][$this->syslang].'" name="btn[new]" class="smallbut" onClick="location.replace(\''.$sLinkNew.'\');">';
		### NEU image statt formfeld
		$button = '<a href="'.$sLinkNew.'" title="'.$this->aMSG['content']['add_slide'][$this->syslang].'">';
		$button .= '<img src="'.$this->aENV['path']['pix']['http'].'btn_add.gif" alt="';
		$button .= $this->aMSG['content']['add_slide'][$this->syslang].'" class="btn"></a>';
		return $button;
	}

/** Ausgabe-Funktion: Tree-Array (max. die ersten 2 Ebenen) automatisiert als Drop-Down-Formfeld zurueckgeben
* Beispiel: 
* <pre><code> 
* echo $oPmsNavi->getAssignSelect($aData); // params: $aData
* </code></pre>
* 
* @access   public
* @param	$aData			aktueller Datensatz
* @return	HTML
*/
	function getAssignSelect(&$aData) {
		// vars
		$aStructure = parent::getAllChildren();
		if (count($aStructure) == 0) { return; }
		$buffer	= array();
		// get parent_id (-> highlight)
		if (isset($aData['parent_id'])) { // im "edit"-mode
			$parent_id = $aData['parent_id'];
		} elseif (isset($_GET['nToggleId'])) { // im "new"-mode
			$parent_id = ($_GET['nToggleId'] + 0); // sichern durch addition von 0
		} else {
			$parent_id = 0; // fallback
		}
		// count sub-level des aktuellen Slides
		$countSublevel = $this->countSublevel($aData['id']);
		
		// start tag
		$buffer[] = '<select name="aData[parent_id]" size="1">';
		// first option
		$sel = ($parent_id == '0') ? ' selected' : ''; // highlight
		$buffer[] = '<option value="0"'.$sel.'>--- '.$this->aMSG['content']['noassignment'][$this->syslang].' ---'.'</option>';
		// tree options
		foreach($aStructure as $kMain => $aData2) {
			if ($countSublevel > 1) continue; // wenn der aktuelle slide schon 2 sublevel hat, darf er nicht in "sub" untergeordnet werden
			if ($aData['parent_id'] == '0' && $aData2['id'] == $aData['id']) { continue; } // wenn parent==hauptpunkt, dann hier nicht nochmal anzeigen
			// highlight
			$sel = ($parent_id == $aData2['id']) ? ' selected' : '';
			$buffer[] = '<option value="'.$aData2['id'].'"'.$sel.'>'.$aData2['title'].'</option>';
			foreach($aData2['children'] as $kSub => $aData3) {
				if ($countSublevel > 0) continue; // wenn der aktuelle slide schon 1 sublevel hat, darf er nicht in "subsub" untergeordnet werden
				if ($aData['parent_id'] == $aData2['id'] && $aData3['id'] == $aData2['id']) { continue; } // wenn parent==unterpunkt, dann hier nicht nochmal anzeigen
				// highlight
				$sel = ($parent_id == $aData3['id']) ? ' selected' : '';
				$buffer[] = '<option value="'.$aData3['id'].'"'.$sel.'>- '.$aData3['title'].'</option>';
			}
		}
		// end tag
		$buffer[] = '</select>';
		// output
		return implode("\n", $buffer)."\n";
	}

#----------------------------------------------------------------------------- ADMIN

/** Admin-Funktion: loesche ein Navipunkt (inkl. aller Unterpunkte!)
* Beispiel: 
* <pre><code> 
* $oPmsNavi->makeDelete($sDeleteId, $sTable); // params: $sDeleteId,$sTable
* </code></pre>
* 
* @access   public
* @param	$sDeleteId			ID des aktuellen Datensatzes
* @param	$sTable				DB-Tabelle
* @return	void
*/
	function makeDelete($sDeleteId, $sTable) {
		$aIDs2delete = $this->getAllChildrenId($sDeleteId);
		// add this id
		$aIDs2delete[] = $sDeleteId;
		
		// make delete(s)
		$this->oDb->query("DELETE FROM ".$sTable." WHERE id IN (".implode(",", $aIDs2delete).")");
		
		// File-actions
		$this->clearCache(); // delete php-array-file
		# (das XML-file wird erst bei betaetigen des preview-buttons aktualisiert!)
		
		// reload tree
		$this->buildTree();
	}

/** Admin-Funktion: Update einen Navipunkt
* Beispiel: 
* <pre><code> 
* $oPmsNavi->makeUpdate($aData, $sTable); // params: $aData,$sTable
* </code></pre>
* 
* @access   public
* @global	$Userdata			Daten des aktuellen Users aus der Session
* @param	$aData				Aktueller Datensatz (als Referenz)
* @param	$sTable				DB-Tabelle
* @return	void
*/
	function makeUpdate(&$aData, $sTable) {
		global $Userdata;
		// set userid
		$aData['last_mod_by'] = $Userdata['id'];
		
		// make update
		$this->oDb->make_update($aData, $sTable, $aData['id']);
		
		// File-actions
		$this->clearCache(); // delete php-array-file
		# (das XML-file wird erst bei betaetigen des preview-buttons aktualisiert!)
		
		// reload tree
		$this->buildTree();
	}

/** Admin-Funktion: Inserte einen Navipunkt
* Beispiel: 
* <pre><code> 
* $oPmsNavi->makeInsert($aData, $sTable); // params: $aData,$sTable
* </code></pre>
* 
* @access   private
* @global	$Userdata			Daten des aktuellen Users aus der Session
* @global	$oDate				Objektreferenz der Klasse "class.datetime_db.php"
* @param	$aData				Aktueller Datensatz (als Referenz)
* @param	$sTable				DB-Tabelle
* @return	Last inserted ID
*/
	function makeInsert(&$aData, $sTable) {
		global $Userdata, $oDate;
		// set timestamp + userid
		$aData['created']		= $oDate->get_mysql_timestamp();
		$aData['created_by']	= $Userdata['id'];
		
		// wenn prio uebergeben wurde, soll ein NEUER datensatz genau an dieser stelle eingefuegt werden - also alle anderen betroffenen verschieben!
		if (isset($_POST['nInsertPrio']) && !empty($_POST['nInsertPrio'])) {
			$this->oDb->query("UPDATE `".$sTable."` SET `prio` = `prio`-1 WHERE `prio` < ".$_POST['nInsertPrio']);
			$aData['prio'] = ($_POST['nInsertPrio']-1);
		} else {
			// find highest prio
			$this->oDb->query("SELECT MIN(`prio`) FROM `".$sTable."`");
			$newPrio = $this->oDb->fetch_row();
			$aData['prio'] = ($newPrio[0] - 1);
		}
		
		// make insert
		$this->oDb->make_insert($aData, $sTable);
		
		// File-actions
		$this->clearCache(); // delete php-array-file
		# (das XML-file wird erst bei betaetigen des preview-buttons aktualisiert!)
		
		// reload tree
		$this->buildTree();
		
		// return last inserted id
		return $this->oDb->insert_id();
	}

#----------------------------------------------------------------------------- PAUSE

/** Admin-Funktion: Leere den Pause-Datensatz
* Beispiel: 
* <pre><code> 
* $oPmsNavi->clearPause($sTable); // params: $sTable
* </code></pre>
* 
* @access   public
* @global	$Userdata			Daten des aktuellen Users aus der Session
* @param	$sTable				DB-Tabelle
* @return	void
*/
	function clearPause($sTable) {
		// datensatz nicht loeschen, sondern nur leeren
		$this->oDb->query("UPDATE `".$sTable."` 
						SET `slide` = NULL, `image` = NULL, `flag_fullscreen` = '0' 
						WHERE `flag_pause` = '1'");
	}

/** Admin-Funktion: Update den Pause-Datensatz
* Beispiel: 
* <pre><code> 
* $oPmsNavi->savePause($aData, $sTable); // params: $aData,$sTable
* </code></pre>
* 
* @access   public
* @global	$Userdata			Daten des aktuellen Users aus der Session
* @param	$aData				Aktueller Datensatz (als Referenz)
* @param	$sTable				DB-Tabelle
* @return	void
*/
	function savePause(&$aData, $sTable) {
		global $Userdata;
		// check
		if (!isset($aData['id']) || empty($aData['id'])) { die ("ERROR: no aData[id]"); }
		// set userid
		$aData['last_mod_by'] = $Userdata['id'];
		
		// make update
		$this->oDb->make_update($aData, $sTable, $aData['id']);
	}

/** Admin-Funktion: lade den Pause-Datensatz
* Beispiel: 
* <pre><code> 
* $aData = $oPmsNavi->loadPause($sTable); // params: $sTable
* </code></pre>
* 
* @access   public
* @param	$sTable				DB-Tabelle
* @return	array Datensatz (als Referenz)
*/
	function &loadPause($sTable) {
		// select
		$sql = "SELECT * FROM `".$sTable."` WHERE `flag_pause`='1'";
		$this->oDb->query($sql);
		if ($this->oDb->num_rows() < 1) {
			$this->_createPause($sTable); // params: $sTable
			$this->oDb->query($sql);
		}
		return $this->oDb->fetch_array();
	}

/** Hilfs-Funktion: Erstelle (initial) einen (leeren) Datensatz fuer die Pause
* 
* @access   private
* @global	$Userdata			Daten des aktuellen Users aus der Session
* @global	$oDate				Objektreferenz der Klasse "class.datetime_db.php"
* @param	$sTable				DB-Tabelle
* @return	Last inserted ID
*/
	function _createPause($sTable) {
		global $Userdata, $oDate;
		$aData = array();
		// set pause flag!
		$aData['flag_pause'] = '1';
		$aData['parent_id'] = '-1';
		// set timestamp + userid
		$aData['created']		= $oDate->get_mysql_timestamp();
		$aData['created_by']	= $Userdata['id'];
		
		// find highest prio (IST NOETIG!)
		$this->oDb->query("SELECT MIN(`prio`) FROM `".$sTable."`");
		$newPrio = $this->oDb->fetch_row();
		$aData['prio'] = ($newPrio[0] - 1);
		
		// make insert
		$this->oDb->make_insert($aData, $sTable);
		
		// return last inserted id
		return $this->oDb->insert_id();
	}

#-----------------------------------------------------------------------------
} // END of class

?>