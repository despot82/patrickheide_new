<?php
/**
 * 
 * Diese Klasse stellt Servicemethoden fuer das Archiv bereit.
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @version 1.0
 * 
 */
class archiv {
	// required attributes
	var $oDb = null;
	var $aENV = null;
	var $aUserdata = null;
	var $entries = 0;
	
	// optional attributes
	var $sTable = '';
	var $oFEA = null;
	/**
	 * 
	 * Konstruktor... initialisiert das Datenbankobject und Array
	 * @param object $oDb
	 * @param array $aENV
     * @access public
	 */
    function archiv(&$oDb,&$aENV) {
    	$this->oDb =& $oDb;
    	$this->aENV =& $aENV;
    }
    /**
     * 
     * @param array $Userdata
     * @access public
     * 
     */
    function setUserdata(&$Userdata) {
    	$this->aUserdata =& $Userdata;
    }
    /**
     * 
     * @param String $sTable
     * @access public
     * 
     */
    function setTable($sTable) {
    	$this->sTable = $sTable;
    }
    /**
     * 
     * @param object $oFEA
     * @access public
     * 
     */
    function setFEA(&$oFEA) {
    	$this->oFEA =& $oFEA;	
    }
    /**
     * 
     * @access public
     * @return int
     */
    function getEntries() {
    	return $this->entries;	
    }
    /**
     * @param int $entries
     * @access public
     */
    function setEntries($entries) {
    	$this->entries = $entries;	
    }
    /**
     * 
     * Speichert einen Datensatz im Archiv. Sei es ein Insert oder Update. Dies gilt auch fuer Folder.
     * @access public
     * @param array $aData Datenbankfelder mit Values
     * @return Boolean
     * 
     */
    function save(&$aData) {
    	if(is_array($aData)) {
    		if(!empty($aData['id']) && isset($aData['id'])) {
    			$ret = $this->doUpdate($aData);
    		}
    		else {
    			$ret = $this->doInsert($aData);
    		}
    	}
    	else {
    		$ret = false;
    	}
    	return $ret;
    }
    /**
     * 
     * Loescht einen Datensatz aus der Folder oder Archivtabelle
     * @access public
     * @param int $delid
     * @return Boolean
     * 
     */
    function delete($delid) {
    	if(!empty($this->sTable)) {
    		$ret = $this->oDb->make_delete(array('id' => $delid),$this->sTable);
    	}
    	else {
    		$ret = $this->oFEA->doDelete($delid,$this->aENV['table']['archiv']);
    	}
    	return $ret;
    }
    /**
     * 
     * Fuehrt einen Update durch auf den Folder bzw. den Archivdatensatz
     * @param array $aData
     * @return Boolean
     * @access protected
     * 
     */
    function doUpdate(&$aData) {
    	$aData['last_mod_by'] = $this->aUserdata['id'];
    	if(!empty($this->sTable)) {

    		unset($aData['searchtext']);
    		$ret = $this->oDb->make_update($aData,$this->sTable,$aData['id']);
			$aLabels = Tools::getLabels($this->oDb,$this->aENV,'arc',$aData['tree_id']);
    		$aData2['searchtext'] = $aData['title'];
    		$aData2['searchtext'] .= ' '.$aData['keywords'];
    		$aData2['searchtext'] .= ' '.$aData['content'];
			$aData2['searchtext'] .= ' '.$aLabels[$aData['label_id']]['name'];
    		$aData2['searchtext'] = trim($aData2['searchtext']);
    		$ret = $this->oDb->make_update($aData2,$this->sTable,$aData['id']);
    		
    	}
    	else {
    		// FEA Update Folder!
    		$ret = $this->oFEA->doUpdate($aData);
    	}
		return $ret;
	}
	
	function copyFromFile($aFiles) {
		if($aFiles['type'] == 'text/plain') {
			$str = addslashes(implode("",file($aFiles['tmp_name'])));
			unlink($aFiles['tmp_name']);
			return $str;
		}
	}
	
    /**
     * 
     * Fuehrt einen Insert durch Folder oder Archivdatensatz
     * @param array $aData
     * @return Boolean
     * @access protected
     * 
     */

    function doInsert(&$aData) {
    	$aData['created_by'] = $this->aUserdata['id'];
    	$aData['created'] = date('YmdHis');
    	if(!empty($this->sTable)) {
			$aLabels = Tools::getLabels($this->oDb,$this->aENV,'arc',$aData['tree_id']);
    		$aData2['searchtext'] = $aData['title'];
    		$aData2['searchtext'] .= ' '.$aData['keywords'];
    		$aData2['searchtext'] .= ' '.$aData['content'];
			$aData2['searchtext'] .= ' '.$aLabels[$aData['label_id']]['name'];
    		$aData2['searchtext'] = trim($aData2['searchtext']);
    		$ret = $this->oDb->make_insert($aData,$this->sTable);
    		$id = $this->oDb->insert_id();
    		$this->oDb->make_update($aData2,$this->sTable,$id);
    		$ret = $id;
    	}
    	else {
    		// FEA Insert Folder!
    		$ret = $this->oFEA->doInsert($aData);
    	}
		return $ret;
    }
    
    /**
     * 
     * Hole dir alle Archive als Array [id] => $name
     * @return array
     * @access public
     * 
     */
    function getAllArchives() {
    	if($this->oFEA->getEntries() != 0) {
    		$this->oFEA->beanResetPointer();
    		for($i=0;$this->oFEA->beanNext();$i++) {
	    		$ret[$this->oFEA->getValueByName('id')] = $this->oFEA->getValueByName('title');
	    	}
    	}
    	else {
    		$ret = false;	
    	}
    	return $ret;
    }
    /**
     * 
     * Hole alle Datensaetze aus einem Archiv
     * @access public
     * @param int $aid
     * @return array
     * 
     */
    function getArchivData($aid,$limit=0,$start=0,$sort='',$sConstraint='') {
    	$sql = "SELECT * FROM `".$this->sTable."` WHERE `tree_id`='$aid'".$sConstraint;
    	if(!empty($sort)) $sql .= ' ORDER BY '.$sort;
    	$this->oDb->query($sql);
    	if($limit != 0) {
    		$this->entries = floor($this->oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
			if ($this->entries > $limit) { $this->oDb->limit($sql, $start, $limit); }
    	}
    	if($this->oDb->num_rows() != 0) {
	    	for($i=0;$row=$this->oDb->fetch_object();$i++) {
	    		foreach($row as $field => $val) {
	    			$ret[$i][$field] = $row->$field;
	    		}
	    	}
    	}
    	else {
    		$ret = array();	
    	}
    	return $ret;
    }
    /**
     * 
     * Liefere die letzten Eintraege im Archiv. Liefert entweder Datensaetze oder false.
     * @access public
     * @param int $limit
     * @return mixed
     * 
     */
    function getRecent($limit = 20, $nAvailable = '') {

    	$sConstraint	= ($nAvailable!='' ? ' WHERE avail = "'.$nAvailable.'"':'');
    	$sql = "SELECT * FROM `".$this->sTable."` ".$sConstraint." ORDER BY `id` DESC LIMIT 0,$limit";
    	$this->oDb->query($sql);
    	
    	if($this->oDb->num_rows() != 0) {
	    	for($i=0;$row=$this->oDb->fetch_object();$i++) {
	    		foreach($row as $field => $val) {
	    			$ret[$i][$field] = $row->$field;
	    		}
	    	}
    	}
    	else {
    		$ret = false;	
    	}
    	return $ret;
    	
    }
    /**
     * 
     * Hole dir einen Eintrag aus dem Archiv.
     * @access public
     * @param int $aeid
     * @return array
     * 
     */
    function getArchivEntry($aeid) {
	    $aData = $this->oDb->fetch_by_id($aeid,$this->sTable);
		if(strstr($aData['keywords'],',')) {
	    	$aData['keywords'] = str_replace(',',' ',$aData['keywords']);
		}
    	return $aData;
    }
    
/** 
* ermittelt alle Postings (nur 1. ebene + nur die wesentlichen Infos fuer eine Uebersicht) mit diesem Suchbegriff.
* Optional kann auf eine nTreeId (Kategorie) eingeschraenkt werden.
* und ermittelt vieviele Antworten zum jew. Eintrag existieren.
* 
* <pre><code> 
* $aData = $oArchiv->getSearchResult($arcSearchterm, $arcFolder); // params: $sSearchtext[,$nTreeId]
* </code></pre>
* 
* @access   public
* @param	string	$sSearchtext	Suchbegriff
* @param	object	$oTree			Referenz auf das Tree Objekt
* @param	int		$nTreeId		ID des Verzeichnisses (optional - default: NULL)
* @param	int		$limit			Max. Anzahl Datensaetze (optional - default: alle)
* @param	int		$start			Start-Datensatz (optional - default: 0)
* @return	array
*/
	function getSearchResult($sSearchtext, &$oTree, $nTreeId=NULL, $limit=0, $start=0, $nAvailable = null) {
		// vars
		$buffer = array();
		$aPostingId = array();
		// wenn kein suchbegriff aber thema -> direkte query
		if (empty($sSearchtext) && !is_null($nTreeId) && $nTreeId != 'all' && $nTreeId > 0) {
			return $this->getArchiv($nTreeId); // params: $nTreeId
		}
		// SEARCH
		$sConstraint	= !is_null($nAvailable) ? ($nAvailable==1 ? 'AND avail = "1"':'AND avail = "0"'):'';
		
		$this->oDb->soptions['select']			= "id, intnr, content, tree_id, title, label_id, keywords,avail, created, created_by";

		if (!is_null($nTreeId) && $nTreeId != 'all') { // wenn auf Kategorie eingeschraenkt wurde
			if (is_object($oTree)) {
				// alle Unterordner ermitteln
				$aTreeId = $oTree->getAllChildrenId($nTreeId);
				$aTreeId[] = $nTreeId; // + sich selbst auch dazu
				// einen bestimmten folder (inkl. aller unterordner)
				$this->oDb->soptions['extra']	= 'tree_id IN ('.implode(',', $aTreeId).')'.$sConstraint;
			} else {
				// sonst einfach nur diesen ordner
				$this->oDb->soptions['extra']	= 'tree_id = '.$nTreeId.$sConstraint;
			}
		} else {
			
			$this->oDb->soptions['extra']	= ($nAvailable!='' ? 'avail = "'.$nAvailable.'"':'');
		}

		$this->oDb->make_search("searchtext", $sSearchtext, $this->sTable, 'created DESC');
		
    	if($limit != 0) {
    		$this->entries = floor($this->oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
			if ($this->entries > $limit) { $this->oDb->make_search('searchtext', $sSearchtext, $this->sTable, 'created DESC', $limit, $start); }
    	}
		while ($row = $this->oDb->fetch_array()) {
			
			if(!in_array($row['id'], $aPostingId)) {
				$aPostingId[] = $row['id'];
			} else {
				continue;
			}
			$buffer[] = array(
				'id'			=> $row['id'],
				'intnr'			=> $row['intnr'],
				'tree_id'		=> $row['tree_id'],
				'title'			=> $row['title'],
				'label_id'		=> $row['label_id'],
				'content'		=> $row['content'],
				'keywords'		=> $row['keywords'],
				'avail'			=> $row['avail'],
				'created'		=> $row['created'],
				'created_by'	=> $row['created_by']
			);
		}
		return $buffer;
	}
	/**
	 * 
	 * Hole dir alle Dateien die derzeit als Backup verfuegbar sind. Falls der Ordner,
	 * in dem die Dateien vorhanden sein sollte, nicht existiert, wird dieser erzeugt.
	 * @access public
	 * @return array
	 * 
	 */
	function getDBBackupArchiv() {
		if(!is_dir($this->aENV['path']['sys_data']['unix'].'backup/')) {
			mkdir($this->aENV['path']['sys_data']['unix'].'backup/', 0777);
		}
		$oFolder =& new FSFolder($this->aENV['path']['sys_data']['unix'].'backup/',$this->aENV);
		$aFiles = $oFolder->getFiles();
		return $aFiles['backup'];
		
	}
	/**
	 * 
	 * Hole die alle Daten aus dem Archiv unter beruecksichtigung, des Starteintrags und Limitmakierung.
	 * @access public
	 * @param int $limit
	 * @param int $start 
	 * @return array
	 */
	function getAllArchivData($limit=20,$start=0,$nAvailable='') {
		
		$limit=$start+$limit;
		$limit--;
		// alle
		$aArchives = $this->getAllArchives();
		$aArchivData = array();
		foreach($aArchives as $id => $name)	{
			$aArchivData = array_merge($aArchivData,$this->getArchivData($id, $nAvailable));	
		}
		for($i=$start;$i <= $limit;$i++) {
			$aNewArchivData[] = $aArchivData[$i];	
		}
		$this->entries = count($aArchivData);
		
		return $aNewArchivData;
	}
}
?>