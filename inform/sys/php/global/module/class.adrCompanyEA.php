<?php

	class adrCompanyEA {

		var $oDb = null;
		var $bean = null;
		
		function setODb($oDb) {

			$this->oDb = $oDb;
			
		}
		
		function initialize($aENV) {
			
			include($aENV['path']['global_bean']['unix']."class.adrCompanyBean.php");
			
			$this->bean = $this->setBeanValues($this->getDBValues(), new adrCompanyBean());
			
   			//$this->setGetBeanValues();

   		}

		function setBeanValues($bdaten, &$bean) {
		
			$set = "set";
		
			foreach($bdaten as $field => $keys) {
			
				$funname = $set.ucfirst($field);
					
				$bean->$funname($keys);
				
			}
		
			return $bean;
			
		}

		/**
		 * 
		 * Diese Methode befuellt das Array, welches nachtraeglich dazu dient den Pointer zu bewegen
		 * 
		 */

/*
		function setGetBeanValues() {
			
			$arbva = array("id","companyname","referenceid","cat","street","building","region","town","pobox","poboxtown","country","phonemob","phonetel","phonefax","email","web","companyid","companydep","companypos","recipient","created","createdby","lastmodified","lastmodby");
			
			$this->getBeanValues($arbva);
				
		}
*/
		function getDBValues() {

			$table1 = "adr_address";
			$table2 = "adr_contact";
			$table3 = "adr_country";
			
			$sql  = "SELECT * FROM ".$table1." INNER JOIN ".$table2." ON $table1.reference_id=$table2.id INNER JOIN $table3 ON $table1.country=$table3.code WHERE $table1.cat='c'";

			$this->oDb->query($sql);
			
			for($i=0;$row = $this->oDb->fetch_object();$i++) {
				
				$bdaten["id"][$row->reference_id]			= $row->id;
				$bdaten["referenceid"][$row->reference_id]	= $row->reference_id;
				$bdaten["cat"][$row->reference_id]			= $row->cat;
				$bdaten["street"][$row->reference_id]		= $row->street;
				$bdaten["building"][$row->reference_id]		= $row->building;
				$bdaten["region"][$row->reference_id]		= $row->region;
				$bdaten["town"][$row->reference_id]			= $row->town;
				$bdaten["pobox"][$row->reference_id]		= $row->po_box;
				$bdaten["poboxtown"][$row->reference_id]	= $row->po_box_town;
				$bdaten["country"][$row->reference_id]		= $row->country;
				$bdaten["phonemob"][$row->reference_id]		= $row->phone_mob;
				$bdaten["phonetel"][$row->reference_id]		= $row->phone_tel;
				$bdaten["phonefax"][$row->reference_id]		= $row->phone_fax;
				$bdaten["email"][$row->reference_id]		= $row->email;
				$bdaten["web"][$row->reference_id]			= $row->web;
				$bdaten["companyname"][$row->reference_id]	= $row->company;
				$bdaten["companyid"][$row->reference_id]	= $row->company_id;
				$bdaten["companydep"][$row->reference_id]	= $row->company_dep;
				$bdaten["companypos"][$row->reference_id]	= $row->company_pos;
				$bdaten["recipient"][$row->reference_id]	= $row->recipient;
				$bdaten["created"][$row->reference_id]		= $row->created;
				$bdaten["createdby"][$row->reference_id]	= $row->created_by;
				$bdaten["lastmodified"][$row->reference_id] = $row->last_modified;
				$bdaten["lastmodby"][$row->reference_id]	= $row->last_mod_by;
				
			}
		
			$this->oDb->free_result();
			
			return $bdaten;
			
		}
		
	}
?>