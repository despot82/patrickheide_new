<?php
/**
* globale Funktionsbibliothek fuer PMS-ADMIN-Seiten
*
* @access	public
* @package  Content
* @author   Andy Fehn <af@design-aspekt.com>
* @version  2.8 / 2006-02-14 [NEU: UTF-8 Behandlung  bei "pms_restorePresentation()" eingebaut!]
*/

/*	----------------------------------------------------------------------------
		Funktionen dieser Funktionsbibliothek:
		----------------------------------------------------------------------------
		function pms_write_xml_structure($slot)
		function pms_write_xml_layouts($slot, $pTemplate)
		
		function pms_return_used_media_filenames($slot, $assiociative=false)
		function pms_getUniqueId()
		function pms_getNextFreeSlot()
		function pms_restorePresentation($restoreid, $slot)
		----------------------------------------------------------------------------
		HISTORY:
		2.7 / 2006-01-18 [AUFGERAEUMT: doppelte Funktionen aus "func.common.php" + "func.admin_common.php" geloescht!]
		2.6 / 2005-04-07 [NEU: "getUniqueId()" + "getNextFreeSlot()"]
		2.5 / 2005-03-14 ["showUserNames()" GELOESCHT. Ist jetzt in "class.user.php"!]
		2.4 / 2005-01-25 (NEU: "shorten_text()" + "pms_write_xml_structure()" umgebaut)
		2.31 / 2004-12-01 (return statt echo bei den js_*()-funktionen)
		2.3 / 2004-02-25 (extended stripForFlash() to convert special characters)
		2.2 / 2004-02-06 (NEU in "write_xml_structure()": "flag_fullscreen")
		2.1 / 2004-01-15 (write_topnavi_array() entfernt)
		2.0 / 2003-12-05 (komplett umgebaut!)
		1.0 / 2003-09-01 (PMS!)
*/

# --------------------------------------------------------------------------------XML/FLASH

/**
* Schreibt ein XML-File mit den benoetigten Seiten-Informationen fuer die Navigation 
* im Praesentations-Flash (kein Rueckgabewert).
*
* @access   public
* @global	array	$aENV
* @global	object	$oDb
* @global	object	$oFile
*/
	function pms_write_xml_structure($slot, $pTemplate) {
		
		global $aENV,$oDb,$oFile;
		if (empty($pTemplate)) die('ERROR: NO $pTemplate!');
		// vars
		$oDb2 = new dbconnect($aENV['db']);
		$oDb3 = new dbconnect($aENV['db']);
		$sTable = $aENV['table']['pms_content'][$slot];
		
		$sDbFields = "`id`,`title`,`headline`,`copytext`,`flag_media`,`image`,`slideshow`,`flv_file`,`flv_width`,`flv_height`,`flv_controls`,`loop`,`layout`,`flag_navi`,`flag_fullscreen`,`multitext`,`multipix`";
		
		// hole die filenames aller verwendeten mediafiles in ein assioziatives array
		$aMediaFilename = pms_return_used_media_filenames($slot, true);
		
		// open structure
		$xmlStr = '<content>'."\n";
		
		// open main
		$oDb->query("SELECT ".$sDbFields." 
					FROM `".$sTable."` 
					WHERE `parent_id` = '0' 
						AND `flag_pause` = '0' 
					ORDER BY `prio` DESC");
		while ($aMainData = $oDb->fetch_array()) {
			$xmlStr .= _pms_format_xml_slide($aMainData, $aMediaFilename, $pTemplate);
			
			// open sub
			$oDb2->query("SELECT ".$sDbFields." 
						FROM `".$sTable."` 
						WHERE `parent_id` = '".$aMainData['id']."' 
							AND `flag_pause` = '0' 
						ORDER BY `prio` DESC");
			while ($aSubData = $oDb2->fetch_array()) {
				$xmlStr .= '	'._pms_format_xml_slide($aSubData, $aMediaFilename, $pTemplate);
				
				// open subsub
				$oDb3->query("SELECT ".$sDbFields." 
							FROM `".$sTable."` 
							WHERE `parent_id` = '".$aSubData['id']."' 
								AND `flag_pause` = '0' 
							ORDER BY `prio` DESC");
				while ($aSubSubData = $oDb3->fetch_array()) {
					$xmlStr .= '		'._pms_format_xml_slide($aSubSubData, $aMediaFilename, $pTemplate);
					// close subsub
					$xmlStr .= '		</slide>'."\n";
				}
				// close sub
				$xmlStr .= '	</slide>'."\n";
			}
			// close main
			$xmlStr .= '</slide>'."\n";
		}
		
		// pause
		$oDb->query("SELECT `id`,`image`,`slideshow` FROM `".$sTable."` WHERE `flag_pause` = '1'");
		$aPauseData = $oDb->fetch_array();
		$aPauseSlideshowID = explode(",", $aPauseData['slideshow']); $tmp = array();
		foreach ($aPauseSlideshowID as $sl_id) { $tmp[] = $aMediaFilename[$sl_id]; }
		$sPauseSlideshow = implode(",", $tmp);
		$xmlStr .= '<pause id="'.$aPauseData['id'].'" image="'.$aMediaFilename[$aPauseData['image']].'" slideshow="'.$sPauseSlideshow.'" />'."\n";
		
		// close structure
		$xmlStr .= '</content>'."\n";
		
		// make file
		$oFile->write_str_in_file($aENV['path']['pms_data']['unix'].$slot."/structure.xml",$xmlStr);
		
		// trigger next function
		pms_write_xml_layouts($slot, $pTemplate);
	}
	
	// Hilfsfunktion fuer "pms_write_xml_structure()" welche den "slide"-tag formatiert
	function _pms_format_xml_slide(&$aData, &$aMediaFilename, $pTemplate) {
		global $aENV;
		if (empty($pTemplate)) die('ERROR: NO $pTemplate!');
		// zur leichteren lesbarkeit:
		$layout_elements = $aENV['pms']['layout'][$pTemplate][$aData['layout']]['contains'];

		// start tag + id + title
		$sStr = '<slide id="'.$aData['id'].'" title="'.stripForFlash($aData['title']).'"';
		// headline
		if (in_array("headline", $layout_elements) && !empty($aData['headline'])) {
			$sStr .= ' headline="'.stripForFlash($aData['headline']).'"';
		}
		// text
		if (in_array("text", $layout_elements) && !empty($aData['copytext'])) {
			$sStr .= ' copytext="'.stripForFlash($aData['copytext']).'"';
		}
		// multipart
		if (in_array("multipart", $layout_elements)) {
			// text-parts [pipe(||)-getrennt]
			$aMultitext = explode("||", $aData['multitext']); $tmp = array();
			foreach ($aMultitext as $sMultitext) {
				$tmp[] = (empty($sMultitext)) ? 'null' : $sMultitext;
			}
			$sStr .= ' multitext="'.stripForFlash(implode("||", $tmp)).'"';
			// image-parts [komma(,)-getrennt]
			$aMultipixID = explode(",", $aData['multipix']); $tmp = array();
			foreach ($aMultipixID as $mp_id) {
				$sMediaFilename = $aMediaFilename[$mp_id];
				$tmp[] = (empty($sMediaFilename)) ? 'null' : $sMediaFilename;
			}
			$sStr .= ' multipix="'.implode(",", $tmp).'"';
		}
		// image
		if ((in_array("image", $layout_elements) 
				&& !in_array("flag_media", $layout_elements)) 
			|| (in_array("flag_media", $layout_elements) 
				&& $aData['flag_media'] == '1')
			) {	//echo "<script>alert('drin');</script>";
			if ($aData['image'] != "") { $sStr .= ' image="'.$aMediaFilename[$aData['image']].'"'; }
		}
		// slideshow
		if ((in_array("slideshow", $layout_elements) 
				&& !in_array("flag_media", $layout_elements)) 
			|| (in_array("flag_media", $layout_elements) 
				&& $aData['flag_media'] == '0')
			) {
			if ($aData['slideshow'] != "") { 
				$aSlideshowID = explode(",", $aData['slideshow']); $tmp = array();
				foreach ($aSlideshowID as $sl_id) { $tmp[] = $aMediaFilename[$sl_id]; }
				$sStr .= ' slideshow="'.implode(",", $tmp).'" loop="'.$aData['loop'].'"';
			}
		}
		// flv
		if (in_array("flag_media", $layout_elements) && $aData['flag_media'] == '2') {			
			if ($aData['flv_file'] != '') {
				$sStr .= ' flv="'.$aData['flv_file'].'|'.$aData['flv_width'].'|'.$aData['flv_height'].'|'.$aData['flv_controls'].'"';
			}
		}
		// restliche attribute
		$sStr .= ' layoutType="'.$aData['layout'].'" showInNavi="'.$aData['flag_navi'].'" fullscreen="'.$aData['flag_fullscreen'].'">'."\n";
		
		// output
		return $sStr;
	}

/**
* Schreibt ein XML-File mit den benoetigten Layout-Definitionen fuer Content-Darstellung 
* im Praesentations-Flash (kein Rueckgabewert).
*
* @access   public
* @global	array	$aENV
* @global	object	$oFile
*/
	function pms_write_xml_layouts($slot, $pTemplate) {
		
		global $aENV, $oFile;
		$xmlStr = '';
		if (empty($pTemplate)) die('ERROR: NO $pTemplate!');
		
		// open structure
		$xmlStr = '<layouts>'."\n";
		
		foreach ($aENV['pms']['layout'][$pTemplate] as $k => $v) {
		// build layout tag
			$xmlStr .= '	<layout type="'.$k.'" ';
			if (in_array('headline', $v['contains']) && $v['headlineConfig'] != '') {
				$xmlStr .= 'headline="'.$v['headlineConfig'].'" ';
			}
			if (in_array('text', $v['contains']) && $v['textConfig'] != '') {
				$xmlStr .= 'copytext="'.$v['textConfig'].'" ';
			}
			if (in_array('image', $v['contains']) && $v['imageConfig'] != '') {
				$xmlStr .= 'image="'.$v['imageConfig'].'" ';
			}
			if (in_array('slideshow', $v['contains']) && $v['slideshowConfig'] != '') {
				$xmlStr .= 'slideshow="'.$v['slideshowConfig'].'" ';
			}
			if (in_array('multipart', $v['contains']) && $v['textConfig'] != '') {
				$xmlStr .= 'copytext="'.$v['textConfig'].'" ';
				if ($v['imageConfig'] != '') {
					$xmlStr .= 'image="'.$v['imageConfig'].'" ';
				}
			}
			if (in_array('flag_media', $v['contains']) && $v['layoutWidth'] != '') {
				$xmlStr .= 'layoutWidth="'.$v['layoutWidth'].'" ';
			}
			if (in_array('flag_media', $v['contains']) && $v['layoutHeight'] != '') {
				$xmlStr .= 'layoutHeight="'.$v['layoutHeight'].'" ';
			}
			$xmlStr .= '/>'."\n";
		}
		
		// close structure
		$xmlStr .= '</layouts>'."\n";
		
		// make file
		$oFile->write_str_in_file($aENV['path']['pms_data']['unix'].$slot."/layouts.xml", $xmlStr);
	}

# -------------------------------------------------------------------------------- COMMON

/**
* Ermittelt alle in der aktuellen Paesentation benutzten Media-DB items und gibt derern Filenames als array zurueck. 
* (wird zum archivieren + XML schreiben benoetigt).
*
* @access   public
* @global	array	$aENV
* @global	object	$oDb
* @global	object	$oFile
* @param	int		$slot			(zum ermitteln der verwendeten DB-table)
* @param	boolean	$assiociative	(sollen die filenames als assioziatives Array zurueckgegeben werden? Default: "false")
* @return	array	filenames
*/
	function pms_return_used_media_filenames($slot, $assiociative=false) {
		
		global $aENV,$oDb,$oFile;
		// vars
		$sTable1 = $aENV['table']['pms_content'][$slot];
		$sTable2 = $aENV['table']['mdb_media'];
		$aUsedMediaId = array();
		$aUsedMediaFilename = array();
		
		// get IDs (store them in temp-array "$aUsedMediaId")
		$oDb->query("SELECT `image`,`slideshow`,`multipix` FROM `".$sTable1."`");
		while ($aData = $oDb->fetch_array()) {
			if (!empty($aData['image'])) { $aUsedMediaId[] = $aData['image']; }
			if (!empty($aData['slideshow'])) {
				$aSlideID = explode(",", $aData['slideshow']);
				foreach ($aSlideID as $id) {
					$aUsedMediaId[] = $id;
				}
			}
			if (!empty($aData['multipix'])) {
				$aMultipixID = explode(",", $aData['multipix']);
				foreach ($aMultipixID as $id) {
					if (!empty($id)) { $aUsedMediaId[] = $id; }
				}
			}
		}
		// doppelte eintraege loeschen
		$aUsedMediaId = array_unique($aUsedMediaId);	/*// DEBUG	return $aUsedMediaId;*/
		
		// get filenames
		$sUsedMediaIDs = implode(",",$aUsedMediaId);
		if (empty($sUsedMediaIDs)) {
			// output
			return array();
		} else {
			$oDb->query("SELECT `id`,`filename` FROM `".$sTable2."` WHERE `id` IN (".$sUsedMediaIDs.")");
			while ($aData = $oDb->fetch_array()) {
				if ($assiociative==false) {
					$aUsedMediaFilename[] = $aData['filename'];
				} else {
					$aUsedMediaFilename[$aData['id']] = $aData['filename'];
				}
			}
			return $aUsedMediaFilename;
		}
	}

/**
* Ermittelt mittels aktuellem timestamp (plus microsekunden) eine 16 Zeichen lange unique-ID und gibt sie als string zurueck. 
* (Wird z.B. bei "pms_content_meta.php" und "popup_archive_restore.php" benutzt.)
*
* @access   public
* @return	string	unique-ID
*/
	function pms_getUniqueId() {
		list($usec, $sec) = explode(" ", microtime());
		return (string)$sec.substr($usec, 2, 6);
	}

/**
* Ermittelt den naechsten freien Slot. Wenn kein Slot mehr frei ist, wird "0" zurueckgegeben!
* (Wird z.B. bei "pms_content_meta.php" und "popup_archive_restore.php" benutzt.)
*
* @access   public
* @return	int		ID des nuechsten freien Slots
*/
	function pms_getNextFreeSlot() {
		global $aENV;
		$nCountSlots = count($aENV['table']['pms_content']); // i.d. _include_all.php definiert!
		$nextFreeSlot = 0;
		for ($i = 1; $i <= $nCountSlots; $i++) {
			$sMetaFile = $aENV['path']['pms_data']['unix'].$i.'/array.meta.php';
			if (!file_exists($sMetaFile)) { $nextFreeSlot = $i; break; }
		}
		return $nextFreeSlot;
	}

/**
* Stellt im naechsten freien Slot die ausgewaehlte (z.Z. archivierte) Praesentation wieder her. 
* (Wird z.B. bei "pms_archive.php" und "popup_archive_restore.php" benutzt.)
*
* @access   public
* @global	array	$aENV
* @global	object	$oDb
* @global	object	$oFile
* @global	object	$oDate
* @global	boolean	$DEBUG
* @param	int		$restoreid		(ID des wiederherzustellenden Datensatzes)
* @param	int		$slot			(zum ermitteln der verwendeten DB-table/Path etc)
* @return	boolean		true, wenn fertig
*/
	function pms_restorePresentation($restoreid, $slot) {
		// check var
		if ($restoreid < 1) return; // throwerror();
		if ($slot < 1) return; // throwerror();
		global $aENV, $oDb, $oFile, $oDate, $DEBUG;
		if (!isset($DEBUG)) $DEBUG = true; // bei "true" wird nicht gespeichert + weitergeleitet, dafuer werden die exec-meldungen ausgegeben!
		
	// ermittle naechsten freien slot in den wiederhergestellt wird
		$slot = pms_getNextFreeSlot(); // (@see func.pms_common.php)
	// vars
		$sContainerTable	= $aENV['table']['pms_content'][$slot];
		$sContainerPath		= $aENV['path']['pms_data']['unix'].$slot."/";
		$sArchivePath		= $aENV['path']['pms_archive']['unix'];
		
	// ueberpruefe, ob content-DB (und slot) leer ist
		$oDb->query("SELECT `id` FROM `".$sContainerTable."`");
		// wenn nicht, nachtraeglich leeren
		if ($oDb->num_rows()) {
			if (!$DEBUG) {
				// leere slot-dir
				$oFile->clear_folder($sContainerPath);
				// loesche aktuelle version in db
				$oDb->query("DELETE from `".$sContainerTable."`");
			} else { // DEBUG
				echo "slot not empty!<br>\n";
			}
		}
	// wiederherstellen der ausgewaehlten version
		if (!isset($alert)) {
		// restoreFile (und andere Meta-Daten aus Archiv-Table) ermitteln
			$aData = $oDb->fetch_by_id($restoreid, $aENV['table']['pms_archive']);
			$sRestoreFile = trim($aData['filename']);
			if ($aENV['archive_type'] == 'pclzip') {
			// unpack PclZip
				require_once($aENV['path']['global_service']['unix'].'pclzip.lib.php');
				// init: auszupackendes zip bestimmen
				$oArchive = new PclZip($sArchivePath.$sRestoreFile); // params: $zipname

				// NUR die folgenden Files entpacken
				$aFileList = array();
				$aFileList[0] = 'data/insert_navi.sql';
				$aFileList[1] = 'data/layouts.xml';
				$aFileList[2] = 'data/meta.xml';
				$aFileList[3] = 'data/structure.xml';
				if ($DEBUG) { // DEBUG
					echo 'PCLZIP_OPT_BY_NAME: '; print_r($aFileList);
					echo 'PCLZIP_OPT_PATH: '.$sContainerPath;
					echo 'PCLZIP_OPT_REMOVE_PATH: data';
				} else {
					// doit
					if ($oArchive->extract(PCLZIP_OPT_BY_NAME, $aFileList,
										  PCLZIP_OPT_PATH, $sContainerPath,
										  PCLZIP_OPT_REMOVE_PATH, 'data') == 0) {
						die("Error : ".$oArchive->errorInfo(true));
					}
				}
			} else if ($aENV['archive_type'] == 'zip') {
			// unpack zip
				// NUR die folgenden Files entpacken
				$sIncludeFiles = "data/insert_navi.sql";
				$sIncludeFiles .= " data/layouts.xml ";
				$sIncludeFiles .= " data/meta.xml";
				$sIncludeFiles .= " data/structure.xml";
				#$sExcludeFiles = " -x *.jpg *.swf *.exe script_archive_save.php "; // <-- geht nicht...
				$sZIPcommand = "unzip -j ".$sArchivePath.$sRestoreFile." ".$sIncludeFiles." -d ".$sContainerPath; // "unzip $src_path.$zipdatei [file(s)] [-x xfile(s)] -d $src_path"
				$zipE_array = array(); $zipE_integer = 0; // init vars
				$zipE_string = exec($sZIPcommand, $zipE_array, $zipE_integer); // store messages in vars
				if ($DEBUG) { // DEBUG
					echo "slot: ".$slot."<br>\n";
					echo "sZIPcommand: ".$sZIPcommand."<br>\n";
					echo "zipE_integer: ".$zipE_integer."<br>\n";
					echo "zipE_string: ".$zipE_string."<br>\n zipE_array: <br>\n";
					if(is_array($zipE_array)) {foreach($zipE_array as $line) echo $line."<br>\n";}
				}
				unset($sZIPcommand); unset($zipE_string); unset($zipE_array); unset($zipE_integer);
			} else {
			// unpack tar
				// 1. ins zielverzeichnis wechseln + alles entpacken
				#$sZIPcommand = "tar --ungzip -xf ".$sArchivePath.$sRestoreFile." ".$sIncludeFiles; // <-- geht nicht...
				$sZIPcommand = "cd ".$sContainerPath."; tar xzf ".$sArchivePath.$sRestoreFile; // (-x = extract; -z = unkopr. mit gzip; -f = file)
				$zipE_array = array(); $zipE_integer = 0; // init vars
				$zipE_string = exec($sZIPcommand, $zipE_array, $zipE_integer); // store messages in vars
				if ($DEBUG) {// DEBUG
					echo "slot: ".$slot."<br>\n";
					echo "sZIPcommand: ".$sZIPcommand."<br>\n";
					echo "zipE_integer: ".$zipE_integer."<br>\n";
					echo "zipE_string: ".$zipE_string."<br>\n zipE_array: <br>\n";
					if(is_array($zipE_array)) {foreach($zipE_array as $line) echo $line."<br>\n";}
				}
				unset($sZIPcommand); unset($zipE_string); unset($zipE_array); unset($zipE_integer);
				// 2. leere zielverzeichnis (slot)
				$oFile->clear_folder($sContainerPath);
				// 3. kopiere nur die relevanten Dateien aus dem unterordner "data" in das zielverzeichnis
				$sCopyFiles = "cp ";
				$sCopyFiles .= $sContainerPath."data/array.meta.php ";
				$sCopyFiles .= $sContainerPath."data/array.structure.php ";
				$sCopyFiles .= $sContainerPath."data/insert_navi.sql ";
				$sCopyFiles .= $sContainerPath."data/layouts.xml ";
				$sCopyFiles .= $sContainerPath."data/meta.xml ";
				$sCopyFiles .= $sContainerPath."data/structure.xml ";
				$sCopyFiles .= $sContainerPath;
				$copyE_array = array(); $copyE_integer = 0; // init vars
				$copyE_string = exec($sCopyFiles, $copyE_array, $copyE_integer); // store messages in vars
				// DEBUG
				if ($DEBUG) { 
					echo $copyE_integer."<br>\n"; unset($copyE_integer);
					echo $copyE_string."<br>\n"; unset($copyE_string);
					if (is_array($copyE_array)) { foreach($copyE_array as $line) echo $line."<br>\n"; } unset($copyE_array);
				}
				unset($sCopyFiles); unset($copyE_string); unset($copyE_array); unset($copyE_integer);
				// 3. nicht benoetigte dateien und verzeichnisse loeschen
				$oFile->delete_folder($sContainerPath."/data/media/.AppleDouble/");
				$oFile->delete_folder($sContainerPath."/data/media/");
				$oFile->delete_folder($sContainerPath."/data/.AppleDouble/");
				$oFile->delete_folder($sContainerPath."/data/");
				$oFile->delete_folder($sContainerPath."/.AppleDouble/");
			}
			
		// make inserts in navi/content table
			$aInserts = $oFile->read_file($sContainerPath."insert_navi.sql",true,true); // params: $sFileName[,$bIngnoreRemarks(default:false)][,$bReturnArray(default:false)]

			if (is_array($aInserts) && count($aInserts) > 0) {
				foreach ($aInserts as $line) {
					$line = trim($line); // trim ist wichtig (wegen vorhandenen zeilenumbruechen)!
					$line = str_replace(PMS_CONTENT, $aENV['table']['pms_content'][$slot], $line); // tablename anpassen ($slot hinzufuegen)
					// ANSI-Umlaute (VOR dem Import) in UTF-8 umwandeln!
					$line = Tools::convertToUTF8($line, $aENV);
					if(!ereg('`', $line) 
					&& !empty($line)) {
						$regex	= "/(INSERT\s+?INTO\s+?pms_content_\d{1,})\s+?\((.*?)\)(.*)/";
						preg_match($regex, $line, $match);
						
						$aTempE	= explode(",", $match[2]);
						$aTempE	= array_map('trim', $aTempE);
						$line	= $match[1]." (`".implode('`,`',$aTempE)."`) ".$match[3];
				    }					
					if (!$DEBUG) {
						if (!empty($line)) { $oDb->query($line); }
					} else { // DEBUG
						echo $line."<br>\n";
					}
				}
			}
			if (!$DEBUG) {
				$oFile->delete_files($sContainerPath."insert_navi.sql"); // params: $sFileName
			} else { // DEBUG
				echo "delete_files: insert_navi.sql"."<br>\n";
			}
			
		// schreibe meta-file immer NEU
			$sMetaFile = $sContainerPath."array.meta.php";
			// ggf. altes meta-file loeschens
			if (!$DEBUG) {
				$oFile->delete_files($sMetaFile); // params: $sFileName
			} else { // DEBUG
				echo "delete_files: ".$sMetaFile."<br>\n";
			}
			// neues meta-file erstellen
			$preg_pattern = '/\r\n|\r|\n/';
			// pflichtparameter ggf. ergaenzen
			if (!isset($aData['pTemplate']) || empty($aData['pTemplate'])) { $aData['pTemplate'] = 1; }
			// build ARRAY string
			$sContainerString = '<?php // this page was created automatically by design aspekt PMS. Do not make any changes!'."\n";
			$sContainerString .= '$aMeta[\'pId\'] = "'.pms_getUniqueId().'";'."\n";
			$sContainerString .= '$aMeta[\'pTitle\'] = "'.$aData['pTitle'].'";'."\n";
			$sContainerString .= '$aMeta[\'pClient\'] = "'.preg_replace($preg_pattern, '', $aData['pClient']).'";'."\n";
			$sContainerString .= '$aMeta[\'pDate\'] = "'.preg_replace($preg_pattern, '', $aData['pDate']).'";'."\n";
			$sContainerString .= '$aMeta[\'pComment\'] = "'.preg_replace($preg_pattern, '', $aData['pComment']).'";'."\n";
			$sContainerString .= '$aMeta[\'pAuthor\'] = "'.preg_replace($preg_pattern, '', $aData['pAuthor']).'";'."\n";
			$sContainerString .= '$aMeta[\'pTeam\'] = "'.preg_replace($preg_pattern, '', $aData['pTeam']).'";'."\n";
			$sContainerString .= '$aMeta[\'pTemplate\'] = "'.$aData['pTemplate'].'";'."\n";
			$sContainerString .= '$aMeta[\'pAutorun\'] = "'.$aData['pAutorun'].'";'."\n";
			$sContainerString .= '$aMeta[\'created\'] = "'.$oDate->get_mysql_timestamp().'";'."\n";
			$sContainerString .= '$aMeta[\'created_by\'] = "'.$Userdata['id'].'";'."\n";
			$sContainerString .= '$aMeta[\'last_modified\'] = "'.$oDate->get_mysql_timestamp().'";'."\n";
			#$sContainerString .= '$aMeta[\'last_mod_by\'] = "'.$Userdata['id'].'";'."\n";
			$sContainerString .= '?>';
			// make array file
			if (!$DEBUG) {
				$oFile->write_str_in_file($sMetaFile, $sContainerString); // params: $sFileName[,$sString='']
			} else { // DEBUG
				echo "write_str_in_file: ".$sContainerString."<br>\n";
			}
			
		// fertig
			return true;
		}
	}

?>