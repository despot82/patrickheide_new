<?php // Class cms_weblang
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung Sprachen zu verwalten. 
*
* Example: 
* <pre><code>
* require_once($aENV['path']['global_module']['unix']."class.cms_weblang.php");
* $oWeblang =& new cms_weblang(); // params: -
* $aAvailableLang = $oWeblang->getAllKeys(); // params: -
* $weblang = $oWeblang->getCurrentKey(); // params: [$bUseComfortCookie=true]
* // get country-code2 by IP
* $sCode2 = $oWeblang->getCountryCode($oDb); // params: &$oDb[,$sFallback='DE'][,$nTyp=2]
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.31 / 2005-04-29 [NEU: "getAllLanguages()" + "getDefaultKey()"]
*/
class cms_weblang {
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse cms_weblang:
		----------------------------------------------------------------------------
		konstruktor cms_weblang()
		function getAllLanguages()
		function getAllKeys()
		function getDefaultKey()
		function getCurrentKey()
		
		function getCountryCode(&$oDb, $sFallback='DE', $nTyp=2)
		private _get_ip()
		----------------------------------------------------------------------------
		HISTORY:
		1.31 / 2005-04-29 [NEU: "getAllLanguages()" + "getDefaultKey()"]
		1.3 / 2005-04-19 [NEU: "getCurrentKey()" mit Comfort-Cookie]
		1.2 / 2005-04-19 [NEU: UMBENANNT + "$sIp" aus "getCountryCode()" rausgenommen]
		1.1 / 2005-04-13 [NEU: "getCountryCode()"]
		1.0 / 2005-04-11 [NEU]
	*/

#-----------------------------------------------------------------------------

/**
* @access   public
* @var	 	array	verfuegbare Ausgabesprachen
*/
	var $aLanguage;
/**
* @access   private
* @var	 	string	default Ausgabesprache
*/
	var $sDefaultLangKey;
/**
* @access   private
* @var	 	string	aktuelle Ausgabesprache
*/
	var $sCurrentLangKey;

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das Objekt und setzt ein paar Variablen
*
* Beispiel: 
* <pre><code> 
* $oWeblang =& new cms_weblang(); // params: -
* </code></pre>
*
* @access   public
* @global 	array	$aENV	globales Environment-Array
* @return   void
*/
	function cms_weblang() {
		// vars
		global $aENV;
		if (!is_array($aENV)) die("ERROR: NO aENV!");
		// client_shortname
		$this->sClientShortname	= $aENV['client_shortname'];
		// available languages
		$this->aLanguage		= array();
		foreach ($aENV['content_language'] as $k => $v) {
			if ($k == 'default') continue;
			$this->aLanguage[$k]	= $v;
		}
		// default lang
		$this->sDefaultLangKey	= $aENV['content_language']['default'];
		// current lang (set default)
		$this->sCurrentLangKey = $this->sDefaultLangKey;
		#$this->sCurrentLangKey	= $aENV['content_language']['default']; // default
		#if (!empty($sCurrentLangKey)) {
		#	$this->sCurrentLangKey = $sCurrentLangKey; // ggf. overwrite default
		#}
	}

#-----------------------------------------------------------------------------

/**
* ermittelt alle Languages als assioziatives Array (key: LangKey, val: LangName).
*
* Beispiel: 
* <pre><code> 
* $aLang = $oWeblang->getAllLanguages(); // params: -
* </code></pre>
*
* @access   public
* @return   array
*/
	function getAllLanguages() {
		// output
		return $this->aLanguage;
	}

/**
* ermittelt alle Language-Keys.
*
* Beispiel: 
* <pre><code> 
* $aLangKeys = $oWeblang->getAllKeys(); // params: -
* </code></pre>
*
* @access   public
* @return   array
*/
	function getAllKeys() {
		// output
		return array_keys($this->aLanguage);
	}

/**
* ermittelt den Default-Language-Key.
*
* Beispiel: 
* <pre><code> 
* $sLangDefault = $oWeblang->getDefaultKey(); // params: -
* </code></pre>
*
* @access   public
* @return   array
*/
	function getDefaultKey() {
		// output
		return $this->sDefaultLangKey;
	}

/**
* ermittelt den aktuellen Language-Key.
*
* Beispiel: 
* <pre><code> 
* $sLangKey = $oWeblang->getCurrentKey(); // params: [$bUseComfortCookie=true]
* </code></pre>
*
* @access   public
* @param 	boolean	$bUseComfortCookie	bei true wird ein Comfort-Cookie geschrieben
* @return   string
*/
	function getCurrentKey($bUseComfortCookie=true) {
		$sCookieName = $this->sClientShortname.'_weblang';
		if ($bUseComfortCookie == true) {
			// 1. ggf. lese cookie (und ueberschreibe default)
			if (!empty($_COOKIE[$sCookieName]) && strlen($_COOKIE[$sCookieName]) == 2) {
				$this->sCurrentLangKey = $_COOKIE[$sCookieName];
			}
			// 2. nur wenn ueber Sprachwahl-Button Sprache gewaehlt wurde, wird Cookie geschrieben
			if (!empty($_GET['weblang'])) {
				$this->sCurrentLangKey = $_GET['weblang'];
				if ($this->sCurrentLangKey != $this->sDefaultLangKey) {
					// Cookie fuer "andere" Sprache ist "Comfort-Cookie" (lange Lebensdauer)!
					SetCookie($sCookieName, $this->sCurrentLangKey, time()+(3600*24*90));
				} else {
					// Cookie fuer Defaultsprache laeuft mit Schliessen des Browsers ab
					SetCookie($sCookieName, $this->sCurrentLangKey);
				}
			}
		} else {
			// default ggf. mit SESSION ueberschreiben?
			if (!empty($_SESSION[$sCookieName]) && strlen($_SESSION[$sCookieName]) == 2) {
				$this->sCurrentLangKey = $_SESSION[$sCookieName];
			}
			// default ggf. mit GET ueberschreiben?
			if (!empty($_GET['weblang']) && strlen($_GET['weblang']) == 2) {
				$this->sCurrentLangKey = $_GET['weblang'];
				$_SESSION[$sCookieName] = $_GET['weblang'];
			}
		}
		// output
		return $this->sCurrentLangKey;
	}

#-----------------------------------------------------------------------------

/**
* ermittelt den Country Code (Typ2) mittels IP. (Benoetigt dazu die DB-Table "ip_country"!)
* Optional kann als 2. Parameter ein country_code2 FALLBACK uebergeben werden (default: "DE").
* Optional kann wenn als 3. Parameter "3" uebergeben wird, der country_code3 zurueckgegeben werden.
*
* Beispiel: 
* <pre><code>
* $sCode = $oWeblang->getCountryCode($oDb); // params: &$oDb[,$sFallback='DE'][,$nTyp=2]
* </code></pre>
*
* @access   public
* @param 	object	$oDb		DB-Connect-Objekt
* @param 	string	$sFallback	CountryCode2-Fallback (optional - default: 'DE')
* @param 	int		$nTyp		CountryCode-Typ (optional - default: 2)
* @return   array
*/
	function getCountryCode(&$oDb, $sFallback='DE', $nTyp=2) {
		// check vars
		$sFallback = strToUpper($sFallback);
		if (!is_object($oDb)) return $sFallback;#die('NO OBJECT!');
		if ($nTyp != 2 && $nTyp != 3) $nTyp = 2;
		// get IP des UserAgents
		$sIp = $this->_get_ip();
		if ($sIp == 'unknown') return $sFallback;
		// sql
		$oDb->query("SELECT `country_code2` 
					FROM `ip_country` 
					WHERE `ip_from` <= INET_ATON('".$sIp."') 
						AND `ip_to` >= INET_ATON('".$sIp."')");
		// output
		return ($oDb->num_rows()) ? $oDb->fetch_field('country_code'.$nTyp) : $sFallback;
	}
/**
* Hilfsfunktion: ermittelt die IP-Adresse
* @access   private
*/
	function _get_ip() {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
		if (empty($ip)) { $ip = $_SERVER['REMOTE_ADDR']; }
		if (empty($ip)) { $ip = getenv('HTTP_CLIENT_IP'); }
		if (empty($ip)) { $ip = getenv('REMOTE_ADDR'); }
		if (empty($ip)) { $ip = 'unknown'; } // fallback (default)
		return $ip;
	}

#-----------------------------------------------------------------------------
} // END of class

?>