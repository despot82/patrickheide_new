<?php
/**
 * Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung die INFORM-Topnavi zu verwalten. 
 *
 * @access	public
 * @package	Content
 * @author	Andy Fehn <af@design-aspekt.com>
 * @version	1.01 / 2006-02-21 [umbenannt]
 */
class InformTopnavi {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse InformTopnavi:
		----------------------------------------------------------------------------
		konstruktor InformTopnavi(&$aTOPNAVI)
		function getHighlightKeys()
		function getCurentTitle()
		----------------------------------------------------------------------------
		HISTORY:
		1.01 / 2006-02-21 [umbenannt]
		1.0 / 2005-05-06
	*/

#-----------------------------------------------------------------------------

/**
 * @access	private
 * @var		array	Container fuer alle Navigationspunkte
 */
	var $aTopnavi = array();
/**
 * @access	private
 * @var		array	Array-Keys des aktuellen Navigationspunktes
 */
	var $aCurrentKey = '';

#-----------------------------------------------------------------------------

/**
 * Konstruktor -> Initialisiert das Objekt und setzt interne Variablen.
 *
 * Beispiel: 
 * <pre><code> 
 * $oTopnavi =& new InformTopnavi($aTOPNAVI); // params: $aTOPNAVI 
 * </code></pre>
 *
 * @access	public
 * @param	array	Topnavi-Array
 */
	function InformTopnavi(&$aTOPNAVI) {
		$this->aTopnavi = $aTOPNAVI;
		$this->_getCurrentKeys();
	}

/** Konstruktor-Hilfsfunktion
 * durchlaufe das array und finde die keys des Haptnavipunktes der aktuellen Seite.
 * @access	private
 */
	function _getCurrentKeys() {
		$aKey = '';
		foreach ($this->aTopnavi as $key1 => $v1) {
			foreach ($v1 as $key2 => $v2) {
				if (!empty($v2['id']) && strpos($_SERVER['PHP_SELF'], $v2['id'])) {
					$aKey['key1'] = $key1; // spezialisiertere ueberschreiben allgemeine...
					$aKey['key2'] = $key2; // spezialisiertere ueberschreiben allgemeine...
					#break(1);->eben nicht!
				}
			}
		}
		$this->aCurrentKey = $aKey;
	}

#-----------------------------------------------------------------------------

/**
 * gebe die keys des Haptnavipunktes der aktuellen Seite zurueck.
 *
 * Beispiel: 
 * <pre><code> 
 * $aHighlightKey = $oTopnavi->getCurrentKeys();
 * ...mit $aTOPNAVI[$aHighlightKey['key1']][0]... hat man den aktuellen Hauptpunkt!
 * ...mit $aTOPNAVI[$aHighlightKey['key1']][$aHighlightKey['key2']]... hat man den aktuellen Unterpunkt!
 * </code></pre>
 *
 * @access	public
 * @return	array	$aCurrentKey
 */
	function getCurrentKeys() {
		return $this->aCurrentKey;
	}

/**
 * gebe den Titel der aktuellen Seite zurueck.
 *
 * Beispiel: 
 * <pre><code> 
 * echo $oTopnavi->getCurentTitle();
 * </code></pre>
 *
 * @access	public
 * @return	string	Titel, sofern vorhanden, sonst leerer String
 */
	function getCurentTitle() {
		if (empty($this->aCurrentKey['key1']) || empty($this->aCurrentKey['key1'])) return '';
		return $this->aTopnavi[$this->aCurrentKey['key1']][$this->aCurrentKey['key2']]['title'];
	} 


#-----------------------------------------------------------------------------
} // END of class

?>