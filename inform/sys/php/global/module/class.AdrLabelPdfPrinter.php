<?php

function __cleanup_whitespace_recursive($var) {
	if (is_array($var)) {
		return array_map('__cleanup_whitespace_recursive', $var);
	} else {
		$var = trim($var);
		$var = str_replace("\r\n", "\n", $var);
		$var = str_replace("\r", "\n", $var);
		$var = preg_replace('/\n{2,}/', "\n", $var);
		return $var;
	}
}

	class AdrLabelPdfPrinter extends BeanService {

		var $LabelBeanDespatcher	= null;

		var $oCompanys = null;
		var $oDb	= null;
		var $aENV	= null;

		var $mailinglist	= null;
		var $format		= null;
		var $abfontsize	= 0;
		var $emfontsize	= 0;
		var $fontfamily	= null;
		var $saveonserver	= true;
		var $usetemplate	= 0;
		var $ablineheight	= 2;
		var $emlineheight	= 4;
		var $druckrandtop	= 3;
		var $druckrandleft	= 4;
		var $savename		= "";

		var $bIsExcel	= false;
		var $sSender	= "";


		function AdrLabelPdfPrinter(&$aENV) {
			$this->aENV =& $aENV;

			include($aENV['path']['global_bean']['unix'].'class.AdrLabelBeanDespatcher.php');

			$this->LabelBeanDespatcher = new AdrLabelBeanDespatcher();

		}

		function setIsExcel($bIsExcel=false) {
			$this->bIsExcel	= $bIsExcel;
		}

		function setODb($oDb) {

			$this->oDb = $oDb;

		}

		function setAENV($aENV) {

			$this->aENV = $aENV;

		}

		function setFontsize($abfosize,$emfosize) {

			$this->abfontsize = $abfosize;
			$this->emfontsize = $emfosize;

		}

		function setFontfamily($fofa) {

			$this->fontfamily = $fofa;

		}

		function setFormate($format) {

			$this->format = $format;

		}

		function setMailinglist($ml) {

			$this->mailinglist = $ml;

		}

		function getSavename() {

			return $this->savename;

		}

		function setDruckrandtop($drandt) {

			$this->druckrandtop = $drandt;

		}

		function setDruckrandleft($drandl) {

			$this->druckrandleft = $drandl;

		}

		function setSender() {
			$aSender	= $this->oDb->fetch_by_id($this->mailinglist, 'adr_mailing_name');
			$this->sSender	= utf8_decode($aSender['sender']);
		}

		function initialize() {

			include($this->aENV['path']['global_bean']['unix'].'class.AdrLabelBeanEmpfaenger.php');

			$this->setCompanyAdresses();

			$this->bean = $this->setBeanValues($this->getDBValues(), new AdrLabelBeanEmpfaenger());

   			$this->setGetBeanValues();

			$this->useTemplate();
			$this->setDruckrandtop($this->format[$this->usetemplate]['druckrandtop']);
			$this->setDruckrandleft($this->format[$this->usetemplate]['druckrandleft']);
   		}

		/**
		 * Diese Methode befuellt das Array, welches nachtraeglich dazu dient den Pointer zu bewegen
		 *
		 */

		function setGetBeanValues() {
			//'abcountry','abstrasse','abhnr','abplz','abname',
			$arbva = array('emadresstype',
						   'emcompany',
						   'emcountry',
						   'emstrasse',
						   'emort',
						   'emname',
						   'ememail',
						   'empobox',
						   'empoboxtown',
						   'embuilding',
						   'emregion',
						   'emrecipient');

			$this->getBeanValues($arbva);

		}

		function setCompanyAdresses() {

			include($this->aENV['path']['global_module']['unix']."class.adrCompanyEA.php");

			$oCompanys	=& new adrCompanyEA();
			$oCompanys->setODb($this->oDb);
			$oCompanys->initialize($this->aENV);

			$this->oCompanys	=& $oCompanys;

		}

		function getDBValues() {

			// EINTRAEGE in dieser mailingliste (ordered by "last_modified" ASC)
			$t1	= "adr_mailing_contact";
			$t2	= "adr_address";
			$t3 = "adr_contact";
			$t4 = "adr_country";

			$sql  = "SELECT $t2.email, $t2.recipient,$t3.title,$t3.firstname,$t3.surname,$t3.company,$t2.street,$t2.town";
			$sql .= ",$t2.po_box,$t2.po_box_town,$t2.building,$t2.region,$t2.recipient,$t4.country,$t1.address";
			$sql .= ",$t2.cat,$t2.company_id";
			$sql .= " FROM $t1 INNER JOIN $t2 ON $t1.contact_id=$t2.reference_id AND $t1.address=$t2.cat ";
			$sql .= " INNER JOIN $t3 ON $t1.contact_id=$t3.id LEFT JOIN $t4 ON $t4.code=$t2.country ";
			$sql .= "WHERE $t1.list_id='".$this->mailinglist."' ORDER BY $t1.last_modified ASC";

			$this->oDb->query($sql);

			$atown		= $this->oCompanys->bean->getTown();
			$acountry	= $this->oCompanys->bean->getCountry();
			$astreet	= $this->oCompanys->bean->getStreet();
			$apobox		= $this->oCompanys->bean->getPobox();
			$apoboxtown	= $this->oCompanys->bean->getPoboxtown();
			$abuilding	= $this->oCompanys->bean->getBuilding();
			$aregion	= $this->oCompanys->bean->getRegion();
			$acompanyname = $this->oCompanys->bean->getCompanyname();
			$arefid		= $this->oCompanys->bean->getReferenceid();

			for($i=0;$row = $this->oDb->fetch_object();$i++) {

				// if address is privat || company
				if($row->cat == 'p' || $row->cat == 'c') {

					$town = str_replace("  ", " ", $row->town);
					$bdaten["emstrasse"][$i]	= mb_convert_encoding($row->street, 'windows-1252', 'UTF-8');
					$bdaten["emort"][$i]		= mb_convert_encoding($town, 'windows-1252', 'UTF-8');
					$bdaten["emadresstype"][$i]	= mb_convert_encoding($row->address, 'windows-1252', 'UTF-8');
					$bdaten["emcompany"][$i]	= mb_convert_encoding($row->company, 'windows-1252', 'UTF-8');
					$bdaten["emcountry"][$i]	= utf8_decode($row->country);
					$bdaten["empobox"][$i]		= utf8_decode($row->po_box);
					$bdaten["empoboxtown"][$i]	= utf8_decode($row->po_box_town);
					$bdaten["embuilding"][$i]	= utf8_decode($row->building);
					$bdaten["emregion"][$i]		= utf8_decode($row->region);
					$bdaten["emcompanyid"][$i]	= utf8_decode($row->company_id);

				} else { // else if address is office1 || office2 || anything else

					$town = str_replace("  ", " ", $atown[$row->company_id]);
					$bdaten["emstrasse"][$i]	= mb_convert_encoding($astreet[$row->company_id], 'windows-1252', 'UTF-8');
					$bdaten["emort"][$i]		= mb_convert_encoding($town, 'windows-1252', 'UTF-8');
					$bdaten["emadresstype"][$i]	= mb_convert_encoding($row->address, 'windows-1252', 'UTF-8');
					$bdaten["emcompany"][$i]	= mb_convert_encoding(str_replace('&oelig;','oe', $acompanyname[$row->company_id]), 'windows-1252', 'UTF-8');
					$bdaten["emcountry"][$i]	= utf8_decode($acountry[$row->company_id]);
					$bdaten["empobox"][$i]		= utf8_decode($apobox[$row->company_id]);
					$bdaten["empoboxtown"][$i]	= utf8_decode($apoboxtown[$row->company_id]);
					$bdaten["embuilding"][$i]	= utf8_decode($abuilding[$row->company_id]);
					$bdaten["emregion"][$i]		= utf8_decode($aregion[$row->company_id]);
					$bdaten["emcompanyid"][$i]	= utf8_decode($arefid[$row->company_id]);

				}
				$bdaten["emrecipient"][$i]	= utf8_decode($row->recipient);
				$bdaten["ememail"][$i]	= $row->email;

				if($this->bIsExcel==false) {
				$name	= str_replace(array('{FIRSTNAME}', '{SURNAME}', '{TITLE}', '{COMPANY}'),
										  array(utf8_decode($row->firstname).' ', utf8_decode($row->surname).' ', utf8_decode($row->title).' ', $bdaten["emcompany"][$i]),
										  $this->aENV['address']['format']);

					$name	= preg_replace('/\{\w+?\}/i', '', $name);
					$name	= preg_replace('/[ ]{2,}/', ' ', $name);

					$bdaten["emname"][$i]	= trim(($name));
				} else {
					$bdaten["emname"][$i]	= trim(utf8_decode($row->title." ".$row->firstname." ".$row->surname));
				}

			}

			return __cleanup_whitespace_recursive($bdaten);

		}

		function useTemplate() {

			$sql = "SELECT template FROM adr_mailing_name WHERE id='".$this->mailinglist."'";

			$this->oDb->query($sql);

			$row = $this->oDb->fetch_object();

			$template = $row->template;

			for($i=0;!empty($this->format[$i]["intname"]);$i++) {

				if($template == $this->format[$i]["intname"]) {

					$this->usetemplate = $i;
				}

			}

		}

		function printPdf() {

			include($this->aENV['path']['pear']['unix'].'PDF.php');

			$pdf	= &File_PDF::factory(array('orientation' => 'P','unit' => 'mm','format' => 'A4'));
  			$pdf->open();

  			//$cfonts	= $pdf->_getCorefonts();
  			$cfonts	= $pdf->_core_fonts;
  			$iscore	= false;

  			foreach ($cfonts as $key) {

  				if($key == $this->fontfamily) {
  					$iscore = true;
  					break;

  				}

  			}

  			// Wenn die von uns gewaehlte Schrift keine Standardschrift ist, wird sie hinzugefuegt.
  			if(!$iscore) {

  				$pdf->path = $this->aENV['path']['pear']['unix'].'PDF/fonts/';
  				$pdf->addFont($this->fontfamily,'',$this->aENV['path']['pear']['unix'].'PDF/fonts/metanormal.php');

  			}

  			if($this->format[$this->usetemplate]['margintop']!=0) {

  				$marginbottom	= $this->format[$this->usetemplate]['margintop']/10;

  			} else {

  				$marginbottom = 0;

  			}

  			$pdf->setAutoPageBreak(true,$marginbottom);

  			// Schreibt die formatierten Label in das Dokument
  			$pdf = $this->printLabel($pdf);

  			if($this->saveonserver) {

  				$this->savename = "Labels".$this->format[$this->usetemplate]['intname'].$this->mailinglist.".pdf";
				// Die Datei einmal auf dem Server speichern
  				$pdf->save($this->aENV['path']['adr_data']['unix'].$this->savename);
  			}

		}

		function printLabel($pdf) {

			for($i=0, $zelle=1, $zeile=0; $this->beanNext(); $i++, $zelle++) {

				if($i==0) {

					$pdf->addPage();
					$pdf->setRightMargin(0);
					$pdf->setLeftMargin(0);
					$zeile	= 1;

				}

				if(($zelle-1) == $this->format[$this->usetemplate]['cols']
				&& $zeile%$this->format[$this->usetemplate]['rows'] == 0) {

					$pdf->addPage();
					$pdf->setRightMargin(0);
					$pdf->setLeftMargin(0);

					$zeile	= 0;

				}

				if($i%$this->format[$this->usetemplate]['cols'] == 0) {

					$zelle	= 1;
					if($i!=0)
						$zeile++;

				}

				for($j=1; $j <= $zelle; $j++) {

					if($zelle == 1) {

						$pdf->setX(($this->druckrandleft + $this->format[$this->usetemplate]['marginleft']));

					} else {

						$rand	= $this->druckrandleft + $this->format[$this->usetemplate]['marginleft'];

						if($this->format[$this->usetemplate]['marginmiddle'] != 0 && $zelle != 1) {

							$rand	= ($rand + $this->format[$this->usetemplate]['marginmiddle']);

						}

						$koord	= $this->format[$this->usetemplate]['width']*($zelle-1);

						$pdf->setX($koord+$rand);

					} // end if

				} // end for

				for($j=1; $j <= $zeile; $j++) {

					if($zeile == 1) {

						$pdf->setY(($this->format[$this->usetemplate]['margintop']+$this->druckrandtop));

					} else {

						$rand	= $this->druckrandtop;
						$koord	= $this->format[$this->usetemplate]['height']*($zeile-1);

						$pdf->setY($koord+$rand+$this->format[$this->usetemplate]['margintop']);

					} // end if

				} // end for

				$pdf->setFont($this->fontfamily, '', $this->abfontsize);
				$pdf->setFontSize($this->abfontsize);

				$text	= $this->sSender."\n\n";
				$x		= $pdf->getX();
				$pdf->write($this->ablineheight, $text);

				$pdf->setFont($this->fontfamily, '', $this->emfontsize);

				$sRecipient	= $this->getValueByName('emrecipient');

				if(!empty($sRecipient)) {

					$pdf->setX($x);

					$text = $sRecipient."\n";

					$pdf->write($this->emlineheight, $text);

				}

				$sAdrtype	= $this->getValueByName('emadresstype');

				if(empty($sRecipient)
				&& $sAdrtype != 'o1'
				&& $sAdrtype != 'o2'
				&& $sAdrtype != 'c') {

					$text = $this->getValueByName('emname')."\n";

					$this->setNewLineText($text, $x, $pdf);
				}

				if($sAdrtype != 'p'
				&& $sAdrtype != 'c') {
					$text = $this->getValueByName('emname')."\n";

					$this->setNewLineText($text, $x, $pdf);

				}

				if($sAdrtype == 'c') {

					$text	= $this->getValueByName('emcompany');

					if(strlen(trim($text)) > $this->format[$this->usetemplate]['maxwidth']
					&& !ereg("\n", $text)) {

						$text	= $this->addNewLine($text);

					}

					if(ereg("\n", $text)) {

						$this->setNewLineText($text, $x, $pdf);

					} else {

						$text	.= "\n";
						$pdf->setX($x);

						$pdf->write($this->emlineheight, $text);

					}

				}

				$empobox		= $this->getValueByName('empobox');
				$empoboxtown	= $this->getValueByName('empoboxtown');

				if(!empty($empobox)
				&& !empty($empoboxtown)) {

					$text	= $this->getValueByName('empobox')."\n";
					$pdf->setX($x);
					$pdf->write($this->emlineheight, $text);

					$text	= $this->getValueByName('empoboxtown')."\n";
					$pdf->setX($x);
					$pdf->write($this->emlineheight, $text);

				} else {

					$text	= $this->getValueByName('emstrasse')."\n";
					$pdf->setX($x);
					$pdf->write($this->emlineheight, $text);

					$embuilding	= $this->getValueByName('embuilding');

					if(!empty($embuilding)) {

						$text	= $embuilding."\n";
						$pdf->setX($x);
						$pdf->write($this->emlineheight, $text);

					}

					$emregion	= $this->getValueByName('emregion');

					if(!empty($emregion)) {

						$text	= $emregion."\n";
						$pdf->setX($x);
						$pdf->write($this->emlineheight, $text);

					}

					$emort	= $this->getValueByName('emort');

					if(!empty($emort)) {

						$text	= $emort."\n";

						$pdf->setX($x);
						$pdf->write($this->emlineheight, $text);
					}
				}

				$text	= $this->getValueByName('emcountry');
				// Fix für Südlicht, da die keine Länder eingepflegt haben!
				if($text != 'Other') {
					$pdf->setX($x);
					$pdf->write($this->emlineheight, $text);
				}
			}

			return $pdf;
		}

		function addNewLine($text) {

			$atext = explode(" ",$text);

			for($i=0;!empty($atext[$i]);$i++) {

				$artext[$i]['text'] = $atext[$i];
				$artext[$i]['len'] = strlen($atext[$i]);
			}
			$tlen = 0;

			for($i=0;!empty($artext[$i]['text']);$i++) {

				$tlen += $artext[$i]['len'];

				if($tlen > $this->format[$this->usetemplate]['maxwidth']) {

					$tlen = $artext[$i]['len'];
					$artext[$i]['text'] = "\n".$artext[$i]['text'];

				}

				$tlen++;

			}

			unset($text);

			for($i=0;!empty($artext[$i]['text']);$i++) {

				$text .= $artext[$i]['text']." ";

			}
			return $text;
		}

		function setNewLineText($text,$x,&$pdf) {

			$comps = explode("\n",$text);

			for($i=0;!empty($comps[$i]);$i++) {

				$text = trim($comps[$i]);

				$text .= "\n";

				$pdf->setX($x);

				$pdf->write($this->emlineheight,$text);

			}
		}
	}
?>
