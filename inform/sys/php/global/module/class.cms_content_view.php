<?php // Class cms_content_view
/**
* Diese Klasse vereinfacht das Auslesen einer Datensatz-Liste. 
*
* NOTE: benoetigt die Klasse "class.db_mysql.php"!
*
* Example: 
* <pre><code> 
* // init + set vars
* require_once($aENV['path']['global_module']['unix'].'class.cms_content_view.php');
* $oView =& new cms_content_view($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]
* // bestimme, welche Felder ausgelesen werden muessen (default: *) -> Performance!
* $oView->setFields(array('name', 'flag_online_de', 'flag_online_en'));
* // bestimme, welche Felder als Link zur verfuegung stehen muessen -> diese Felder muessen auch vom Preview-Datensatz ermittelt werden!
* $oView->setLinkFields(array('name'));
* #$oView->setConstraint('parent_id = 0');
* $oView->setOrderBy('name ASC');
* $oView->setLimit($limit);
* // uebergebe navi_id (ausser sie waere bei einer Liste kein DB-Feld, dann koennte das weggelassen werden)
* $oView->setNaviId($navi_id);
* // Link zur Edit-Seite (und ggf. weiterer GET-Vars)
* $oView->setEditPage($sEditPage);
* 
* // Erzeuge die Liste
* $oView->select(); // params: - 
* while ($oView->hasNext()) {
*	echo $oView->nPos;
*	echo $oView->getField('title_de'); // params: $sText[,$nLength=0]
*	echo $oView->getPrioButtons(); // params: [$sGEToptions='']
*	echo $oView->getLink('name'); // params: $sField[,$sGEToptions=''][,$nLength=80][,$bFallback=true]
*	echo $oView->getEditStatus('edit_status'); // params: $sField[,$sGEToptions=''] 
*	foreach ($aAvailableLanguages as $key => $val) {
*		echo $oView->getOnlineStatus('flag_online_'.$key, 'name', '&weblang='.$key); // params: $sField,$sDataField[,$sGEToptions]
*	}
* }
* 
* // Blaettern
* echo $oView->getDbStatus(); // params: -
* echo $oView->getDbLinks(); // params: [$sGEToptions=''][,$nPageLimit=5]
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.2 / 2005-08-31	[NEU: "movePrio()"]
*/
class cms_content_view {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse search:
		----------------------------------------------------------------------------
		konstruktor cms_content_view($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]
		function setFields($aFieldnames)
		function setLinkFields($aField)
		function setConstraint($sConstraint)
		function setOrderBy($sField)
		function setLimit($nLimit)
		function setNaviId($sNaviId)
		function setEditPage($sEditPage)
		function movePrio($sCatField='', $sAddSqlConstraint='')
		
		function select()
		function hasNext()
		
		function getField($sField, $nLength=0)
		function getLink($sField, $sGEToptions='', $nLength=80, $bFallback=true)
		function buildLink($sText, $sGEToptions='', $nLength=80, $bFallback=true)
		private _shortenString($sStr, $nLength)
		function getOnlineStatus($sField, $sDataField='', $sGEToptions='')
		function getEditStatus($sField, $sGEToptions='')
		function getPrioButtons($sGEToptions='')
		
		function getDbStatus()
		function getDbLinks($sGEToptions='', $nPageLimit=5)
		----------------------------------------------------------------------------
		HISTORY:
		1.2 / 2005-08-31	[NEU: "movePrio()"]
		1.12 / 2005-08-30	[BUGFIX: Constraint-Beruecksichtigung bei movePrio() in "select()" eingebaut]
		1.11 / 2005-08-18	[BUGFIX: $entries Berechnung bei "hasNext()" eingebaut]
		1.1 / 2005-08-16	[NEU: "buildLink()" + "getLink()" umgebaut]
		1.0 / 2005-05-13
	*/
### TODO: select::movePrio: $sAddSqlConstraint konfigurierbar machen!

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	object	DB-Objekt (muss beim initialisieren der Klasse uebergeben werden)
*/
	var $oDb;
/**
* @access   private
* @var	 	boolean	true (mit Vorschau) oder false (wird beim initialisieren der Klasse uebergeben)
*/
	var $bWithPreview;
/**
* @access   private
* @var	 	array	globales Environment-Array
*/
	var $aENV;
/**
* @access   private
* @var	 	array	globales Messages-Array
*/
	var $aMSG;
/**
* @access   private
* @var	 	string	globale System-Sprache
*/
	var $syslang;
/**
* @access   private
* @var	 	int		DB-LIMIT Startwert
*/
	var $nStart;
/**
* @access   private
* @var	 	string	Name der Datenbank-Tabelle
*/
	var $sTable;

/**
* @access   private
* @var	 	array	Namen der Felder, die ausgelesen werden
*/
	var $aFieldnames;
/**
* @access   private
* @var	 	array	Namen der Felder, die zur Detailseite linken
*/
	var $aLinkFields;
/**
* @access   private
* @var	 	string	WHERE constraint
*/
	var $sConstraint;
/**
* @access   private
* @var	 	string	ORDER BY statement
*/
	var $sOrderBy;
/**
* @access   private
* @var	 	int		DB-LIMIT value
*/
	var $nLimit;
/**
* @access   private
* @var	 	string	Navi-ID der Seite
*/
	var $sNaviId;
/**
* @access   private
* @var	 	string	Link zur Edit-Seite
*/
	var $sEditPage;
/**
* @access   private
* @var	 	int		DB-NUM_ROWS
*/
	var $nEntries;
/**
* @access   private
* @var	 	array	ermittelte Datensaetze als Array
*/
	var $aList;
/**
* @access   private
* @var	 	boolean	Flag, ob bei "select()" die Prio-Funktion aufgerufen werden soll
*/
	var $bMovePrio;

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das Objekt und bekommt ein DB-Objekt uebergeben
*
* Beispiel: 
* <pre><code> 
* $oView =& new cms_content_view($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]
* </code></pre>
*
* @access   public
* @global	array	$aENV
* @param	object	$oDb		Datenbank-Objekt, welches fuer die Klasse "class.db_mysql.php" benoetigt wird
* @param	string	sTable		Name der DB-Table
* @return   void
*/
	function cms_content_view(&$oDb, $sTable, $bWithPreview=false) {
		// DB-Objekt importieren
		if (!is_object($oDb)) {
			echo "<script>alert('".basename(__FILE__)."-ERROR: \\n -> Es wurde kein DB-Objekt uebergeben!')</script>";
			return;
		}
		$this->oDb			= $oDb;
		// content table
		$this->sTable		= $sTable;
		// mit preview?
		$this->bWithPreview	= $bWithPreview;
		// globales ENV-Array importieren
		global $aENV;
		if (!is_array($aENV)) die("ERROR: NO aENV!");
		$this->aENV			= $aENV;
		// globales MSG-Array importieren
		global $aMSG;
		if (!is_array($aMSG)) die("ERROR: NO aMSG!");
		$this->aMSG			= $aMSG;
		// system-lang importieren
		global $syslang;
		if (empty($syslang)) {
			$syslang		= $aENV['system_language']['default'];
		}
		$this->syslang		= $syslang;
		// start (DB-Navi)
		$this->nStart		= (isset($_GET['start'])) ? $_GET['start'] + 0 : 0;
		// set defaults
		$this->aList		= array(); // Liste-Array
		###$this->nPos (erst bei "select()"!)
		$this->aFieldnames	= '*';
		$this->setLinkFields= '';
		$this->setConstraint= '';
		$this->setOrderBy	= '';
		$this->setLimit		= 0;
		$this->setNaviId	= '';
		$this->setEditPage	= '';
		$this->bMovePrio	= true;
	}

#----------------------------------------------------------------------------- SET

/**
* Setzt Fieldnames, die aus der Datenbank ausgelesen werden
* @access   public
* @param	array	aFieldnames		Namen der Felder als Array
*/
	function setFields($aFieldnames) {
		if (!in_array('id', $aFieldnames)) {
			$aFieldnames[] = 'id';
		}
		$this->aFieldnames	= $aFieldnames;
	}

/**
* Gibt an, welche der Felder zur Detailseite linken
* @access   public
* @param	string	aField		Name der/des Feldes als array
*/
	function setLinkFields($aField) {
		$this->aLinkFields	= $aField;
	}

/**
* Setzt etwaige zusaetzliche Constraints
* @access   public
* @param	string	sConstraint		SQL Bedingung
*/
	function setConstraint($sConstraint) {
		$this->sConstraint	= $sConstraint;
	}

/**
* Setzt die Ausgabereihenfolge
* @access   public
* @param	string	sField		Name des/der Feldes/Felder (kommasepariert)
*/
	function setOrderBy($sField) {
		$this->sOrderBy		= $sField;
	}

/**
* Setzt die Anzahl auszulesender Datensaetze
* @access   public
* @param	int		nLimit
*/
	function setLimit($nLimit) {
		$this->nLimit		= $nLimit;
	}

/**
* Setzt die navi_id der Seite
* @access   public
* @param	string	sNaviId
*/
	function setNaviId($sNaviId) {
		$this->sNaviId		= $sNaviId;
	}

/**
* Setzt die navi_id der Seite
* @access   public
* @param	string	sNaviId
*/
	function setEditPage($sEditPage) {
		if (empty($sEditPage)) return;
		if (!strpos($sEditPage, '?')) {
			$sEditPage .= '?';
		}
		if ($this->nStart > 0) {
			$sEditPage .= '&start='.$this->nStart;
		}
		$this->sEditPage	= $sEditPage;
	}

/**
* Manuelles anstossen der PRIO-Funktion (laedt ggf. die Seite neu!).
* NOTE: Die Prio-Funktion wird in diesem Fall nicht nochmal bei "Select()" aufgerufen.
* @access   public
* @param	string	$sCatFeldname		Name der DB-column, in dem die Unterteilung gespeichert ist (nur angeben, wenn unterteilt werden soll!)
* @param	string	$sAddSqlConstraint	fuer evtl. zusaetzliche DB-Bedingung (in der Form "AND field='value'")
*/
	function movePrio($sCatField='', $sAddSqlConstraint='') {
		// Prio-Funktion aufrufen
		movePrio($this->sTable, $sCatField, $sAddSqlConstraint); // params: "meine_tabelle"[, "mein_kategorie_feld"][,$sAddSqlConstraint='']
		// Flag setzen, dass die Funktion nicht nochmals bei "select()" aufgerufen wird
		$this->bMovePrio	= false;
	}

#------------------------------------------------------------------------------- SELECT

/**
* ADMIN: Ermittelt den gesuchten Inhalt in der DB-Tabelle.
* NOTE: Wenn keine Datensaetze gefunden werden, wird ein leeres Array zurueckgegeben.
*
* Beispiel:
* <pre><code>
* $aList = $oView->select(); // params: -
* </code></pre>
*
* @access   public
* @return	array	leeres oder mit Daten gefuelltes aData-array
*/
	function select() {
		// init
		$buffer = array();
		$counter = 0; // zaehler
		// Aktuelle Position in der Liste
		$this->nPos			= -1; // damit nach dem ersten hochzaehlen "0", also der erste Array-Eintrag erreicht wird
		// DEBUG $this->oDb->debug=true;
		
		// ggf. PRIO zuerst aufrufen!
		if ($this->bMovePrio == true) {
			$sCatField = (!empty($this->sNaviId)) ? 'navi_id' : '';
			$sAddSqlConstraint = ($this->sConstraint != '') ? ' AND '.$this->sConstraint : ''; // wenn es ein constraint gibt, hier ebenfalls beruecksichtigen!
			movePrio($this->sTable, $sCatField, $sAddSqlConstraint); // params: "meine_tabelle"[, "mein_kategorie_feld"][,$sAddSqlConstraint='']
		}
		
		// build query
		if (!empty($this->sNaviId) && $this->aFieldnames != '*') {
			$this->aFieldnames[] = "navi_id";
		}
		if ($this->aFieldnames == '*') {
			$this->aFieldnames = $this->oDb->get_fields($this->sTable);
		}
		$sQuery = "SELECT `".implode('`, `', $this->aFieldnames)."` ";
		$sQuery .= "FROM `".$this->sTable."` WHERE ";
		$sQuery .= ($this->bWithPreview == true) ? "(`preview_ref_id` = 0 OR `preview_ref_id` IS NULL)" : '1';
		if (!empty($this->sNaviId)) {
			$sQuery .= " AND `navi_id`='".$this->sNaviId."'";
		}
		if (!empty($this->sConstraint)) {
			$sQuery .= " AND ".$this->sConstraint;
		}
		$sQuery .= " ORDER BY ".$this->sOrderBy;
		
		// query
		$this->oDb->query($sQuery);
		$this->nEntries = floor($this->oDb->num_rows());	// noetig fuer DB-Navi und PRIO!
		if ($this->nLimit > 0 && $this->nLimit < $this->nEntries) {
			$this->oDb->limit($sQuery, $this->nStart, $this->nLimit);
		}
		
		// 1. durchlauf
		$counter = 0;
		while ($aData = $this->oDb->fetch_array()) {
			foreach ($this->aFieldnames as $key) {
				$buffer[$counter][$key] = $aData[$key];
				// bearbeitungs-zustand
				$buffer[$counter]['edit_status'] = 'live';
			}
			$counter++; // zaehle hoch
		}
		// ggf. 2.durchlauf
		if ($this->bWithPreview == true && count($this->aLinkFields) > 0) {
			foreach ($buffer as $key => $val) {
				// preview-version ermitteln
				$this->oDb->query("SELECT `id`, `".implode('`, `', $this->aLinkFields)."` 
									FROM `".$this->sTable."` 
									WHERE `preview_ref_id`='".$val['id']."'");
				$aData2 = $this->oDb->fetch_array();
				// titel von preview-version holen, wenn noch keiner i.d. live-version existiert
				foreach ($this->aLinkFields as $field) {
					if (!isset($val[$field]) || empty($val[$field])) {
						$buffer[$key][$field] = $aData2[$field];
					}
				}
				// bearbeitungs-zustand
				$buffer[$key]['edit_status'] = (isset($aData2['id']) && !empty($aData2['id'])) ? 'preview' : 'live';
			}
		}
		#print_r($buffer); // DEBUG
		return $this->aList	= $buffer;
	}


#----------------------------------------------------------------------------- GET

/**
* Setzt die aktuelle Position der Liste um eins hoch (solage bis das Ende der Liste erreicht ist).
* 
* @access   public
* @return	bool	false, wenn das Ende der Liste erreicht ist
*/
	function hasNext() {
		$this->nPos++;
		$entries = ($this->nLimit > 0 && $this->nLimit < ($this->nEntries - $this->nStart)) 
			? $this->nLimit 
			: ($this->nEntries - $this->nStart);
		return ($this->nPos < $entries) ? true : false;
	}

/**
* Gibt die Gesamtanzahl gefundener Datensaetze zurueck.
* 
* @access   public
* @return	int		Gesamtanzahl Datensaetze
*/
	function getEntries() {
		return $this->nEntries;
	}

/**
* Ermittelt den Wert des Feldes, dessen Name uebergeben wird.
* Wird als zweiter optionaler Parameter eine Zahl uebergeben, wird der zurueckgegebene String auf diese Anzahl Zeichen gekuerzt.
* 
* Beispiel:
* <pre><code>
* echo $oView->getField('title_de'); // params: $sField[,$nLength=0]
* </code></pre>
*
* @access   public
* @param	string	$sField			Name des DB-Feldes (z.B. 'headline')
* @param	int		[$nLength]		Anzahl Zeichen, nachdenen abgeschnitten wird	(optional -> default:'80')
* @return	string	Wert des Feldes oder leerer String
*/
	function getField($sField, $nLength=0) {
		// get value
		/*$buffer = (isset($this->aList[$this->nPos][$sField])) 
			? $this->aList[$this->nPos][$sField] 
			: die('ERROR: "'.$sField.'" muss bei "setFields()" abgefragt werden!');#return;*/
		$buffer = (array_key_exists($sField, $this->aList[$this->nPos])) 
			? $this->aList[$this->nPos][$sField] 
			: die('ERROR: "'.$sField.'" muss bei "setFields()" abgefragt werden!');#return;
		$buffer =  $this->aList[$this->nPos][$sField];
		// output
		return ($nLength > 0) ? shorten_text($buffer, $nLength) : $buffer;
	}

/**
* Shortcut fuer "getField()" + "buildLink()"
*
* Beispiel:
* <pre><code>
* echo $oView->getLink('title_de'); // params: $sField[,$sGEToptions=''][,$nLength=80][,$bFallback=true]
* </code></pre>
*
* @access	public
* @param	string	$sText			Langtext
* @param	string	[$sGEToptions]	Zusaetzliche GET-Parameter	(optional -> default:'')
* @param	int		[$nLength]		Anzahl Zeichen, nach denen abgeschnitten wird	(optional -> default:'80')
* @param	boolean	[$bFallback]	["true" (default) gibt bei "keinem text" eine entsprechende meldung aus | "false" nicht ]
* @return 	string					(verlinkter/gekuerzter) Text
*/
	function getLink($sField, $sGEToptions='', $nLength=80, $bFallback=true) {
		// output
		return $this->buildLink($this->getField($sField), $sGEToptions, $nLength, $bFallback);
	}

/**
* Verlinkt den uebergebenen String mit einem ggf. uebergebenen Link 
* und verkuerzt ihn ggf..
*
* Beispiel:
* <pre><code>
* $date = $oView->getField('date'); // params: $sText[,$nLength=0]
* echo $oView->buildLink($oDb->date_mysql2trad($date)); // params: $sText[,$sGEToptions=''][,$nLength=80][,$bFallback=true]
* </code></pre>
*
* @access	public
* @param	string	$sText			Text, der verlinkt wird
* @param	string	[$sGEToptions]	Zusaetzliche GET-Parameter	(optional -> default:'')
* @param	int		[$nLength]		Anzahl Zeichen, nach denen abgeschnitten wird	(optional -> default:'80')
* @param	boolean	[$bFallback]	["true" (default) gibt bei "keinem text" eine entsprechende meldung aus | "false" nicht ]
* @return 	string					(verlinkter/gekuerzter) Text
*/
	function buildLink($sText, $sGEToptions='', $nLength=80, $bFallback=true) {
		// vars
		$buffer = '';
		$sGEToptions = str_replace('?', '&', $sGEToptions);
		// fallback
		if ($bFallback == true && empty($sText)) {
			$sText = $this->aMSG['std']['no_entries_lang'][$this->syslang];
		}
		// code
		$sText = $this->_shortenString($sText, $nLength);
		// build string
		if (!empty($this->sEditPage)) {
			$buffer .= '<a href="'.$this->sEditPage.'&id='.$this->getField('id').$sGEToptions.'" title="'.$this->aMSG['btn']['edit'][$this->syslang].'">';
		}
		$buffer .= $sText;
		if (!empty($this->sEditPage)) {
			$buffer .= '</a>';
		}
		// output
		return $buffer;
	}

/**
* Hilfsfunktion, die den uebergegebenen String auf uebergebene Anzahl Zeichen kuerzt.
* NOTE: Es werden ausserdem HTML-Tags entfernt!
* 
* @access   private
* @param	string	$sStr			String, der gekuerzt wird
* @param	int		$nLength		Anzahl Zeichen, nach denen abgeschnitten wird
* @return	string	gekuerzter String
*/
	function _shortenString($sStr, $nLength) {
		// vars
		$sStr = strip_tags(trim($sStr));
		// code
		if (strlen($sStr) > $nLength) {
			$sStr = substr($sStr, 0, ($nLength-3)).'...';
		}
		// output
		return $sStr;
	}

/**
* Zeigt den Freigabestatus (des Live-Datensatzes) in Form eines Img-Tags an 
* und verlinkt ggf. das Icon mit der Edit-Seite.
*
* Beispiel:
* <pre><code>
* echo $oView->getOnlineStatus('flag_online_de', 'title_de', '&weblang=de');
* </code></pre>
*
* @access	public
* @uses		function "compareOnlineFromToWithToday()" [file: func.admin_common.php]
* @param	integer	$sField			Feldname, der den Freigabestatus enthaelt [ 0 fuer "nicht Freigegeben" | 1 fuer "Freigegeben"]
* @param	string	$sDataField		(z.B. die Headline): wenn $sData leer ist, wird ein Image fuer "noch nicht angelegt" angezeigt
* @param	string	[$sGEToptions]	f.d. edit-page (optional)
* @param	string	[$sOnlineFromField]	Start-Datums-Feldname bei Zeitsteuerung (optional)
* @param	string	[$sOnlineToField]	End-Datums-Feldname bei Zeitsteuerung (optional)
* @return	html-code
*/
	function getOnlineStatus($sField, $sDataField='', $sGEToptions='', $sOnlineFromField='', $sOnlineToField='') {
		// vars
		$buffer =  '';
		if (!empty($sGEToptions)) {
			$sGEToptions = str_replace('?', '&', $sGEToptions);
		}
		// status
		$nStatus = $this->getField($sField);
		switch ($nStatus) {
			case '0': $img = 'icn_off.gif';		$alt = 'Offline';	break;
			case '1': $img = 'icn_on.gif'; 		$alt = 'Online';	break;
		}
		// offline wegen zeitsteuerung?
		if (!empty($sOnlineFromField) && !empty($sOnlineToField)) {
			$sOnlineFrom = $this->getField($sOnlineFromField);
			$sOnlineTo = $this->getField($sOnlineToField);
			$isValid = compareOnlineFromToWithToday($sOnlineFrom, $sOnlineTo);
			if (!$isValid) {
				$img = 'icn_on_timer.gif';
				$alt = 'Offline';
			}
		}
		// leerer datensatz?
		if (!empty($sDataField)) {
			$aData = $this->getField($sDataField);
			if (empty($aData) || !isset($img)) {
				$img = 'icn_empty.gif';
				$alt = $this->aMSG['btn']['edit'][$this->syslang];
			}
		}
		// image (/link)
		if (file_exists($this->aENV['path']['pix']['unix'].$img)) {
			if (!empty($this->sEditPage)) {
				$buffer .= '<a href="'.$this->sEditPage.'&id='.$this->getField('id').$sGEToptions.'" title="'.$alt.'">';
			}
			#$imgsize = getimagesize($this->aENV['path']['pix']['unix'].$img);
			#$buffer .= '<img src="'.$this->aENV['path']['pix']['http'].$img.'" '.$imgsize[3].' alt="'.$alt.'" class="btn"';
			$buffer .= '<img src="'.$this->aENV['path']['pix']['http'].$img.'" alt="'.$alt.'" class="btn">';
			if (!empty($this->sEditPage)) {
				$buffer .= '</a>';
			}
			$buffer .= "\n";
		}
		// output
		return $buffer;
	}

/**
* Zeigt den Bearbeitungsstatus [live|preview] in Form eines Img-Tags an 
* und verlinkt ggf. das Icon mit der Edit-Seite.
*
* Beispiel:
* <pre><code>
* echo $oView->getEditStatus('edit_status'); // params: $sField[,$sGEToptions='']
* </code></pre>
*
* @access	public
* @param	integer	$sField		[live|preview]
* @param	string	[$sGEToptions]	f.d. edit-page (optional)
* @return	html-code
*/
	function getEditStatus($sField, $sGEToptions='') {
		// vars
		$buffer =  '';
		if ($this->bWithPreview == false) {
			return $buffer; // weiter nur wenn "mit preview"
		}
		if ($this->aLinkFields == '') {
			die('ERROR: es muss mindestens 1 Feld bei "setLinkFields()" abgefragt werden!');#return;
		}
		if (!empty($sGEToptions)) {
			$sGEToptions = str_replace('?', '&', $sGEToptions);
		}
		// status
		$nStatus = $this->getField($sField);
		switch ($nStatus) {
			case 'preview':	$img = 'btn_edit_draft.gif';	$alt = 'Offline';	break;
			case 'live':	$img = 'btn_edit.gif'; 			$alt = 'Online';	break;
		}
		// image (/link)
		if (isset($img) && file_exists($this->aENV['path']['pix']['unix'].$img)) {
			if (!empty($this->sEditPage)) {
				$buffer .= '<a href="'.$this->sEditPage.'&id='.$this->getField('id').$sGEToptions.'" title="'.$alt.'">';
			}
			$imgsize = getimagesize($this->aENV['path']['pix']['unix'].$img);
			$buffer .= '<img src="'.$this->aENV['path']['pix']['http'].$img.'" '.$imgsize[3].' alt="'.$alt.'" class="btn">';
			if (!empty($this->sEditPage)) {
				$buffer .= '</a>'."\n";
			}
		}
		// output
		return $buffer;
	}

/**
* Wrapper fuer die Funktion "get_prio_buttons()" aus der "func.admin_view.php":
* Erzeugt die Prio-Buttos [Pfeile top/hoch/runter/bottom].
* Optional koennen zusaetzliche GET-Parameter uebergeben werden.
*
* Beispiel:
* <pre><code>
* echo $oView->getPrioButtons(); // params: [$sGEToptions='']
* </code></pre>
*
* @access	public
* @param	string	[$sGEToptions]	Zusaetzliche GET-Parameter	(optional -> default:'')
* @return	html-code
*/
	function getPrioButtons($sGEToptions='') {
		// get vars
		if (!empty($sGEToptions)) {
			$sGEToptions = str_replace('?', '&', $sGEToptions);
		}
		$sId = $this->getField('id');
		$sCat = (!empty($this->sNaviId)) ? $this->getField('navi_id') : '';
		$nCounter = $this->nPos + 1; // counter beginnt bei 1, nicht bei 0!
		// output
		return get_prio_buttons($nCounter, $this->nEntries, $sId, $sCat, $sGEToptions); // params: $nCounter,$nEntries,$sID[,$sCat=''][,$sGEToptions='']
	}

#----------------------------------------------------------------------------- DB-NAVI

/**
* Wrapper fuer die Funktion "get_dbnavi_links()" aus der "func.admin_view.php":
* Baut komplette Statusinformation (Pages + Hits) der DB-Abfrage (formatiert).
*
* Beispiel:
* <pre><code>
* echo $oView->getDbStatus(); // params: -
* </code></pre>
*
* @access	public
* @return	html-code
*/
	function getDbStatus() {
		global $Userdata;
		require_once($this->aENV['path']['global_service']['unix']."class.DbNavi.php");
		$oDBNavi = new DbNavi($Userdata,$this->aENV,$this->nEntries, $this->nStart, $this->nLimit);
		// code
		$sStr = "<b>";
		$sStr .= $oDBNavi->getDbnaviStatusPages();
		$sStr .= "</b> [";
		$sStr .= $oDBNavi->getDbnaviStatusHits();
		$sStr .= "]";
		// output
		return $sStr;
	}

/**
* Wrapper fuer die Funktion "get_dbnavi_links()" aus der "func.admin_view.php":
* Baut Navigationslinks zu DB-Abfrage-Unterteilungsseiten. 
* Die Ausgabe der Seitenzahlen wird ab $nPageLimit abgebrochen und durch Pfeile ersetzt!
*
* Beispiel:
* <pre><code>
* echo $oView->getDbLinks('&navi_id='.$navi_id); // params: [$sGEToptions=''][,$nPageLimit=5]
* </code></pre>
*
* @access	public
* @param 	string	[$sGEToptions]	Zusaetzlich zu uebergebende GET-Parameter (mit fuehrendem '&')	(optional -> default:'')
* @param 	integer	[$nPageLimit]	Anzahl der angezeigten Seiten	(optional -> default:'5')
* @return	html-code
*/
	function getDbLinks($sGEToptions='', $nPageLimit=5) {
		global $Userdata;
		require_once($this->aENV['path']['global_service']['unix']."class.DbNavi.php");
		$oDBNavi = new DbNavi($Userdata,$this->aENV,$this->nEntries, $this->nStart, $this->nLimit);
		if (!empty($sGEToptions)) {
			$sGEToptions = str_replace('?', '&', $sGEToptions);
		}
		return $oDBNavi->getDbnaviLinks($sGEToptions, $nPageLimit); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
	}

#-----------------------------------------------------------------------------
} // END of class
?>