<?php
/*
 * Created on 17.03.2006
 *
 */

/**
 * Diese Klasse kapselt die Datenbankzugriffe im ADM/Projects Bereich. 
 *
 * @access   public
 * @package  module
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @version	1.0 / 2006-03-17
 * 
 * @param object $oDb
 * 
 */
	class AdmProposalEA {
		var $oDb = null;
		var $entries = 0;
		/**
		 * 
		 * Konstruktor
		 * 
		 */
		function AdmProposalEA($oDb) {
			$this->oDb = $oDb;
		}
		
		/**
		 * 
		 * Liefert die Daten, die die Adm Klasse für das Aufbereiten zum Pdf benötigt
		 * @param int $projectid
		 * @param String $orderby
		 * @param String $orderway
		 * @return array
		 * @access public
		 * 
		 */
		function getPDFData($projectid,$orderby,$orderway) {
			
			$table1 = 'adm_proposal';
			$table2 = 'adm_project';
			
			$sql = "SELECT $table2.`id`, $table1.`project_id`, $table1.`job_cat`, $table1.`alt_jobcat`, $table1.`description`, $table1.`hours`, $table1.`quantity`, $table1.`quantity_cat`, $table1.`price_net`, $table1.`flag_extern`, $table2.`handling`,
			$table2.`currency`,$table2.`comment`,$table2.`po_number`,$table2.`projectname`,$table2.`prj_nr`,$table2.`contact_id`, $table2.`discount`, $table2.`prj_date0` as date, $table2.`proposal_introduction` as introduction, $table2.`proposal_resume` as resume
			FROM $table1 RIGHT JOIN $table2 ON $table2.`id`=$table1.`project_id` WHERE $table2.`id` = ".$projectid."";
			
			if(empty($orderway) && empty($orderby)) {
				$sql .= ' ORDER BY `id` ASC';
			}
			else {
				$sql .= ' ORDER BY `'.$orderby.'` '.$orderway;	
			}

			return $this->generateData($sql);
		}
		/**
		 * Diese Methode generiert die Daten für diese EA. Dabei ist der erste Schlüssel numerisch und der Zweite der jeweilige Feldname.
		 * @param String $sql
		 * @return array
		 * @access private
		 */
		function generateData($sql) {
			$aData = $this->oDb->fetch_in_aarray($sql);
			$this->entries = $this->oDb->num_rows();
			$this->oDb->free_result();
			return $aData;	
		}

# Proposal-Admin -------------------------------------------------------------

		/**
		 * Diese Methode loescht einen Datensatz aus der Proposal-(Dataset-)Tabelle (adm_proposal) sowie die entsprechenden Verknuepfungsdaten.
		 * @param	int		$proposal_id
		 * @return	void
		 */
		function deleteProposal($proposal_id) {
			if (!$proposal_id) return false; // TODO: error?
			// delete data
			$aData2delete['id'] = $proposal_id;
			$this->oDb->make_delete($aData2delete, 'adm_proposal');
		}
		
		/**
		 * Diese Methode speichert einen Datensatz in der Proposal-(Stammdatensatz-)Tabelle (adm_project).
		 * @param	array	$aData
		 * @param	int		$proposal_id
		 * @return	void
		 */
		function saveProposal(&$aData) {
			// update data
			$aUpdate['id'] = $aData['id'];
			$this->oDb->make_update($aData, 'adm_project', $aUpdate);
		}
		
		/**
		 * Diese Methode ermittelt, ob es mindestens einen Proposal-Datensatz zu einer Project-ID gibt.
		 * @param	int		$project_id
		 * @return	boolean
		 */
		function hasProposal($project_id) {
			if (!$project_id) return false; // TODO: error?
			// update data
			$this->oDb->query("SELECT `id` FROM `adm_proposal` WHERE `project_id` = '$project_id'");
			return ($this->oDb->num_rows() > 0) ? true : false;
		}

# Proposal-Invoice-Verknuepfungstabelle -------------------------------------------------------------y
		/**
		 * Diese Methode ermittelt nur alle Invoice-IDs, die mit einer Proposal-ID verknuepft sind
		 * @param	int		$proposal_id 
		 * @return	array[key] = invoice_id
		 */
		function getProposalInvoices($proposal_id) {
			$buffer = array();
			if (empty($proposal_id)) return $buffer; // TODO: error?
			
			// select invoices
			$sql = "SELECT `invoice_id` 
					FROM `adm_proposal_invoice` 
					WHERE `proposal_id` = '$proposal_id' 
					ORDER BY `invoice_id` ASC;";
			$this->oDb->query($sql); /*,`created`,`created_by`,`last_modified`,`last_mod_by`*/
			$this->entries = $this->oDb->num_rows();
			// get data
			while ($row = $this->oDb->fetch_array()) {
				if(!empty($row['invoice_id'])) {
					$buffer[$row['invoice_id']] = $row['invoice_id'];
				}
			}
			$this->oDb->free_result();
			
			// output
			return $buffer;
		}

		/**
		 * Diese Methode speichert Datensaetze in der Proposal-Invoice-Verknuepfungstabelle.
		 * Der zweite Parameter kann ein Array mit IDs oder ein String mit einer oder mehreren kommaseparierten IDs sein.
		 * @param	int		$proposal_id 
		 * @param	mixed	$invoice_id [string|array] 
		 * @return	void
		 */
		function saveProposalInvoices($proposal_id, $invoice_id) {
			if (empty($proposal_id)) return false; // TODO: error?
			$aInvoiceId = (!is_array($invoice_id)) ? explode(',', $invoice_id) : $invoice_id;
			if (count($aInvoiceId) == 0) return false; // TODO: error?
			
			// neue verknuepfungen speichern
			$aData = array();
			$aData['proposal_id'] = $proposal_id;
			
			foreach ($aInvoiceId as $invoice_id) {
				$aData['invoice_id'] = $invoice_id;
				$this->oDb->make_insert($aData, 'adm_proposal_invoice');
			}
		}

		/**
		 * Diese Methode loescht Datensaetze aus der Proposal-Invoice-Verknuepfungstabelle.
		 * Wenn der erste Parameter uebergeben wird, werden alle Datensaetze mit der uebergebenen 
		 * Proposal-Id geloescht, sonst alle mit der uebergebenen Invoice-ID (zweiter Parameter).
		 * @param	int		$proposal_id 	[optional] 
		 * @param	int		$invoice_id		[optional] 
		 * @return	void
		 */
		function deleteProposalInvoices($proposal_id=null, $invoice_id=null) {
			if (!$proposal_id && !$invoice_id) return false; // TODO: error?
			
			// verknuepfungen loeschen
			$aDelete = ($proposal_id)
				? array('proposal_id' => $proposal_id)
				: array('invoice_id' => $invoice_id);
			$this->oDb->make_delete($aDelete, 'adm_proposal_invoice');
		}

		function getProposalDatasets($project_id) {
			$sql = "SELECT `id`,`project_id`,`job_cat`,`alt_jobcat`,`description`,`hours`,`quantity`,`quantity_cat`,`price_net`,`flag_extern` 
			FROM `adm_proposal` 
			WHERE `project_id` = ".$project_id."
			ORDER BY `prio` DESC";

			return $this->generateData($sql);
		}

#  --------------------------------------------------------------------------------------------------

### Ab hier sind vorruebergehend ADR EAs... 
### TODO: in eigene EA-Klasse auslagern (bzw. nach Umbau auf ADR v2 anpassen!)
		/**
		 * @deprecated 1.0 - 17.03.2006
		 * 
		 */
		function getAdrContact($rcontact_id) {
			return $this->oDb->fetch_by_id($rcontact_id,'adr_contact');	
		}
		/**
		 * @deprecated 1.0 - 17.03.2006
		 */
		function getAdrCountries() {
			$aCountry = array();
			$this->oDb->query("SELECT code, country, phone_prefix FROM adr_country ORDER BY country");
			while ($tmp = $this->oDb->fetch_array()) {
				$aCountry[$tmp['code']]['name']		= $tmp['country'];
				$aCountry[$tmp['code']]['phone']	= (!empty($tmp['phone_prefix'])) ? '+'.$tmp['phone_prefix'] : '';
			}
			return $aCountry;
		}
		
		
		/**
		 * @deprecated 1.0 - 17.03.2006
		 */
		function getAdrRefId($raddress_id) {
			$sql = "SELECT reference_id 
					FROM adr_address 
					WHERE id='$raddress_id'";	
			return $this->generateData($sql);
		}
		
		/**
		 * @deprecated 1.0 - 17.03.2006
		 */
		function getAdrDetails($clientid) {
			$t1 = 'adr_address';
			$t2 = 'adr_country';
			
			$sql = "SELECT $t1.`po_box`,$t1.`po_box_town`,$t1.`street`,$t1.`building`,$t1.`region`,$t1.`town`,$t2.`country` 
					FROM $t1 INNER JOIN $t2 
					ON $t1.`country`=$t2.`code` 
					WHERE $t1.`reference_id`='".$clientid."'";
			
			return $this->generateData($sql);
		}
	
	}
?>