<?php // Class cms_content_edit
/**
* Diese Klasse vereinfacht das Speichern/Loeschen eines Datensatzes (inkl. Prio-Ermittlung bei "insert", 
* Suche mittels spezieller Such-Tabelle, Zuordnung zu CUGs, Trigern von Funktionen sowie Datenaufbereitung 
* von Daten, die aus bestimmmten Methoden der Admin-Form-Klasse generiert werden (Date-Dropdown, Flash-Texteditor,...)). 
*
* NOTE: benoetigt die Klasse "class.db_mysql.php"!
* NOTE: benoetigt die Klasse "class.Search.php"!
* NOTE: benoetigt die Funktion "write_xml_linkStructure()" (in der Methode"_replace_flasheditor_specials()")!
*
* Example: 
* <pre><code> 
* $oCmsContent =& new cms_content_edit($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]
* // erzeuge neue maxprio bei insert
* $oCmsContent->setPrio(true); // params: [$bPrio=false]
* // ...aber der neue Datensatz soll unten angehaengt werden...
* $oCmsContent->setPrioMode("addOnBottom"); // params: ["addOnTop"|"addOnBottom"]
* // Feld, das die Form-Methode "$oForm->date_dropdown()" benutzt, definieren (default: KEIN solches Feld)
* $oCmsContent->setDateDropdownField('date1'); // params: $sFieldname
* $oCmsContent->setDateDropdownField('date2'); // params: $sFieldname
* // Feld, das die Form-Methode "$oForm->pdf_tools()" benutzt, definieren (default: KEIN solches Feld)
* $oCmsContent->setPdfToolsField('pdf_id_de'); // params: $sFieldname
* $oCmsContent->setPdfToolsField('pdf_id_en'); // params: $sFieldname
* // Feld, das den flash-editor benutzt, definieren (default: KEIN Flasheditor-Feld)
* $oCmsContent->setFlashEditorField('copytext_'.$weblang); // params: $sFieldname
* $oCmsContent->setFlashEditorField('irgendwas'); // params: $sFieldname
* // Inhalte fuer search-table definieren
* $oCmsContent->setSearchLang($weblang); // params: $sLang
* $oCmsContent->setSearchTitle($aData['name']); // params: $sTitle
* $oCmsContent->setSearchText($aData['copytext_'.$weblang]); // params: $sSearchtext
* // custom-funktionen definieren
* $oCmsContent->setFunctionOnDelete("write_xml_client", $aData['navi_id']); // params: $sName[,args]
* $oCmsContent->setFunctionOnSave("write_xml_structure"); // params: $sName[,args]
* $oCmsContent->setFunctionOnSave("write_xml_client", array($navi_id, $aData['navi_id'])); // params: $sName[,args]
* // CUG -> diese Seite kann fuer CUGs geschuetzt werden
* require_once($aENV['path']['global_service']['unix']."class.cug.php"); // Klasse einbinden ...
* $oCug =& new cug($aENV['db'], $syslang); // ... + iitialisieren
* $oCmsContent->setCUG(&$oCug); // params: [$oCug=NULL] // ... + dem Content-Object mitteilen!
* 
* // Insert/Update eines Eintrags (kuemmert sich selbst drum)
* if (isset($btn['save'])) 
*	$aData = $oCmsContent->save($aData); // params: $aData
* // Insert eines neuen Eintrags
* if (isset($btn['save']) && !$aData['id']) 
*	$aData = $oCmsContent->insert($aData); // params: $aData
* // Update eines bestehenden Eintrags
* if (isset($btn['save']) && $aData['id']) 
*	$aData = $oCmsContent->update($aData, $aData['id']); // params: $aData,$id
* // Delete eines bestehenden Eintrags
* if (isset($btn['delete'])) 
*	$oCmsContent->delete($aData['id']); // params: $id
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.33 / 2005-08-26	[BUGFIX: Sicherheits-Abfrage bei "accept()" eingebaut]
*/
class cms_content_edit {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse search:
		----------------------------------------------------------------------------
		konstruktor cms_content_edit($oDb, $sTable, $bWithPreview=false)
		
		function setCUG($oCug=NULL)
		function setPrio($bPrio=false)
		function setPrioMode(["addOnTop"|"addOnBottom"])
		function setDateDropdownField($sFieldname)
		function setFlashEditorField($sFieldname)
		function setPdfToolsField($sFieldname)
		function setSearchLang($sLang)
		function setSearchTitle($sTitle)
		function setSearchText($sSearchtext)
		function setFunctionOnDelete($sName, $args=NULL)
		function setFunctionOnSave($sName, $args=NULL)
		
		function selectStandard($navi_id, $id=NULL)
		function select($id)
		function getModeKey()
		function getStatus()
		
		function save($aData)
		function save_close($aData, $sViewerPage) 
		function insert($aData)
		function update($aData, $id)
		function accept($aData)
		function restore($aData)
		function delete($id)
		
		function _reload($sNewNaviId, $sNewId)
		function _get_new_prio()
		function _save_searchdata($id)
		#function _insert_search($id)
		#function _update_search($aData)
		#function _is_in_searchtable($id)
		#function _get_searchdata($id)
		function _replace_flasheditor_specials(&$aData)
		function _write_flasheditor_xmlstructure()
		function _prepare_pdf_tools_data(&$aData)
		function _prepare_date_dropdown_data(&$aData)
		----------------------------------------------------------------------------
		HISTORY:
		1.33 / 2005-08-26	[BUGFIX: Sicherheits-Abfrage bei "accept()" eingebaut]
		1.32 / 2005-08-18	[BUGFIX: creator-data ermitteln bei "update()" (Preview-Insert) eingebaut]
		1.31 / 2005-08-17	[NEU: timestamp ans ende von "_get_searchdata()" eingebaut]
		1.3 / 2005-07-13	[NEU: "save_close()" + "_reload()" (bei "save,accept,restore" eingebaut)]
		1.25 / 2005-06-27	[BUGFIX: abfrage bei "restore()" eingebaut]
		1.24 / 2005-06-15	[BUGFIX: "accept()" umgebaut]
		1.23 / 2005-06-15	[BUGFIX: data neu auslesen bei "accept()"]
		1.22 / 2005-06-08	[BUGFIX: created-handling bei "insert()"]
		1.21 / 2005-06-07	[BUGFIX: prio-handling bei "insert()" + NEU: "_get_new_prio()"]
		1.2 / 2005-05-18	[NEU: "selectStandard()"]
		1.1 / 2005-05-17	[NEU: "$sTable" in den Konstruktor genommen und "$table" + "make_" aus den Methoden entfernt]
		1.0 / 2005-05-13	[NEU: umgebaut "class.cms_content_v1.74.php" (plus Preview)]
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	object	DB-Objekt (muss beim initialisieren der Klasse uebergeben werden)
*/
	var $oDb;
/**
* @access   private
* @var	 	string	Name der Content Datenbank-Tabelle
*/
	var $sTable;
/**
* @access   private
* @var	 	boolean	true (mit Vorschau) oder false (wird beim initialisieren der Klasse uebergeben)
*/
	var $bWithPreview;
/**
* @access   private
* @var	 	boolean	true (Reload der Seite nach Save-Aktionen) oder false (wird beim initialisieren der Klasse uebergeben)
*/
	var $bReloadAfterSave;
/**
* @access   private
* @var	 	array	globales Environment-Array
*/
	var $aENV;
/**
* @access   public
* @var	 	string	Select: Bearbeitungs-Modus [new|edit]
*/
	var $sModeKey;
/**
* @access   public
* @var	 	string	Select: Bearbeitungs-Status [live|preview]
*/
	var $sStatus;
/**
* @access   public
* @var	 	string	Suche: Search-Tabelle
*/
	var $sSearchTable;
/**
* @access   public
* @var	 	string	Suche: Sprachversion des search-datensatzes (optional)
*/
	var $sSearchLang;
/**
* @access   public
* @var	 	string	Suche: der Teil des Textes, der in der Suchausgabe als Titel angezeigt wird
*/
	var $sSearchTitle;
/**
* @access   public
* @var	 	string	Suche: der Text, der in die search-table in das feld searchtext geschrieben wird
*/
	var $sSearchText;
/**
* @access   public
* @var	 	string	Suche: Trenner, um den Titel vom Rest des Suchtextes abzutrennen
*/
	var $sSearchTrenner;
/**
* @access   public
* @var	 	int		ID des aktuellen Users aus der Session (wird beim initialisieren der Klasse importiert)
*/
	var $nUserId;
/**
* @access   private
* @var	 	array	Name(n) der Funktion(en) die onDelete ausgefuehrt werden soll(en)
*/
	var $onDeleteFunction;
/**
* @access   private
* @var	 	array	Parameter der Funktion(en) die onDelete ausgefuehrt werden soll(en)
*/
	var $onDeleteArgs;
/**
* @access   private
* @var	 	array	Name(n) der Funktion(en) die onSave ausgefuehrt werden soll(en)
*/
	var $onSaveFunction;
/**
* @access   private
* @var	 	array	Parameter der Funktion(en) die onSave ausgefuehrt werden soll(en)
*/
	var $onSaveArgs;
/**
* @access   private
* @var	 	array	Name(n) der Funktion(en) die onAccept ausgefuehrt werden soll(en)
*/
	var $onAcceptFunction;
/**
* @access   private
* @var	 	array	Parameter der Funktion(en) die onAccept ausgefuehrt werden soll(en)
*/
	var $onAccepteArgs;
/**
* @access   private
* @var	 	object	CUG-Objekt - default = NULL: Keine Verknuepfungen zu CUG(s)
*/
	var $oCug;
/**
* @access   private
* @var	 	boolean	Flag ob beim Insert die naechsthoehere Prio zum Stammdatensatz hinzugefuegt werden soll
*/
	var $bPrio;
/**
* @access   private
* @var	 	string	Flag ob beim Insert der neue Datensatz OBEN "addOnTop" (default) oder UNTEN "addOnBottom" angehaengt werden soll
*/
	var $sPrioMode;
/**
* @access   private
* @var	 	array	Fieldnames, die den Flash-Texteditor benutzen
*/
	var $aFlashFieldname;
/**
* @access   private
* @var	 	array	Zeichen, die aus dem Flash-Texteditor rauskommen und vor dem speichern in der DB ersetzt werden muessen
*/
	var $aFlashSearch;
/**
* @access   private
* @see      $aFlashSearch
* @var	 	array	Zeichen, die eingesetzt werden
*/
	var $aFlashReplace;
/**
* @access   private
* @var	 	array	Fieldnames, die die Form-Methode "$oForm->pdf_tools()" benutzen
*/
	var $aPdfFieldname;
/**
* @access   private
* @var	 	array	Fieldnames, die die Form-Methode "$oForm->mdb_tools()" benutzen
*/
	var $aMdbFieldname;	
/**
* @access   private
* @var	 	array	Fieldnames, die die Form-Methode "$oForm->date_dropdown()" benutzen
*/
	var $aDateDDFieldname;
/**
* @access	private
* @var		string	Slideshow Identifikation
*/
	var $sSlideshowId;
/**
* @access	private
* @var		object	Search-Object
*/
	var $oSearch;
#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das Objekt und bekommt ein DB-Objekt uebergeben
*
* Beispiel: 
* <pre><code> 
* $oCmsContent =& new cms_content_edit($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]
* </code></pre>
*
* @access   public
* @global	array	$aENV
* @param	object	$oDb	Datenbank-Objekt, welches fuer die Klasse "class.db_mysql.php" benoetigt wird
* @return   void
*/
	function cms_content_edit(&$oDb, $sTable, $bWithPreview=false) {
		// DB-Objekt importieren
		if (!is_object($oDb)) {
			echo "<script>alert('".basename(__FILE__)."-ERROR: \\n -> Es wurde kein DB-Objekt uebergeben!')</script>";
			return;
		}
		$this->oDb				= $oDb;
		// content table
		$this->sTable			= $sTable;
		// mit preview?
		$this->bWithPreview		= $bWithPreview;
		// nach save aktionen reloaden?
		$this->bReloadAfterSave	= true;
		// globales ENV-Array importieren
		global $aENV;
		if (!is_array($aENV)) die("ERROR: NO aENV!");
		$this->aENV			= $aENV;
		// ID (Userdata)
		global $Userdata;
		$this->nUserId			= $Userdata['id'];
		// Flash-Texteditor
		$this->aFlashFieldname	= array(); // default: KEIN Flash-Texteditor
		$this->aFlashSearch		= array('&lt;', '&gt;', '&amp;', '&quot;', '&apos;');
		$apos = (!get_magic_quotes_gpc()) ? "'" : "\'";
		$this->aFlashReplace	= array('<', '>', '&', '"', $apos);
		// CUG
		$this->oCug				= NULL;	// default: KEINE CUGs beruecksichtigen
		// Prio
		$this->bPrio			= false;		// default: KEINE Prio hinzufuegen
		$this->sPrioMode		= "addOnTop";	// default: neuen Datensatz oben anhaengen
		// custom functions init
		$this->onDeleteFunction	= array();
		$this->onDeleteArgs		= array();
		$this->onSaveFunction	= array();
		$this->onSaveArgs		= array();
		$this->onAcceptFunction	= array();
		$this->onAcceptArgs		= array();
		// Pdf-Tools
		$this->aPdfFieldname	= array();	// default: KEIN Formfield Pdf-Tools
		// Date-Dropdown
		$this->aDateDDFieldname	= array();	// default: KEIN Formfield Date-Dropdown
		
		// SEARCH NEW
		include_once($aENV['path']['global_service']['unix'].'class.Search.php');
		$this->oSearch =& new Search($oDb, $aENV, 'cms', $Userdata['id']);
		### SEARCH OLD
		###if (empty($aENV['table']['cms_search'])) die("ERROR: NO search-table in aENV!");
		###$this->sSearchTable	= $aENV['table']['cms_search'];
		###$this->sSearchTrenner	= ' || ';
	}

#----------------------------------------------------------------------------- SET

/**
* ADMIN: Setzt Fieldnames, die ein Date-Dropdown benutzen
*
* @access   public
* @param	string	sFieldname		Name des Feldes, das die Form-Methode "$oForm->date_dropdown()" benutzt
* @return	void
*/
	function setDateDropdownField($sFieldname) {
		$this->aDateDDFieldname[]	= $sFieldname;
	}

/**
* ADMIN: Setzt Fieldnames, die den Flash-Texteditor benutzen
*
* @access   public
* @param	string	sFieldname		Name des Feldes, das Form-Methode "$oForm->textarea_flash()" (Flash-Texteditor) benutzt
* @return	void
*/
	function setFlashEditorField($sFieldname) {
		$this->aFlashFieldname[]	= $sFieldname;
	}

/**
* ADMIN: Setzt Fieldnames, die das Multiple-Pdf-Modul benutzen
*
* @access   public
* @param	string	sFieldname		Name des Feldes, das die Form-Methode "$oForm->pdf_tools()" benutzt
* @return	void
*/
	function setPdfToolsField($sFieldname) {
		$this->aPdfFieldname[]	= $sFieldname;
	}
	
/**
* ADMIN: Setzt Fieldnames, die das Multiple-Pdf-Modul benutzen
*
* @access   public
* @param	string	sFieldname		Name des Feldes, das die Form-Methode "$oForm->pdf_tools()" benutzt
* @return	void
*/
	function setMdbToolsField($sFieldname) {
		$this->aMdbFieldname[]	= $sFieldname;
	}	

/**
*ADMIN:  Setzt das CUG-Objekt und damit den Flag ob Verknuepfungen zu CUGs geprueft/gespeichert/geloescht werden sollen
*
* @access   public
* @param	object	oCug		Cug-Objekt (default: NULL)
* @return	void
*/
	function setCUG($oCug=NULL) {
		$this->oCug			= $oCug;
	}

/**
* ADMIN: Setzt den Flag ob eine Prio zu einem neuen Datensatz hinzugefuegt werden soll
*
* @access   public
* @param	boolean	bPrio		bei "true" wird bei insert eine neue Prio errechnet und in die DB geschrieben (default: false)
* @return	void
*/
	function setPrio($bPrio=false) {
		$this->bPrio		= $bPrio;
	}

/**
* ADMIN: Setzt den Flag ob beim Insert der neue Datensatz OBEN "addOnTop" (default) oder UNTEN "addOnBottom" angehaengt werden soll
*
* @access   public
* @param	string	sPrioMode	bei "addOnBottom" wird bei insert der neue Datensatz UNTEN angehaengt (default: "addOnTop")
* @return	void
*/
	function setPrioMode($sPrioMode="addOnTop") {
		if ($sPrioMode != "addOnTop" && $sPrioMode != "addOnBottom")
			die("setPrioMode(): ungueltiger Parameter: '".$sPrioMode."'");
		$this->sPrioMode	= $sPrioMode;
	}

/**
* ADMIN: Suche: Setzt als Bedingung ein Sprachkuerzel (wird in das Feld "lang" eingetragen bzw. abgefragt)
* Gibt es fuer einen Datensatz zwei (oder mehr) Sprachversionen, muss die jeweilige aktuelle Sprache dieser Methode uebergeben werden.
* In der Search-Tabelle wird dann fuer jede Sprachversion ein eigener Datensatz angelegt (und entsprechend in der Ausgabe abgefragt bzw. durchsucht).
*
* Beispiel:
* <pre><code>
* $oCmsContent->setSearchLang($weblang); // params: $sLang
* </code></pre>
*
* @access   public
* @param	string	$sLang			optionale Kennung der Sprachversion des Suchdatensatzes
* @return	void
*/
	function setSearchLang($sLang) {
		$this->sSearchLang	= trim($sLang);
	}

/**
* ADMIN: Suche: Setzt den Teil des Textes, der in der Suchausgabe als Titel angezeigt wird und dem searchstring (durch $sSearchTrenner getrennt) vorangestellt wird
*
* @access   public
* @param	string	$sTitle			der Teil des Textes, der in der Suchausgabe als Titel angezeigt wird
* @return	void
*/
	function setSearchTitle($sTitle) {
		$this->sSearchTitle	= trim($sTitle);
	}

/**
* ADMIN: Suche: Setzt den searchstring (ohne Titel! -> Den Teil des Suchtextes der bei Suchtreffern als Titel angezeigt werden soll der Methode "setSearchText()" uebergeben.)
*
* @access   public
* @param	string	sSearchText		der Text, der in die search-table in das feld searchtext geschrieben wird
* @return	void
*/
	function setSearchText($sSearchText) {
		$this->sSearchText	= trim($sSearchText);
	}

/**
* ADMIN: Setzt eine Funktion (optional mit einem Parameter), die bei "delete()" (nach den DB-queries!) ausgefuehrt wird.
*
* @access   public
* @param	string	sName		Name der Funktion
* @param	mixed	args		optionales Argument (string|array|object)
* @return	void
*/
	function setFunctionOnDelete($sName, $args=NULL) {
		$this->onDeleteFunction[]		= trim($sName);
		if (!is_null($args)) {
			$this->onDeleteArgs[$sName]	= $args;
		}
	}

/**
* ADMIN: Setzt eine Funktion (optional mit einem Parameter), die bei [save|insert|update] (nach den DB-queries!) ausgefuehrt wird.
*
* @access   public
* @param	string	sName		Name der Funktion
* @param	mixed	args		optionaler Argument (string|array|object)
* @return	void
*/
	function setFunctionOnSave($sName, $args=NULL) {
		$this->onSaveFunction[]			= trim($sName);
		if (!is_null($args)) {
			$this->onSaveArgs[$sName]	= $args;
		}
	}

/**
* ADMIN: Setzt eine Funktion (optional mit einem Parameter), die bei [accept] (nach den DB-queries!) ausgefuehrt wird.
*
* @access   public
* @param	string	sName		Name der Funktion
* @param	mixed	args		optionaler Argument (string|array|object)
* @return	void
*/
	function setFunctionOnAccept($sName, $args=NULL) {
		$this->onAcceptFunction[]		= trim($sName);
		if (!is_null($args)) {
			$this->onAcceptArgs[$sName]	= $args;
		}
	}

/**
* ADMIN: Setzt SlideshowID, die bei [accept][delete][discard] (nach den DB-queries!) ausgefuehrt wird.
*
* @access   public
* @param	string	sSlideId	SlideshowIdentifikation
* @return	void
*/
	function setSlideshowId($sid) {
		$this->sSlideshowId = $sid;
	}

#------------------------------------------------------------------------------- SELECT

/**
* ADMIN: Ermittelt den gesuchten Inhalt in der Stammdatensatz-Tabelle.
* NOTE: Muss bei Standard-Seiten ANSTATT der Methode "select()" aufgerufen werden!
*
* Beispiel:
* <pre><code>
* $aData = $oCmsContent->selectStandard($navi_id, $aData['id']); // params: $navi_id,$id=NULL
* </code></pre>
*
* @access   public
* @param	string	$navi_id		NaviId des gesuchten Datensatzes
* @param	string	$id				ID des gesuchten Datensatzes (optional)
* @return	string	ID der aktuellen Standard-Seite
*/
	function selectStandard($navi_id, $id=NULL) {
		if (empty($navi_id)) return false;
		if ($this->bWithPreview == true) {
			// mit preview
			if (is_null($id) || empty($id)) { // ggf. id ermitteln
				$this->oDb->query("SELECT `id` FROM `".$this->sTable."` WHERE `navi_id`='".$navi_id."'");
				$id = ($this->oDb->num_rows() > 0) ? $this->oDb->fetch_field('id') : 0;
			}
			return $this->select($id);
		} else {
			// ohne preview
			$this->sModeKey = 'edit'; // set default
			$this->oDb->query("SELECT * FROM `".$this->sTable."` WHERE `navi_id`='".$navi_id."'");
			
			return $this->oDb->fetch_array();
		}
	}

/**
* ADMIN: Ermittelt den gesuchten Inhalt in der Stammdatensatz-Tabelle.
* NOTE: Wenn keine ID uebergeben wird, wird ein leeres Array zurueckgegeben und der mode_key auf "new" gesetzt.
* (mit preview:) Wird ein Preview-Datensatz gefunden, wird dieser zurueckgegeben, sonst der aktuelle Live-Datensatz.
*
* Beispiel:
* <pre><code>
* $aData = $oCmsContent->select($id); // params: $id
* </code></pre>
*
* @access   public
* @param	string	$id				ID des gesuchten Datensatzes
* @return	array	leeres oder mit Daten gefuelltes aData-array
*/
	function select($id) {
		$buffer = array();
		#$this->oDb->debug=true;
		$this->sModeKey = 'new'; // set default
		if (empty($id)) return $buffer;
		// wenn "mit preview"
		if ($this->bWithPreview == true) {
			// 1. gibt es einen preview-datensatz?
			$this->oDb->query("SELECT * FROM `".$this->sTable."` WHERE `preview_ref_id`='".$id."'");
			if ($this->oDb->num_rows() > 0) { // ja
				$buffer = $this->oDb->fetch_array();
				$this->sStatus = 'preview';
			} else { // wenn nein, hole live-datensatz
				$buffer = $this->oDb->fetch_by_id($id, $this->sTable);
				// oder bin ich doch schon ein preview-datensatz?
				$this->sStatus = (isset($buffer['preview_ref_id']) && $buffer['preview_ref_id'] > 0) ? 'preview' : 'live';
			}
		} else {
			// sonst immer live-datensatz
			$buffer = $this->oDb->fetch_by_id($id, $this->sTable);
			#$this->sStatus = 'live';
		}
		$this->sModeKey = 'edit'; // do not change value!
		return $buffer;
	}

/**
* ADMIN: Ermittelt den Bearbeitungs-Modus der aktuellen Seite ("new"/"edit").
* NOTE: Kann nur NACH der Methode "select()" aufgerufen werden!
*
* @access   public
* @return	string	Modus (Key) der aktuellen Seite (new|edit)
*/
	function getModeKey() {
		return $this->sModeKey;
	}

/**
* ADMIN: Ermittelt den Bearbeitungs-Status der aktuellen Seite ("live"/"preview").
* NOTE: Kann nur NACH der Methode "select()" aufgerufen werden!
*
* @access   public
* @return	string	Status (Key) der aktuellen Seite (live|preview)
*/
	function getStatus() {
		return $this->sStatus;
	}

#------------------------------------------------------------------------------- SAVE/INSERT/UPDATE

	/**
	 * 
	 * Diese Methode kopiert einen Datensatz aus der ausgew�hlten Sprachversion in die aktuelle Sprachversion.
	 * @access public
	 * @param int $navi_id
	 * @param array $aData
	 * @return $aData
	 */
	function copyTo($navi_id,$aData) {
		$aData2	= $this->selectStandard($navi_id, $aData['id']); // params: $navi_id,$id=NULL
		
		foreach($aData as $field => $value) {
			if(strstr($field,'copytext') || strstr($field,'title_')) {
				$aCopyFields[] = explode('_',$field);
			}
			
		}
		for($i=0;!empty($aCopyFields[$i]);$i++) {
			$oname = implode('_',$aCopyFields[$i]);
			$aData[$oname] = addslashes($aData2[$aCopyFields[$i][0].'_'.$aData['copyfromlang']]);
		}
		return $aData;
	}

/**
* ADMIN: Traegt die gewuenschten Inhalte in die Stammdatensatz- sowie die Search-Tabelle ein (kuemmert sich selbst um jeweiliges insert/update...).
* NOTE: Die insert/update Unterscheidung im Stammdatensatz wird am Vorhanden-sein von $aData['id'] festgemacht!
* Die insert/update Unterscheidung im search-Datensatz wird mittels einer SQL-Query ermittelt.
*
* Beispiel:
* <pre><code>
* $aData = $oCmsContent->save($aData); // params: $aData
* </code></pre>
*
* @access   public
* @param	array	$aData			Daten, die upgedated werden
* @return	array	von den jeweiligen Methoden modifiziertes aData-array
*/
	function save($aData) {
		// check stammdatensatz auf id
		if (isset($aData['id']) && $aData['id'] != 0 && !empty($aData['id'])) {
			// update
			$ret = $this->update($aData, $aData['id']);
		} else {
			// insert
			$ret = $this->insert($aData);
		}
		
		$this->writeSitemaps();

		if ($this->bReloadAfterSave	== true) {
			// reload
			$this->_reload($ret['navi_id'], $ret['id']);
		} else {
			// output
			return $ret;
		}
	}
	
	function writeSitemaps() {
		global $aENV, $oDate;
		
		$oFile	= new filesystem();
		
		$aLanguageFlag	= $this->oDb->fetch_by_id(1, 'cms_meta');
		$aSitemaps	= array();
		
		// foreach aENV contentlanguage
		foreach ($this->aENV['content_language'] as $sKey => $sValue) {
			// create file and write sitemap xml
			$sFilename	= "google_sitemap_".$sKey.".xml";
			
			if ($sKey == "default") { continue; } 
			if ($aLanguageFlag['flag_online_'.$sKey] == 0) { if(file_exists($this->aENV['path']['xml']['unix'].$sFilename)) { unlink($this->aENV['path']['xml']['unix'].$sFilename); } continue; } 

			$sXml	=  '<?xml version="1.0" encoding="UTF-8"?>'."\n";
			$sXml	.= '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">'."\n";
			
			if($this->aENV['cug']['writeNotInSitemap']) { $aCugRel	= $this->oDb->fetch_in_flatarray('SELECT rel_id FROM cug_usergroup_rel WHERE rel_table="cms_navi"'); }

			$sql	= "SELECT * FROM `".$this->aENV['table']['cms_navi']."` WHERE flag_online_".$sKey." = '1' ORDER BY parent_id ASC , prio DESC";
			$this->oDb->query($sql);
			
			$aResult	= array();
			while($aData = $this->oDb->fetch_array()) {
				if($this->aENV['cug']['writeNotInSitemap']
				   && in_array($aData['id'], $aCugRel)) { continue; }				
				if ($aData['template'] != 'no_page') {
					$aResult[]	= $aData;
				}
			}
	
			foreach($aResult as $aData) {
				$sUrl	= 'http://'.$aENV['web_url'].'/'.$aData['filename_'.$sKey];
				
				if(strlen($aData['last_modified'])==14) $aData['last_modified']	= $oDate->get_isodatetime($oDate->convert_timestamp($aData['last_modified']));
				
				$sXml	.= "<url>\n";
				$sXml	.= "	<loc>$sUrl</loc>\n";
				$sXml	.= "	<lastmod>".str_replace(" ", "T", $aData['last_modified'])."+01:00</lastmod>\n";
				$sXml	.= "</url>\n";

				if(!empty($this->aENV['int_linkman'][$aData['template']])) {
					$sql	= "SELECT * FROM `".$this->aENV['int_linkman'][$aData['template']]['table']."` WHERE flag_online_".$sKey." = '1' ";
					$sql	.= (!empty($this->aENV['int_linkman'][$aData['template']]['orderby'])) ? "ORDER BY ".$this->aENV['int_linkman'][$aData['template']]['orderby']:"";
					$this->oDb->query($sql);

					while($aData2 = $this->oDb->fetch_array()) {
						if(strlen($aData2['last_modified'])==14) $aData2['last_modified']	= $oDate->get_isodatetime($oDate->convert_timestamp($aData2['last_modified']));
						$sXml	.= "<url>\n";
						$sXml	.= "	<loc>$sUrl?id=".$aData2['id']."</loc>\n";
						$sXml	.= "	<lastmod>".str_replace(" ", "T", $aData2['last_modified'])."+01:00</lastmod>\n";
						$sXml	.= "</url>\n";						
					}					
				}
			}
			
			$sXml .= "</urlset>\n";

			$oFile->write_str_in_file($this->aENV['path']['xml']['unix'].$sFilename, $sXml);
			$aSitemaps[]	= 'http://'.$aENV['web_url'].'/data/cms/xml/'.$sFilename;
		}// END foreach aENV contentlanguage

		$sXml	= '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		$sXml	.= '<sitemapindex xmlns="http://www.google.com/schemas/sitemap/0.84">'."\n";
	
		foreach($aSitemaps as $nKey => $sFilename) {
			$sXml	.= "<sitemap>\n";
		  	$sXml	.= "	<loc>".$sFilename."</loc>\n";
			$sXml	.= "	<lastmod>".str_replace(" ", "T", $oDate->get_isodatetime(time()))."+01:00</lastmod>\n";
			$sXml	.= "</sitemap>\n";
		}
		
		$sXml .= "</sitemapindex>\n";
		
		// write Sitemap Index
		$oFile->write_str_in_file($this->aENV['path']['root']['unix'].$this->aENV['sitemap']['index']['name'], $sXml);
	}	

/**
* ADMIN: Identisch zu "save()", ausser dass nach erfolgter Speicherung auf die Overview-Seite weitergeleitet wird.
*
* Beispiel:
* <pre><code>
* $aData = $oCmsContent->save_close($aData, $sViewerPage); // params: $aData, $sViewerPage
* </code></pre>
*
* @access   public
* @param	array	$aData			Daten, die upgedated werden
* @param	string	$sViewerPage	URL der Seite zu der weitergeleitet werden soll
* @return	array	von den jeweiligen Methoden modifiziertes aData-array
*/
	function save_close($aData, $sViewerPage) {
		// check var
		if (empty($sViewerPage)) die('ERROR in cms_content_edit::save_close(): EMPTY $sViewerPage!');
		// check stammdatensatz auf id
		if (isset($aData['id']) && $aData['id'] != 0 && !empty($aData['id'])) {
			// update
			$this->update($aData, $aData['id']);
		} else {
			// insert
			$this->insert($aData);
		}
		
		$this->writeSitemaps();
		
		// weiterleitung
		header('Location: '.$sViewerPage); exit;
	}

/**
* ADMIN: Inserted einen Eintrag in die Stammdatensatz-Tabelle sowie den entsprechenden in die Search-Tabelle.
* NOTE: Defaultmaessig wird nachdem alle Aktionen dieser Methode abgeschlossen sind, die Seite neu geladen. 
* Ist das nicht gewuenscht, muss die Klassenvariable "bReloadAfterSave" auf "false" gesetzt werden, bevor 
* diese Methode aufgerufen wird. In diesem Fall wuerde das uebergebene Daten-Array um die Werte
* 'created' + 'created_by' + 'id' ("mysql_insert_id()") erweitert und zurueckgegeben.
*
* Beispiel:
* <pre><code>
* $aData = $oCmsContent->insert($aData); // params: $aData
* </code></pre>
*
* @access   public
* @param	array	$aData			Daten, die upgedated werden
* @return	array	Wenn KEIN reload: Um 'created' + 'created_by' + 'id' ("mysql_insert_id()") erweitertes Daten-Array "$aData"
*/
	function insert($aData) {
	// weblang wird nur in update genutzt. @todo: parameteruebergabe
	unset($aData['weblang']);
		// ggf. Sonderzeichen- und TAG-Behandlung des Flash-Editors mittels Ersetzungen ausgleichen
		$this->_replace_flasheditor_specials($aData);
		
		// ggf. Daten aufbereiten, die von der Form-Methode "$oForm->pdf_tools()" kommen
		$this->_prepare_pdf_tools_data($aData);

		// ggf. Daten aufbereiten, die von der Form-Methode "$oForm->mdb_tools()" kommen
		$this->_prepare_mdb_tools_data($aData);
				
		// ggf. Daten aufbereiten, die von der Form-Methode "$oForm->date_dropdown()" kommen
		$this->_prepare_date_dropdown_data($aData);
		
		// CUG: CUG-ID(s) zwischenspeichern
		if (is_object($this->oCug) && isset($aData['cug'])) {
			$temp_cug = $aData['cug'];
			unset($aData['cug']); // loesche dieses feld (da es in der content-table nicht existiert!)
		}
		
		// ggf. find highest prio
		$newPrio =  $this->_get_new_prio();
		
		// "mit preview": insert (Live-Datensatz)
		if ($this->bWithPreview == true) {
			$aInsert = (isset($aData['navi_id']) && $aData['navi_id'] > 0) 
				? array('navi_id' => $aData['navi_id']) 
				: array();
			if ($this->bPrio == true) {
				$aInsert['prio'] = $newPrio;
			}
			$aInsert['created'] = date("YmdHis");
			$aInsert['created_by'] = $this->nUserId;
			$this->oDb->make_insert($aInsert, $this->sTable);
			$aData['preview_ref_id'] = $this->oDb->insert_id();
		}
		
		// insert ("mit preview": Preview-Stammdatensatz; "ohne" ist dies der Live-Datensatz)
		$aData['created'] = date("YmdHis");
		$aData['created_by'] = $this->nUserId;
		// ggf. find highest prio
		if ($this->bPrio == true) {
			$aData['prio'] = $newPrio;
		}
		$this->oDb->make_insert($aData, $this->sTable);
		$aData['id'] = mysql_insert_id();
		
		// CUG: Usergroups in Verknuepfungstabelle speichern
		if (is_object($this->oCug) && isset($temp_cug)) {
			$this->oCug->assignUsergroup($aData['id'], $this->sTable, $temp_cug); // params: $sRelId, $sRelTable, $usergroup
		}
		
		// "mit preview" das folgende -> ERST BEI ACCEPT!!!:
		if ($this->bWithPreview == false) {
			// insert (search)
			#$this->_insert_search($aData['id']); // OLD
			$this->_save_searchdata($aData['id']); // NEW
		
			// ggf. update xml-struktur fuer int.-linkmanager des flash-editors
			$this->_write_flasheditor_xmlstructure($this->aENV['linkman']['multilang']);
		}
		
		// ggf. fuehre eine oder mehrere Funktionen aus
		$this->_call_func_on_save();
		
		// output
		return $aData;
	}

/**
* Private Funktion, die ggf. die neachste prio-nummer ermittelt.
*
* @access   private
* @return	integer
*/
	function _get_new_prio() {
		$newPrio = 0;
		if ($this->bPrio == true) {
			if ($this->sPrioMode == "addOnBottom") {
				$this->oDb->query("SELECT MIN(`prio`) FROM `".$this->sTable."`");
				$newPrio = $this->oDb->fetch_row();
				$newPrio = ($newPrio[0]-1);
			} else { // default: "addOnTop"
				$this->oDb->query("SELECT MAX(`prio`) FROM `".$this->sTable."`");
				$newPrio = $this->oDb->fetch_row();
				$newPrio = ($newPrio[0]+1);
			}
		}
		return $newPrio;
	}

/**
* ADMIN: Updated einen Eintrag aus der eigenen (Stammdatensatz-Tabelle) sowie den entsprechenden aus der Search-Tabelle.
* Wie bei der DB-Klasse auch kann $aUpdate ein array oder die id als string sein.
* Ob in der Search-Tabelle ein insert oder ein update erfolgt, wird durch eine query ermittelt.
*
* Beispiel:
* <pre><code>
* $aData = $oCmsContent->update($aData, $aData['id']); // params: $aData,$aUpdate
* </code></pre>
*
* @access   public
* @param	array	$aData			Daten, die upgedated werden
* @param	mixed (array|string)	Felder des where-constraint als assioziatives Array oder Datensatz-ID als String (MUSS "id" i.d.Tabelle sein!)
* @return	array	Um 'last_mod_by' erweitertes Daten-Array $aData
*/
	function update($aData, $aUpdate) {
		$sWeblang = $aData['weblang'];
		unset($aData['weblang']);
		// ggf. Sonderzeichen- und TAG-Behandlung des Flash-Editors mittels Ersetzungen ausgleichen
		$this->_replace_flasheditor_specials($aData);
		
		// ggf. Daten aufbereiten, die von der Form-Methode "$oForm->pdf_tools()" kommen
		$this->_prepare_pdf_tools_data($aData);

		// ggf. Daten aufbereiten, die von der Form-Methode "$oForm->pdf_tools()" kommen
		$this->_prepare_mdb_tools_data($aData);
				
		// ggf. Daten aufbereiten, die von der Form-Methode "$oForm->date_dropdown()" kommen
		$this->_prepare_date_dropdown_data($aData);
		
		// CUG: Usergroups in Verknuepfungstabelle speichern
		if (is_object($this->oCug)) {
			// 1.: loesche ggf. alle bisherigen Verknuepfungen mit dem Datensatz zu Usergroups
			$this->oCug->removeAssignments($aData['id'], $this->sTable); // params: $sRelId, $sRelTable
			// 2.: neue Usergroups in Verknuepfungstabelle speichern
			$this->oCug->assignUsergroup($aData['id'], $this->sTable, $aData['cug']); // params: $sRelId, $sRelTable, $usergroup
			unset($aData['cug']); // loesche dieses feld (da es in der content-table nicht existiert!)
		}
		
		$aData['last_mod_by'] = $this->nUserId;
		
		// wenn "mit preview"
		if ($this->bWithPreview == true) {
			if (isset($aData['preview_ref_id']) && $aData['preview_ref_id'] > 0) {
				
				$tmp = $this->oDb->fetch_by_id($aData['preview_ref_id'],$this->sTable);
				if(!empty($tmp['prio'])) {
				$aData['prio']			= $tmp['prio'];
				}
				$aData['created']		= $tmp['created']; #date("YmdHis");
				$aData['created_by']	= $tmp['created_by']; #$this->nUserId;
				unset($tmp);
				// update (Preview-Stammdatensatz)
				$this->oDb->make_update($aData, $this->sTable, $aUpdate);
			} else {
				
				if(!empty($sWeblang)) {
				// Fuege die Mehrsprachige Felder hinzu!
					foreach($aData as $Fieldname => $value) {
					
						if(strstr($Fieldname,'_'.$sWeblang)) {
							
							$aReqField[] = str_replace('_'.$sWeblang,'',$Fieldname);
							
						}
						
					}
					reset($aData);
					foreach($this->aENV['content_language'] as $key => $val) {
					
						if($key != 'default' && $key != $sWeblang) {
					
							for($i=0;!empty($aReqField[$i]); $i++) {
							
								$aReqFields[] = $aReqField[$i].'_'.$key;
								
							}
							
						}
						
					}
					if(is_array($aReqFields)) {
					
						for($i=0;!empty($aReqFields[$i]);$i++) {
						
							$sFields .= "`".$aReqFields[$i].'`,';
							
						}
						
					}
				}
				$sFields .= "`created`, `created_by`";
				
				// get original creator-data
				$this->oDb->query("SELECT $sFields FROM `".$this->sTable."` WHERE `id`='".$aData['id']."'");
				$tmp = $this->oDb->fetch_array();
				
				$aData['created']		= $tmp['created']; #date("YmdHis");
				$aData['created_by']	= $tmp['created_by']; #$this->nUserId;
				if(is_array($aReqFields)) {
				
					for($i=0;!empty($aReqFields[$i]);$i++) {
					
						$aData[$aReqFields[$i]] = addslashes($tmp[$aReqFields[$i]]);
						
					}					
				}
			
				// insert (Preview-Stammdatensatz)
				$aData['preview_ref_id'] = $aData['id'];
				unset($aData['id']);
				$this->oDb->make_insert($aData, $this->sTable);
				$aData['id'] = $this->oDb->insert_id();
			}
			// ggf. fuehre eine oder mehrere Funktionen aus
			$this->_call_func_on_save();
			// ... die anderen methoden ERST BEI ACCEPT!
		} else {
		// "ohne preview"
			// update (Preview-Stammdatensatz)
			$this->oDb->make_update($aData, $this->sTable, $aUpdate);
			
			// update (search)
			#$this->_update_search($aData); // OLD
			$this->_save_searchdata($aData['id']); // NEW
			
			// ggf. update xml-struktur fuer int.-linkmanager des flash-editors
			$this->_write_flasheditor_xmlstructure($this->aENV['linkman']['multilang']);
			
			// ggf. fuehre eine oder mehrere Funktionen aus
			$this->_call_func_on_save();
			
			// MOVE-TO add-on: if category has changed -> reload with new cat (unabhaengig von "bReloadAfterSave"!!!)
			global $navi_id;
			if ($this->bReloadAfterSave	== false && !empty($navi_id) && $navi_id != $aData['navi_id']) {
				$this->_reload($aData['navi_id'], $aData['id']);
			}
		}
		
		// output
		return $aData;
	}

/**
* ADMIN: Bestaetigt die Aenderungen an einem Preview-Datensatz und macht ihn zum Live-Datensatz (der alte Live-Datensatz wird geloescht!).
* Updated einen Eintrag aus der eigenen (Stammdatensatz-Tabelle) sowie den entsprechenden aus der Search-Tabelle.
* Wie bei der DB-Klasse auch kann $aUpdate ein array oder die id als string sein.
* Ob in der Search-Tabelle ein insert oder ein update erfolgt, wird durch eine query ermittelt.
*
* Beispiel:
* <pre><code>
* $aData = $oCmsContent->accept($aData); // params: $aData
* </code></pre>
*
* @access   public
* @param	array	$aData			Daten, die upgedated werden
* @param	mixed (array|string)	Felder des where-constraint als assioziatives Array oder Datensatz-ID als String (MUSS "id" i.d.Tabelle sein!)
* @return	array	Um 'last_mod_by' erweitertes Daten-Array $aData
*/
	function accept($aData) {
		// vars zwischenspeichern
		$live_id	= $aData['preview_ref_id'];
		$preview_id	= $aData['id'];
		
		if(isset($aData['containerPath']) && isset($aData['container']) && isset($aData['containerProcess'])) {
			
			unlink($aData['containerPath'].$aData['container']);
			rename($aData['containerPath'].$aData['containerProcess'] , $aData['containerPath'].$aData['container']);
			unset($aData['containerPath']);
			unset($aData['container']);
			unset($aData['containerProcess']);
		}
		
		// CHECK
		if (empty($live_id) || $live_id < 1 || empty($preview_id) || $preview_id < 1) {
			$error = '<b>ERROR: eine der benoetigten Variablen ist LEER oder 0</b><br> 
				(class:"cms_content_edit, "method:"accept", 
				$live_id:"'.$live_id.'", $preview_id:"'.$preview_id.'", $user_id:"'.$this->nUserId.'").<br> 
				Was war die letzte Aktion?';
			@mail('cms@design-aspekt.de', 'ERROR: '.$_SERVER['SERVER_NAME'], $error);
			die($error);
		}
		if ($live_id == $preview_id) {
			$error = '<b>ERROR: beide Variablen sind GLEICH</b><br>  
				(class:"cms_content_edit, "method:"accept", 
				$live_id:"'.$live_id.'", $preview_id:"'.$preview_id.'", $user_id:"'.$this->nUserId.'").<br> 
				Was war die letzte Aktion?';
			@mail('cms@design-aspekt.de', 'ERROR: '.$_SERVER['SERVER_NAME'], $error);
			die($error);
		}
		//check, ob ein Preview-Datensatz exisitert!
		$this->oDb->query("SELECT `id` FROM `".$this->sTable."` WHERE `id` = ".$preview_id);
		if ($this->oDb->num_rows() == 0) {
			$error = '<b>ERROR: Dieser Datensatz wurde bereits (in einem anderen Browserfenster? / von einem anderen User?) freigegeben!</b><br>  
				DEBUG: Der Preview-Datensatz existiert nicht mehr. (class:"cms_content_edit, "method:"accept", 
				$live_id:"'.$live_id.'", $preview_id:"'.$preview_id.'", $user_id:"'.$this->nUserId.'").<br> 
				Was war die letzte Aktion?';
			@mail('cms@design-aspekt.de', 'ERROR: '.$_SERVER['SERVER_NAME'], $error);
			die($error);
		}
	
		// 1. Preview-Datensaz sichern
		$this->update($aData, $preview_id); // params: $aData,$aUpdate
		
		// 2. alten Live-Datensaz loeschen
		$this->oDb->make_delete($live_id, $this->sTable);
		
		// 3. Preview-Datensaz zum neuen Live-Datensatz machen
		$this->oDb->query("UPDATE `".$this->sTable."` SET `id` = ".$live_id.", `preview_ref_id` = 0 WHERE `id` = ".$preview_id);
		$aData['id']				= $live_id;
		$aData['preview_ref_id']	= 0;
		/*
		$oSlide->clearAll($this->sSlideshowId,$live_id);
		// Slideshow der Preview auf das Original 'mounten'
		$aSlides = $oSlide->getSlideshow($this->sSlideshowId,$preview_id);
		*/
		// CUG Zuordnungen umschichten
		if (is_object($this->oCug)) {
			// 1.: loesche ggf. alle Verknuepfungen mit dem alten Live-Datensatz zu Usergroups
			$this->oCug->removeAssignments($live_id, $this->sTable); // params: $sRelId, $sRelTable
			// 2.: loesche ggf. alle Verknuepfungen mit dem Preview-Datensatz zu Usergroups
			$this->oCug->removeAssignments($preview_id, $this->sTable); // params: $sRelId, $sRelTable
			// 3.: neue Verknuepfungen mit neuer ID speichern
			$this->oCug->assignUsergroup($live_id, $this->sTable, $aData['cug']); // params: $sRelId, $sRelTable, $usergroup
			unset($aData['cug']); // loesche dieses feld (da es in der content-table nicht existiert!)
		}
		
		// update (search)
		$aData['id'] = $live_id; // "id" wiederherstellen!
		$this->_save_searchdata($aData['id']); 
		
		// ggf. update xml-struktur fuer int.-linkmanager des flash-editors
		$this->_write_flasheditor_xmlstructure($this->aENV['linkman']['multilang']);
		
		// ggf. fuehre eine oder mehrere Funktionen aus
		$this->_call_func_on_accept();
		
		$this->writeSitemaps();
		
		if ($this->bReloadAfterSave	== true) {
			// reload
			$this->_reload($aData['navi_id'], $aData['id']);
		} else {
			// output
			return $aData;
		}
	}

/**
* Private Funktion, die die  aktuelle Seite nach einem Speicher-Vorgang neu laedt um unerwuenschte effekte zu vermeiden.
*
* @access   private
* @param	string	$sNewNaviId		Navi-ID des neu-zu-ladenden Datensatzes
* @param	string	$sNewId			ID des neu-zu-ladenden Datensatzes
* @return	void
*/
	function _reload($sNewNaviId, $sNewId) {
		global $navi_id, $sGEToptions, $aENV;
		// check auf Fragezeichen
		if (empty($sGEToptions)) {
			$sGEToptions = '?'; // Fragezeichen = PFLICHT
		}
		// MOVE-TO add-on: if category has changed -> reload with new cat
		if (!empty($navi_id) && !empty($sNewNaviId) && $navi_id != $sNewNaviId) {
			$sGEToptions = str_replace('navi_id='.$navi_id, 'navi_id='.$sNewNaviId, $sGEToptions);
		}
		// check auf Pflichtparameter
		if (!empty($sNewNaviId) && !strstr($sGEToptions, '&navi_id=')) {
			$sGEToptions .= '&navi_id='.$sNewNaviId; // navi_id = PFLICHT
		}
		if (!empty($sNewId) && !strstr($sGEToptions, '&id=')) {
			$sGEToptions .= '&id='.$sNewId; // id = PFLICHT
		}
		// reload!
		header('Location: '.$aENV['PHP_SELF'].$sGEToptions);
		exit;
	}

/**
* Private Funktion, die das Inserten in die search-table vornimmt.
*
* @access   private
* @param	string	$id				ID des neuen Stammdatensatzes
* @return	void
* @deprecated version - 28.04.2006
*/
	/*function _insert_search($id) {
		// check vars
		if (empty($id) || $id == 0) die("NO id in _insert_search"); // DEBUG
		
		// get search-data
		$aDataSearch = $this->_get_searchdata($id);
		if (!is_array($aDataSearch)) { return false; }
		
		// insert (search)
		$this->oDb->make_insert($aDataSearch, $this->sSearchTable);
		
		// NEU
		$this->_save_searchdata($id);
	}*/

/**
* Private Funktion, die das Updaten in der search-table vornimmt.
*
* @access   private
* @param	array	$aData			Daten, die im Stammdatensatz upgedated werden
* @return	void
* @deprecated version - 28.04.2006
*/
	/*function _update_search($aData) {
		// check vars
		if (!$aData['id']) die("NO id in _update_search"); // DEBUG
		#if (empty($aData['id']) || $aData['id'] == 0) { return false; }
		
		// get search-data
		$aDataSearch = $this->_get_searchdata($aData['id']);
		if (!is_array($aDataSearch)) { return false; } // wenn keine Daten: exit.
		
		// check search-table (man kann nicht davon ausgehen dass des search-datensatz auch schon existiert!)
		if (!$this->_is_in_searchtable($aData['id'])) { // wenn nicht...
			$this->_insert_search($aData['id']); // insert (search)
			return false; // ... und exit.
		}
		
		// update bedingung definieren
		$aUpdateSearch['ref_table']	= $this->sTable;
		$aUpdateSearch['ref_id']	= $aData['id'];
		if (!empty($this->sSearchLang)) { // fuege ggf. $sLang zum constraint
			$aUpdateSearch['lang']	= $this->sSearchLang;
		}
		// update (search)
		if (is_array($aDataSearch)) {
			$this->oDb->make_update($aDataSearch, $this->sSearchTable, $aUpdateSearch);
		}
	}*/

/**
* Private Funktion, die ueberprueft, ob ein bestimmter Datensatz schon in der search-table vorhanden ist oder nicht.
*
* @access   private
* @param	string	$id				Datensatz-ID
* @return	boolean	(true wenn datensatz vorhanden ist)
* @deprecated version - 28.04.2006
*/
	/*function _is_in_searchtable($id) {
		if (!$id) die("NO id in _is_in_searchtable"); // DEBUG
		global $aENV;
		if ($this->sTable == $aENV['table']['cms_navi']) return false; // Es gibt KEINE Eintraege der Navi-table in der Such-table.
		
		// check auf insert/update
		$sql = "SELECT `ref_id` FROM `".$this->sSearchTable."` WHERE `ref_id`='".trim($id)."' AND `ref_table`='".trim($this->sTable)."'";
		if (!empty($this->sSearchLang)) { $sql .= " AND `lang`='".$this->sSearchLang."'"; } // fuege ggf. $sSearchLang zur ueberpruefung hinzu
		$this->oDb->query($sql);
		
		// output (true wenn datensatz vorhanden ist)
		return ($this->oDb->num_rows() == 0) ? false : true;
	}*/

/**
* Private Funktion, die die insert/update Daten fuer die search-table zusammenbaut und zurueckgibt.
*
* @access   private
* @param	string	$id				Datensatz-ID
* @return	array	insert/update Daten fuer die search-table
* @deprecated version - 28.04.2006
*/
	/*function _get_searchdata($id) {
		//check
		if (!$id) die("NO id in _get_searchdata"); // DEBUG
		if (empty($this->sSearchTitle) && empty($this->sSearchText)) return false; // keine leeren Datensaetzt speichern
		
		// searchtext zusammenbauen
		$this->sSearchTitle = $this->sSearchTitle.$this->sSearchTrenner;
		$this->sSearchText = $this->sSearchTitle.' '.$this->sSearchText; // title mit in den searchtext nehmen
		$this->sSearchText = str_replace($this->aFlashSearch, $this->aFlashReplace, $this->sSearchText); // flash ersetzungen rueckgaengig machen
		#$this->sSearchText = Tools::formatSearchString($this->sSearchText);
		$sSearchtext = $this->sSearchTitle.$this->sSearchText.' '.time();
		
		// definiere insert/update-values
		$aData = array(
			"ref_table"		=> $this->sTable,
			"ref_id"		=> $id,
			"searchtext"	=> $sSearchtext
		);
		if (!empty($this->sSearchLang)) { $aData['lang'] = $this->sSearchLang; } // fuege ggf. $sLang hinzu
		
		// output
		return $aData;
	}*/

/**
* Private Funktion, die die insert/update Daten fuer die search-table uebergibt und speichert.
*
* @access   private
* @param	string	$id				Datensatz-ID
* @return	void
*/
	function _save_searchdata($id) {
		// check
		if (!$id) die("NO id in _get_searchdata"); // DEBUG
		if (empty($this->sSearchTitle) && empty($this->sSearchText)) return false; // keine leeren Datensaetzt speichern
		
		// searchtext vars uebergeben
		$this->oSearch->setRefTable($this->sTable);
		$this->oSearch->setRefId($id);
		$this->oSearch->setTitle($this->sSearchTitle);
		$this->oSearch->setText($this->sSearchText);
		if (!empty($this->sSearchLang)) {
			$this->oSearch->setLang($this->sSearchLang);
		}
		// save
		$this->oSearch->save();
	}

/**
* Private Funktion, die ggf. Ersetzungen direkt in den Daten vornimmt, die aus dem Flash-Texteditor kommen.
*
* @access   private
* @global	string	$weblang			Ausgabesprache
* @param	array	$aData			Datensatz (value by reference)
* @return	void
*/
	function _replace_flasheditor_specials(&$aData) {
		$anz = count($this->aFlashFieldname);
		if ($anz == 0) return; // funktioniert komischerweise nicht in einer Zeile!?!
		
		for ($i = 0; $i < $anz; $i++) {
			$aData[$this->aFlashFieldname[$i]] = str_replace(	$this->aFlashSearch, 
																$this->aFlashReplace, 
																$aData[$this->aFlashFieldname[$i]]);
		}
	}

/**
* Private Funktion, die ggf. ein XML-File fuer den Linkmanager im Flash-Texteditor schreibt.
* ACHTUNG: benoetigt "stripForFlash()" aus der "func.admin_common.php"!
* ACHTUNG: benoetigt ein initialisiertes Objekt "$oNav" der Klasse "class.cms_navi.php"!
*
* @access   public
* @global	array	$aENV
* @global	object	$oDb
* @global	object	$oFile
* @global	object	$oNav
*/
	function _write_flasheditor_xmlstructure($bLanguage = true) {
		// global vars
		global $aENV, $oFile, $oNav;
		// CONTENT-NAVI wird nicht aus der DB, sondern mittels Navi-Klasse generiert!
		if (!is_object($oNav)) { die("ERROR: kein CMS-Navi-Objekt!"); }
		
		// Templates sammeln, die unterseiten ('subsubpages') haben und ein db-query benoetigen
		$aTemplates	= array();
		// Tables sammeln, bei denen die XML-Struktur aktualisiert werden muss
		$aTable		= array($aENV['table']['cms_navi']); // navi-table auf jeden fall!
		foreach ($aENV['int_linkman'] as $k => $v) {
			$aTemplate[]	= $k;
			$aTable[]		= $v['table'];
		}
		$anzTempl = count($aTemplate);
		// check table
		if (!in_array($this->sTable, $aTable)) return; // abbrechen bei tables die nicht im linkmanager angezeigt werden
		
		// XML fuer jede sprachversion schreiben
		foreach ($aENV['content_language'] as $weblang => $val) {
			if ($weblang == "default") { continue(0); } // ueberspringe default
			
			// vars
			$xmlStr = '<XMLDocumentObject>'."\n";
			
			// NAVI ######################################################
			foreach($aENV['content_navi'] as $flag_topnavi => $naviBez) { // i.d. "setup.php" definiert!
				// Name des Navigationsbaumes
				if (is_array($naviBez)) {
					$naviBez = (!empty($naviBez[$weblang])) 
						? $naviBez[$weblang] 
						: $naviBez[$aENV['content_language']['default']]; // fallback
				}
				$xmlStr .= '<node label="'.strToUpper($naviBez).'" intLink="">'."\n";
				
				// MAIN /////////////////////////////////////////////////////
				$aMainNavi = $oNav->_getAllChildren('0', $flag_topnavi);
				
				foreach ($aMainNavi as $key => $aMainItem) {
					$tmpl = 0;
					
					//show only accessible main-pages
					if ($aMainItem['flag_online_'.$weblang] != '1') continue;
					
					// node
					$xmlStr .= "\t".$this->_getXmlNaviNodeString($aMainItem['title_'.$weblang], $aMainItem['href_'.$weblang], $aMainItem['id']); // params: $title, $href
					
					// DB-sub-pages (of MAIN): dafuer wird jetzt die db abgefragt... 
					$xmlStr .= $this->_getXmlTableNodes($anzTempl, $aMainItem, $weblang,$bLanguage);
					
					// SUB **************************************************
					$aSubNavi = $oNav->getSubNavi($aMainItem['id']); // params: [$nParentId=null]
					foreach ($aSubNavi as $key => $aSubItem) {
						//show only accessible sub-pages
						if ($aSubItem['flag_online_'.$weblang] != '1') { continue(0); }
						// node
						$xmlStr .= "\t\t".$this->_getXmlNaviNodeString($aSubItem['title_'.$weblang], $aSubItem['href_'.$weblang], $aSubItem['id']);
						// DBsub-pages (of SUB): dafuer wird jetzt die db abgefragt... 
						$xmlStr .= $this->_getXmlTableNodes($anzTempl, $aSubItem, $weblang, $bLanguage);
						
						// SUBSUB ..........................................
						$aSubSubNavi = $oNav->getSubNavi($aSubItem['id']); // params: [$nParentId=null]
						foreach ($aSubSubNavi as $key => $aSubSubItem) {
							//show only accessible sub-pages
							if ($aSubSubItem['flag_online_'.$weblang] != '1') { continue(0); }
							// node
							$xmlStr .= "\t\t\t".$this->_getXmlNaviNodeString($aSubSubItem['title_'.$weblang], $aSubSubItem['href_'.$weblang], $aSubSubItem['id']);
							// DBsub-pages (of SUBSUB): dafuer wird jetzt die db abgefragt... 
							$xmlStr .= $this->_getXmlTableNodes($anzTempl, $aSubSubItem, $weblang,$bLanguage);
							
							$xmlStr .= "\t\t\t".'</node>'."\n";
						} // END SUBSUB ..........................................
						
						$xmlStr .= "\t\t".'</node>'."\n";
					} // END SUB **************************************************
					
					$xmlStr .= "\t".'</node>'."\n";
				} // END MAIN /////////////////////////////////////////////////////
				
				$xmlStr .= '</node>'."\n";
			} // END foreach (NAVI) ###########################################
			
			$xmlStr .= '</XMLDocumentObject>'."\n\n";
			
			// make file
			$filename = (!isset($weblang) || empty($weblang)) ? "link_structure.xml" : "link_structure_".$weblang.".xml";
			$oFile->write_str_in_file($aENV['path']['xml']['unix'].$filename, $xmlStr);
			
		}// END foreach (Sprachversion)
	}
	
	/** Hilfsfunktion fuer "_write_flasheditor_xmlstructure()"
	* @access   private
	*/
	function _getXmlNaviNodeString($title, $href, $id) {
		$title = strip_tags(str_replace('"', "'", urldecode(stripForFlash($title))));
		return '<node label="'.$title.'" intLink="'.$href.'" naviId="'.$id.'">'."\n"; // wird nicht geschlossen!
	}
	
	/** Hilfsfunktion fuer "_write_flasheditor_xmlstructure()"
	* @access   private
	*/
	function _getXmlTableNodeString($title, $href, $id, $mainId) {
		$title = strip_tags(str_replace('"', "'", urldecode(stripForFlash($title))));
		return '<node label="'.$title.'" intLink="'.$href.'?id='.$id.'" naviId="'.$mainId.'" id="'.$id.'" />'."\n"; // wird geschlossen!
	}
	
	/** Hilfsfunktion fuer "_write_flasheditor_xmlstructure()"
	* @access   private
	*/
	function _getXmlTableNodes($anzTempl, &$aItem, $weblang,$bLanguage=true) {
		// vars
		if ($anzTempl == 0) { return; }
		global $aENV, $oDb;
		$xmlStr = '';
		if($bLanguage) {
			$wherefield = "`flag_online_".$weblang."`";
		}
		else {
			$wherefield = "`flag_online`";
		}
		// es werden alle in der "_include_all.php" unter "$aENV['int_linkman']" angegeben Tabellen abgefragt
		foreach ($aENV['int_linkman'] as $k => $v) {
			if (empty($aENV['int_linkman'][$k]['fieldname']))	{$aENV['int_linkman'][$k]['fieldname']	= "title_".$weblang;}
			if (empty($aENV['int_linkman'][$k]['orderby']))		{$aENV['int_linkman'][$k]['orderby']	= "title_".$weblang." ASC";}
			
			$sql	= "SELECT `id`, `".$aENV['int_linkman'][$k]['fieldname']."` as `title` 
						FROM `".$aENV['int_linkman'][$k]['table']."` 
						WHERE `navi_id`='".$aItem['id']."' 
							AND $wherefield='1' 
						ORDER BY ".$aENV['int_linkman'][$k]['orderby']."";
			$oDb->query($sql);
			while ($aData = $oDb->fetch_array()) {
				$xmlStr .= $this->_getXmlTableNodeString($aData['title'], $aItem['href_'.$weblang], $aData['id'], $aItem['id'] ); // params: $href,$title,$id
			} // END while
		} // END foreach
		
		return $xmlStr;
	}

/**
* Private Funktion, die ggf. Ersetzungen direkt in den Daten vornimmt, die aus der Methode "$oForm->pdf_tools()" kommen.
*
* @access   private
* @param	array	$aData			Datensatz (value by reference)
* @return	void
*/
	function _prepare_pdf_tools_data(&$aData) {
		// check var
		$anz = count($this->aPdfFieldname);
		if ($anz == 0) return; // funktioniert komischerweise nicht in einer Zeile!?!
		
		for ($i = 0; $i < $anz; $i++) {
			// wenn leer oder string -> in array umwandeln
			if (!is_array($aData[$this->aPdfFieldname[$i]])) {
				$aData[$this->aPdfFieldname[$i]][0] = $aData[$this->aPdfFieldname[$i]];
			}
			// check if empty-array-element (foreach liefert im gegensatz zu sizeof-for-schleife auch LEERE array-elemente)
			foreach ($aData[$this->aPdfFieldname[$i]] as $key => $val) {
				if ($val == '') { unset($aData[$this->aPdfFieldname[$i]][$key]); }
			}
			// wenn ein neues PDF angehaengt wurde, dieses zum array hinzufuegen
			if (!empty($aData['new_'.$this->aPdfFieldname[$i]])) {
				$aData[$this->aPdfFieldname[$i]][] = $aData['new_'.$this->aPdfFieldname[$i]];
			}
			// array zusammenfuehren zu comma-sep.-string
			$aData[$this->aPdfFieldname[$i]] = implode(',', $aData[$this->aPdfFieldname[$i]]);
			#if ($aData[$this->aPdfFieldname[$i]] == 0) {$aData[$this->aPdfFieldname[$i]] = NULL;}
			
			// Fake-Field (in dem ein neu angehaengtes ankommt) loeschen, da es nicht in der DB-table vorkommen wird
			unset($aData['new_'.$this->aPdfFieldname[$i]]);
		}
	}

/**
* Private Funktion, die ggf. Ersetzungen direkt in den Daten vornimmt, die aus der Methode "$oForm->pdf_tools()" kommen.
*
* @access   private
* @param	array	$aData			Datensatz (value by reference)
* @return	void
*/
	function _prepare_mdb_tools_data(&$aData) {
		// check var
		$anz = count($this->aMdbFieldname);
		if ($anz == 0) return; // funktioniert komischerweise nicht in einer Zeile!?!
		
		for ($i = 0; $i < $anz; $i++) {
			// wenn leer oder string -> in array umwandeln
			if (!is_array($aData[$this->aMdbFieldname[$i]])) {
				$aData[$this->aMdbFieldname[$i]][0] = $aData[$this->aMdbFieldname[$i]];
			}
			// check if empty-array-element (foreach liefert im gegensatz zu sizeof-for-schleife auch LEERE array-elemente)
			foreach ($aData[$this->aMdbFieldname[$i]] as $key => $val) {
				if ($val == '') { unset($aData[$this->aMdbFieldname[$i]][$key]); }
			}
			// wenn ein neues PDF angehaengt wurde, dieses zum array hinzufuegen
			if (!empty($aData['new_'.$this->aMdbFieldname[$i]])) {
				$aData[$this->aMdbFieldname[$i]][] = $aData['new_'.$this->aMdbFieldname[$i]];
			}
			// array zusammenfuehren zu comma-sep.-string
			$aData[$this->aMdbFieldname[$i]] = implode(',', $aData[$this->aMdbFieldname[$i]]);
			#if ($aData[$this->aMdbFieldname[$i]] == 0) {$aData[$this->aMdbFieldname[$i]] = NULL;}
			
			// Fake-Field (in dem ein neu angehaengtes ankommt) loeschen, da es nicht in der DB-table vorkommen wird
			unset($aData['new_'.$this->aMdbFieldname[$i]]);
		}
	}

/**
* Private Funktion, die ggf. die einzelnen Daten, die aus der Methode "$oForm->date_dropdown()" kommen, zu einem gueltigen ISO-Datum zusammenbaut.
*
* @access   private
* @param	array	$aData			Datensatz (value by reference)
* @return	void
*/
	function _prepare_date_dropdown_data(&$aData) {
		$anz = count($this->aDateDDFieldname);
		if ($anz == 0) return;
		
		for ($i = 0; $i < $anz; $i++) {
			$aData[$this->aDateDDFieldname[$i]] =	$_POST[$this->aDateDDFieldname[$i]]['year']."-".
													$_POST[$this->aDateDDFieldname[$i]]['month']."-".
													$_POST[$this->aDateDDFieldname[$i]]['day'];
		}
	}

/**
* Private Funktion, die ggf. eine oder mehrere Funktionen nach dem insert/update aufruft.
*
* @access   private
* @return	void
*/
	function _call_func_on_save() {
		$aFunc = $this->onSaveFunction;
		if (count($aFunc) == 0) { return; }
		
		foreach ($aFunc as $key => $func) {
			if (isset($this->onSaveArgs[$func]) && !empty($this->onSaveArgs[$func])) {
				call_user_func($this->onSaveFunction[$key], $this->onSaveArgs[$func]);
			} else {
				call_user_func($this->onSaveFunction[$key]);
			}
		}
	}

/**
* Private Funktion, die ggf. eine oder mehrere Funktionen nach dem accept aufruft.
*
* @access   private
* @return	void
*/
	function _call_func_on_accept() {
		$aFunc = $this->onAcceptFunction;
		if (count($aFunc) == 0) { return; }
		
		foreach ($aFunc as $key => $func) {
			if (isset($this->onAcceptArgs[$func]) && !empty($this->onAcceptArgs[$func])) {
				call_user_func($this->onAcceptFunction[$key], $this->onAcceptArgs[$func]);
			} else {
				call_user_func($this->onAcceptFunction[$key]);
			}
		}
	}

#------------------------------------------------------------------------------- DELETE

/**
* ADMIN: Loescht einen Live-Eintrag aus der eigenen sowie (alle Sprachversionen) aus der Search-Tabelle sowie eventuelle Verknuepfungen zu CUGs.
* Ggf. werden Funktionen nach den DB-deletes getriggert. Pflichtparameter um den richtigen Datensatz zu loeschen ist $id. 
* ACHTUNG: $id MUSS das Feld "id" i.d. Stammdatensatztabelle sein und darf nicht anders heissen!
*
* Beispiel:
* <pre><code>
* $oCmsContent->delete($aData['id']); // params: $id
* </code></pre>
*
* @access   public
* @param	string	$id				Datensatz-ID
* @return	void
*/
	function delete($id) {
		// make delete (live-stammdatensatz)
		$this->oDb->make_delete($id, $this->sTable);
		
		// make delete in search table NEW -> loesche ALLE sprachversionen!
		$this->oSearch->delete($id, $this->sTable);
		
		// CUG: loesche ggf. alle Verknuepfungen mit diesem Datensatz zu Usergroups
		if (is_object($this->oCug)) {
			$this->oCug->removeAssignments($id, $this->sTable); // params: $sRelId, $sRelTable
		}
		
		// ggf. update xml-struktur fuer int.-linkmanager des flash-editors
		$this->_write_flasheditor_xmlstructure($this->aENV['linkman']['multilang']);
		
		$this->writeSitemaps();
		
		// ggf. fuehre eine oder mehrere Funktionen aus
		$this->_call_func_on_delete();
	}

/**
* ADMIN: Loescht den Preview-Eintrag aus der eigenen sowie (alle Sprachversionen) aus der Search-Tabelle sowie eventuelle Verknuepfungen zu CUGs.
* Danach wird die ID des Live-Datensatzes zurueckgegeben. 
*
* Beispiel:
* <pre><code>
* $aData['id'] = $oCmsContent->restore($aData); // params: $aData
* if ($aData['id'] == false) {...}
* </code></pre>
*
* @access   public
* @param	string	$id				Datensatz-ID
* @return	mixed	Neue ID oder FALSE, wenn auch der Live-Datensatz geloescht wurde
*/
	function restore(&$aData) {
		// delete preview data
		$this->oDb->make_delete($aData['id'], $this->sTable);
		
		if(isset($aData['containerPath']) && isset($aData['containerProcess'])) {
			
			unlink($aData['containerPath'].$aData['containerProcess']);
			unset($aData['containerPath']);
			unset($aData['containerProcess']);
		}
		
		global $aENV;
		if (!empty($aENV['table']['cms_standard']) && $this->sTable != $aENV['table']['cms_standard']) {
			// ggf. auch den live-datensatz loeschen, sofern er noch nie online war
			// dazu zunaechst ermitteln, ob das feld "created" und "last_modified" identisch sind und "last_modified_by"!=0
			$this->oDb->query("SELECT `created`,`last_modified` FROM `".$this->sTable."` WHERE `id`='".$aData['preview_ref_id']."'");
			$buffer = $this->oDb->fetch_array();
			if ($buffer['created']==$buffer['last_modified']) {
				// wenn noch nicht befuellt -> live-datensatz loeschen!
				$this->oDb->make_delete($aData['preview_ref_id'], $this->sTable);
				// und neue id FALSE setzen, damit in der adminseite reagiert werden kann.
				$aData['preview_ref_id'] = FALSE;
			}
		}
		
		if ($this->bReloadAfterSave	== true) {
			// reload
			$this->_reload($aData['navi_id'], $aData['preview_ref_id']);
		} else {
			// id des live-datensatzes zurueckgeben
			return $aData['preview_ref_id'];
		}
		
	}

/**
* Private Funktion, die ggf. eine oder mehrere Funktionen nach dem DB-delete aufruft.
*
* @access   private
* @return	void
*/
	function _call_func_on_delete() {
		$aFunc = $this->onDeleteFunction;
		if (count($aFunc) == 0) { return; }
		
		foreach ($aFunc as $key => $func) {
			if (isset($this->onDeleteArgs[$func]) && !empty($this->onDeleteArgs[$func])) {
				call_user_func($this->onDeleteFunction[$key], $this->onDeleteArgs[$func]);
			} else {
				call_user_func($this->onDeleteFunction[$key]);
			}
		}
	}

#-----------------------------------------------------------------------------
} // END of class

?>
