<?php
/*
 * Created on 20.04.2006
 *
 */
/**
 * Diese Klasse kümmert sich um die Error behandelung. Mögliche Parameter sind Email oder Filelogging
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * 
 */
	class ErrorHandling {
	
		var $msg = array();
		
		function getMessage() {
			return $this->msg;
		}
		function addMessage($mesg) {
			$this->msg[] = $mesg;
		}
		
		/**
		 * Diese Methode schreibt den Errorreport ins Log.
		 * Benutzt dafür das zuvor gefüllte Attribut msg[].
		 */
		function writeErrors() {
			if(count($this->msg) > 0) { 
				global $aENV;
				$sFilename='inform.log';
				$fo = fopen($aENV['path']['sys_data']['unix'].$sFilename,'a+');
				fwrite($fo,'// LOGFILE CREATED BY DESIGN ASPEKT INFORM //'."\r\n");
				fwrite($fo,'// LOGFILE CREATED ON '.date('d.m.Y H:i:s').' //'."\r\n");
				fwrite($fo,'// CLIENT IP : '.$_SERVER['REMOTE_ADDR'].' //'."\r\n");
				fwrite($fo,'// PORT : '.$_SERVER['SERVER_PORT'].' //'."\r\n");
				fwrite($fo,"\r\n".'// START OF SCRIPT : '.$_SERVER['SCRIPT_NAME']."\r\n");
	
				for($i=0;!empty($this->msg[$i]);$i++) {
					fwrite($fo,$this->msg[$i]);
				}

				fwrite($fo,'// END OF SCRIPT : '.$_SERVER['SCRIPT_NAME']."\r\n\r\n");
				fclose($fo);
			}
		}
		
		/**
		 * Es wird eine Errormeldung zum internen MSG Attribut hinzugefügt und später durch eine der Errorhandlermethode ausgewertet.
		 * @param int $errno
		 * @param String $errstr
		 * @param String $errfile
		 * @param int $errline
		 * @return void
		 * 
		 */
		function errorReport($errno, $errstr, $errfile, $errline) {

			$str = '';
			
			switch($errno) {
				case E_USER_ERROR: // FATAL
					$str .= "<b>FATAL</b> [$errno] $errstr\r\n";
					$str .= "  Fatal error in line $errline of file $errfile\r\n";
					break;
				
				case E_USER_WARNING: // ERROR
					$str .= "<b>ERROR</b> [$errno] $errstr\r\n";
					$str .= "  error in line $errline of file $errfile\r\n";
					break;
					
				case E_USER_NOTICE: // WARNING
					$str .= "<b>WARNING</b> [$errno] $errstr\r\n";
					$str .= "  warning in line $errline of file $errfile\r\n";
					break;
				
				case E_WARNING: // System Warning
					$str .= "<b>SYSTEM WARNING</b> [$errno] $errstr\r\n";
					$str .= "  warning in line $errline of file $errfile\r\n";
					break;
				
				case E_STRICT: // nur Warnungen aus kompatiblitätsgründen unter PHP 5 lediglich - no report!
					break;
				
				case E_NOTICE: // Notice lediglich - no report!
					break;
				
				default:
					$str .= "Unkown error type: [$errno] $errstr\r\n";
					$str .= "  error in line $errline of file $errfile\r\n";
			}
			if($str != '') {
				$this->addMessage($str);
			}
		}
		
		/**
		 * Diese Methode versendet den Errorreport per Mail. Achtung es werden die Errormeldungen erst gecached und dann versendet.
		 * @return bool
		 */
		function writeErrorsToPerMail() {
			if(count($this->msg) > 0) { 
				$str = '// LOGFILE CREATED BY DESIGN ASPEKT INFORM //'."\r\n";
				$str .= '// LOGFILE CREATED ON '.date('d.m.Y H:i:s').' //'."\r\n";
				$str .= '// LOGFILE CREATED @ '.$_SERVER['SERVER_NAME'].' //'."\r\n";
				$str .= "\r\n".'// START OF SCRIPT : '.$_SERVER['SCRIPT_NAME']."\r\n";
	
				for($i=0;!empty($this->msg[$i]);$i++) {
					$str .= $this->msg[$i];
				}

				$str .= '// END OF SCRIPT : '.$_SERVER['SCRIPT_NAME']."\r\n\r\n";
				return mail('bugs@design-aspekt.com','BUG FROM SERVER: '.$_SERVER['SERVER_NAME'],$str);
			}
		}
	}
?>
