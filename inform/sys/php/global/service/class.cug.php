<?php // Class cug
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung eine Closed-User-Group (CUG) zu verwalten. 
* NOTE: benoetigt die Klassen "class.db_mysql.php" und  "class.session.php"!
*
* Example: 
* <pre><code> 
* $oCug =& new cug($aENV['db'], $weblang, true); // params: $aDbVars[,$sLang='de'][,$bPasswordRequired=false]
* //-- ADMIN --//
* // Fuelle das Array $this->aUsergroup mit den Daten aus der DB und gibt es zurueck
* $aUG = $oCug->getAllUsergroups(); // params: -
* // Ermittle alle mit dem uebergebenen Datensatz verknuepften usergroups. 
* $aUgRelated = $oCug->getUsergroup($sRelId, $sRelTable); // params: $sRelId ,$sRelTable 
* // Ermittle ob mit dem uebergebenen Datensatz usergroups verknuepft sind (schneller als "getUsergrouop()").
* $checkUg = $oCug->hasUsergroup($sRelId, $sRelTable); // params: $sRelId ,$sRelTable 
* // ADMIN: usergroups in verknuepfungstabelle speichern
* $oCug->assignUsergroup($aData['id'], $sTable, $aData['usergroup']); // params: $sRelId, $sRelTable, $usergroup
* 
* //-- WEB --//
* // WEB: Starte eine CUG-User-Session (und gib ggf. das erzeugte Objekt zurueck - z.B. fuer einen Warenkorb)
* $oSess =& $oCug->startSession(); // params: -
* // WEB: Login User (und gib alle seine Daten zurueck)
* $aCugUser = $oCug->loginUser($sLogin); // params: $sLogin[,$sPassword='']
* if ($oCug->isLoggedUser()) { doSomething(); } // params: -
* // WEB: Logout User
* [$oCug->setLogoutPage($sLogoutPage);] // optional!
* if (isset($_GET['btn_logout']) && $_GET['btn_logout'] == "true") $oCug->logoutUser(); // params: -
* // WEB: Gib alle Userdaten
* $aCugUser = $oCug->getUserData(); // params: -
* 
* // WEB: Pruefe den CUG-Status der Seite. 
* $bValid = $oCug->authPage($sNaviId); // params: $sNaviId
* // WEB: Ermittle alle geschuetzten Datensaetze einer (uebergebenen) Table.
* $oCug->setRecordTable($sTable);
* // WEB: Pruefe aktuellen Datensatz
* $bValid = $oCug->authRecord($aData['id']); // params: $id
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.51 / 2006-03-16	[BUGFIX: update table in "loginUser()"]
*/
class cug {
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse cug:
		----------------------------------------------------------------------------
		konstruktor cug($aDbVars, $sLang='de', $bPasswordRequired=false)
		function getAllUsergroups($bTitleOnly=true)
		function hasUsergroup($sRelId, $sRelTable)
		function getUsergroup($sRelId, $sRelTable)
		function getUsergroupByUid($sUserId)
		function getUser($id)
		function getUserLoginName($id, $bValidOnly=false)
		function getUserByCug($id)
		
		WEB:
		function startSession()
		private _login($id)
		private _logout()
		function loginUser($sLogin)
		function refreshUserdata($id)
		function changePassword($sNewPassword, $sConfirmPassword, $nUserId)
		function generatePassword($nPasswordLength=8)
		function logoutUser()
		function setLogoutPage($sPage)
		function getUserData()
		function isLoggedUser()
		function authPage()
		function setRecordTable($sTable)
		function authRecord()
		
		ADMIN:
		function assignUsergroup($sRelId, $sRelTable, $usergroup)
		function removeAssignments($sRelId, $sRelTable)
		function deleteUsergroup($sUsergroupId)
		function hasUsergroupUser($sUsergroupId)
		function isTitelUnique(&$aData, $aLanguages)
		----------------------------------------------------------------------------
		HISTORY:
		1.5 / 2006-03-03	[NEU: "getUserByCug()" + parameter in "getAllUsergroups()"]
		1.41 / 2005-07-12	[BUGFIX: in "_login()" + "_logout()"]
		1.4 / 2005-06-29	[NEU: "getUser()" + "getUserLoginName()"]
		1.33 / 2005-04-29	[NEU: "changePassword()" + "generatePassword()"]
		1.32 / 2005-04-29	[NEU: 2. Login-Parameter "password" + 3. Konstruktor-Parameter "$bPasswordRequired"]
		1.31 / 2005-04-28	[BUGFIX: return in "isTitelUnique()"]
		1.3 / 2005-04-21	[NEU: "refreshUserdata()" + "_login()" + "_logout()"]
		1.21 / 2005-04-19	[$lang in $weblang umbenannt]
		1.2 / 2005-04-11	[NEU: "hasUsergroupUser()" + "isTitelUnique()"]
		1.12 / 2005-02-22	[BUGFIX: auf "empty($sRelId)" pruefen bei "*Usergroup()"]
		1.11 / 2004-12-13	[NEU: DB-Feld last_login updaten bei "loginUser()"]
		1.1 / 2004-10-21	[NEU: alle WEB Methoden]
		1.01 / 2004-09-09	[NEU: "hasUsergroup()"]
		1.0 / 2004-08-30
	*/

#-----------------------------------------------------------------------------

/**
* @access   public
* @var	 	array	DB-Zugangsdaten
*/
	var $aDbVars;
/**
* @access   private
* @var	 	string	Ausgabesprache
*/
	var $sLang;
/**
* @access   private
* @var	 	boolean	Flag, ob fuer Login ein Passwort benoetigt wird
*/
	var $bPasswordRequired;
/**
* @access   private
* @var	 	object	DB-Object
*/
	var $oDb;
/**
* @access   private
* @var	 	object	DB-Object fuer subqueries
*/
	var $oDb2;
/**
* @access   public
* @var	 	array	alle Usergroups
*/
	var $aUsergroup;
/**
* @access   public
* @var	 	array	DB-Tabellen
*/
	var $aDbTable;
/**
* @access   private
* @var	 	object	Session-Objekt (fuer WEB)
*/
	var $oSess;
/**
* @access   private
* @var	 	array	Userdaten des eingeloggten Users (fuer WEB)
*/
	var $aCugUserData;
/**
* @access   private
* @var	 	string	Login Seite (fuer WEB)
*/
	var $sLoginPage;
/**
* @access   private
* @var	 	string	Logout Seite (fuer WEB)
*/
	var $sLogoutPage;
/**
* @access   private
* @var	 	array	IDs der geschuetzten Seiten eines Templates (table) als Array (fuer WEB)
*/
	var $aProtectedRecord;
/**
* @access   private
* @var	 	string	Tablename deren geschuetzte Datensaetze ermittelt werden (fuer WEB)
*/
	var $sProtectedRecordTable;

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das cug-Objekt und setzt ein paar Variablen
*
* Beispiel: 
* <pre><code> 
* $oCug =& new cug($aENV['db'], $weblang); // params: $aDbVars[,$sLang='de'][,$bPasswordRequired=false]
* </code></pre>
*
* @access   public
* @param 	array	$aDbVars	DB-Zugangsdaten
* @param 	string	$sLang		Ausgabesprache
* @return   void
*/
	function cug($aDbVars, $sLang='de', $bPasswordRequired=false) {
		if (is_array($aDbVars)) {
			$this->aDbVars = $aDbVars;
			$this->oDb =& new dbconnect($aDbVars);
		}
		if (empty($sLang)) $sLang = 'de'; // fallback!
		$this->sLang = $sLang;
		$this->bPasswordRequired = $bPasswordRequired;
		$this->aUsergroup = array();
		$this->aDbTable = array(
			"user"			=> "cug_user",
			"usergroup"		=> "cug_usergroup",
			"usergroup_rel"	=> "cug_usergroup_rel"
		);
		// check db-object
		if (!is_object($this->oDb)) {
			trigger_error("unable to init DB-object!",E_USER_ERROR);
			die("ERROR: unable to init DB-object!");
		}
	}

#-----------------------------------------------------------------------------

/**
* fuellt das Array $this->aUsergroup mit den Daten aus der DB und gibt es zurueck.
* Defaultmaessig wird ein einfaches Array (key: id, val: title) zurueckgegeben,
* wenn man jedoch als optionalen Parameter "true" uebergibt, wird der array-value
* mit allen Feldern der Usergroup-table gefuellt.
*
* Beispiel: 
* <pre><code> 
* $aUG = $oCug->getAllUsergroups(); // params: [$bTitleOnly=true]
* </code></pre>
*
* @access   public
* @return   array	$this->aUsergroup
*/
	function getAllUsergroups($bTitleOnly=true) {
		$sFields = ($bTitleOnly) 
			? "`id`, `title_".$this->sLang."` as `title`" 
			: "`id`, `title_".$this->sLang."` as `title`, `description`, `created`, `created_by`, `last_modified`, `last_mod_by`";
		$this->oDb->query("SELECT ".$sFields." 
							FROM `".$this->aDbTable['usergroup']."` 
							ORDER BY `id` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			$this->aUsergroup[$tmp['id']] = ($bTitleOnly) ? $tmp['title'] : $tmp;
		}
		
		// output
		return $this->aUsergroup;
	}

/**
* Ermittelt alle mit dem uebergebenen Datensatz verknuepften usergroups. 
* Gibt ein Array in der Form Key: Usergroup-ID, Value: Usergroup-Name zurueck.
*
* Beispiel: 
* <pre><code> 
* $aUgRelated = $oCug->getUsergroup($sRelId, $sRelTable); // params: $sRelId ,$sRelTable 
* </code></pre>
*
* @access   public
* @param	string	$sRelId		ID des Datensatzes dessen Usergroups ermittelt werden sollen
* @param	string	$sRelTable	Table-Name des Datensatzes dessen Usergroups ermittelt werden sollen
* @return   array				related Usergroup(s)
*/
	function getUsergroup($sRelId, $sRelTable) {
		$buffer = array();
		// check vars
		if (empty($sRelId)) { return $buffer; }
		if (count($this->aUsergroup) == 0) { $this->getAllUsergroups(); }
		
		// alle Verknuepfungen eines users zu usergroups ermitteln...
		$this->oDb->query("SELECT `usergroup_id` 
							FROM `".$this->aDbTable['usergroup_rel']."` 
							WHERE `rel_id` = '".$sRelId."' 
								AND `rel_table` = '".$sRelTable."'");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['usergroup_id']] = $this->aUsergroup[$tmp['usergroup_id']];
		}
		
		// output
		return $buffer;
	}

/**
* Ermittelt ob mit dem uebergebenen Datensatz usergroups verknuepft sind (schneller als "getUsergrouop()").
*
* Beispiel: 
* <pre><code> 
* $checkUg = $oCug->hasUsergroup($sRelId, $sRelTable); // params: $sRelId ,$sRelTable 
* </code></pre>
*
* @access   public
* @param	string	$sRelId		ID des Datensatzes dessen Usergroups gecheckt werden sollen
* @param	string	$sRelTable	Table-Name des Datensatzes dessen Usergroups gecheckt werden sollen
* @return   boolean				true = ist mit Usergroups verknuepft
*/
	function hasUsergroup($sRelId, $sRelTable) {
		// check vars
		if (empty($sRelId)) { return false; }
		if (count($this->aUsergroup) == 0) { $this->getAllUsergroups(); }
		
		// check Verknuepfungen
		$this->oDb->query("SELECT `usergroup_id` 
							FROM `".$this->aDbTable['usergroup_rel']."` 
							WHERE `rel_id` = '".$sRelId."' 
								AND `rel_table` = '".$sRelTable."'");
		
		// output
		return ($this->oDb->num_rows() > 0) ? true : false;
	}

/**
* Wrapper fuer getUsergroup() -> ermittelt alle mit dem uebergebenen USER verknuepften usergroups.
*
* Beispiel: 
* <pre><code> 
* $aUGrel = $oCug->getUsergroupByUid($sUserId); // params: $ssUserId
* </code></pre>
*
* @access   public
* @param	string	$sUserId		ID des User-Datensatzes dessen Usergroups ermittelt werden sollen
* @return   array					related Usergroup(s)
*/
	function getUsergroupByUid($sUserId) {
		// output
		return $this->getUsergroup($sUserId, $this->aDbTable['user']);
	}

/**
* Ermittelt alle ermittelbaren Daten des CUG-Users.
* Gibt ein leeres Array zurueck, wenn kein user mit der uebergebenen ID gefunden wurde.
*
* Beispiel: 
* <pre><code> 
* $aCugUser = $oCug->getUser($id); // params: $id
* </code></pre>
*
* @access   public
* @param	string	$id		ID des User-Datensatzes
* @return   array			alle ermittelbaren Daten des CUG-Users
*/
	function getUser($id) {
		$aData = array();
		// pruefen, ob User vorhanden -> wenn bekannt: Userdaten ermitteln
		$this->oDb->query("SELECT * FROM `".$this->aDbTable['user']."` WHERE `id` = '".$id."'");
		if ($this->oDb->num_rows() == 0) return $aData; // User = UNbekannt
		$aData = $this->oDb->fetch_array();
		
		// pruefen, ob User einer gueltigen Gruppe angehoert -> Usergroups ermitteln
		$aData['usergroup_id'] = $this->getUsergroupByUid($aData['id']);
		if (count($aData['usergroup_id']) == 0) return $aData; // User gehoert KEINER UG an!
		
		// User gehoert Usergroups an -> zusaetzliches flag setzen zum einfacheren referenzieren
		$aData['cug_login'] = true;
		
		// ggf. restliche Userdaten aus ADR-Tabellen holen
// TODO: in Klasse kapseln!
		if (isset($aData['contact_id']) && !empty($aData['contact_id'])) {
			// stamm-datensatz
			$this->oDb->query("SELECT `company`, `client_nr`, `client_shortname`, `gender`, `title`, `firstname`, `surname`, `birthday`, `grp`, `description`, `comment` 
								FROM `adr_contact`
								WHERE `id` = '".$aData['contact_id']."'");
			$aDataC = $this->oDb->fetch_array();
			// add type
			$aDataC['type'] = (!isset($aData['company']) || empty($aData['company'])) ? "contact" : "company";
			// adress-datensatz
			$cat = ($aDataC['type'] == "contact") ? "p" : "c";
			$this->oDb->query("SELECT `cat`, `street`, `building`, `region`, `town`, `po_box`, `po_box_town`, `c`.`country`, `phone_prefix`, `phone_mob`, `phone_tel`, `phone_fax`, `email`, `web`, `company_id`, `company_dep`, `company_pos`, `recipient` 
								FROM `adr_address` `a`, `adr_country` `c` 
								WHERE `reference_id` = '".$aData['contact_id']."' 
									AND `cat` = '".$cat."' 
									AND `a`.`country` = `c`.`code`");
			$aDataAdr = $this->oDb->fetch_array();
			if ($aDataC['type'] == "contact") { // wenn person:
				// ermittle o1 / o2 dieser kontakt(person)
				$this->oDb->query("SELECT `a`.`cat`, `a`.`company_id`, `c`.`company` 
								FROM `adr_address` `a`, `adr_contact` `c` 
								WHERE `a`.`company_id` = `c`.`id` 
									AND `a`.`reference_id` = '".$aData['contact_id']."'");
				$aCompany = array();
				while ($aData2 = $this->oDb->fetch_array()) {
					$aCompany[$aData2['cat']]['id'] = $aData2['company_id'];
					$aCompany[$aData2['cat']]['company'] = $aData2['company'];
				}
				$aDataAdr['company']     = $aCompany;
			}
			// arrays zusammenfuehren
			$aData = array_merge($aData, $aDataC, $aDataAdr);
		}
		
		// output Userdaten
		return $aData;
	}

/**
* Ermittelt nur den Login-Namen des CUG-Users.
* Gibt einen leeren String zurueck, wenn kein user mit der uebergebenen ID gefunden wurde.
*
* Beispiel: 
* <pre><code> 
* $sCugUsername = $oCug->getUserLoginName($id); // params: $id[,$bValidOnly=false]
* </code></pre>
*
* @access   public
* @param	string	$id		ID des User-Datensatzes
* @return   string			Login-Name des CUG-Users
*/
	function getUserLoginName($id, $bValidOnly=false) {
		// pruefen, ob User vorhanden -> wenn bekannt: Userdaten ermitteln
		$this->oDb->query("SELECT `id`, `login` 
							FROM `".$this->aDbTable['user']."` 
							WHERE `id` = '".$id."'");
		if ($this->oDb->num_rows() == 0) return ''; // User = UNbekannt
		$aData = $this->oDb->fetch_array();
		
		// ggf. pruefen, ob User einer gueltigen Gruppe angehoert -> Usergroups ermitteln
		if ($bValidOnly == true) {
			$aData['usergroup_id'] = $this->getUsergroupByUid($aData['id']);
			if (count($aData['usergroup_id']) == 0) $aData['login'] = ''; // User gehoert KEINER UG an -> Name NICHT ausgeben!
		}
		
		// output Login-Name
		return $aData['login'];
	}

/**
* Ermittelt alle CUG-User dieser Usergroup (key: UserID, val: array(loginname, name, ...)).
* Gibt ein leeres Array zurueck, wenn kein user gefunden wurde.
*
* Beispiel: 
* <pre><code> 
* $aCugUser = $oCug->getUserByCug($id); // params: $id
* </code></pre>
*
* @access   public
* @param	string	$id		ID des Usergroup-Datensatzes
* @return   array			CUG-User als assioziatives Array
*/
	function getUserByCug($id) {
		$buffer = array();
		// alle User dieser Usergroup ermitteln
		$sQuery = "SELECT `u`.* 
					FROM `".$this->aDbTable['user']."` `u`, `".$this->aDbTable['usergroup_rel']."` `r` 
					WHERE `u`.`id` = `r`.`rel_id` 
						AND `r`.`rel_table` = '".$this->aDbTable['user']."' 
						AND `r`.`usergroup_id` = '".$id."' 
					ORDER BY `u`.`login`";
		$this->oDb->query($sQuery);
		while ($aData = $this->oDb->fetch_array()) {
			$buffer[$aData['id']] = $aData;
		}
		
		// ggf. weitere Userdaten aus ADR-Tabellen holen
// TODO: in Klasse kapseln!
		foreach ($buffer as $u_id => $u_tmp) {
			// wenn keine adr-id -> gleich ueberspringen...
			if (!isset($u_tmp['contact_id']) || empty($u_tmp['contact_id'])) continue;
			
			// ...ansonsten neu zusammensetzen
			$buffer[$u_id] = $u_tmp;
			// ADR stamm-datensatz hinzufuegen
			$this->oDb->query("SELECT `company`, `client_nr`, `client_shortname`, `gender`, `title`, `firstname`, `surname`, `birthday`, `grp` 
								FROM `adr_contact`
								WHERE `id` = '".$u_tmp['contact_id']."'");
			$aData = $this->oDb->fetch_array();
			$buffer[$u_id]['company']			= $aData['company'];
			$buffer[$u_id]['client_nr']			= $aData['client_nr'];
			$buffer[$u_id]['client_shortname']	= $aData['client_shortname'];
			$buffer[$u_id]['gender']			= $aData['gender'];
			$buffer[$u_id]['title']				= $aData['title'];
			$buffer[$u_id]['firstname']			= $aData['firstname'];
			$buffer[$u_id]['surname']			= $aData['surname'];
			$buffer[$u_id]['birthday']			= $aData['birthday'];
			$buffer[$u_id]['grp']				= $aData['grp'];
			// add type
			$buffer[$u_id]['type'] = (!isset($aData['company']) || empty($aData['company'])) ? "contact" : "company";
		}
		
		// output
		return $buffer;
	}

#------------------------------------------------------------------------------- WEB

/**
* WEB: Startet eine CUG-User-Session und gibt das erzeugte Objekt zurueck.
*
* Beispiel: 
* <pre><code> 
* $oSess =& $oCug->startSession(); // params: -
* </code></pre>
*
* @access   public
* @return   object				Session Objekt
*/
	function startSession() {
		// build session name
		global $aENV;
		$name = $aENV['client_shortname'];
		if (empty($name)) $name = "web"; // fallback
		$name .= "_cuguser";
		
		// Session Init
		$this->oSess =& new session($name);  // params: $sName
		// ggf. schon vorhandene Userdaten aus der Session ermitteln und in Klassenvar speichern
		$this->aCugUserData = $this->oSess->get_var('aCugUser'); // params: $sName[,$default=null]
		
		// output
		return $this->oSess;
	}

/**
* INTERNE Funktion: Login
* @access   private
* @param	string	$id		ID des User-Datensatzes
* @return   boolean			true bei erfolgreichem Login, sonst false
*/
	function _login($id) {
		
		// Userdaten in der Klasse speichern
		$this->aCugUserData = $this->getUser($id); // params: $id
		
		// check
		if (empty($this->aCugUserData['id'])) return false;
		
		// ggf. Userdaten in der Session speichern
		if (is_object($this->oSess)) {
			$this->oSess->set_var('aCugUser', $this->aCugUserData); // params: $sName[,$value=null]
		}
		
		return true;
	}

/**
* INTERNE Funktion: Logout
* @access   private
* @return   void
*/
	function _logout() {
		// Userdaten in der Klasse loeschen
		$this->aCugUserData = array();
		// daten aus der session loeschen
		if (is_object($this->oSess)) {
			$this->oSess->unset_var('aCugUser');
		}
	}

/**
* WEB: Versucht einen User mittels uebergebenem Login einzuloggen.
* Im Fehlerfall wird "false" zurueckgegeben ansonsten werden alle mit dem user verknuepften Daten (inklusive usergroups!) ermittelt und zurueckgegeben. 
* Wurde vorher die Methode "startSession()" aufgerufen, werden die Userdaten ausserdem in der Session i.d. Array-Variablen "aCugUser" gespeichert.
*
* Beispiel: 
* <pre><code> 
* $aCugUser = $oCug->loginUser($sLogin); // params: $sLogin[,$sPassword=''][,$bUpdateLastloginAtFirstlogin=true]
* </code></pre>
*
* @access   public
* @param	string	$sLogin		Value des Feldes "login" des User-Datensatzes dessen Usergroups ermittelt werden sollen
* @param	string	$sPassword	Value des Feldes "password" des User-Datensatzes dessen Usergroups ermittelt werden sollen
* @param	string	$bUpdateLastloginAtFirstlogin	[soll beim ersten Login deas Feld "last_login" upgedatet werden (default: true)
* @return   mixed				Userdata-Array oder false
*/
	function loginUser($sLogin, $sPassword='', $bUpdateLastloginAtFirstlogin=true) {
		// check vars
		$sLogin = str_replace(';', '', strip_tags($sLogin)); // sichern
		if (empty($sLogin)) return;
		if (!is_object($this->oDb)) {
			trigger_error("unable to init DB-object!",E_USER_ERROR);	
			return false;
		}
		if (!empty($sPassword) && $this->bPasswordRequired == true) {
			$sPassword = str_replace(';', '', strip_tags($sPassword)); // sichern
		}
		
		// pruefen, ob User vorhanden -> wenn bekannt: Userdaten ermitteln
		$sQuery = "SELECT `id`, `last_login` 
					FROM `".$this->aDbTable['user']."` 
					WHERE `login` = '".trim($sLogin)."'";
		if ($this->bPasswordRequired == true) {
			$sQuery .= " AND `password` = '".trim($sPassword)."'";
		}
		$this->oDb->query($sQuery);
		if ($this->oDb->num_rows() == 0) return false; // User = UNbekannt
		$id = $this->oDb->fetch_field('id');
		if (empty($id)) return false;
		
		// do login
		if (!$this->_login($id)) return false;
		
		// Update 'last_login'-field (ausser beim allerersten login!)
		if ((isset($this->aCugUserData['last_login']) && !empty($this->aCugUserData['last_login']))
			|| (empty($this->aCugUserData['last_login']) && $bUpdateLastloginAtFirstlogin == true)) {
			$aUpdateData['last_login'] = date("Y-m-d H:i:s");
			$this->oDb->make_update($aUpdateData, $this->aDbTable['user'], $this->aCugUserData['id']);
		}
		
		$_SERVER['QUERY_STRING']	= eregi_replace("btn_login=", "", $_SERVER['QUERY_STRING']);
		$_SERVER['QUERY_STRING']	= eregi_replace("Login", "", $_SERVER['QUERY_STRING']);
		$_SERVER['QUERY_STRING']	= eregi_replace("&&", "&", $_SERVER['QUERY_STRING']);
		
		// seite neu laden
		if (is_object($this->oSess)) {
			header("Location: ".basename($_SERVER['PHP_SELF']).'?'.$_SERVER['QUERY_STRING']); exit;
		}
		// output
		return $this->aCugUserData;
	}

/**
* WEB: Aktualisiert die Userdaten mittels uebergebener ID.
* 
* Beispiel: 
* <pre><code> 
* $aCugUser = $oCug->refreshUserdata($id); // params: $id
* </code></pre>
*
* @access   public
* @param	string	$sLogin		Value des Feldes "login" des User-Datensatzes dessen Usergroups ermittelt werden sollen
* @return   mixed				Userdata-Array oder false
*/
	function refreshUserdata($id) {
		// check vars
		$id = str_replace(';', '', strip_tags($id)); // sichern
		if (empty($id)) return false;
		if (!is_object($this->oDb)) return false;
				
		// Login
		if (!$this->_login($id)) return false;
		
		// output
		return $this->aCugUserData;
	}

/**
* WEB: Aendert das Passwort eines Users (kann vom User aufgerufen werden).
* Das "last_login"-Feld wird upgedatet und die Seite wird daraufhin reloaded.
*
* Beispiel: 
* <pre><code> 
* if (isset($_GET['btn_changepassword'])) $oCug->changePassword($_GET['new_password'], $_GET['new_password2'], $aCugUser['id']); // params: $sNewPassword,$sConfirmPassword,$nUserId
* </code></pre>
*
* @access   public
* @param	string	$sNewPassword		Neues Passwort
* @param	string	$sConfirmPassword	Passwort-Bestaetigung
* @param	int		$nUserId			ID des aktuellen CUG-Nutzers
* @return   void
*/
	function changePassword($sNewPassword, $sConfirmPassword, $nUserId) {
		// check vars
		
		if (empty($nUserId)) { 
			trigger_error('CUG Userid empty! Exiting ChangePassword!',E_USER_ERROR);
			return false; 
		}
		$sNewPassword = strip_tags(trim($sNewPassword));
		$sConfirmPassword = strip_tags(trim($sConfirmPassword));
		if (empty($sNewPassword)) {
			trigger_error('CUG NewPassword empty! Exiting ChangePassword!',E_USER_WARNING);
			return false;
		}
		if ($sNewPassword != $sConfirmPassword) return false;
		
		// change password
		$aUpdateData['password'] = $sNewPassword;
		$aUpdateData['last_login'] = date("Y-m-d H:i:s");
		$this->oDb->make_update($aUpdateData, $this->aDbTable['user'], $nUserId);
		
		// Userdaten in der Klasse um neue Daten ergaenzen
		$this->aCugUserData['password'] = $aUpdateData['password'];
		$this->aCugUserData['last_login'] = $aUpdateData['last_login'];
		
		// ggf. Userdaten in der Session aktualisieren
		if (is_object($this->oSess)) {
			$this->oSess->set_var('aCugUser', $this->aCugUserData); // params: $sName[,$value=null]
		}
		// seite neu laden
		header("Location: ".basename($_SERVER['PHP_SELF'])); exit;
	}

/**
* Function to generate Random Password.
*
* Beispiel: 
* <pre><code> 
* $sRandomPw = $oCug->generatePassword(); // params: [$nPasswordLength=8]
* </code></pre>
*
* @access   public
* @param	int		$nPasswordLength	Anzahl Zeichen, die das generierte Passwort umfassen soll
* @return   string	Passwort
*/
	function generatePassword($nPasswordLength=8) {
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabchefghjkmnpqrstuvwxyz0123456789";
		$i = 0;
		$buffer = '';
		while ($i <= $nPasswordLength) {
			$num = rand() % 50;
			$buffer .= substr($chars, $num, 1);
			$i++;
		}
		return $buffer;
	}

/**
* WEB: Loggt einen User aus und beendet ggf. die Session.
* Die Seite wird daraufhin reloaded, ausser es wurde mittels "setLogoutPage()" eine spezielle Logout-Seite definiert, dann wird auf diese weitergeleitet.
*
* Beispiel: 
* <pre><code> 
* if (isset($_GET['btn_logout']) && $_GET['btn_logout'] == "true") $oCug->logoutUser(); // params: -
* </code></pre>
*
* @access   public
* @return   void
* @see		setLogoutPage()
*/
	function logoutUser() {
		// Logout
		$this->_logout();
		
		// seite neu laden
		if (!empty($this->sLogoutPage)) { // ggf. spezielle Logout-Seite
			header("Location: ".$this->sLogoutPage); exit;
		} else { // default: reload
			$_SERVER['QUERY_STRING'] = str_replace('btn_logout=true', '', $_SERVER['QUERY_STRING']);
			header("Location: ".basename($_SERVER['PHP_SELF']).'?'.$_SERVER['QUERY_STRING']); exit;
		}
	}

/**
* WEB: Setzt eine Custom-Seite, zu der Weitergeleitet wird, wenn ein User ausgeloggt wird.
*
* @access   public
* @param	string	$sPage			Pfad/Filename der Logout-Seite
* @return   void
* @see		logoutUser()
*/
	function setLogoutPage($sPage) {
		$this->sLogoutPage = $sPage;
	}

/**
* WEB: Gibt alle Daten des eingeloggten Users zurueck (inklusive Usergroups).
*
* Beispiel: 
* <pre><code> 
* $aCugUser = $oCug->getUserData(); // params: -
* </code></pre>
*
* @access   public
* @return   array	Alle Daten des eingeloggten Users
*/
	function getUserData() {
		// output
		return $this->aCugUserData;
	}

/**
* WEB: Gibt true zurueck, wenn der User existiert, eingeloggt ist und mindestens einer Usergroup angehoert, sonst false.
*
* Beispiel: 
* <pre><code> 
* if ($oCug->isLoggedUser()) { doSomething(); } // params: -
* </code></pre>
*
* @access   public
* @return   boolean
*/
	function isLoggedUser() {
		// output
		return (is_array($this->aCugUserData) 
				&& count($this->aCugUserData) > 0
				&& is_array($this->aCugUserData['usergroup_id'])
				) ? true : false;
	}

/**
* WEB: Prueft den CUG-Status der Seite. 
* Wenn die Seite fuer ein oder mehrere CUGs geschuetzt ist, wird geprueft ob ein gueltiger User eingeloggt ist. 
* Falls nein, wird false zurueckgegeben, andernfalls (oder wenn die Seite nicht geschuetzt ist) true.
*
* Beispiel: 
* <pre><code> 
* $bValid = $oCug->authPage($sNaviId); // params: $sNaviId
* </code></pre>
*
* @access   public
* @param	string	$sNaviId			NaviId der Seite
* @return   boolean
*/
	function authPage($sNaviId) {
		// vars
		$sNaviId = !empty($sNaviId) ? $sNaviId+0 : ''; // sichern -->> Konvertierung auf Integer durch Addition von 0
		if (empty($sNaviId)) return true;
		global $aENV;
		
		// CUGs der Seite ermitteln
		$aUgRelated = $this->getUsergroup($sNaviId, $aENV['table']['cms_navi']); // params: $sRelId ,$sRelTable
		 
		if (count($aUgRelated) == 0) return true; // seite ist NICHT geschuetzt -> abbrechen!
		
		// Seite ist geschuetzt -> Pruefe ob User die richtigen Rechte hat
		if ($this->isLoggedUser()) {	// Ueberpruefung nur, wenn eingeloggt und valide
			$aUserCug = array_keys($this->aCugUserData['usergroup_id']);
			// Der Seite zugeordnete Usergroup-Ids vergleichen mit denen des Users
			foreach($aUgRelated as $key => $val) {
				if (in_array($key, $aUserCug)) { return true; break; } // bei Uebereinstimmung abbrechen
			}
		}
		// Keine Uebereinstimmung: Seite ist geschuetzt und der User hat keine oder nicht die richtigen Rechte
		return false;
	}

/**
* WEB: Ermittelt alle geschuetzten Datensaetze einer (uebergebenen) Table.
*
* @access   public
* @param	string	$sTable			Tablename der Datensaetze deren CUG-Status ermittelt werden soll
* @return   mixed	false oder Array mit den IDs der geschuetzten Datensaetze
* @see		logoutUser()
*/
	function setRecordTable($sTable) {
		// check vars
		$sTable = str_replace(';', '', strip_tags($sTable)); // sichern
		if (empty($sTable)) return;
		if (!is_object($this->oDb)) return;
		$this->sProtectedRecordTable = $sTable;
		$this->oDb->debug=true;
		// geschuetzte Eintraege der uebergebenen Table ermitteln
		$this->oDb->query("SELECT `usergroup_id`, `rel_id` 
						FROM `".$this->aDbTable['usergroup_rel']."` 
						WHERE `rel_table` = '".$sTable."'");
		if ($this->oDb->num_rows() == 0) return false;
		
		// es bestehen geschuetzte Eintraege -> speichern
		$aProtectedRecord = array();
		while($aData = $this->oDb->fetch_array()) {	// zugelassene Usergruppen und ID des Eintrags ermitteln
			$aProtectedRecord[$aData['rel_id']][] = $aData['usergroup_id'];
		}
		#print_r($aProtectedRecord);
		$this->aProtectedRecord = $aProtectedRecord;
		
		// output
		return $aProtectedRecord;
	}

/**
* WEB: Prueft den CUG-Status des Datensatzes. 
* Wenn der Datensatz fuer ein oder mehrere CUGs geschuetzt ist, wird geprueft ob ein gueltiger User eingeloggt ist. 
* Falls nein, wird false zurueckgegeben, andernfalls (oder wenn der Datensatz nicht geschuetzt ist) true.
*
* Beispiel: 
* <pre><code> 
* $bValid = $oCug->authRecord($aData['id']); // params: $id
* </code></pre>
*
* @access   public
* @param	string	$id			ID des Datensatzes
* @return   boolean
*/
	function authRecord($id) {
		// vars
		$id = !empty($id) ? $id+0 : ''; // sichern -->> Konvertierung auf Integer durch Addition von 0
		if (empty($id)) return true;
		if (empty($this->sProtectedRecordTable)) { 
			trigger_error('setRecordTable($sTable) first!',E_USER_ERROR);
			die('ERROR: setRecordTable($sTable) first!');
		}

		// Datensatz ist nicht geschuetzt
		if (empty($this->aProtectedRecord)) return true;
		if (!is_array($this->aProtectedRecord[$id])) return true;
		
		// Datensatz gehoert zu den geschuetzten Eintraegen -> Pruefe ob User die richtigen Rechte hat
		if ($this->isLoggedUser()) {	// Ueberpruefung nur, wenn eingeloggt und valide
			$aUserCug = array_keys($this->aCugUserData['usergroup_id']);
			// Dem Datensatz zugeordnete Usergroup-Ids vergleichen mit denen des Users
			foreach ($this->aProtectedRecord[$id] as $key => $val) {
				if (in_array($val, $aUserCug)) { return true; break; } // bei Uebereinstimmung abbrechen
			}
		}
		
		// Keine Uebereinstimmung: Datensatz ist geschuetzt und der User hat keine oder nicht die richtigen Rechte
		return false;
	}

#------------------------------------------------------------------------------- ADMIN

/**
* ADMIN: usergroups in verknuepfungstabelle speichern
*
* Beispiel: 
* <pre><code> 
* $oCug->assignUsergroup($aData['id'], $sTable, $aData['usergroup']); // params: $sRelId, $sRelTable, $usergroup
* </code></pre>
*
* @access   public
* @param	string	$sRelId			ID des Datensatzes dessen Usergroups ermittelt werden sollen
* @param	string	$sRelTable		Table-Name des Datensatzes dessen Usergroups ermittelt werden sollen
* @param	mixed (string|array)	einzelne oder mehrere Usergroups (als array oder kommasep. string) die zugewiesen werden sollen
* @return   void
*/
	function assignUsergroup($sRelId, $sRelTable, $usergroup) {
		// erst alle eintraege mit diesem verknuepfungs-datensatz loeschen...
		$this->removeAssignments($sRelId, $sRelTable);
		
		// check value
		if (empty($usergroup)) return;
		if (!is_array($usergroup)) { $usergroup = explode(',', $usergroup); }
		if (!is_array($usergroup)) return;
		
		// dann alle verknuepfungen neu schreiben
		foreach ($usergroup as $ug) {
			$aInsert = array('usergroup_id'	=> $ug, 
							 'rel_id'		=> $sRelId, 
							 'rel_table'	=> $sRelTable);
			$this->oDb->make_insert($aInsert, $this->aDbTable['usergroup_rel']);
		}
	}

/**
* ADMIN: Loescht Zuordnungen (zu einer Usergroup) anhand der ZuordnungsID und -Table
*
* Beispiel: 
* <pre><code> 
* // z.B. loesche alle verknuepfungen mit diesem user zu usergroups
* $oCug->removeAssignments($user_id, $user_table); // params: $sRelId, $sRelTable
* </code></pre>
*
* @access   public
* @return   array	$this->aUsergroup
*/
	function removeAssignments($sRelId, $sRelTable) {
		// alle verknuepfungen mit diesem user und usergroups loeschen
		$aData2delete = array('rel_id'		=> $sRelId, 
							  'rel_table'	=> $sRelTable);
		return $this->oDb->make_delete($aData2delete, $this->aDbTable['usergroup_rel']);
	}

/**
* ADMIN: Loescht eine Usergroup und alle ihre Zuordnungen.
*
* @access   public
* @param	string	$sUsergroupId		ID der Usergroup die geloescht werden soll
* @return   boolean
*/
	function deleteUsergroup($sUsergroupId) {
		// check vars
		if (empty($sUsergroupId)) return;
		
		// make delete (in usergroup-table)
		$aData2delete['id'] = $sUsergroupId;
		$this->oDb->make_delete($aData2delete, $this->aDbTable['usergroup']);
		
		// alle verknuepfungen mit diesem user und usergroups loeschen
		$aData2delete = array('usergroup_id' => $sUsergroupId);
		return $this->oDb->make_delete($aData2delete, $this->aDbTable['usergroup_rel']);
	}

/**
* ADMIN: Prueft, ob eine Usergroup zugeordnete User hat (true, wenn User mit der uebergebenen Usergroup-ID gefunden wurden).
*
* @access   public
* @param	string	$sUsergroupId		ID der Usergroup die auf User geprueft werden soll
* @return   boolean
*/
	function hasUsergroupUser($sUsergroupId) {
		// check vars
		if (empty($sUsergroupId)) return;
		// build sql
		$sQuery = "SELECT `rel_id` 
					FROM `".$this->aDbTable['usergroup_rel']."` 
					WHERE `rel_table` = '".$this->aDbTable['user']."' 
						AND `usergroup_id` = '".$sUsergroupId."'";
		// do it
		$this->oDb->query($sQuery);
		return ($this->oDb->num_rows() > 0) ? true : false;
	}

/**
* ADMIN: Prueft, ob ein Name fuer eine Usergroup bereits vergeben wurde.
*
* @access   public
* @param	string	$sUsergroupId		ID der Usergroup die auf User geprueft werden soll
* @return   boolean
*/
	function isTitelUnique(&$aData, $aLanguages) {
		// available languages
		if (!is_array($aLanguages) || count($aLanguages) == 0) {
			$aLanguages = array($this->sLang); // set default (fallback)
		}
		// build sql
		$sQuery = 'SELECT `id` FROM `'.$this->aDbTable['usergroup'].'` WHERE ';
		$aConstraint = array();
		foreach ($aLanguages as $weblang) {
			$aConstraint[] = "`title_".$weblang."` = '".$aData['title_'.$weblang]."'";
		}
		if (isset($aData['id']) && !empty($aData['id'])) {
			$aConstraint[] = '`id` <> '.$aData['id'];
		}
		// do it
		$this->oDb->query($sQuery.implode(' AND ', $aConstraint));
		return ($this->oDb->num_rows() > 0) ? false : true;
	}

#-----------------------------------------------------------------------------
} // END of class

?>