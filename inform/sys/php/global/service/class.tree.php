<?php // Class tree
/**
* This class provides funktions to deal with trees (with data from a db-table). 
* (needs "class.db_mysql.php"!!!)
*
* Example: 
* <pre><code>
* require_once("php/class.tree.php");
* $oTree =& new tree($oDb, $sTable); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
* $oTree->buildTree();
* echo "gefundene Datensaetze: ".$oTree->getCount();
* $aStructure = $oTree->getAllChildren();
* foreach($aStructure as $k => $aData) { ... }
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.8 / 2006-03-29 (NEU: "unsetAllName()"+"removeOption()"+"$bAllowDouble" bei "addOption()"]
*/

class tree {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse tree:
		----------------------------------------------------------------------------
		constructor tree(&$oDb, $sTable, $sCachefile='', $module='', $aOptions=array())
		function setAuth($uid, &$oTACEA, &$oSess);
		function buildTree()
		private _buildFlatTree()
		private _checkTable()
		private _createTable()
		private _moveChildren($completeArray, &$childArray)
		function clearCache()
		
		function getFlatArray()
		function getTreeArray()
		function getCount()
		function getEbenen()
		function getValueById($id, $field)
		function countSublevel($id)
		function getAllChildren($parentId='0')
		function getAllChildrenId($parentId='0')
		function getAllParentId($id, $firstrun=true)
		private  _getCurrentPosition($id)
		function getPositionString($id)
		function getPrev($id)
		function getNext($id)
		function getParent($id)
		function isInArray($needle, $checkkey='')
		
		function setRootName($sName)
		function setRootOff()
		function addOption($aValue, $bAllowDouble=true)
		function removeOption($aValue)
		function setAllName($sName)
		function unsetAllName()
		function setSelectExtra($sString)
		function treeFormSelect($name='', $currentId='', $arr='', $ebene=0)
		----------------------------------------------------------------------------
		HISTORY:
		2.8 / 2006-03-29 (NEU: "unsetAllName()"+"removeOption()"+"$bAllowDouble" bei "addOption()"]
		2.7 / 2006-02-07 (NEU: Authorisierung + Initialisierung umgebaut -> ACHTUNG: nicht abwaertskompatibel!)]
		2.6 / 2006-01-09 (NEU: Authorisierung eingebaut, checkTable deaktiviert)]
		2.5 / 2005-07-18 (NEU: "setSelectExtra()"]
		2.4 / 2005-04-11 (NEU: mehrsprachfaehig gemacht + "aOptions" als neuer Kunstruktorparameter (tree2 ist damit ueberfluessig!)]
		2.3 / 2005-04-07 (NEU: "countSublevel()"]
		2.22 / 2005-03-31 (BUGFIX: rekursives weiterhangeln i.d. Kinder bei "getAllChildrenId()"]
		2.21 / 2005-03-03 (BUGFIX: doppelte Anfuehrungszeichen escapen bei array-file schreiben bei"_buildFlatTree()"]
		2.2 / 2005-01-07 (NEU: "addOption()"]
		2.1 / 2004-12-21 (NEU: "getAllParentId()"]
		2.0 / 2004-12-03 (NEU: "clearCache()" + private Funktionen umbenannt (+Prefix '_')]
		1.9 / 2004-12-02 (NEU: "getAllChildrenId()" + "initTree()"]
		1.8 / 2004-11-30 (NEU: "getParent()"]
		1.72 / 2004-06-15 [NEU: "setRootOff()"]
		1.71 / 2004-06-14 [NEU: moeglichkeiten "root" + Prefix(-) i.d. 1.Ebene auszublenden in "treeFormSelect()"]
		1.7 / 2004-06-04 [NEU: (von class.tree2.php): "getValueById()"]
		1.65 / 2004-06-03 [BUGFIX in "treeFormSelect()": "$buffer" in "$this->buffer" geaendert]
		1.64 / 2004-04-27 [durchgaengigkeit schreibweisen korrigiert und "writeTreePopup()" in "treeFormSelect()" umbenannt]
		1.63 / 2004-02-04 [BUGFIX checked root folder in "writeTreePopup()"]
		1.62 / 2004-02-02 [reihenfolge "all folders" in "writeTreePopup()" geaendert]
		1.61 / 2004-01-29 [NEU: ggf. "prio" in "_createTable()" mit aufgenommen]
		1.6 / 2004-01-27 [NEU: "setAllName()" + "$sOrderBy"]
		1.5 / 2004-01-26 [NEU: "checkTable()" + "writeTable()"]
		1.43 / 2004-01-23 [Namen geaendert: "write_tree_popup()" -> "writeTreePopup()"]
		1.42 / 2004-01-16 [check auf $oDb im konstruktor!]
		1.41 / 2004-01-09 [globale Variablen entfernt -> ALLE vars muessen uebergeben werden!]
		1.4 / 2004-01-09 [NEU: SQL parametrisiert]
		1.3 / 2004-01-09 [NEU: "setRootName()"]
		1.2 / 2004-01-05 [NEU: "write_tree_popup()"]
		1.1 / 2003-12-02
		1.0 / 2003-11-28
	*/
	
	// Class Vars
	var $oDb;						// DB-Objekt Referenz
	var $sTable = '';				// Tree-DB-Tabelle
	var $sCachefile = '';			// ggf. Cachefile
	var $aOptions = array(
		'sSqlConstraint'	=> '',
		'sOrderBy'			=> '',
		'aLang'				=> NULL,
		'sCurrentLang'		=> ''
	);								// ggf. Sql-Constraint / ORDER-BY / Languages... definieren
	var $aFields = array();			// auszulesende Felder der DB-Table
	
	var $flatArr = array();			// flaches Array, das so aus der DB gelesen wird
	var $treeArr = array();			// verschachteltes Array, das die Baumstruktur abbildet
	var $countFlatArr = 0;			// Gesamtanzahl Eintraege
	var $countEbenen = 0;			// Gesamtanzahl Ebenen (Verschachtelungstiefe)
	var $curPos = '';				// Aktuelle Position des Zeigers im flatArr
	
	var $sRootName = '--';			// Name des root-directory (fuer treeFormSelect())
	var $aAddOption = array();		// Array fuer zusaetzliche options (fuer treeFormSelect())!
	var $sSelectExtra = '';			// zusaetzliche Attribute (fuer treeFormSelect())!
	var $buffer = array();			// Buffer fuer "treeFormSelect()"
	var $sLang	= "";
	
	var $onlyAuthed = false;		// true, wenn das array von nicht-authorisierten daten befreit werden soll
	var $uid = 0;					// User, auf den die Daten authorisiert werden (Pflichtparameter, wenn $onlyAuthed = true)
	var $module = '';				// Modul in dem sich der Tree befindet. Wird benoetigt sofern die Standard/globale Tree Tabelle verwendet wird.
	var $_oTACEA = null;			// Rechteobject fuer Teams/User 
	var $_oSess = null;				// Sessionobject fuer Teams/User 
	var $debug = false;

/**
* Konstruktor -> Initialisiert das tree-object und zieht die Daten aus der DB.
* Wird als 5. Parameter true uebergeben, wird das ausgelesene Array noch auf den uebergebenen 
* User (6.Parameter) autorisiert und dieses "bereinigte" array in der Session gespeichert. 
* Wird als 7.Parameter true uebergeben, wird die Session aktualisiert.
*
* Beispiel: 
* <pre><code> 
* require_once("php/class.tree.php"); 
* $oTree =& new tree($oDb, $sTable); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
* </code></pre>
*
* @global	object	$oDb				Datenbank-Objekt, welches fuer die Klasse "class.db_mysql.php" benoetigt wird
* @param	string	$sTable				Content-Tabelle, die ausgelesen wird
* @param	string	[$sCachefile]		ggf. Cachefile fuer DB-Abfrage
* @param	string	[$module]			ggf. das Modul angeben, damit dies gesetzt werden kann (global tree)
* @param	array	[$aOptions]			OPTIONS (ggf. Sql-Constraint / OrderBy / Languages...)
* @access   public
*/
	function tree(&$oDb, $sTable, $sCachefile='', $module='', $aOptions=array()) {
		
		// DB-Objekt importieren
		if (!is_object($oDb)) {
			echo "<script>alert('".basename(__FILE__).": ".__CLASS__."-ERROR: \\n -> Es wurde kein DB-Objekt uebergeben!')</script>";
			return;
		}
		$this->oDb = $oDb;
		
		// Content-Tabelle definieren
		$this->sTable = $sTable;
		// ggf. Cachefile definieren
		if (!empty($sCachefile))	$this->sCachefile = $sCachefile;
		// ggf. module definieren
		if(!empty($module))			$this->module = $module;
		
		// OPTIONS
		if(strstr($_SERVER['PHP_SELF'], 'cms/')) {
			$this->aFields = array('id', 'parent_id', 'flag_navi'); // auszulesende Felder der DB-Tabelle definieren
		} else {
			$this->aFields = array('id', 'parent_id'); // auszulesende Felder der DB-Tabelle definieren
		}
		// ggf. Sprachen definieren
		if (isset($aOptions['aLang'])) { // wenn sprach(en) uebergeben wurden
			if (!is_array($aOptions['aLang']) && !empty($aOptions['aLang'])) {
				$aOptions['aLang'] = explode(',', $aOptions['aLang']); // ggf. string zu array wandeln
			}
			if (empty($aOptions['sCurrentLang'])) { // wenn Sprachen + current_lang fehlt...
				reset($aOptions['aLang']);
				$aOptions['sCurrentLang'] = current($aOptions['aLang']); // ...erste nehmen.
			}
			// auszulesende Titel-Felder ergaenzen
			foreach ($aOptions['aLang'] as $l) {
				$this->aFields[] = 'title_'.$l;
			}
		} else {
			// auszulesendes Titel-Feld ergaenzen
			$this->aFields[] = 'title';
		}
		// OrderBy definieren
		if (!isset($aOptions['sOrderBy']) || empty($aOptions['sOrderBy'])) {
			$aOptions['sOrderBy'] = (empty($aOptions['sCurrentLang'])) 
				? 'title ASC' 
				: 'title_'.$aOptions['sCurrentLang'].' ASC';
		}
		$this->aOptions = $aOptions;
		
		// pruefe ob table vorhanden
		#$this->_checkTable();
		
	}

	function setLang($sLang) {
		$this->sLang	= $sLang;
	}

/**
* @param	string	[$uid]				ggf. User-ID auf den authorisiert wird
* @param	object	[$oTACEA]			Team-Acces Object
* @param	object	[$oSess]			Session Object
*/
	function setAuth($uid, &$oTACEA, &$oSess) {
		// autorisierungsparams
		$this->onlyAuthed = true;
		$this->_oTACEA =& $oTACEA;
		if (!is_object($this->_oTACEA)) die(__CLASS__."-ERROR: _oTACEA ist kein Object!");
		$this->_oSess =& $oSess;
		if (!is_object($this->_oSess)) die(__CLASS__."-ERROR: _oSess ist kein Object!");
		$this->uid = $uid;
		if (empty($this->uid)) die(__CLASS__."-ERROR: uid ist empty!");
	}

# --------------------------------------------------------------------------------

/** Erstell-Funktion: Aufbau des Baumes und aller dazugehoerigen Variablen.
* (kann zum reseten genommen werden!)
* 
* @access   public
*/
	function buildTree() {
		// clear
		$this->flatArr = array();
		$this->treeArr = array();
		
		// personalisiert: verwende session
		if ($this->onlyAuthed) {
			
			$this->_oSess->unset_var($this->module.'Tree');
			
			if (!$this->_oSess->is_registered($this->module.'Tree')) {

				// baue flaches array
				$this->flatArr = $this->_buildFlatTree();
				// filtern
				$this->killTheNotAuthedFolder();
				// aktualisiere Session
				$this->_oSess->unset_var($this->module.'Tree');
				$this->_oSess->set_var($this->module.'Tree', $this->flatArr);
			} else {
				// nehme Daten aus der Session
				$this->flatArr = $this->_oSess->get_var($this->module.'Tree');
			}

		} else {
			// nicht persanalisiert: baue flaches array
			$this->flatArr = $this->_buildFlatTree();
		}
		
		// Anzahl Datensaetze ermitteln
		$this->countFlatArr = count($this->flatArr);
		// Verschachtelungstiefe ermitteln
		$this->_countEbenen();
		// kopiere array fuer tree
		$this->treeArr = $this->flatArr;
		// baue baum array
		$this->_moveChildren($this->treeArr, $this->treeArr);	#print_r($this->treeArr);
		
	}

/** Erstell-Funktion: Lese DB in ein flaches Array (schon in der richtigen Reihenfolge)
* 
* @access   private
* @param	string	[optional] wird von der Funktion selbst benötigt
* @param	string	[optional] wird von der Funktion selbst benötigt
* @param	int		[optional] wird von der Funktion selbst benötigt
* @param	int		[optional] wird von der Funktion selbst benötigt
* @return	array
*/
	function _buildFlatTree($db='', $buffer='', $parentId=0, $ebene=0) {
		// Ebene 0 Init
		if ($ebene == 0) {
			$buffer = Array();
			if (!$db) { $db = &$this->oDb; }
		}
		// schaue nach ob evtl. cachefile existiert
		if ($ebene == 0 && !empty($this->sCachefile) && file_exists($this->sCachefile)) {
			unset($buffer);
			$buffer = Array();
			include_once($this->sCachefile); // nur bei bedarf inkludieren!
		} else {
		// ansonsten array via db-abfrage erstellen
			if(!empty($this->module) && !strstr($this->aOptions['sSqlConstraint']," AND flag_tree='".$this->module."'")) $this->aOptions['sSqlConstraint'] .= " AND flag_tree='".$this->module."'";
			
			$sQuery = "SELECT ".implode(',', $this->aFields)." 
						FROM ".$this->sTable." 
						WHERE parent_id = ".$parentId."
						".$this->aOptions['sSqlConstraint']." 
						ORDER BY ".$this->aOptions['sOrderBy'];
			$lines = $db->fetch_in_Aarray($sQuery);
			foreach ($lines as $aData) {
				$tmp = array(); // felder in array kopieren
				foreach ($this->aFields as $field) {
					$tmp[$field] = $aData[$field];
				}
				$tmp['ebene'] = ($ebene+1); // virtuelles feld anhaengen
				$tmp['children'] = array(); // virtuelles feld anhaengen
				$buffer[] = $tmp;
				// rekoursiv weiterhangeln
				$this->_buildFlatTree(&$db, &$buffer, $aData['id'], ++$ebene); $ebene--;
			}
			// ggf. abfrage als array in cachedatei speichern
			if ($ebene == 0 && $this->sCachefile && count($buffer)) {
				$fp = fopen($this->sCachefile, "w");
				if (!$fp && $this->debug) { die(__CLASS__."-ERROR: cannot create sCachefile!"); }
				fputs($fp,'<?php'."\n");
				for ($i = 0; $i < count($buffer); $i++) {
					fputs($fp, '$buffer['.$i.'] = array('."\n");
					foreach($buffer[$i] as $k => $v) {
						if ($k == "children") {
							fputs($fp,'	"'.$k.'" => array()'."\n");
						} else {
							$v = str_replace('"', '\"', $v); // es darf keine nicht-escapeten doppelten anfuehrungszeichen geben!
							fputs($fp,'	"'.$k.'" => "'.$v.'",'."\n");
						}
					}
					fputs($fp,');'."\n");
				}
				fputs($fp,'?>'."\n");
				fclose($fp);
			}
		}
		// Ebene 0, Wert zurueckgeben
		if ($ebene == 0) {
			return $buffer;
		}
	}

/** Erstell-Funktion: Verschachtelungstiefe tracken
* 
* @access   private
*/
	function _countEbenen() {
		if ($this->countFlatArr == 0) return;
		
		for ($i = 0; $i < $this->countFlatArr; $i++) {
			if ($this->flatArr[$i]['ebene'] > $this->countEbenen) {
				$this->countEbenen = $this->flatArr[$i]['ebene'];
			}
		}
	}

/** Erstell-Funktion: checke ob DB-Table vorhanden, wenn nicht, starte Funktion zum Erstellen
* 
* @access   private
*/
	function _checkTable() {
		if (!$this->oDb) return;
		$tables = $this->oDb->get_tables();
		if (!empty($this->sTable) && !in_array($this->sTable, $tables)) {
			$this->_createTable();
		}
	}

/** Erstell-Funktion: erstelle DB-Table (minimum required fields)
* 
* @access   private
*/
	function _createTable() {
		if (empty($this->sTable)) return false;
		$sQuery		=  "CREATE TABLE `".$this->sTable."` ( 
						`id` int(11) NOT NULL auto_increment, 
						`parent_id` int(11) NOT NULL default '0', 
						`flag_navi` enum('0','1') NOT NULL default '1', ";
		// sprachabhaengige felder
		if (is_array($this->aOptions['aLang'])) {
			foreach ($this->aOptions['aLang'] as $l) {
				$sQuery	.=	"`title_".$l."` varchar(150) default NULL, ";
			}
		} else {
			$sQuery	.=	"`title` varchar(150) default NULL, ";
		}
		// prio oder nicht?
		$aOrderBy = explode(' ', $this->aOptions['sOrderBy']); // kommt "prio" im sql-orderby vor?
		if ($aOrderBy[0] == "prio") { // ggf. feld prio erzeugen
			$sQuery	.=	"`prio` int(11) NOT NULL default '0', ";
		}
		$sQuery		.=	"`created` varchar(14) default NULL, 
						`created_by` smallint(5) default NULL, 
						`last_modified` timestamp(14) NOT NULL, 
						`last_mod_by` smallint(5) default NULL, 
						PRIMARY KEY  (`id`) 
						) TYPE=MyISAM;";
		return $this->oDb->query($sQuery);
	}

/** Erstell-Funktion: Ermittelt rekursiv Kinder und verschiebt sie in Unter-Arrays
* (Re-strukturiert das Array neu, indem Kinder an ihre Eltern geknuepft werden)
* 
* @access   private
* @param	array	&$completeArray	Array als Referenz (das komplette flache Array, das von "_buildFlatTree()" zurueckgeliefert wird!)
* @param	array	&$childArray	Array als Referenz (der Zweig des Arrays, an den Kinder angehaengt werden - kann auch das ganze sein!)
* @return	void
*/
	function _moveChildren($completeArray, &$childArray) {
		if ($this->countFlatArr == 0) return;
		foreach ($childArray as $key => $val) {
			$temp = array();
			// Kinder ermitteln
			foreach ($completeArray as $k => $v) {
				if ($v['parent_id'] == $val['id']) {
					$temp[$k] = $v; // gefunden! -> in $temp zwischenspeichern
					unset($this->treeArr[$k]); // loesche Original
				}
			}
			$childArray[$key]['children'] = $temp; // Kinder neu zuweisen
			// und rekursiv weiterhangeln...
			$this->_moveChildren($completeArray, $childArray[$key]['children']);
		}
	}

/** Erstell-Funktion: Re-strukturiert das flat-Array neu, indem unauthorisierte Daten rausgefiltert werden.
* 
* @access   private
* @return	void
*/
	function killTheNotAuthedFolder() {
		// vars
		$newFlatArr = array();
		if (!is_array($this->flatArr) || count($this->flatArr) == 0) return;
		$aParentIdsToDelete = array();
		// code
		foreach ($this->flatArr as $treekey => $treeval) {
		 	// getRightForID() gibt false oder ein array zurueck
		 	$uright = $this->_oTACEA->getRightForID($treeval['id'], $this->module);
			if (is_array($uright)) {
   				// wenn array, dann schauen, ob uid bei den keys dabei ist...
   				foreach ($uright as $ukey => $uval) {
					if ($this->uid == $ukey) { // wenn ja, dann = authed
   						$newFlatArr[$treekey] = $treeval;
   					}
   				}
   				// 1. wenn einer rausgeschmissen wurde, diesen fuer spaeter merken...
   				if (!is_array($newFlatArr[$treekey])) {
   					$aParentIdsToDelete[] = $this->flatArr[$treekey]['id'];
   				}
   			} else { // dieser ist nicht personalisiert
	   			$newFlatArr[$treekey] = $treeval;	
   			}
		}
		// 2. wenn einer rausgeschmissen wurde, alle kinder + kindeskinder rausschmeissen!
		if (count($aParentIdsToDelete) > 0) {
			$aIdsToDelete = array();
			foreach($aParentIdsToDelete as $parentIdToDelete) {
				$aIdsToDelete = array_merge($this->getAllChildrenIdsByFlatArr($parentIdToDelete), $aIdsToDelete);
			}
			foreach ($newFlatArr as $treekey => $treeval) {
				if (in_array($treeval['id'], $aIdsToDelete)) {
					unset($newFlatArr[$treekey]);
				}
			}
		}
		// output
		$this->flatArr = $newFlatArr;
	}

# --------------------------------------------------------------------------------

/** Funktion zum Loeschen des Navi-Cache-Files
* 
* @access   public
*/
	function clearCache() {
		// ggf. kill session
		if (is_object($this->_oSess) && $this->_oSess->is_registered($this->module.'Tree')) {
			$this->_oSess->unset_var($this->module.'Tree');
		}
		// check vars
		if (empty($this->sCachefile)) {
			if ($this->debug) { die(__CLASS__."-ERROR: empty var sCachefile!"); } else { return; }
		}
		if (!file_exists($this->sCachefile)) {
			if ($this->debug) { die(__CLASS__."-ERROR: sCachefile does not exist!"); } else { return; }
		}
		// delete file
		@unlink($this->sCachefile);
	}

# --------------------------------------------------------------------------------

/** Ausgabe-Funktion: Gebe flaches Array aus
* 
* @access   public
* @return	array
*/
	function getFlatArray() {
		return $this->flatArr;
	}

/** Ausgabe-Funktion: Gebe verschachteltes Array aus
* 
* @access   public
* @return	array
*/
	function getTreeArray() {
		return $this->treeArr;
	}

/** Ausgabe-Funktion: Gebe Gesamtanzahl Eintraege aus
* Beispiel: 
* <pre><code> 
* echo "gefundene Datensaetze: ".$oTree->getCount(); 
* </code></pre>
* 
* @access   public
* @return	array
*/
	function getCount() {
		return $this->countFlatArr;
	}

/** Ausgabe-Funktion: Gebe Verschachtelungstiefe aus
* 
* @access   public
* @return	int
*/
	function getEbenen() {
		return $this->countEbenen;
	}

/** Ausgabe-Funktion: Gebe alle IDs als Array zurueck, NUR wenn "onlyAuthed==true"
* 
* @access   public
* @return	array (oder false)
*/
	function getAllAuthedIds() {
		if ($this->countFlatArr == 0 || !$this->onlyAuthed) return false;
		$buffer = array();
		$flatArr = $this->flatArr;
		foreach ($this->flatArr as $k => $v) {
			// unautorisierte subs rausschmeißen!
			foreach($flatArr as $key => $val) {
				if($val['id'] == $v['parent_id'] || $v['parent_id'] == 0) {
					$buffer[] = $v['id'];
					break;
				}
			}
			reset($flatArr);
		}
		return $buffer;
	}

/** Ausgabe-Funktion: Gebe Value des gesuchten Datensatzfeldes aus
* 
* @access   public
* @param	int		$id		ID des gesuchten Datensatzes
* @param	string	$field	Feldname des gesuchten Wertes
* @return	mixed	value oder false
*/
	function getValueById($id, $field) {
		if ($this->countFlatArr == 0) return;
		$arr = $this->flatArr;
		foreach ($arr as $key => $val) {
			if ($val['id'] == $id) {
				return $val[$field]; // gefunden!
				unset($arr);
				break;
			}
		}
		if (is_array($arr)) return false;
	}

# --------------------------------------------------------------------------------

/** Hilfs-Funktion: Gebe die Anzahl Sublevel des Datensatzes dessen ID uebergeben wurde. 
* 
* @access   public
* @param	string	$id			"needle in a haystack"
* @param	array	[optional]	-> INTERNER PARAMETER: wird von der Funktion selbst benötigt
* @param	int		[optional]	-> INTERNER PARAMETER: wird von der Funktion selbst benötigt
* @return	array
*/
	function countSublevel($id, $arr='', $ebene=0) {
		if ($this->countFlatArr == 0) return;
		if ($arr == '') {
			$arr = $this->treeArr;
			$this->temp = 0; // reset
		}
		// Verschachtelungstiefe tracken
		if ($ebene > $this->temp) {
			$this->temp = $ebene;
		}
		foreach ($arr as $key => $val) {
			if ($val['parent_id'] == $id) {
				$this->countSublevel($val['id'], $val['children'], ++$ebene); $ebene--; // auch die Kinder der gefundenen ID sammeln!
				if (count($val['children']) == 0) break(1); // = unten angekommen
			} else {
				$this->countSublevel($id, $val['children']); // ... ansonsten weiterhangeln...
			}
		}
		return $this->temp;
	}

/** Ausgabe-Funktion: Gebe die Kinder (und Kindeskinder)einer $parentId als Array zurueck
* Beispiel: 
* <pre><code> 
* $aStructure = $oTree->getAllChildren(); 
* foreach($aStructure as $k => $aData) { ... }
* </code></pre>
* 
* @access   public
* @param	string	[$parentId]	(optional) "needle in a haystack" - default: 0
* @param	array	[$arr]		-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function getAllChildren($parentId='0', $arr='') {
		if ($this->countFlatArr == 0) return array();
		if ($arr == '') {
			$arr = $this->treeArr;
			$this->temp = array(); // reset
		}
		if(is_array($arr)) {
		foreach ($arr as $key => $val) {
			if ($val['parent_id'] == $parentId) {
				$this->temp[$key] = $val; // gefunden! -> merken und am schluss zurueckgeben
			} else {
				$this->getAllChildren($parentId, $val['children']); // ... ansonsten weiterhangeln...
			}
		}
		return $this->temp;
		}
		else {
		
			return null;
			
		}
	}

/** Hilfs-Funktion: Gebe nur die IDs der Kinder (und Kindeskinder -> rekursiv!) 
* einer $parentId als flaches (numerisches) Array zurueck. 
* Beispiel: 
* <pre><code> 
* $aChildrenId = $oTree->getAllChildrenId(); 
* </code></pre>
* 
* @access   public
* @param	string	[$parentId]	(optional) "needle in a haystack" - default: 0
* @param	array	[$arr]		-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function getAllChildrenId($parentId='0', $arr='') {
		if ($this->countFlatArr == 0) return array();
		if ($arr == '') {
			$arr = $this->treeArr;
			$this->temp = array(); // reset
		}
		foreach ($arr as $key => $val) {
			if ($val['parent_id'] == $parentId) {
				$this->temp[] = $val['id']; // gefunden! -> ID merken und am schluss zurueckgeben
				$this->getAllChildrenId($val['id'], $val['children']); // auch die Kinder der gefundenen ID sammeln!
			} else {
				$this->getAllChildrenId($parentId, $val['children']); // ... ansonsten weiterhangeln...
			}
		}
		return $this->temp;
	}
/** Hilfs-Funktion: Gebe nur die IDs der Kinder (und Kindeskinder -> rekursiv!) 
* einer $parentId als flaches (numerisches) Array zurueck. 
* Diese Methode gibt das selbe Ergebnis zurueck wie "getAllChildrenId()", ist aber nicht so effektiv. 
* ALLERDINGS funktioniert sie auch bereits, wenn noch kein treeArr existiert!
* Wird z.B. bei "killTheNotAuthedFolder()" benoetigt!!!
* 
* @access   public
* @param	string	[$parentId]		(optional) "needle in a haystack" - default: 0
* @param	boolean	[$bFirstrun]	-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function getAllChildrenIdsByFlatArr($parentId='0', $bFirstrun=true) {
		if ($bFirstrun) $this->temp = array(); // reset
		if (count($this->flatArr) == 0) return $this->temp;
		foreach ($this->flatArr as $key => $val) {
			if ($val['parent_id'] == $parentId) {
				$this->temp[] = $val['id']; // gefunden! -> ID merken und am schluss zurueckgeben
				$this->getAllChildrenIdsByFlatArr($val['id'], false); // auch die Kinder der gefundenen ID sammeln!
			}
		}
		return $this->temp;
	}

/** Hilfs-Funktion: Gebe nur die IDs der Eltern (und Grosseltern -> rekursiv!) 
* einer $Id als flaches (numerisches) Array zurueck. 
* Beispiel: 
* <pre><code> 
* $aParentId = $oTree->getAllParentId($navi_id); 
* </code></pre>
* 
* @access   public
* @param	string	[$id]		Navigationspunkt-ID, dessen Ahnen ermittelt werden sollen
* @param	array	[$firstrun]		-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	array
*/
	function getAllParentId($id, $firstrun=true) {
		if ($this->countFlatArr == 0 || empty($id)) return array();
		if ($firstrun == true) {
			$this->temp = array(); // reset
		}
		foreach ($this->flatArr as $key => $val) {
			if ($val['id'] == $id && $val['parent_id'] == '0') { break; } // oben angekommen... schleife beenden
			if ($val['id'] == $id) { // gefunden!
				array_unshift($this->temp, $val['parent_id']); // -> ID an den Anfang haengen
				$this->getAllParentId($val['parent_id'], false);
			}
		}
		return $this->temp; // alle gesammelten IDs zurueckgeben
	}

/** Erstell-Funktion Funktion: Gebe die aktuelle Position des aktuellen Datensatzes zurueck
* 
* @access   private
* @param	int	$id		ID des aktuellen Datensatzes
* @return	int			Position im flatArr
*/
	function _getCurrentPosition($id) {
		if ($this->countFlatArr == 0) return;
		// ueberpruefe Zeiger und gebe ihn ggf. direkt zurueck
		if ($this->flatArr[$this->curPos]['id'] == $id) {
			return $this->curPos;
		} else {
		// ansonsten finde aktuellen Array-Eintrag
			reset($this->flatArr);
			while ($v = current($this->flatArr)) {
				if ($v['id'] == $id) {
					break;
				} else {
					next($this->flatArr);
				}
			}
			$this->curPos = key($this->flatArr); // aktuelle Position speichern...
			return $this->curPos; // ... und zurueckgeben
		}
	}

/** HTML-Ausgabe-Funktion: Gebe die aktuelle Position des aktuellen Datensatzes von allen Datensaetzen zurueck
* 
* @access   public
* @param	int	$id		ID des aktuellen Datensatzes
* @return	string		Position / Gesamtanzahl
*/
	function getPositionString($id) {
		return $this->_getCurrentPosition($id)+1 . " | " . $this->countFlatArr;
	}

/** Ausgabe-Funktion: Gebe die ID des vorigen Datensatzes zurueck
* 
* @access   public
* @param	int	$id		ID des aktuellen Datensatzes
* @return	int			ID des vorigen Datensatzes
*/
	function getPrev($id) {
		if ($this->countFlatArr == 0) return;
		// finde aktuellen Array-Eintrag
		$curr = $this->_getCurrentPosition($id);
		// gehe einen Schritt zurueck
		if ($curr-1 < 0) {
			return false; // wenn schon erster Eintrag
		} else {
			return $this->flatArr[$curr-1]['id']; // id zurueckgeben
		}
	}

/** Ausgabe-Funktion: Gebe die ID des naechsten Datensatzes zurueck
* 
* @access   public
* @param	int		$id		ID des aktuellen Datensatzes
* @return	mixed			ID des naechsten Datensatzes(int) || false (boolean)
*/
	function getNext($id) {
		if ($this->countFlatArr == 0) return;
		// finde aktuellen Array-Eintrag
		$curr = $this->_getCurrentPosition($id);
		// gehe einen Schritt weiter
		if ($curr+1 >= $this->countFlatArr) {
			return false; // wenn schon letzter Eintrag
		} else {
			return $this->flatArr[$curr+1]['id']; // id zurueckgeben
		}
	}

/** Ausgabe-Funktion: Gebe die ID des Eltern-Datensatzes zurueck
* 
* @access   public
* @param	int		$id		ID des aktuellen Datensatzes
* @return	mixed			ID des Eltern-Datensatzes(int) || false (boolean)
*/
	function getParent($id) {
		if ($this->countFlatArr == 0) return;
		// finde aktuellen Array-Eintrag
		$curr = $this->_getCurrentPosition($id);
		return $this->flatArr[$curr]['parent_id']; // id zurueckgeben
	}

/** Hilfs-Funktion: ermittle, ob der Datensatz mit der uebergebenen ID Kinder hat. 
* Beispiel: 
* <pre><code> 
* echo ($oTree->hasChildren($id)) ? 'hasChildren' : '-'; 
* </code></pre>
* 
* @access   public
* @param	string	$id	"needle in a haystack"
* @return	boolean	(true, wenn Kinder)
*/
	function hasChildren($id) {
		if ($this->countFlatArr == 0) return;
		// finde Array-Eintrag dessen parent-ID == der uebergebenen ID
		reset($this->flatArr);
		$hasChildren = false;
		while ($v = current($this->flatArr)) {
			if ($v['parent_id'] == $id) {
				$hasChildren = true;
				break;
			} else {
				next($this->flatArr);
			}
		}
		return $hasChildren;
	}

/** Ausgabe-Funktion: Suche (in flatArr) nach $needle (optional mit abgleich auf $key)
* Beispiel: 
* <pre><code> 
* echo ($oTree->isInArray(6, "parent_id")) ? "Einer hat parent_id=6" : "Keiner hat parent_id=6"; 
* </code></pre>
* 
* @access   public
* @param	string	$needle
* @param	string	[$checkkey]	(optional)
* @param	array	[$haystack]	-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	boolean
*/
	function isInArray($needle, $checkkey='', $haystack='') {
		if (empty($needle)) {
			if ($this->debug) { die(__CLASS__."-ERROR: empty var needle!"); } else { return; }
		}
		if ($haystack == '') $haystack=$this->flatArr;
		
		if (in_array($needle, $haystack)) {
			if ($checkkey != '') {
				foreach ($haystack as $key => $value) {
					if ($needle == $value && $checkkey == $key) {
						return true;
					}
				}
			} else {
				return true;
			}
		} else {
			while (list($tmpkey, $tmpval) = each($haystack)) {
				if (is_array($haystack[$tmpkey])){
					if ($this->isInArray($needle, $checkkey, $haystack[$tmpkey])) {
						return true;
					}
				}
			}
		}
		return false;
	}

#----------------------------------------------------------------------------- DROP-DOWN

/** Config-Funktion: Setze den Namen des "Root-Verzeichnisses" in der Ausgabe bei "treeFormSelect()"
* 
* @access   public
* @param	string	$sName		Angezeigter Name fuer die "root"
*/
	function setRootName($sName) {
		$this->sRootName = $sName;
	}

/** Config-Funktion: Unterbinde die Ausgabe des "Root-Verzeichnisses" in der Ausgabe bei "treeFormSelect()".
* (alle (Unter-)punkte in der ersten Ebene erhalten dadurch kein Prefix (-)!)
* 
* @access   public
*/
	function setRootOff() {
		$this->sRootName = '';
	}

/** Config-Funktion: Bestimme dass (bei "treeFormSelect()") eine Option vorne dran gesetzt wird.
* 
* @access   public
* @param	array	$aValue		assioziatives array (key = option-key, value = option-value)
*/
	function addOption($aValue, $bAllowDouble=true) {
		// ggf. ueberpruefung ob bereits im array vorhanden
		if (!$bAllowDouble && in_array($aValue, $this->aAddOption)) return;
		$this->aAddOption[] = $aValue;
	}

/** Config-Funktion: Entferne ggf. eine bereits gesetzte Option.
* 
* @access   public
* @param	array	$aValue		assioziatives array (key = option-key, value = option-value)
*/
	function removeOption($aValue) {
		$buffer = array();
		foreach ($this->aAddOption as $array) {
			if ($array == $aValue) continue;
			$buffer[] = $array;
		}
		$this->addOption = $buffer;
	}

/** Config-Funktion: Bestimme zusaetzliche Atrribute fuer das oeffnender Option-Tag (bei "treeFormSelect()").
* 
* @access   public
* @param	string	$sString		Zusaetzliche Atrribute fuer das oeffnender Option-Tag
*/
	function setSelectExtra($sString) {
		$this->sSelectExtra = $sString;
	}

/** Config-Funktion: Bestimme dass (bei "treeFormSelect()") eine Option fuer "all" angezeigt wird und setze den Namen.
* 
* @access   public
* @param	string	$sName		Angezeigter Name fuer "all" (wenn nicht gesetzt -> keine Option fuer "all"!)
*/
	function setAllName($sName) {
		$this->addOption(array("all" => $sName), false);
	}

/** Config-Funktion: Entferne ggf. eine bereits gesetzte Option fuer "all".
* 
* @access   public
* @param	string	$sName		Angezeigter Name fuer "all" (wenn nicht gesetzt -> keine Option fuer "all"!)
*/
	function unsetAllName() {
		$buffer = array();
		foreach ($this->aAddOption as $array) {
			$key = (string) key($array);
			if ($key != "all") {
				$buffer[] = $array;
			}
		}
		$this->aAddOption = $buffer;
	}

/** Ausgabe-Funktion: Tree-Array automatisiert (rekursiv) als Drop-Down-Formfeld ausgeben
* Beispiel: 
* <pre><code> 
* echo $oTree->treeFormSelect("dir", $actDir); // params: $name[,$currentId='']
* </code></pre>
* 
* @access   public
* @param	$name			Name-Attribut des FormFeldes
* @param	[$currentId]	(optional) ID des aktuellen Datensatzes (fuers highlighten)
* @param	[$arr]			-> INTERNER PARAMETER: sollte nie angegeben werden!
* @param	[$ebene]		-> INTERNER PARAMETER: sollte nie angegeben werden!
* @return	HTML
*/
	function treeFormSelect($name='', $currentId='', $arr='', $ebene=0) {

		$sLang	= $this->sLang;
		
		// vars
		if (empty($name)) {
			if ($this->debug) { die(__CLASS__."-ERROR: empty var name!"); } else { return; }
		}
		
		if ($arr == '') {#debug($this->treeArr);
			$arr = $this->treeArr;
		} 
		
		if (count($arr) == 0) return;
		// generate
		if ($ebene == 0) {
			$this->buffer = array();
			// start tag
			$this->buffer[] = '<select name="'.$name.'" size="1" '.$this->sSelectExtra.'>'."\n";
			// ggf. zusaetzliche options
			if (count($this->aAddOption) > 0) {
				foreach ($this->aAddOption as $k => $option) {
					$key = key($option);
					$sel = ((string)$currentId == (string)$key) ? " selected" : ''; // highlight
					$this->buffer[] = '<option value="'.$key.'"'.$sel.'>'.current($option)."</option>\n";
				}
			}
			// ggf. root folder
			if ($this->sRootName != '') {
				$sel = ($currentId == '0' || empty($currentId)) ? " selected" : ''; // highlight
				$this->buffer[] = '<option value="0"'.$sel.'>'.$this->sRootName."</option>\n";
			}
		}
		
		// tree
		foreach ($arr as $key => $val) {
			$sel = ($currentId == $val['id']) ? " selected" : ''; // highlight
			// prefix
			$prefix_repeat = ($this->sRootName != '') ? $ebene+1 : $ebene;
			$prefix = str_repeat("- ", $prefix_repeat);
			$sField	= (empty($sLang)) ? 'title':'title_'.$sLang;
			$this->buffer[] = '<option value="'.$val['id'].'"'.$sel.'>'.$prefix.$val[$sField]."</option>\n";
			
			// und rekursiv weiterhangeln...
			if (count($val['children'])) {
				$this->treeFormSelect($name, $currentId, $val['children'], ++$ebene); $ebene--;
			}
		}
		if ($ebene == 0) {
			// end tag
			$this->buffer[] = '</select>'."\n";
			// output
			return implode('', $this->buffer);
		}
	}


#-----------------------------------------------------------------------------
} // END of class

?>