<?php // Class basket
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung einen Warenkorb zu verwalten. 
* NOTE: benoetigt entweder die Klasse "class.session.php" bzw. ein Session-Objekt der Klasse "class.session.php" oder die Klasse "class.db_mysql.php"!
*
* Example: 
* <pre><code> 
* // init
* require_once("php/class.basket.php");
* $oBasket =& new basket();
* // ein Produkt dem Warenkorb hinzufuegen/ersetzen
* $oBasket->add($aData['id']); // params: $sProdId[,$sQuantity='1'] 
* // ein Produkt aus dem Warenkorb entfernen
* $oBasket->remove($aData['id']); // params: $sProdId 
* // wieviele verschieden Produkte sind derzeit im Warenkorb (ungeachtet ihrer Anzahl)?
* echo $oBasket->count(); // params: - 
* // gebe ALLE Produkte aus dem Warenkorb (als Array)
* $aBasket = $oBasket->getAll(); // params: - 
* // ersetze ALLE Produkte aus dem Warenkorb
* $aOLD = $oBasket->replaceAll($aNEW); // params: $aBasket 
* // loesche ALLE Produkte aus dem Warenkorb
* $aOLD = $oBasket->clearAll(); // params: - 
* </code></pre>
*
* @access   public
* @package  service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-10-20
*/
class basket {
	/*
	TODO: auf MySQL+Cookie umschaltbar machen
	*/
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse basket:
		----------------------------------------------------------------------------
		konstruktor basket($bStorageContainer='SESSION')
		function add($sProdId, $sQuantity='1')
		function remove($sProdId)
		function count()
		function countAll()
		function getAll()
		function replaceAll($aBasket)
		function clearAll()
		
		private _init_container($bStorageContainer)
		private _init_basket()
		private _store()
		----------------------------------------------------------------------------
		HISTORY:
		1.0 / 2004-10-19
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	string	Art der Datenhaltung [SESSION|MYSQL]
*/
	var $bStorageContainer = '';
/**
* @access   private
* @var	 	array	Container fuer alle Produkte im Warenkorb
*/
	var $aBasket = array();
/**
* @access   private
* @var	 	object	Container-Objekt bei SESSION
*/
	var $oSess = '';
/**
* @access   private
* @var	 	object	Container-Objekt bei MYSQL
*/
	var $oDb = '';
/**
* @access   private
* @var	 	string	Fehlermeldung
*/
	var $error_string = '';

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das basket-Objekt und interne Variablen. 
* Wenn der Container "SESSION" ist, wird nach einer Objektinstanz im globalen Namensraum namens $oSess gesucht. 
* (wenn ein Warenkorb eingesetzt wird, sollte schon eine Session existieren, in der der User gefuehrt wird...
*  Beispiel: $oSess =& new session("CLIENTNAME"); oder bei einer CUG: $oSess =& $oCug->startSession();  - ist diese nicht da, wird eine "frische" erezugt!)
*
* Beispiel: 
* <pre><code> 
* $oBasket =& new basket(); // params: [$bStorageContainer='SESSION') 
* </code></pre>
*
* @access   public
* @param 	string	$bStorageContainer	Art der Datenhaltung [SESSION|MYSQL] (default: SESSION)
* @return   string	$this->bStorageContainer
*/
	function basket($bStorageContainer='SESSION') {
		// init container
		$this->_init_container($bStorageContainer);
		// init basket
		$this->_init_basket();
		// output
		return $this->bStorageContainer;
	}

#-----------------------------------------------------------------------------

/**
* Fuegt ein Produkt dem Warenkorb hinzu. Optional kann die Anzahl (Menge) oder ein Wert (z.B. "DL") angegeben werden (default: 1). 
* Wenn als Anzahl "0" uebergeben wird, wird dieses Produkt aus dem Warenkorb entfernt!
* Wenn das Produkt schon im Warenkorb existiert, wird es ueberschrieben!
*
* Beispiel: 
* <pre><code> 
* $oBasket->add($aData['id']); // params: $sProdId[,$sQuantity='1'] 
* </code></pre>
*
* @access   public
* @param	string	$sProdId	Datensatz-ID des
* @param	string	$sQuantity	Anzahl/Wert (default: '1')
* @return   array	Inhalt des Warenkorbs vor dem Aufruf der Funktion
*/
	function add($sProdId, $sQuantity='1') {
		// check vars
		if (!$sProdId) return;
		// get old basket-data
		$buffer =& $this->aBasket;
		
		if ($sQuantity == '0' || $sQuantity == '') { // wenn KEINE $sQuantity
			// Artikel aus dem Array aBasket entfernen
			$this->remove($sProdId);
		} else {
			// Artikel dem Array aBasket hinzufuegen
			$this->aBasket[$sProdId] = $sQuantity;
			// Container speichern
			$this->_store();
		}
		// output
		return $buffer;
	}

/**
* Entfernt ein Produkt aus dem Warenkorb. 
*
* Beispiel: 
* <pre><code> 
* $oBasket->remove($aData['id']); // params: $sProdId 
* </code></pre>
*
* @access   public
* @param	string	$sProdId	Datensatz-ID des zu entfernenden Produkts
* @return   array	Inhalt des Warenkorbs vor dem Aufruf der Funktion
*/
	function remove($sProdId) {
		// check vars
		if (!$sProdId) return;
		// check basket
		if (!isset($this->aBasket[$sProdId])) return;
		// get old basket-data
		$buffer =& $this->aBasket;
		// remove
		unset($this->aBasket[$sProdId]);
		// Container speichern
		$this->_store();
		// output
		return $buffer;
	}

/**
* Anzahl verschiedener Produkte im Warenkorb (ungeachtet ihrer jeweiligen Anzahl). 
*
* Beispiel: 
* <pre><code> 
* echo $oBasket->count(); // params: - 
* </code></pre>
*
* @access   public
* @return   integer	Anzahl (verschiedener) Produkte aus dem Warenkorb
*/
	function count() {
		// count products
		return count($this->aBasket);
	}

/**
* Anzahl ALLER Produkte (Produkte mit ihrer jeweiligen Anzahl malgenommen). 
*
* Beispiel: 
* <pre><code> 
* echo $oBasket->countAll(); // params: - 
* </code></pre>
*
* @access   public
* @return   integer	Anzahl (verschiedener) Produkte aus dem Warenkorb
*/
	function countAll() {
		// count basket
		$count = 0;
		foreach ($this->aBasket as $k => $v) {
			if (($v+0) == $v) {	// wenn value==integer, dann plus anzahl
				$count += $v;
			} else {			// sonst plus 1
				$count ++;
			}
		}
		return $count;
	}

#-----------------------------------------------------------------------------

/**
* Gibt den kompletten Warenkorb als Array zurueck. 
*
* Beispiel: 
* <pre><code> 
* $aBasket = $oBasket->getAll(); // params: - 
* </code></pre>
*
* @access   public
* @return   array	Inhalt des Warenkorbs vor dem Ueberschreiben
*/
	function getAll() {
		// output basket
		return $this->aBasket;
	}

/**
* Speichert (ueberschreibt!) den kompletten Warenkorb. 
* Gibt den alten Inhalt des Warenkorbs (vor dem Ueberschreiben) zurueck.
*
* Beispiel: 
* <pre><code> 
* $aOLDbasket = $oBasket->replaceAll($aNEWbasket); // params: $aBasket 
* </code></pre>
*
* @access   public
* @param	array	$aBasket	Neuer Inhalt des Warenkorbs
* @return   array	Inhalt des Warenkorbs vor dem Ueberschreiben
*/
	function replaceAll($aBasket) {
		// check var
		if (!is_array($aBasket)) return;
		// get old basket-data
		$buffer =& $this->aBasket;
		// aBasket-Array ueberschreiben
		$this->aBasket = $aBasket;
		// Container speichern
		$this->_store();
		// return old basket-data
		return $buffer;
	}

/**
* Warenkorb leeren. 
*
* Beispiel: 
* <pre><code> 
* $oBasket->clearAll(); // params: - 
* </code></pre>
*
* @access   public
* @return   array	Inhalt des Warenkorbs vor dem Loeschen
*/
	function clearAll() {
		// get old basket-data
		$buffer =& $this->aBasket;
		// empty array
		$this->aBasket = array();
		// Container speichern
		$this->_store();
		// return old basket-data
		return $buffer;
	}

#-----------------------------------------------------------------------------

/** -->> IST ZU SPEZIELL!!!
*
* Alle Produkte aus dem Shop die im Warenkorb sind ermitteln. 
* Der Methode muss ein DB-Objekt uebergeben werden!
*
* Beispiel: 
* <pre><code> 
* $aProdInBasket = $oBasket->getBasketProducts($oDb, $weblang); // params: $oDb[,$sLang='de'] 
* </code></pre>
*
* @access   public
* @param	object	$oDb	DB-Objekt als Referenz
* @param	string	$sLang	Aktuelle Ausgabesprache
* @param	string	$which	[ALL|DL|NO_DL] (Alle Produkte|Nur Downloadprod.|Nur Versandprod.)
* @return   array	Inhalt des Warenkorbs en detail

	function getBasketProducts(&$oDb, $sLang='de', $which='ALL') {
		$buffer = array();
		if (!is_array($this->aBasket)) return;
		// DB-Objekt importieren
		if (!is_object($oDb)) {
			echo "<script>alert('".basename(__FILE__)."-ERROR: \\n -> Es wurde kein DB-Objekt uebergeben!')</script>";
			return;
		}
		// Nur bestimmte Produkte?
		if ($which == "DL") {
			$sConstraint = " AND shop_product_".$sLang." IS NOT NULL AND shop_product_".$sLang." != ''";
		} elseif ($which == "NO_DL") {
			$sConstraint = " AND (shop_product_".$sLang." IS NULL OR shop_product_".$sLang." = '')";
		} else {
			$sConstraint = '';
		}
		// Alle Detaildaten der Produkte im Warenkorb mitteles EINER Query ermitteln
		$sBasketId = implode(',', array_keys($this->aBasket));
		$oDb->query("SELECT s.id, s.navi_id, s.title_".$sLang." as title, s.price_net, s.tax, s.author, s.publisher, s.isbn, s.pages, s.ordernr, s.date, s.shop_product_".$sLang." as shop_product, s.preview_pdf_".$sLang." as preview_pdf, s.preview_image_".$sLang." as preview_image, 
							n.filename 
					FROM web_shop s, cms_navi n 
					WHERE s.id IN (".$sBasketId.") 
						AND s.flag_online_".$sLang."='1' 
						AND s.navi_id=n.id".$sConstraint);
		while ($tmp = $oDb->fetch_array()) {
			$buffer[$tmp['id']]['navi_id']		= $tmp['navi_id'];
			$buffer[$tmp['id']]['filename']		= $tmp['filename'];
			$buffer[$tmp['id']]['title']		= $tmp['title'];
			$buffer[$tmp['id']]['price_net']	= $tmp['price_net'];
			$buffer[$tmp['id']]['tax']			= $tmp['tax'];
			$buffer[$tmp['id']]['author']		= $tmp['author'];
			$buffer[$tmp['id']]['publisher']	= $tmp['publisher'];
			$buffer[$tmp['id']]['pages']		= $tmp['pages'];
			$buffer[$tmp['id']]['ordernr']		= $tmp['ordernr'];
			$buffer[$tmp['id']]['date']			= $tmp['date'];
			$buffer[$tmp['id']]['shop_product']	= $tmp['shop_product'];
			$buffer[$tmp['id']]['preview_pdf']	= $tmp['preview_pdf'];
			$buffer[$tmp['id']]['preview_image']= $tmp['preview_image'];
		}
		// Warenkorb-Detaildaten als Array zurueckgeben
		return $buffer;
	}*/

#-----------------------------------------------------------------------------

/**
* Initialisiert alle noetigen Vars fuer den gewaehlten Container [SESSION|MYSQL]. 
*
* @access   private
* @return   void
*/
	function _init_container($bStorageContainer) {
		$this->bStorageContainer = $bStorageContainer;
		// SESSION
		if ($this->bStorageContainer == 'SESSION') {
			// Zuerst versuchen ein bereits vorhandenes Session-Objekt zu importieren
			global $oSess;
			$this->oSess = $oSess;
			// Wenn keines vorhanden -> ein neues initialisieren
			if (!is_object($this->oSess)) {
				global $aENV;
				$name = (isset($aENV['client_shortname']) && !empty($aENV['client_shortname'])) ? $aENV['client_shortname'] : '';
				require_once($aENV['path']['global_service']['unix']."class.session.php");
				$this->oSess =& new session($name);
				#die('"'.basename(__FILE__).'"-ERROR: Es wurde kein globales Session-Objekt gefunden!');
			}
		}
		// MYSQL
		if ($this->bStorageContainer == 'MYSQL') {
			// TODO!
			return false;
				#require_once("class.db_mysql.php");
				#$this->oDb =& new dbconnect($aDbVars);
		}
	}

/**
* Initialisiert den Inhalt des Warenkorbs im gewaehlten Container [SESSION|MYSQL]. 
*
* @access   private
* @return   void
*/
	function _init_basket() {
		$this->aBasket = array();
		// SESSION
		if ($this->bStorageContainer == 'SESSION') {
			// schauen, ob es schon Daten in der Session gibt + ggf. Warenkorb damit fuellen
			$this->aBasket = $this->oSess->get_var("aBasket");
		}
		// MYSQL
		if ($this->bStorageContainer == 'MYSQL') {
			// TODO!
			return false;
		}
		if (count($this->aBasket == 0)) $this->_store();
	}

/**
* Speichert den Inhalt des Warenkorbs im gewaehlten Container [SESSION|MYSQL]. 
*
* @access   private
* @return   boolean
*/
	function _store() {
		if (!is_array($this->aBasket)) return;
		// SESSION
		if ($this->bStorageContainer == 'SESSION') {
			return ($this->oSess->set_var("aBasket", $this->aBasket)) ? true : false; // params: $sName,$value
		}
		// MYSQL
		if ($this->bStorageContainer == 'MYSQL') {
			// TODO!
			return false;
		}
	}


#-----------------------------------------------------------------------------
} // END of class

?>