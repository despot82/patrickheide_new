<?php
/**
 * Allgemeine Tool Klasse fuer Methoden, die sonst nirgendwo hinpassen
 * 
 * @author Andy Fehn <af@design-aspekt.com>
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @version 0.5
 * 
 * created: 02.01.2006
 * lastedit: 2007-11-26 undo magic quotes <da@design-aspekt.com>
 */

class Tools {
	// Von dieser Klasse kann keine Instanz erzeugt werden.
	function Tools() {
    }
    
	function printDebug($aArray, $sString='', $sColor='#000000') {
		echo (!empty($sString)) ? "<b>$sString</b>":'';
		echo "<span style=\"color:$sColor;\"<pre>";
		print_r($aArray);
		echo "</pre></span>";
	}    

/**
* Gibt true zurueck, wenn ein String (Needle) in einem Array oder kommaseparierten String enthalten ist. 
*
* Beispiel: 
* <pre><code> 
* $bool = Tools::isStringInHaystack($needle, $haystack);
* </code></pre>
*
* @access   public
* @param	string	$sString		Needle
* @param	mixed	$Haystack		Haystack
* @return	boolean	(true, wenn drin, sonst false)
*/
	function isStringInHaystack($sString, $Haystack) {
		// fehler! TODO: throw Exception => PHP 5
		if (empty($sString) || empty($Haystack)) {
			trigger_error('No String in $sString',E_USER_NOTICE);
			return false;
		}
		// do-it
		$aSepStr = (!is_array($Haystack)) ? explode(",", $Haystack) : $Haystack;
		return (in_array($sString, $aSepStr)) ? true : false;
	}

/**
 * Diese Methode uebersetzt Keywords aus der Schreibweise : key word = key,word oder key,word oder "key word" => key word,
 * @access public
 * @param String $keywords
 * @return String
 */
	function getValuesFromKeywords($keywords) {
		
		$keywords = str_replace(", "," ", $keywords);
		$keywords = str_replace(","," ", $keywords);
		$lastpos = 0;
		$keywords = str_replace('\\','',$keywords);
		// Kopie dringend beibehalten Original in $okeywords
		// Da $keywords in der Schleife veraendert wird!
		$okeywords = $keywords;
			
		for($i=0;strstr($keywords,'"');$i++) {
				
			$pos = strpos($keywords, '"');
			$other[$i] = substr($okeywords,$lastpos,$pos);
			$search = substr($keywords, ($pos+1));
				
			// Position um einen nach vorne setzen, damit das Anfuehrungszeichen nicht genommen wird
			$pos++;
			$endpos = strpos($search, '"');
			$buffer[$i] = str_replace('"','',substr($keywords,$pos,$endpos));
			$buffer[$i] = str_replace(' ','_',$buffer[$i]);
			$keywords = substr($search, ($endpos+1));
			// Letzte Position speichern - wichtig!
			$lastpos = ($endpos+$pos+1)+$lastpos;
		}
		
		$other[] = (substr($okeywords,$lastpos) != '"') ? substr($okeywords,$lastpos) : NULL;
		$icother = count($other);
		
		if($other[($icother-1)] == NULL) { 
			unset($other[($icother-1)]);
		}
		$puf = implode(' ',$other);
		$puf = str_replace(' ',',',$puf);
		if(is_array($buffer)) {
			$buffit = implode(' ',$buffer);
			$puf .= ','.str_replace(' ',',',$buffit);
		}
		$aPuf = explode(',',$puf);
		$iCount = count($aPuf);
		for($i=0;$iCount >= $i;$i++) {
			if(empty($aPuf[$i])) unset($aPuf[$i]);
		}
		$puf = implode(',',$aPuf);
		$puf = str_replace('_',' ',$puf);

		return $puf;
	}

/**
 * Achtung dies ist nur ein Wrapper für: $oLabel->getLabels.
 * Liefere alle verfuegbaren Labels fuer das jeweilige modul zurueck. 
 * 
 * Beispiel: 
 * <pre><code> 
 * $array = Tools::getLabels($oDb,$aENV,$flag_type,$parent);
 * </code></pre>
 * 
 * @deprecated 1.1 - 04.05.2006
 * @access public
 * @param object $oDb
 * @param array $aENV
 * @param String $flag_type (modul)
 * @param String parent
 * @return array
 */
	function getLabels(&$oDb,&$aENV,$flag_type,$parent) {
		$oLabel =& new Label($oDb,$aENV,$flag_type,$parent);
		return $oLabel->getLabels();
	}

/**
 * Diese Methode wandelt in der Datenbank abgespeicherte UTF 8 Strings in normale Ansi Strings um.
 * @access public
 * @param String $utf8string
 * @param array $aENV
 * @return ansi String
 */
	function convertFromUTF8($utf8string,$aENV=array()) {
		$oCC = new ConvertCharset();
		return $oCC->Convert($utf8string,'UTF-8','windows-1252');
	}

/**
 * Diese Methode wandelt Ansi Strings zu UTF 8 Strings um.
 * @access public
 * @param String $ansistring
 * @param array $aENV
 * @param array $pattern (optional) [num] => $val
 * @return ansi String
 */
	function convertToUTF8($ansistring,$aENV=array()) {
		$oCC = new ConvertCharset();
		return $oCC->Convert($ansistring,'windows-1252','UTF-8');
	}

/**
 * Diese Methode formatiert in der Datenbank abgespeicherte Keywords in einen String (inkl. Anker-Tag) um.
 * @param string $sTargetHref
 * @param array $aKeywords
 * @param array $aAddGetParams
 * @return ansi String
 */
	function formatKeywords($sTargetHref, $aKeywords, $aAddGetParams=array()) {
		$buffer = '';
		if (!is_array($aKeywords))  return $buffer;
		if (count($aAddGetParams) > 0) {
			foreach($aAddGetParams as $key => $val) {
				$sGetVars .= '&'.$key.'='.$val;
			}
		}
		list($sField,$sValue) = each($aKeywords);
		if(empty($sValue)) return $buffer;
		$buffer .= HR;
		$aKeywords = explode(',', $sValue);
		foreach ($aKeywords as $sKw) {
			$buffer .= '<a href="'.$sTargetHref.'?'.$sField.'='.$sKw.$sGetVars.'">'.$sKw.'</a> ';
		} // END foreach
		return $buffer;
	}

/**
 * Diese Methode gibt das schon fertig formatierte Bild (inkl. IMG-Tag) eines SYS- oder eines CUG-Users zurueck.
 * @param object $oMediadb
 * @param int $nUserId
 * @param int $nCugUserId=NULL
 * @param boolean $bWithDummy=false
 * @return String HTML
 */
	function getUserPicture(&$oMediadb, $nUserId, $nCugUserId=NULL, $bWithDummy=false) {
		// vars
		$buffer = '';
		if (empty($nUserId) && empty($nCugUserId))  return $buffer;
		global $aENV;
		
		if (!empty($nUserId)) {
			// SYS User
			global $oUser;
			// get userdata
			$aUserdata = $oUser->getUserdata($nUserId); // params: $userid
			if (!empty($aUserdata['picture'])) {
				$buffer = $oMediadb->getImageTag($aUserdata['picture'], ' width="40" height="40" border="0" class="userPict"'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)]
			}
			$type = 'sysuser';
		} else {
			// CUG User
			global $oCug;
			if (!is_object($oCug)) {
				global $syslang;
				require_once($aENV['path']['global_service']['unix'].'class.cug.php');
				$oCug =& new cug($aENV['db'], $syslang); // params: $aDbVars[,$sLang='de'][,$bPasswordRequired=false]
			}
			// get userdata
			$nUserId = $nCugUserId;
			$aUserdata = $oCug->getUser($nUserId); // params: $userid
			if (!empty($aUserdata['photo'])) {
				$buffer = $oMediadb->getImageTag($aUserdata['photo'], ' width="40" height="40" border="0" class="userPict"'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)]
			}
			$type = 'cuguser';
		}
		if ($bWithDummy && empty($aUserdata['photo']) && empty($aUserdata['picture'])) {
			$buffer = $oMediadb->getImageTag($aENV['path']['adr_data']['http'].'pix/dummy_'.$aUserdata['gender'].'.gif', ' width="40" height="40" border="0" class="userPict"'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)]
		}
		if (!empty($buffer)) {
			$buffer = '<a href="javascript:userinfoWin('.$nUserId.',\''.$type.'\')">'.$buffer.'</a>';
		}
		return $buffer;
	}

/**
 * Diese Methode gibt den schon fertig formatierten Namen eines SYS- oder eines CUG-Users zurueck.
 * @param object $oMediadb
 * @param int $nUserId
 * @param int $nCugUserId=NULL
 * @return String HTML
 */
	function formatUsername($nUserId, $nCugUserId=NULL) {
		if (empty($nUserId) && empty($nCugUserId))  return '';
		// get userdata
		if (!empty($nUserId)) {
			global $oUser;
			return $oUser->getUsername($nUserId); // params: $userid
		} else {
			if (empty($nCugUserId))  return '';
			global $oCug;
			$nUserId = $nCugUserId;
			return $oCug->getUserLoginName($nUserId); // params: $id[,$bValidOnly=false]
		}
	}

/**
 * Diese Methode gibt den schon fertig formatierten Date- und Time-String zurueck.
 * @param int $nUserId
 * @param int $sDateTime
 * @return String HTML
 */
	function formatDatetime($oDate, $sDateTime) {
		if (empty($sDateTime))  return '';
		$buffer = $oDate->date_from_mysql_timestamp($sDateTime);
		$buffer .= ' ['.$oDate->time_from_mysql_timestamp($sDateTime).']';
		return $buffer;
	}

/**
 * Diese Methode droeselt die Downloads fuer die Edit-Seite im entsprechenden Modul auf.
 * @access	public
 * @param	object	$oMdb
 * @param	object	$oDb
 * @param	string	$sDownloads
 * @return	array
 */
	function getDownloadsArray(&$oMediadb, &$oDb, $sDownloads) {
		global $aENV;
		$buffer = array();
		if (empty($sDownloads)) return $buffer;
		// define paths
		$aDlPath['pms']['unix']	= $aENV['path']['pms_archive']['unix'];
		$aDlPath['pms']['http']	= $aENV['path']['pms_archive']['http'];
		$aDlPath['mdb']['unix'] = $aENV['path']['mdb_upload']['unix'];
		$aDlPath['mdb']['http'] = $aENV['path']['mdb_upload']['http'];
		
		$aLines = explode("\n", $sDownloads);
		foreach ($aLines as $line) {
			if (!strstr($line, "||")) continue; // leere zeilen ueberspringen!
			list($dl_type, $dl_file, $dl_title) = explode("||", trim($line));
			if ($dl_type == 'mdb') {
				$aMediaFile = $oMediadb->getMedia($dl_file); // params: $nMediaId
				$aMediaFile['width']	= (!isset($aMediaFile['width']) || empty($aMediaFile['width']) || $aMediaFile['width'] < 20 || $aMediaFile['width'] > 1024) ? 500 : $aMediaFile['width'];
				$aMediaFile['height']	= (!isset($aMediaFile['height']) || empty($aMediaFile['height']) || $aMediaFile['height'] < 20 || $aMediaFile['height'] > 786) ? 400 : $aMediaFile['height'];
				
				$width = (!empty($aMediaFile['width'])) ? $aMediaFile['width'] : "''";
				$height = (!empty($aMediaFile['height'])) ? $aMediaFile['height'] : "''";
				
				$buffer[] = array(
					'type'		=> $dl_type,
					'id'		=> $dl_file,
					'filename'	=> $aMediaFile['filename'],
					'href'		=> "javascript:viewMediaWin('".$aMediaFile['id']."',".$width.",".$height.");",
					'src'		=> $aDlPath[$dl_type]['unix'].$aMediaFile['filename'],
					'title'		=> $dl_title
				);#OLD	'href'		=> $aDlPath[$dl_type]['http'].$aMediaFile['filename'],
			}
			elseif ($dl_type == 'pms') {
				$oDb->query("SELECT `id`,`comment`,`filename`,`pTitle`,`pAuthor`,`pDate`,`last_modified` 
							FROM `".$aENV['table']['pms_archive']."` 
							WHERE `id` = '".$dl_file."'");
				$aPmsFile = $oDb->fetch_array();
				$buffer[] = array(
					'type'		=> $dl_type,
					'id'		=> $dl_file,
					'filename'	=> $aPmsFile['filename'].'" target="_blank',
					'href'		=> $aDlPath[$dl_type]['http'].$aPmsFile['filename'],
					'src'		=> $aDlPath[$dl_type]['unix'].$aPmsFile['filename'],
					'title'		=> $dl_title
				);
			}
		} // END foreach
		return $buffer;
	}

/**
 * Diese Methode verbindet die Downloads aus der Edit-Seite vor dem DB-save in einen String.
 * @access	public
 * @param	object	$oMdb
 * @param	object	$oDb
 * @param	object	$oFile
 * @param	string	$sDownloads
 * @return	string
 */
	function formatDownloads(&$oMediadb, &$oDb, &$oFile, $sDownloads) {
		$buffer = '';
		if (empty($sDownloads))  return $buffer;
		// get dl-data as array
		$aDl = Tools::getDownloadsArray($oMediadb, $oDb, $sDownloads);
		foreach ($aDl as $dl) {
			// build string
			$buffer .= '<a href="'.$dl['href'].'">';
			$buffer .= $oMediadb->getMediaIcon($dl['filename']); // params: $sFilename
			$buffer .= $dl['title'].' <small>['.$oFile->get_filesize($dl['src'], 'AUTO', 0).']</small></a><br>'."\n";
		}
		return $buffer;
	}

/**
 * Diese Methode prueft, ob auf dem aktuellen System die PHP-Funktion "exec()", also das Ausführen von Systembefehlen erlaubt ist.
 * @access public
 * @return Boolean
 */
	function isExecAvail() {
		$ret = 1;
		$aVal = array();
		@exec('pwd',$aVal,$ret);
		return ($ret == 0) ? true : false;
	}

/**
 * Prüft die Konfiguration des Servers und liefert alle Relevanten Daten zurück
 * @access public
 * @return array
 */
	function getImportantInformation() {
		$info = array();
		$info['phpversion'] = phpversion();
		$info['mysqlversion'] = mysql_get_server_info();
		$info['savemode'] = (ini_get('safe_mode')) ? 0 : 1;
		$info['maxpostsize'] = ini_get('post_max_size');
		$info['maxuploadsize'] = ini_get('upload_max_filesize');
		$info['gdexists'] = (extension_loaded('gd')) ? 0 : 1;
		$info['deactfunc'] = ini_get('disable_functions');
		$info['exec'] = (Tools::isExecAvail()) ? 0 : 1;
		return $info;
	}

/**
 * Diese Methode loescht alle im uebergebenen Array enthaltenen MediaDB-files.
 * @access	public
 * @param	object	$oMdb
 * @param	array	$aDl
 * @return	array
 */
	function deleteAllUploadfiles(&$oMediadb, &$aDl) {
		if (!is_array($aDl) || count($aDl) < 1) return;
		$aMdbId = array();
		foreach($aDl as $k => $v) {
			if ($aDl[$k]['type'] != 'mdb') { continue; }
			$aMdbId[] = $aDl[$k]['file'];
		} // END foreach
		// make delete in MediaDB
		$oMediadb->moduleDelete($aMdbId); // params: $aMdbId
	}

/**
 * Diese Methode verbindet die Daten aus dem Upload-Modul aus der Edit-Seite vor dem DB-save in einen String.
 * @access	public
 * @param	object	$oMdb
 * @param	array	$aDl
 * @param	array	$aData
 * @param	string	$sModTitle (optional, default: '')
 * @return	string
 */
	function prepareUploadToolsData(&$oMediadb, &$aDl, &$aData, $sModTitle='') {
		/* SPECIAL: gehe durch array und baue title(s) + filename/ID(s) + title(s) zu definiertem string:
			1. dl-type || 1.dl-file || 1.dl-title \n
			2. dl-type || 2.dl-file || 2.dl-title \n
			...	(wobei dl-type "mdb" oder "pms" sein kann!)
		*/
		global $mdb_id_title, $pms_id_title; // nicht besonders elegant, aber geht nicht anders... :(
		$buffer = ''; // string der im DB-Feld "downloads" gespeichert wird
		$aMdbId = array();
		if (is_array($aDl)) { // $aDl = POST-array der vorhandenen file-ids
			foreach($aDl as $k => $v) {
				if (isset($aDl[$k]['delete'])) {
					// wenn delete-checkbox gecheckt, ggf. Media-ID zum loeschen-array hinzufuegen
					if ($aDl[$k]['type'] == 'mdb') {
						$aMdbId[] = $aDl[$k]['file'];
					}
				} else {
					// ansonsten string zusammenbauen
					if (!empty($aDl[$k]['file'])) {
						$aDl[$k]['title'] = str_replace('"', '&quot;', trim($aDl[$k]['title']));
						$buffer .= str_replace("\n", '', $aDl[$k]['type'].'||'.$aDl[$k]['file'].'||'.$aDl[$k]['title'])."\n";
					}
				}
			} // END foreach
			// ggf. make delete in MediaDB
			$oMediadb->moduleDelete($aMdbId); // params: $aMdbId
		} // END if
		
		// ggf. Neues File an String oben dran haengen
		if (isset($_FILES['uploadfile']['name'])) { // wenn upload (local file)
			$newUploadId = $oMediadb->moduleUpload('uploadfile', $aDl[0]['title'], $sModTitle); // params: $sUploadFieldname[,$sDescription=''][,$sKeywords='']
			if (!empty($newUploadId)) {
				$buffer = 'mdb||'.$newUploadId.'||'.$aDl[0]['title']."\n".$buffer;
			}
		}
		if (isset($aData['mdb_id']) && !empty($aData['mdb_id'])) { // wenn MediaDB-file
			if (empty($aDl[0]['title'])) { $aDl[0]['title'] = $mdb_id_title; }
			$buffer = 'mdb||'.$aData['mdb_id'].'||'.$aDl[0]['title']."\n".$buffer;
		}
		if (isset($aData['pms_id']) && !empty($aData['pms_id'])) { // wenn Presentation archive-file
			if (empty($aDl[0]['title'])) { $aDl[0]['title'] = $pms_id_title; }
			$buffer = 'pms||'.$aData['pms_id'].'||'.$aDl[0]['title']."\n".$buffer;
		}
		// nicht existierende db-felder vor dem speichern loeschen!
		unset($aData['mdb_id']);
		unset($aData['pms_id']);
		
		// return
		return $buffer;
	}
	
	/**
	 * 
	 * Diese Methode baut die Multipartfelder zusammen.
	 * @param string $sFieldname
	 * @param array $aData
	 * @return array
	 * 
	 */
	function combineMultipart($sFieldname,$aData) {
		
		if(isset($aData[$sFieldname.'_text_new']) && isset($aData[$sFieldname.'_mdb_new']) && !empty($aData[$sFieldname.'_text_new']) && !empty($aData[$sFieldname.'_mdb_new'])) {
			$sText = nl2br_cms($aData[$sFieldname.'_text_new']);
			$iMdb = $aData[$sFieldname.'_mdb_new'];
			$sAdd = $iMdb.'||'.$sText;
			unset($aData[$sFieldname.'_mdb_new']);
			unset($aData[$sFieldname.'_text_new']);
			unset($sText);
			unset($iMdb);
		} 
		foreach($aData as $key => $value) {
			$aParts = explode('_',$key);
			if(strstr($key,$sFieldname.'_mdb_')) {
				if(!empty($value)) {
					$sMdb[] = $value.'||';
				}
				unset($aData[$key]);
			}
			$sString = $sFieldname.'_mdb_'.$aParts[2];
			if(strstr($key,$sFieldname.'_text_')) {
				if(!empty($aData[$sString])) {
					$sText[] = str_replace("\n",'<br>',$value);
				}
				unset($aData[$key]);
			}
		}
		for($i=0;!empty($sMdb[$i]);$i++) {
			$sText2 .= $sMdb[$i].$sText[$i];
			if(!empty($sMdb[($i+1)]) || !empty($sAdd)) $sText2 .= "\n";
		}
		$sText2 .= $sAdd;
		$aData[$sFieldname] = $sText2;
		return $aData;
	}
	
	/**
	 * Strips slashes recursively from a variable (if array or object).
	 *
	 * @param mixed $var
	 * @return mixed
	 */
	function stripslashes_rec($var) {
		if (is_array($var)) {
			foreach ($var as $key => $val) {
				$var[$key] = Tools::stripslashes_rec($val);
			}
		} elseif (is_object($var)) {
			$obj_vars = get_object_vars($var);
			foreach ($obj_vars as $v) {
				$var->$v = Tools::stripslashes_rec($var->$v);
			}
		} else { // scalar
			$var = stripslashes($var);
		}
		return $var; 
	}
	
	/**
	 * Cleans up GET, POST, and COOKIE data if they have been tainted by 
	 * magic quotes.
	 * 
	 * After executing this method, $_GET, $_POST and $_COOKIE will not contain
	 * additional slashes.
	 */
	function undo_magic_quotes_gpc() {
		if (get_magic_quotes_gpc()) {
			global $_GET, $_POST, $_COOKIE;
			$_GET = Tools::stripslashes_rec($_GET);
			$_POST = Tools::stripslashes_rec($_POST);
			$_COOKIE = Tools::stripslashes_rec($_COOKIE);
		}
	}
	
	/**
	 * Escapes <, >, ", and & to Entities.
	 * @param string $string
	 * @return string
	 */
	function xmlescape($string) {
		return htmlspecialchars($string, ENT_COMPAT, 'UTF-8');
	}
}
?>