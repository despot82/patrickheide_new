<?php
/**
 * 
 * <pre>
 * 	$oMail = new SendMail('ansihtml');
 *  $oMail->setTo('fh@design-aspekt.com');
 *  $oMail->setFrom('fh@design-aspekt.com');
 *  $oMail->setSubject('Ansi Mail!');
 *  $oMail->setCharset('UTF-8');
 *  $oMail->setAnsitext('Dies ist der Ansitext der Mail.');
 *  $oMail->setHtmltext('<html><head></head><body>Dies ist der Htmltext der Mail.</body></html>');
 *  $oMail->initialize();
 *  $oMail->send();
 * </pre>
 * 
 * @param string $mailmode (ansi, html oder ansihtml)
 * @version 1.0
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 */
 
	class SendMail {
		
		var $to = '';
		var $subject = '';
		var $from = '';
		var $replyto = '';
		var $ansiText = '';
		var $htmlText = '';
		var $charset = 'ISO-8859-1';
		
		var $header = '';
		var $mailMode = 'ansi'; // ansi,html oder ansihtml
		var $aAttachment = null;
		var $compare = '/[a-z0-9_\-]+@[a-z0-9_\-]{2,}\.[a-z0-9_\-]{2,}/i';
		
		function SendMail($mailmode='ansi') {
			$this->mailMode = $mailmode;
		}
		
		/**
		 * 
		 * Set the Recipient
		 * @param string $to
		 * @access public
		 */
		function setTo($to) {

			if(preg_match($this->compare, $to)) {
				$this->to = $to;
			}
			else {
				trigger_error('Email doesn\'t match pattern!',E_USER_ERROR);
			}
		}
		
		/**
		 * 
		 * Set the ReplyTo
		 * @param string $replyto
		 * @access public
		 */
		function setReplyto($replyto) {

			if(preg_match($this->compare, $replyto)) {
				$this->replyto = $replyto;
			}
			else {
				trigger_error('Email doesn\'t match pattern!',E_USER_ERROR);
			}
		}
		/**
		 * 
		 * Set the dispatcher
		 * @param string $from
		 * @access public
		 */
		function setFrom($from) {
			if(preg_match($this->compare, $from)) {
				$this->from = $from;
			}
			else {
				trigger_error('Email doesn\'t match pattern!',E_USER_ERROR);
			}
		}
		
		/**
		 * 
		 * Set the Subject
		 * @param string $subject
		 * @access public
		 */
		function setSubject($subject) {
			$this->subject = $subject;
		}
		
		/**
		 * 
		 * Set the ansimailtext
		 * @param string $ansitext
		 * @access public
		 */
		function setAnsitext($ansitext) {
			$this->ansiText = wordwrap($ansitext,70);
		}
		
		/**
		 * 
		 * Set the Htmlmailtext
		 * @param string $htmltext
		 * @access public
		 */
		function setHtmltext($htmltext) {
			$this->htmlText = $htmltext;
		}
		
		/**
		 * 
		 * Set the Charset (UTF-8, ISO-8859-1)
		 * @param string $charset
		 * @access public
		 */
		function setCharset($charset='ISO-8859-1') {
			$this->charset = $charset;
		}
		
		/**
		 * 
		 * Initialize all header an prepare the text to be send.
		 * @access public
		 */
		
		function initialize() {
			$this->doTextReplacement();
			$this->setHeader();
		}
		
		function doTextReplacement() {
			// hier könnten Textreplacements durchgeführt werden.
		}
		
		/**
		 * 
		 * Set the headerinformation. Be sure, that the enviroment is set.
		 * @access public
		 */
		function setHeader() {
			if (strtoupper(substr(PHP_OS,0,3)=='WIN')) {
				$eol="\r\n";
			} elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) {
				$eol="\r";
			} else {
				$eol="\n";
			}
			$header  = "MIME-Version: 1.0".$eol;
			$header .= "X-Mailer: PHP v".phpversion().$eol;
			$header .= "From: ".$this->from.$eol;
			$header .= "Reply-To: ".$this->replyto.$eol;

			switch(true) {
				case ($this->mailMode == 'ansi') :
					$header .= "Content-type: text/plain; charset=".$this->charset.$eol;
					if($this->charset == 'ISO-8859-1') {
						$header .= $eol.Tools::convertFromUTF8($this->ansiText);
					}
					else {
						$header .= $eol.$this->ansiText;
					}
					break;
				
				case ($this->mailMode == 'html') :
					/* Um eine HTML-Mail zu senden, können Sie den "Content-type"-Header setzen */
					$header .= "Content-type: text/html; charset=".$this->charset.$eol;
					$header .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
					if($this->charset == 'ISO-8859-1') {
						$header .= Tools::convertFromUTF8($this->htmlText);
					}
					else {
						$header .= $this->htmlText;
					}
					break;
				
				case ($this->mailMode == 'ansihtml') :
					$mime_boundary=md5(time());
					$header .= "MIME-Version: 1.0\n";
					$header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\";".$eol;
					# Text Version
					$header .= "--".$mime_boundary.$eol;
					$header .= "Content-Type: text/plain; charset=".$this->charset.$eol;
					$header .= "Content-Transfer-Encoding: 7bit".$eol.$eol;
					if($this->charset == 'ISO-8859-1') {
						$header .= Tools::convertFromUTF8($this->ansiText).$eol.$eol;
					}
					else {
						$header .= $this->ansiText.$eol.$eol;
					}
									
					# HTML Version
					$header .= "--".$mime_boundary.$eol;
					$header .= "Content-Type: text/html".$eol;
					$header .= "Content-Transfer-Encoding: quoted-printable".$eol.$eol;
					if($this->charset == 'ISO-8859-1') {
						$header .= Tools::convertFromUTF8($this->htmlText).$eol.$eol; 
					}
					else {
						$header .= $this->htmlText.$eol.$eol;
					}
					$header .= "--".$mime_boundary."--".$eol.$eol;
					break;
					
				default :
					$header .= "Content-type: text/plain; charset=".$this->charset.$eol;
					break;
			}
			$this->header = $header;
		}
		/**
		 * 
		 * Derzeit nur ein Dummy.
		 * Add a new attachment to the email.
		 * @access public
		 */
		function addAttachment($sFilename) {
			$this->aAttachment[] = $sFilename;
		}
		
		/**
		 * 
		 * Send the mail
		 * @access public
		 */
		function send() {
			if(empty($this->to)) {
				trigger_error('No mailto defined!', E_USER_ERROR);
				$error = 1;
			}
			if(empty($this->from)) {
				trigger_error('No from defined!', E_USER_ERROR);
				$error = 1;
			}
			if(empty($this->subject)) {
				trigger_error('No subject defined!', E_USER_ERROR);
				$error = 1;
			}
			if($error == 1) 
				return false; 
			else {
				if(!$this->sendEmail()) {
					trigger_error('Mail dispatch has failed!',E_USER_ERROR);
					return false;
				}
				else {
					return true;
				}
			}
		}
		/**
		 * 
		 * main method to send email.
		 * @access private
		 */
		function sendEmail() {
			return mail($this->to,$this->subject,'',$this->header);
		}
		
		/**
		 * 
		 * Sendet eine Mail an eine Mailingliste
		 * @param array $aMailingList (num => $email)
		 * 
		 */
		function sendMailingList($aMailingList) {
			
			if(!is_array($aMailingList)) {
				trigger_error('The Mailinglist ist not an Array!',E_USER_ERROR);
				return false;
			}
			$this->initialize();
			for($i=0;!empty($aMailingList[$i]);$i++) {
				if(preg_match($this->compare,$aMailingList[$i])) {
					$this->setTo($aMailingList[$i]);
					$this->send();	
				}
			}
			
		}
		
	}
?>
