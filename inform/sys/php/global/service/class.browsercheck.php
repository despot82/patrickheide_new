<?php // Class browsercheck
/**
* This class provides browser- and platform-detection.
*
* Example: 
* <pre><code>
* // init
* $oBc =& new browsercheck;
* // who am I?
* echo "<b>OS:</b><br>\n";
* echo "isWin: ".($oBc->isWin() ? "<b>true</b>" : "false")."<br>\n";
* echo "isMac: ".($oBc->isMac() ? "<b>true</b>" : "false")."<br>\n";
* echo "isMacOSX: ".($oBc->isMacOSX() ? "<b>true</b>" : "false")."<br>\n";
* echo "isLinux: ".($oBc->isLinux() ? "<b>true</b>" : "false")."<br>\n";
* echo "isOs2: ".($oBc->isOs2() ? "<b>true</b>" : "false")."<br>\n";
* echo "<b>Browser:</b><br>\n";
* echo "isIE: ".($oBc->isIE() ? "<b>true</b>" : "false")."<br>\n";
* echo "isMacIE40: ".($oBc->isMacIE40() ? "<b>true</b>" : "false")."<br>\n";
* echo "isIE4: ".($oBc->isIE4() ? "<b>true</b>" : "false")."<br>\n";
* echo "isIE5: ".($oBc->isIE5() ? "<b>true</b>" : "false")."<br>\n";
* echo "isIE55: ".($oBc->isIE55() ? "<b>true</b>" : "false")."<br>\n";
* echo "isIE6: ".($oBc->isIE6() ? "<b>true</b>" : "false")."<br>\n";
* echo "isNS: ".($oBc->isNS() ? "<b>true</b>" : "false")."<br>\n";
* echo "isNN4: ".($oBc->isNN4() ? "<b>true</b>" : "false")."<br>\n";
* echo "isNN6: ".($oBc->isNN6() ? "<b>true</b>" : "false")."<br>\n";
* echo "isNN7: ".($oBc->isNN7() ? "<b>true</b>" : "false")."<br>\n";
* echo "isMoz: ".($oBc->isMoz() ? "<b>true</b>" : "false")."<br>\n";
* echo "isFirefox: ".($oBc->isFirefox() ? "<b>true</b>" : "false")."<br>\n";
* echo "isCamino: ".($oBc->isCamino() ? "<b>true</b>" : "false")."<br>\n";
* echo "isSafari: ".($oBc->isSafari() ? "<b>true</b>" : "false")."<br>\n";
* echo "isOmniWeb: ".($oBc->isOmniWeb() ? "<b>true</b>" : "false")."<br>\n";
* echo "isICab: ".($oBc->isICab() ? "<b>true</b>" : "false")."<br>\n";
* echo "isOpera: ".($oBc->isOpera() ? "<b>true</b>" : "false")."<br>\n";
* echo "isAOL: ".($oBc->isAOL() ? "<b>true</b>" : "false")."<br>\n";
* echo "isKonqueror: ".($oBc->isKonqueror() ? "<b>true</b>" : "false")."<br>\n";
* echo "isGaleon: ".($oBc->isGaleon() ? "<b>true</b>" : "false")."<br>\n";
* echo "isLynx: ".($oBc->isLynx() ? "<b>true</b>" : "false")."<br>\n";
* echo "isAmaya: ".($oBc->isAmaya() ? "<b>true</b>" : "false")."<br>\n";
* echo "isWebTV: ".($oBc->isWebTV() ? "<b>true</b>" : "false")."<br>\n";
* // show "HTTP_USER_AGENT"
* echo $oBc->getUserAgent();
* echo $oBc->getName();
* echo $oBc->getVersion();
* // IP-Adresse/Host/...
* echo "IP: ".$oBc->getIP();
* echo "Host: ".$oBc->getHost();
* echo "Referrer: ".$oBc->getReferrer();
* // COOKIE-TEST
* echo ($oBc->cookieTest() == true) ? "good" : "bad";
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.91 / 2006-03-23	[NEU: IE 7 zu Browserversionen hinzugefuegt]
*/

class browsercheck {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse browsercheck:
		----------------------------------------------------------------------------
		constructor browsercheck()
		
		function isIE()
		function isMacIE40()
		function isIE4()
		function isIE5()
		function isIE55()
		function isIE6()
		
		function isNS()
		function isNN4()
		function isNN6()
		function isNN7()
		
		function isMoz()
		function isFirefox()
		function isCamino()
		
		function isSafari()
		function isSafari10()
		function isSafari12()
		function isSafari13()
		function isSafari20()
		function isOmniWeb()
		function isICab()
		
		function isOpera()
		function isAOL()
		
		function isKonqueror()
		function isGaleon()
		function isLynx()
		function isAmaya()
		
		function isWebTV()
		
		function isWin()
		function isMac()
		function isMacOSX()
		function isLinux()
		function isOs2()
		
		function getUserAgent()
		function getName()
		function getVersion()
		
		function getIP()
		function getHost()
		function getReferrer()
		
		function cookieTest()
		
		TODO: Plugins (Flash etc. / JS-Version bzw. Faehigkeit)?
		----------------------------------------------------------------------------
		HISTORY:
		1.9 / 2006-01-17	[NEU: Browserversionen + "getName()" + "getVersion()"]
		1.8 / 2006-01-16	[NEU: "isWebTV()" + Linux Browser]
		1.7 / 2006-01-13	[NEU: "isIE4()" + "isIE5()" + "isIE55" + "isIE6"]
		1.6 / 2004-11-25	[NEU: "isMacOSX()" + "isCamino()" + "getIP/Host/Referrer()" + interne Umstellungen bei Vars und Konstruktor]
		1.5 / 2004-10-26	[NEU: "cookieTest()"]
		1.4 / 2004-03-10	[isNN6(),isNN7(),isFirefox() hinzugefuegt + isNS() ueberarbeitet]
		1.3 / 2003-08-28	[isAOL hinzugefuegt]
		1.2 / 2003-06-18	[isOpera & isSafari hinzugefuegt]
		1.11 / 2003-06-13	[nur kommentare fuer PHPDoc ueberarbeitet]
		1.1 / 2003-01-23
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	string	kompletter user-agent-string der vom browser geliefert wird
*/
	var $ua = '';
/**
* @access   private
* @var	 	string	Betriebssystem(-kuerzel)
*/
	var $os = 'unknown'; // [win|mac|linux]
/**
* @access   private
* @var	 	string	Browser(-kuerzel)
*/
	var $browser = 'unknown';
/**
* @access   private
* @var	 	string	Browser-Version
*/
	var $version = 'unknown';
/**
* @access   private
* @var	 	string	IP-Adresse des Clients
*/
	var $ip = 'unknown';

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das browsercheck-object
*
* Beispiel:
* <pre><code>
* $oBc =& new browsercheck();
* </code></pre>
*
* @access   public
*/
	function browsercheck() {
		// set internal vars
		$this->_get_useragent();
		$this->_get_ip();
		$this->_get_os();
		$this->_get_browser();
	}

/**
* Konstruktor-Hilfsfunktion: ermittelt den User-Agent-String und wndelt ihn in Kleinbuchstaben
* @access   private
*/
	function _get_useragent() {
		$ua = $_SERVER['HTTP_USER_AGENT'];
		if (empty($ua)) { $ua = getenv('HTTP_USER_AGENT'); }
		$this->ua	= strtolower($ua);
	}
/**
* Konstruktor-Hilfsfunktion: ermittelt die IP-Adresse
* @access   private
*/
	function _get_ip() {
		
/*  function tep_get_ip_address() {
    if (isset($_SERVER)) {
      if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }
    } else {
      if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      } elseif (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      } else {
        $ip = getenv('REMOTE_ADDR');
      }
    }
    $this->ip	= $ip;
  }*/
		$ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : '';
		if (empty($ip)) { $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''; }
		if (empty($ip)) { $ip = getenv('HTTP_CLIENT_IP'); }
		if (empty($ip)) { $ip = getenv('REMOTE_ADDR'); }
		if (empty($ip)) { $ip = 'unknown'; } // fallback (default)
		$this->ip	= $ip;
	}
/**
* Konstruktor-Hilfsfunktion: ermittelt das Betriebssystem
* @access   private
*/
	function _get_os() {
		if (empty($this->ua)) return;
		$os = 'unknown'; // set default
		if (strpos($this->ua, 'win')) {
			$os = 'win'; // Windows
		} elseif (strpos($this->ua, 'mac')) {
			$os = 'mac'; // Apple-Mac
			// OS 9 bzw. OS X laesst sich HIER NICHT ermitteln!!!
		} elseif (strpos($this->ua, 'linux') || strpos($this->ua, 'freebsd')) {
			$os = 'linux'; // Linux
		} elseif (strpos($this->ua, 'os/2')) {
			$os = 'os2'; // OS/2
		}
		$this->os	= $os;
	}
/**
* Konstruktor-Hilfsfunktion: ermittelt das Browserkuerzel (quasi die Browserfamilie ohne Version).
* @access   private
*/
	function _get_browser() {
		if (empty($this->ua)) return;
		$browser = 'unknown'; // set default
		$version = 'unknown'; // set default
		
		// Microsoft Internet Explorer
		if (strstr($this->ua, 'msie') || strstr($this->ua, 'iexplore')) {
			$browser = 'ie';
			if (strstr($this->ua, 'msie 3.')) {
				$version = '3';
			} elseif (strstr($this->ua, 'msie 4.')) {
				$version = '4';
			} elseif (strstr($this->ua, 'msie 5.')) {
				$version = '5';
				if (strstr($this->ua, 'msie 5.5')) {
					$version = '5.5';
				}
			} elseif (strstr($this->ua, 'msie 6.')) {
				$version = '6';
			} elseif (strstr($this->ua, 'msie 7.')) {
				$version = '7';
			}
		}
		// Netscape (alt)
		if ((strstr($this->ua, 'mozilla/4') && !strstr($this->ua, 'msie')) || strstr($this->ua, 'mozilla/3')) {
			$browser = 'ns';
			if (strstr($this->ua, 'mozilla/3')) {
				$version = '3';
			} elseif (strstr($this->ua, 'mozilla/4')) {
				$version = '4';
			}
		}
		// Gecko (Mozilla, etc.)
		if (strstr($this->ua, 'mozilla/5')) {
			$browser = 'gecko';
			if (strstr($this->ua, 'netscape6')) {
			// Netscape (ab Version 6)
				$browser = 'ns';
				$version = '6';
			} elseif (strstr($this->ua, 'netscape/7')) {
				$browser = 'ns';
				$version = '7';
			} elseif (strstr($this->ua, 'firefox')) {
			// Mozilla Firefox
				$browser = 'firefox';
				if (strstr($this->ua, 'firefox/1.0')) {
					$version = '1.0';
				} elseif (strstr($this->ua, 'firefox/1.5')) {
					$version = '1.5';
				}
			} elseif (strstr($this->ua, 'camino')) {
			// Camino (nur Mac OS-X)
				$browser = 'camino';
			} elseif (strstr($this->ua, 'galeon')) {
			// Galeon (nur Linux) 
				$browser = 'galeon';
			} else {
			// Mozilla
				$browser = 'moz';
				if (strstr($this->ua, 'rv:1.2')) {
					$version = '1.2';
				} elseif (strstr($this->ua, 'rv:1.7')) {
					$version = '1.7';
				}
			}
		}
		// KHTML (Gecko Compatible)
		if (strstr($this->ua, 'safari')) {
			$browser = 'safari'; // Safari (nur Mac OS-X) 
			if (strstr($this->ua, 'safari/85')) {
				$version = '1.0';
			} elseif (strstr($this->ua, 'safari/12')) {
				$version = '1.2';
			} elseif (strstr($this->ua, 'safari/31')) {
				$version = '1.3';
			} elseif (strstr($this->ua, 'safari/41')) {
				$version = '2.0';
			}
		}
		if (strstr($this->ua, 'omniweb')) {
			$browser = 'omniweb'; // OmniWeb (nur Mac OS-X) 
		}
		if (strstr($this->ua, 'konqueror')) {
			$browser = 'konqueror'; // Konqueror (nur Linux) 
		}
		
		// Sonstige Browser
		if (strstr($this->ua, 'icab')) {
			$browser = 'icab'; // iCab (nur Mac OS-X) 
		}
		if (strstr($this->ua, 'opera')) {
			$browser = 'opera'; // Opera
			if (strstr($this->ua, 'opera 5')) {
				$version = '5';
			} elseif (strstr($this->ua, 'opera/6')) {
				$version = '6';
			} elseif (strstr($this->ua, 'opera/7') || strstr($this->ua, 'opera 7')) {
				$version = '7';
			} elseif (strstr($this->ua, 'opera/8') || strstr($this->ua, 'opera 8')) {
				$version = '8';
				if (strstr($this->ua, 'opera/8.5') || strstr($this->ua, 'opera 8.5')) {
					$version = '8.5';
				}
			}
		}
		if (strstr($this->ua, 'libwww')) { // Linux Browser 
			if (strstr($this->ua, 'amaya')) {
				$browser = 'amaya'; // Amaya (nur Linux) 
			} else{
				$browser = 'lynx'; // Lynx (nur Linux) 
			}
		}
		if (strstr($this->ua, 'aol')) {
			$browser = 'aol'; // AOL
		}
		if (strstr($this->ua, 'webtv')) {
			$browser = 'webtv'; // WebTV
		}
		$this->browser	= $browser;
		$this->version	= $version;
	}

#----------------------------------------------------------------------------- BROWSER IE

/**
* prueft auf Internet Explorer
*
* prueft, ob der Browser ein MS Internet Explorer ist und gibt true (is IE) oder false (is not IE) zurueck
* Beispiel:
* <pre><code>
* if($oBc->isIE()) echo "Ich bin ein berliner");
* </code></pre>
*
* @access   public
* @return	boolean		[true|false]
*/
	function isIE() {
		return ($this->browser == 'ie') ? true : false;
	}
/**
* prueft auf Microsoft IE 4.0 mit Betriebssystem Apple
* @access   public
* @return	boolean		[true|false]
*/
	function isMacIE40() {
		return (strstr($this->ua, 'mozilla/4') && strstr($this->ua, 'msie 4.0') && isMac()) ? true : false;
	}
/**
* prueft auf Internet Explorer 4.x
* @access   public
* @return	boolean		[true|false]
*/
	function isIE4() {
		return ($this->isIE() && $this->version == '4') ? true : false;
	}
/**
* prueft auf Internet Explorer 5.x
* @access   public
* @return	boolean		[true|false]
*/
	function isIE5() {
		return ($this->isIE() && $this->version == '5') ? true : false;
	}
/**
* prueft auf Internet Explorer 5.5
* @access   public
* @return	boolean		[true|false]
*/
	function isIE55() {
		return ($this->isIE() && $this->version == '5.5') ? true : false;
	}
/**
* prueft auf Internet Explorer 6.x
* @access   public
* @return	boolean		[true|false]
*/
	function isIE6() {
		return ($this->isIE() && $this->version == '6') ? true : false;
	}
	

#----------------------------------------------------------------------------- BROWSER NETSCAPE

/**
* prueft auf Netscape
*
* prueft, ob der Browser ein Netscape ist und gibt true (is NS) oder false (is not NS) zurueck
* Beispiel:
* <pre><code>
* if($oBc->isNS()) echo "Ich bin ein hamburger");
* </code></pre>
*
* @access   public
* @return	boolean		[true|false]
*/
	function isNS() {
		return ($this->browser == 'ns') ? true : false;
	}
/**
* prueft auf Netscape Navigator 4.x
* @access   public
* @return	boolean		[true|false]
*/
	function isNN4() {
		return ($this->isNS() && $this->version == '4') ? true : false;
	}
/**
* prueft auf Netscape 6.x
* @access   public
* @return	boolean		[true|false]
*/
	function isNN6() {
		return ($this->isNS() && $this->version == '6') ? true : false;
	}
/**
* prueft auf Netscape 7.x
* @access   public
* @return	boolean		[true|false]
*/
	function isNN7() {
		return ($this->isNS() && $this->version == '7') ? true : false;
	}


#----------------------------------------------------------------------------- BROWSER MOZILLA

/**
* prueft auf Mozilla (Netscape 6+/Mozilla Suite/Firefox..) allgemein!
*
* prueft, ob der Browser ein Mozilla ist und gibt true (is Moz) oder false (is not Moz) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isMoz() {
		return (strpos($this->ua, 'mozilla')) ? true : false;
	}
/**
* prueft auf Mozilla Firefox (erster Name: Phoenix, dann: Firebird, dann Firefox)
* @access   public
* @return	boolean		[true|false]
*/
	function isFirefox() {
		return ($this->browser == 'firefox') ? true : false;
	}
/**
* prueft auf Mozilla Camino (basiert auf Firefox und gibt's nur auf Mac OS-X)
* @access   public
* @return	boolean		[true|false]
*/
	function isCamino() {
		return ($this->browser == 'camino') ? true : false;
	}
	

#----------------------------------------------------------------------------- BROWSER SPEZIELL

/**
* prueft auf Apple Safari (nur Mac OS-X)
*
* prueft, ob der Browser ein Apple Safari (basiert auf Konqueror und gibt's nur auf Mac OS-X) ist und gibt true (is Safari) oder false (is not Safari) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isSafari() {
		return ($this->browser == 'safari') ? true : false;
	}
/**
* prueft auf OmniWeb Browser (kostenpflichtiger Mac OS-X Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isSafari10() {
		return ($this->browser == 'safari' && $this->version == '1.0') ? true : false;
	}
/**
* prueft auf OmniWeb Browser (kostenpflichtiger Mac OS-X Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isSafari12() {
		return ($this->browser == 'safari' && $this->version == '1.2') ? true : false;
	}
/**
* prueft auf OmniWeb Browser (kostenpflichtiger Mac OS-X Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isSafari13() {
		return ($this->browser == 'safari' && $this->version == '1.3') ? true : false;
	}
/**
* prueft auf OmniWeb Browser (kostenpflichtiger Mac OS-X Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isSafari20() {
		return ($this->browser == 'safari' && $this->version == '2.0') ? true : false;
	}

/**
* prueft auf OmniWeb Browser (kostenpflichtiger Mac OS-X Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isOmniWeb() {
		return ($this->browser == 'omniweb') ? true : false;
	}
/**
* prueft auf OmniWeb Browser (kostenpflichtiger Mac OS-X Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isICab() {
		return ($this->browser == 'icab') ? true : false;
	}

/**
* prueft auf Opera
*
* prueft, ob der Browser ein Opera ist und gibt true (is Opera) oder false (is not Opera) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isOpera() {
		return ($this->browser == 'opera') ? true : false;
	}

/**
* prueft auf AOL-Browser
*
* prueft, ob der Browser ein AOL-Browser ist und gibt true (is AOL) oder false (is not AOL) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isAOL() {
		return ($this->browser == 'aol') ? true : false;
	}

/**
* prueft auf Konqueror (Linux-Browser)
*
* prueft, ob der Browser ein Konqueror (Linux) ist und gibt true (is Konqueror) oder false (is not Konqueror) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isKonqueror() {
		return ($this->browser == 'konqueror') ? true : false;
	}
/**
* prueft auf Galeon (Linux Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isGaleon() {
		return ($this->browser == 'galeon') ? true : false;
	}
/**
* prueft auf Lynx Browser (Linux Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isLynx() {
		return ($this->browser == 'lynx') ? true : false;
	}
/**
* prueft auf Amaya (Linux Browser)
* @access   public
* @return	boolean		[true|false]
*/
	function isAmaya() {
		return ($this->browser == 'amaya') ? true : false;
	}

/**
* prueft auf WebTV
*
* prueft, ob der Browser ein WebTV-Browser ist und gibt true (is WebTV) oder false (is not WebTV) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isWebTV() {
		return ($this->browser == 'webtv') ? true : false;
	}

#----------------------------------------------------------------------------- BETRIEBSSYSTEME

/**
* prueft auf Betriebssystem Windows
*
* prueft, ob der Browser auf einem Windows Betriebssystem laeuft und gibt true (is Win) oder false (is not Win) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isWin() {
		return ($this->os == 'win') ? true : false;
	}

/**
* prueft auf Betriebssystem Apple Macintosh
*
* prueft, ob der Browser auf einem Apple Mac Betriebssystem laeuft und gibt true (is Mac) oder false (is not Mac) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isMac() {
		return ($this->os == 'mac') ? true : false;
	}

/**
* prueft auf Betriebssystem Apple Macintosh OS X
*
* prueft, ob der Browser auf einem Apple Mac Betriebssystem OS X laeuft und gibt true (is MacOSX) oder false (is not MacOSX) zurueck
* Notes: 
* - Der erste IE auf OS X war 5.2, alle davor gab es nur bis OS 9. 
* - Moz wurde nur bis 1.0.2 auf OS9 vertrieben; der erste NN/Moz war NN6.1(en) bzw. NN6.2. 
* - Bei Moz/NN steht "OS X" in der Kennung. 
* - Safari + Camino + Firefox gibt es nur ab OS X.
*
* @access   public
* @return	boolean		[true|false]
*/
	function isMacOSX() {
		if (!$this->isMac()) return false;
		if (	$this->isSafari() || $this->isFirefox() || $this->isCamino() || $this->isOmniWeb() || $this->isICab() 
				|| strstr($this->ua, 'mac os x') || strstr($this->ua, 'mac 10') 
				|| ($this->isIE() && strstr($this->ua, 'msie 5.2'))
			) {
			return true;
		} else {
			return false;
		}
	}

/**
* prueft auf Betriebssystem Linux
*
* prueft, ob der Browser auf einem Linux Betriebssystem laeuft und gibt true (is Linux) oder false (is not Linux) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isLinux() {
		return ($this->os == 'linux') ? true : false;
	}

/**
* prueft auf Betriebssystem OS/2
*
* prueft, ob der Browser auf einem OS/2 Betriebssystem laeuft und gibt true (is OS/2) oder false (is not OS/2) zurueck
*
* @access   public
* @return	boolean		[true|false]
*/
	function isOs2() {
		return ($this->os == 'os2') ? true : false;
	}

#----------------------------------------------------------------------------- USER AGENT

/**
* gibt den kompletten Erkennungsstring des Browsers zurueck
*
* Beispiel:
* <pre><code>
* echo $oBc->getUserAgent();
* </code></pre>
*
* @access   public
* @return	string		komplette Browserkennung
*/
	function getUserAgent() {
		return $this->ua;
	}

/**
* gibt den Namen des Browsers zurueck
*
* Beispiel:
* <pre><code>
* echo $oBc->getName();
* </code></pre>
*
* @access   public
* @return	string		Browsername
*/
	function getName() {
		return $this->browser;
	}

/**
* gibt die Version des Browsers zurueck
*
* Beispiel:
* <pre><code>
* echo $oBc->getVersion();
* </code></pre>
*
* @access   public
* @return	string		Browsername
*/
	function getVersion() {
		return $this->version;
	}

#----------------------------------------------------------------------------- IP/HOST

/**
* gibt die IP-Adresse des Clients zurueck
*
* Beispiel:
* <pre><code>
* echo $oBc->getIP();
* </code></pre>
*
* @access   public
* @return	string		IP-Adresse
*/
	function getIP() {
		return $this->ip;
	}

/**
* gibt die Host-Adresse des Clients zurueck (sofern verfuegbar!)
*
* @access   public
* @return	string		Host-Adresse
*/
	function getHost() {
		return gethostbyaddr($this->ip);
	}

/**
* gibt die vom Client zuvor besuchte Seite zurueck (sofern verfuegbar!)
*
* @access   public
* @return	string		IP-Adresse
*/
	function getReferrer() {
		$referer = $_SERVER['HTTP_REFERER'];
		if (empty($referer)) { $referer = getenv('HTTP_REFERER'); }
       return (!empty($referer)) ? $referer : NULL;
	}

#----------------------------------------------------------------------------- COOKIE

/**
* fuehrt einen cookie-test durch und gibt true zurueck, wenn der client cookies akzeptiert, ansonsten false. 
* Dazu gibt es beim ersten Aufruf der Seite (und immer wieder wenn man auf diese Seite kommt 
* und KEINE cookies akzeptiert werden) einen Reload! Wenn hingegen cookies akzeptiert werden, steht die Information 
* ohne reload solange zur Verfuegung bis der Browser geschlossen wird. 
* ACHTUNG: wenn kein output-buffering eingesetzt wird, muss diese Methode VOR jeder HTML-Ausgabe erfolgen, da ein header gesendet wird!!!
*
* Beispiel:
* <pre><code>
* echo ($oBc->cookieTest() == true) ? "good" : "bad";
* </code></pre>
*
* @access   public
* @return	boolean		true, wenn ein cookie geschrieben werden kann, sonst false
*/
	function cookieTest() {
		if (!isset($_COOKIE['cookieTest']) && !isset($_GET['cookie'])) {
			// versuche ein cookie zu schreiben
			setCookie('cookieTest', 'test');
			// reload
			header('Location: '.basename($_SERVER['PHP_SELF']).'?'.$_SERVER['QUERY_STRING'].'&cookie=test'); exit;
		}
		// output (erst nach dem reload!)
		return (!isset($_COOKIE['cookieTest']) && isset($_GET['cookie'])) ? false : true;
	}

#-----------------------------------------------------------------------------
} // END of class
?>