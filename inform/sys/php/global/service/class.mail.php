<?
/*----------------------------------------------------------
	AMAN_RedSYS(R) - Fileinfo:
	$Author: juergen $
	$Source: /home/cvs/redsys/kernel/system/inc/mail.class.php,v $
	$Revision: 1.18 $
	
	Copyright (C) AMAN Media GmbH 2000 - 2003
	http://www.aman.de | http://www.redsys.de

---------------------------------------------------------*/

/**
* Sende HTML-Mail mit alternativem (nur)Textmail (wird automatisch erzeugt)
* <pre><code>$mail->body = "<html><body><h1>Dies ist ein HTML-Mail</h1></body></html>";</code></pre>
*/
define("MIMEMAIL_HTML_ALTERNATIVE",100);
/**
* Sende HTML-Mail
* <pre><code>$mail->body = "<html><body><h1>Dies ist ein HTML-Mail</h1></body></html>";</code></pre>
*/
define("MIMEMAIL_HTML",102);
/**
* Sende normales Email(nur Text)
* <pre><code>$mail->body = "Dies ist ein normales Text-Mail";</code></pre>
*/
define("MIMEMAIL_TEXTONLY",101);
/**
* Verwende 8BIT Encoding zum Mailversand 
* kann bei Anhaenge und Teile des Mails, abhaengig vom Empfaenger unleserlich machen
*/
define("MIMEMAIL_ENCODING_8BIT","8bit");
/**
* Verwende 7BIT Encoding zum Mailversand 
* kann bei Anhaenge und Teile des Mails, abhaengig vom Empfaenger unleserlich machen
*/
define("MIMEMAIL_ENCODING_7BIT","7bit");
/**
* Verwende BASE64 Encoding zum Mailversand 
* ist die sicherste Methode erzeugt aber auch die groesste Datenmenge und
* funktioniert nicht einwandfrei mit 'MIMEMAIL_HTML_ALTERNATIVE' 
* (manche Mailclients bevorzugen dann den Textteil der Nachricht)
*/
define("MIMEMAIL_ENCODING_BASE64","base64");
/**
* Verwende quoted-printable Encoding zum Mailversand
* funktioniert mit 'MIMEMAIL_HTML_ALTERNATIVE' 
* aber nicht einwandfrei in Verbindung mit T-Online
*/
define("MIMEMAIL_ENCODING_QUOTED","quoted-printable");

/**
* Versendet Emails in unterschiedlichen Formaten, mit und ohne Anhaenge
* 
* Beispiel:
* <pre><code>
* $attachment = fread(fopen( "mail.php3",  "r"), filesize( "mail.php3")); 
* 
* $mail = new mime_mail(MIMEMAIL_TEXTONLY);
* $mail -> from =  "foo@bar.com";
* $mail -> headers =  "Errors-To: foo@bar.com";
* $mail -> to =  "info@ticinonline.to";
* $mail -> subject =  "Testing...";
* $mail -> MailbodyEncoding = MIMEMAIL_ENCODING_8BIT;
* $mail -> body =  "This is just a test.";
* $mail -> add_attachment( "$attachment",  "mail.php3",  "text/plain", MIMEMAIL_ENCODING_BASE64);
* if($mail -> send())
* 	print( "Mail sent.");
* </code></pre>
*
* von PHP.NET --- Posting eines users:
*	There used to be a note with a complete set of headers to use with the mail function but somehow now its gone so here they are again hope it helps :)
*	
*	$headers .= "From: Name <mail@server.com>\n";
*	$headers .= "X-Sender: <mail@server.com>\n";
*	$headers .= "X-Mailer: PHP\n"; //mailer
*	$headers .= "X-Priority: 3\n"; //1 UrgentMessage, 3 Normal
*	$headers .= "Return-Path: <mail@server.com>\n";
*	//Uncomment this to send html format
*	//$headers .= "Content-Type: text/html; charset=iso-8859-1\n";
*	//$headers .= "cc: birthdayarchive@php.net\n"; // CC to
*	//$headers .= "bcc: mail@server.com"; // BCCs to, separete multiple with commas mail@mail.com, mail2@mail.com
*
* <pre>Ergaenzung von Andy Fehn: 
*	List of common mime-types:
*	application/msword				*.doc *.dot							Microsoft Word Dateien
*	application/octet-stream		*.bin *.exe *.com *.dll *.class		Ausfuehrbare Dateien
*	application/pdf					*.pdf								Adobe PDF-Dateien
*	application/x-httpd-php			*.php *.phtml						PHP-Dateien
*	application/x-shockwave-flash	*.swf *.cab							Flash Shockwave-Dateien
*	application/zip					*.zip								ZIP-Archivdateien
*	audio/x-pn-realaudio			*.ram *.ra							RealAudio-Dateien
*	image/gif						*.gif								GIF-Dateien
*	image/jpeg						*.jpeg *.jpg *.jpe					JPEG-Dateien
*	text/comma-separated-values		*.csv								komma-separierte Datendateien
*	text/css						*.css								CSS Stylesheet-Dateien
*	text/html						*.htm *.html *.shtml				HTML-Dateien
*	text/javascript					*.js								JavaScript-Dateien
*	text/plain						*.txt								reine Textdateien
*	video/mpeg						*.mpeg *.mpg *.mpe					MPEG-Dateien
*	video/quicktime					*.qt *.mov							Quicktime-Dateien
*	MP3 ???
*	
*	(almost) complete list:
*	http://selfhtml.teamone.de/diverses/mimetypen.htm
* </pre>
*
* @access   public
* @package  Content
*/ 
class mime_mail {
	var $parts = array();
	var $to = "such@aman.de";
	var $from = "";
	var $headers = "";
	var $subject = "";
	var $enctype = "";
	var $body = "";
	var $ishtmlbody = false;
	var $wordwrap = 75;
	var $MailbodyEncoding = "quoted-printable";
	var $priority = Array(0=>"High", 3 => "Normal", 6 => "Low");
	var $priorityvalue = "Normal";
	var $parseEmailadresses = false;
	/**wenn die emailadresse diesen match nicht uebersteht, wird nicht versucht zu versenden*/
	var $emailcheck = '/[a-z0-9_\-]+@[a-z0-9_\-]{2,}\.[a-z0-9_\-]{2,}/i';
	/**Wenn ein String nicht gemacht wird, wird er mit quotet-printable encodiert*/
	var $emailisocheck = '/[a-z0-9 _\-]/i';
	/**
	* CVS - Dateiversion
	* @access 	private
	*/
	var $fileVersion;
/**
* Konstruktor
* 
* @access	public
* @param	const	Konstante zur Bestimmung des Typs von Mailbody
* @param	integer	Anzahl von Zeichen in einer Zeile, ab denen ein 
					automatischer Zeilenumbruch vorgenommen wird
*/
function mime_mail($const=0,$wrap = 75) {
 	$this->mailbodyConst = $const;
	$this->ishtmlbody = ($const==MIMEMAIL_HTML_ALTERNATIVE || $const==MIMEMAIL_HTML)?true:false;
	$this->wordwrap = $wrap;
	// CVS
	$this -> fileVersion = Array(
		"revision" => '$Revision: 1.18 $', 
		"changedate" => '$Date: 2002/11/22 10:16:55 $',
		"source" => '$Source: /home/cvs/redsys/kernel/system/inc/mail.class.php,v $'
	);
}

/**
* Liefert die Revisionsnummer der Klasse, oder alternativ die komplette Versionsinformation, bestehend aus Datum, Revision und Dateiname
* 
* @access	public
* @param	boolean	$numeric	numerischen Wert der Revision, oder assoc. Array mit erweiterten Informationen liefern
* @return	mixed	Informationen ueber die Dateiversion
*/
	function getVersion($numeric=true) {
		if(!$this -> fileVersion["filtered"]) {
			foreach($this -> fileVersion as $key => $value)
				if(preg_match('/^\$[^:]*:(.*)\$$/ism', $value, $matches))
					$this -> fileVersion[$key] = trim($matches[1]);
			$this -> fileVersion["filtered"] = true;
		}
		return ($numeric ? doubleval($this -> fileVersion["revision"]) : $this -> fileVersion);
	}

/**
* Schaltet das Parsen von Emailadressen ein, bzw. aus (Standard: aus)
* Hinsweis:
* Wenn diese Option eingeschaltet ist, darf der "to" Parameter folgende Formate haben:
* - Array(Array("Empfaenger Name", "email@domain.de"),...)
* - Array("email@domain.de",...)
* - email@domain.de, ...
* - <email@domain.de>, ....
* - Empfaenger Name <email@domain.de>, ...
* Aus einem dieser Vorgaben wird der korrekte To-Header erzeugt und die Emailadressen auf
* plausibilitaet geprueft. Andernfalls muessen Sie den To-Header selbst erzeugen, 
* im einfachsten fall einfach nur eine Emailadresse
* 
* @access	public
* @param	boolean $enable	true/false
*/
function enableParseEmailadresses($enable = true) {
	$this -> parseEmailadresses = $enable;
}

/**
* Setzt die Prioritaet der Email (Normal/High/Low)
* 
* @access	public
* @param	mixed	$prio	Prio-Nummer oder Wort (0-6 oder Normal/High/Low)
*/
function setPriority($prio="Normal") {
	if(in_array($prio, $this -> priority))
		$this -> priorityvalue = $prio;
	if(isset($this -> priority[$prio]))
		$this -> priorityvalue = $this -> priority[$prio];
}

/**
* Fuegt dem Mail einen Anhang hinzu.
* $message muss alle Daten des Anhangs enthalten.
*
* @access	public
* @param	string	Daten des Anhangs
* @param	string	Dateiname (wird im Email dann als Dateiname des Anhangs angezeigt)
* @param	string	Mime-Typ des Anhangs (im Zweifel Standardwert verwenden)
* @param	string	Zu verwendendes Encoding fuer diesen Anhang (siehe Konstanten)
*/ 
function add_attachment($message, $name =  "", $ctype = "application/octet-stream", $encode = "base64") {
	$this->parts[] = array ("ctype" => $ctype,"message" => $message,"encode" => $encode,"name" => $name);
}

/**
* Erzeugt die einzelnen Mailbestandteile (Mail und Anhaenge).
* 
* @access	private
* @param	array	Mailpart als Array
* @return	string	Mailpart als String
*/ 
function build_message(&$part) {
	$message = &$part["message"];
	$this->enctype = $part["encode"];
	if($this->enctype=="quoted-printable") {
		$message = $this->qp_enc($message, $this->wordwrap - 15);
	}
	if ($this->enctype=="base64" || $this->enctype=="") {
		$message = chunk_split(base64_encode($message));
		$this->enctype = "base64";
	} 
	$encoding = $this->enctype;
	return	"Content-Type: ".$part[ "ctype"].
			(($part[ "name"])? "; name = \"".$part[ "name"]. "\"" :  "").
			(($this->enctype=="quoted-printable")?";\n\tcharset=\"iso-8859-1\"":"").
			"\nContent-Transfer-Encoding: $encoding\n\n$message\n";
}

/**
* Erzeugt aus den einzelnen Mailbestandteilen die Multipart-Mail (Sofern es mehrere Bestandteile sind!)
* 
* @access 	private
* @return 	string	Multipartmail als String
*/ 
function build_multipart() {
	 if(count($this->parts)>1) {
		 $boundary  =	"----=_NextPart_000_b".md5(time()+microtime());
		 $multipart =	"Content-Type: multipart/" .
		 				($this->mailbodyConst == MIMEMAIL_HTML_ALTERNATIVE ? "alternative" : "mixed") .
		 				"; \n\tboundary=\"$boundary\"\n\nThis is a multi-part message in MIME format.\n\n--$boundary";
		
		 for($i = count($this->parts)-1; $i >= 0; $i--) 
		    $multipart .=  "\n".$this->build_message($this->parts[$i]). "--$boundary";
		 return $multipart . "--\n";
	 } else {
	 	return $this->build_message($this->parts[0]);
	 }
 }

/**
* Versendet die Mail
* 
* @access 	public
* @param	Delay in Sekunden (z.B. bei versandt mehrere Mails in Schleife)
*/
function send($delay=0) {
	$mime =  "";
	if (!empty($this->from))	{
		$mime .=  "From: ".($this -> parseEmailadresses ? $this -> getEmaillist($this->from) : $this->from). "\n";
		$mime .=  "Sender: ".($this -> parseEmailadresses ? $this -> getEmaillist($this->from) : $this->from). "\n";
		$mime .=  "Return-Path: ".($this -> parseEmailadresses ? $this -> getEmaillist($this->from) : $this->from). "\n";
	}
	$mime .=  "X-Priority: ".array_search($this -> priorityvalue, $this -> priority)."\n";
	$mime .=  "X-MSMail-Priority: ".$this -> priorityvalue." \n";
	$mime .=  "Importance: ".$this -> priorityvalue."\n";

	if (!empty($this->headers))	$mime .= $this->headers. "\n";
	   
	if (!empty($this->body)) {
		//translation der Umlaute in html-sonderzeichen (beim html-newsletter)
		$trans=array(	"ä"=>"&auml;","ö"=>"&ouml;","ü"=>"&uuml;","Ä"=>"&Auml;",
						"Ö"=>"&Ouml;","Ü"=>"&Uuml;","ß"=>"&szlig;","€"=>"&euro;",
						 "®"=>"&reg;", "©"=>"&copy;", "&&Uuml;uml;"=>"&Uuml;"
					);
		$html2text = array_merge(array_flip($trans),Array("&nbsp;"=>" ", "&amp;" => "&"));
		if($this->ishtmlbody) {
			$this->bodytext = eregi_replace("<br>","#linebreak#",$this->body);
			$this->bodytext = eregi_replace("<p>","#linebreak##linebreak#",$this->bodytext);
			$this->bodytext = strip_tags(preg_replace(
				Array(
					'/\<style[^\>]*?\>[^\^]*?\<\/style\>/i',
					'/\<object[^\>]*?\>[^\^]*?\<\/object\>/i',
					'/\<embed[^\>]*?\>[^\^]*?\<\/embed\>/i',
					'/\<script[^\>]*?\>[^\^]*?\<\/script\>/i'
					),
				' ',
				strtr($this->bodytext, $html2text)
			));
			$this->bodytext = str_replace("#linebreak#","\n",$this->bodytext);
			$this->body = strtr($this->body,$trans);			
		} else {
			$this->bodytext = &$this->body;
		}
		// HTML-Teil der Nachricht
		if($this->ishtmlbody)
			$this->add_attachment($this->body,  "",  "text/html",$this->MailbodyEncoding);
		// Textteil der Nachricht (erzeugen)
		if($this->mailbodyConst != MIMEMAIL_HTML) {
			if($this->wordwrap) $this->bodytext = wordwrap($this->bodytext,$this->wordwrap);
			$this->add_attachment($this->bodytext,  "",  "text/plain",$this->MailbodyEncoding);
		}
	}
	$mime .=  "MIME-Version: 1.0\n".$this->build_multipart();
	
	if ($delay) {usleep($delay*1000000);}
	
	// Wenn dies Wahr ist, wird die Emailadresse(n) auf Gueltigkeit geprueft und es ist moeglich die alternative Syntax dafuer zu verwenden.
	if($this -> parseEmailadresses) {
		$this->to = $this -> getEmaillist($this->to);
	} else {
		$this->to = ereg_replace("(\n|\r)","",$this->to);
	}
	if($this->to)
		return mail($this->to, $this->subject,  "", $mime);
	else
		return false;
 }


/**
* Formatiert/Erzeugt aus Emailvorgaben ein Array mit Emailadressen
* 
* @access 	public
* @see formatEmailList();enableParseEmailadresses();
* @param	mixed	$emaillist	Liste von Emailadressen wahlweise als Array oder mit "," getrennt im 
* @return	string	Emailaddress - Headerpart
*/
function getEmaillist($emaillist) {
	$emailbuffer = Array();
	$emaillist = $this -> formatEmailList($emaillist);
	foreach($emaillist as $email) {
		if(is_array($email) && preg_match($this->emailcheck, $email[1])) {
			$emailbuffer[] = '"'.$email[0].'" <'.$email[1].'>';
		} elseif(preg_match($this->emailcheck, $email)) {
			$emailbuffer[] = '<'.$email.'>';
		}
	}
	return implode(",",$emailbuffer);
}

/**
* Formatiert/Erzeugt aus Emailvorgaben ein Array mit Emailadressen
* 
* @access 	public
* @param	mixed	$emaillist	Liste von Emailadressen wahlweise als Array oder mit "," getrennt im 
* @return	array	Formatiertes Emailarray [Array(Array(name,emailaddr),emailaddr,...)]
*/
function formatEmailList($emaillist) {
	if(!is_array($emaillist))
		$emaillist = explode(",",$emaillist); // Mailadressen spliten
	foreach($emaillist as $k => $email) {
		if(!is_array($email)) { // Mailadressen in Namen und Email aufteilen
			$emailname = "";
			if(preg_match('/([^\<]*?)\<([^\>]*?)\>/i', $email, $matches))
				list(, $emailname, $emailaddr) = $matches;
			elseif(preg_match('/\<([^\>]*?)\>/i', $email, $matches))
				list(, $emailaddr) = $matches;
			else
				$emailaddr = $email;
			$email = Array(trim(preg_replace('/\n\r\t/','',$emailname)), trim(preg_replace('/\n\r\t/','',$emailaddr)));
		}
		// Liste zurueckschreiben
		if($email[0]) {
			// Namen formatieren
			if(!preg_match($this->emailisocheck, $email[0]))
				$email[0] = '?iso-8859-1?'.qp_enc($email[0], 300);
			$emaillist[$k] = $email;
		} else {
			$emaillist[$k] = $email[1];
		}
	}
	return $emaillist;
}


/**
* Setzt das Mailobject wieder zurueck auf die Standardwerte
* 
* @access 	public
*/
function clear() {
  $this->parts = array();
  $this->to =  "";
  $this->from =  "";
  $this->subject =  "";
  $this->enctype =  "";
  $this->body =  "";
  $this->headers =  "";
 }

/**
* Funktion fuer "quoted-printable encoding"
* 
* @access 	public
* @param	string	Zeichenkette, die mit quoted-printable Encodiert werden soll
* @param	integer	Zeilelaenge bis zu einem automatischem Zeilenumbruch 
			(betrifft Zeilenumbrueche im Ausgabeformat)
* @param	Umbruchzeichen
* @param	Escape-Zeichen
* @return	string  encodierte Version der Zeichenkette
*/
function qp_enc($input, $line_max = 76, $eol = "\r\n", $escape = "=") { 
	$hex = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'); 
	$lines = preg_split("/(?:\r\n|\r|\n)/", $input); 
	$output = ""; 
	
	while( list(, $line) = each($lines) ) { 
		$linlen = strlen($line); 
		$newline = ""; 
		for($i = 0; $i < $linlen; $i++) { 
			$c = substr($line, $i, 1); 
			$dec = ord($c); 
			if ( ($dec == 32) && ($i == ($linlen - 1)) ) { // convert space at eol only 
				$c = "=20"; 
			} elseif ( ($dec == 61) || ($dec < 32 ) || ($dec > 126) ) { // always encode "\t", which is *not* required 
				$h2 = floor($dec/16); $h1 = floor($dec%16); 
				$c = $escape.$hex["$h2"].$hex["$h1"]; 
			} 
			if ( (strlen($newline) + strlen($c)) >= $line_max ) { // CRLF is not counted 
				$output .= $newline.$escape.$eol; // soft line break; " =\r\n" is okay 
				$newline = ""; 
			} 
			$newline .= $c; 
		}
		$output .= $newline.$eol; 
	} 
	return trim($output); 
} 

 
};
?>