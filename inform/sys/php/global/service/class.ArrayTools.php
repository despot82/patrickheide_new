<?php
/**---------------------------------------------------------------------------------
#	filename:	array_handler.inc.php
#	version:	1.1 af
#	date:		2002-06-07
#---------------------------------------------------------------------------------

#	Methoden zum Verwalten von komma-separierten Wertelisten (z.B. prio-listen):
#
#	function prio1Up($str,$pos)
#	function prio1Down($str,$pos)
#	function prioTop($str,$pos)
#	function prioBottom($str,$pos)
#	function addTop($str,$neu)
#	function addBottom($str,$neu)
#	function remElm($str,$pos)
#
#	(alle Funktionen erwarten einen string [z.B.: 12,4,7] & die aktuelle position
#	 [als INT] bzw. den neuen Wert des zu verschiebenden oder neuen Elements)

#---------------------------------------------------------------------------------
*/
class ArrayTools {

/**	
 * Eine Position nach oben oder unten verschieben
 * 
 * @param String $str
 * @param int $pos
 * @return array
 * @access public
 */
	function moveUp($str,$pos) {	// erwartet einen prio-string & die aktuelle position
		$arr = explode(",",$str);		// string in array teilen
		if($pos > 0) {					// nur ausf�hren wenn $pos nicht schon das erste element ist
			$arr[] = $arr[$pos];			// aktuellen wert am ende des arrays zwischenlagern
			$arr[$pos] = $arr[$pos-1];		// aktuellen wert mit dem darunter �berschreiben
			$arr[$pos-1] = array_pop($arr);	// den darunter mit zwischenlager �berschreiben und zwischenlager l�schen
		}
		return implode(",",$arr);
	}
	/**
	 * @param String $str
	 * @param int $pos
	 * @return array
	 * @access public
	 */
	function moveDown($str,$pos) {	// erwartet einen prio-string & die aktuelle position
		$arr = explode(",",$str);		// string in array teilen
		if($pos < (count($arr)-1)) {	// nur ausf�hren wenn $pos nicht schon das letzte element ist
			$arr[] = $arr[$pos];			// aktuellen wert am ende des arrays zwischenlagern
			$arr[$pos] = $arr[$pos+1];		// aktuellen wert mit dem dar�ber �berschreiben
			$arr[$pos+1] = array_pop($arr);	// den dar�ber mit zwischenlager �berschreiben und zwischenlager l�schen
		}
		return implode(",",$arr);
	}
	/**
	 * ganz nach oben oder ganz nach unten verschieben
	 * 
	 * @param String $str
	 * @param int $pos
	 * @return array
	 * @access public
	 */
	function moveTop($str,$pos) {		// erwartet einen prio-string & die aktuelle position
		$arr = explode(",",$str);			// string in array teilen
		$rem = array_splice($arr,$pos,1);	// aktuellen wert ausschneiden
		array_unshift($arr,$rem[0]);		// ausgeschnittenen wert an den anfang des arrays schreiben
		return implode(",",$arr);
	}
	/**
	 * @param String $str
	 * @param int $pos
	 * @return array
	 * @access public
	 */
	function moveBottom($str,$pos) {	// erwartet einen prio-string & die aktuelle position
		$arr = explode(",",$str);			// string in array teilen
		$rem = array_splice($arr,$pos,1);	// aktuellen wert ausschneiden
		array_push($arr,$rem[0]);			// ausgeschnittenen wert ans ende des arrays schreiben
		return implode(",",$arr);
	}
	/**
	 * neues Element oben oder unten dranh�ngen
	 * 
	 * @param String $str
	 * @param int $pos
	 * @return array
	 * @access public
	 */
	function addTop($str,$neu) {		// erwartet einen prio-string & neuer Wert
		if($str) {						// nur ausf�hren wenn $str schon existiert
			$arr = explode(",",$str);		// string in array teilen
			array_unshift($arr,$neu);		// neuen wert an den anfang des arrays schreiben
			return implode(",",$arr);
		} else { return $neu; }
	}
	/**
	 * @param String $str
	 * @param int $pos
	 * @return array
	 * @access public
	 */
	function addBottom($str,$neu) {		// erwartet einen prio-string & neuer Wert
		$arr = explode(",",$str);			// string in array teilen
		array_push($arr,$neu);				// neuen wert ans ende des arrays schreiben
		return implode(",",$arr);
	}

	/**
	 * Element von Position entfernen
	 * 
	 * @param String $str
	 * @param int $pos
	 * @return array
	 * @access public
	 */
	function remElm($str,$pos) {		// erwartet einen prio-string & die aktuelle position
		$arr = explode(",",$str);			// string in array teilen
		array_splice($arr,$pos,1);			// aktuellen wert ausschneiden
		return implode(",",$arr);
	}
	/**
	 * Uebergebene ID entfernen
	 * 
	 * @param String $str
	 * @param int $pos
	 * @return array
	 * @access public
	 */
	function remID($str,$id) {		// erwartet einen prio-string & die aktuelle ID
		if(strstr($str,',')) { $id = $id.","; }		// "," anh�ngen wenn's nicht die einzige ID im $str ist
		return str_replace($id,'',$str);			// ID ausschneiden
	}
}
?>