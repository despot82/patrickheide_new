<?php
/*
 * Created on 25.03.2006
 *
 */
/**
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * Diese Klasse stellt einen einfachen Zugriff auf die Slideshow bereit. Alle Werte die ermittelt werden,
 * sind bereits nach Prio sortiert.
 * @param object $oDb
 * @param array $Userdata
 * @version 1.0 - 27.03.2006
 */
	class Slideshow {
		
		var $oDb = null;
		var $Userdata = null;
		
		var $oMediadb = null;
		var $entries = 0;
		
		function Slideshow($oDb,$Userdata) {
			$this->oDb = $oDb;
			$this->Userdata =& $Userdata;
		}
		/**
		 * 
		 * @return int $entries
		 * 
		 */
		function getEntries() {
			return $this->entries;
		}
		
		/**
		 * 
		 * @param object $oMediadb
		 * 
		 */
		
		function setMediadb(&$oMediadb) {
			$this->oMediadb =& $oMediadb;
		}
		
		/**
		 * 
		 * Diese Methode liefert eine Slideshow als Kommasepariertem String
		 * @param String $sid [Identifikation des Slides]
		 * @param int $fkid forreignkey identifikation 
		 * @return String
		 * @uses getSlideshow
		 * 
		 */
		
		function getCommaSepratedSlide($sid,$fkid) {
			$aSlide = $this->getSlideshow($sid,$fkid);
			for($i=0;is_array($aSlide[$i]);$i++) {
				$slidemids[] = $aSlide[$i]['aImg']['id'];
			}
			return implode(',',$slidemids);
		}
		
		/**
		 * 
		 * Zentrale Methode zum Abfragen der Slideshow aus der Datenbank. Liefert ein nummerisch - Assoziatives Array zurück
		 * Bsp. $aData[$i]['id']
		 * @param String $sid [Identifikation des Slides]
		 * @param int $fkid forreignkey identifikation 
		 * @return array
		 * 
		 */
		
		function getSlideshow($sid,$fkid) {
			
			$sql = "SELECT * FROM sys_slideshow WHERE slide_id='$sid' AND fk_id='$fkid' ORDER BY prio DESC";
			
			$this->oDb->query($sql);
			$this->entries = $this->oDb->num_rows();
			$aBuffer['sid'] = $sid;
			$aBuffer['fkid'] = $fkid;
			for($i=0;$row = $this->oDb->fetch_object();$i++) {
				foreach($row as $field => $value) {
					if($field == 'mdb_id') {
						$aImg = $this->oMediadb->getMedia($value);
						$aBuffer[$i]['aImg'] = $aImg;
						$aBuffer['img_'.($i+1).'_title'] = $aImg['name'];
						$aBuffer['img_'.($i+1)] = $value;
					}
					$aBuffer[$i][$field] = $value;
				}
			}
			$this->oDb->free_result();
			return $aBuffer;
		}
		
		/**
		 * 
		 * Diese Methode speichert ein neues Objekt in der Slideshow oder editiert ein vorhandenes
		 * @param array $aData
		 * @return mixed
		 * 
		 */
		
		function save($aData) {
			if(isset($aData['img_neu']) && $aData['replaceSave'] == 0) { 
					return $this->insert($aData['img_neu'],$aData['sid'],$aData['fkid']);
			}
			if($aData['replaceSave'] != 0) {
				return $this->update($aData);
			}
		}
		
		function clearAll($sid,$fkid) {
			return $this->oDb->make_delete(array('fk_id' => $fkid,'slide_id' => $sid),'sys_slideshow');
		}
		/**
		 * Diese Methode ersetzt einen vorhanden Datensatz.
		 * @param array $aData
		 * @return bool
		 */
		function update($aData) {
			
			$aSlide = $this->getSlideshow($aData['sid'],$aData['fkid']);
			
			$sql = "UPDATE sys_slideshow SET mdb_id='".$aData['img_'.$aData['replaceSave']]."',last_mod_by='".$this->Userdata['id']."'" .
					" WHERE id='".$aSlide[($aData['replaceSave']-1)]['id']."'";
			return $this->oDb->query($sql);
			
		}
		/**
		 * 
		 * Diese Methode fügt einen neuen Datensatz in der Slideshow ein.
		 * @param int $mdb_id
		 * @param int $sid
		 * @param int $fkid
		 * @return bool
		 * 
		 */
		function insert($mdb_id,$sid,$fkid) {
			
			$aSlide = $this->getSlideshow($sid,$fkid);
			
			$prio = ($aSlide[0]['prio']+1);
			
			$sql = "INSERT INTO sys_slideshow (fk_id,slide_id,mdb_id,prio,created_by,created) VALUES " .
					" ('$fkid','$sid','$mdb_id','$prio','".$this->Userdata['id']."','".date('dmYHis')."')";

			return $this->oDb->query($sql);
		}
		
	}
?>