<?php
/**
* globale Funktionsbibliothek fuer WEB- oder ADMIN-Seiten, die beim Download von Dateien helfen.
*
* Example: 
* <pre><code> 
* require_once($aENV['path']['global_service']['unix']."class.download.php");
* $oDl =& new download(); // params: -
* // set vars
* $oDl->setFile($file);
* $oDl->setName($aData['name']);
* $oDl->setContentType($aData['filetype']);
* $oDl->setContentDisposition($mode);
* #$oDl->debug = true;
* // get file
* $oDl->send();
* </code></pre>
*
* @access	public
* @package	Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.2 / 2006-03-27 [NEU: public function setMsieDownloadsRedirect()]
*/

class download {
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse download:
		----------------------------------------------------------------------------
		konstruktor download()
		function setFile($sValue)
		function setName($sValue)
		function setContentType($sValue)
		function setContentDisposition($sValue)
		function setExtraHeader($sValue)
		function setMsieDownloadsRedirect($b=true)
		function send()
		
		private _prepareData()
		private _checkFileExists()
		private _checkFilePermission()
		private _getFilesize()
		private _checkName()
		private _checkContentType()
		----------------------------------------------------------------------------
		HISTORY:
		1.2 / 2006-03-27 [NEU: public function setMsieDownloadsRedirect()]
		1.1 / 2006-02-22 [NEU: public function getMimeType()]
		1.0 / 2005-05-04 []
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	string	Datei inkl. Pfad
*/
	var $sFile;
/**
* @access   private
* @var	 	string	im Download-Dialogfenster angezeigter Dateiname
*/
	var $sName;
/**
* @access   private
* @var	 	string	Value f. header
*/
	var $sContentType;
/**
* @access   private
* @var	 	string	Value f. header
*/			
	var $nContentLength;
/**
* @access   private
* @var	 	string	Value f. header
*/
	var $sContentDisposition;
/**
* @access   private
* @var	 	string	Value f. header
*/
/**
* @access   private
* @var	 	array	zusaetzliche Header-Angaben
*/
	var $aExtraHeader;
/**
* @access   private
* @var	 	boolean	Information ob Datei existiert
*/
	var $bFileExists;
/**
* @access   private
* @var	 	boolean	Information ob Dateirechte stimmen
*/
	var $bFilePermission;
/**
* @access   private
* @var	 	boolean	Information ob bei MS-IE (NUR bei downloadfiles) statt "readfile()" direkt auf die Datei weitergeleitet wird.
*/
	var $bMsieDownloadsRedirect;
/**
* @access   public
* @var	 	boolean	Schalter fuer DEBUG Ausgabe
*/
	var $debug;

#-----------------------------------------------------------------------------

	/**
	* Constructor
	* @access		public	
	*/	
	function download() {
		$this->sFile					= '';
		$this->sName					= '';
		$this->sContentType 			= '';
		$this->nContentLength			= 0;	
		$this->sContentDisposition		= 'attachment';
		$this->aExtraHeader				= array();
		$this->bFileExists				= NULL;
		$this->bFilePermission			= NULL;
		$this->bMsieDownloadsRedirect	= false;
		$this->debug					= false;		
	}

#-----------------------------------------------------------------------------

	/**
	* It configures the real path/filename (filesystem path!)
	* @param		string	$sValue
	* @access		public
	*/	
	function setFile($sValue) {
		$this->sFile = $sValue;
	}		
	
	/**
	* It configures the personalized name of the file 
	* (therefore it can be different to the real name on the server)
	* @param		string	$sValue
	* @access		public
	*/	
	function setName($sValue) {
		$this->sName = $sValue;
	}			
	
	/**
	* It configures value Header 'ContentType'
	* @param		string	$sValue
	* @access		public
	*/		
	function setContentType($sValue) {
		$this->sContentType = $sValue;
	}
	
	/**
	* It configures value Header 'ContentDisposition'
	* @param		string	$sValue
	* @access		public
	*/	
	function setContentDisposition($sValue) {
		$this->sContentDisposition = $sValue;
	}
	
	/**
	* It configures value Header 'ContentTransferEncoding'
	* @param		string	$sValue 
	* @access		public
	*/	
	function setExtraHeader($sValue) {
		$this->aExtraHeader[] = $sValue;
	}
	
	/**
	* Setzt die Information ob bei MS-IE (NUR bei downloadfiles) statt "readfile()" direkt auf die Datei weitergeleitet wird.
	* @param		boolean	$b 
	* @access		public
	*/	
	function setMsieDownloadsRedirect($b=true) {
		$this->bMsieDownloadsRedirect = (bool) $b;
	}
	
	/**
	* Schaltet den DEBUG-Modus an (oder aus).
	* @param		boolean	$b 
	* @access		public
	*/	
	function setDebug($b=true) {
		$this->debug = (bool) $b;
	}

#-----------------------------------------------------------------------------

	/**
	* Init Download
	* @access		public
	*/	
	function send() {
		// check file & vars
		$this->_prepareData();
		// go
		if ($this->debug == true) {
			// DEBUG output
			header("Content-Type: text/html");
			print("this->sFile: ".$this->sFile."<br>\n"); 
			print("this->sName: ".$this->sName."<br>\n");	
			print("this->sContentType: ".$this->sContentType."<br>\n"); 	
			print("this->nContentLength: ".$this->nContentLength."<br>\n");
			print("this->sContentDisposition: ".$this->sContentDisposition."<br>\n");
			print("this->aExtraHeader: ".implode(', ', $this->aExtraHeader)."<br>\n");
			
		} else {
			// build headers
			/* TODO: TEST (aus PHP Classes kopiert...)
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: public"); 
			header("Content-Description: File Transfer");
			*/
			
			// IE-Download-Bug workaround
			if ($this->bMsieDownloadsRedirect && isset($_SERVER['HTTP_USER_AGENT']) && strstr('MSIE', $_SERVER['HTTP_USER_AGENT'])) {
				if ($this->sContentDisposition == 'attachment') {
					// http path ermitteln
					$http_path = $_SERVER['HTTP_HOST'].str_replace($_SERVER['DOCUMENT_ROOT'],'', dirname($this->sFile)).'/';
					// weiterleitung auf das echte file :-(
					header("HTTP/1.0 307 Temporary redirect");
					header("Location: http://".str_replace('//', '/', $http_path).basename($this->sFile));
					exit;
				} else {
					ini_set('zlib.output_compression','Off');
				}
			}
			
			header("Content-Type: ".$this->sContentType);
			###header("Content-Length: ".$this->nContentLength); // DEACTIVATED -> weil macht Probleme!
			header("Content-Disposition: ".$this->sContentDisposition."; filename=\"".$this->sName."\"");
			if (count($this->aExtraHeader) > 0) {
				foreach ($this->aExtraHeader as $header) {
					header($header);
				}
			}
			// output file
			@readfile($this->sFile);
			###exit;
		}
	}
	
	/**
	* @return		boolean
	* @access		private
	*/
	function _prepareData() {
		// check if headers already sent
		if (headers_sent()) {
			if ($this->debug == true) {
				print('ERROR: Headers already sent!');
			} else {
				return false;	exit;
			}
		}
		// check if file exists
		if (!$this->_checkFileExists()) {
			if ($this->debug == true) {
				print('ERROR: file "'.$this->sFile.'" does not exist!');
			} else {
				return false;	exit;
			}
		}
		// check permission
		if (!$this->_checkFilePermission()) {
			if ($this->debug == true) {
				print('ERROR: filepermission!');
			} else {
				return false;	exit;
			}
		}
		// get filesize
		$this->_getFilesize();
		// check download filename
		$this->_checkName();
		// check mimetype
		$this->_checkContentType();
		// output
		return true;
	}

	/**
	* check if file exists
	* @return		boolean
	* @access		private
	*/
	function _checkFileExists() {
		// check if file exists
		if (is_null($this->bFileExists)) {
			$this->bFileExists = file_exists($this->sFile);
		}
		return ($this->bFileExists) ? true : false;
	}

	/**
	* check permission number for user 'other'
	* @return		boolean
	* @access		private
	*/
	function _checkFilePermission() {
		if (!$this->_checkFileExists()) return false;
		// check if file exists
		if (is_null($this->bFilePermission)) {
			$this->bFilePermission = substr(decoct(fileperms($this->sFile)), -1);
		}
		return ($this->bFilePermission >= 4) ? true : false;
	}

	/**
	* get filesize
	* @return		int
	* @access		private
	*/
	function _getFilesize() {
		if (!$this->_checkFileExists()) return false;
		if ($this->nContentLength == 0) {
			$this->nContentLength = filesize($this->sFile);
		}
		return $this->nContentLength;
	}
	
	/**
	* check download filename
	* @return		string
	* @access		private
	*/
	function _checkName() {
		if ($this->sName == '') {
			$this->sName = basename($this->sFile);
		}
		return $this->sName;
	}
	
	/**
	* get mimetype if ContentType is empty
	* @return		string
	* @access		private
	*/
	function _checkContentType() {
		if ($this->sContentType == '') {
			$this->sContentType = $this->getMimeType();
		}
		return $this->sContentType;
	}

	/**
	* Hilfsfunktion: mime-type aufgrund der Datei-Endung ermitteln
	* @param	string	file, dessen mime-type ermittelt werden soll
	* @return	string	mime-type
	* @access	public
	*/
	function getMimeType($sFile='') {
		// check vars
		if ($sFile == '') $sFile = $this->sFile;
		if ($sFile == '') return false;
		
		// mimetype mittels Datei-Endung ermitteln
		$ext = strToLower(substr(strrchr($sFile, "."), 1)); // file-extension
		switch($ext) {
			// pdf
			case "pdf":	$type = "application/pdf";				break;
			// archive
			case "zip":	$type = "application/zip";				break; //x-zip-compressed
			case "tar":	$type = "application/x-tar";			break;
			case "sit":	$type = "application/x-stuffit";		break;
			case "gtar":$type = "application/x-gtar";			break;
			case "hqx":	$type = "application/mac-binhex40";		break;
			case "gz":	$type = "application/gzip";				break;
			case "tgz": $type  = "application/x-compressed";	break;
			case "rar": $type  = "application/x-compressed rar";break;
			// images
			case "jpg":	$type = "image/jpeg";					break; //image/pjpeg
			case "jpeg":$type = "image/jpeg";					break;
			case "gif":	$type = "image/gif";					break;
			case "png":	$type = "image/png";					break; //image/x-png
			case "tif":	$type = "image/tiff";					break;
			case "tiff":$type = "image/tiff";					break;
			case "ai":	$type = "image/ai";						break; //application/postscript
			case "eps":	$type = "image/eps";					break; //application/postscript
			case "psd":	$type = "application/psd";				break;
			case "ico":	$type = "image/x-icon";					break;
			case "bmp":	$type = "image/bmp";					break;
			// flash
			case "swf":	$type = "application/x-shockwave-flash";break;
			// web-dokumente
			case "php":	$type = "application/x-httpd-php";		break;
			case "htm":	$type = "text/html";					break;
			case "html":$type = "text/html";					break;
			case "xml":	$type = "text/xml";						break;
			case "xsl":	$type = "text/xml";						break;
			case "xslt":$type = "text/xml";						break;
			case "js":	$type = "text/javascript";				break;
			case "css":	$type = "text/css";						break;
			case "svg": $type = "image/svg+xml";				break;
			// textformate/office
			case "csv":	$type = "text/comma-separated-values";	break;
			case "txt":	$type = "text/plain";					break;
			case "rtf":	$type = "text/rtf";						break;
			case "doc":	$type = "application/msword";			break;
			case "dot":	$type = "application/msword";			break;
			case "xls":	$type = "application/msexcel";			break;
			case "ppt":	$type = "application/mspowerpoint";		break;
			case "pps":	$type = "application/mspowerpoint";		break;
			case "vcf":	$type = "text/x-vcard";					break;
			// audio/video
			case "ram":	$type = "audio/x-pn-realaudio";			break;
			case "ra":	$type = "audio/x-pn-realaudio";			break;
			case "rm":	$type = "audio/x-pn-realaudio";			break;
			case "wav":	$type = "audio/x-wav";					break;
			case "aif":	$type = "audio/x-aiff";					break;
			case "aiff":$type = "audio/aiff";					break;
			case "mp3":	$type = "audio/mpeg";					break;
			case "mpeg":$type = "video/mpeg";					break;
		    case "mpg":	$type = "video/mpeg";					break;
		    case "mpe":	$type = "video/mpeg";					break;
		    case "mp4": $type = "video/mp4";					break;
			case "mov":	$type = "video/quicktime";				break;
			case "qt":	$type = "video/quicktime";				break;
			case "avi":	$type = "video/x-msvideo";				break;
			case "ogg": $type = "application/ogg";				break;
			case "wmv":	$type = "video/x-ms-wmv";				break;
			case "flv":	$type = "video/x-flv";					break;
			// default ("file")
			default:	$type = "application/octet-stream";
		}
		return $type;
	}

#-----------------------------------------------------------------------------

} // END of class
?>