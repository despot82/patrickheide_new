<?php // Class form_admin -> geht NUR MIT NEUER MEDIA-DB!!!
/**
* This class provides HTML-form-functions for admin detail-pages.
*
* Example:
* <pre><code>
* // init
* $oForm = new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
* // NUR wenn CMS:
* $oForm->set_cms_vars('client', $navi_id); // params: $sTemplate[,$navi_id=NULL][,$bWithPreview=true]
* // zu pruefende Felder in der gewuenschten Reihenfolge
* $oForm->check_field("title_".$weblang, $sMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
* $oForm->check_field("subtitle_".$weblang, $sMSG['form']['subtitle'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
* // oeffnendes Form-Tag
* echo $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions, 'POST', 'editForm'); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
* // Ausgabe
* echo $oForm->textfield("title_".$weblang); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra='']
* echo $oForm->textarea("subtitle_".$weblang, 2); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra='']
* echo $oForm->textarea_tools("description_".$weblang, 5, 43); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''][,$sClass="smallbut"][,$sLayer="content"]
* echo $oForm->media_tools("img", "flash"); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0]
* // buttons
* echo $oForm->button("SAVE");
* // schliessendes Form-Tag
* echo $oForm->end_tag();
* </code></pre>
*
* @access	public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @author Frederic Hoppenstock <fh@design-aspekt.com>
* @author Nils Hitze <nh@design-aspekt.com>
* @version	2.64 / 2006-07-20	[BUGFIX: "textarea_flash()" umgebaut, dass Flash-Einbettung für IE über JS erfolgt]
* @version	2.65 / 2006-08-16	[NEW: "multiMdb_tools()" eingebaut, für mehrere beliebige Anhänge aus der MDB]
*/

class form_admin extends form_web {
	
// TODO: radio check bei "check_field()"!
	
	/*	----------------------------------------------------------------------------
		Funktionen dieser Funktionsbibliothek:
		----------------------------------------------------------------------------
		constructor form_admin($aData='', $sLang='de')
		function set_cms_vars($sTemplate, $navi_id=NULL, $bWithPreview=true)
		function check_field($sFieldname, $sAlertName='', $sCheck='FILLED')
		function add_js($sJScode)
		function change_true_into($sJScode)
		function change_delete_msg_into($sMessage)
		function start_tag($sAction='', $sUrlParams='', $sMethod='post', $sName='', $sExtra='')
		function end_tag()
		
		function password($sFieldname, $nMaxlength=50, $nSize=57, $sDefault='', $sExtra='')
		function textfield($sFieldname, $nMaxlength=150, $nSize=57, $sDefault='', $sExtra='')
		function textarea($sFieldname, $nRows=10, $nCols=43, $sDefault='', $sExtra='')
		function textarea_flash($sFieldname, $sTable, $id)
		function slideshow_tools($sFieldname, $sTable, $sId, $nWidth='', $nHeight='', $bMustfit=0)
		function media_tools($sFieldname, $sMediaType='all', $nWidth='', $nHeight='', $bMustfit=0)
		function pdf_tools($sFieldname)
		function media_upload()
		function file_upload($sFieldname='uploadfile', $sPathKey='mdb_upload')
		private get_media_icon($sFilename, $sExtra='', $sPathKey='mdb_upload')
		private get_media_type($sFilename)
		function linkmanager_tools($sFieldname, $nSize=45)
		function checkbox($sFieldname, $sDefaultCheckboxValue='1', $sDefaultHiddenValue='0')
		function radio($sFieldname, $aValues='', $bFirstChecked=false, $bLinebreaks=false)
		function select($sFieldname, $aValues, $sExtra='', $nSize=1, $bMultiple=false)
		
		function lang_dropdown($size=1)
		function date_dropdown($sFieldname, $bDateRequired=true, $nPastYears=5, $nFutureYears=1)
		function date_popup($sFieldname, $nMaxlength=10, $nSize=12, $sDefault='', $sExtra='', $sImg='')
		function categories_dropdown($navi_id, $sTrenner='&nbsp;|&nbsp;')
		
		function button($sType, $sClass='')
		----------------------------------------------------------------------------
		HISTORY:
		2.63 / 2006-04-11	[BUGFIX: "validFromTo()" umgebaut, dass es auch mohne $lang funktioniert]
		2.61 / 2006-02-10	[NEU: neuer Parameter fuer labelDropdown]
		2.60 / 2006-01-23	[BUGFIX: Parameterbehandlung bei GET bei "start_tag()"]
		2.59 / 2005-12-23	[NEU: downloadlinkmanager_tools - zum auslesen, eines FTP Verzeichnisses und anbieten von dessen inhalt]
		2.58 / 2005-12-07	[BUGFIX: Weite fuer Textarea in texarea_flash() angepasst]
		2.57 / 2005-11-07	[NEU: bei den Buttons werden die Buttons, wo die Seite Folder enthaelt geswitched]
		2.56 / 2005-10-17	[textarea_tools entfernt, da JS-Texteditor nicht mehr verwendet wird]
		2.55 / 2005-09-22	[BUGFIX: JS fuer Formularfelder wegen Focus-Problem Flash-Editor hinzugefuegt"]
		2.54 / 2005-08-26	[BUGFIX: href-ermittlung in "button()"]
		2.53 / 2005-06-15	[NEU: Preview-Erweiterungen in "slideshow_tools()"]
		2.52 / 2005-06-14	[NEU: Preview-Erweiterungen in "start_tag()" + "button()"]
		2.51 / 2005-06-01	[BUGFIX: HTTPS-Einschraenkung in "textarea_flash()" korrigiert]
		2.5 / 2005-05-13	[NEU: "set_cms_vars()" + "default_hidden_fields()" + umgebaut: "categories_dropdown()" + "start_tag()"]
		2.45 / 2005-05-09	[BUGFIX: w/h in "slideshow_tools()" und "media_tools()"]
		2.44 / 2005-04-29	[NEU: "password()" + neue pruefung 'POS_INT' in "check_field()"]
		2.43 / 2005-04-28	[BUGFIX: value/default-ermittlung in allen methoden mit $sDefault]
		2.42 / 2005-04-26	[NEU: save_close-button + w/h/mustfit in "slideshow_tools()" und "media_tools()" eingebaut]
		2.41 / 2005-04-25	[BUGFIX: in "textarea()" sValue ermitteln]
		2.4 / 2005-04-25	[NEU: "file_upload()" auf modul-mediadb-upload umgestellt]
		2.31 / 2005-04-19	[$lang in $weblang umbenannt]
		2.3 / 2005-04-06	[NEU: auf MEDIA-DB umgestellt]
		2.23 / 2005-04-01	[NEU Konstante MDB_THUMB_WIDTH eingebaut]
		2.22 / 2005-03-17	[BUGFIX "JS:viewMediaWin()" in "slideshow_tools()" eingebaut]
		2.21 / 2005-03-14	[BUGFIX bei "date_popup()"]
		2.2 / 2005-03-10	[NEU: $sExtra bei "checkbox()" + "sXtra" ueberall in "sExtra" umbenannt]
		2.17 / 2005-02-21	[NEU: case "deactivate" in "button()"]
		2.16 / 2005-02-11	[BUGFIX: path zu thumbs.php in "slideshow_tools()"]
		2.15 / 2005-01-31	[BUGFIX: "<span class="text">" statt "<p>"]
		2.14 / 2005-01-24	[BUGFIX: bei "textarea_flash()"]
		2.13 / 2005-01-20	[BUGFIX: bei Rechte-reduzieren]
		2.12 / 2005-01-19	[NEU: ggf.Rechte-reduzieren + admin-gewichtung veraendert!]
		2.1 / 2005-01-14	[auf neues Rechtemanagement umgestellt - NICHT MEHR ABWAERTSKOMPATIBEL!]
		2.0 / 2005-01-11	[auf neue MediaDB umgestellt - NICHT MEHR ABWAERTSKOMPATIBEL!]
		1.74 / 2005-01-07	[NEU: "filesize" mittels "oFile" ermitteln]
		1.73 / 2004-12-20	[BUGFIX: "selected"-Erkennung in "categories_dropdown()" korrigiert]
		1.72 / 2004-12-16	[BUGFIX: Kommentar fuer Beispiel in "file_upload()" korrigiert]
		1.71 / 2004-12-06	[BUGFIX: Parent-Title in "categories_dropdown()"]
		1.7 / 2004-11-03	[BUGFIX: "$upload_path" in "get_media_icon()" hinzugefuegt]
		1.69 / 2004-10-28	[NEU: "$weblang" in "textarea_flash()" hinzugefuegt]
		1.68 / 2004-10-25	[BUGFIX: "$weblang" in "pdf_tools()"]
		1.67 / 2004-10-25	[BUGFIX: check-auf "file_exists()" in "file_upload()" eingebaut]
		1.66 / 2004-10-19	[BUGFIX: check-auf-vorhandensein in "get_media_icon()" eingebaut]
		1.65 / 2004-10-01	[NEU: "$bProtected" in die "upload()" + "get_media_icon()"-Methoden eingebaut]
		1.64 / 2004-09-28	[BUGFIX: js-weiterleitung in "button()"]
		1.63 / 2004-09-28	[BUGFIX: strToLower in "get_media_*()"]
		1.62 / 2004-09-24	[NEU: 2. Parameter Hintergrundfarbe in "pdf_tools()"]
		1.61 / 2004-09-22	[BUGFIX: regex in "textarea_flash()"]
		1.6 / 2004-09-20	[NEU: "pdf_tools()"]
		1.51 / 2004-09-16	[BUGFIX: filesize in "get_media_icon()"]
		1.5 / 2004-09-16	[NEU: "textarea_flash()" + "end_tag()" (= jetzt obligatorisch bei verwendung von "textarea_flash()"!)]
		1.43 / 2004-09-14	["back" + "new" bei "button()" jetzt als verlinktes image ausgeben]
		1.42 / 2004-09-14	[NEU: class="radiocheckbox" bei radios + checkboxen auf hinzugefuegt]
		1.41 / 2004-09-09	[NEU: beschriftungen von radios + checkboxen auf <label> umgestellt]
		1.4 / 2004-08-31	[NEU: "get_media_icon()" + "upload"-funktionen daraufhin umgestellt]
		1.32 / 2004-08-27	[NEU: neue types "copy" + "save_copy" bei "button()"]
		1.31 / 2004-08-23	[kl. erweiterung bei "check_field()"]
		1.3 / 2004-08-11	[NEU: "date_popup()"]
		1.2 / 2004-08-03	[NEU: "textarea()"; "write_media_*()" um mehrere icons erweitert]
		1.11 / 2004-08-02	[BUGFIX in "radio()"]
		1.1 / 2004-08-02	["categories_dropdown()" an Navi-Klasse angepasst + "media_tools()" um "file" erweitert]
		1.03 / 2004-06-28	[BUGFIX an "*_tools()" + "*_upload()": rel.-path fuer thumbs ermitteln]
		1.02 / 2004-06-23	[AENDERUNG an "write_linkmanager_tools()": buttonbeschriftung 2sprachig gemacht]
		1.01 / 2004-06-02	[AENDERUNG an "media_tools()": default geaendert und 2.parameter verbessert]
		1.0 / 2004-04-23	[aus der alten Form-Klasse entwickelt]
	*/
	
	// Parent-Class Vars.
	#var $aData;
	#var $alert;
	#var $sUrlParams;
	#var $sName;
	#var $js_true_action;	// default: 'return true;';
	#var $js_delete_msg; 	// default: '' (-> message wird aus array.messages.php ermittelt)
	#var $sLang;
	#var $oDb;
	// add. Class Vars.
	var $sCmsTemplate;
	var $sCmsNaviId;
	var $bCmsWithPreview;
	var $oBrowser;
	var $oDate;
	var $oFile;
	var $oPerm;
	var $oMediadb;
	var $aENV;
	var $aMSG;
	var $Userdata;
	var $bEditMode	= true;

/**
* Konstruktor -> Initialisiert das form-Objekt und fuellt es mit dem array $aData (welches i.d.R. aus der DB kommt) 
* und diversen anderen "globalen" Variablen! 
* NOTE: der 3. Parameter wird fuer die Vorschau + move_to verwendet!
*
* Beispiel:
* <pre><code>
* $oForm = new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
* </code></pre>
*
* @access	public
* @global	object	$oDb		"class.db_mysql.php"
* @global	object	$oBrowser	"class.browsercheck.php"
* @global	object	$oDate		"class.datetime.php" / "class.datetime_db.php"
* @global	array	$aENV
* @global	array	$aMSG
* @global	array	$Userdata
* @param 	array	$aData		Daten-Array
* @param 	string	$sLang		Sprachversion fuer Buttons / Alerts (default: 'de')
*/
	function form_admin($aData='', $sLang='de') {
		
		// apply
		$this->aData		= $aData;
		$this->sLang		= $sLang;
		// set defaults
		$this->js_true_action	= 'return true;';
		$this->js_delete_msg 	= '';
		// objekte importieren
		global $oDb, $oBrowser, $oDate, $oFile, $oPerm;
		$this->oDb			= &$oDb;
		$this->oBrowser		= &$oBrowser;
		$this->oDate		= &$oDate;
		$this->oFile		= &$oFile;
		$this->oPerm		= &$oPerm;
		$this->oMediadb		= NULL; // MediaDB nur bei Bedarf!
		// globale arrays importieren
		global $aENV, $aMSG, $Userdata;
		$this->aENV			= &$aENV;
		$this->aMSG			= &$aMSG;
		$this->Userdata		= &$Userdata;
		// check user-rights
		if (is_object($this->oPerm) && $this->oPerm->getModule() != "sys") { // NICHT im SYS-Modul!
			$this->bEditMode = ($this->oPerm->hasPriv('create') || $this->oPerm->hasPriv('edit')) ? true : false;
			if ($this->oPerm->hasPriv('create') && !$this->oPerm->hasPriv('edit') && isset($this->aData['created_by']))  {
				$this->bEditMode = false; // Recht reduzieren (wenn im edit-mode und der aktuelle Nutzer kein edit-Recht hat)
			}
			// hilfsfunktion bemuehen um edit-rechte in bezug auf besitzer anzupassen
			if ($this->_check_edit_all_privilege() == false) {
				$this->bEditMode = false; // Recht reduzieren (wenn der aktuelle Nutzer einen Datensatz eines anderen Nutzers nicht editieren darf)
			}
			// hilfsfunktion bemuehen um edit-rechte im cms in bezug auf freigabestatus anzupassen
			if ($this->_check_cms_edit_privilege() == false) {
				$this->bEditMode = false; // SPEZIALFALL CMS (wenn der aktuelle Nutzer kein publish-recht hat)
			}
		}
	}

/**
* hilfsfunktion bemuehen um edit-rechte ggf. in bezug auf Datensaetze anderer Nutzer anzupassen
* 
* @access   private
* @return	boolean	true wenn edit-privileg erhalten bleibt, sonst false
*/
	function _check_edit_all_privilege() {
		if (!isset($this->aData['created_by'])) return true;
		// ggf. edit-/delete-rechte reduzieren (wenn der akt. nutzer nicht der eigentuemer ist)
		if (($this->oPerm->hasPriv('delete') || $this->oPerm->hasPriv('edit')) 
			&& !$this->oPerm->hasPriv('admin') && !$this->oPerm->hasPriv('management') 
			&& $this->Userdata['id'] != $this->aData['created_by']
			) {
			if ($this->oPerm->hasPriv('edit'))		$this->oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
			if ($this->oPerm->hasPriv('delete'))	$this->oPerm->removePriv('delete'); // params: $sPrivilege[,$sModule='']
			return false;
		}
		return true;
	}
	
	function setMediadb(&$oMediadb) {
		
		$this->oMediadb =& $oMediadb;
	}

/**
* hilfsfunktion bemuehen um edit-rechte im cms in bezug auf freigabestatus anzupassen
* 
* @access   private
* @return	boolean	true wenn edit-privileg erhalten bleibt, sonst false
*/
	function _check_cms_edit_privilege() { global $sTable;
		// SPEZIALFALL CMS (wenn der aktuelle Nutzer kein publish-recht hat)
		if ($this->oPerm->getModule() == "cms" && 
			(!$this->oPerm->hasPriv('edit') || ($this->oPerm->hasPriv('edit') && !$this->oPerm->hasPriv('publish')))) {
			if (	(isset($this->aData['flag_online']) && $this->aData['flag_online'] == '1')
				||	(isset($this->aData['flag_online_'.$this->sLang]) && $this->aData['flag_online_'.$this->sLang] == '1')
				||	($sTable == $this->aENV['table']['cms_standard'])
				) { // edit-privileg beschneiden, wenn datensatz schon freigegeben wurde!
				$this->oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
				return false;
			}
		}
		return true;
	}

/**
* Wenn diese Klasse im CMS eingesetzt wird: Mit dieser Methode einige in diesem Zusammenhang wichtige Vars setzen!
* 
* Beispiel:
* <pre><code>
* $oForm->set_cms_vars('client', $navi_id); // params: $sTemplate[,$navi_id=NULL][,$bWithPreview=true]
* </code></pre>
*
* @access   public
* @param	boolean	true wenn edit-privileg erhalten bleibt, sonst false
*/
	function set_cms_vars($sTemplate, $navi_id=NULL, $bWithPreview=true) {
		if (empty($sTemplate)) return false;
		$this->sCmsTemplate		= $sTemplate;
		$this->sCmsNaviId		= $navi_id;
		$this->bCmsWithPreview	= $bWithPreview;
	}

# -------------------------------------------------------------------------------- JS-PRUEFUNG

/**
* Fuehrt eine JS Pruefung des dieser Methode uebergebenen Formularfeldes durch. 
* ACHTUNG: Funktioniert NUR mit "start_tag()" zusammen un muss VOR dessen Aufruf erfolgen! 
* 
* Beispiel:
* <pre><code>
* $oForm->check_field("title_".$weblang, $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
* $oForm->check_field("subtitle_".$weblang, $aMSG['form']['subtitle'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
* $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions, 'POST', 'editForm'); // params: [$sAction=''][,$sUrlParams=''][,$sMethod=''][,$sName=''][,$sExtra='']
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	string	[$sAlertName]	im alert-fenster angezeigter Name des Formularfeldes (optional - default: $sFieldname)
* @param	string	[$sCheck]		[FILLED|NaN|NaNFILLED] (optional - default: 'FILLED')
* @return	js-code
*/
	function check_field($sFieldname, $sAlertName='', $sCheck='FILLED',$bWithAData=true) {
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if ($sAlertName=='') {$sAlertName = $sFieldname; };
		$sCheck = strToLower($sCheck);
		// code
		if($bWithAData) {
		
			$sJS  = "obj=elements['aData[".$sFieldname."]'];\n";
		
		}
		else {
		
			$sJS  = "obj=elements['".$sFieldname."'];\n";
			
		}
		switch ($sCheck) {

			case 'nan':			// not a number
				$sJS .= "if(isNaN(obj.value))";
				$sJS .= " {ok=false;m=' ".$this->aMSG['err']['part_check'][$this->sLang].$sAlertName." ';mfocus=obj;}\n";
				break;
			case 'nanfilled':	// not a number AND filled
				$sJS .= "if(!obj.value)"; // check FILLED first...
				$sJS .= " {ok=false;m=' ".$this->aMSG['err']['part_insert'][$this->sLang].$sAlertName." ';mfocus=obj;}\n";
				$sJS .= "if(isNaN(obj.value))"; // ... then NaN
				$sJS .= " {ok=false;m=' ".$this->aMSG['err']['part_check'][$this->sLang].$sAlertName." ';mfocus=obj;}\n";
				break;
			case 'pos_int':	// wert muss integer und > 0 sein
				$sJS .= "if(!obj.value)"; // check FILLED first...
				$sJS .= " {ok=false;m=' ".$this->aMSG['err']['part_insert'][$this->sLang].$sAlertName." ';mfocus=obj;}\n";
				$sJS .= "if(isNaN(obj.value) || obj.value < 1)";
				$sJS .= " {ok=false;m=' ".$this->aMSG['err']['part_check'][$this->sLang].$sAlertName." ';mfocus=obj;}\n";
				break;
			default:			// default = filled
				$sJS .= "if(!obj.value)";
				$sJS .= " {ok=false;m=' ".$this->aMSG['err']['part_insert'][$this->sLang].$sAlertName." ';mfocus=obj;}\n";
				break;
		}
		// output
		$this->alert = $sJS.$this->alert; // vorne dranhaengen
	}


# -------------------------------------------------------------------------------- FORM TAGS

/**
* Baut die von dieser Klasse benoetigten JS-Funktionen und den oeffnenden Form-Tag des HTML-Formulars. 
* Diese Methode zu verwenden ist PFLICHT! ;-) 
* Ausserdem werden die GET-Parameter dieser Seite zentral gespeichert (z.B. fuer buttons ganz wichtig)!) 
* 
* Beispiel:
* <pre><code>
* echo $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
* </code></pre>
*
* @access	public
* @param	string	[$sAction]		"action" Attribut des Form -Tags (optional - default: PHP_SELF)
* @param	string	[$sUrlParams]	speichert die GET-Parameter dieser Seite zentral ab (optional - default: '')
* @param	string	[$sMethod]		"method" Attribut des Form -Tags (optional - default: 'post')
* @param	string	[$sName]		"name" Attribut des Form -Tags (optional - default: 'editForm')
* @param	string	[$sExtra]		zusaetzliche Attribute (z.B. JS-Aufruf) fuer den Form-Tag (optional - default: '')
* @return	html-code
*/
	function start_tag($sAction='', $sUrlParams='', $sMethod='post', $sName='editForm', $sExtra='') {
		global $sTable;
		
		// check vars
		if (!$sAction) { $sAction = $_SERVER['PHP_SELF']; }
		if ($sUrlParams != '') {
			$this->sUrlParams = '?'.substr($sUrlParams, 1);
			if ($sMethod == 'get') {
				// wenn GET -> sUrlParams zu hiddenfields umwandeln
				$aUrlParams = explode('&', substr($sUrlParams, 1));
				$hiddenfields = '';
				foreach($aUrlParams as $param) {
					$aParam = explode('=', $param);
					$hiddenfields .= '<input type="hidden" name="'.$aParam[0].'" value="'.$aParam[1].'">'."\n";
				}
			}
		}
		
		if ($sName != '') { $this->sName = $sName; }
		$sNameAtt = (!empty($this->sName)) ? ' name="'.$sName.'" id="'.$sName.'"' : '';
		if (!empty($sExtra)) { $sExtra = ' '.trim($sExtra); }
		global $aPageInfo,$weblang;

		// Form-Tag
		$sFormTag = '<form action="'. $sAction.$this->sUrlParams.'" enctype="multipart/form-data" method="'.$sMethod.'"'.$sNameAtt.$sExtra.">\n";
		$sFormTag .= $hiddenfields;
		
		// schreibt die von dieser Klasse benoetigten JS-Funktionen VOR den oeffnenden Form-Tag
		$sJSstart = '<script language="JavaScript" type="text/javascript">'."\n";
		if (!empty($this->sCmsTemplate) && $this->bCmsWithPreview == true) {
		$sJSstart .= "function previewWin() {\n";
		#$sJSstart .= "	preview = window.open('preview.php?page=".$aPageInfo['filename']."&template=template.".$this->sCmsTemplate.".php&sNaviId=".$this->aData['navi_id']."&id=".$this->aData['id']."','preview');\n";
		$preview_params = ($this->sCmsTemplate != 'standard') ? '&id='.$this->aData['id'].'&table='.$sTable : '';
		// bugfix für whatever 27/10/06
		// $sJSstart .= "	preview = window.open('cms_preview.php?page=".urlencode($aPageInfo['filename']).$preview_params."','preview','toolbar=0,status=1,scrollbars=1,resizable=1');\n";		
		$sJSstart .= "	preview = window.open('cms_preview.php?page=".urlencode($aPageInfo['filename']).$preview_params."&weblang=$weblang','preview','toolbar=0,status=1,scrollbars=1,resizable=1');\n";
		$sJSstart .= "	preview.focus();\n";
		$sJSstart .= "}\n";
		}
		$sJSstart .= "function checkCopyFields() {\n";
		$sJSstart .= "	var check = window.confirm('".$this->aMSG['form']['confirmcopy'][$this->sLang]."');\n";
		$sJSstart .= "	return check;\n";
		$sJSstart .= "} \n";
		$sJSstart .= "function confirmClear() {\n";
		$sJSstart .= "	var chk = window.confirm('".$this->aMSG['err']['clear'][$this->sLang]."');\n";
		$sJSstart .= "	return(chk);\n";
		$sJSstart .= "}\n";
		$sJSstart .= "function confirmDelete() {\n";
		$sJSstart .= "	var chk = window.confirm('";
		$sJSstart .= ($this->js_delete_msg == '') ? $this->aMSG['err']['delete_rec'][$this->sLang] : $this->js_delete_msg; // standard oder custom message
		$sJSstart .= "');\n";
		$sJSstart .= "	return(chk);\n";
		$sJSstart .= "}\n";
		$sJSstart .= "function confirmDeleteAll() {\n";
		$sJSstart .= "	var chk = window.confirm('".$this->aMSG['err']['delete_all'][$this->sLang]."');\n";
		$sJSstart .= "	return(chk);\n";
		$sJSstart .= "}\n";
		$sJSstart .= "function checkReqFields() { var m,ok,obj,mfocus,frm; ok=true;\n";
		$sJSstart .= (!empty($this->sName)) ? "with (document.forms['".$this->sName."']) {\n" : "with (document.forms[0]) {\n";
		### hier werden die alerts ausgegeben ###
		$sJSend = "if (!ok) {\n";
		$sJSend .= "	if (m) {alert(m);}\n";
		$sJSend .= "	if (mfocus) {mfocus.focus();}\n";
		$sJSend .= "	return false; stop();\n";
		$sJSend .= "} else {".$this->js_true_action."}}}\n";
		// hilfsfunktion: datumsformat ueberpruefen und ggf. ausbessern
		$sJSend .= "function formatDateField(field, bWithDayname) { 
	if (field.value == '') return;
	field.value = field.value.replace(/(-|\/|,)/, '.'); // dash + slash + comma gegen dot ersetzen
	var trenner = '.';
	var aDatePart = field.value.split(trenner);
	while (aDatePart.length > 3) { // ggf. wochentagsnamen am anfang rausnehmen
		aDatePart.shift();
	}
	if (aDatePart.length == 3) { // alles ausser zahlen entfernen
		aDatePart[0] = aDatePart[0].replace(/(\D)*/, '');
		aDatePart[1] = aDatePart[1].replace(/(\D)*/, '');
		aDatePart[2] = aDatePart[2].replace(/(\D)*/, '');
	}
	if (aDatePart.length < 3) { // wenn falsches Format: aktuelles Datum einfuegen
		var d = new Date(); aDatePart = new Array();
		aDatePart[0] = d.getDate(); aDatePart[0] = aDatePart[0].toString();
		aDatePart[1] = parseInt(d.getMonth() + 1); aDatePart[1] = aDatePart[1].toString();
		aDatePart[2] = d.getFullYear();
	}
	// ggf. tag korrigieren
	if (aDatePart[0] < 10 && aDatePart[0].length == 1) { aDatePart[0] = '0' + aDatePart[0]; }
	if (aDatePart[0] > 31) { aDatePart[0] = 31; }
	// ggf. monat korrigieren
	if (aDatePart[1] < 10 && aDatePart[1].length == 1) { aDatePart[1] = '0' + aDatePart[1]; }
	if (aDatePart[1] > 12) { aDatePart[1] = 12; }
	// ggf. jahr korrigieren
	if (aDatePart[2] < 100) { aDatePart[2] = '20' + aDatePart[2]; }
	// Output
	if (bWithDayname == true) { // ggf. mit Wochentagname
		d2 = new Date(aDatePart[2], (aDatePart[1] - 1), aDatePart[0]); weekday = d2.getDay()
		aWdName = new Array('".implode ("','", $this->oDate->aDaynameShort['de'])."');
		field.value = aWdName[weekday] + ', ' + aDatePart.join(trenner);
	} else {
		field.value = aDatePart.join(trenner);
	}
}\n";
		$sJSend .= "</script>\n";
		
		// output
		return $sJSstart.$this->alert.$sJSend.$sFormTag;
	}

/**
* Gibt den schliessenden Form-Tag des HTML-Formulars zurueck. 
* --> tatsaechlich ist jedoch der Aufruf dieser Funktion MEHR Tipp-arbeit als pures HTML :(
* 
* @access	public
* @return	html-code
*/
	function end_tag() {
		$sFormTag = '</form>';
		
		// output
		return $sFormTag;
	}

/**
* Gibt die Pflicht-Hidden-Fields zurueck. 
* 
* Beispiel:
* <pre><code>
* echo $oForm->default_hidden_fields(); // params: -
* </code></pre>
*
* @access	public
* @return	html-code
*/
	function default_hidden_fields() {
		
		// Pflicht-Hidden-Fields
		$sHiddenFields = '<input type="hidden" name="dummyIEwordcrap" value="0">'."\n"; // fuer seltsame WinIE-phaenomaene
		$sHiddenFields .= '<input type="hidden" name="remoteSave" value="0">'."\n";
		if (isset($this->aData['id'])) {
			$sHiddenFields .= '<input type="hidden" name="aData[id]" value="'.$this->aData['id'].'">'."\n";
		}
		// NUR CMS
		if (isset($this->aData['preview_ref_id']) && $this->bCmsWithPreview == true) {
			$sHiddenFields .= '<input type="hidden" name="aData[preview_ref_id]" value="'.$this->aData['preview_ref_id'].'">'."\n";
		}
		if (isset($this->sCmsNaviId)) {
			$sHiddenFields .= '<input type="hidden" name="aData[navi_id]" value="'.$this->sCmsNaviId.'">'."\n";
		}
		
		// output
		return $sHiddenFields;
	}

# -------------------------------------------------------------------------------- COMMON ELEMENTS

/**
* Baut kompletten <input type="password"> (mit beachtung der nutzer-rechte).
*
* Beispiel:
* <pre><code>
* echo $oForm->password("password"); // params: $sFieldname[,$nMaxlength=50][,$nSize=57][,$sDefault=''][,$sExtra='']
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	integer	[$nMaxlength]	(optional)	(default: 150)
* @param	integer	[$nSize]		(optional)	(default:  57)
* @param	string	[$sDefault]		(optional)	(default:  '')
* @param	string	[$sExtra]		(optional)	(default:  '')
* @return	html-code
*/
	function password($sFieldname, $nMaxlength=50, $nSize=57, $sDefault='', $sExtra='') {
		
		// check vars
		if (!$sFieldname) die('NO sFieldname!');
		
		// JS fuer Focus-Problem Flash-Editor hinzufuegen
		$sExtra .= " onClick=\"setNewFocus('".$this->sName."', 'aData[".$sFieldname."]')\"";
		
		// code
		if ($this->bEditMode) {
			$sStr = form_web::password($sFieldname, $nMaxlength, $nSize, $sDefault, $sExtra);
		} else {
			$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
			$countChars = strlen($sValue);
			$sStr = '<span class="text">'.str_repeat('*', $countChars)."</span>\n";
		}
		
		// output
		return $sStr;
	}

/**
* Baut kompletten <input type="text"> (mit beachtung der nutzer-rechte).
*
* Beispiel:
* <pre><code>
* echo $oForm->textfield("title_".$weblang, '250'); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra='']
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	integer	[$nMaxlength]	(optional)	(default: 150)
* @param	integer	[$nSize]		(optional)	(default:  57)
* @param	string	[$sDefault]		(optional)	(default:  '')
* @param	string	[$sExtra]		(optional)	(default:  '')
* @return	html-code
*/
	function textfield($sFieldname, $nMaxlength=150, $nSize=57, $sDefault='', $sExtra='') {
		
		// check vars
		if (!$sFieldname) die('NO sFieldname!');
		
		// JS fuer Focus-Problem Flash-Editor hinzufuegen
		$sExtra .= " onClick=\"setNewFocus('".$this->sName."', 'aData[".$sFieldname."]')\"";
		
		// code
		if ($this->bEditMode) {
			$sStr = form_web::textfield($sFieldname, $nMaxlength, $nSize, $sDefault, $sExtra);
		} else {
			$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
			$sStr = (strstr($sFieldname, 'title')) 
				? '<span class="text"><span class="title">'.$sValue."<br></span></span>\n" 
				: '<span class="text">'.$sValue."</span>\n";
		}
		
		// output
		return $sStr;
	}

/**
* Baut komplette textarea OHNE TOOL-Button (mit beachtung der nutzer-rechte).
*
* Beispiel:
* <pre><code>
* echo $oForm->textarea("description_".$weblang, 2); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra='']
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	integer	[$nRows]	(optional)	(default: 10)
* @param	integer	[$nCols]	(optional)	(default: 43)
* @param	string	[$sDefault]	(optional)	(default:  '')
* @param	string	[$sExtra]	(optional)	(default:  '')
* @return	html-code
*/
	function textarea($sFieldname, $nRows=10, $nCols=78, $sDefault='', $sExtra='') {
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		
		// JS fuer Focus-Problem Flash-Editor hinzufuegen
		$sExtra .= " onClick=\"setNewFocus('".$this->sName."', 'aData[".$sFieldname."]')\"";
		
		// code
		if ($this->bEditMode) {
			$sStr = form_web::textarea($sFieldname, $nRows, $nCols, $sDefault, $sExtra);
		} else {
			$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
			$sValue = (empty($sValue)) ? $sDefault : $sValue; // wenn kein value dann ggf. default
			$sStr = '<span class="text">'.nl2br($sValue)."<br><br></span>\n";
		}
		
		// output
		return $sStr;
	}

/**
* Baut Flash-editor als Alternative zu einer textarea (mit beachtung der nutzer-rechte).
* 
* ACHTUNG: 
* benoetigt folgende zeile vor der db-speicherung bei Verwendung der "class.cms_content"-Klasse: 
* <pre>
* // Feld, das den flash-editor benutzt, definieren (default: KEIN Flasheditor-Feld)
* $oCmsContent->setFlashEditorField('copytext_'.$weblang); // params: $sFieldname
* // ...oder eine entsprechende Bearbeitung der Textvariable auf manuellem Wege...
* </pre>
* 
* NOTE: Der Flasheditor benoetigt ausserdem die Datei "xml_data.php" (im gleichen Verzeichnis wie die aufrufende Datei).
* Diese Datei wiederum benoetigt die beiden Funktionen "prepareHtmlTagsForFlashEditor()" und 
* "stripForFlash()" aus dem Inkludefile "func.admin_dommon.php".
*
* Beispiel:
* <pre><code>
* echo $oForm->textarea_flash($sFieldname, $sTable, $aData['id']); // params: $sFieldname,$sTable,$id[,$editMode='large']
* </code></pre>
*
* @access	public
* @global	object	$oSess				("class.session.php")
* @global	string	$aTexteditLayout	("setup.php")
* @global	string	$aTexteditBg		("setup.php")
* @param	string	$sFieldname
* @param	string	$sTable
* @param	string	$id
* @param	string	$editMode	['large'|'small']
* @return	html-code
*/
	function textarea_flash($sFieldname, $sTable, $id, $editMode='large') {
		global $aTexteditLayout, $aTexteditBg, $oSess, $weblang;
		if (!isset($id) || empty($id)) { $id = 'none'; }
		$sStr = "";
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = !empty($this->aData[$sFieldname]) ? $this->aData[$sFieldname] : '';
		
		/* Der Content-Text wird von "cms/xml_data.php" bereitgestellt und vom Flash-Texteditor selbstaendig gezogen! */
		
		// code
		if ($this->bEditMode) {
			
			// bei zu grossen Textmengen der Flash-Editor nicht!
			if (strlen($sValue) > 1000000) {
				return $this->textarea($sFieldname, 30);
			}
			// bei HTTPS funktioniert auf den IEs der Flash-Editor nicht!
			if ($_SERVER['HTTPS'] == 'on' && $this->oBrowser->isIE() && $this->oBrowser->isWin()) {
				return $this->textarea($sFieldname);
			}
			// identifiziere opera browser
			$bOpera = ($this->oBrowser->isOpera()) ? "true" : "false";
			
			if ($oSess->get_var('bHasFlash') == 'true') { // flashtest (cms_portal.php) ist in der admin-session gespeichert
				$sParams = '?vTable='.$sTable.'&vId='.$id.'&vField='.$sFieldname.'&sLayout='.urlencode($aTexteditLayout[$this->sLang]).'&hiddenField=aData['.$sFieldname.']&sysLang='.$this->sLang.'&contentLang='.$weblang.'&editMode='.$editMode.'&vOpera='.$bOpera;
				$sHeight = ($editMode == 'small') ? 85 : 200;
				
				// VBScript
				if ($this->oBrowser->isWin() && $this->oBrowser->isIE()) { 	// nur IE auf Win benoetigt VBScript-Steuerung fuer Flash-Editor
					$sStr .= '<script language="VBScript">'."\n";
					$sStr .= 'Sub '.$sFieldname.'_FSCommand(ByVal command, ByVal args)'."\n\t";
					$sStr .= 'call texteditor_DoFSCommand(command, args)'."\n";
					$sStr .= 'end sub'."\n";
					$sStr .= '</script>'."\n";
				}
				// Hidden-field
				$sStr .= '<input type="hidden" name="aData['.$sFieldname.']" value="">'."\n";
				// JS, damit das Hidden-field schon gefuellt ist bevor der Flashfilm geladen ist (fuer langsame Rechner)
				$sStr .= '<script language="JavaScript" type="text/javascript">'."\n";
				$sStr .= 'set("'.str_replace(array("\r","\n"), '', addslashes($sValue)).'", "aData['.$sFieldname.']")'."\n";
				
				// Flashfilm
				$sStr .= 'var oTag = "";'."\n";
				$sStr .= 'oTag += \'<OBJECT classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" CODEBASE="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" WIDTH="500" HEIGHT="'.$sHeight.'" ID="'.$sFieldname.'" ALIGN="">\';'."\n\t";
				$sStr .= 'oTag += \'<PARAM NAME="allowScriptAccess" VALUE="sameDomain" />\';'."\n\t";
				$sStr .= 'oTag += \'<PARAM NAME="movie" VALUE="texteditor.swf'.$sParams.'" />\';'."\n\t";
				$sStr .= 'oTag += \'<PARAM NAME="quality" VALUE="high" />\';'."\n\t";
				$sStr .= 'oTag += \'<PARAM NAME="scale" VALUE="noscale" />\';'."\n\t";
				$sStr .= 'oTag += \'<PARAM NAME="salign" VALUE="TL" />\';'."\n\t";
				$sStr .= 'oTag += \'<PARAM NAME="bgcolor" VALUE="'.$aTexteditBg[$this->sLang].'" />\';'."\n\t";
				$sStr .= 'oTag += \'<EMBED SRC="texteditor.swf'.$sParams.'" QUALITY="high" SCALE="noscale" BGCOLOR="'.$aTexteditBg[$this->sLang].'" WIDTH="500" HEIGHT="'.$sHeight.'" NAME="'.$sFieldname.'" allowScriptAccess="sameDomain" SALIGN="TL" ALIGN="" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>\';'."\n";
				$sStr .= 'oTag += \'</OBJECT>\';'."\n";
				$sStr .= 'embedFlash(oTag);'."\n";
				$sStr .= '</script>'."\n";
			} else {
				$sStr = $this->textarea($sFieldname, 10, 75);
			}
		} else {
			$sStr = '<span class="text">'.nl2br($sValue)."<br><br></span>\n";
		}
		
		// output
		return $sStr;
	}
/**
* Baut komplette select-/delete-buttons und thumbnail/icon etc. fuer 'slideshow-mediafiles-auswaehlen' 
* (mit beachtung der nutzer-rechte) zur erstellung einer slideshow. 
*
* Dazu wird das popup_slideshow geoeffnet und die Daten in einer zusätzlichen Tabelle abgespeichert. 
*
* Beispiel:
* <pre><code>
*	require_once($aENV['path']['global_service']['unix'].'class.Slideshow.php');
*	require_once($aENV['path']['global_service']['unix'].'class.mediadb.php');
*	if(!is_object($oMediadb)) {
*		$oMediadb =& new mediadb();
*	}
*	$oForm->setMdb($oMediadb);
*
* echo $oForm->slideshow_tools("image", $sTable, $aData['id']); // params: $sFieldname,$sTable,$sId[,$nWidth=''][,$nHeight=''][,$bMustfit=0]
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	string 	$fkId	(kann leer sein) - wird benoetigt, um zu sehen, ob es schon einen datensatz gibt
* @param	string	$sId	wird benoetigt um die Slideshow in der sys_slideshow Tabelle zu identifizieren.
* @param	string	$nWidth		(kann leer sein)
* @param	string	$nHeight	(kann leer sein)
* @param	string	$bMustfit	(0 = false (w/h ist maxsize), 1 = true (w/h ist exactsize))
* @return	html-code
*/
	function slideshowTool($sFieldname, $fkId, $sId,&$oDb, $nWidth='', $nHeight='', $bMustfit=0) {
		$oSlide =& new Slideshow($oDb,$this->Userdata);
		$oSlide->setMediadb($this->oMediadb);

		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if (!$sId) die("NO sId!");
		$sValue = $oSlide->getCommaSepratedSlide($sId,$fkId);
		$aValue = $oSlide->getSlideshow($sId,$fkId);
		if (empty($nWidth))		{ $nWidth = "''"; }
		if (empty($nHeight))	{ $nHeight = "''"; }
		// texte
		$MSG = array();
		$MSG['contains']['de'] = "Die ".$this->aENV['slideshow'][$this->sLang]." enthält";
		$MSG['contains']['en'] = "".$this->aENV['slideshow'][$this->sLang]." contains currently";
		$MSG['images']['de'] = "Bild(er)";
		$MSG['images']['en'] = "image(s)";
		$MSG['btn_edit']['de'] = "Editieren";
		$MSG['btn_edit']['en'] = "Edit";
		$MSG['btn_create']['de'] = "Erstellen";
		$MSG['btn_create']['en'] = "Create";
		$MSG['first_save']['de'] = "Bevor eine ".$this->aENV['slideshow'][$this->sLang]." erstellt werden kann, muss einmal gespeichert werden.";
		$MSG['first_save']['en'] = "Before you can create a ".$this->aENV['slideshow'][$this->sLang]." you have to save at least once.";
		$MSG['first_preview_save']['de'] = "Bevor die ".$this->aENV['slideshow'][$this->sLang]." bearbeitet werden kann, muss einmal gespeichert werden.";
		$MSG['first_preview_save']['en'] = "Before you can edit this ".$this->aENV['slideshow'][$this->sLang]." you have to save at least once.";
		
		$sStr = '';
		$anzahl = 0;
		$aImg = array();
		//echo debug($aValue);
		/*if(!is_array($aValue[0])) {
			for($i=0;is_array($aValue[$i]);$i++) {
				$aImg[$aValue[$i]['id']]['filename']	= $aValue[$i]['filename'];
				$aImg[$aValue[$i]['id']]['width']		= $aValue[$i]['width'];
				$aImg[$aValue[$i]['id']]['height']		= $aValue[$i]['height'];
				$aImg[$aValue[$i]['id']]['filesize']	= $this->oFile->format_filesize($aValue[$i]['filesize'], 'AUTO', 0);
			}
			if (function_exists("gd_info") && file_exists("../mdb/thumb.php")) {
			}
			else {
				$sStr = '<span class="text" id="'.$sFieldname.'">'.$MSG['contains'][$this->sLang].' '.($i+1).' '.$MSG['images'][$this->sLang].'.&nbsp;&nbsp;</span>';
			}
		}*/
		// code
		if (!empty($sValue) && isset($fkId)) { 
		// A: "edit" + es gibt bereits values
			$aImgID = explode(",",$sValue);
			$anzahl = count($aImgID);
			// mit installierter GD-Lib -> erzeuge thumbnails...
			if (function_exists("gd_info") && file_exists("../mdb/thumb.php")) {
				// get image-data into temp-array
				$this->oDb->query("SELECT id, filename, width, height, filesize 
									FROM ".$this->aENV['table']['mdb_media']." 
									WHERE id IN (".$sValue.")");
				while ($tmp = $this->oDb->fetch_array()) {
					$aImg[$tmp['id']]['filename']	= $tmp['filename'];
					$aImg[$tmp['id']]['width']		= $tmp['width'];
					$aImg[$tmp['id']]['height']		= $tmp['height'];
					$aImg[$tmp['id']]['filesize']	= $this->oFile->format_filesize($tmp['filesize'], 'AUTO', 0);
				}
				// printout thumbs in right order
				for ($i=0; $i<$anzahl; $i++) {
					$width = (!empty($aImg[$aImgID[$i]]['width'])) ? $aImg[$aImgID[$i]]['width'] : "''";
					$height = (!empty($aImg[$aImgID[$i]]['height'])) ? $aImg[$aImgID[$i]]['height'] : "''";
					
					$sStr .= '<a href="javascript:viewMediaWin(\''.$aImgID[$i].'\','.$width.','.$height.');" title="View Mediafile ['.$aImg[$aImgID[$i]]['filename'].' | '.$aImg[$aImgID[$i]]['filesize'].']">';
					$sStr .= $this->get_media_icon($aImg[$aImgID[$i]]['filename'], 'valign="bottom"').'</a>&nbsp;'; // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
				}
				$sStr .= '<br>';
			} else {
			// ... ansonsten schreibe Text
				$sStr = '<span class="text" id="'.$sFieldname.'">'.$MSG['contains'][$this->sLang].' '.$anzahl.' '.$MSG['images'][$this->sLang].'.&nbsp;&nbsp;</span>';
			}
			if ($this->bEditMode) {
				if ($this->bCmsWithPreview == true && (!isset($this->aData['preview_ref_id']) || $this->aData['preview_ref_id'] == 0)) {
					// wichtig: hidden-field mit bisherigen values zum kopieren i.d. preview-datensatz!
				//	$sStr .= '<input type="hidden" name="aData['.$sFieldname.']" id="aData['.$sFieldname.']" value="'.$sValue.'">';
					$sStr .= '<span class="small">'.$MSG['first_preview_save'][$this->sLang].'</span>';
				} else {
					$sStr .= '<input type="button" value="'.$MSG['btn_edit'][$this->sLang].'" name="btn[select_slideshow]" class="smallbut" onClick="javascript:selectNewSlideshowWin(\''.$fkId.'\',\''.$sId.'\',\''.$sFieldname.'\',\''.$this->sName.'\','.$nWidth.','.$nHeight.',\''.$bMustfit.'\')">';
				}
			}
		} else if (isset($fkId) && !empty($fkId)) { 
		// B: "edit" + noch keine values
			if ($this->bEditMode) {
				if ($this->bCmsWithPreview == true && (!isset($this->aData['preview_ref_id']) || $this->aData['preview_ref_id'] == 0)) {
					$sStr .= '<span class="small">'.$MSG['first_preview_save'][$this->sLang].'</span>';
				} else {
					$sStr = '<input type="button" value="'.$MSG['btn_create'][$this->sLang].'" name="btn[select_slideshow]" class="smallbut" onClick="javascript:selectNewSlideshowWin(\''.$fkId.'\',\''.$sId.'\',\''.$sFieldname.'\',\''.$this->sName.'\','.$nWidth.','.$nHeight.',\''.$bMustfit.'\')">';
				}
			}
		} else {
		// C: "new"
			if ($this->bEditMode) {
				$sStr = '<span class="small">'.$MSG['first_save'][$this->sLang].'</span>';
			}
		}
		
		// output
		return $sStr;
	}
/**
* Baut komplette select-/delete-buttons und thumbnail/icon etc. fuer 'slideshow-mediafiles-auswaehlen' 
* (mit beachtung der nutzer-rechte) zur erstellung einer slideshow. 
*
* Dazu wird das popup_slideshow geoeffnet und nach beendeter auswahl EIN varchar-feld mit kommagetrennten IDs gefuellt. 
*
* Beispiel:
* <pre><code>
* echo $oForm->slideshow_tools("image", $sTable, $aData['id']); // params: $sFieldname,$sTable,$sId[,$nWidth=''][,$nHeight=''][,$bMustfit=0]
* </code></pre>
* @deprecated 1.1.3 - 25.03.2006 pls use slideshowTools('image',$fkid,$sId);
* @access	public
* @param	string	$sFieldname
* @param	string	$sValue	(kann leer sein)
* @param	string	$sTable (des datensatzes in dem die slideshow gespeichert werden soll)
* @param	string	$sId	(kann leer sein) - wird benoetigt, um zu sehen, ob es schon einen datensatz gibt
* @param	string	$nWidth		(kann leer sein)
* @param	string	$nHeight	(kann leer sein)
* @param	string	$bMustfit	(0 = false (w/h ist maxsize), 1 = true (w/h ist exactsize))
* @return	html-code
*/
	function slideshow_tools($sFieldname, $sTable, $sId, $nWidth='', $nHeight='', $bMustfit=0) {
				
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if (!$sTable) die("NO sTable!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		if (empty($nWidth))		{ $nWidth = "''"; }
		if (empty($nHeight))	{ $nHeight = "''"; }
		// texte
		$MSG = array();
		$MSG['contains']['de'] = "Die ".$this->aENV['slideshow'][$this->sLang]." enthält";
		$MSG['contains']['en'] = "".$this->aENV['slideshow'][$this->sLang]." contains currently";
		$MSG['images']['de'] = "Bild(er)";
		$MSG['images']['en'] = "image(s)";
		$MSG['btn_edit']['de'] = "Editieren";
		$MSG['btn_edit']['en'] = "Edit";
		$MSG['btn_create']['de'] = "Erstellen";
		$MSG['btn_create']['en'] = "Create";
		$MSG['first_save']['de'] = "Bevor eine ".$this->aENV['slideshow'][$this->sLang]." erstellt werden kann, muss einmal gespeichert werden.";
		$MSG['first_save']['en'] = "Before you can create a ".$this->aENV['slideshow'][$this->sLang]." you have to save at least once.";
		$MSG['first_preview_save']['de'] = "Bevor die ".$this->aENV['slideshow'][$this->sLang]." bearbeitet werden kann, muss einmal gespeichert werden.";
		$MSG['first_preview_save']['en'] = "Before you can edit this ".$this->aENV['slideshow'][$this->sLang]." you have to save at least once.";
		
		$sStr = '';
		$anzahl = 0;
		$aImg = array();
		
		// code
		if (!empty($sValue) && isset($sId)) { 
		// A: "edit" + es gibt bereits values
			$aImgID = explode(",",$sValue);
			$anzahl = count($aImgID);
			// mit installierter GD-Lib -> erzeuge thumbnails...
			if (function_exists("gd_info") && file_exists("../mdb/thumb.php")) {
				// get image-data into temp-array
				$this->oDb->query("SELECT id, filename, width, height, filesize 
									FROM ".$this->aENV['table']['mdb_media']." 
									WHERE id IN (".$sValue.")");
				while ($tmp = $this->oDb->fetch_array()) {
					$aImg[$tmp['id']]['filename']	= $tmp['filename'];
					$aImg[$tmp['id']]['width']		= $tmp['width'];
					$aImg[$tmp['id']]['height']		= $tmp['height'];
					$aImg[$tmp['id']]['filesize']	= $this->oFile->format_filesize($tmp['filesize'], 'AUTO', 0);
				}
				// printout thumbs in right order
				for ($i=0; $i<$anzahl; $i++) {
					$width = (!empty($aImg[$aImgID[$i]]['width'])) ? $aImg[$aImgID[$i]]['width'] : "''";
					$height = (!empty($aImg[$aImgID[$i]]['height'])) ? $aImg[$aImgID[$i]]['height'] : "''";
					
					$sStr .= '<a href="javascript:viewMediaWin(\''.$aImgID[$i].'\','.$width.','.$height.');" title="View Mediafile ['.$aImg[$aImgID[$i]]['filename'].' | '.$aImg[$aImgID[$i]]['filesize'].']">';
					$sStr .= $this->get_media_icon($aImg[$aImgID[$i]]['filename'], 'valign="bottom"').'</a>&nbsp;'; // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
				}
				$sStr .= '<br>';
			} else {
			// ... ansonsten schreibe Text
				$sStr = '<span class="text" id="'.$sFieldname.'">'.$MSG['contains'][$this->sLang].' '.$anzahl.' '.$MSG['images'][$this->sLang].'.&nbsp;&nbsp;</span>';
			}
			if ($this->bEditMode) {
				if ($this->bCmsWithPreview == true && (!isset($this->aData['preview_ref_id']) || $this->aData['preview_ref_id'] == 0)) {
					// wichtig: hidden-field mit bisherigen values zum kopieren i.d. preview-datensatz!
					$sStr .= '<input type="hidden" name="aData['.$sFieldname.']" id="aData['.$sFieldname.']" value="'.$sValue.'">';
					$sStr .= '<span class="small">'.$MSG['first_preview_save'][$this->sLang].'</span>';
				} else {
					$sStr .= '<input type="button" value="'.$MSG['btn_edit'][$this->sLang].'" name="btn[select_slideshow]" class="smallbut" onClick="javascript:selectSlideshowWin(\''.$sId.'\',\''.$sFieldname.'\',\''.$sTable.'\',\''.$this->sName.'\','.$nWidth.','.$nHeight.',\''.$bMustfit.'\')">';
				}
			}
		} else if (isset($sId) && !empty($sId)) { 
		// B: "edit" + noch keine values
			if ($this->bEditMode) {
				if ($this->bCmsWithPreview == true && (!isset($this->aData['preview_ref_id']) || $this->aData['preview_ref_id'] == 0)) {
					$sStr .= '<span class="small">'.$MSG['first_preview_save'][$this->sLang].'</span>';
				} else {
					$sStr = '<input type="button" value="'.$MSG['btn_create'][$this->sLang].'" name="btn[select_slideshow]" class="smallbut" onClick="javascript:selectSlideshowWin(\''.$sId.'\',\''.$sFieldname.'\',\''.$sTable.'\',\''.$this->sName.'\','.$nWidth.','.$nHeight.',\''.$bMustfit.'\')">';
				}
			}
		} else {
		// C: "new"
			if ($this->bEditMode) {
				$sStr = '<span class="small">'.$MSG['first_save'][$this->sLang].'</span>';
			}
		}
		
		// output
		return $sStr;
	}

/**
* Baut komplette select-/delete-buttons und thumbnail/icon etc. fuer 'mediafile-(aus der media-db-)auswaehlen' (mit beachtung der nutzer-rechte). 
*
* Der optionale zweite Parameter bestimmt welche Dateitypen im Popup "selectmedia" zur Auswahl gezeigt werden. 
* Moegliche Angaben sind ein oder mehrere kommagetrennte MediaTypen. Wird "all" (=default) angegeben, werden alle ausgegeben.
*
* Beispiel:
* <pre><code>
* echo $oForm->media_tools("img", "flash"); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0]
* echo $oForm->media_tools("img2", "flash,image"); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0]
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	string	[$sMediaType]	(optional)	possible values: [ image | flash | pdf | zip | all ]	(default:'all')
* @param	string	$nWidth		(kann leer sein)
* @param	string	$nHeight	(kann leer sein)
* @param	string	$bMustfit	(0 = false (w/h ist maxsize), 1 = true (w/h ist exactsize))
* @return	html-code
*/
	function media_tools($sFieldname, $sMediaType='all', $nWidth='', $nHeight='', $bMustfit=0) {
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		$sMediaType = strToLower(trim($sMediaType));
		$sTable = $this->aENV['table']['mdb_media'];
		if (empty($nWidth))		{ $nWidth = "''"; }
		if (empty($nHeight))	{ $nHeight = "''"; }
		// texte
		$MSG = array();
		$MSG['btn_replace']['de'] = "Ersetzen";
		$MSG['btn_replace']['en'] = "Replace";
		$MSG['btn_remove']['de'] = "Leeren";
		$MSG['btn_remove']['en'] = "Clear";
		$MSG['btn_select']['de'] = "Auswählen";
		$MSG['btn_select']['en'] = "Select";
		$MSG['no_selected']['de'] = "Kein Mediafile ausgewählt.";
		$MSG['no_selected']['en'] = "No mediafile selected.";
		$MSG['alt']['de'] = "Titel";
		$MSG['alt']['en'] = "Title";
		$MSG['btn_edit_title']['de'] = "Mediafile editieren";
		$MSG['btn_edit_title']['en'] = "Edit mediafile";
		
		// hidden field = container for media-id
		$sStr = '<input type="hidden" name="aData['.$sFieldname.']" value="'.$sValue.'" id="'.$sFieldname.'">'."\n";
		
		// if mediadata is already selected
		if ($sValue) {
			
			// open table
			$sStr .= '<table border="0" cellspacing="1" cellpadding="2" class="tabelle"><tr><td>';
			
			// get media-data
			$this->oDb->query("SELECT id, name, filename, width, height, description, filesize FROM ".$sTable." WHERE id='".$sValue."'");
			$aMedia = $this->oDb->fetch_array();
			if (empty($aMedia['width']))	{$aMedia['width'] = "600";}
			if (empty($aMedia['height']))	{$aMedia['height'] = "500";}
			// filesize
			if(file_exists($aMedia['filename'])) {
				$sFilesize	= $this->oFile->format_filesize($aMedia['filesize'], 'AUTO', 0);
			}				
			$type = $this->get_media_type($this->aData['filename']); // params: $sFilename
			if ($type == "image" || $type == "flash") {
			// link -> web-images
				$width = (!empty($aMedia['width'])) ? $aMedia['width'] : "''";
				$height = (!empty($aMedia['height'])) ? $aMedia['height'] : "''";
				$linktag .= '<a href="javascript:viewMediaWin(\''.$aMedia['id'].'\','.$width.','.$height.');" title="View Mediafile ['.$aMedia['filename'].' | '.$sFilesize.']">';
			} else {
			// link -> alle anderen media-typen
				$linktag .= '<a href="'.$this->aENV['path']['mdb_upload']['http'].$aMedia['filename'].'" target="_blank" title="View Mediafile ['.$aMedia['filename'].' | '.$sFilesize.']">';
			}
			$sStr .= $linktag.$this->get_media_icon($aMedia['filename']).'</a> '; // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
			
			if ($this->bEditMode) {
				
				// new table td
				$sStr .= '</td><td>';
				
				// text field shows description of mediafile
				$sStr .= '<span class="text">'.$linktag.shorten_text($aMedia['description'], 60).'</a></span><br>';
				
				// text field shows name of mediafile (for usability reason only - will not be saved!)
				$sStr .= '<input type="text" name="'.$sFieldname.'_title" value="'.$aMedia['name'].'" size="30" readonly> ';
				// replace / remove buttons
				$sStr .= '<input type="button" value="'.$MSG['btn_replace'][$this->sLang].'" name="btn[select_'.$sFieldname.']" class="smallbut" onClick="javascript:selectMediaWin(\''.$sMediaType.'\',\''.$sFieldname.'\',\''.$this->sName.'\','.$nWidth.','.$nHeight.',\''.$bMustfit.'\')"> ';
				if (!empty($this->sName)) {
					$sStr .= '<input type="button" value="'.$MSG['btn_remove'][$this->sLang].'" name="btn[delete_'.$sFieldname.']" class="smallbut" onClick="document.forms[\''.$this->sName.'\'][\'aData['.$sFieldname.']\'].value=0; document.forms[\''.$this->sName.'\'][\''.$sFieldname.'_title\'].value=\'\';">';
				} else {
					$sStr .= '<input type="button" value="'.$MSG['btn_remove'][$this->sLang].'" name="btn[delete_'.$sFieldname.']" class="smallbut" onClick="document.forms[0][\'aData['.$sFieldname.']\'].value=0; document.forms[0][\''.$sFieldname.'_title\'].value=\'\';">';
				}
			}
			
			// close table
			$sStr .= '</td></tr></table>';
		
		} else { // if NO mediadata is already selected
			
			if ($this->bEditMode) {
				$sStr .= '<input type="text" name="'.$sFieldname.'_title" id="'.$sFieldname.'_title" value="" size="50"  readonly> ';
				$sStr .= '<input type="button" value="'.$MSG['btn_select'][$this->sLang].'" name="btn['.$sFieldname.']" class="smallbut" onClick="javascript:selectMediaWin(\''.$sMediaType.'\',\''.$sFieldname.'\',\''.$this->sName.'\','.$nWidth.','.$nHeight.',\''.$bMustfit.'\')">';
			} else {
				$sStr .= '<span class="text"><small>'.$MSG['no_selected'][$this->sLang].'</small></span>';
			}
		
		} // END if ($sValue)
		
		// output
		return $sStr;
	}

/**
* Baut komplette select-/delete-buttons und icon etc. fuer MEHRERE 'pdf-(aus der media-db-)auswaehlen' (mit beachtung der nutzer-rechte). 
*
* Wird als optionaler zweiter Parameter $weblang ("de" oder "en") uebergeben, wird die Hintergrundfarbe an die aktuelle Sprachversion angepasst. 
* ACHTUNG: Um die PDFs speichern zu koennen, muessen sie mit der CmsContent-Methode "prepare_pdf_tools_data($sFieldname)" vorbereitet werden:
* <pre>
* // Feld, das die PDF-Tools benutzt, definieren (default: KEIN FPDF-Tools-Feld)
* $oCmsContent->setPdfToolsField('pdf_id'); // params: $sFieldname
* // ...oder eine entsprechende Bearbeitung der Variablen auf manuellem Wege...
* </pre>
*
* Beispiel:
* <pre><code>
* echo $oForm->pdf_tools("pdf_id"); // params: $sFieldname[,$sLang=false]
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	string	[$sLang]	(optional - default: false)
* @return	html-code
*/
	function pdf_tools($sFieldname, $sLang=false) {
		
		// vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue	= (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		$sTable	= $this->aENV['table']['mdb_media'];
		$i		= 0;
		$sStr	= '';
		// texte
		$MSG = array();
		$MSG['btn_remove']['de']	= "Entfernen";
		$MSG['btn_remove']['en']	= "Remove";
		$MSG['btn_select']['de']	= "Hinzufügen";
		$MSG['btn_select']['en']	= "Add";
		
		// if mediadata is already selected
		if ($sValue) {
			// vars
			$aPdfId		= explode(",", $sValue);
			$anzahl		= count($aPdfId);
			$aPDF		= array();
			$sTdClass	= ($sLang != false) ? ' class="lang'.$sLang.'"' : '';
			
			// open table
			$sStr .= '<table width="360" border="0" cellspacing="1" cellpadding="2" class="tabelle">';
			
			// get media-data
			$this->oDb->query("SELECT id, name, description, filename, filesize FROM ".$sTable." WHERE id IN (".$sValue.")");
			while ($tmp = $this->oDb->fetch_array()) {
				// description
				$aPDF[$tmp['id']]['title']		= (!empty($tmp['description'])) ? shorten_text($tmp['description'], 40) : $tmp['name'];
				// filename
				$aPDF[$tmp['id']]['filename']	= $tmp['filename'];
				// filesize
				$aPDF[$tmp['id']]['filesize']	= $this->oFile->format_filesize($tmp['filesize'], 'AUTO', 0);
			} #print_r($aPDF);
			
			// Ausgabe der schon gespeicherten PDFs
			foreach ($aPdfId as $id) {
				$sStr .= '<tr><td width="80%"'.$sTdClass.'><span class="text">'."\n";
				 // hidden-field fuer pdf_id-string
				$sStr .= '<input type="hidden" name="aData['.$sFieldname.']['.$i.']" value="'.$id.'">'."\n";
				// link zu PDF (in neuem Fenster)
				$sStr .= '<a href="'.$this->aENV['path']['mdb_upload']['http'].$aPDF[$id]['filename'].'" target="_blank" title="View Mediafile ['.$aPDF[$id]['filename'].' | '.$aPDF[$id]['filesize'].']">';
				// icon
				$sStr .= $this->get_media_icon($aPDF[$id]['filename']); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
				// name
				$sStr .= $aPDF[$id]['title'].'</a>&nbsp;</span></td>';
				// delete-checkbox
				if ($this->bEditMode) {
					$sStr .= '<td width="20%"'.$sTdClass.' align="right" nowrap><span class="text">&nbsp;<input type="checkbox" name="aData['.$sFieldname.']['.$i.']" value="" class="radiocheckbox">&nbsp;'.$MSG['btn_remove'][$this->sLang].'<br></span></td></tr>'."\n";
				}
				// zaehle hoch
				$i++;
			}
			// close table
			$sStr .= '</table>';
		} // END if ($sValue)
		
		
		if ($this->bEditMode) {
			// hidden field = container for NEW pdf-id
			$sStr .= '<input type="hidden" name="aData[new_'.$sFieldname.']" id="new_'.$sFieldname.'" value="">'."\n";
			// text field shows name of mediafile (for usability reason only - will not be saved!)
			$sStr .= '<input type="text" name="new_'.$sFieldname.'_title" value="" size="50" readonly> ';
			// add button
			$sStr .= '<input type="button" value="'.$MSG['btn_select'][$this->sLang].'" name="btn[select_'.$sFieldname.']" class="smallbut" onClick="javascript:selectMediaWin(\'pdf\',\'new_'.$sFieldname.'\',\''.$this->sName.'\')"> ';
		}
		
		// output
		return $sStr;
	}

/**
* Schreibt komplette select-/replace-buttons und thumbnail/icon etc. fuer 'mediafile-upload' auf der seite 'mdb_media_detail.php'.
* ACHTUNG: Benoetigt natuerlich vor den db-actions entsprechendes file-upload handling! (-> s. 'media_detail.php')
*
* Beispiel:
* <pre><code>
* echo $oForm->media_upload();
* </code></pre>
*
* @access	public
* @return	html-code
*/
	function media_upload($sExtra='') {
		
		// vars
		$sStr = '';
		$MSG = array(); // texte
		$MSG['btn_replace']['de'] = "Ersetzen";
		$MSG['btn_replace']['en'] = "Replace";
		
		// if media is in DB -> show icon/thumbnail and replace-button
		if (isset($this->aData['id']) && !empty($this->aData['id'])) {

			$type = $this->get_media_type($this->aData['filename']); // params: $sFilename

			if (!empty($type) ) {
				// link/thumb -> web-images
				$width = ($this->aData['width'] != '') ? $this->aData['width'] : "''";
				$height = ($this->aData['height'] != '') ? $this->aData['height'] : "''";
				$sStr .= '<a href="javascript:viewMediaWin(\''.$this->aData['id'].'\','.$width.','.$height.');" title="View Uploadfile">';
				$sStr .= $this->get_media_icon($this->aData['filename']); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
				$sStr .= '</a>&nbsp;&nbsp;<span class="small"><b>'.$this->aData['name'].'</b></span>'."\n";
			} 
			// + replace-button und file-field
			if ($this->bEditMode) {
				$sStr .= '<br><span class="small">'.$MSG['btn_replace'][$this->sLang].':<br></span><input type="file" size="25" name="uploadfile" '.$sExtra.'>';
			}
		} else { // new: only file-input field
			if ($this->bEditMode) {
				$sStr .= '<input type="file" size="25" name="uploadfile" '.$sExtra.'>';
			}
		}
		
		// output
		return $sStr;
	}

/**
* Schreibt einfachen 'file-upload' (file-input, select-/replace-buttons und thumbnail/icon etc.) auf einer beliebigen seite 
* (mit beachtung der nutzer-rechte).
* ACHTUNG: Benoetigt natuerlich vor den db-actions entsprechendes file-upload handling!
*
* Beispiel Funktionen:
* <pre><code>
* // DB-action: delete
* 	if (isset($btn['delete']) && $aData['id']) {
* 		// altes Bild in MediaDB loeschen
* 		$oMediadb->moduleDelete($aData['picture']); // params: $mdbId
* 		// delete data
* 		$oDb->make_delete($aData['id'], $sTable);
* 		// weiterleitung
* 		header("Location: ".$sViewerPage); exit;
* 	}
* // DB-action: save or update
* 	if (isset($btn['save']) || $remoteSave==1) {
* 		// Media-DB: ggf. altes Bild loeschen
* 		if (isset($_POST['delete_picture']) && !empty($_POST['existing_picture'])) {
* 			$oMediadb->moduleDelete($_POST['existing_picture']); // params: $mdbId
* 			$aData['picture'] = '';
* 		}
* 		// Media-DB: ggf. neues Bild hochladen
*		if (isset($_FILES['picture']['name']) && !empty($_FILES['picture']['name'])) {
*	 		$aData['picture'] = $oMediadb->moduleUpload('picture', $aData['firstname'].' '.$aData['surname'], $aMSG['topnavi']['users'][$syslang]); // params: $sUploadFieldname[,$sDescription=''][,$sKeywords='']
*		}
* 		if (!$alert) {
* 			if ($aData['id']) { // Update eines bestehenden Eintrags
* 				$oDb->make_update($aData, $sTable, $aData['id']);
* 			} else { // Insert eines neuen Eintrags
* 				$oDb->make_insert($aData, $sTable);
* 				$aData['id'] = $oDb->insert_id();
* 			}
* 		}
* </code></pre>
*
* Beispiel Formulareinbau:
* <pre><code>
* echo $oForm->file_upload('picture'); // params: [$sFieldname='uploadfile'][,$sPathKey='mdb_upload']
* </code></pre>
*
* @access	public
* @param	string	[$sFieldname]	(optional -> default:'uploadfile')
* @param	boolean	[$sPathKey]		(optional -> default:'mdb_upload')
* @return	html-code
*/
	function file_upload($sFieldname='uploadfile', $sPathKey='mdb_upload') {
		
		// vars
		$MSG = array(); // texte
		$MSG['btn_replace']['de'] = "Ersetzen";
		$MSG['btn_replace']['en'] = "Replace";
		$MSG['btn_delete']['de'] = "Löschen";
		$MSG['btn_delete']['en'] = "Delete";
		$MSG['no_selected']['de'] = "Keine Datei ausgewählt.";
		$MSG['no_selected']['en'] = "No file selected.";
		
		if (!$sFieldname) die("NO sFieldname!");
		$sValue	= (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		
		// ermittle filename des MediaDB-Files
		$sFilename	= $this->get_media_filename($sValue); // params: $nMediaId
		$sFilename	= (!empty($sFilename)) ? $sFilename:$sValue;

		// ermittle filesize
		if (file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
			$sFilesize	= $this->oFile->get_filesize($this->aENV['path'][$sPathKey]['unix'].$sFilename, 'AUTO', 0); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		}
		// if $sFilename -> show icon/thumbnail and replace-button
		if (!empty($sValue)) {
			$sStr = '<input type="hidden" name="existing_'.$sFieldname.'" id="existing_'.$sFieldname.'" value="'.$sValue.'">'."\n";
			if (file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
				$href = ($sPathKey == "protected") 
					? $this->aENV['path']['mdb']['http'].'popup_viewprotected.php?filename='.$sFilename 
					: $this->aENV['path'][$sPathKey]['http'].$sFilename;
				$sStr .= '<a href="'.$href.'" target="_blank" title="View Uploadfile ['.$sFilename.' | '.$sFilesize.']">';
				$sStr .= $this->get_media_icon($sFilename, '', $sPathKey); // params: $sFilename[,$sExtra=''][,$sPathKey='upload']
				$sStr .= '</a>'."\n";
			} else {
				$sStr .= 'File "'.$sFilename.'" does not exist!'."\n";
			}
			if ($this->bEditMode) {
				$sStr .= '&nbsp;&nbsp;<input type="checkbox" name="delete_'.$sFieldname.'" id="delete_'.$sFieldname.'" value="1" class="radiocheckbox">&nbsp;<label for="delete_'.$sFieldname.'" class="small">'.$MSG['btn_delete'][$this->sLang].'</label>'."\n";
				$sStr .= '<br><span class="small">'.$MSG['btn_replace'][$this->sLang].':<br> </span><input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'">';
			}
		} else { // new: only file-input field
			if ($this->bEditMode) {
				$sStr .= '<input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'">';
			} else {
				$sStr .= '<span class="text"><small>'.$MSG['no_selected'][$this->sLang].'</small></span>';
			}
		}
		
		// output
		return $sStr;
	}

// Und das Selbe noch mal mit upload in die mediadb
	function file_upload_mediadb($sFieldname='uploadfile',  $bProtected=false) {
		
		// vars
		$MSG = array(); // texte
		$MSG['btn_replace']['de'] = "Ersetzen";
		$MSG['btn_replace']['en'] = "Replace";
		$MSG['btn_delete']['de'] = "Löschen";
		$MSG['btn_delete']['en'] = "Delete";
		$MSG['no_selected']['de'] = "Keine Datei ausgewählt.";
		$MSG['no_selected']['en'] = "No file selected.";
		
		// uploadpath bestimmen
		$sPathKey = ($bProtected) ? "protected" : "mdb_upload";
		
		if (!$sFieldname) die("NO sFieldname!");
		$sValue	= (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		
		// ermittle filename des MediaDB-Files
		
		$aMediaFile	= $this->oDb->fetch_by_id($sValue, 'mdb_media');
		$sFilename	= $aMediaFile['filename'];

		// ermittle filesize
		if (file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
			$sFilesize	=  filesize($this->aENV['path'][$sPathKey]['unix'].$sFilename);
		}
		
		// if $sFilename -> show icon/thumbnail and replace-button
		if (!empty($sValue)) {
			$sStr = '<input type="hidden" name="existing_'.$sFieldname.'" id="existing_'.$sFieldname.'" value="'.$sValue.'">'."\n";
		
			if (file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
				$href = ($sPathKey == "protected") 
					? $this->aENV['path']['mdb']['http'].'popup_viewprotected.php?filename='.$sFilename 
					: $this->aENV['path'][$sPathKey]['http'].$sFilename;
				$sStr .= '<a href="'.$href.'" target="_blank" title="View Uploadfile ['.$sFilename.' | '.$sFilesize.']">';
				$sStr .= $this->get_media_icon($sFilename, '', $sPathKey); // params: $sFilename[,$sExtra=''][,$bProtected=false]
				$sStr .= '</a>'."\n";
			} else {
				$sStr .= 'File "'.$sFilename.'" does not exist!'."\n";
			}
			if ($this->bEditMode) {
				$sStr .= '&nbsp;&nbsp;<input type="checkbox" name="delete_'.$sFieldname.'" id="delete_'.$sFieldname.'" value="1" class="radiocheckbox">&nbsp;<label for="delete_'.$sFieldname.'" class="small">'.$MSG['btn_delete'][$this->sLang].'</label>'."\n";
				$sStr .= '<br><span class="small">'.$MSG['btn_replace'][$this->sLang].':<br> </span><input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'">';
			}
		} else { // new: only file-input field
			if ($this->bEditMode) {
				$sStr .= '<input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'">';
			} else {
				$sStr .= '<span class="text"><small>'.$MSG['no_selected'][$this->sLang].'</small></span>';
			}
		}
		
		// output
		return $sStr;
	}

/**
* Schreibt einfachen 'file-upload' (file-input, select-/replace-buttons und thumbnail/icon etc.) auf einer beliebigen seite 
* (mit beachtung der nutzer-rechte).
* ACHTUNG: Benoetigt natuerlich vor den db-actions entsprechendes file-upload handling!
* NOTE: diese Funktion benoetigt im Moment (im Gegensatz zu allen anderen) den value (hier $sFilename) direkt uebergeben (da nicht bekannt ist, wie das db-field heisst)!
*
* Beispiel Funktionen:
* <pre><code>
* // DB-action: delete
* 	if (isset($btn['delete']) && $aData['id']) {
* 		// delete file
* 		$oFile->delete_files($aENV['path']['upload']['unix'].$existing_uploadfile);
* 		$oFile->delete_files($aENV['path']['upload_cache']['unix'].$existing_uploadfile); // ...und altes Thumbnail-File loeschen
* 		// delete data
* 		$aData2delete['id'] = $aData['id']; // Delete data from editwiew
* 		$oDb->make_delete($aData2delete,$sTable);
* 		header("Location: ".$sViewerPage); exit;
* 	}
* // DB-action: save or update
* 	if (isset($btn['save']) || $remoteSave==1) {
* 		// upload new file
* 		if ($uploadfile_name) {
* 			$upload1 = new upload("uploadfile");
* 			$upload1->set_syslang($syslang); // params: $syslang
* 			$upload1->check_filename_length(40, $aMSG['media']['alert_length']); // params: [$maxlength=40][,$error_msg (als array)]
* 			$upload1->check_filename($aMSG['media']['alert_chars']); // params: [$error_msg (als array)]
* 			if (!($aData['pdf'] = $upload1->make_upload($aENV['path']['upload']['unix']))) {$upload1->script_alert(); $alert=true;}
* 		}
* 		// delete old file
* 		if (!$alert && (!empty($delete_uploadfile) || !empty($uploadfile_name)) && !empty($existing_uploadfile)) {
* 			$oFile->delete_files($aENV['path']['upload']['unix'].$existing_uploadfile); // wenn alles OK, altes File loeschen
*			$oFile->delete_files($aENV['path']['upload_cache']['unix'].$existing_uploadfile); // ...und altes Thumbnail-File loeschen
* 			if (empty($uploadfile_name)) { $aData['pdf'] = ''; } // Wenn nur "delete" (checkbox), dann DB-Feld leeren!
* 		}
* 		if (!$alert) {
* 			if ($aData['id']) { // Update eines bestehenden Eintrags
* 				$oDb->make_update($aData,$sTable,$aUpdate);
* 			} else { // Insert eines neuen Eintrags
* 				$oDb->make_insert($aData,$sTable);
* 				$aData['id'] = $oDb->insert_id();
* 			}
* 		}
* </code></pre>
*
* Beispiel Formulareinbau:
* <pre><code>
* echo $oForm->file_upload($aData['filename']); // params: $sFilename[,$sFieldname='uploadfile'][,$sPathKey='upload']
* </code></pre>
*
* @access	public
* @param	string	[$sFilename]	(optional)
* @param	string	[$sFieldname]	(optional -> default:'uploadfile')
* @param	boolean	[$sPathKey]		(optional -> default:'upload')
* @return	html-code
*/
	function file_upload_OLD($sFilename, $sFieldname='uploadfile', $sPathKey='upload') {
		
		// vars
		$MSG = array(); // texte
		$MSG['btn_replace']['de'] = "Ersetzen";
		$MSG['btn_replace']['en'] = "Replace";
		$MSG['btn_delete']['de'] = "Löschen";
		$MSG['btn_delete']['en'] = "Delete";
		$MSG['no_selected']['de'] = "Keine Datei ausgewählt.";
		$MSG['no_selected']['en'] = "No file selected.";
		
		// ermittle filesize
		if (file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
			$sFilesize	= $this->oFile->get_filesize($this->aENV['path'][$sPathKey]['unix'].$sFilename, 'AUTO', 0); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		}
		// if $sFilename -> show icon/thumbnail and replace-button
		if (!empty($sFilename)) {
			$sStr = '<input type="hidden" name="existing_'.$sFieldname.'" id="existing_'.$sFieldname.'" value="'.$sFilename.'">'."\n";
			if (file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
				$href = ($sPathKey == "protected") 
					? $this->aENV['path']['mdb']['http'].'popup_viewprotected.php?filename='.$sFilename 
					: $this->aENV['path'][$sPathKey]['http'].$sFilename;
				$sStr .= '<a href="'.$href.'" target="_blank" title="View Uploadfile ['.$sFilename.' | '.$sFilesize.']">';
				$sStr .= $this->get_media_icon($sFilename, '', $sPathKey); // params: $sFilename[,$sExtra=''][,$sPathKey='upload']
				$sStr .= '</a>'."\n";
			} else {
				$sStr .= 'File "'.$sFilename.'" does not exist!'."\n";
			}
			if ($this->bEditMode) {
				$sStr .= '&nbsp;&nbsp;<input type="checkbox" name="delete_'.$sFieldname.'" id="delete_'.$sFieldname.'" value="1" class="radiocheckbox">&nbsp;<label for="delete_'.$sFieldname.'" class="small">'.$MSG['btn_delete'][$this->sLang].'</label>'."\n";
				$sStr .= '<br><span class="small">'.$MSG['btn_replace'][$this->sLang].':<br> </span><input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'">';
			}
		} else { // new: only file-input field
			if ($this->bEditMode) {
				$sStr .= '<input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'">';
			} else {
				$sStr .= '<span class="text"><small>'.$MSG['no_selected'][$this->sLang].'</small></span>';
			}
		}
		
		// output
		return $sStr;
	}

/**
* Ermittelt thumbnail/icon etc. fuer z.B. die upload-Funktionen.
* (Wrapper fuer die MediaDB-Klasse!)
*
* @access	private
* @param	string	$sFilename		Nur der Dateiname ohne Pfad
* @param	string	[$sExtra]		(optional -> default:'')
* @param	boolean	[$sPathKey]		(optional -> default:'mdb_upload')
* @return	html-code
* @deprecated version - 28.03.2006
*/
	function get_media_icon($sFilename, $sExtra='', $sPathKey='mdb_upload') {
		// init MEDIA-DB
		if (!is_object($this->oMediadb)) {
			require_once($this->aENV['path']['global_service']['unix']."class.mediadb.php");
			$this->oMediadb =& new mediadb(); // params: - 
		}
		// output
		return $this->oMediadb->getMediaThumb($sFilename, $sExtra, $sPathKey); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
	}

/**
* Ermittelt den Media-Typ ("image"/"pdf",...).
* (Wrapper fuer die MediaDB-Klasse!)
*
* @access	private
* @param	string		$sFilename		Nur der Dateiname ohne Pfad
* @return	string		Media-Typ
* @deprecated version - 28.03.2006
*/
	function get_media_type($sFilename) {
		// init MEDIA-DB
		if (!is_object($this->oMediadb)) { 
			require_once($this->aENV['path']['global_service']['unix']."class.mediadb.php");
			$this->oMediadb =& new mediadb(); // params: - 
		}
		// output
		return $this->oMediadb->getMediaType($sFilename); // params: $sFilename
	}

/**
* Ermittelt den (echten) Filename eines Media-Files.
* (Wrapper fuer die MediaDB-Klasse!)
*
* @access	private
* @param	string		$nMediaId		ID der Datei
* @return	string		Dateiname
* @deprecated version - 28.03.2006
*/
	function get_media_filename($nMediaId) {
		// init MEDIA-DB
		if (!is_object($this->oMediadb)) {
			require_once($this->aENV['path']['global_service']['unix']."class.mediadb.php");
			$this->oMediadb =& new mediadb(); // params: - 
		}
		// output
		$aMediaFile = $this->oMediadb->getMedia($nMediaId); // params: $nMediaId
		return $aMediaFile['filename'];
	}


/**
* Baut komplette select-/delete-buttons und thumbnail/icon etc. fuer 'download-link(-manager-solo)' (mit beachtung der nutzer-rechte).
*
* Beispiel:
* <pre><code>
* echo $oForm->downloadlinkmanager_tools("int_link"); // params: $sFieldname[,$nSize=45]
* </code></pre>
* @todo Muss noch per JS angebunden werden!
* @access	public
* @param	string	$sFieldname
* @param	integer	[$nSize]	(optional -> default:'45')
* @return	html-code
*/
	function downloadlinkmanager_tools($sFieldname, $nSize=45) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		
		// vars
		$MSG = array(); // texte
		$MSG['btn']['de'] = "Download Link";
		$MSG['btn']['en'] = "Download Link";
		
		// code
		if ($this->bEditMode) {
			// text field = container for real filename
			$sStr = '<input type="text" name="aData['.$sFieldname.']" id="'.$sFieldname.'" value="'.$sValue.'" size="'.$nSize.'">'."\n";
			$sStr .= '<input type="button" value="'.$MSG['btn'][$this->sLang].'" name="btn[dl_link]" class="smallbut" onClick="javascript:selectFtpWin(\''.$sFieldname.'\')">';
		} else {
			$sStr = '<span class="text">'.$sValue.'</span>';
		}
		
		// output
		return $sStr;
	}

/**
* Baut komplette select-/delete-buttons und thumbnail/icon etc. fuer 'internal-link(-manager-solo)' (mit beachtung der nutzer-rechte).
*
* Beispiel:
* <pre><code>
* echo $oForm->linkmanager_tools("int_link"); // params: $sFieldname[,$nSize=45]
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	integer	[$nSize]	(optional -> default:'45')
* @return	html-code
*/
	function linkmanager_tools($sFieldname, $nSize=45) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		
		// vars
		$MSG = array(); // texte
		$MSG['btn']['de'] = "Interner Link";
		$MSG['btn']['en'] = "Internal Link";
		
		// code
		if ($this->bEditMode) {
			// text field = container for real filename
			$sStr = '<input type="text" name="aData['.$sFieldname.']" id="'.$sFieldname.'" value="'.$sValue.'" size="'.$nSize.'">'."\n";
			$sStr .= '<input type="button" value="'.$MSG['btn'][$this->sLang].'" name="btn[int_link]" class="smallbut" onClick="javascript:selectLinkmanagerWin(\''.$sFieldname.'\')">';
		} else {
			$sStr = '<span class="text">'.$sValue.'</span>';
		}
		
		// output
		return $sStr;
	}

/**
* Baut checkbox (mit defaultValue: "1") und hidden-field davor (mit defaultValue: "0") (mit beachtung der nutzer-rechte).
*
* Beispiel:
* <pre><code>
* echo $oForm->checkbox("flag_irgendwas","yes","no"); // params: $sFieldname[,$sDefaultCheckboxValue='1'][,$sDefaultHiddenValue='0'][,$sExtra=''][,$sExtra='']
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	string	[$sDefaultCheckboxValue]	(optional -> default: '1')
* @param	string	[$sDefaultHiddenValue]		(optional -> default: '0')
* @param	string	[$sExtra]					(optional -> default: '')
* @return	html-code
*/
	function checkbox($sFieldname, $sDefaultCheckboxValue='1', $sDefaultHiddenValue='0', $sExtra='') {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		
		// code
		if ($this->bEditMode) {
			$sStr = form_web::checkbox($sFieldname, $sDefaultCheckboxValue, $sDefaultHiddenValue, $sExtra);
		} else {
			$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
			$sStr = '<span class="text" id="'.$sFieldname.'">';
			$sStr .= ($sValue == $sDefaultCheckboxValue) ? '&Oslash; </span>' : 'O </span>';
		}
		
		// output
		return $sStr;
	}

/**
* Baut radio-buttons (aus array) (mit beachtung der nutzer-rechte). 
*
* Defaultmaessig wird eine Ja/Nein (mit 0/1-keys) Kombi geschrieben (2sprachig). Man kann aber durch 
* Angabe des 2. Parameters (in der Form "array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...) 
* auch andere Kombis erzeugen. 
* Der optionale 3. Parameter bestimmt, ob der erste Eintrag default-maessig "checked" sein soll oder 
* nicht (default). 
* Der optionale 4. Parameter bestimmt, ob die Radio-buttons untereinander oder nebeneineander (default) 
* ausgegeben werden. 
*
* Beispiel:
* <pre><code>
* echo $oForm->radio("flag_irgendwas"); // params: $sFieldname[,$aValues=''][,$bFirstChecked=false][,$bLinebreaks=false]
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	array	[$aValues]			(optional -> default: array('0'=>'Nein/No','1'=>'Ja/Yes')
* @param	boolean	[$bFirstChecked]	(optional -> default: false)
* @param	boolean	[$bLinebreaks]		(optional -> default: false)
* @return	html-code
*/
	function radio($sFieldname, $aValues='', $bFirstChecked=false, $bLinebreaks=false) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		// texte
		$MSG = array();
		$MSG['yes']['de']	= "Ja";
		$MSG['yes']['en']	= "Yes";
		$MSG['no']['de']	= "Nein";
		$MSG['no']['en']	= "No";
		// set default
		$sStr = '';
		$aDefaultValues = array('1' => $MSG['yes'][$this->sLang], '0' => $MSG['no'][$this->sLang]);
		if ($aValues == '') { $aValues = $aDefaultValues; }
		// ggf. ersten array-eintrag als "checked" markieren
		if ($sValue == '' && $bFirstChecked) { reset($aValues); $sValue = key($aValues); }
		
		// code
		if ($this->bEditMode) { // print radio-buttons
			foreach ($aValues as $key => $val) {
				$sel = ($key == $sValue) ? " checked" : '';
				$sStr .= '<input type="radio" name="aData['.$sFieldname.']" id="'.$sFieldname.'_'.$key.'" value="'.$key.'" class="radiocheckbox"'.$sel.'>';
				$sStr .= '<label for="'.$sFieldname.'_'.$key.'" class="text">'.$val.'</label> ';
				if ($bLinebreaks == true) {$sStr .= '<br>';}
			}
		} else { // print only "checked" value
			foreach ($aValues as $key => $val) {
				if ($key == $sValue) { $sStr = '<span class="text">'.$val.'</span> '; }
			}
		}
		
		// output
		return $sStr;
	}

/**
* Schreibt komplettes Dropdown (Select-Field) aus array (mit beachtung der nutzer-rechte). 
* Der 2. Parameter (in der Form "array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...) erzeugt die
* <option>-tags. Im optionalen 3. Parameter kann man zusaetzliche Attribute (js-eventhandler,class,...)
* uebergeben.
*
* Beispiel:
* <pre><code>
* $aOptions = array('key1'=>'value1','key2'=>'value2','key3'=>'value3')
* $oForm->select("select_bsp",$aOptions,'onchange="tuwas(this)"'); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false]
* </code></pre>
*
* @access	public
* @param	string	$sFieldname
* @param	array	$aValues		(required: array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...)
* @param	int		[$sExtra]		(optional: Attribute fuer den SELECT-tag -> default: '')
* @param	int		[$nSize]		(optional: Attribut "size" -> default: 1)
* @param	boolean	[$bMultiple]	(optional: Attribut "multiple" -> default: false)
* @return	html-code
*/
	function select($sFieldname, $aValues, $sExtra='', $nSize=1, $bMultiple=false) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		#if (!$aValues) die("NO aValues!"); // -> es koennte auch einleeres dropdown geschrieben werden...
		
		// code
		if ($this->bEditMode) { // build select-field
			$sStr = parent::select($sFieldname, $aValues, $sExtra, $nSize, $bMultiple);
		} else { // print only "selected" value
			$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
			foreach ($aValues as $key => $val) {
				if ($key == $sValue) { $sStr = '<span class="text">'.$val.'</span> '; }
			}
		}
		
		// output
		return $sStr;
	}


# -------------------------------------------------------------------------------- LANGUAGE

/**
* Baut komplettes Dropdown (Select-Field) der zur Verfuegung stehenden CONTENT-languages - inkl. javascript!
*
* Beispiel:
* <pre><code>
* $oForm->lang_dropdown(); // params: [$size=1]
* </code></pre>
*
* @access	public
* @param	string	[$size]			Attribut "size"
* @return	html-code
*/
	function lang_dropdown($size=1) {
		
		global $weblang; // TODO: wert uebergeben!
		
		// rebuild needed GET params
		$sGEToptions = '?'.str_replace("&weblang=".$weblang, '', $_SERVER['QUERY_STRING']);
		// fallback fuer $id -> muss IMMER korrekt sein!
		if (!isset($_GET['id']) && !empty($this->aData['id'])) { $sGEToptions .= '&id='.$this->aData['id']; } // wenn nicht im get-array, dann hole id aus data-array!
		
		// build JavaScript-function
		$sStr = '<script language="JavaScript" type="text/javascript">'."\n"; // Funktion zum automatischen reload nach auswahl einer Sprache aus einer option-list
		$sStr .= 'function selectLangMenu(selObj) {';
		$sStr .= 'eval("window.location=\''.$_SERVER['PHP_SELF'].$sGEToptions.'&weblang="+selObj.options[selObj.selectedIndex].value+"\'");';
		$sStr .= "}</script>\n";
		
		// build select-field
		$sStr .= '<select name="weblang" id="weblang" size="'.$size.'" onchange="selectLangMenu(this)">';
		foreach ($this->aENV['content_language'] as $key => $val) {
			if ($key != 'default') { // don't show default-language!
				$sel = ($key == $weblang) ? " selected" : '';
				$sStr .= "<option value=\"$key\"$sel>$val</option>";
			}
		}
		$sStr .= "</select>\n";
		
		// output
		return $sStr;
	}

	/**
	 * 
	 * Diese Methode erzeugt ein Kopierdropdown
	 * @param array $aLang
	 * @param int $size
	 * @return String
	 * 
	 */

	function copyToLangDropdown($aLang,$size=1) {
		if(count($aLang) > 2) { $str = $this->select('copyfromlang',$aLang,'',$size); }
		else { $cur = each($aLang); $str = $cur[1]; $str .= $oForm->hidden('copyfromlang',$cur[0]); }
		return $str;
	}

# -------------------------------------------------------------------------------- DATE/TIME

/**
* Baut als Datums-Eingabe drei Dropdowns (fuer tag/monat/jahr) (mit beachtung der nutzer-rechte). 
*
* ACHTUNG: 
* benoetigt folgende zeile vor der db-speicherung: 
* <pre>
* // merge date-dropdowns into a valid mysql- date 
* $aData['date'] = $_POST['date']['year']."-".$_POST['date']['month']."-".$_POST['date']['day'];
* </pre>
* plus analog bei verwendung von mehreren dropdowns: 
* <pre>
* // merge date-dropdowns into a valid mysql- date (wobei hier der DB-feldname 'date2' bzw. 'dateFoo' ist) 
* $aData['date2'] = $_POST['date2']['year']."-".$_POST['date2']['month']."-".$_POST['date2']['day'];
* $aData['dateFoo'] = $_POST['dateFoo']['year']."-".$_POST['dateFoo']['month']."-".$_POST['dateFoo']['day'];
* ... 
* // oder bei Verwendung der class.cms_content-Klasse:
* $oCmsContent->setDateDropdownField('date'); // params: $sFieldname
* $oCmsContent->setDateDropdownField('date2'); // params: $sFieldname
* </pre>
*
* Beispiel: 
* <pre><code>
* echo $oForm->date_dropdown("date"); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]
* echo $oForm->date_dropdown("date2"); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]
* echo $oForm->date_dropdown("dateFoo",20,2); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]
* </code></pre>
*
* @access	public
* @param	string		$sFieldname
* @param	string		$bDateRequired	[bei angabe nicht required (false) wird auch ein Null-Value angezeigt -> default:true]
* @param	integer		$nPastYears		[wieviele Jahre i.d. Vergangenheit werden angezeigt -> default:5]
* @param	integer		$nFutureYears	[wieviele Jahre i.d. Zukunft werden angezeigt -> default:1]
* @return	html-code
*/
	function date_dropdown($sFieldname, $bDateRequired=true, $nPastYears=5, $nFutureYears=1) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		
		// code
		if ($this->bEditMode) { // wenn edit-rechte
			$sStr = form_web::date_dropdown($sFieldname, $bDateRequired, $nPastYears, $nFutureYears);
		} else { // wenn keine edit-rechte
			$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
			if ($sValue != '') { $sStr = '<span class="text">'.$this->oDate->date_mysql2trad($sValue).'</span>'; }
		}
		
		// output
		return $sStr;
	}

/**
* Baut als Datums-Eingabe ein Textfeld und einen Button zum oeffnen eines Calendar-Popups.
* Soll ein Plausi-Check durchgefuehrt werden kann als 5. Parameter die JS-Funktion 
* 'onBlur="formatDateField(this, false)"' uebergeben werden, wobei hier als zweiter Parameter
* bei Verwendung von Wochentagsnamen "true" uebergeben werden muss. 
*
* ACHTUNG: 
* benoetigt folgende zeile vor der db-speicherung: 
*	$aData['date'] = $oDate->date_trad2mysql($aData['date']); // convert trad.-date into a valid mysql-date 
* SOWIE die JS-Funktion "calendarWin(formfield)"
* SOWIE die Datei "popup_calendar.php" im CAL-Verzeichnis!
*
* Beispiel: 
* <pre><code>
* echo $oForm->date_popup("date", 10, 12, $oDate->get_today(), 'onBlur="formatDateField(this, false)"');
* echo $oForm->date_popup("date2", 10, 12, '', 'onBlur="formatDateField(this, true)"', '', true); 
* </code></pre>
*
* @access	public
* @see		textfield()
* @param	string	$sFieldname
* @param	integer	[$nMaxlength]	(optional)	(default: 10)
* @param	integer	[$nSize]		(optional)	(default: 12)
* @param	string	[$sDefault]		(optional)	(default: '')
* @param	string	[$sExtra]		(optional)	(default: '')
* @param	string	[$sImg]			(optional)	(default: '')
* @return	html-code
*/
	function date_popup($sFieldname, $nMaxlength=10, $nSize=12, $sDefault='', $sExtra='', $sImg='', $bWithDayname=false) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if (empty($sImg)) {
			$sImg = '<img src="'.$this->aENV['path']['pix']['http'].'btn_calendar.gif" class="btn" style="position:relative; top:4px; margin-left:3px;" alt="'.$this->aMSG['adm']['open_cal_popup'][$this->sLang].'">';
		}
		if ($bWithDayname) { 
			$nSize += 5;
			$nMaxlength += 5;
		}
		// code
		$sStr = $this->textfield($sFieldname, $nMaxlength, $nSize, $sDefault, $sExtra); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] 
		if ($this->bEditMode) { // wenn edit-rechte
			$sStr .= '<a href="javascript:calendarWin(\''.$sFieldname.'\', \''.$this->sName.'\');" title="'.$this->aMSG['adm']['open_cal_popup'][$this->sLang].'">';
			$sStr .= $sImg;
			$sStr .= '</a>';
		}
		// output
		return $sStr;
	}


# -------------------------------------------------------------------------------- CATEGORY

/**
* Baut komplettes Dropdown (Select-Field) der zur Verfuegung stehenden (Template-)Kategorien.
* NOTE: Benoetigt die mittels "set_cms_vars()" uebergebenen Klassenvariablen "sCmsTemplate" + "sCmsNaviId"!
*
* Beispiel:
* <pre><code>
* echo $oForm->categories_dropdown(); // params: [$sTrenner='&nbsp;|&nbsp;']
* </code></pre>
*
* @access	public
* @global	array	$aNavi			Navi-Array
* @param	string	[$sTrenner]		trenner zwischen haupt- und sub-navipunkt
* @return	html-code
*/
	function categories_dropdown($sTrenner='&nbsp;|&nbsp;',$sTable='') {
		// check vars
		if (empty($this->sCmsTemplate))	return '';
		if (empty($this->sCmsNaviId))	return '';
		if(empty($sTable))	$sTable	= $this->aENV['table']['cms_navi'];
		
		// vars
		$buffer = array();
		// get titles
		$this->oDb->query("SELECT id, parent_id, title_".$this->sLang." as title 
						FROM ".$sTable." 
						WHERE template='".$this->sCmsTemplate."' 
						ORDER BY prio DESC");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[] = array(
				'id'		=> $tmp['id'], 
				'parent_id'	=> $tmp['parent_id'], 
				'title'		=> $tmp['title']
			);
		}
		// ggf. get parent-title
		foreach ($buffer as $key => $val) {
			if ($val['parent_id'] == 0) { continue; }
			$this->oDb->query("SELECT id, parent_id, title_".$this->sLang." as p_title 
								FROM ".$this->aENV['table']['cms_navi']." 
								WHERE id='".$val['parent_id']."'");
			$tmp = $this->oDb->fetch_array();
			if (!empty($tmp['p_title'])) {
				$buffer[$key]['title'] = $tmp['p_title'].' '.$sTrenner.' '.$val['title'];
			}
		}
		// build select-field string
		$sStr = '<select name="aData[navi_id]" id="navi_id" size="1">';
		foreach ($buffer as $key => $val) {
			$sel = ($this->sCmsNaviId == $val['id']) ? " selected" : "";
			$sStr .= '<option value="'.$val['id'].'"'.$sel.'>'.$val['title']."</option>";
		}
		$sStr .= "</select>\n";
		// HTML output
		return $sStr;
	}


# -------------------------------------------------------------------------------- BUTTONS

/**
* Baut kompletten <input type="button"> - wie alles in dieser Klasse mehr oder weniger SPEZIELL fuer die CMS view- und detail-seiten!
* (... im Moment nur die Standard-Buttons... bewusst nicht erweiterbar...)
*
* Beispiel:
* <pre><code>
* echo $oForm->button("DELETE"); // params: $sType[,$sClass='but']
* </code></pre>
*
* @access	public
* @global	array	$sViewerPage
* @global	array	$sEditPage
* @global	array	$aBnParts
* @global	string	$sViewerPage
* @param	string	$sType			[SAVE|DELETE|DEL_ALL|CANCEL|RESET|BACK|NEW|SEARCH|CLOSE_WIN]
* @param	string	$sClass			(optional)
* @return	html-code
*/
	function button($sType, $sClass='') {
		if (!empty($this->sUrlParams))	{
			global $sViewerPage,$sNewPage;#,$sEditPage
		} else {
	
			$sViewerPage	= str_replace(array('_detail.php'), '.php', basename($_SERVER['PHP_SELF'])).$this->sUrlParams;
			$sNewPage		= basename($_SERVER['PHP_SELF']).$this->sUrlParams;
			#$sEditPage		= str_replace('.php','_detail.php',basename($_SERVER['PHP_SELF'])).$this->sUrlParams;
		}
		
		// vars
		if (!$sType) die("NO sType!");
		$sType = strToLower($sType);
		$valid = false;
		
		// code
		switch ($sType) {
			// sonderfall images
			case "back":		$valid=true; $t="image";	$image='btn_list';		$js='javascript:window.location.replace(\''.$sViewerPage.'\');';	break;
			case "new":			$valid=true; $t="image";	$image='btn_add';		$js='javascript:window.location.replace(\''.$sNewPage.'\');';		break;
			case "preview":		$valid=true; $t="image";	$image='btn_preview';	$js='javascript:previewWin();';	break;
			// html-buttons
			case "save":		$valid=true; $t="submit";	$class='but';		$js=' onClick="return checkReqFields()"';	break;
			case "save_close":	$valid=true; $t="submit";	$class='but';		$js=' onClick="return checkReqFields()"';	break;
			case "delete":		$valid=true; $t="submit";	$class='but';		$js=' onClick="return confirmDelete()"';	break;
			case "deactivate":	$valid=true; $t="submit";	$class='but';		$js='';	break;
			case "delete_all":	$valid=true; $t="submit";	$class='but';		$js=' onClick="return confirmDeleteAll()"';	break;
			case "cancel":		$valid=true; $t="button";	$class='but';		$js=' onClick="window.location.href=\''.$sViewerPage.'\';"';	break;
			case "reset":		$valid=true; $t="reset";	$class='but';		$js='';	break;
			case "search":		$valid=true; $t="submit";	$class='but';		$js='';	break;
			case "close_win":	$valid=true; $t="button";	$class='smallbut';	$js=' onClick="javascript:self.close()"';	break;
			case "copy":		$valid=true; $t="submit";	$class='smallbut';	$js=' onClick="return checkReqFields()"';	break;
			case "save_copy":	$valid=true; $t="submit";	$class='but';		$js=' onClick="return checkReqFields()"';	break;
			case "save_new":	$valid=true; $t="submit";	$class='but';		$js=' onClick="return checkReqFields()"';	break;			
			case "container_create":	$valid=true; $t="submit";	$class='but';	break;
			// CMS preview
			case "accept_changes":	$valid=true; $t="submit";	$class='but';		$js=' onClick="return checkReqFields()"';	break;
			case "discard_changes":	$valid=true; $t="submit";	$class='but';		$js='';	break;
		}
		if ($valid == false) die("WRONG $sType!");
		if ($sClass != '') { $class = $sClass; }
		
		if ($t == "image") { // kein html-form-element sondern verlinktes image!
			$sStr = '<a href="'.$js.'" title="'.$this->aMSG['btn'][$sType][$this->sLang].'">';
			$sStr .= '<img src="'.$this->aENV['path']['pix']['http'].$image.'.gif" alt="'.$this->aMSG['btn'][$sType][$this->sLang].'" class="btn"></a>';
		} else {
			$sStr = '<input type="'.$t.'" value="'.$this->aMSG['btn'][$sType][$this->sLang].'" class="'.$class.'" name="btn['.$sType.']" id="'.$sType.'"'.$js.'>';
		}
		
		// NEU: RECHTEMANAGEMENT (nicht bei admin / management)
		if (is_object($this->oPerm) && !$this->oPerm->hasPriv('admin') && !$this->oPerm->hasPriv('management')) {
			if ($this->oPerm->getModule() != "sys") { // NICHT im SYS-Modul!
				if ($sType == "new" && !$this->oPerm->hasPriv('create')) {
					$sStr = '';
				}
				if ($sType == "save" || $sType == "save_close" || $sType == "copy" || $sType == "save_copy" || $sType == "cancel" || $sType == "reset" || $sType == "discard_changes") {
					if (isset($this->aData['created_by']) && !$this->oPerm->hasPriv('edit')) $sStr = ''; // edit
					if (!isset($this->aData['created_by']) && !$this->oPerm->hasPriv('create')) $sStr = ''; // create
				}
				if (($sType == "delete" || $sType == "delete_all") && !$this->oPerm->hasPriv('delete')) {
					$sStr = '';
				}
				if (($sType == "accept_changes" || $sType == "discard_changes") && !$this->oPerm->hasPriv('publish')) {
					$sStr = '';
				}
				if ($sType == "preview" && (!isset($this->aData['id']) || !$this->oPerm->hasPriv('preview'))) {
					$sStr = ''; // kein preview-button bei NEW (oder wenn kein preview-recht)
				}
			}
		}
		// SPECIAL im SYS-USER-Modul:
		if (is_object($this->oPerm) && basename($this->aENV['PHP_SELF']) == "sys_user_detail.php") {
			if ($sType == "delete" && !$this->oPerm->hasPriv('admin')) {
				$sStr = ''; // kein delete-button wenn nicht admin!
			}
		}
		
		// output
		return $sStr;
	}
	/**
	 * 
	 * Baut ein Labeldropdown und einen Editbutton f�r die Labels. Komplett Datenbank gest�tzt.
	 * @param String $sFieldname
	 * @param string $parent
	 * @param string $flagtype
	 * @param object $oDb
	 * @param boolean $bWithColorPicker
	 * @return string
	 * 
	 */
	function labelDropdown($sFieldname,$parent,$flagtype,&$oDb,$bWithColorPicker=true) {
		global $aMSG,$syslang;
		$sql = "SELECT * FROM ".$this->aENV['table']['sys_labels']." WHERE flag_type='$flagtype' AND parent='$parent' ORDER BY name ASC";
		
		$oDb->query($sql);
		$aLabels = array();
		for($i=0;$row = $oDb->fetch_object();$i++) {
		
			$aLabels[$row->id]['name'] = $row->name;
			$aLabels[$row->id]['color'] = $row->color;
			
		}
		if($bWithColorPicker) $sWithColorPicker = 'true'; else $sWithColorPicker = 'false';
		$sLink .= '	<a href="#" onClick="newwin = window.open(\''.$this->aENV['path']['sys']['http'].'popup_editlabels.php?flagtype='.$flagtype.'&parent='.$parent.'&bWithColorPicker='.$sWithColorPicker.'&parentField='.$sFieldname.'\', \'popup\', \'toolbar=0,status=0,scrollbars=1,width=400,height=500,resizable=1\'); newwin.focus(); return false;" title="'.$aMSG['std']['edit'][$syslang].'">';
		
		$oDb->free_result();
		return $this->labelDropdownHTML($sFieldname,$aLabels,$parent,$sLink,$sWithColorPicker);
	}
	/**
	 * 
	 * reines HTML Frontend f�r die Labels. Die Datensourcen k�nnen theoretisch ausgetauscht werden.
	 * @param string $sFieldname
	 * @param array $aLabels
	 * @param string $parent
	 * @param string $link
	 * @param string $sWithColorPicker
	 * @return string
	 */
	function labelDropdownHTML($sFieldname,$aLabels,$parent=0,$link,$sWithColorPicker) {
		global $aMSG,$syslang;
		$sOnChange = ($sWithColorPicker == "true") 
			? ' onChange="document.getElementById(\'colorPickerTD\').style.backgroundColor = this.options[selectedIndex].style.backgroundColor;"' 
			: "";		
		
		$eol = "\n";
		$sText .= '<div id="labelIdDiv">'.$eol;
		$sText .= ' <table border="0" cellspacing="0" cellpadding="1">'.$eol;
		$sText .= '  <tr>'.$eol;
		$sText .= '   <td ';
		if(!empty($aLabels[$this->aData[$sFieldname]]['color']) && $sWithColorPicker == "true") {
			$sText .= 'style="background-color:'.$aLabels[$this->aData[$sFieldname]]['color'].';"';
		} 
 		$sText .= 'id="colorPickerTD">'.$eol;
		// custom-label
		$sText .= '<input type="hidden" id="selectedLabelId" name="selectedLabelId" value="'.$this->aData[$sFieldname].'">';
		if ($this->bEditMode == true) {
			$sText .= '<select name="aData['.$sFieldname.']"'.$sOnChange.'>'.$eol;
			$sText .= '<option value="">--</option>'.$eol;
			if(count($aLabels) != 0) {
				
				foreach($aLabels as $id => $val) {
					$sel = ($this->aData[$sFieldname] == $id) ? ' selected' : '';
					$sText .= '<option value="'.$id.'"'.$sel;
					$sText .= ($sWithColorPicker == "true" && !empty($val['color'])) ? ' style="background-color:'.$val['color'].'"' : "";
					$sText .= '>'.$val['name'].'</option>'.$eol;
				}
			}
			$sText .= '</select>'.$eol;
			$sText .= $link.'<img src="'.$this->aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['std']['edit'][$syslang].'" class="btn" style="vertical-align:middle; margin-bottom: 2px"></a>'.$eol;
		} else {
			echo '<span class="text">'.$aLabels[$this->aData[$sFieldname]]['name'].'</span>';
		}
		$sText .= '   </td>'.$eol;
		$sText .= '  </tr>'.$eol;
		$sText .= ' </table>'.$eol;
		$sText .= '</div>'.$eol;
		
		return $sText;
 
	}
	/**
	 * 
	 * Diese Methode fügt die Möglichtkeit hinzu, dass ein Textelement mit einem Textbaustein versehen werden kann.
	 * @param string $sFieldname Originaler Feldname der Editiert werden soll
	 * @param string $parent
	 * @param string $module
	 * @return string $sText
	 * 
	 */
	function textmoduleHTML($sFieldname,$parent=0,$module) {
		if ($this->bEditMode == true) {
			$sText = '<a href="#" onClick="newwin = window.open(\''.$this->aENV['path']['sys']['http'].'popup_textmodule.php?flagtype='.$module.'&parent='.$parent.'&parentField='.$sFieldname.'&parentFormName='.$this->sName.'\', \'popup\', \'toolbar=0,status=0,scrollbars=1,width=600,height=500,resizable=1\'); newwin.focus(); return false;" title="'.$sFieldname.'">';
			$sText .= '<img src="'.$this->aENV['path']['pix']['http'].'btn_snippet.gif" alt="'.$sFieldname.'" class="btn" style="vertical-align:top; margin-top: 1px"></a>'."\n";
		}		
		return $sText;
	}
	/**
	 * 
	 * Diese Methode baut ein komplettes MultiPart Modul. In dem sich wie in einer Gallery Bilder hinzufügen lassen und mit Texten versehen lassen.
	 * @param string $sFieldname
	 * @param string $syslang
	 * @return string
	 */
	function multiPart($sFieldname,$syslang='de') {
						
		if (isset($this->aENV['module']['mdb'])) {
		
			$aData2 = explode("\n",$this->aData[$sFieldname]);
			for($i=0;!empty($aData2[$i]);$i++) {
			   /* 
				* Splittet auf in 	[0] => mdb_id
				*					[1] => Beschreibungstext
				*/
				$aTmp = explode('||',$aData2[$i]);
				$this->aData[$sFieldname.'_mdb_'.$i] = $aTmp[0];
				$this->aData[$sFieldname.'_text_'.$i] = $aTmp[1];
				unset($aTmp);
				$count=$i+1;

			}
			$str .= '<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">';
			for ($i = 0; $i < $count; $i++) { 
				$str .= ('
						<tr valign="top">
							<td>
								'.$this->textarea($sFieldname.'_text_'.$i,3).'
							</td>
						</tr>
						<tr valign="top">
							<td>
								'.$this->media_tools($sFieldname.'_mdb_'.$i).'
							</td>
						</tr>
						<tr><td colspan="2" class="off"><img src="'.$this->aENV['path']['pix']['http'].'onepix.gif" width="1" height="10" alt="" border="0"></td></tr>
				');
			} // END for 

			if ($this->bEditMode == true) { // ADD-part-modul 
				$str .= ('
					<tr valign="top">
						<td width="100%" class="sub2"><p><b>'.$this->aMSG['std']['new'][$syslang].'</b></p></td>
					</tr>
					<tr valign="top">
						<td class="sub2">
							'.$this->textarea($sFieldname.'_text_new',3).'
						</td>
					</tr>
					<tr valign="top">
						<td class="sub2">
							'.$this->media_tools($sFieldname.'_mdb_new').'
						</td>
					</tr>
				');
			} // END if ADD-part-modul 

			$str .= '</table>';
			// Aufräumen!!!
			$aData2 = explode("\n",$this->aData[$sFieldname]);
			for($i=0;!empty($aData2[$i]);$i++) {
			   /* 
				* Splittet auf in 	[0] => mdb_id
				*					[1] => Beschreibungstext
				*/
				$aTmp = explode('||',$aData2);
				unset($this->aData[$sFieldname.'_mdb_'.$i]);
				unset($this->aData[$sFieldname.'_text_'.$i]);
			}
		} // END if MDB
		return $str;
	}

	function multiMdb_tools($sFieldname, $sLang=false) {

		// vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue	= (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		$sTable	= $this->aENV['table']['mdb_media'];
		$i		= 0;
		$sStr	= '';
		// texte
		$MSG = array();
		$MSG['btn_remove']['de']	= "Entfernen";
		$MSG['btn_remove']['en']	= "Remove";
		$MSG['btn_select']['de']	= "Hinzufügen";
		$MSG['btn_select']['en']	= "Add";
		
		// if mediadata is already selected
		if ($sValue) {
			// vars
			$aAttId		= explode(",", $sValue);
			$anzahl		= count($aAttId);
			$aAtt		= array();
			$sTdClass	= ($sLang != false) ? ' class="lang'.$sLang.'"' : '';
			
			// open table
			$sStr .= '<table width="360" border="0" cellspacing="1" cellpadding="2" class="tabelle">';
			
			// get media-data
			$this->oDb->query("SELECT id, name, description, filename, filesize FROM ".$sTable." WHERE id IN (".$sValue.")");
			while ($tmp = $this->oDb->fetch_array()) {
				// description
				$aATT[$tmp['id']]['title']		= (!empty($tmp['description'])) ? shorten_text($tmp['description'], 40) : $tmp['name'];
				// filename
				$aATT[$tmp['id']]['filename']	= $tmp['filename'];
				// filesize
				$aATT[$tmp['id']]['filesize']	= $this->oFile->format_filesize($tmp['filesize'], 'AUTO', 0);
			}
			
			// Ausgabe der schon gespeicherten Attachments
			foreach ($aAttId as $id) {
				$sStr .= '<tr><td width="80%"'.$sTdClass.'><span class="text">'."\n";
				 // hidden-field fuer pdf_id-string
				$sStr .= '<input type="hidden" name="aData['.$sFieldname.']['.$i.']" value="'.$id.'">'."\n";
				// link zu PDF (in neuem Fenster)
				$sStr .= '<a href="'.$this->aENV['path']['mdb_upload']['http'].$aATT[$id]['filename'].'" target="_blank" title="View Mediafile ['.$aATT[$id]['filename'].' | '.$aATT[$id]['filesize'].']">';
				// icon
				$sStr .= $this->get_media_icon($aATT[$id]['filename']); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
				// name
				$sStr .= $aATT[$id]['title'].'</a>&nbsp;</span></td>';
				// delete-checkbox
				if ($this->bEditMode) {
					$sStr .= '<td width="20%"'.$sTdClass.' align="right" nowrap><span class="text">&nbsp;<input type="checkbox" name="aData['.$sFieldname.']['.$i.']" value="" class="radiocheckbox">&nbsp;'.$MSG['btn_remove'][$this->sLang].'<br></span></td></tr>'."\n";
				}
				// zaehle hoch
				$i++;
			}
			// close table
			$sStr .= '</table>';
		} // END if ($sValue)
		
		
		if ($this->bEditMode) {
			// hidden field = container for NEW attachment-id
			$sStr .= '<input type="hidden" name="aData[new_'.$sFieldname.']" id="new_'.$sFieldname.'" value="">'."\n";
			// text field shows name of mediafile (for usability reason only - will not be saved!)
			$sStr .= '<input type="text" name="new_'.$sFieldname.'_title" value="" size="50" readonly> ';
			// add button
			$sStr .= '<input type="button" value="'.$MSG['btn_select'][$this->sLang].'" name="btn[select_'.$sFieldname.']" class="smallbut" onClick="javascript:selectMediaWin(\'all\',\'new_'.$sFieldname.'\',\''.$this->sName.'\')"> ';
		}
		
		// output
		return $sStr;
	}


	
/**
* Diese Methode stellt die "online_from" und "online_to" Felder-Kombi zur Verfuegung.
* 
* Beispiel:
* <pre><code>
* echo $oForm->onlineFromTo(''); // params: [$lang='de'][,$type='popup']
* </code></pre>
*
* @access	public
* @param	string	$lang	[''|de|en|...]		(optional -> default:'de')
* @param	string	$type	[dropdown|popup]	(optional -> default:'popup')
* @return	string	HTML
* 
*/
	function onlineFromTo($lang='de', $type='dropdown') {
		$method		= 'date_'.$type; // [date_dropdown | date_popup]
		// startdate
		$sText = '&nbsp;'.$this->aMSG['form']['from'][$this->sLang].'&nbsp;';
		$fieldname	= ($lang != '') ? 'online_from_'.$lang : 'online_from';
		if ($method == 'date_popup') {
			$sText .= $this->date_popup($fieldname, 10, 12, '', 'onBlur="formatDateField(this, false)"'); 
		} else {
			$sText .= $this->date_dropdown($fieldname, false); 
		}
		// trenner
		$sText .= '&nbsp;'.$this->aMSG['form']['to'][$this->sLang].'&nbsp;';
		// enddate 
		$fieldname = ($lang != '') ? 'online_to_'.$lang : 'online_to';
		if ($method == 'date_popup') {
			$sText .= $this->date_popup($fieldname, 10, 12, '', 'onBlur="formatDateField(this, false)"'); 
		} else {
			$sText .= $this->date_dropdown($fieldname, false); 
		}
		// output
		return $sText;
	}

#-----------------------------------------------------------------------------
} // END of class

?>