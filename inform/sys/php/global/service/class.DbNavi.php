<?php
/*
 * Created on 16.08.2006
 *
 */
/**
 * Diese Klasse löst die derzeitigen funx.admin_view Funktionen ab. Bitte im System nur noch diese Methoden benutzen.
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * 
 * @param array $Userdata
 * @param array $aENV
 * @param int $entries
 * @param int $start
 * @param int $limit
 */
 
	class DbNavi {
	
		var $Userdata = null;
		var $aENV = null;
	
		var $iEntries = 0;
		var $iStart = 0;
		var $iLimit = 0;
	
		function DbNavi($Userdata,$aENV,$entries,$start=0,$limit=20) {
			$this->Userdata = $Userdata;
			$this->aENV = $aENV;
			$this->iEntries = $entries;
			$this->iStart = $start;
			$this->iLimit = $limit;
		}
		
	   /**
		* Baut komplette Statusinformation (Pages + Hits) der DB-Abfrage (formatiert).
		*
		* Beispiel:
		* <pre><code>
		* $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
		* </code></pre>
		*
		* @access	public
		* @return	html-code
		*/
		function getDbnaviStatus() {
			// check vars
			if ($this->iEntries == 0) return;
			// output
			$sStr = "<b>";
			$sStr .= $this->getDbnaviStatusPages();
			$sStr .= "</b> ";
			if (strstr($_SERVER['PHP_SELF'], '/mobile/')) $sStr .= "<br />";
			$sStr .= "[";
			$sStr .= $this->getDbnaviStatusHits();
			$sStr .= "]";
			return $sStr;
		}
	
		/**
		* Baut Statusinformation (Hits) der DB-Abfrage (zeigt also "wieviele von wieviel treffer").
		*
		* <pre><code>
		* $oDBNavi->getDbnaviStatusHits(); // params: $nEntries[,$nStart=0][,$nLimit=20]
		* </code></pre>
		* 
		* @access	private
		* @return	html-code
		*/
		function getDbnaviStatusHits() {
			// check vars
			if ($this->iEntries == 0) return;

			if (isset($this->Userdata['system_lang_default'])) {$syslang = $this->Userdata['system_lang_default'];} else {$syslang = $this->aENV['system_language']['default'];}
			
			// user-vars
			$MSG = array(); // texte
			$MSG['hits']['de'] = "Treffer ";
			$MSG['hits']['en'] = "Hits ";
			$separator = " / "; // trenner zwischen den statuszahlen
			
			$sStr = $MSG['hits'][$syslang]." ".$this->iStart." - ";
			if ($this->iStart + $this->iLimit < $this->iEntries) {
				$sStr .= $this->iStart + $this->iLimit; // next
			} else {
				$sStr .= $this->iEntries;
			}
			$sStr .= $separator.$this->iEntries;
			
			return $sStr;
		}
	
		/**
		* Baut Statusinformation (Pages) der DB-Abfrage (zeigt also "Page {soundsoviel} of {anzahlpages}").
		*
		* <pre><code>
		* $oDBNavi->getDbnaviStatusPages(); // params: $nEntries[,$nStart=0][,$nLimit=20]
		* </code></pre>
		* 
		* @access	private
		* @return	html-code (naja, eher einen kurzen Satz ohne Formatierung!)
		*/
		function getDbnaviStatusPages() {
			// check vars
			if ($this->iEntries == 0) return;

			if (isset($this->Userdata['system_lang_default'])) {$syslang = $this->Userdata['system_lang_default'];} else {$syslang = $this->aENV['system_language']['default'];}
			
			// user-vars
			$MSG = array(); // texte
			$MSG['page']['de'] = "Seite ";
			$MSG['page']['en'] = "Page ";
			$MSG['of']['de'] = " von ";
			$MSG['of']['en'] = " of ";
			
			// vars
			#$nPages = floor($nEntries/$nLimit)+1;
			$nPages = ceil($this->iEntries/$this->iLimit);
			$nPage  = floor($this->iStart/$this->iLimit)+1;
			
			// output
			return $MSG['page'][$syslang].$nPage.$MSG['of'][$syslang].$nPages;
		}
	
		/**
		* Baut Navigationslinks zu DB-Abfrage - Unterteilungsseiten. 
		* Die Ausgabe der Seitenzahlen wird ab $nPageLimit abgebrochen und durch Pfeile ersetzt!
		*
		* Beispiel:
		* <pre><code>
		* $oDBNavi->getDbnaviStatusPages($sGEToptions, $nPageLimit);
		* </code></pre>
		*
		* @access	public
		* @param 	string	[$sGEToptions]	Zusaetzlich zu uebergebende GET-Parameter (mit fuehrendem '&')	(optional -> default:'')
		* @param 	integer	[$nPageLimit]	Anzahl der angezeigten Seiten	(optional -> default:'10')
		* @return	html-code
		*/
		function getDbnaviLinks($sGEToptions='', $nPageLimit=10) {
			
			// check vars
			if ($this->iEntries == 0) return;
			$sGEToptions = preg_replace("/^\?/","&",$sGEToptions);

			if (isset($this->Userdata['system_lang_default'])) {$syslang = $this->Userdata['system_lang_default'];} else {$syslang = $this->aENV['system_language']['default'];}
			
			// user-vars
			$separator = " | "; // trenner zwischen den seitenzahlen
			$MSG = array(); // texte
			$MSG['next_page']['de'] = "nächste Seite";
			$MSG['next_page']['en'] = "next Page";
			$MSG['prev_page']['de'] = "vorige Seite";
			$MSG['prev_page']['en'] = "previous Page";
			#$MSG['next_page']['de'] = "nächsten ".$nPageLimit."Seiten";
			#$MSG['next_page']['en'] = "next ".$nPageLimit." Pages";
			#$MSG['prev_page']['de'] = "vorigen ".$nPageLimit." Seiten";
			#$MSG['prev_page']['en'] = "previous ".$nPageLimit." Pages";
			$MSG['first_page']['de'] = "erste Seite";
			$MSG['first_page']['en'] = "first Page";
			$MSG['last_page']['de'] = "letzte Seite";
			$MSG['last_page']['en'] = "last Page";
			$aIcon = array(); // pfeile
			$aIcon['first'] = '<img src="'.$this->aENV['path']['pix']['http'].'arrow_fast_bk.gif" width="11" height="9" border="0" alt="'.$MSG['first_page'][$syslang].'">';#"&lt;&lt;";
			$aIcon['prev'] = '<img src="'.$this->aENV['path']['pix']['http'].'arrow_bk.gif" width="11" height="9" border="0" alt="'.$MSG['prev_page'][$syslang].'">';#"&lt;";
			$aIcon['next'] = '<img src="'.$this->aENV['path']['pix']['http'].'arrow_frw.gif" width="11" height="9" border="0" alt="'.$MSG['next_page'][$syslang].'">';#"&gt;";
			$aIcon['last'] = '<img src="'.$this->aENV['path']['pix']['http'].'arrow_fast_frw.gif" width="11" height="9" border="0" alt="'.$MSG['last_page'][$syslang].'">';#"&gt;&gt;";
			
			// vars
			$sStr = '';
		
			if ($this->iEntries > $this->iLimit) {
				// make vars
				#$pages = floor($nEntries/$nLimit)+1;
				$pages = ceil($this->iEntries/$this->iLimit);
				$page  = floor($this->iStart/$this->iLimit)+1;
				if (!$page) {$page=1;}
				
				$block_start = floor(($page - 1) / $nPageLimit);
				$showpage_start = 1 + ($block_start * $nPageLimit);
				
				$showpage_end = $showpage_start+($nPageLimit-1);
				$sep = "";
				
				// pfeile nach links
				if ($showpage_start > 1) { // ganz an anfang
					$sStr .= '<a href="'.$_SERVER['PHP_SELF'].'?start=0'.$sGEToptions.'" title="'.$MSG['first_page'][$syslang].'"><small>'.$aIcon['first'].'</small></a>';
					$sep = "&nbsp;"; #$sep = "&nbsp;".$separator."&nbsp;";
				}
				if ($showpage_start > 2) { // eine zurueck
					$sStr .= $sep.'<a href="'.$_SERVER['PHP_SELF'].'?start='.strval(($showpage_start-2)*$this->iLimit).$sGEToptions.'" title="'.$MSG['prev_page'][$syslang].'"><small>'.$aIcon['prev'].'</small></a>';#&laquo;
					$sep = "&nbsp;".$separator."&nbsp;";
				}
				// seitenzahlen-links
				for ($i=0; $i < $pages; $i++) {
					if ($i+1 >= $showpage_start && $i+1 <= $showpage_end) { // nur die im bereich zwischen showpage_start und showpage_end
						if ($page == $i+1)	{$sStr .= $sep.'<a href="'.$_SERVER['PHP_SELF'].'?start='.strval($i*$this->iLimit).$sGEToptions.'"><b>'.strval($i+1).'</b></a>';} // aktuell (highlighted)
						else				{$sStr .= $sep.'<a href="'.$_SERVER['PHP_SELF'].'?start='.strval($i*$this->iLimit).$sGEToptions.'">'.strval($i+1).'</a>';} // normal
					}
					$sep = "&nbsp;".$separator."&nbsp;";
				}
				// pfeile nach rechts
				if ($pages > $showpage_end+1) { // eine weiter
					$sStr .= $sep.'<a href="'.$_SERVER['PHP_SELF'].'?start='.strval($showpage_end*$this->iLimit).$sGEToptions.'" title="'.$MSG['next_page'][$syslang].'"><small>'.$aIcon['next'].'</small></a>';
					$sep = "&nbsp;";
				}
				if ($pages > $showpage_end) { // ganz ans ende
					$sStr .= $sep.'<a href="'.$_SERVER['PHP_SELF'].'?start='.strval(($pages-1)*$this->iLimit).$sGEToptions.'" title="'.$MSG['last_page'][$syslang].'"><small>'.$aIcon['last'].'</small></a>';
				}
			}
			// output
			return $sStr;
		}
	}
?>