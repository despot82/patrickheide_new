<?php // Class form_web
/**
* This class provides HTML-form-functions.
*
* Example:
* <pre><code>
* // init
* $oForm = new form_web($aData); // params: $aData[,$sLang='de']
* // zu pruefende Felder in der gewuenschten Reihenfolge
* $oForm->check_field("title_".$weblang, 'Bitte machen Sie eine Eingabe im Feld Title'); // params: $sFieldname,$sAlertText[,$sCheck='FILLED']
* $oForm->check_field("subtitle_".$weblang, 'Bitte machen Sie eine Eingabe im Feld Subtitle'); // params: $sFieldname,$sAlertText[,$sCheck='FILLED']
* // oeffnendes Form-Tag
* $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions, 'POST', 'editForm'); // params: [$sAction=''][,$sUrlParams=''][,$sMethod=''][,$sName=''][,$sExtra='']
* // Ausgabe
* $oForm->textfield("title_".$weblang); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra='']
* $oForm->textfield("subtitle_".$weblang, 250); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra='']
* $oForm->textarea("description_".$weblang, 2); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra='']
* // buttons
* $oForm->button("save"); // params: $sType[,$sLabel=''][,$sClass='but'][,$sJsOnlick='']
* // schliessendes Form-Tag
* $oForm->end_tag();
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.41 / 2006-01-30	[BUGFIX: $sValue[0]-Behandlung bei "select()"]
*/

class form_web {
	
	/*	----------------------------------------------------------------------------
		Funktionen dieser Funktionsbibliothek:
		----------------------------------------------------------------------------
		constructor form_web($aData='', $sLang='de')
		function check_field($sFieldname, $sAlertText='', $sCheck='FILLED')
		function add_js($sJScode)
		function change_true_into($sJScode)
		function change_delete_msg_into($sMessage)
		function start_tag($sAction='', $sUrlParams='', $sMethod='post', $sName='', $sExtra='')
		
		function hidden($sFieldname, $sDefault='')
		function password($sFieldname, $nMaxlength=50, $nSize=57, $sDefault='', $sExtra='')
		function textfield($sFieldname, $nMaxlength=150, $nSize=57, $sDefault='', $sExtra='')
		function textarea($sFieldname, $nRows=10, $nCols=43, $sDefault='', $sExtra='')
		function file_upload($sFilename, $sFieldname='uploadfile', $sFiletype='image')
		function checkbox($sFieldname, $sDefaultCheckboxValue='1', $sDefaultHiddenValue='0', $sExtra='')
		function radio($sFieldname, $aValues='', $bFirstChecked=false, $bLinebreaks=false)
		function addOptionForSelect($sFieldname, $aValues, $bPre=true)
		function select($sFieldname, $aValues, $sExtra='', $nSize=1, $bMultiple=false)
		function date_dropdown($sFieldname, $bDateRequired=true, $nPastYears=5, $nFutureYears=1)
		function button($sType, $sLabel='', $sClass='', $sJsOnlick='')
		----------------------------------------------------------------------------
		HISTORY:
		1.41 / 2006-01-30	[BUGFIX: $sValue[0]-Behandlung bei "select()"]
		1.4 / 2006-01-23	[NEU: "addOptionForSelect()"]
		1.33 / 2005-12-07	[BUGFIX: Weite bei textfield() und texarea()]
		1.32 / 2005-06-24	[NEU: type "submit" + Kommentare bei "button()"]
		1.31 / 2005-05-19	[BUGFIX: 0-value bei "select()"]
		1.3 / 2005-04-29	[NEU: "password()"]
		1.24 / 2005-04-28	[BUGFIX: value/default-ermittlung in allen methoden mit $sDefault]
		1.23 / 2005-04-19	[$lang in $weblang umbenannt]
		1.22 / 2005-03-29	[BUGFIX: "htmlspecialchars()" bei "hidden()", "textfield()", "textarea()"]
		1.21 / 2005-03-15	[NEU: $sExtra bei "hidden()"]<br>
		1.2 / 2005-03-10	[NEU: $sExtra bei "checkbox()" + "sXtra" ueberall in "sExtra" umbenannt]
		1.1 / 2004-11-05	[NEU: "hidden()"]
		1.06 / 2004-10-11	[BUGFIX: multiple-behandlung bei "select()"]
		1.05 / 2004-09-30	[BUGFIX: $sel bei Year bei "date_dropdown()"]
		1.04 / 2004-09-14	[NEU: class="radiocheckbox" bei radios + checkboxen auf hinzugefuegt]
		1.03 / 2004-09-09	[NEU: beschriftungen von radios + checkboxen auf <label> umgestellt]
		1.02 / 2004-09-06	["--" statt "00" bei "date_dropdown()"]
		1.01 / 2004-06-02	[multiple verarbeiten bei "select()"]
		1.0 / 2004-04-23	[aus der alten Form-Klasse entwickelt]
	*/
	
	// Class Vars.
	var $aData;
	var $sLang;
	var $alert;
	var $sUrlParams;
	var $sName;
	var $js_true_action;
	var $js_delete_msg;
	var $aSelectOption;
	// kopien von objects
	var $oDb;
	var $oBrowser;

/**
* Konstruktor -> Initialisiert das form-Objekt und fuellt es mit dem array $aData (welches i.d.R. aus der DB kommt) 
* und diversen anderen "globalen" Variablen! 
*
* Beispiel:
* <pre><code>
* $oForm = new form_web($aData); // params: $aData[,$sLang='de']
* </code></pre>
*
* @access   public
* @global	object	$oDb		"class.db_mysql.php"
* @global	object	$oBrowser	"class.browsercheck.php"
* @global	object	$oDate		"class.datetime.php" + "class.datetime_db.php"
* @param 	array	$aData		Daten-Array
* @param 	string	$sLang		Sprachversion fuer Buttons / Alerts

*/
	function form_web($aData='', $sLang='de') {
		// check vars
		if ($sLang != 'de' || $sLang != 'en') { $sLang = 'de'; }
		// apply
		$this->aData			= $aData;
		$this->sLang			= $sLang;
		// set defaults
		$this->js_true_action	= 'return true;';
		$this->js_delete_msg 	= 'Wirklich loeschen?';
		$this->aSelectOption	= array();
		// objekte importieren
		global $oDb, $oBrowser;
		$this->oDb			= &$oDb;
		$this->oBrowser		= &$oBrowser;
	}


# -------------------------------------------------------------------------------- JS-PRUEFUNG

/**
* Fuehrt eine JS Pruefung des dieser Methode uebergebenen Formularfeldes durch. 
* ACHTUNG: Funktioniert NUR mit "start_tag()" zusammen un muss VOR dessen Aufruf erfolgen! 
* 
* Beispiel:
* <pre><code>
* $oForm->check_field("title_".$weblang, 'Bitte machen Sie eine Eingabe im Feld Title'); // params: $sFieldname,$sAlertText[,$sCheck='FILLED']
* $oForm->check_field("subtitle_".$weblang, 'Bitte machen Sie eine Eingabe im Feld Subtitle'); // params: $sFieldname,$sAlertText[,$sCheck='FILLED']
* $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions, 'POST', 'editForm'); // params: [$sAction=''][,$sUrlParams=''][,$sMethod=''][,$sName=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	string	$sAlertText		im alert-fenster angezeigter Text
* @param	string	[$sCheck]		[FILLED|NaN|NaNFILLED] (optional - default: 'FILLED')
* @return	js-code
*/
	function check_field($sFieldname, $sAlertText, $sCheck='FILLED') {
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if (!$sAlertText) die("NO sAlertText!");
		$sCheck = strToLower($sCheck);
		// code
		$sJS  = "obj=elements['aData[".$sFieldname."]'];\n";
		switch ($sCheck) {
			// TODO: radio check!
			case 'nan':			$sJS .= "if(isNaN(obj.value))";	break;	// not a number
			case 'nanfilled':	$sJS .= "if(!obj.value || isNaN(obj.value))";	break; // not a number AND filled
			default:			$sJS .= "if(!obj.value)";	break;	// default = filled
		}
		$sJS .= " {ok=false;m=' ".$sAlertText."! ';mfocus=obj;}\n";
		// output
		$this->alert = $sJS.$this->alert; // vorne dranhaengen
	}

/**
* Setzt eine uebergebene JS Pruefung in die entsprechende Stelle der JS-function "checkReqFields()". 
* 
* Beispiel:
* <pre><code>
* $sJScode = "...irgendein js-code der zu der hier verwendeten function checkReqFields()...";
* $oForm->add_js($sJScode); // params: $sJScode
* </code></pre>
*
* @access   public
* @param	string	$sJScode
* @return	js-code
*/
	function add_js($sJScode) {
		// check vars
		if (!$sJScode) die("NO sJScode!");
		// output
		$this->alert = $sJScode."\n".$this->alert; // vorne dranhaengen
	}

/**
* Aendert den die Default-Rueckgabe der JS-function "checkReqFields()" in den uebergebenen String. 
* 
* Beispiel:
* <pre><code>
* $oForm->change_true_into("return confirmSend();"); // params: $sJScode
* $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod=''][,$sName=''][,$sExtra='']
* <script>function confirmSend() { confirm('Are you sure?'); }</script>
* </code></pre>
*
* @access   public
* @param	string	$sJScode
* @return	js-code
*/
	function change_true_into($sJScode) {
		// check vars
		if (!$sJScode) die("NO sJScode!");
		// output
		$this->js_true_action = $sJScode;
	}

/**
* Setzt die Default-Message der JS-function "confirmDelete()". 
* 
* Beispiel:
* <pre><code>
* $oForm->set_delete_msg("Eine Message, die im confirm-window ausgegeben wird."); // params: $sMessage
* $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod=''][,$sName=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	$sMessage
* @return	js-code
*/
	function set_delete_msg($sMessage) {
		// check vars
		if (!$sMessage) die("NO sMessage!");
		// output
		$this->js_delete_msg = $sMessage;
	}

# -------------------------------------------------------------------------------- FORM TAGS

/**
* Baut die von dieser Klasse benoetigten JS-Funktionen und den oeffnenden Form-Tag des HTML-Formulars. 
* Diese Methode zu verwenden ist PFLICHT! ;-) 
* Ausserdem werden die GET-Parameter dieser Seite zentral gespeichert (z.B. fuer buttons ganz wichtig)!) 
* 
* Beispiel:
* <pre><code>
* echo $oForm->start_tag($_SERVER['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod=''][,$sName=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	[$sAction]		"action" Attribut des Form-Tags (optional - default: PHP_SELF)
* @param	string	[$sUrlParams]	speichert die GET-Parameter dieser Seite zentral ab (optional - default: '')
* @param	string	[$sMethod]		"method" Attribut des Form-Tags (optional - default: 'post')
* @param	string	[$sName]		"name" Attribut des Form-Tags (optional - default: '')
* @param	string	[$sExtra]		zusaetzliche Attribute (z.B. JS-Aufruf) fuer den Form-Tag (optional - default: '')
* @return	html-code
*/
	function start_tag($sAction='', $sUrlParams='', $sMethod='post', $sName='', $sExtra='') {
		// check vars
		if (!$sAction) { $sAction = $_SERVER['PHP_SELF']; }
		if ($sUrlParams != '') { $this->sUrlParams = '?'.substr($sUrlParams, 1); }
		if ($sName != '') { $this->sName = $sName; }
		$sNameAtt = (!empty($this->sName)) ? ' name="'.$sName.'" id="'.$sName.'" ' : ' ';
		
		// Form-Tag
		$sFormTag = '<form action="'.$sAction.$this->sUrlParams.'" enctype="multipart/form-data" method="'.$sMethod.'" '.$sNameAtt.$sExtra.">\n";
		
		// schreibt die von dieser Klasse benoetigten JS-Funktionen vor den oeffnenden Form-Tag
		$sJSstart = '<script language="JavaScript" type="text/javascript">'."\n";
		$sJSstart .= "function confirmDelete() {\n";
		$sJSstart .= "	var chk = window.confirm('".$this->js_delete_msg."');\n";
		$sJSstart .= "	return(chk);\n";
		$sJSstart .= "}\n";
		$sJSstart .= "function checkReqFields() { var m,ok,obj,mfocus,frm; ok=true;\n";
		$sJSstart .= (!empty($this->sName)) ? "with (document.forms['".$this->sName."']) {\n" : "with (document.forms[0]) {\n";
		### hier werden die alerts ausgegeben ###
		$sJSend = "if (!ok) {\n";
		$sJSend .= "	if (m) {alert(m);}\n";
		$sJSend .= "	if (mfocus) {mfocus.focus();}\n";
		$sJSend .= "	return false; stop();\n";
		$sJSend .= "} else {".$this->js_true_action."}}}\n";
		$sJSend .= "</script>\n";
		
		// output
		return $sJSstart.$this->alert.$sJSend.$sFormTag;
	}

/**
* Gibt den schliessenden Form-Tag des HTML-Formulars zurueck. 
* --> tatsaechlich ist jedoch der Aufruf dieser Funktion MEHR Tipp-arbeit als pures HTML :(
* 
* @access   public
* @return	html-code
*/
	function end_tag() { return "</form>"; }


# -------------------------------------------------------------------------------- COMMON ELEMENTS

/**
* Baut kompletten <input type="hidden">.
*
* Beispiel:
* <pre><code>
* echo $oForm->hidden("irgendwas"); // params: $sFieldname[,$sDefault=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	string	[$sDefault]		(optional)	(default:  '')
* @param	string	[$sExtra]		(optional)	(default:  '')
* @return	html-code
*/
	function hidden($sFieldname, $sDefault='', $sExtra='') {
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname]) && !empty($this->aData[$sFieldname])) 
			? $this->aData[$sFieldname] 
			: $sDefault;
		if (!empty($sExtra)) { $sExtra = ' '.trim($sExtra); }
		
		// output
		return '<input type="hidden" name="aData['.$sFieldname.']" id="'.$sFieldname.'" value="'.htmlspecialchars($sValue, ENT_QUOTES).'"'.$sExtra.' />';
	}

/**
* Baut kompletten <input type="password">.
*
* Beispiel:
* <pre><code>
* echo $oForm->password("password"); // params: $sFieldname[,$nMaxlength=50][,$nSize=57][,$sDefault=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	integer	[$nMaxlength]	(optional)	(default: 50)
* @param	integer	[$nSize]		(optional)	(default:  57)
* @param	string	[$sDefault]		(optional)	(default:  '')
* @param	string	[$sExtra]		(optional)	(default:  '')
* @return	html-code
*/
	function password($sFieldname, $nMaxlength=50, $nSize=57, $sDefault='', $sExtra='') {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname]) && !empty($this->aData[$sFieldname])) 
			? $this->aData[$sFieldname] 
			: $sDefault;
		if (!empty($sExtra)) { $sExtra = ' '.trim($sExtra); }
		
		// output
		return '<input type="password" name="aData['.$sFieldname.']" id="'.$sFieldname.'" value="'.htmlspecialchars($sValue, ENT_QUOTES).'" size="'.$nSize.'" maxlength="'.$nMaxlength.'"'.$sExtra.' />';
	}

/**
* Baut kompletten <input type="text">.
*
* Beispiel:
* <pre><code>
* echo $oForm->textfield("title_".$weblang, 250); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	integer	[$nMaxlength]	(optional)	(default: 150)
* @param	integer	[$nSize]		(optional)	(default:  57)
* @param	string	[$sDefault]		(optional)	(default:  '')
* @param	string	[$sExtra]		(optional)	(default:  '')
* @return	html-code
*/
	function textfield($sFieldname, $nMaxlength=150, $nSize=57, $sDefault='', $sExtra='') {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname]) && !empty($this->aData[$sFieldname])) 
			? $this->aData[$sFieldname] 
			: $sDefault;
		if (!empty($sExtra)) { $sExtra = ' '.trim($sExtra); }
		
		$nSize = round($nSize * 6.2);
		// output
		#return '<input type="text" name="aData['.$sFieldname.']" id="'.$sFieldname.'" value="'.htmlspecialchars($sValue, ENT_QUOTES).'" size="'.$nSize.'" maxlength="'.$nMaxlength.'"'.$sExtra.'>';
		return '<input type="text" name="aData['.$sFieldname.']" id="'.$sFieldname.'" value="'.htmlspecialchars($sValue, ENT_QUOTES).'" '.get_input_size($nSize).' maxlength="'.$nMaxlength.'"'.$sExtra.' />';
	}

/**
* Baut komplette textarea.
*
* Beispiel:
* <pre><code>
* echo $oForm->textarea("description_".$weblang, 2); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	integer	[$nRows]	(optional)	(default: 10)
* @param	integer	[$nCols]	(optional)	(default: 43)
* @param	string	[$sDefault]	(optional)	(default:  '')
* @param	string	[$sExtra]	(optional)	(default:  '')
* @return	html-code
*/
	function textarea($sFieldname, $nRows=10, $nCols=78, $sDefault='', $sExtra='') {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname]) && !empty($this->aData[$sFieldname])) 
			? $this->aData[$sFieldname] 
			: $sDefault;
		if (!empty($sExtra)) { $sExtra = ' '.trim($sExtra); }
		
		// mac-/mozilla-breite an pc-breite anpassen
		#if ($this->oBrowser->isMac() && !$this->oBrowser->isIE()) { $nCols = round($nCols / 100 * 127); }
		#if ($this->oBrowser->isMac() && $this->oBrowser->isIE()) { $nCols = round($nCols / 100 * 160); }
		#if ($this->oBrowser->isMoz()) { $nCols = round($nCols * 96); }
		$nCols = round($nCols * 6.2);
		// hoehe anpassen
		if (!$this->oBrowser->isIE()) { $nRows = $nRows - 1; }
		// output
		#return '<textarea  rows="'.$nRows.'" cols="'.$nCols.'" name="aData['.$sFieldname.']" id="'.$sFieldname.'" wrap="virtual"'.$sExtra.'>'.htmlspecialchars($sValue, ENT_QUOTES)."</textarea><br>\n";
		return '<textarea  rows="'.$nRows.'" '.get_textarea_size($nCols).' name="aData['.$sFieldname.']" id="'.$sFieldname.'" '.$sExtra.'>'.htmlspecialchars($sValue, ENT_QUOTES)."</textarea><br />\n";
	}

/**
* Baut einfachen 'file-upload' (file-input, select-/replace-buttons und thumbnail/icon etc.) auf einer beliebigen seite. 
* ACHTUNG: Benoetigt natuerlich vor den db-actions entsprechendes file-upload handling! (-> s. 'media_detail.php')
* NOTE: diese Funktion benoetigt im Moment (im Gegensatz zu allen anderen) den value (hier $sFilename) direkt uebergeben (da nicht bekannt ist, wie das db-field heisst)!
*
* Beispiel Funktionen:
* <pre><code>
* // DB-action: delete
* 	if (isset($btn['delete']) && $aData['id']) {
* 		// delete file
* 		$oFile->delete_files($aENV['path']['upload']['unix'].$existing_uploadfile);
* 		// delete data
* 		$aData2delete['id'] = $aData['id']; // Delete data from editwiew
* 		$oDb->make_delete($aData2delete,$sTable);
* 		header("Location: ".$sViewerPage); exit;
* 	}
* // DB-action: save or update
* 	if (isset($btn['save']) || $remoteSave==1) {
* 		// delete old file
* 		if ((isset($delete_uploadfile) || isset($uploadfile_name)) && isset($existing_uploadfile)) {
* 			$oFile->delete_files($aENV['path']['upload']['unix'].$existing_uploadfile);
* 		}
* 		// upload new file
* 		if ($uploadfile_name) {
* 			$upload1 = new upload("uploadfile");
* 			$upload1->set_syslang($syslang); // params: $syslang
* 			$upload1->check_filename_length(40, $aMSG['media']['alert_length']); // params: [$maxlength=40][,$error_msg (als array)]
* 			$upload1->check_filename($aMSG['media']['alert_chars']); // params: [$error_msg (als array)]
* 			if (!($aData['pdf'] = $upload1->make_upload($aENV['path']['upload']['unix']))) {$upload1->script_alert(); $alert=true;}
* 		}
* 		if (!$alert) {
* 			if ($aData['id']) { // Update eines bestehenden Eintrags
* 				$oDb->make_update($aData,$sTable,$aUpdate);
* 			} else { // Insert eines neuen Eintrags
* 				$oDb->make_insert($aData,$sTable);
* 				$aData['id'] = $oDb->insert_id();
* 			}
* 		}
* </code></pre>
*
* Beispiel Formulareinbau:
* <pre><code>
* 	echo $oForm->file_upload($aData['filename']); // params: $sFilename[,$sFieldname='uploadfile'][,$sFiletype='image']
* </code></pre>
*
* @access   public
* @param	string	[$sFilename]	(optional)
* @param	string	[$sTargetPath]	(optional -> default:'./')
* @param	string	[$sFieldname]	(optional -> default:'uploadfile')
* @param	string	[$sFieldname]	(optional -> default:'image') [image|flash|pdf|zip|other]
* @return	html-code
*/
	function file_upload($sFilename, $sTargetPath='./', $sFieldname='uploadfile', $sFiletype='image') {
		
		// vars
		if (substr($sTargetPath, -1) != '/') { $sTargetPath .= '/'; }
		$MSG = array(); // texte
		$MSG['btn_replace']['de'] = "Ersetzen";
		$MSG['btn_replace']['en'] = "Replace";
		$MSG['btn_delete']['de'] = "Löschen";
		$MSG['btn_delete']['en'] = "Delete";
		$MSG['no_selected']['de'] = "Keine Datei ausgewählt.";
		$MSG['no_selected']['en'] = "No file selected.";
		
		// if $sFilename -> show icon/thumbnail and replace-button
		if (!empty($sFilename)) {
			$sStr = '<input type="hidden" name="existing_'.$sFieldname.'" id="existing_'.$sFieldname.'" value="'.$sFilename.'">'."\n";
			$sStr .= '<a href="'.$sTargetPath.$sFilename.'" target="_blank">'."\n";
			switch ($sFiletype) {
				case "image":
					$sStr .= '<img src="'.$sTargetPath.$sFilename.'" width="40" alt="Image ['.$sFilename.']" border="0" />';
					break;
				case "flash":
					$sStr .= '<img src="./web/pix/icn_flash.gif" width="22" height="18" alt="View Flash ['.$sFilename.']" border="0" />';
					break;
				case "pdf":
					$sStr .= '<img src="./web/pix/icn_pdf.gif" width="22" height="18" alt="View PDF ['.$sFilename.']" border="0" />';
					break;
				case "zip":
					$sStr .= '<img src="./web/pix/icn_zip.gif" width="width="22" height="18" alt="View ZIP ['.$sFilename.']" border="0" />';
					break;
				case "other":
					$sStr .= '<img src="./web/pix/icn_file.gif" width="22" height="18" alt="View Uploadfile ['.$sFilename.']" border="0" />';
					break; // alle anderen media-typen
			}
			$sStr .= '&nbsp;&nbsp;<span class="small"><strong>'.$sFilename.'</strong></span>'."\n";
			$sStr .= '</a>'."\n";
			
			$sStr .= '&nbsp;&nbsp;<input type="checkbox" name="delete_'.$sFieldname.'" id="delete_'.$sFieldname.'" value="1" class="radiocheckbox">&nbsp;<span class="small" />'.$MSG['btn_delete'][$this->sLang].'</span>'."\n";
			$sStr .= '<br><span class="small">'.$MSG['btn_replace'][$this->sLang].':<br /> </span><input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'" />';
		} else { // new-mode: only file-input field
			$sStr .= '<input type="file" size="25" name="'.$sFieldname.'" id="'.$sFieldname.'" />';
		}
		
		// output
		return $sStr;
	}

/**
* Baut checkbox (mit defaultValue: "1") und hidden-field davor (mit defaultValue: "0").
*
* Beispiel:
* <pre><code>
* echo $oForm->checkbox("flag_irgendwas","yes","no"); // params: $sFieldname[,$sDefaultCheckboxValue='1'][,$sDefaultHiddenValue='0'][,$sExtra=''][,$sExtra='']
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	string	[$sDefaultCheckboxValue]	(optional -> default: '1')
* @param	string	[$sDefaultHiddenValue]		(optional -> default: '0')
* @param	string	[$sExtra]					(optional -> default: '')
* @return	html-code
*/
	function checkbox($sFieldname, $sDefaultCheckboxValue='1', $sDefaultHiddenValue='0', $sExtra='') {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		$sel = ($sValue == $sDefaultCheckboxValue) ? ' checked="checked"' : '';
		if (!empty($sExtra)) { $sExtra = ' '.trim($sExtra); }
		
		// code
		$sStr = '<input type="hidden" name="aData['.$sFieldname.']" value="'.$sDefaultHiddenValue.'" />'."\n";
		$sStr .= '<input type="checkbox" name="aData['.$sFieldname.']" id="'.$sFieldname.'" value="'.$sDefaultCheckboxValue.'" class="radiocheckbox"'.$sExtra.''.$sel.' />';
		
		// output
		return $sStr;
	}

/**
* Baut radio-buttons (aus array). 
*
* Defaultmaessig wird eine Ja/Nein (mit 0/1-keys) Kombi geschrieben (2sprachig). Man kann aber durch 
* Angabe des 2. Parameters (in der Form "array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...) 
* auch andere Kombis erzeugen. 
* Der optionale 3. Parameter bestimmt, ob der erste Eintrag default-maessig "checked" sein soll oder 
* nicht (default). 
* Der optionale 4. Parameter bestimmt, ob die Radio-buttons untereinander oder nebeneineander (default) 
* ausgegeben werden. 
*
* Beispiel:
* <pre><code>
* echo $oForm->radio("flag_irgendwas"); // params: $sFieldname[,$aValues=''][,$bFirstChecked=false][,$bLinebreaks=false]
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	array	[$aValues]			(optional -> default: array('0'=>'Nein/No','1'=>'Ja/Yes')
* @param	boolean	[$bFirstChecked]	(optional -> default: false)
* @param	boolean	[$bLinebreaks]		(optional -> default: false)
* @return	html-code
*/
	function radio($sFieldname, $aValues='', $bFirstChecked=false, $bLinebreaks=false) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		// texte
		$MSG = array();
		$MSG['yes']['de'] = "Ja";
		$MSG['yes']['en'] = "Yes";
		$MSG['no']['de'] = "Nein";
		$MSG['no']['en'] = "No";
		// set default
		$sStr = '';
		$aDefaultValues = array('1' => $MSG['yes'][$this->sLang], '0' => $MSG['no'][$this->sLang]);
		if ($aValues=='') { $aValues = $aDefaultValues; }
		// ggf. ersten array-eintrag als "checked" markieren
		if ($sValue == '' && $bFirstChecked) { reset($aValues); $sValue = key($aValues); }
		
		// code
		foreach ($aValues as $key => $val) {
			$sel = ($key == $sValue) ? ' checked="checked"' : '';
			$sStr .= '<input type="radio" name="aData['.$sFieldname.']" id="'.$sFieldname.'_'.$key.'" value="'.$key.'" class="radiocheckbox"'.$sel.' />';
			$sStr .= '<label for="'.$sFieldname.'_'.$key.'" class="text">'.$val.'</label> ';
			if ($bLinebreaks == true) { $sStr .= '<br />'; }
		}
		
		// output
		return $sStr;
	}

/**
* Hier koennen optionale Option-Eintraege fuer das Dropdown (Select-Field) definiert werden, die vor oder hinter das Array das den Dropdown uebergeben wird. 
* Der 2. Parameter (in der Form "array('key','value') befuellt den <option>-tag. 
* Der optionale 3. Parameter steuert, ob die Option vorne (default) oder hinten angehaengt wird.
* ACHTUNG: Damit diese Methode funktioniert, muss sie VOR der Methode "select()" aufgerufen werden!
*
* Beispiel:
* <pre><code>
* $oForm->addOptionForSelect("job_cat", array('','bitte ausws')); // params: $sFieldname,$aValues[,$bPre=true]
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	array	$aValues		(required: array('key1', 'value1')
* @param	boolean	[$bPre]			(optional: bei false wird die option hintenangehaengt -> default: true)
* @return	void
*/
	function addOptionForSelect($sFieldname, $aValues, $bPre=true) {
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if (!$aValues) die("NO aValues!");
		if (!is_array($aValues) && is_string($aValues)) {
			$aValues = array($aValues, $aValues);
		}
		$pre = ($bPre) ? 'pre' : 'post';
		// option hinzufuegen
		$this->aSelectOption[$sFieldname][$pre][] = $aValues;
	}

/**
* Baut komplettes Dropdown (Select-Field) aus array. 
* Der 2. Parameter (in der Form "array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...) erzeugt die
* <option>-tags. Im optionalen 3. Parameter kann man zusaetzliche Attribute (js-eventhandler,class,...)
* uebergeben.
*
* Beispiel:
* <pre><code>
* $aOptions = array('key1'=>'value1', 'key2'=>'value2', 'key3'=>'value3')
* echo $oForm->select("select_bsp", $aOptions, 'onchange="tuwas(this)"'); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false]
* </code></pre>
*
* @access   public
* @param	string	$sFieldname
* @param	array	$aValues		(required: array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...)
* @param	int		[$sExtra]		(optional: Attribute fuer den SELECT-tag -> default: '')
* @param	int		[$nSize]		(optional: Attribut "size" -> default: 1)
* @param	boolean	[$bMultiple]	(optional: Attribut "multiple" -> default: false)
* @return	html-code
*/
	function select($sFieldname, $aValues, $sExtra='', $nSize=1, $bMultiple=false) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if (!empty($sExtra)) { $sExtra = ' '.trim($sExtra); }
		
		$m1 = ($bMultiple == true) ? '[]' : '';
		$m2 = ($bMultiple == true) ? ' multiple="multiple"' : '';
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		if ($bMultiple == true && is_string($sValue)) { // wenn multiple, aber value==kommasep.-string:
			$sValue = explode(',', $sValue);
		}
		if (isset($sValue[0]) && $sValue[0] == '') unset($sValue[0]); // ersten array-eintrag loeschen wenn == 0!
		//print_r($sValue); // DEBUG
		
		// code
		$sStr = '<select name="aData['.$sFieldname.']'.$m1.'" id="'.$sFieldname.'" size="'.$nSize.'"'.$sExtra.$m2.">\n";
		if (count($this->aSelectOption[$sFieldname]['pre']) > 0) {
			foreach ($this->aSelectOption[$sFieldname]['pre'] as $aOpt) {
				$sStr .= '<option value="'.$aOpt[0].'">'.$aOpt[1].'</option>'."\n";
			}
		}
		foreach ($aValues as $key => $val) {
			if ($bMultiple == true && is_array($sValue)) {
				$sel = (in_array($key, $sValue)) ? ' selected="selected"' : '';
			} else {
				$sel = ($sValue != '' && $key == $sValue) ? ' selected="selected"' : ''; // "!empty()" ist nicht das selbe wie "!= ''"
			}
			$sStr .= '<option value="'.$key.'"'.$sel.'>'.$val.'</option>'."\n";
		}
		if (count($this->aSelectOption[$sFieldname]['post']) > 0) {
			foreach ($this->aSelectOption[$sFieldname]['post'] as $aOpt) {
				$sStr .= '<option value="'.$aOpt[0].'">'.$aOpt[1].'</option>'."\n";
			}
		}
		$sStr .= "</select>\n";
		
		// output
		return $sStr;
	}


# -------------------------------------------------------------------------------- DATE/TIME

/**
* Baut als Datums-Eingabe drei Dropdowns (fuer tag/monat/jahr). 
*
* ACHTUNG: 
* benoetigt folgende zeile vor der db-speicherung: 
*	$aData['date'] = $_POST['date']['year']."-".$_POST['date']['month']."-".$_POST['date']['day']; // merge date-dropdowns into a valid mysql- date 
* plus analog bei verwendung von mehreren dropdowns: 
*	$aData['date2'] = $_POST['date2']['year']."-".$_POST['date2']['month']."-".$_POST['date2']['day']; // wobei hier 'date2' der DB-feldname ist 
*	$aData['dateFoo'] = $_POST['dateFoo']['year']."-".$_POST['dateFoo']['month']."-".$_POST['dateFoo']['day']; // wobei hier 'dateFoo' der DB-feldname ist 
*	... 
* (am besten (wie hier gezeigt) gleich aus dem POST-array referenzieren - dann muessen die vars nicht extra vorher abgeholt werden...) 
*
* Beispiel: 
* <pre><code>
* echo $oForm->date_dropdown("date"); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]
* echo $oForm->date_dropdown("date2"); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]
* echo $oForm->date_dropdown("dateFoo", 20, 2); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]
* </code></pre>
*
* @access   public
* @param	string		$sFieldname
* @param	string		$bDateRequired	[bei angabe nicht required (false) wird auch ein Null-Value angezeigt -> default:true]
* @param	integer		$nPastYears		[wieviele Jahre i.d. Vergangenheit werden angezeigt -> default:5]
* @param	integer		$nFutureYears	[wieviele Jahre i.d. Zukunft werden angezeigt -> default:1]
* @return	html-code
*/
	function date_dropdown($sFieldname, $bDateRequired=true, $nPastYears=5, $nFutureYears=1) {
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		$sValue = (isset($this->aData[$sFieldname])) ? $this->aData[$sFieldname] : '';
		// vars
		$sel = '';
		$sStr = '';
		$bSelYear = false; // wird zur erkennung des "selected"-Attributs bei "Year" benutzt
		
		// code
		
		// make data-date
		if ($sValue != '') {
			list($sDataYear, $sDataMonth, $sDataDay) = explode("-", $sValue);
		} else {$sDataYear=''; $sDataMonth=''; $sDataDay='';}
		
		// make today-date
		$sTodayYear		= date("Y");
		$sTodayMonth	= date("m");
		$sTodayDay		= date("d");
		
		// build select-field 'day'
		$sStr = '<select name="'.$sFieldname.'[day]" id="'.$sFieldname.'_day" size="1">';
		for ($i=0; $i<32; $i++) {
			if ($i < 10) { $i = "0".$i; }
			if ($sDataDay) {
				$sel = ($i == $sDataDay) ? ' selected="selected"' : '';
			} else {
				$sel = ($i == $sTodayDay && $bDateRequired == true) ? ' selected="selected"' : ''; // today = selected bei NO $sData && required
			}
			$sStr .= "<option value=\"$i\"$sel>";
			$sStr .= ($i == '00') ? '--' : $i;
			$sStr .= "</option>";
		}
		$sStr .= "</select>\n";
		
		// build select-field 'month'
		$sStr .= '<select name="'.$sFieldname.'[month]" id="'.$sFieldname.'_month" size="1">';
		for ($i=0; $i<13; $i++) {
			if ($i < 10) { $i = "0".$i; }
			if ($sDataMonth) {
				$sel = ($i == $sDataMonth) ? " selected" : '';
			} else {
				$sel = ($i == $sTodayMonth && $bDateRequired == true) ? ' selected="selected"' : ''; // today = selected bei NO $sData && required
			}
			$sStr .= "<option value=\"$i\"$sel>";
			$sStr .= ($i == '00') ? '--' : $i;
			$sStr .= "</option>";
		}
		$sStr .= "</select>\n";
		
		// build select-field 'year'
		$sStr .= '<select name="'.$sFieldname.'[year]" id="'.$sFieldname.'_year" size="1">';
		// Null-Jahr bei nicht required
		if ($bDateRequired == false) {
			if ($sDataYear == '0000' || !$sDataYear) { $sel = ' selected="selected"'; $bSelYear = true; } // 0000 = selected bei NO-$sData && nicht required
			$sStr .= "<option value=\"0000\"$sel>----</option>"; // Null-Value-Option bei "nicht required"
		}
		// next year(s)
		for ($i=$nFutureYears; $i >= 1; $i--) { 
			$sStr .= '<option value="'.($sTodayYear+$i).'"';
			if ($sDataYear == ($sTodayYear+$i)) { $sStr .= ' selected="selected"'; $bSelYear = true; }
			$sStr .= '>'.($sTodayYear+$i).'</option>';
		}
		// this year
		$sStr .= '<option value="'.($sTodayYear).'"'; 
		if ($sDataYear == $sTodayYear || (!$sDataYear && $bDateRequired == true)) { $sStr .= " selected"; $bSelYear = true; } // today = selected bei NO $sData && required
		$sStr .= '>'.($sTodayYear).'</option>';
		// past year(s)
		for ($i=1; $i <= $nPastYears; $i++) { 
			$sStr .= '<option value="'.($sTodayYear-$i).'"';
			if ($sDataYear == ($sTodayYear-$i)) { $sStr .= ' selected="selected"'; $bSelYear = true; }
			$sStr .= '>'.($sTodayYear-$i).'</option>';
		}
		// wenn $sDataYear nicht bei der Vorauswahl dabei ist, direkt $sDataYear anzeigen
		if ($sDataYear && $bSelYear == false) {
			$sStr .= '<option value="'.$sDataYear.' selected="selected">'.$sDataYear.'</option>';
		}
		$sStr .= "</select>\n";
		
		// output
		return $sStr;
	}


# -------------------------------------------------------------------------------- BUTTONS

/**
* Baut kompletten <input type="button">. 
*
* Beispiel:
* <pre><code>
* echo $oForm->button("DELETE"); // params: $sType[,$sLabel=''][,$sClass='but'][,$sJsOnlick='']
* </code></pre>
*
* @access   public
* @param	string	$sType			[SAVE|DELETE|DEL_ALL|CANCEL|RESET|BACK|NEW|SEARCH|CLOSE_WIN]
* @param	string	$sLabel			(optional)
* @param	string	$sClass			(optional)
* @param	string	$sJsOnlick		(optional)
* @return	html-code
*/
	function button($sType, $sLabel='', $sClass='', $sJsOnlick='') {
		
		// vars
		if (!$sType) die('NO sType!');
		$type = strToLower($sType);
		
		// code
		switch ($type) {
			case 'submit':		$t='submit';	$class='but';	$js=' onClick="return checkReqFields()"';	break;
			case 'save':		$t='submit';	$class='but';	$js=' onClick="return checkReqFields()"';	break;
			case 'delete':		$t='submit';	$class='but';	$js=' onClick="return confirmDelete()"';	break;
			case 'back':		$t='submit';	$class='but';	$js=' onClick="location.back();"';	break;
			case 'close_win':	$t='submit';	$class='but';	$js=' onClick="javascript:self.close()"';	break;
			default:			$t = $type;		$class='but';	$js='';	break;
		}
		if ($sClass != '')		{ $class = $sClass; }
		if ($sJsOnlick != '')	{ $js = $sJsOnlick; }
		if (empty($sLabel))		{ $sLabel = $sType; }
		
		$sStr = '<input type="'.$t.'" value="'.$sLabel.'" name="btn['.$sType.']" id="'.$sType.'" class="'.$class.'"'.$js.' />';
		
		// output
		return $sStr;
	}

#-----------------------------------------------------------------------------
} // END of class

?>