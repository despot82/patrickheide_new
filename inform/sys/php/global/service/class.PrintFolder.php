<?php
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung eine Verzeichnisstruktur in HTML auszugeben. 
* ACHTUNG: Die Verzeichnisstruktur muss mit der Klasse "class.tree.php" kompatibel sein! 
* Diese Klasse benoetigt ebenfalls die Klasse tree (sowie ggf. die Klasse TACEA).
* NOTE: Diese Klasse holt sich ALLE verwendeten HTML-Tags aus der Klasse "class.HtmlFolder.php" (wie auch die Klassen "cms_navi" und "pms_navi").
*
* Example: 
* <pre><code> 
* require_once($aENV['path']['global_service']['unix'].'class.PrintFolder.php'); 
* $oPrintFolder =& new PrintFolder($oDb, $aENV['table']['global_tree'], FORUM_STRUCTURE_FILE, 'frm', array("sOrderBy" => " prio DESC"), $Userdata['id']);
* // JavaScript Icon-Preloader
* echo $oPrintFolder->getJS_IconPreloader();
* // write NAVI
* $oPrintFolder->writeNavi($current_folder, $href, $divider);
* // Default-Aufklappen
* echo $oPrintFolder->getJS_OpenDefault()
* </code></pre>
*
* @access   public
* @package  service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2006-02-01	[NEU: Anbindung der class.HtmlFolder.php]
* @uses		class.tree.php
* @uses		class.HtmlFolder.php
* @uses		class.TeamAccessControllEA.php
* @uses		class.TeamAccessControllBean.php
* @see		class.cms_navi.php
* @see		class.pms_navi.php
*/
class PrintFolder {

	/*	----------------------------------------------------------------------------
		Funktionen der Klasse PrintFolder:
		----------------------------------------------------------------------------
		konstruktor PrintFolder(&$oDb, $sTable, $sCachefile='', $module='', $aOptions=array(), $nAuthId=NULL)
		function writeNavi($current_folder=0, $href='', $divider='hDivider')
		function getJS_IconPreloader()
		function getJS_OpenDefault()
		----------------------------------------------------------------------------
		HISTORY:
		1.1 / 2006-02-01	[NEU: Anbindung der class.HtmlFolder.php]
		1.0 / 2006-01-31
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	array	globales Environment-Array
*/
	var $aENV = array();
/**
* @access   private
* @var	 	object	HtmlFolder-Object
*/
	var $oHtmlFolder = NULL;
/**
* @access   private
* @var	 	object	Tree-Object
*/
	var $oTree = NULL;
/**
* @access   private
* @var	 	array	
*/
	var $aHighlightId = array();
/**
* @access   private
* @var	 	string	
*/
	var $sFolderHref = '';
/**
* ID des aktuellen Folders
* @access   private
* @var	 	integer	
*/
	var $nCurrentFolder = 0;
/**
* Einrueckung in Pixeln
* @access   private
* @var	 	integer	
*/
	var $nIndent = 0;

#-----------------------------------------------------------------------------

/**
* konstruktor PrintFolder() -> Initialisiert das Objekt.
*
* @access   public
* @global	array	$aENV	globales Environment-Array
* @return	void
*/
	function PrintFolder(&$oTree) {
		// Set vars
		global $aENV;
		$this->aENV =& $aENV;
		$this->nCurrentFolder = 0;
		// Einrueckung in Pixeln
		$this->nIndent = 17;
		
		// Tree-Objekt importieren
		if (!is_object($oTree)) {
			echo "<script>alert('".basename(__FILE__)."-ERROR: \\n -> Es wurde kein Tree-Objekt uebergeben!')</script>";
			return;
		}
		$this->oTree = $oTree;
		
		// init HtmlFolder
		require_once($aENV['path']['global_service']['unix'].'class.HtmlFolder.php');
		$this->oHtmlFolder =& new HtmlFolder();
		
	}

#-----------------------------------------------------------------------------

// schreibe die komplette navi-struktur rekursiv als html-links mit dhtml und icons
	function writeNavi($current_folder=0, $href='', $divider='hDivider') { 
		// set vars
		$this->nCurrentFolder = $current_folder;
		$this->sFolderHref = $href;
		// ermittle alle aufzuklappenden naviIDs
		$this->aHighlightId = $this->getHighlightIds($current_folder);
		
		$aStructure = $this->oTree->getAllChildren();
		$this->_writeNavi($aStructure, 0, 0, $divider);
	}
	// sub von "writeNavi()"
	function _writeNavi(&$data, $depth=0, $id=0, $divider='hDivider') { 
		if (!is_array($data)) return; 
		// umschliessender layer ist nicht noetig im level 0, da immer aufgeklappt
		if ($depth > 0) { // alle tieferen level erst mal geschlossen
			###echo '<div id="nLayer_'.$id.'" style="visibility:visible;display:none">'."\n";
			echo $this->oHtmlFolder->getLayerStart($id);
		}
		// ebene durchlaufen
		foreach ($data as $key => $val) {
			
			// get sub-navi array
			$nEntriesSub = count($val['children']); // get total sub-entries
			
			// navi entry
			echo $this->getNaviItem($val, $nEntriesSub, $depth);	#print_r($val); // DEBUG
			
			// rekursiv weiterhangeln
			if ($nEntriesSub > 0) { $this->_writeNavi($val['children'], ++$depth, $val['id']); --$depth; }
			
			// divider fuer level 0 (drueber)
			if ($depth == 0) {
				###echo '<div class="'.$divider.'"><img src="'.$this->aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"></div>'."\n";
				echo $this->getDivider($divider);
			}
		}
		// umschliessenden layer schliessen
		if ($depth > 0) {
			###echo '</div>'."\n";
			echo $this->oHtmlFolder->getLayerEnd();
		}
	}
// sub von "_writeNavi()": gebe den Divider zurueck
	function getDivider($class) {
		// output
		return $this->oHtmlFolder->getDivider($class);
	}
// sub von "_writeNavi()": gebe den aktuellen navi-item formatiert zurueck
	function getNaviItem(&$aItem, $nEntriesSub=0, $depth=0) { 
		// einrueckung
		$sStr = $this->getEinrueckung($depth);
		// toggle icon
		$sStr .= $this->getIcon($aItem['id'], $nEntriesSub);
		// highlight
		#$class	= (in_array($aItem['id'], $this->aHighlightId)) ? 'cnavihi' : 'cnavi';
		$class	= ($aItem['id'] == $this->nCurrentFolder) ? 'cnavihi' : 'cnavi';
		// build href
		$href = $this->sFolderHref.$aItem['id'];
		// wenn nicht freigegeben erscheint Eintrag kursiv
		$kursiv = (array_key_exists('flag_online', $aItem) && $aItem['flag_online'] == "0") ? true : false;
		$sStr .= $this->getLink($href, $class, $aItem['title'], $kursiv);
		// output
		return $sStr;
	}
// sub von "getNaviItem()": gebe die richtigen einrueckung zurueck
	function getEinrueckung($depth) {
		$spacer_width = 0;
		for ($i = 0; $i < $depth; $i++) { $spacer_width += $this->nIndent; }
		// output
		return ($spacer_width > 0) ? $this->oHtmlFolder->getSpacer($spacer_width) : '';
	}
// sub von "getNaviItem()": gebe das richtige icon zurueck
	function getIcon($id, $nEntriesSub) {
		// output
		return ($nEntriesSub > 0) 
			? $this->oHtmlFolder->getIconFolder($id) 
			: $this->oHtmlFolder->getIconFile();
	}
// sub von "getNaviItem()": gebe den kompletten Link zurueck
	function getLink($href, $class, $link, $kursiv=false) {
		// text kuerzen
		$link =  shorten_text($link, 25);
		// ggf. kursiv
		if ($kursiv) { $link = '<i>'.$link.'</i>'; }
		// output
		return $this->oHtmlFolder->getLink($href, $class, $link);
	}
	
// schreibe ein JavaScript fuer den Vorlader fuer Toggle-Icon-tausch
	function getJsIconPreloader() {
		// output
		return $this->oHtmlFolder->getJsIconPreloader();
	}
// schreibe ein JavaScript, mit dem defaultmaessig die richtigen unterpunkte aufgeklappt werden
	function getJsOpenDefault() {
		$highlight_ids = implode('","', $this->aHighlightId);
		// output
		return $this->oHtmlFolder->getJsOpenDefault($highlight_ids);
	}
// ermittle alle folder-ids, die gehighlightet werden muessen
	function getHighlightIds($current_folder) {
		$buffer = array();
		if (empty($current_folder) || $current_folder == 0) return $buffer;
		// ermittle alle parent-IDs
		$aParentId = $this->oTree->getAllParentId($current_folder);
		if (!is_array($aParentId)) return $buffer;
		foreach ($aParentId as $pid) {
			if ($pid > 0) { $buffer[] = $pid; } // wenn nicht root -> dazu
		}
		$buffer[] = $current_folder; // + sich selbst auch dazu
		// output
		return $buffer;
	}
// ermittle alle folder, die als Pfad/Breadcrumbs angezeigt werden muessen
	function getBreadcrumbs($current_folder) {
		global $aMSG, $syslang;
		$path = '';
		$buffer = array();
		#if (empty($current_folder) || $current_folder == 0) return $path;
		if ($current_folder == '') return $path;
		// ermittle alle parent-IDs
		$buffer = $this->oTree->getAllParentId($current_folder);
		$buffer[] = $current_folder; // + sich selbst auch dazu
		// build breadcrumbs
		foreach ($buffer as $pId) {
			if ($pId == -1) {
				$path .= ''.$aMSG['media']['upload_folder'][$syslang].'/';
			} elseif ($pId > 0) {
				$path .= ''.$this->oTree->getValueById($pId, "title").'/';
			} else {
				$path .= ''.$this->oTree->sRootName.'/';
			}
		}
		
		// output
		return $path;
	}

#-----------------------------------------------------------------------------

} // END class
?>