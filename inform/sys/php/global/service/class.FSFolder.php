<?php
/**
 * Klasse zum einfachen Auslesen eines Verzeichnisses (rekursiv).
 * Die Klasse kann diegefundenen Files und Folders hierarchisch (immer aufgeklappt) darstellen.
 * Standardmaessig sind die Namen der Dateien verlinkt.
 * Optional kann ein JS-Handler uebergeben werden.
 *
 * Created on 19.12.2005
 * 
 * @author Oliver Hilscher <oh@design-aspekt.com>
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @author Andy Fehn <af@design-aspekt.com>
 * @param String $path
 * @param array $aENV
 * @version 1.1
 */

	class FSFolder {
	
		var $path = '';
		var $extpath = null;
		var $filedir = array();
		var $directories = null;
		var $files = null;
		var $aENV = null;
		var $space = null;
		var $openDepth = 2;
		var $_html = array();
		var $jsHandler = '';
		var $sortdir	= '';
		var $url	= '';
		
		function FSFolder($path,&$aENV, $sSort = 'ASC', $sUrl = '') {
			$len = strlen($path);
			// Wenn am Ende des uebergenem Ordners ein / steht wird dieser weggeschnitten

			if($path[($len-1)]=='/') {
				$path = substr($path,0,($len-1));
			}
			$this->path = $path;
			$this->aENV =& $aENV;
			
			$this->sortdir	= $sSort;
			$this->url	= $sUrl;

			$this->initialize();
		}
	
		function initialize() {
			// init MDB
			include_once($this->aENV['path']['global_service']['unix'].'class.mediadb.php');
			$this->oMediadb =& new MediaDb();
			// set default (Einrueckung in Pixeln)
			$this->space = $this->aENV['ftp']['spacer'];
			// HTML elemente
			$this->_html['spacer']		= '<img src="'.$this->aENV['path']['pix']['http'].'onepix.gif" width="{WIDTH}" height="1" alt="" border="0">';
			$this->_html['DESC']			= '<img src="'.$this->aENV['path']['pix']['http'].'pf_up.gif" width="9" height="9" alt="" border="0">';
			$this->_html['ASC']		= '<img src="'.$this->aENV['path']['pix']['http'].'pf_down.gif" width="9" height="9" alt="" border="0">';
			$this->_html['dirimage']	= '<img src="'.$this->aENV['path']['pix']['http'].'{ICON}" border="0" width="22" height="18" style="position:relative; top:3px; margin-right:3px;">';
			$this->_html['link']		= '<a href="{HREF}" {JS_HANDLER}>{FILENAME}</a>';
			$this->_nCounter 			= 1;
			// ACTION
			$this->files = $this->readDir();
			$this->directories	= $this->sortDir($this->directories);
			
		}

		function sortDir($aDir) {
			foreach($aDir as $key => $aValue) {
				$aDir[$key]	= is_array($aValue) ? $this->sortDir($aValue):$aValue;
			}
			
			$this->sortdir=='ASC' ? asort($aDir):arsort($aDir);
			return $aDir;
		}
		
/**
 * 
 * Gibt die Files einer Ordnerstruktur zurueck
 * 
 */
		function getFiles() {		
			return $this->files;			
		}
		
/**
 * 
 * Setzt den Space (Width) zwischen Ordnern und Files <= Einrueckung
 * 
 */		
		function setSpace($space) {			
			$this->space = $space;			
		}
		
/**
 * 
 * Setzt die Ebenentiefe, ab der der Baum zugeklappt ist
 * 
 */		
		function setOpenDepth($openDepth) {			
			$this->openDepth = $openDepth;			
		}

/**
 * Hier kann optional ein JS-Handler uebergeben werden, der in jeden Link zu den Dateien eingefuegt wird.
 * @param String $string  Js-Event-Handler Value
 */
		function setJsHandler($string) {
			if (!empty($string)) {
				$this->jsHandler = $string;
			}
		}
	
		function readDir($files='',$ebene=0) {			
			$path = $this->path;
		
			if (is_array($this->extpath)) {			
				foreach ($this->extpath as $key => $val) {				
					$path .= '/'.$val;					
				}				
			}
		
			$handle = opendir($path);

			for ($i=0;$file = readdir($handle);$i++) {
				if (is_dir($path.'/'.$file) && $file != '.' && $file != '..') {					
					$this->extpath[] = $file;
					$apath = explode('/',$path);
					$cpath = count($apath);
					if ($ebene == 0) {					
						$this->directories[$ebene][] = $apath[($cpath-1)];
						$ebene++;	
					}		
					$this->directories[$ebene][$apath[($cpath-1)]][] = $file;
				
					$files = $this->readDir($files,($ebene+1));
					$key = array_search($file,$this->extpath);
					unset($this->extpath[$key]);
					
				}
				if (is_file($path.'/'.$file) && $file != '.' && $file != '..') {					
					$apath = explode('/',$path);
					$cpath = count($apath);
					$files[$apath[($cpath-1)]][] = $file;
					if(!in_array($apath[($cpath-1)],$this->filedir)) {
						$this->filedir[] = $apath[($cpath-1)];
						if($ebene==0) {
						$this->directories[$ebene][0] = $apath[($cpath-1)];	
						}
					}
				}
			}
			closedir($handle);

			return $files;
		}
		
		function showFolder($folder='',$ebene=0) {
				
			if ($folder == '') {
				$this->textbuffer = '<div>'."\n";
				$this->makeSpace($ebene);
				if(!empty($this->url)) $this->textbuffer .= '<a href="'.$this->url.'">';
				$this->textbuffer .= str_replace('{ICON}', 'icn_folder.gif', $this->_html['dirimage']);
				$this->textbuffer .= $_SERVER["HTTP_HOST"].'/'.$this->directories[0][0];
				if(!empty($this->url)) $this->textbuffer .= "</a>";
				if(!empty($this->url)) { $this->textbuffer .= $this->_html[$this->sortdir]; }
				$this->textbuffer .= "<br>\n";
								
				if(in_array($this->directories[0][0],$this->filedir)) {					
					$this->showFile($this->directories[0][0],($ebene+1));					
				}
				
				$this->textbuffer .= '</div>'."\n";
				
				$ebene += 1;
				$folder = $this->directories[0][0];
			}
			
			if (is_array($this->directories[$ebene][$folder])) {			
				foreach ($this->directories[$ebene][$folder] as $key => $sfolder) {				
					$sDisplay = ($ebene > $this->openDepth) ? 'none' : 'block';
					$this->makeSpace($ebene);
					$this->textbuffer .= '<a href="javascript:sublayerSwitch(\'nLayer_'.($this->_nCounter+$ebene).'\')" style="text-decoration:none">'.str_replace('{ICON}', 'icn_folder.gif', $this->_html['dirimage']).'</a>';
					$this->textbuffer .= '<a href="javascript:sublayerSwitch(\'nLayer_'.($this->_nCounter+$ebene).'\')" style="text-decoration:none">'.$sfolder."</a><br>\n";
					
					$sDisplay = ($ebene > $this->openDepth) ? 'none' : 'block';
					$this->textbuffer .= '<div id="nLayer_'.($this->_nCounter+$ebene).'" style="display:'.$sDisplay.'">'."\n\t";
					
					if (is_array($this->directories[($ebene+1)][$sfolder])) {
						$this->showFolder($sfolder,($ebene+1));					
					}
										
					if (in_array($sfolder,$this->filedir)) {						
						$this->showFile($sfolder,($ebene+1));						
					}
					
					$this->textbuffer .= "</div>\n";
					$this->_nCounter++;
				}
			}
		}
		
/**
 * Diese Methode baut einen spacer fuer die einrueckung der sub-folder.
 * @param integer $ebene
 * @return HTML-Tag
 */
		function makeSpace($ebene) {
			// errechne spacer-breite (einrueckung)
			$x = $this->space * $ebene;
			// html
			$this->textbuffer .= str_replace('{WIDTH}', $x, $this->_html['spacer']);
		}
		
/**
 * Diese Methode baut eine komplette Zeile fuer ein file zusammen.
 * @param String $folder
 * @param integer $ebene
 * @return HTML-Tag
 */
		function showFile($folder,$ebene) {
			
			if(is_array($this->files[$folder])) {
				// errechne spacer-breite (einrueckung)
				$x = $this->space * $ebene;

				// wurde so nich gewünscht, also auskommentiert
				//$this->sortdir=='ASC' ? asort($this->files[$folder], SORT_STRING):arsort($this->files[$folder], SORT_STRING);

				// build HTML
				foreach($this->files[$folder] as $key => $filename) {
					$this->makeSpace($ebene);
					$this->textbuffer .= $this->getLink($filename,$folder,$ebene)."<br>\n\t";
				}
			}
		}
		
/**
 * Diese Methode schreibt die HTML Links fuer die Dateien.
 * @todo onClick Methode einbauen, damit diese Methode aequivalent zu dem Linkmanager funktioniert.
 * @param String $file
 * @param String $folder
 * @param integer $ebene
 * @return HTML Link zu der Datei
 */
		function getLink($file,$folder,$ebene) {
			// href
			$href = $this->getStringPath($folder,$ebene).$file;
			// filename (inkl. Icon)
			$filename = $this->oMediadb->getMediaIcon($file,'','', 'style="position:relative; top:3px; margin-right:3px;"').$file;
			// HTML
			return str_replace(
				array('{HREF}', '{JS_HANDLER}', '{FILENAME}'), 
				array($href, $this->jsHandler, $filename), 
				$this->_html['link']);
		}
		
		
		function getStringPath($folder,$ebene) {			
			$aOriPath = explode('/',$this->path);
			$iOriCoun = count($aOriPath)-1;
			$aPath = $this->getPath($folder,($ebene-1),null);
			$ico = count($aPath)-1;
			$sPath = $this->path;
			for ($i=$ico;!empty($aPath[$i]);$i--) {
				if( $i != $ico) {
					$sPath .= '/'.$aPath[$i];
				}
			}
			// ggf. slash dranhaengen
			if (substr($sPath, -1, 1) != '/') $sPath .= '/';
			$sPath = str_replace($_SERVER['DOCUMENT_ROOT'],'',$sPath);
			return $sPath;
			
		}
		
		function getPath($folder='',$ebene=0,$path) {		
			$path[] = $folder;
			
			for ($i=$ebene;is_array($this->directories[$i]);$i--) {			
				$dirs = $this->directories[$i];
				foreach ($dirs as $pdir => $subs) {					
					if (is_array($subs)) {
						for ($j=0;!empty($subs[$j]);$j++) {							
							if ($subs[$j] == $folder) {								
								$path = $this->getPath($pdir,($i-1),$path);
							}
						}
					}
				}				
			}
			
			return $path;
		}
		
		function showFileBrowser($sSort	= "ASC") {
			$this->showFolder();
			return $this->textbuffer;
		}		
	}
?>