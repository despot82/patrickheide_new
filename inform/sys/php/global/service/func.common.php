<?php
/**
* globale Funktionsbibliothek fuer WEB- oder auch ADMIN-Seiten
*
* @access	public
* @package  Content
* @author   Andy Fehn <af@design-aspekt.com>
* @version  6.11  / 2006-04-06 [BUGFIX: BR-Tag gegen Leerzeichen ersetzen in "shorten_text()"]
*/

/*	HISTORY 
	6.11  / 2006-04-06 [BUGFIX: BR-Tag gegen Leerzeichen ersetzen in "shorten_text()"]
	6.1  / 2006-04-05 [NEU: "format_mobile_phone()" hinzugefügt]
	6.04 / 2005-07-12 [BUGFIX: 2. Verbesserte Ersetzungen in "nl2br_cms()"]
	6.03 / 2005-06-14 [BUGFIX: Verbesserte Ersetzungen in "nl2br_cms()"]
	6.02 / 2005-02-02 [BUGFIX: ' maskieren in "jsAddSlashes()"]
	6.01 / 2005-31-01 [NEU: sLang in "format_hours()"]
	6.0 / 2005-01-11 [MEDIA-Funktionen in "func.mediadb.php" ausgelagert - NICHT MEHR ABWAERTSKOMPATIBEL!]
	5.31 / 2004-11-05 [BUGFIX: "is_null" in "format_money()" hinzugefuegt]
	5.3 / 2004-10-26 [NEU: "format_hours()" hinzugefuegt]
	5.22 / 2004-10-25 [BUGFIX: "px" bei "get_*_size()" hinzugefuegt]
	5.21 / 2004-10-14 [BUGFIX: preg_match fuer "cover_email()" in "nl2br_cms()"]
	5.2 / 2004-08-20 [NEU: function "format_money()"]
	5.1 / 2004-08-12 [NEU: "shorten_words()"]
	5.01 / 2004-06-25 [kleine aenderung an int. vorwahl-regel in  "format_phone()"]
	5.0 / 2004-05-03 [alle "write_*" entfernt und "$CMS_" umbenannt -> NICHT MEHR ABWAERTSKOMPATIBEL!!!)
	4.7 / 2004-05-03 ["shorten_text()" aus "func.admin_view.php" hierher verschoben]
	4.6 / 2004-04-19 [NEU: Integration der Funktion "cover_email()" in die Funktion "nl2br_cms()"]
	4.53 / 2004-04-02 [BUGFIX in weiterentwicklung in get_image_tag()...]
	4.52 / 2004-03-30 [weiterentwicklung in get_image_tag(): bei slideshow erstes bild in img-tag schreiben]
	4.51 / 2004-03-18 [BUGFIX: format_phone()]
	4.4 / 2004-02-19 [NEU: format_phone()]
	4.32 / 2003-11-03 [BUGFIX: write_image_tag() onepix als default bei slideshow eingebaut]
	4.31 / 2003-10-24 [NEU: cover_email($sStr) in die anderen link/email/uri funktionen eingebaut]
	4.3 / 2003-10-23 [NEU: debug() + cover_email($sStr)]
	4.24 / 2003-10-10 [weiterentwicklung in link_uri() + show_uri() (-> mailto wird jetzt auch verarbeitet)]
	4.23 / 2003-09-22 [bugfix in write_flash_tag() (-> "preg_match" statt "strstr" bei "?")]
	4.22 / 2003-08-18 [NEU in write_flash_tag() (-> wmode="transparent")]
	4.21 / 2003-07-22 [bugfix in nl2br_cms() (-> return statt echo)]
	4.2 / 2003-07-21 [NEU: make_clickable()]
	4.12 / 2003-06-17 [bugfix und weiterentwicklung in nl2br_cms()]
	4.11 / 2003-06-16 [bugfix in write_image_tag()]
	4.1 / 2003-06-10 af:
	- write_image_tag && write_flash_tag verbessert (beides geht jetzt sowohl mit echten filenames als auch mit IDs!),
	- write_img_tag rausgeschmissen.
	- nl2br_cms && link_uri && show_uri neu.

	----------------------------------------------------------------------------
		Funktionen dieser Funktionsbibliothek:
		----------------------------------------------------------------------------
		function js_history_back()
		function js_location_replace($uri)
		function js_alert($string)
		
		function get_input_size($nValue)
		function get_textarea_size($nValue)
		
		function shorten_text($sText, $nLength=80)
		function shorten_words($sText, $nWords=8)
		function nl2br_cms($text)
		
		function format_phone($sPhone, $sCountryPrefix=NULL)
		function format_mobilephone($sPhonenumber, $sCountryPrefix)
		function format_money($nMoney, $sLang='de')
		function format_hours($nHours)
		
		function link_uri($uri)
		function show_uri($showuri,$max=0)
		function make_clickable($text)
		function cover_email($sStr)
		
		function debug()
		----------------------------------------------------------------------------
*/

#-----------------------------------------------------------------------------
# // JavaScript FUNCTIONS:				[WEB | ADMIN]
#-----------------------------------------------------------------------------
// vereinfachen das schreiben der javascript-funktionen mit allen tags und umlaute-umwandlung etc...

/**
* Gibt den kompletten JavaScript-Block mit der JS-Funktion "history.back()" zurueck.
*
* @access   public
* @return	html-code
*/
	function js_history_back() {
		return '<script language="JavaScript" type="text/javascript">history.back()</script>';
	}

/**
* Gibt den kompletten JavaScript-Block mit der JS-Funktion "location.replace('uri')" zurueck.
*
* @access   public
* @param	string	$uri	URI wohin der Browser geschickt werden soll
* @return	html-code
*/
	function js_location_replace($uri) {
		return '<script language="JavaScript" type="text/javascript">location.replace(\''.$uri.'\')</script>';
	}

/**
* Gibt den kompletten JavaScript-Block mit der JS-Funktion "alert('string')" zurueck. 
* (inkl. MAC-sichere umlaute-umwandlung!)
*
* @access   public
* @param	string	$string	Meldung, die alerted werden soll
* @return	html-code
*/
	function js_alert($string) {
		$string = jsAddSlashes($string);
		return '<script language="JavaScript" type="text/javascript">escape(\''.$string.'\'); alert(unescape(\''.$string.'\'))</script>';
	}

/**
* HILFSFUNKTION: maskiert "JS-sicher" Zeilenumbrueche und schwierige Sonderzeichen
*
* @access   public
* @param	string	$string		String der maskiert werden soll
* @return	string	$string		maskierter String
*/
	function jsAddSlashes($string) {
		$pattern = array(
			"\\\\"     , "\n"    , "\r"     , "\""      ,
			"'"        , "\'"    , "&"      , "<"       , ">"
		);
		$replace = array(
			"\\\\\\\\", "\\n"    , "\\r"    , "\\\""    ,
			"\'"      , "\\'"    , "\\x26"  , "\\x3C"   , "\\x3E"
		);
		return str_replace($pattern, $replace, $string);
	}

# --------------------------------------------------------------------------------------------------
# // FUNKTIONEN, die bei Formularen helfen		[WEB]
# --------------------------------------------------------------------------------------------------

/**
* Gibt NUR das size-attribut fuer <input type="text">-felder (z.B. 'size="44"') zurueck
*
* Beispiel:
* <pre><code>
* <input type="text" <?php echo get_input_size(250); // params: $nValue ? > name="testext">
* </code></pre>
*
* @access   public
* @global	object	$oBrowser
* @param	string	$nValue		(angabe in px)
* @return	html-code			(z.B. 'size="44"')
*/
	function get_input_size($nValue) {
		
		global $oBrowser;
		
		if ($oBrowser->isNN4()) {
			($oBrowser->isMac()) ? $factor = 150 : $factor = 125; // n4 auf MAC OS9 macht die felder noch kleiner
			$new = substr($nValue, 0, -1) / 100 * $factor;
			$buffer = 'size="'.round($new).'"';
		} else {
			$buffer = 'style="width:'.$nValue.'px"';
		}
		
		return $buffer;
	}


/**
* Gibt NUR das cols-attribut fuer < textarea>-felder (z.B. 'cols="44"') zurueck
*
* Beispiel:
* <pre><code>
* <textarea <?php echo get_textarea_size(250); // params: $nValue ? > name="testextarea"></textarea>
* </code></pre>
*
* @access   public
* @global	object	$oBrowser
* @param	string	$nValue		(angabe in px)
* @return	html-code			(z.B. 'size="44"')
*/
	function get_textarea_size($nValue) {
		
		global $oBrowser;
		
		if ($oBrowser->isNN4()) {
			($oBrowser->isMac()) ? $factor = 150 : $factor = 120; // n4 auf MAC OS9 macht die felder noch kleiner
			$new = substr($nValue, 0, -1) / 100 * $factor;
			$buffer = 'cols="'.round($new).'"';
		} else {
			$buffer = 'style="width:'.$nValue.'px"';
		}
		
		return $buffer;
	}


#-------------------------------------------------------------------------------
# // sonstige FUNKTIONEN [WEB]
#-------------------------------------------------------------------------------

/**
* Gibt ggf. einen Teil eines Textes zurueck. Abgeschnitten wird nach der uebergebenen Anzahl ZEICHEN (-3!) und "..." wird angehaengt.
*
* Beispiel:
* <pre><code>
* $aData['title_de'] = shorten_text($aData['title_de']); // params: $sText[,$nLength=80]
* </code></pre>
*
* @access	public
* @param	string	$sText			Langtext
* @param	string	[$nLength]		Anzahl Zeichen, nachdenen abgeschnitten wird	(optional -> default:'80')
* @return 	string	$sText			gekuerzter Text
*/
//	function shorten_text($sText, $nLength=80) {
//		// code
//		$sText = str_replace(array('<BR>', '<br>', '<br />'), ' ', $sText);
//		$sText = strip_tags(trim($sText));
//		if (strlen($sText) > $nLength) {
//			$sText = substr($sText, 0, ($nLength-3))."...";
//		}
//		// output
//		return $sText;
//	}

/**
* Unicode aware replacement for strlen(). Returns the number
* of characters in the string (not the number of bytes), replacing
* multibyte characters with a single byte equivalent
* utf8_decode() converts characters that are not in ISO-8859-1
* to '?', which, for the purpose of counting, is alright - It's
* much faster than iconv_strlen
* Note: this function does not count bad UTF-8 bytes in the string
* - these are simply ignored
* @author <chernyshevsky at hotmail dot com>
* @link   http://www.php.net/manual/en/function.strlen.php
* @link   http://www.php.net/manual/en/function.utf8-decode.php
* @param string UTF-8 string
* @return int number of UTF-8 characters in string
* @package utf8
* @subpackage strings
*/
	function utf8_strlen($str){
	    return strlen(utf8_decode($str));
	}
	
/**
* UTF-8 aware alternative to str_split
* Convert a string to an array
* Note: requires utf8_strlen to be loaded
* @param string UTF-8 encoded
* @param int number to characters to split string by
* @return string characters in string reverses
* @see http://www.php.net/str_split
* @see utf8_strlen
* @package utf8
* @subpackage strings
*/
	function utf8_str_split($str, $split_len = 1) {
	    
	    if ( !preg_match('/^[0-9]+$/',$split_len) || $split_len < 1 ) {
	        return FALSE;
	    }
	    
	    $len = utf8_strlen($str);
	    if ( $len <= $split_len ) {
	        return array($str);
	    }
	    
	    preg_match_all('/.{'.$split_len.'}|[^\x00]{1,'.$split_len.'}$/us', $str, $ar);
	    return $ar[0];
	    
	}	

/**
* Gibt ggf. einen Teil eines Textes zurueck. Bytesicher und damit UTF8 sicher 
* Abgeschnitten wird nach der uebergebenen Anzahl ZEICHEN (-3!) und "..." wird angehaengt.
*
* Beispiel:
* <pre><code>
* $aData['title_de'] = shorten_text_utf8($aData['title_de']); // params: $sText[,$nLength=80]
* </code></pre>
*
* @access	public
* @param	string	$sText			Langtext
* @param	string	[$nLength]		Anzahl Zeichen, nachdenen abgeschnitten wird	(optional -> default:'80')
* @return 	string	$sText			gekuerzter Text
*/	
	function shorten_text($sText, $nLength=80) {
		// code
		$sText	= str_replace(array('<BR>', '<br>', '<br />'), ' ', $sText);
		$sText	= strip_tags(trim($sText));

		if (utf8_strlen($sText) > $nLength) {
			$aText	= utf8_str_split($sText, $nLength-3);
				
			$sText = $aText[0]."...";
		}
		// output
		return $sText;
	}

/**
* Gibt ggf. einen Teil eines Textes zurueck. Abgeschnitten wird nach der uebergebenen Anzahl WOERTER und "..." wird angehaengt.
*
* Beispiel:
* <pre><code>
* $aData['title_de'] = shorten_words($aData['title_de']); // params: $sText[,$nWords=8]
* </code></pre>
*
* @access	public
* @param	string	$sText			Langtext
* @param	string	[$nWords]		Anzahl Woerter, nach denen abgeschnitten wird	(optional -> default:'8')
* @return 	string	$sText			gekuerzter Text
*/
	function shorten_words($sText, $nWords=8) {
		// code
		$sText = trim($sText);
		$aText = explode(' ', $sText, $nWords + 1);	// sText in array zerlegen (1 elem. mehr als gewuenscht!)
		if (count($aText) > $nWords) {				// wenn aText mehr woerter enthaelt als nWords:
			array_pop($aText);						// letztes array-element entfernen
			$sText = implode(' ', $aText) . ' ...';	// $nWords array-elemente wieder zusammensetzen
		}
		// output
		return $sText;
	}

/**
* wie die PHP-Funktion nl2br() - plus ein optimiertes "line-break"-feature speziell fuer dieses CMS:
*
* Bei der Ausgabe von Texten, welche mit dem txteditor formatiert sein koennten, 
* wird hinter bestimmten Tags [< /li>|< /tr>|< /td>] KEIN "< br />" erzeugt. 
* Ausserdem wird hinter bestimmten Tags [< /ul>|< /ol>|< /h1-6>|< /hr>] EIN "< br />" rausgeloescht. 
* Ausserdem werden evtl. vorkommende email-links mit der Funktion "cover_email()" behandelt. 
*
* Beispiel:
* <pre><code>
* echo nl2br_cms($aData['copytext']); // params: $sStr
* </code></pre>
*
* @access   public
* @param	string	$sStr	(zu formatierender Text)
* @return	string
*/
	function nl2br_cms($sStr) {
	// vars
		if (!$sStr) return;
	// code
		// line breaks
		$sStr = preg_replace('/\r\n|\r|\n/', '<br />', $sStr);
		$sStr = str_replace('<BR>', '<br />', $sStr); // Flash-Texteditor br's ersetzen
		# (-> Flash-Texteditor behandelt bereits die Listen und gibt die Tags in Versalien aus)
		// einzel-tags (einzelne br's entfernen)
		$sStr = preg_replace('/<hr([^>]*)><br \/>/i', '<hr\1>', $sStr);;
		// oeffnende tags (mehrere br's entfernen)
		$sStr = preg_replace('/<table([^>]*)><br \/>/i', '<table\1>', $sStr); // beruecksichtige attribute
		$sStr = preg_replace('/<tr([^>]*)><br \/>/i', '<tr\1>', $sStr);
		$sStr = preg_replace('/<th([^>]*)><br \/>/i', '<th\1>', $sStr);
		$sStr = preg_replace('/<td([^>]*)><br \/>/i', '<td\1>', $sStr);
		// schliessende tags (mehrere br's entfernen)
		$sStr = preg_replace('/(<\/t[rhd]+>)(<br \/>)*/i', '\1', $sStr);
		// schliessende tags (einzelne br's entfernen)
		$sStr = preg_replace('/(<\/table+>)<br \/>/i', '\1', $sStr);
		$sStr = preg_replace('/<\/h([123456])([^>]*)><br \/>/', '</h\1\2>', $sStr);
		
		// Sonderfall Listen -> fuer Texte, die NICHT aus dem Flash-Texteditor kommen
		// oeffnende tags (mehrere br's entfernen)
		$sStr = preg_replace('/(<[ul|ol]+>)(<br \/>)*/', '\1', $sStr); // KEINE attribute + NICHT case-sensitive!
		// schliessende tags (mehrere br's entfernen)
		$sStr = preg_replace('/(<\/li>)(<br \/>)*/', '\1', $sStr); // NICHT case-sensitive!
		// schliessende tags (einzelne br's entfernen)
		$sStr = preg_replace('/(<\/[ul|ol]+>)<br \/>/', '\1', $sStr); // NICHT case-sensitive!
		
		// cover email...
		$sStr = str_replace("mailto:", "&#109;&#97;&#105;&#108;&#116;&#111;&#58;", $sStr); // "mailto:" verstecken
		$emailPattern = "/(([a-z0-9_]|\\-|\\.)+@([^[:space:]|\"|\'|\<|\?]*))/";
		while (preg_match($emailPattern, $sStr, $emailStr)) { // ALLE mailto-link-vorkommen verstecken
			$sStr = str_replace($emailStr[0], cover_email($emailStr[0]), $sStr);
		}
	// output
		return $sStr;
	}

/**
* Diese Funktion stellt einer Telefonnummer ggf. eine Laendervorwahl vor 
* - falls an der ersten Stelle der Telefonnummer "0" oder "(" UND NICHT "+" oder "00" steht. 
* Wenn keine Laendervorwahl uebergeben wird oder die Telefonnummer nicht mit einer 
* 0 bzw. einer "(" beginnt, wird die Telefonnummer unveraendert zurueckgegeben. 
*
* Beispiel:
* <pre><code>
* echo format_phone('089 / 12345', '49'); // sPhone: $sStr[,$sCountryPrefix=NULL]
* </code></pre>
*
* @access   public
* @param	string	$sPhone			(zu formatierende Telefonnummer)
* @param	string	$sCountryPrefix	(Laender-Vorwahl)
* @return	string
*/
	function format_phone($sPhone, $sCountryPrefix=NULL) {
	// vars
		if (!$sPhone) return;
		$sPhone = trim($sPhone);
	// code
		// countryPrefix NICHT, wenn nummer mit internat. vorwahl beginnt!
		if ($sCountryPrefix 
			&& substr($sPhone, 0, 3) != '(00' 
			&& substr($sPhone, 0, 2) != '00' 
			&& substr($sPhone, 0, 2) != '(+' 
			&& substr($sPhone, 0, 1) != '+') {
			// ggf. + vor $sCountryPrefix stellen
			$sStr = (substr($sCountryPrefix, 0, 1) != '+') ? '+'.$sCountryPrefix : $sCountryPrefix;
			// spacer + Rufnummer
			$sStr .= ' '.$sPhone;
		} else {
			$sStr = $sPhone;
		}
	// output
		return $sStr;
	}

/**
* Diese Funktion stellt einer Telefonnummer ggf. eine Laendervorwahl vor 
* - falls an der ersten Stelle der Telefonnummer "0" oder "(" UND NICHT "+" oder "00" steht. 
* Sämtlich nicht numerische Zeichen werden entfernt, damit die Nummer über einen
* Link in Mobile Devices direkt aungerufen werden kann 
*
* Beispiel:
* <pre><code>
* echo format_phone('089 / 12345', '49'); // sPhone: $sStr[,$sCountryPrefix=NULL]
* </code></pre>
*
* @access   public
* @param	string	$sPhonenumber	(zu formatierende Telefonnummer)
* @param	string	$sCountryPrefix	(Laender-Vorwahl)
* @return	string
*/
function format_mobilephone($sPhonenumber, $sCountryPrefix=null) {
	$sPhonenumber 	= trim($sPhonenumber);
	if ($sCountryPrefix) {
		$sCountryPrefix = (substr($sCountryPrefix, 0, 1) == "+") ? $sCountryPrefix : "+".$sCountryPrefix;
		$sPrefix 		= (substr($sPhonenumber, 0, 1) == "+") ? "+" : $sCountryPrefix;
	}
	// nicht numerische zeichen veschwinden lassen
	$aReplace = array();
	for ($i=0; $i<strlen($sPhonenumber); $i++) {
		$chr = substr($sPhonenumber, $i, 1);
		if (intval($chr) == 0 && $chr != "0") { $aReplace[] = $chr; }
	}
	$sPhonenumber = str_replace($aReplace, "", $sPhonenumber);
	
	// countryPrefix NICHT, wenn nummer mit internat. vorwahl beginnt!
	if ($sCountryPrefix && substr($sPhonenumber, 0, 2) != '00') {
		if (substr($sPhonenumber, 0, 1) == '0') {	// 0 aus nationaler Vorwahl entfernen
			$sPhonenumber = substr($sPhonenumber, 1);
		}
		// ggf. + vor $sCountryPrefix stellen
		$sPhonenumber = $sPrefix.$sPhonenumber;
	}
	
	return $sPhonenumber;
}

/**
* Diese Ausgabe-Funktion formatiert den uebergebenen Geldbetrag (optional nach uebergebener Sprache - default: deutsch [1.000,00]). 
*
* Beispiel:
* <pre><code>
* echo format_money('12345.99', $syslang); // params: $nMoney[,$sLang='de']
* </code></pre>
*
* @access   public
* @param	string	$nMoney		(zu formatierender Betrag)
* @param	string	$sLang		(Sprache)
* @return	string
*/
	function format_money($nMoney, $sLang='de') {
	// vars
		if (is_null($nMoney)) return;
		$nMoney = trim($nMoney);
	// code
		if ($sLang == "de") {	// D
			$nMoney = number_format($nMoney, 2, ',', '.');
		} else {				// E
			$nMoney = number_format($nMoney, 2);
		}
	// output
		return $nMoney;
	}

/**
* Diese Ausgabe-Funktion formatiert die uebergebene Anzahl Stunden auf eine Stelle hinterm Komma. 
* Ein Punkt wird ggf. gegen Komma getauscht und eine 0 hinterm Komma wird nicht angezeigt
*
* Beispiel:
* <pre><code>
* echo format_hours('2.5'); // params: $nHours
* </code></pre>
*
* @access   public
* @param	string	$nHours		(zu formatierende Anzahl Stunden)
* @param	string	$sLang		(Sprache)
* @return	string
*/
	function format_hours($nHours, $sLang='de') {
	// vars
		if (empty($nHours)) return;
		$nHours = trim($nHours);
	// code
		if ($sLang == "de") {	// D
			$nHours = number_format($nHours, 1, ',', '.'); // auf eine Stelle hinter dem Komma runden + Komma statt Punkt als Dezimaltrenner
			$nHours = str_replace(',0', '', $nHours); // ",0" nicht anzeigen
		} else {				// E
			$nHours = round($nHours, 1); // auf eine Stelle hinter dem Komma runden
			$nHours = str_replace('.0', '', $nHours); // ".0" nicht anzeigen
		}
	// output
		return $nHours;
	}


// ------------------------------------------------------------------------------------ LINKS

/**
* Prueft den uebergeben string und fuegt ggf. die Zeichenkette "http://" oder "mailto:" hinzu.
*
* Ist also sehr hilfreich zum Sicherstellen, dass in einem <a href> ein funktionierender Link ist.
*
* @access   public
* @param	string	$uri	(zu formatierender Link)
* @return	string	$uri	(formatierter Link)
*/
	function link_uri($uri) {
		// vars
		if (!$uri) return;
		
		// code // fuer <a href> ggf. 'http://' voranstellen
		if (!strstr($uri, "http://") 
		&& strstr($uri, "www") 
		&& !strstr($uri, "@")) {
			$uri	= "http://".$uri; 
		} // end if
		elseif (!strstr($uri, "mailto:") 
		&& strstr($uri, "@")) { 
			$uri	= "&#109;&#97;&#105;&#108;&#116;&#111;&#58;".cover_email($uri); 
		} // end elseif
		else { 
			$uri	= $uri; 
		} // end else
		
		// output
		return $uri;
	}

/**
* Prueft den uebergeben string auf die Zeichenkette "http://" und loescht sie ggf. raus.
*
* Ausserdem kann man mittels optionalem Parameter $max" eine Ausgabebegrenzung auf $max Zeichen erzwingen. 
* Ist also sehr hilfreich zum Anzeigen von "schoenen" Links.
*
* @access   public
* @param	string	$showuri	(zu formatierender Link)
* @param	int		$max		(Optional: Ausgabebegrenzung auf $max Zeichen)
* @return	string	$showuri	(formatierter Link)
*/
	function show_uri($showuri, $max=0) {
	// vars
		if (!$showuri) return;
		$max = isset($max) ? $max + 0 : 0; // Fallback: Konvertierung auf Integer durch Addition von 0
		// code
	
		// fuer Usersicht des Links ggf. 'http://' entfernen
		$showuri = substr(str_replace("http://www", "www", $showuri), 0);

		// und ggf. 'mailto:' entfernen
		$showuri = substr(str_replace("mailto:", '', $showuri), 0);
		
		// und ggf. email-adresse in unicode verstecken
		if (strstr($showuri, "@")) { $showuri = cover_email($showuri); }
		if ($max > 0) {
		// Ausgabebegrenzung auf $max Zeichen
			if (strlen($showuri) > $max) { $showuri = substr($showuri, 0, $max)."..."; }
		}

		// return output
		return $showuri;
	}

/**
* Durchsucht den uebergeben Text (string) auf Links (erwartet "http://" bei Links!) und E-Mail Adressen und baut Anker-TAGs drumrum.
*
* Ist also sehr hilfreich zum einfachen "klickbar-machen" von groesseren textbloecken.
*
* @access   public
* @param	string	$text	(zu formatierender Text)
* @return	string	$text	(formatierter Text)
*/
	function make_clickable($sText, $sTarget="_blank") {
		#TEST: $pattern = '#(^|[^\"=]{1})(http://|ftp://|mailto:|news:)([^\s<>]+)([\s\n<>]|$)#sm';
		#TEST: return preg_replace($pattern,"\\1<a href=\"\\2\\3\"><u>\\2\\3</u></a>\\4",$str);
		$sText = preg_replace( '-([[:alnum:]]+)://([^[:space:]]*)([[:alnum:]#?/&=])-',  "<a href=\"\\1://\\2\\3\" target=\"$sTarget\">\\1://\\2\\3</a>", $sText);
		$sText = preg_replace( "/(([a-z0-9_]|\\-|\\.)+@([^[:space:]]*)([[:alnum:]-]))/",  "<a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;\\1\">\\1</a>", $sText);
		#OLD: $sText = preg_replace( "/(([a-z0-9_]|\\-|\\.)+@([^[:space:]]*)([[:alnum:]-]))/",  "<a href=\"mailto:\\1\">\\1</a>", $sText);
		return $sText;
	}

/**
* Ersetzt in der uebergebenen E-Mail Adresse alle Zeichen gegen ihre UniCode-Gegenstuecke.
*
* Verhindert so (angeblich) das unerwuenschte Auslesen der E-Mail Adressen von Spammern.
*
* @access   public
* @param	string	$sStr	(zu ersetzende E-Mail Adresse)
* @return	string	$sStr	(formatierte E-Mail Adresse)
*/
	function cover_email($sStr) {
		$aTranslate	= array("-"	=> "&#45;",	"."	=> "&#46;",	"@"	=> "&#64;",	"_"	=> "&#95;", 
							"0"	=> "&#48;",	"1"	=> "&#49;",	"2"	=> "&#50;",
							"3"	=> "&#51;",	"4"	=> "&#52;",	"5"	=> "&#53;",	
							"6"	=> "&#54;",	"7"	=> "&#55;",	"8"	=> "&#56;",	"9"	=> "&#57;", 
							"A"	=> "&#65;", "B"	=> "&#66;", "C"=>"&#67;", "D"	=> "&#68;", "E"	=> "&#69;", "F"	=> "&#70;", "G"	=> "&#71;", 
							"H"	=> "&#72;", "I"	=> "&#73;", "J" =>"&#74;", "K"	=> "&#75;", "L"	=> "&#76;", "M"	=> "&#77;", "N"	=> "&#78;", 
							"O"	=> "&#79;", "P"	=> "&#80;", "Q"	=> "&#81;", "R"	=> "&#82;", "S"	=> "&#83;", "T"	=> "&#84;", "U"	=> "&#85;", 
							"V"	=> "&#86;", "W"	=> "&#87;", "X"	=> "&#88;", "Y"	=> "&#89;", "Z"	=> "&#90;", 
							"a"	=> "&#97;", "b"	=> "&#98;", "c"	=> "&#99;", "d"	=> "&#100;", "e"	=> "&#101;", "f"	=> "&#102;", "g"	=> "&#103;", 
							"h"	=> "&#104;", "i"	=> "&#105;", "j"	=> "&#106;", "k"	=> "&#107;", "l"	=> "&#108;", "m"	=> "&#109;", "n"	=> "&#110;", 
							"o"	=> "&#111;", "p"	=> "&#112;", "q"	=> "&#113;", "r"	=> "&#114;", "s"	=> "&#115;", "t"	=> "&#116;", "u"	=> "&#117;", 
							"v"	=> "&#118;", "w"	=> "&#119;", "x"	=> "&#120;", "y"	=> "&#121;", "z"	=> "&#122;" );
							
		// return output							
		return strtr($sStr, $aTranslate);
	}


// ------------------------------------------------------------------------------------ DEBUG

/**
* Zeigt den Inhalt der Super-Variablen $GLOBALS oder die eubergebe Variable an. 
* In $GLOBALS sind die aktuellen Variablen des ablaufenden Scriptes ebenso gespeichert wie die 
* Umgebungsvariablen des Webservers sowie alle per $_REQUEST uebergebenen Werte. 
* Wird "debug()" innerhalb einer Funtion aufgerufen, gibt es auch die dort verwendeten 
* lokalen Variablen aus! :)
*
* @access   public
* @param	mixed	String oder Array
* @return	html
*/
	function debug($var='') {
		if($var == '') $var = $GLOBALS;
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
	
/**
* returns the searchtitle string for the global search table
*
* @access   public
* @global	array config array aENV
* @global	string content language
* 
* @param	string template key for array aENV searchpattern
* @param	array  data array with replacing values
* 
* @return	string searchtitle
*/	
	function getSearchTitle($sTemplate, $aArray, $sLang = '') {
		global $aENV, $weblang;
		
		// fetch errors
		if(empty($sLang)) { $sLang = $weblang; }
		if(!is_array($aArray))	{ return false; }
		if(empty($aArray))		{ return false; }
		if(empty($sTemplate))	{ return false; }
		if(!isset($aENV['searchpattern'][$sTemplate]['title'])) { return $aArray['title_'.$sLang]; }

		$sTemplateString	= $aENV['searchpattern'][$sTemplate]['title'];
		// replace language for field_contentlanguage
		$sTemplateString	= str_replace('{weblang}', $sLang, $sTemplateString);
		
		// find all occurences of patterns in function calls (like 'function({param})')
		preg_match_all("/\(({(.*?)}).*?\)/", $sTemplateString, $aMatch);
		
		// initialise arrays for pattern & replace
		$aReplace	= array();
		$aPattern	= array();
		
		// fill pattern & replace arrays with matches from the preg_match & content of the data array	
		foreach($aMatch[1] as $nKey => $null) {
			$aReplace[]	= $aArray[$aMatch[2][$nKey]];
			$aPattern[]	= $aMatch[1][$nKey];
		}
				
		// make the replace		
		$sBuffer	= str_replace($aPattern, $aReplace, $sTemplateString);		
		
		// initialise arrays for pattern & replace		
		$aReplace	= array();
		$aPattern	= array();
		
		// find all occurences of functions with parameters (like '[function(param)]')
		preg_match_all("/(?:\[(.*?)\((.*?)\)\])/", $sBuffer, $aMatch);

//		$sRegex	= "/".paramDelim."/";

		// fill pattern & replace arrays with function calls
		foreach($aMatch[0] as $nKey => $null) {
			$aReplace[]	= (function_exists($aMatch[1][$nKey])) ? call_user_func_array ($aMatch[1][$nKey],  explode(",", $aMatch[2][$nKey])):'';
			$aPattern[]	= $aMatch[0][$nKey];	
		}
		
		// make the replace
		$sBuffer	= str_replace($aPattern, $aReplace, $sBuffer);
		
		// find all occurences of remaining non-function parameters (like '{pattern}')
		preg_match_all("/(?:{(.*?)})/", $sBuffer, $aMatch);
		
		// initialise arrays for pattern & replace
		$aReplace	= array();
		$aPattern	= array();
		
		// fill pattern & replace arrays with matches from preg_match & content of the data array
		foreach($aMatch[0] as $nKey => $null) {
			$aReplace[]	= $aArray[$aMatch[1][$nKey]];
			$aPattern[]	= $aMatch[0][$nKey];
		}

		// make final replace
		$sTitle	= str_replace($aPattern, $aReplace, $sBuffer);
		
		// return searchtitle
		return $sTitle;
	}
	
/**
* returns the searchtext string for the global search table
*
* @access   public
* @param	string template key for array aENV searchpattern
* @param	array  data array with replacing values
* @return	string searchtext
*/
	function getSearchText($sTemplate, $aArray, $sLang	= '') {
		global $aENV, $weblang;
		
		// fetch errors
		if(empty($sLang)) { $sLang = $weblang; }
		if(!is_array($aArray))	{ return false; }
		if(empty($aArray))		{ return false; }
		if(empty($sTemplate))	{ return false; }
		if(!isset($aENV['searchpattern'][$sTemplate]['text'])) { return $aArray['text'.$sLang]; }

		$sTemplateString	= $aENV['searchpattern'][$sTemplate]['text'];
		// replace language for field_contentlanguage
		$sTemplateString	= str_replace('{weblang}', $sLang, $sTemplateString);
		
		// find all occurences of patterns in function calls (like 'function({param})')
		preg_match_all('/\(({(.*?)}).*?\)/', $sTemplateString, $aMatch);
		
		// initialise arrays for pattern & replace
		$aReplace	= array();
		$aPattern	= array();
		
		// fill pattern & replace arrays with matches from the preg_match & content of the data array	
		foreach($aMatch[1] as $nKey => $null) {
			$aReplace[]	= $aArray[$aMatch[2][$nKey]];
			$aPattern[]	= $aMatch[1][$nKey];
		}
				
		// make the replace		
		$sBuffer	= str_replace($aPattern, $aReplace, $sTemplateString);		
		
		// initialise arrays for pattern & replace		
		$aReplace	= array();
		$aPattern	= array();
		
		// find all occurences of functions with parameters (like '[function(param)]')
		preg_match_all("/(?:\[(.*?)\((.*?)\)\])/", $sBuffer, $aMatch);
		
		// fill pattern & replace arrays with function calls
		$sRegex	= "/".paramDelim."/";

		foreach($aMatch[0] as $nKey => $null) {
			$aReplace[]	= (function_exists($aMatch[1][$nKey])) ? call_user_func_array ($aMatch[1][$nKey], explode(",", $aMatch[2][$nKey])):'';
			$aPattern[]	= $aMatch[0][$nKey];	
		}
		
		// make the replace
		$sBuffer	= str_replace($aPattern, $aReplace, $sBuffer);
		
		// find all occurences of remaining non-function parameters (like '{pattern}')
		preg_match_all("/(?:{(.*?)})/", $sBuffer, $aMatch);
		
		// initialise arrays for pattern & replace
		$aReplace	= array();
		$aPattern	= array();
		
		// fill pattern & replace arrays with matches from preg_match & content of the data array
		foreach($aMatch[0] as $nKey => $null) {
			$aReplace[]	= $aArray[$aMatch[1][$nKey]];
			$aPattern[]	= $aMatch[0][$nKey];
		}

		// make final replace
		$sText	= str_replace($aPattern, $aReplace, $sBuffer);
		
		// return searchtitle
		return $sText;	
	}		

?>