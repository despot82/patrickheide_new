<?php
/*
 * Created on 22.09.2005
 *
 */
/**
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * Diese Klasse stellt Methoden zum behandeln von Beans bereit.
 * @version 0.2 - Bugfix in der Reset Methode
 * history 0.1 
 */
 
  class BeanService {
  	
  	var $bean = null;
  	var $beanvalues = null;
  	var $arpointer = null;
  	
  	/**
  	 * Befuellt eine Bean mit Werten, die aus einem Array kommen. 
  	 * @param String=>Array(num => "name") $werte]
  	 * @param BeanObject $bean 
  	 * @return BeanObject $bean
  	 */
  	
  	function setBeanValues($werte,$bean) {
   		
   		if(is_array($werte)) {
   			
   			foreach ($werte as $key => $val) {
   			
   				$setmet = "set".ucfirst($key);
   				
   				$bean->$setmet($val);
   				
   			}
   		}

   		return $bean;
   	}
   	
    /**
     * Diese Methode setzt in dem Attribute beanvalues die Werte aus der Bean in EINEM Element 
     * @param String $get
     * @return null
     * 
     */	
   	
  	function getBeanValue($get) {
  	
  		$gets = "get".ucfirst($get);
  	
  		$this->beanvalues[$get][] = $bean->$gets();
  		
  		$this->arpointer[$get]["pointer"] = -1;
  		
  	}
  	
  	/**
  	 * Diese Methode setzt anhand eines uebergebenem Arrays alle Werte in beanvalues. 
  	 * @param String[] $arget
  	 * @return null
  	 * 
  	 */
  	 
  	function getBeanValues($arget) {
  	
  		$coar = count($arget);
  	
  		for($i=0;$coar > $i;$i++) {

	  		$gets = "get".ucfirst($arget[$i]);
  	
  	 		$this->beanvalues[$arget[$i]] = $this->bean->$gets();
  			$this->arpointer[$arget[$i]]["pointer"] = -1;
  		}
  	
   	}
  	
  	/**
  	 * Ueber diese Methode werden die Daten aus der Datenbean abgefragt.
  	 * @param String $get
  	 * @return String
  	 * 
  	 */
  	
  	function getValueByName($get) {
  	  	
  		return $this->beanvalues[$get][$this->arpointer[$get]["pointer"]];
  		
  	}
  	
  	/**
  	 * Prueft ob weitere Array Elemente vorhanden sind
  	 * @return bool
  	 * 
  	 */
  	
  	function beanHasNextElement() {
  		
  		if($this->bean != null) {
  		
  			foreach($this->arpointer as $key => $val) {
  			
  				$i=$this->arpointer[$key]["pointer"];

  				if ((count($this->beanvalues[$key])-1) > $i) {
  					
  					continue;
  					
  				}
  				else {
  				 
  					return false;
  					
  				}
  				
  			}
  			return true;
  		}
  			
  	}
  	
  	/**
  	 * Bewegt den internen Arrayzeiger um ein Element vor
  	 * 
  	 */
  	
  	function beanNext() {
  	
  		if($this->beanHasNextElement()) {
  		
  			foreach($this->arpointer as $key => $val) {
  			    
  				$this->arpointer[$key]["pointer"]++;
  				
  			}
  			return true;
  		}
  		else {
  		
  			return false;
  			
  		}
  		
  	}
  	
  	function beanResetPointer() {
  	
  		foreach ($this->arpointer as $key => $val) {
  		
  			$this->arpointer[$key]["pointer"] = -1;
  			
  		}
  		
  	}
  	
  }
?>