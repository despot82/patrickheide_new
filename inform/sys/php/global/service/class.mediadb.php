<?php
/**
* globale Funktionsbibliothek fuer WEB- oder ADMIN-Seiten, die bei der Media-Ausgabe helfen!
* NOTE: benoetigt die Klasse "class.db_mysql.php"!
*
* Example: 
* <pre><code> 
* require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
* $oMediadb =& new mediadb(); // params: - 
* // baue aus einem image-filename oder einer MDB-ID einen kompletten IMG-Tag 
* echo $oMediadb->getImageTag($aData['slideshow'], 'border="0"', 'keyvisual2'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)] 
* // baue aus einem filename oder einer MDB-ID einen kompletten Flash-Tag 
* echo $oMediadb->getFlashTag($shockwaveFile, 756, 96, "555555", "low", 6, $sFallback); // params: $sFile,$nWidth,$nHeight[,$sBgColor='000000'][,$sQuality='high'][,$nVersion=6][,$sFallback='']
* // gebe das komplette Array eines MediaDB-Files zurueck
* $aMediaFile = $oMediadb->getMedia($aData['download_id']); // params: $nMediaId
* // gabe den typ (image|flash|pdf...) zurueck
* echo $oMediadb->getMediaType($aData['filename']); // params: $sFilename
* // gebe ein Icon fuer dieses Mediafile zurueck
* echo $oMediadb->getMediaIcon($aData['filename']); // params: $sFilename[,$sAlt=''][,$sPath=''][,$sExtra='']
* // gebe ein Thumbnail/Icon fuer dieses Mediafile zurueck
* echo $oMediadb->getMediaThumb($aData['filename']); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
* </code></pre>
*
* @access	public
* @package	service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.28 / 2006-03-03 [IMG-Tags XHTML-Konform gemacht]
*/

class mediadb {
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse mediadb:
		----------------------------------------------------------------------------
		konstruktor mediadb()
		function getImageTag($imgSrc, $extra='', $layerID='')
		function getFlashTag($sFile, $nWidth='', $nHeight='', $sBgColor='000000', $sQuality='high', $nVersion=6, $sFallback='')
		function getMedia($nMediaId)
		function getMediaType($sFilename)
		function getMediaIcon($sFilename, $sAlt='', $sPath='')
		function getMediaThumb($sFilename, $sExtra='', $sPathKey='mdb_upload')
		function getMediatypesDropdown($sSelectedValue, $sActLang, $sShow='all')
		
		function moduleUpload($sUploadFieldname, $sDescription='', $sKeywords='')
		function moduleDelete($aMdbId)
		----------------------------------------------------------------------------
		HISTORY:
		1.28 / 2006-03-03 [IMG-Tags XHTML-Konform gemacht]
		1.27 / 2005-21-15 [Erweiterung: "getImageIcon()" kann Extra-Parameter (Styles etc.) fuer IMG-Tag annehmen]
		1.26 / 2005-07-15 [BUGFIX: bei File-exists-Ermittlung in "getImageTag()"]
		1.25 / 2005-07-01 [BUGFIX: bei Fallback-Behandlung in "getFlashTag()"]
		1.24 / 2005-05-31 [BUGFIX: bei Filesize-Ermittlung in "getMediaThumb()"]
		1.23 / 2005-05-03 [BUGFIX: bei Upload-Methoden in "moduleUpload()"]
		1.22 / 2005-04-29 [BUGFIX: bei slideshow in "getImageTag()"]
		1.21 / 2005-04-25 [BUGFIX: in "moduleDelete()"]
		1.2 / 2005-04-20 [NEU: "moduleUpload()" + "moduleDelete()"]
		1.11 / 2005-04-12 [BUGFIX: "getMediaThumb()"]
		1.1 / 2005-04-06 [NEU: "getMediaType()", "getMediaIcon()", "getMediaThumb()", "getMediatypesDropdown()" eingebaut]
		1.0 / 2005-04-04 [NEU: aus "func.mediadb.php" entwickelt]
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	array	HOME Environment
*/
	var $aENV;
/**
* @access   private
* @var	 	object	DB-Object
*/
	var $oDb;

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das cug-Objekt und setzt ein paar interne Variablen
*
* Beispiel: 
* <pre><code> 
* $oMediadb =& new mediadb(); // params: - 
* </code></pre>
*
* @access	public
* @global	global	$aENV		HOME Environment
* @return	void
*/
	function mediadb() {
		// import globals
		global $aENV;
		$this->aENV =& $aENV;
		// check table
		if (!isset($aENV['table']['mdb_media']) || empty($aENV['table']['mdb_media'])) die("class.mediadb.php -> ERROR: aENV['table']['mdb_media'] nicht definiert!");
		// erzeuge noch kein DB-object, weil ggf. gar keins benoetigt wird
		$this->oDb = NULL;
	}

/**
* Hilfsfunktion: erzeuge ein DB-object, damit die Methoden auch in Schleifen verwendet werden koennen
*
* @access   private
*/
	function _makeDbConnect() {
		// um unnoetige connects zu vermeiden, erzeuge NUR ein DB-object, wenn noch keins existiert!
		if (!is_object($this->oDb)) {
			$this->oDb =& new dbconnect($this->aENV['db']);
		}
		// check DB-object
		if (!is_object($this->oDb)) die("ERROR: unable to init DB-object!");
	}

#-----------------------------------------------------------------------------

/**
* Schreibt kompletten image-tag bzw. slideshow-tags fuer WEB (fuer ADMIN nicht noetig).
*
* (wenn ein filename als $imgSrc uebergeben wird, aber kein pfad, wird (wie bei uebergabe einer ID) der upload-ordner als ort der datei angenommen). 
* (wenn bei $extra width, height oder alt angegeben wird, ueberschreibt dies die wirklichen bildabmessungen bzw. die description aus der media-db). 
*
* ACHTUNG: Bei einer Slideshow muss im header eingebunden sein: 
* < script src="scripts/slideshow.js" language="JavaScript" type="text/javascript">< /script> 
* ACHTUNG: Wenn der Image-Tag der Slideshow in einem Layer liegt, muss als 3. Parameter der Layername uebergeben werden!
*
* Beispiel:
* <pre><code>
* echo $oMediadb->getImageTag($aData['slideshow'], 'border="0"', 'keyvisual2'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)]
* </code></pre>
*
* @access	public
* @param	string	$imgSrc		ID(s) oder Filename(s) des/der Image(s) (z.B. "1123" bzw. "region56a674f6.jpg" oder "1123,1124,1125" bzw. "region56a674f6.jpg,region5879cfb0.jpg,kontaktb9d8d7cb.jpg")
* @param	string	$extra		extra attribute im img-tag (z.B. "border="0") (optional => default: '')
* @param	string	$layerID	ID (name) des Layers - nur bei slideshow wichtig (z.B. "content") (optional => default: '')
* @return	html-code
*/
	function getImageTag($imgSrc, $extra='', $layerID='') {
		
	// vars
		// imgSrc
		if (empty($imgSrc)) return '';
		// imgSrc: wenn ein pfad mit uebergeben wurde wird er benutzt - wenn nicht, wird der pfad zum upload-ordner genommen
		$path = (!strstr($imgSrc,"/")) ? $this->aENV['path']['mdb_upload'] : array('http'=>'','unix'=>'');
		// extra:
		if ($extra != 'border="0"') {
			// width / height rausfiltern und separat in $width / $height speichern, falls es uebergegeben wird
			if (preg_match("/(width=[\"]?)([0-9]*)([\"]?)/i", $extra, $matches)) {$width = $matches[2]; $extra = str_replace($matches[0],'',$extra);}
			if (preg_match("/(height=[\"]?)([0-9]*)([\"]?)/i", $extra, $matches)) {$height = $matches[2]; $extra = str_replace($matches[0],'',$extra);}
			// extra: alt rausfiltern und separat in $alt speichern, falls es uebergegeben wird
			if (preg_match("/(alt=\")(.*)(\")/i", $extra, $matches)) {$alt = strtok($matches[2],'"'); $extra = str_replace($matches[0],'',$extra);}
		}
		// unterscheidung mediadb oder filename
		$use_mediadb = (preg_match("/\.[gif|jpg|jpeg|png]/i", $imgSrc)) ? false : true;	// true = imgSrc ist/sind mediadb-ID(s)
		
		// unterscheidung image oder slideshow
		if (!strstr($imgSrc, ",")) { // impliziert, dass im filename(/mediadb-id) kein komma erlaubt sein darf!
	// wenn nur ein image
			if ($use_mediadb == true) { // hole image-data von mediadb
				$aImg = $this->getMedia($imgSrc); // params: $nMediaId
				$filename = $aImg['filename'];
				if (!isset($alt))	{ $alt		= $aImg['description']; }
				if (!isset($width))	{ $width	= $aImg['width']; }
				if (!isset($height)){ $height	= $aImg['height']; }
			} else {
				if (!empty($path['unix']) && !file_exists($path['unix'].$imgSrc)) return '';
				$aImgsize = @getimagesize($path['unix'].$imgSrc);
				$filename = $imgSrc;
				if (!isset($alt))	{ $alt		= ''; }
				if (!isset($width))	{ $width	= $aImgsize[0]; }
				if (!isset($height)){ $height	= $aImgsize[1]; }
			}
		// code
			// write image tag
			$sStr = '<img src="'.$path['http'].$filename.'" width="'.$width.'" height="'.$height.'" title="'.$alt.'" alt="'.$alt.'" '.trim($extra).' />';
		} else {
	// wenn slideshow
			$aImgSrc = explode(",", $imgSrc);
			$anzahl = count($aImgSrc);
			if ($use_mediadb == true) {
				$this->_makeDbConnect();
				$this->oDb->query("SELECT id, filename, width, height, description FROM ".$this->aENV['table']['mdb_media']." WHERE id IN (".$imgSrc.")");
				// get image-data into temp-array
				while ($aImg = $this->oDb->fetch_array()) {
					$aImgData[$aImg['id']]['filename']		= $aImg['filename'];
					$aImgData[$aImg['id']]['width']			= $aImg['width'];
					$aImgData[$aImg['id']]['height']		= $aImg['height'];
					$aImgData[$aImg['id']]['description']	= $aImg['description'];
				}
				if (!isset($alt))	{ $alt		= $aImgData[$aImgSrc[0]]['description']; }
				if (!isset($width))	{ $width	= $aImgData[$aImgSrc[0]]['width']; }
				if (!isset($height)){ $height	= $aImgData[$aImgSrc[0]]['height']; }
			} else {
				// use description, width & height of first image for img-attributes
				if (!file_exists($path['unix'].$aImgSrc[0])) return '';
				$aImgsize = getimagesize($path['unix'].$aImgSrc[0]);
				if (!isset($alt))	{ $alt		= ''; }
				if (!isset($width))	{ $width	= $aImgsize[0]; }
				if (!isset($height)){ $height	= $aImgsize[1]; }
			}
			if (isset($width) && !empty($width))	{ $width	= $width; }
			if (isset($height) && !empty($height))	{ $height	= $height; }
		// code
			// write javascript - define array
			$sStr = '<script language="JavaScript" type="text/javascript">'."\n";
			$sStr .= "Slides = new Array();\n";
			for ($i=0; $i<$anzahl; $i++) { // put all picture-filenames in this JS-array
				$filename = ($use_mediadb == true) ? $aImgData[$aImgSrc[$i]]['filename'] : $aImgSrc[$i];
				if ($i == 0) { $first_image = $filename; } // erstes bild fuer image-tag merken
				$sStr .= "Slides[". $i."]='".$path['http'].$filename."';\n";
			}
			$sStr .="</script>\n";
			// write image-tag
			$sStr .= '<img name="Screen" src="'.$path['http'].$first_image.'" width="'.$width.'" height="'.$height.'" title="'.$alt.'" alt="'.$alt.'" '.trim($extra).' />';
			// write javascript - object init
			$sStr .= '<script language="JavaScript" type="text/javascript">'."\n";
			$sStr .= 'Slideshow = new slideshow(Slides, "Screen", "'.$layerID.'");	// params: aImagePathArray,imgName[,layerID][,direction=1][,autostart="true"]'."\n";
			$sStr .= '</script>'."\n";
		}
	// output
		return $sStr;
	}


/**
* Gibt den kompletten flash-tag fuer WEB zurueck (fuer ADMIN nicht noetig).
*
* -> benoetigt echten filename (inkl. Pfad) und $nWidth/$nHeight, ODER (nur) die ID aus der media-db! 
* (standard "no-flash" aktion: reload der seite mit all ihren GET-parametern PLUS "&flash=no") 
*
* Beispiel:
* <pre><code>
* $shockwaveFile = "pix/projects.swf?sNaviId=12&lang=de&picpath=/pix/&movement=true";
* $sFallback = 'window.document.location.href = "noflash.php?sNaviId=12";';
* echo $oMediadb->getFlashTag($shockwaveFile, 756, 96, "555555", "low", 6, $sFallback); // params: $sFile,$nWidth,$nHeight[,$sBgColor='000000'][,$sQuality='high'][,$nVersion=6][,$sFallback='']
* </code></pre>
*
* @access	public
* @param	string	$sFile			mediadb-ID oder echter Filename (inkl. Pfad) des flashfilms
* @param	integer	$nWidth			breite des flashfilms
* @param	integer	$nHeight		hoehe des flashfilms
* @param	string	$sBgColor		hintergrundfarbe des flashfilms (optional => default: '000000')
* @param	string	$sQuality		abspielqualitaet des flashfilms ['high'|'low'] (optional => default: 'high')
* @param	integer	$nVersion		benoetigte version des flash-(plugin-)players (optional => default: '6')
* @param	string	$sFallback		was tun, wenn keine benoetigte version da? (optional => default: weiterleitung auf PHP_SELF?sNaviId='.$sNaviId.'&flash=no)
* @return	html-code
*/
	function getFlashTag($sFile, $nWidth='', $nHeight='', $sBgColor='000000', $sQuality='high', $nVersion=6, $sFallback='') {
		
	// vars
		if (!$sFile) return '';
		if (!preg_match("/^#/", $sBgColor)) { $sBgColor = '#'.$sBgColor; } // ggf. raute vor farbangabe setzen
		
		// unterscheidung: mediadb oder filename
		if (preg_match("/\.swf/i", $sFile)) {	// sFile ist filename
			$sFilename = $sFile;
		} else {	// sFile ist mediadb-ID
			$aMediaFile = $this->getMedia($sFile); // params: $nMediaId
			$sFilename = $this->aENV['path']['mdb_upload']['http'].$aMediaFile['filename'];
			// uebergebene width/height-angaben werden wichtiger behandelt als die in der mediadb gespeicherten
			if (empty($nWidth))		{ $nWidth = $aMediaFile['width']; }
			if (empty($nHeight))	{ $nHeight = $aMediaFile['height']; }
		}
		
	// code
		// write tag
		$sStr = '<script language="JavaScript1.1" type="text/javascript"><!--'."\n";
		$sStr .= 'var shockwaveFile = "'.$sFilename.'";'."\n";
		$sStr .= 'var shockwaveWidth = "'.$nWidth.'";'."\n";
		$sStr .= 'var shockwaveHeight = "'.$nHeight.'";'."\n";
		$sStr .= 'var shockwaveBgColor = "'.$sBgColor.'";'."\n";
		$sStr .= 'var shockwaveQuality = "'.$sQuality.'";'."\n";
		$sStr .= 'var MM_contentVersion = '.$nVersion.';'."\n";
		// check
		$sStr .= 'var plugin = (navigator.mimeTypes && navigator.mimeTypes["application/x-shockwave-flash"]) ? navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin : 0;'."\n";
		$sStr .= 'if (plugin) {'."\n";
		$sStr .= '	var words = navigator.plugins["Shockwave Flash"].description.split(" ");'."\n";
		$sStr .= '	for (var i = 0; i < words.length; ++i) {'."\n";
		$sStr .= '		if (isNaN(parseInt(words[i]))) continue;'."\n";
		$sStr .= '		var MM_PluginVersion = words[i];'."\n";
		$sStr .= '	}'."\n";
		$sStr .= '	var MM_FlashCanPlay = MM_PluginVersion >= MM_contentVersion;'."\n";
		$sStr .= '} else if (navigator.userAgent && navigator.userAgent.indexOf("MSIE")>=0 && (navigator.appVersion.indexOf("Win") != -1)) {'."\n";
		$sStr .= '	document.write(\'<SCR\' + \'IPT LANGUAGE=VBScript>\');'."\n";
		$sStr .= '	document.write(\'on error resume next \n\');'."\n";
		$sStr .= '	document.write(\'MM_FlashCanPlay = ( IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash." & MM_contentVersion)))\n\');'."\n";
		$sStr .= '	document.write(\'</SCR\' + \'IPT\>\');'."\n";
		$sStr .= '}'."\n";
		$sStr .= 'if ( MM_FlashCanPlay ) {'."\n";
		
		// flash
		$sStr .= 'document.write(\'<!-- flash --><table border="0" cellpadding="0" cellspacing="0"><tr>\');'."\n";
		$sStr .= 'document.write(\'<td><OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"\');'."\n";
		$sStr .= 'document.write(\'  codebase="http://active.macromedia.com/flash6/cabs/swflash.cab#version=\'+MM_contentVersion+\',0,0,0" \');'."\n";
		$sStr .= 'document.write(\' ID="flash" WIDTH="\'+shockwaveWidth+\'" HEIGHT="\'+shockwaveHeight+\'" ALIGN="">\');'."\n";
		$sStr .= 'document.write(\' <PARAM NAME="movie" VALUE="\'+shockwaveFile+\'"><PARAM NAME="quality" VALUE="\'+shockwaveQuality+\'"><PARAM NAME="bgcolor" VALUE="\'+shockwaveBgColor+\'"><PARAM NAME="wmode" VALUE="transparent">  \');'."\n";
		$sStr .= 'document.write(\' <EMBED src="\'+shockwaveFile+\'" quality="\'+shockwaveQuality+\'" bgcolor="\'+shockwaveBgColor+\'" wmode="transparent"  \');'."\n";
		$sStr .= 'document.write(\' swLiveConnect="FALSE" WIDTH="\'+shockwaveWidth+\'" HEIGHT="\'+shockwaveHeight+\'" NAME="flash" ALIGN=""\');'."\n";
		$sStr .= 'document.write(\' TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">\');'."\n";
		$sStr .= 'document.write(\' </EMBED></OBJECT></td>\');'."\n";
		$sStr .= 'document.write(\'</tr></table>\');'."\n";
		
		$sStr .= '} else {	// NoFlash-version'."\n";
		
		// fallback
		if ($sFallback == '')  {
			$noFlashStr = (preg_match("/\?/", $_SERVER['REQUEST_URI'])) ? '&flash=no' : '?flash=no';
			$sStr .= 'window.document.location.href = "'.$_SERVER['REQUEST_URI'].$noFlashStr.'";'."\n";
		} else {
			$sStr .= 'document.write(\''.$sFallback.'\');'."\n";
		}
		$sStr .= '}'."\n";
		$sStr .= '//--></script>'."\n";
		
		// noscript
		if ($sFallback != '')  {
			$sStr .= '<noscript>'.$sFallback.'</noscript>'."\n";
		}
		
	// output
		return $sStr;
	}

/**
* Gibt das komplette Array eines MediaDB-Files zurueck.
*
* Beispiel:
* <pre><code>
* $aMediaFile = $oMediadb->getMedia($mediaFileId); // params: $nMediaId
* </code></pre>
*
* @access	public
* @param	integer	$nMediaId		mediadb-ID
* @return	array
*/
	function getMedia($nMediaId) {
		// vars
		if (!$nMediaId) return false;
		$this->_makeDbConnect();
		
		// output
		return $this->oDb->fetch_by_id($nMediaId, $this->aENV['table']['mdb_media']);
	}

/**
* Ermittelt den Media-Typ ("image","pdf",...) z.B. fuer die "getMediaIcon()"-Funktion.
*
* Beispiel:
* <pre><code>
* echo $oMediadb->getMediaType($aData['filename']); // params: $sFilename
* </code></pre>
*
* @access	public
* @param	string		$sFilename		Nur der Dateiname ohne Pfad
* @return	string		Media-Typ
*/
	function getMediaType($sFilename) {
		// vars
		$sFilename = strToLower(basename($sFilename));
		$sFiletype = 'file'; // set default
		// get array
		$aFiletype = mediadb::getFiletypeArray();
		// ermittle filetype
		foreach ($aFiletype as $filetype => $regex) {
			if (preg_match("/.(".$regex.")$/", $sFilename))	{
				$sFiletype = $filetype;
				break;
			}
		}
		// output
		return $sFiletype;
	}

/**
* Haelt ein Array vor, welches die Beziehung von Datei-Endungen zu Filetype-Klassifizierungen abbildet.
* NOTE: Wenn hier eine Endung hinzugefuegt wird, unbedingt auch in der class.download.php unter "getMimeType()" den entsprechenden Eintrag ergaenzen!
*
* @access   public
* @return	array	[key:filetype, val:datei-endungen ("|"-getrennt)]
*/
	function getFiletypeArray() {
		return array(
			'image'		=> 'jpg|jpeg|gif|png|tif|tiff|ai|eps|psd|ico|bmp',
			'flash'		=> 'swf|fla',
			'pdf'		=> 'pdf',
			'zip'		=> 'zip|tar|sit|gtar|hqx|gz|tgz|rar',
			'text'		=> 'txt',
			'web'		=> 'php|htm|html|xml|xsl|xslt|js|css|svg',
			'word'		=> 'rtf|doc|dot',
			'powerpoint'=> 'ppt|pps',
			'excel'		=> 'xls|csv',
			'sound'		=> 'ram|wav|aif|aiff|mp3|ogg|ra|rm',
			'video'		=> 'flv|mpeg|mpg|mpe|mov|qt|avi|wmv|mp4|x-flv|x-ms-wmv'
		);
	}

/**
* Haelt ein Array vor, welches die Beziehung von Filetype-Klassifizierungen zu Mime-Types abbildet.
*
* @access   public
* @return	array	[key:filetype, val:mimetypes als flaches array]
*/
	function getFiletypeMimetypeArray() {
		// vars
		require_once($this->aENV['path']['global_service']['unix']."class.download.php");
		$aBuffer = array();
		// definition der Filetype-Klassifizierungen holen
		$aFiletype = mediadb::getFiletypeArray();
		// Filetypes durchlaufen und Datei-Endungen splitten
		foreach($aFiletype as $filetype => $sData) {
			$aBuffer[$filetype]['suffix'] = explode('|',$sData);
		}
		// Filetypes durchlaufen und fuer jede Datei-Endung den MimeType ermitteln
		foreach($aBuffer as $filetype => $sData) {
			for($i=0; !empty($sData['suffix'][$i]); $i++) {
				$aBuffer[$filetype]['mimetype'][$i] = download::getMimeType('test.'.$sData['suffix'][$i]);
			}
		}
		// output
		return $aBuffer;
	}

/**
* Haelt ein Array vor, welches die Beziehung von Mime-Types zu Filetype-Klassifizierungen abbildet.
*
* @access   public
* @return	array	[key:mimetype, val:filetype]
*/
	function getMimetypeFiletypeArray() {
		// vars
		$aBuffer = array();
		require_once($this->aENV['path']['global_service']['unix']."class.download.php");
		$aFiletype = mediadb::getFiletypeArray();
		// Filetypes durchlaufen und Datei-Endungen splitten
		foreach($aFiletype as $filetype => $sData) {
			$aData = explode('|',$sData);
			for($i=0;!empty($aData[$i]);$i++) {
				// fuer jeden Mime-Type (ermittelt via Datei-Endung) den filetype (mediatype) sammeln
				$aBuffer[download::getMimeType('test.'.$aData[$i])] = $filetype;
			}
		}
		// output
		return $aBuffer;
	}
	

/**
* Gibt ein Icon (komplettes IMG-Tag) zureuck (ermittelt dazu zuerst den Media-Typ).
*
* Beispiel:
* <pre><code>
* echo $oMediadb->getMediaIcon($aData['filename']); // params: $sFilename[,$sAlt=''][,$sPath=''][,$sClass='']
* </code></pre>
*
* @access	public
* @param	string	$sFilename		Nur der Dateiname ohne Pfad
* @param	string	$sAlt			Alt-Tag Attribut (optional => default: '')
* @param	string	$sPath			Pfad zum Icon (optional => default: sys-pix-Ordner)
* @param	string	$sClass			Pfad zum Icon (optional => default: '')
* @return	string	Icon IMG-Tag
*/
	function getMediaIcon($sFilename, $sAlt='', $sPath='', $sExtra='') {
		// check vars
		if (empty($sFilename)) return '';
		// get media-type
		$sType = $this->getMediaType($sFilename); // params: $sFilename
		// path
		$aPath = array();
		$aPath['unix'] = (empty($sPath)) ? $this->aENV['path']['pix_global']['unix'] : $sPath;
		$aPath['http'] = (empty($sPath)) ? $this->aENV['path']['pix_global']['http'] : $sPath;
		// file
		$sFile = 'icn_'.$sType.'.gif';
		if (!file_exists($aPath['unix'].$sFile)) $sFile = 'icn_file.gif'; // fallback
		if (!file_exists($aPath['unix'].$sFile)) return ''; // fallback fallback
		// icon image size
		$imagedata = getimagesize($aPath['unix'].$sFile);
		// output HTML
		return '<img src="'.$aPath['http'].$sFile.'" '.$imagedata[3].' border="0" alt="'.$sAlt.'"'.$sExtra.' />';
	}

/**
* Ermittelt thumbnail (icon a.d. sys-pix-Ordner als fallback) fuer z.B. die upload-Funktionen oder auch fuer die Listendarstellung auf der Seite '*_media.php'.
*
* Beispiel:
* <pre><code>
* echo $oMediadb->getMediaThumb($aData['filename']); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
* </code></pre>
*
* @access	public
* @global	global	$oFile			File-Object
* @param	string	$sFilename		Nur der Dateiname ohne Pfad
* @param	string	$sExtra			(optional -> default:'')
* @param	boolean	$sPathKey		(optional -> default:'mdb_upload')
* @return	html-code
*/
	function getMediaThumb($sFilename, $sExtra='', $sPathKey='mdb_upload', $width = 40, $thumb_max_kb = 100, $bWeb=false) {
		// $width = 40 angezeigte thumb-groesse (kann sich von der echten thumb-groesse (50) unterscheiden!)
		// $thumb_max_kb = 100 ab dieser filesize werden bei bildern icons statt thumbs angezeigt
		
		// ermittle filetype
		$sDbMediaType = $this->getMediaType($sFilename);
		
		// pfad zum thumbnail-script bestimmen
		$sThumbPath['http'] = ($sPathKey == 'mdb_upload') ? $this->aENV['path']['mdb']['http'] : $this->aENV['SELF_PATH'];
		$sThumbPath['unix'] = ($sPathKey == 'mdb_upload') ? $this->aENV['path']['mdb']['unix'] : './';
		
		// checke auf vorhandensein
		if (empty($sFilename) || !file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
			return '<img src="'.$this->aENV['path']['pix_global']['http'].'icn_nofile.gif" alt="" border="0" '.$sExtra.' />';
		}
		
		// ermittle filesize
		global $oFile;
		$nFilesizeInBytes	= $oFile->get_filesize($this->aENV['path'][$sPathKey]['unix'].$sFilename, ''); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		$nFilesizeInKB		= round($nFilesizeInBytes / 1024);
		$sFilesize			= $oFile->format_filesize($nFilesizeInBytes, 'AUTO', 0); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		
		// default: ICON des ermittelten media-types
		$sStr = $this->aENV['path']['pix_global']['http'].'icn_'.$sDbMediaType.'.gif" width="22" height="18"'; // icon-groesse
				
		// bei "web-bildern" -> teste auf thumbnail-faehigkeit
		if (preg_match("/.(gif|jpg|png|bmp)$/i", $sFilename) 
			&& function_exists("gd_info") 
			&& file_exists($sThumbPath['unix']."thumb.php") 
			//&& $nFilesizeInBytes < 500000
			) { // muss kleiner 500KB sein
			
			$gd_info	= gd_info();
			$imagedata	= getimagesize($this->aENV['path'][$sPathKey]['unix'].$sFilename);
			$types		= array (1 => "gif", 
								"jpeg", 
								"png", 
								"swf", 
								"psd", 
								"wbmp");
								
			if (	(strToLower($types[$imagedata[2]]) == "gif"		&& $gd_info['GIF Create Support'] == true)
				 ||	(strToLower($types[$imagedata[2]]) == "jpeg"	&& $gd_info['JPG Support'] == true)
				 ||	(strToLower($types[$imagedata[2]]) == "png"		&& $gd_info['PNG Support'] == true)
				 ||	(strToLower($types[$imagedata[2]]) == "wbmp"	&& $gd_info['WBMP Support'] == true)
				) {
			// mit funktionierender GD-Lib -> erzeuge thumbnail...
				$this_path = $this->aENV['path']['mdb_upload'];
				$upload_path = str_replace(array("http://", "https://", $_SERVER['HTTP_HOST']), '', $this->aENV['path'][$sPathKey]['http']);
				if(!$bWeb) {
					$relativeMediaPath = $_SERVER['DOCUMENT_ROOT'].str_replace($this_path, '', $upload_path);
				}
				else {
					$relativeMediaPath = $_SERVER['DOCUMENT_ROOT'].$upload_path;
				}
				$relativeMediaPath = urlencode($relativeMediaPath);				if(!$bWeb) {
					$sStr = $sThumbPath['http'].'thumb.php?image='.$relativeMediaPath.$sFilename.'&x='.MDB_THUMB_WIDTH.'&bWeb=1" width="'.$width.'"';
				}
				else {
					$sStr = $sThumbPath['http'].'thumb.php?image='.$relativeMediaPath.$sFilename.'&x='.$width.'&bWeb=0" width="'.$width.'"';
				}
			} else {
				// ... ansonsten verwende original
				if ($nFilesizeInKB < $thumb_max_kb && $sPathKey != 'protected') { // (-> NUR wenn unter $thumb_max_kb UND wenn verzeichnis nicht geschuetzt!)
					$sStr = $this->aENV['path'][$sPathKey]['http'].$sFilename.'" width="'.$width.'"';
				}
			}
		}
		
		// output
		$sDescription = (strstr($sExtra, 'alt=')) ? '' : ' alt="'.$sFilename.' ['.$sFilesize.']" title="'.$sFilename.' ['.$sFilesize.']"';
		return '<img src="'.$sStr.$sDescription.' border="0" '.$sExtra.' />';
	}
	
	function generateThumb ($sFileName, $sExtra='', $sPathKey='mdb_upload', $sCacheDir='', $nMaxKb = 100, $nSrcWidth = 40, $nCutWidth = '', $bRatio = true, $bCut = false) {
//		print_r(func_get_args());
//		exit;
		$sDbMediaType = $this->getMediaType($sFileName);
		
		$sThumbPath['http'] = ($sPathKey == 'mdb_upload') ? $this->aENV['path']['mdb']['http'] : $this->aENV['SELF_PATH'];
		$sThumbPath['unix'] = ($sPathKey == 'mdb_upload') ? $this->aENV['path']['mdb']['unix'] : './';
		
		// if file doesn't exists, return NO FILE Icon
		if (empty($sFileName) 
		|| !file_exists($this->aENV['path'][$sPathKey]['unix'].$sFileName)) {
			
			return false;
			
		}
		// set File Object to global for later use
		global $oFile;
				
		// get the filesize of the image
		$nFilesizeInBytes	= $oFile->get_filesize($this->aENV['path'][$sPathKey]['unix'].$sFileName, ''); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		$nFilesizeInKB		= round($nFilesizeInBytes / 1024);
		$sFilesize			= $oFile->format_filesize($nFilesizeInBytes, 'AUTO', 0); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
				
		// if the file is an image and the gd lib works and the filesize is smaller than 500 kB		
		if (preg_match("/.(gif|jpg|png|bmp)$/i", $sFileName) 
			&& function_exists("gd_info") 
//			&& $nFilesizeInBytes < 500000
			) { // muss kleiner 500KB sein
			
			$gd_info	= gd_info();
			$aImgData	= getimagesize($this->aENV['path'][$sPathKey]['unix'].$sFileName);
			$types		= array (1 => "gif", 
								"jpeg", 
								"png", 
								"swf", 
								"psd", 
								"wbmp");

			// if gd lib can work with the image, initialise thumbcreation
/*			if (	(strToLower($types[$aImgData[2]]) == "gif"		&& $gd_info['GIF Create Support'] == true)
				 ||	(strToLower($types[$aImgData[2]]) == "jpeg"	&& $gd_info['JPG Support'] == true)
				 ||	(strToLower($types[$aImgData[2]]) == "png"		&& $gd_info['PNG Support'] == true)
				 ||	(strToLower($types[$aImgData[2]]) == "wbmp"	&& $gd_info['WBMP Support'] == true)
				) {*/
					
				$this_path		= $this->aENV['path']['mdb_upload'];
				$upload_path	= str_replace(array("http://", "https://", $_SERVER['HTTP_HOST']), '', $this->aENV['path'][$sPathKey]['http']);
				
				$relativeMediaPath = $_SERVER['DOCUMENT_ROOT'].$upload_path;
				$sFileName	= $relativeMediaPath.$sFileName;
				
			//} 
			 
		} // end if  
				
		$types = array (1 => "gif", "jpeg", "png", "swf", "psd", "wbmp");		

//		if (empty($sFileName) 
//		|| !file_exists($sFileName)) { return false; }
		
		$sCacheDir	= (empty($sCacheDir)) ? 'thumbs/':$sCacheDir."/";	
	
		$nWidth	= $nSrcWidth;
	
		if (empty($nWidth))		unset($nWidth);
		if (empty($nHeight))	unset($nHeight);
		if (empty($bRatio))		unset($bRatio);
	
		if (!isset($nWidth) && !isset($nHeight)) { return false; }
		
		if (strToLower($types[$aImgData[2]]) == "swf") {
			return false;
		}
	
		//$sCacheDir	= substr($sFileName, 0, strrpos($sFileName, '/') + 1).$sCacheDir;
		$sCacheDir	= $this->aENV['path'][$sPathKey]['unix'].$sCacheDir;
		
		(!is_dir($sCacheDir))
			? mkdir($sCacheDir, 0777)
			: system("chmod 0777 ".$sCacheDir);
	
		eval ('
			if (!(imagetypes() & IMG_'.strtoupper($types[$aImgData[2]]).')) {
				return false;
			}
		');

		(!isset($bRatio))
			? (isset($nWidth) && isset($nHeight)
				? $bRatio = true
				: $bRatio = false)
			: $bRatio;
		
		(!isset($nWidth))
			? $nWidth = floor($nHeight * $aImgData[0] / $aImgData[1])
			: $nWidth;
		
		(!isset($nHeight))
			? $nHeight = floor($nWidth * $aImgData[1] / $aImgData[0])
			: $nHeight;
		
		if($nHeight < $nCutWidth) {
			$nScale		= round ($nCutWidth / $nHeight);
			$nHeight	= $nCutWidth;
			$nWidth		= floor($nWidth * $nScale);
		}

		if($nWidth < $nCutWidth) {
			$nScale		= round ($nCutWidth / $nWidth);
			$nWidth		= $nCutWidth;
			$nHeight	= floor($nHeight * $nScale);
		}		
		
		if(!isset($bRatio)
		&& !empty($nHeight)
		&& !empty($nWidth)) {
			
			if ($aImgData[0] > $aImgData[1]) {
				
				$nHeight = floor ($nWidth * $aImgData[1] / $aImgData[0]);
				
			} else if ($aImgData[1] > $aImgData[0]) {
				
				$nWidth = floor ($nHeight * $aImgData[0] / $aImgData[1]);
				
			} // end else
			
		} // end if
	
		$sThumbFile = substr($sFileName, strrpos($sFileName, '/') + 1);

		if (file_exists($sCacheDir.$sThumbFile)) {
			$aThumbdata 	= getimagesize($sCacheDir.$sThumbFile);
			$aThumbdata[0] == $nWidth && $aThumbdata[1] == $nHeight
				? $bIsCached	= true
				: $bIsCached	= false;
		} else {
			$bIsCached = false;
		}
		
		if ($bIsCached == false) {
			($aImgData[0] > $nWidth || $aImgData[1] > $nHeight)
				? $bMakeThumb = true
				: $bMakeThumb = false;
		} else {
			$bMakeThumb = false;
		}
		
		if ($bMakeThumb == true) {
		
			$rImage	= call_user_func("imagecreatefrom".$types[$aImgData[2]], $sFileName);
				
			if (function_exists('imagecreatetruecolor') && ($aImgData[2] == 2 || $aImgData[2] == 3)) {
				$rThumb = imagecreatetruecolor($nWidth, $nHeight); // use imagecreatetruecolor (bei GD 2.01 und JPG/PNG)
			} else {
				$rThumb = imagecreate($nWidth, $nHeight); // use imagecreate
			}

			imagecopyresampled($rThumb, $rImage, 0, 0, 0, 0, $nWidth, $nHeight, $aImgData[0], $aImgData[1]);
			
			call_user_func("image".$types[$aImgData[2]], $rThumb, $sCacheDir.$sThumbFile, 100);

			imagedestroy($rImage);
			imagedestroy($rThumb);
		}
		
		if($bCut == true) {
			
			$nSrcHeight	= (empty($nSrcHeight))	? $nSrcWidth:$nSrcHeight;
			$nSrcWidth	= (empty($nSrcWidth))	? $nSrcHeight:$nSrcWidth;
			
			$nSrcY	= ($nHeight != $nSrcHeight) ? (($nHeight > $nSrcHeight ? $nHeight : $nSrcHeight) - ($nHeight < $nSrcHeight ? $nHeight : $nSrcHeight)) / 2:0;
			$nSrcX	= ($nWidth != $nSrcWidth) ? (($nWidth > $nSrcWidth ? $nWidth:$nSrcWidth) - ($nWidth < $nSrcWidth ? $nWidth : $nSrcWidth)) / 2:0;

			$rImage	= call_user_func("imagecreatefrom".$types[$aImgData[2]], $sCacheDir.$sThumbFile);
				
			if (function_exists('imagecreatetruecolor') && ($aImgData[2] == 2 || $aImgData[2] == 3)) {
				$rThumb = imagecreatetruecolor($nSrcWidth, $nSrcHeight); // use imagecreatetruecolor (bei GD 2.01 und JPG/PNG)
			} else {
				$rThumb = imagecreate($nSrcWidth, $nSrcHeight); // use imagecreate
			}

			imagecopyresampled($rThumb, $rImage, 0, 0, $nSrcX, $nSrcY, $nSrcWidth, $nSrcHeight, $nSrcWidth, $nSrcHeight);
			
			call_user_func("image".$types[$aImgData[2]], $rThumb, $sCacheDir.$sThumbFile, 100);

			imagedestroy($rImage);
			imagedestroy($rThumb);
		}

		return true;
			
	}

	function generateThumb_old($sFilename, $sExtra='', $sPathKey='mdb_upload', $nWidth = 40, $nMaxKb = 100, $sCacheDir='') {

		$sDbMediaType = $this->getMediaType($sFilename);
		
		$sThumbPath['http'] = ($sPathKey == 'mdb_upload') ? $this->aENV['path']['mdb']['http'] : $this->aENV['SELF_PATH'];
		$sThumbPath['unix'] = ($sPathKey == 'mdb_upload') ? $this->aENV['path']['mdb']['unix'] : './';
		
		// if file doesn't exists, return NO FILE Icon
		if (empty($sFilename) 
		|| !file_exists($this->aENV['path'][$sPathKey]['unix'].$sFilename)) {
			
			return '<img src="'.$this->aENV['path']['pix_global']['http'].'icn_nofile.gif" alt="" border="0" '.$sExtra.' />';
			
		}

		// set File Object to global for later use
		global $oFile;
		
		// get the filesize of the image
		$nFilesizeInBytes	= $oFile->get_filesize($this->aENV['path'][$sPathKey]['unix'].$sFilename, ''); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		$nFilesizeInKB		= round($nFilesizeInBytes / 1024);
		$sFilesize			= $oFile->format_filesize($nFilesizeInBytes, 'AUTO', 0); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		
		// get the icon for the image type, if gd is not working
		$sStr	= $this->aENV['path']['pix_global']['http'].'icn_'.$sDbMediaType.'.gif" width="22" height="18"'; // icon-groesse
				
		// if the file is an image and the gd lib works and the filesize is smaller than 500 kB		
		if (preg_match("/.(gif|jpg|png|bmp)$/i", $sFilename) 
			&& function_exists("gd_info") 
			&& $nFilesizeInBytes < 500000) { // muss kleiner 500KB sein
			
			$gd_info	= gd_info();
			$imagedata	= getimagesize($this->aENV['path'][$sPathKey]['unix'].$sFilename);
			$types		= array (1 => "gif", 
								"jpeg", 
								"png", 
								"swf", 
								"psd", 
								"wbmp");

			// if gd lib can work with the image, initialise thumbcreation
			if (	(strToLower($types[$imagedata[2]]) == "gif"		&& $gd_info['GIF Create Support'] == true)
				 ||	(strToLower($types[$imagedata[2]]) == "jpeg"	&& $gd_info['JPG Support'] == true)
				 ||	(strToLower($types[$imagedata[2]]) == "png"		&& $gd_info['PNG Support'] == true)
				 ||	(strToLower($types[$imagedata[2]]) == "wbmp"	&& $gd_info['WBMP Support'] == true)
				) {
					
				$this_path		= $this->aENV['path']['mdb_upload'];
				$upload_path	= str_replace(array("http://", "https://", $_SERVER['HTTP_HOST']), '', $this->aENV['path'][$sPathKey]['http']);
				
				$relativeMediaPath = $_SERVER['DOCUMENT_ROOT'].$upload_path;
				$sStr	= $sThumbPath['http'].'generateThumb.php?image='.$relativeMediaPath.$sFilename.'&w='.$nWidth.(!empty($sCacheDir) ? '&cachedir='.$sCacheDir:'') ;
				
			} elseif($nFilesizeInKB < $nMaxKb 
					 && $sPathKey != 'protected') { // else use the original image, if it's not protected

				$sStr	= $this->aENV['path'][$sPathKey]['http'].$sFilename.'" width="'.$nWidth.'"';
				 
			} // end else
			 
		} // end if  
		
		// alt tag and title tag
		$sDescription = (strstr($sExtra, 'alt=')) ? '' : ' alt="'.$sFilename.' ['.$sFilesize.']" title="'.$sFilename.' ['.$sFilesize.']"';

		// return the image tag 
		return '<img src="'.$sStr.'" '.$sDescription.' border="0" '.$sExtra.' />';
	}

#-----------------------------------------------------------------------------

/**
* Erledigt den kompletten Media-Upload /-Insert aus einem Modul heraus und gibt die neue ID des Mediafiles zurueck.
*
* Beispiel:
* <pre><code>
* $newUploadId = $oMediadb->moduleUpload('uploadfile'); // params: $sUploadFieldname[,$sDescription=''][,$sKeywords='']
* </code></pre>
*
* @access	public
* @global 	array		$Userdata			Session-Array des aktuellen Users
* @global 	array		$aMSG				globales Messages-Array f.d. Fehlermeldungen beim Upload
* @global 	string		$syslang			aktuelle System-Sprache f.d. Fehlermeldungen beim Upload
* @param	string		$sUploadFieldname	Name des Upload-Formfeldes
* @param	string		$sDescription		Inhalt f.d. DB-Field "description" (optional - default: '')
* @param	string		$sKeywords			Inhalt f.d. DB-Field "keywords" (optional - default: '')
* @return	int			ID des neuen MediaDB-Datensatzes
*/
	function moduleUpload($sUploadFieldname, $sDescription='', $sKeywords='', $nMaxSize=NULL, $nMaxWidth=NULL, $nMaxHeight=NULL) {
		// check vars
		if (empty($sUploadFieldname)) return false;
		if (!isset($_FILES[$sUploadFieldname]['name']) || empty($_FILES[$sUploadFieldname]['name'])) return false;
		global $aMSG, $Userdata, $syslang;
		// get additional file infos
		$aData['name']			= $_FILES[$sUploadFieldname]['name'];
		$aData['filesize']		= $_FILES[$sUploadFieldname]['size'];
		$aData['filetype']		= $_FILES[$sUploadFieldname]['type'];
		// ggf. mimetype korrigieren
		if ($aData['filetype'] == 'image/pjpeg') 		{ $aData['filetype'] = 'image/jpeg'; } // NO progressive JPEGs [... IE-Bug (PC only): always sets filetype to progressive... ]
		if ($aData['filetype'] == 'image/x-png') 		{ $aData['filetype'] = 'image/png'; }
		if ($aData['filetype'] == 'application/x-gzip') { $aData['filetype'] = 'application/gzip'; }
		if ($aData['filetype'] == 'application/x-zip-compressed') { $aData['filetype'] = 'application/zip'; }
			// ggf. add. image-infos
		if (strstr($_FILES[$sUploadFieldname]['type'], "image") || strstr($_FILES[$sUploadFieldname]['type'], "flash")) {
			$imgsize			= getimagesize($_FILES[$sUploadFieldname]['tmp_name']);
			$aData['width']		= $imgsize[0];
			$aData['height']	= $imgsize[1];
		}
		$aData['description']	= $sDescription;
		$aData['keywords']		= $sKeywords;
		$aData['created']		= date('YmdHis');
		$aData['created_by']	= $Userdata['id'];
		$aData['tree_id']		= '-1';
		// make upload
		$oUpload =& new upload($sUploadFieldname);
		$oUpload->set_syslang($syslang); // params: $syslang
		#Die folgenden 2 methoden sind ueberfluessig, da ein "unique_filename" erzeugt wird, der sowieso "clean" ist:
		#$oUpload->check_filename_length(40, $aMSG['media']['alert_length']); // params: [$maxlength=40][,$error_msg (als array)]
		#$oUpload->check_filename($aMSG['media']['alert_chars']); // params: [$error_msg (als array)]
		if (!($aData['filename'] = $oUpload->make_upload($this->aENV['path']['mdb_upload']['unix']))) {$oUpload->script_alert(); $alert=true;}
		if ($alert == true) return false;
		// make insert
		$this->_makeDbConnect();
		$this->oDb->make_insert($aData, $this->aENV['table']['mdb_media']);
		// return new ID
		return $this->oDb->insert_id();
	}

/**
* Erledigt den kompletten Delete-Vorgang von Media-Upload-Dateien aus einem Modul heraus.
* Es kann dazu eine ID, mehrere IDs (kommasepariert oder als Array) uebergeben werden.
*
* Beispiel:
* <pre><code>
* $oMediadb->moduleDelete($mdbId); // params: $mdbId
* </code></pre>
*
* @access	public
* @global 	object		$oFile				globales File-Object
* @param	mixed		$mdbId				Array (/kommasep.String) aus IDs der MediaFiles, die nach Pruefung ggf. geloescht werden
* @return	void
*/
	function moduleDelete($mdbId) {
		if (!is_array($mdbId)) $mdbId = explode(',', $mdbId);
		if (count($mdbId) < 1) return;
		global $oFile;
		$aFilesToDelete = array();
		$aMdbIdsToDelete = array();
		foreach($mdbId as $mId) {
			$aMediaFile = $this->getMedia($mId); // params: $nMediaId
			if ($aMediaFile['tree_id'] == -1 && file_exists($this->aENV['path']['mdb_upload']['unix'].$aMediaFile['filename'])) {
				// collect files
				$aFilesToDelete[] = $this->aENV['path']['mdb_upload']['unix'].$aMediaFile['filename'];
				$aFilesToDelete[] = $this->aENV['path']['mdb_upload_cache']['unix'].$aMediaFile['filename'];
				// collect ids
				$aMdbIdsToDelete[] = $aMediaFile['id'];
			}
		}
		if (is_array($aMdbIdsToDelete) && count($aMdbIdsToDelete) > 0) {
			#print_r($aMdbIdsToDelete);
			#print_r($aFilesToDelete);
			// delete files
			$oFile->delete_files($aFilesToDelete);
			// delete data
			$this->_makeDbConnect();
			$this->oDb->query('DELETE FROM '.$this->aENV['table']['mdb_media'].' WHERE id IN ('.implode(',', $aMdbIdsToDelete).')');
		}
	}
#-----------------------------------------------------------------------------

} // END of class
?>