<?php
/*
 * Created on 03.11.2005
 * lastedit: 11.01.2006
 */
/**
 * Diese Klasse sorgt fuer den TeamUser Access und stellt Servicemethoden zum vereinfachten Zugriff bereit.
 * - ein Interface auf HTML-basis
 * - Methoden zum Setzen
 * - Methoden zum Abfragen
 * - Allgemeine Methoden fuer interne zwecke
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @author Oliver Hilscher <oh@design-aspekt.com>
 * @author Andy Fehn <af@design-aspekt.com>
 * @version 1.0
 */
class TeamAccessControllEA extends BeanService {

  	var $oDb = null;
	var $entries = 0;
	var $order = "ASC";
	var $orderby = "id";
	var $aENV = null;
	var $table = "";
	var $flagtree = "";
  	var $uid = 0;
  	var $oTeam = null;
  	var $bEditMode = true;
  
  	/**
  	 * Setzt die Grundeinstellungen fuer diese Klasse
  	 * @param object $oDb Datenbankobject
   	 * @return void
  	 */
	function TeamAccessControllEA($oDb) {
		$this->oDb = $oDb;
		$this->oTeam =& new Team($oDb);
	}
   
   	/**
   	 * Setzt die Table
   	 * @param String $table
   	 * @return void
   	 */
	function setTable($table) {
   		$this->table = $table;	
	}
   
   	/**
   	 * Setzt das Env fuer diese Klasse und setzt automatisch die Table
   	 * @param array $aenv
   	 * @return void
   	 */
	function setAENV(&$aenv) {
		$this->aENV =& $aenv;
   		$this->setTable($aenv['table']['sys_team_access']);
	}
   
   	/**
   	 * Setzt den Editmode fuer die Ausgabe
   	 * @param boolean $bEditMode
   	 * @return void
   	 */
	function setBeditmode($bEditMode) {
		$this->bEditMode = $bEditMode;
	}
	
   	/**
   	 * Legt die Reihenfolge fest in der Sortiert werden soll. (ASC|DESC)
   	 * @param String $order
   	 * @return void
   	 */
	function setOrder($order) {
		$this->order = $order;
	}

   	/**
   	 * Legt die Felder fest nach denen sortiert werden soll.
   	 * @param String $orderby
   	 * @return void
   	 */
   		
	function setOrderBy($orderby) {
		$this->orderby = $orderby;
	}

	/**
	 * Diese Methode initialisiert mit Hilfe der Serviceklasse die Bean
	 * @version 0.11
	 * @param Int $start Der Startwert fuer das DB-Limit (Seitenauflistung)
	 */
	function initialize($start=0) {
   		$this->bean = $this->setBeanValues($this->getValuesFromDb($start),new TeamAccessControllBean());
   		$this->setGetBeanValues();
   	}

	/**
	 * Diese Methode befuellt das Array, welches nachtraeglich dazu dient den Pointer zu bewegen
	 * @deprecated version - 11.01.2006 - funktioniert wahrscheinlich nicht richtig, da die Klasse ein Array baut, welches fuer den Pointer nicht geeignet ist.
	 */
	function setGetBeanValues() {
		$this->getBeanValues(array('flagshow','fkid','ugid','id','flagtype','flagtree'));	
	}

   	/**
   	 * Diese Methode fragt alle Notwendigen Daten von der DB ab und speichert sie in einem Array
   	 * @return String[]
   	 */
   	function getValuesFromDb() {
   		   		
   		$table1 = $this->table;
   		
		$sql = "SELECT $table1.id,$table1.flag_type,$table1.ug_id,$table1.fk_id,$table1.flag_show,$table1.flag_tree";
		$sql .= " FROM $table1 ";
		$sql .= "WHERE $table1.flag_show='1'";	
		if($this->orderby == "id") {
			$sql .= " ORDER BY $table1.id ".$this->order;
		}
		else {
			$sql .= " ORDER BY ".$this->orderby." ".$this->order;	
		}
   		
		$this->oDb->query($sql);
		
   		for($i=0;$row=$this->oDb->fetch_object();$i++) {
   			$ret["id"][$row->flag_tree][$row->fk_id][]	 		= $row->id;
   			$ret["flagtype"][$row->flag_tree][$row->fk_id][]  	= $row->flag_type;	
   			$ret["ugid"][$row->flag_tree][$row->fk_id][] 		= $row->ug_id;
   		}
		
   		return $ret;
   			
   	}

  	/**
   	 * Diese Methode fragt ob ein Datensatz personalisiert wurde.
   	 * Wenn ja, gibt sie ein array, sonst false zurueck.
  	 * @param int $fkid
  	 * @param string $mod
   	 * @return mixed [false oder array]
  	 */
  	function getRightForID($fkid,$mod) {
  	
		$id			= $this->bean->getId();
		$augid		= $this->bean->getUgid();
		$flagtype	= $this->bean->getFlagtype();
		$right		= false;
		
		for($i=0;!empty($id[$mod][$fkid][$i]);$i++) {
		
			$ugid = $augid[$mod][$fkid][$i];
			
			if($flagtype[$mod][$fkid][$i] == 'gid') {
			
				$teamuser = $this->oTeam->getTeamUserIds($augid[$mod][$fkid][$i]);
				
				for($j=0;!empty($teamuser[$j]);$j++) {
				
					$right[$teamuser[$j]]['id']			= $id[$mod][$fkid][$i];
					$right[$teamuser[$j]]['ugid']		= $teamuser[$j];
					$right[$teamuser[$j]]['fkid']		= $fkid;
					$right[$teamuser[$j]]['flagtree']	= $mod;
						
				}
				
			}
			else {
			
				$right[$ugid]['id']			= $id[$mod][$fkid][$i];
				$right[$ugid]['ugid']		= $augid[$mod][$fkid][$i];
				$right[$ugid]['fkid']		= $fkid;
				$right[$ugid]['flagtree']	= $mod;	
			}
			
		}

  		return $right;
  		
  	}
  	/**
   	 * Diese Methode fragt welche Datensaetze fuer den uebergebenen user personalisiert wurden.
   	 * Wenn es welche gibt, gibt sie ein array, sonst false zurueck.
  	 * @param int $uid
  	 * @param String $mod
  	 * @return mixed [false oder array]
  	 */
  	 
  	function getRightForUID($uid,$mod) {
  	
		$id			= $this->bean->getId();
		$augid		= $this->bean->getUgid();
		$flagtype	= $this->bean->getFlagtype();
		$right		= false;
		
		foreach($id[$mod] as $fkid => $vals) {
			
			$rights = $this->getRightForID($fkid,$mod);
			foreach($rights as $userid => $val) {
				if($uid == $userid) $right[$fkid] = $val;
			}
			
		}
		
  		return $right;
  		
  	}

  	/**
  	 * Dies ist die zentrale Methode zum Setzen und aendern der Rechte fuer die Teams bzw. Nutzer.
  	 * Ob die Rechte geaendert oder hinzugefuegt werden muessen, entscheidet diese Methode fuer sich selbst.
  	 * @param array $right - Die zuverwaltenden Rechte
  	 * @param int $fkid - Der Key auf den die Rechte hinzugefuegt oder geaendert werden sollen
  	 * @param int $autor - Die Id des Autors
  	 * @param String $ug - Parameter zum festlegen ob ein Team oder ein Nutzer gesetzt werden soll (uid|gid)
  	 * @param String $mod - Modul zu welchem dieses Recht hinzugefuegt wird
  	 * @param Boolean $bWithAutor - soll der Autor automatisch mit eingefuegt werden
  	 * @return void
  	 */
  	function setRightForFolder($right,$fkid,$autor='',$ug='gid',$mod,$bWithAutor=true) {

		$sql = "SELECT id,ug_id,fk_id,flag_tree,flag_show FROM ".$this->table." WHERE fk_id='$fkid' AND flag_tree='$mod' AND flag_type='$ug'";
			
		$this->oDb->query($sql);
		
		for($i=0;$row = $this->oDb->fetch_object();$i++) {
			
			$userrights[$row->ug_id]['id'] = $row->id;
			$userrights[$row->ug_id]['ugid'] = $row->ug_id;
			$userrights[$row->ug_id]['fkid'] = $row->fk_id;
			$userrights[$row->ug_id]['flagtree'] = $row->flag_tree;
			$userrights[$row->ug_id]['flagshow'] = $row->flag_show;
			
		}
			
		$this->oDb->free_result();
		
		if(!empty($autor) && $bWithAutor) {

			$right[$autor] = 1;
			
		}
		if(is_array($right) && $ug == 'uid') {
		
			foreach($right as $id => $show) {
				
				if(empty($userrights[$id]['ugid'])) {
			
  					$insert = "INSERT INTO ".$this->table." (flag_type,ug_id,fk_id,flag_show,flag_tree) VALUES";
  					$insert .= "('".$ug."','".$id."','$fkid','1','".$mod."')";
					$this->oDb->query($insert);
				
					$userrights[$id]['id'] = $this->oDb->insert_id();
					$userrights[$id]['ugid'] = $id;
					$userrights[$id]['fkid'] = $fkid;
					$userrights[$id]['flagtree'] = $mod;
					$userrights[$id]['flagshow'] = 1;
				}
			}
		}
		
		if(is_array($userrights)) {
  			
  			if($ug == 'gid') { $userrights = $right; } 
  			
	  		foreach ($userrights as $key => $val) {
  			
  				if($right[$key] == 0) {

  					$show = 0;
  					
  				}
  				else {
  				
  					$show = 1;
  					
  				}
  				
  				$update = "UPDATE ".$this->table." SET flag_show='$show' WHERE fk_id='$fkid' AND ug_id='$key' AND flag_type='$ug'";

  				$this->oDb->query($update);
  						
  			}
  		
  		}
  		else {
  			foreach($right as $key => $val) {
  				
  				$insert = "INSERT INTO ".$this->table." (flag_type,ug_id,fk_id,flag_show,flag_tree) VALUES";
  				$insert .= "('".$ug."','$key','$fkid','$val','".$mod."')";
  				
  				$this->oDb->query($insert);
  			
  			}
  		}
  		$this->initialize();
  	}
  	
  	/**
  	 * Loescht die Rechte fuer eine FKID in abhaengigkeit mit dem uebergebenen Modul
  	 * @param int $fkid
  	 * @param String $mod
  	 * @return Boolean
  	 */
  	function delRightForTree($fkid,$mod) {
  		$sql = "DELETE FROM ".$this->table." WHERE fk_id='$fkid' AND flag_tree='".$mod."'";
  		return $this->oDb->query($sql);
  	}
  	
  	/**
  	 * ueberprueft ob ueberhaupt ein Nutzer bereits in einer FKid vorhanden ist.
  	 * @param int $fkid
  	 * @return Boolean
  	 */
  	function isInAuthTable($fkid) {
  	
  		$sql = "SELECT id FROM ".$this->table." WHERE fk_id='$fkid' AND flag_type='uid'";
  		$this->oDb->query($sql);
  		
  		if($this->oDb->num_rows() == 0) {
  			return false;
  		}
  		else {
			return true;  		
  		}
  	}

  	/**
  	 * Liefert alle User/Teams die valid sind fuer diese FKid
  	 * @param int $fkid - Id des Originaldatensatzen
  	 * @param String $ug - User (uid) oder Team (gid)
  	 * @param String $mod - Modul
  	 * @return array
  	 */
  	function getUserGroupInfos($fkid,$ug='uid',$mod) {
		$id			= $this->bean->getId();
		$augid		= $this->bean->getUgid();
		$flagtype	= $this->bean->getFlagtype();
		for($i=0;!empty($id[$mod][$fkid][$i]);$i++) {
			$ugid = $augid[$mod][$fkid][$i];
			if($flagtype[$mod][$fkid][$i] == $ug) {
				$right[$ugid]['id']			= $id[$mod][$fkid][$i];
				$right[$ugid]['ugid']		= $augid[$mod][$fkid][$i];
				$right[$ugid]['fkid']		= $fkid;
				$right[$ugid]['flagtree']	= $mod;
			}
		}
		return $right;	
  	}

  	/**
  	 * Diese Methode ruft das HTML - Interface fuer die Datenbankgestuetzte Accessverwaltung
  	 * @param array $aData
  	 * @param String $syslang
  	 * @param int $accessid - FKID
  	 * @param int $flagopen
  	 * @param array $aMSG
  	 * @param array $param - Parameter fuer die Ausgabe (HTML Tabellen angaben etc.)
  	 * @param String $mod - Modul
  	 * @param Boolean $bWithOpenClose
  	 * @param Boolean $bWithNotify
  	 * @return String
  	 */
  	function showTeamAccess($aData,$syslang,$accessid,$flagopen,$aMSG,$param,$mod,$bWithOpenClose=false,$bWithNotifiy=false) {
		$aGroupAccessRules = $this->getUserGroupInfos($accessid,'gid',$mod);
		$aUserAccessRules = $this->getUserGroupInfos($accessid,'uid',$mod);
		return $this->showTeamAccessHTML($aData,$syslang,$accessid,$flagopen,$aMSG,$param,$aUserAccessRules,$aGroupAccessRules,$bWithOpenClose,$bWithNotifiy);
  	}

  	/**
  	 * Diese Methode stellt das HTML - Interface fuer die Accessverwaltung bereit und schreibt die dazu noetigen JavaScripts
  	 * @param array $aData
  	 * @param String $syslang
  	 * @param int $accessid - FKID
  	 * @param int $flagopen
  	 * @param array $aMSG
  	 * @param array $param - Parameter fuer die Ausgabe (HTML Tabellen angaben etc.)
  	 * @param array $aUserAccessRules
  	 * @param array $aGroupAccessRules
  	 * @param String $mod - Modul
  	 * @param Boolean $bWithOpenClose
  	 * @param Boolean $bWithNotify
  	 * @return String
  	 */
  	function showTeamAccessHTML($aData,$syslang,$accessid,$flagopen,$aMSG,$param,$aUserAccessRules,$aGroupAccessRules,$bWithOpenClose=false,$bWithNotifiy=false) {
  		
  		$userteam = $aData['viewug'];
  		$eol = "\n";

  		$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
		$oForm->bEditMode = $this->bEditMode;
		$oTeam =& new Team($this->oDb);
		// USER
		$oUser =& new User($this->oDb);
		$aUsergroup = $oTeam->getAllTeamNames(true); // params: [$bWithUsers=false] 
		$aUsers = $oUser->getAllUserNames();
		if(is_array($aGroupAccessRules)) {
			$aGroups = array();
			foreach($aGroupAccessRules as $gid => $val) {
				$aGroups[] = $gid;
			}
			reset($aGroupAccessRules);
			$aGroupRelations = $oTeam->getTeamFlagDeleted($aGroups);
			if(in_array(1,$aGroups)) {
				$gmsg = true;
			}
		}
		else {
			$gmsg = false;
		}
		if(is_array($aUserAccessRules)) {
			foreach($aUserAccessRules as $uid => $val) {
				$aUserdata = $oUser->getUserdata($uid);
				if($aUserdata['flag_deleted'] == 1) {
					$umsg = true;	
				}	
			}
			reset($aUserAccessRules);
		}
		else {
			$umsg = false;
		}
		
		if(!$oForm->bEditMode) {
		
			if(is_array($aGroupAccessRules) || is_array($aUserAccessRules)) {
			
				$sText .= '<table width="100%" border="'.$param['border'].'" cellspacing="'.$param['cellspacing'].'" cellpadding="'.$param['cellpadding'].'" class="'.$param['class'].'">'.$eol;
				$sText .= '<tr valign="top">'.$eol;
				###$sText .= '<td><p><b>'.$aMSG['form']['access'][$syslang].'</b></p></td>'.$eol;
				if(isset($param['third'])) $cols = 2; else $cols = 1;
				$sText .= '<td class="text" colspan="'.$cols.'" valign="top">'.$eol;
				$excludeduser = array();
				$aParticipants = array();
				if(is_array($aGroupAccessRules)) {
					foreach($aGroupAccessRules as $g_id => $groupdata) {
			
						$aParticipants[] = '<span class="text">'.$aUsergroup[$g_id]."</span>&nbsp;&nbsp;<br>\n";
						$excludeduser = array_merge($excludeduser,$oTeam->getTeamUserIds($g_id));
						
					}
				}
				if(is_array($aUserAccessRules)) {
					
					foreach ($aUserAccessRules as $u_id => $userdata) {
					// output
						
						$aUserdata = $oUser->getUserdata($u_id);
						
						if ($aUserdata['flag_da_service'] != 1 && !in_array($u_id,$excludeduser)) {
							$aParti[] = ''.$aUsers[$u_id]."&nbsp;&nbsp;<br>\n";
						}
					}
					if(is_array($aParti)) {
					
						sort($aParti);
						$aParticipants = array_merge($aParticipants,$aParti);
					}
				}

				$sText .= implode('', $aParticipants);
				$sText .= '</td></tr></table>';
			}
		}
		else {
			if($umsg || $gmsg) {
				
				if($umsg && $gmsg) $msg = $aMSG['form']['relations'][$syslang];
				if($umsg) $msg = $aMSG['form']['userrelation'][$syslang];
				if($gmsg) $msg = $aMSG['form']['grouprelation'][$syslang];
				
				$sText .= '<script language="JavaScript" type="text/javascript">'.$eol;
				$sText .= '		alert(\''.$msg.'\')';
				$sText .= '</script>'.$eol;
  
			}
		if($oTeam->countTeams() != 0) {
			if($userteam == 'teams') {
			
				$styleteams = "display:block";
				$bTeams = true;
			
				$styleuser = "display:none";
			}
			else {
			
				$styleteams = "display:none";
				$bTeams = true;
			
				$styleuser = "display:block";
			}
		}
		else {
			$styleteams = "display:none";
			$styleuser = "display:block";
			$bTeams = false;
		}
		if ($bWithOpenClose) {
			$sText .= '<span class="text">'.$eol;
			$sChecked = ($flagopen == 1) ? " checked" : "";
			$sText .= '<input type="radio" name="aData[flag_open]" id="flag_open_1" value="1" class="radiocheckbox"'.$sChecked.' onClick="document.getElementById(\'accessDiv\').style.display=\'none\';"><label for="flag_open_1" class="text">'.$aMSG['access']['open'][$syslang].'</label>'.$eol;
			
			$sChecked = ($flagopen == 0) ? " checked" : "";
			$sText .= '<input type="radio" name="aData[flag_open]" id="flag_open_0" value="0" class="radiocheckbox"'.$sChecked.' onClick="document.getElementById(\'accessDiv\').style.display=\'block\';"><label for="flag_open_0" class="text">'.$aMSG['access']['closed'][$syslang].'</label>'.$eol;
			
			$sText .= '</span>'.HR.$eol;
		}
		$sViewMode = ($flagopen == 0) ? "block" : "none";
		
		$sText .= '<div id="accessDiv" style="display: '.$sViewMode.';">'.$eol;
		$sText .= '<table width="100%" border="'.$param['border'].'" cellspacing="'.$param['cellspacing'].'" cellpadding="'.$param['cellpadding'].'" class="'.$param['class'].'">'.$eol;
		$sText .= '<colgroup><col width="'.$param['first'].'%"><col width="'.$param['second'].'%"></colgroup>'.$eol;
		$sText .= '<tr valign="top">'.$eol;
		$sText .= '<td class="text">'.$eol;
		$sText .= $oForm->hidden("group"); // params: $sFieldname[,$sDefault=''][,$sExtra=''] 
		$sText .= $oForm->hidden("user"); // params: $sFieldname[,$sDefault=''][,$sExtra=''] 
		$sText .= $oForm->hidden("viewug"); // params: $sFieldname[,$sDefault=''][,$sExtra=''] 
	
		// TEAMS Checkboxen
	 	if($bTeams) {

			$sText .= '<a href="#" id="linkTeams" class="cnavi" onclick="switchAccessLayer(\'teams\');document.forms[\'editForm\'].elements[\'viewug\'].value=\'teams\';" onfocus="this.blur()" title="'.$aMSG['topnavi']['teams'][$syslang].'">'.$aMSG['topnavi']['teams'][$syslang].'</a> | <a href="#" id="linkUser" class="cnavi" onclick="switchAccessLayer(\'user\');document.forms[\'editForm\'].elements[\'viewug\'].value=\'users\';" onfocus="this.blur()" title="'.$aMSG['topnavi']['users'][$syslang].'">'.$aMSG['topnavi']['users'][$syslang].'</a><br><br>'.$eol;
			$sText .= '<div id="DivTeams" style="'.$styleteams.'">'.$eol;
			$sText .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabelle"><tr valign="top"><td nowrap>'.$eol;
			
			$aCheckedTeams = array();
			$i	= 0; // zaehler fuer mehrspaltigkeit
			$num = count($aUsergroup);
			
			foreach($aUsergroup as $key => $val) {
			
				$checkbox = '<input type="checkbox"';
				if(is_array($aGroupAccessRules[$key])) {
			
					$checkbox .= ' checked';
					$aCheckedTeams[] = $key;
			
				}
				$checkbox .= ' value="'.$key.'" id="group'.$key.'" class="radiocheckbox" name="group" onclick="formatHidden(\'group\');checkUsers(\'edit\');"> ';
				$checkbox .= '<label for="group'.$key.'" class="text">'.$val.'</label>&nbsp;&nbsp;<br>';
				
				$i++; // ggf. zweispaltig (bei mehr als 5 Usern)
				if ($num > 5 && $i == ceil($num / 2)) { $checkbox .= '</td><td nowrap>'; }
				
				$sText .= $checkbox.$eol;
			}
	
			$sText .= '</td></tr></table>'.$eol;
			$sText .= '</div>'.$eol;
		}

		// USER Checkboxen
	 	$sText .= '<div id="DivUser" style="'.$styleuser.'">'.$eol;
		$sText .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabelle"><tr valign="top"><td nowrap>'.$eol;
	
		$i	= 0; // zaehler fuer mehrspaltigkeit
		$num = count($aUsers);
		
		foreach($aUsers as $key => $val) {
				
			$checkbox = '<input type="checkbox"';
			
			if(is_array($aUserAccessRules[$key]) && !empty($accessid)) {
				
				$checkbox .= ' checked';
				
			}
			$checkbox .= ' value="'.$key.'" id="user'.$key.'" class="radiocheckbox" name="user" onclick="formatHidden(\'user\')"> ';
			$checkbox .= '<label for="user'.$key.'" class="text">'.$val.'</label>&nbsp;&nbsp;<br>';
			
			$i++; // ggf. zweispaltig (bei mehr als 5 Usern)
			if ($num > 5 && $i == ceil($num / 2)) { $checkbox .= '</td><td nowrap>'; }
			
			$sText .= $checkbox.$eol;
		}
		$sText .= '</td></tr></table>'.$eol;
		$sText .= '</div>'.$eol;
		
		if ($oForm->bEditMode == true && $bWithNotifiy) {
		$sText .= '</td>'.$eol;
		$sText .= '<td width="'.$param['second'].'" align="right">'.$eol;
				$sText .= '<a href="javascript:notifyUsers();" title="'.$aMSG['btn']['notify_users'][$syslang].'" class="textbtn" style="width: 193px"><img src="'.$this->aENV['path']['pix']['http'].'btn_sendmail.gif" alt="'.$aMSG['btn']['notify_users'][$syslang].'">| '.$aMSG['btn']['notify_users'][$syslang].'&nbsp;</a>';
		}
		$sText .= '</td>'.$eol;
		$sText .= '</tr>'.$eol;
		$sText .= '</table>'.$eol;
		$sText .= '</div>'.$eol;
		
		$sText .= '<script language="JavaScript" type="text/javascript">'.$eol;
		$sText .= '<!--'.$eol;
	
		if ($bTeams) { $sText .= 'var bTeams = true;'.$eol; }
		else { $sText .= 'var bTeams = false;'.$eol; }
		$sText .= 'var aAllSelectedUsers = new Array();	// speichert alle gecheckten User-Ids fuer externe Verwendung, z.B. in "cal_calender_detail.php (Notify)'.$eol;
		$sText .= $eol;
		
		$sText .= 'function formatHidden(field) {	// field bezeichnet das zu aendernde Hidden-Field'.$eol;
		$sText .= '	with (document.forms["editForm"]) {'.$eol;
		$sText .= '		var str = "";'.$eol;
		$sText .= '		var hiddenfield = elements["aData[" + field + "]"];'.$eol; // hiddenfield
		$sText .= '		var cbx = elements[field];	// Checkbox-Gruppe (name="group" oder name="user")'.$eol.$eol; // checkbox(en)
		$sText .= '		if (field == "user") { aAllSelectedUsers = new Array();	}'.$eol;
		$sText .= '		if (cbx.length > 0) {	// mehrere checkboxen (=array)'.$eol;
		$sText .= '			for (var i=0; i<cbx.length; i++) {'.$eol;
		$sText .= '				if (cbx[i].checked == true) {'.$eol; // wenn eine gecheckt ist:
		$sText .= '					if (field == "user") {	// Fall: User-Checkbox (user)'.$eol;
		$sText .= '						aAllSelectedUsers.push(cbx[i].value);'.$eol;
		$sText .= '						groupMember = false;	// default'.$eol;
		$sText .= '						// ab hier pruefen, ob gecheckter User einem Team angehoert'.$eol;
		$sText .= '						var teamCbx = elements["group"];'.$eol;
		$sText .= '						for (var j=0; j<teamCbx.length; j++) {'.$eol;	// alle Team-Checkboxen ueberpruefen, ob gecheckt
		$sText .= '							if (teamCbx[j].checked == true) {'.$eol;	// wenn gecheckt, alle zugehoerigen User auch checken
		$sText .= '								teamId = teamCbx[j].value;'.$eol;
		$sText .= '								for (var c=0; c<aTeamUsers[teamId].length; c++) {'.$eol;
		$sText .= '									if (aTeamUsers[teamId][c] == cbx[i].value) {'.$eol;
		$sText .= '										groupMember = true;'.$eol;
		$sText .= '										/* Frage:'.$eol.'Muesste man hier den User gegebenenfalls aus Array aCheckedUsers loeschen?'.$eol.'Beides hat Nachteile, haengt von Usability ab */'.$eol;
		$sText .= '									}'.$eol;
		$sText .= '								}'.$eol;	
		$sText .= '							}'.$eol;
		$sText .= '						}'.$eol;
		$sText .= '						// wenn keinem Team angehoerig, mit ins User-Hidden-Field aufnehmen'.$eol;
		$sText .= '						if (groupMember == false) { str += "," + cbx[i].value; }'.$eol; // value = kommagetrennte IDs
		$sText .= '					}'.$eol;
		$sText .= '					// Fall: Team-Checkbox (group)'.$eol;
		$sText .= '					else { str += "," + cbx[i].value; }'.$eol; // value = kommagetrennte IDs
		$sText .= '				}'.$eol;
		$sText .= '			}'.$eol;
		$sText .= '			strlen = str.length; '.$eol;
		$sText .= '			if (strlen > 0) { str = str.substr(1, strlen); }'.$eol; // erstes komma entfernen
		$sText .= '		} else {	// nur eine Checkbox (kein Array)'.$eol;
		$sText .= '			if (cbx.checked == true) { str = cbx.value; } '.$eol;// value = ID
		$sText .= '		}'.$eol;
		$sText .= '		hiddenfield.value = str;'.$eol;// schreibe value ins hidden-field
		$sText .= '		if (typeof checkFlagPrivate == "function") { checkFlagPrivate(); }'.$eol;
		$sText .= '	}'.$eol;
		$sText .= '}'.$eol.$eol;
	
		$sText .= 'var aTeamUsers = new Array();'.$eol;

		foreach ($oTeam->aTeam as $key => $val) {
			$sText .= 'aTeamUsers['.$key.'] = new Array();'.$eol;
			$aTeamUsers = explode(",", $val['user']);
			for ($i=0,$j=0; $i<count($aTeamUsers); $i++) {
				if(!empty($aUsers[$aTeamUsers[$i]])) {
					if (!$aTeamUsers[$i]) $aTeamUsers[$i] = 0; // fallback, wenn einer usergroup keine user zugeordnet sind!
					$sText .= 'aTeamUsers['.$key.']['.$j.'] = '.$aTeamUsers[$i].';'.$eol;
					$j++;
				}
			}
		}
		$sText .= $eol;
	
		$sText .= 'var aCheckedUsers = new Array();	// Array mit gecheckten Usern, die NICHT den bereits ausgewaehlten Teams angehoeren'.$eol;
		$nCounter = 0;
		foreach($aUsers as $key => $val) {
			if (is_array($aUserAccessRules[$key])) {
				$sText .= 'aCheckedUsers['.$nCounter.'] = '.$key.';'.$eol;
				$nCounter++;
			}
		}
		$sText .= $eol;
	
		$sText .= 'function checkUsers(mode) {'.$eol;
		$sText .= '	if (bTeams == true) {'.$eol;
		$sText .= '		with (document.forms["editForm"]) {'.$eol;
		$sText .= '			if (mode == "edit") {	// braucht bei Initialisierung nicht ausgefuehrt zu werden'.$eol;
		$sText .= '				var cbx = elements["user"];'.$eol;
		$sText .= '				for (var i=0; i<cbx.length; i++) {	// alle User-Checkboxen ausschalten'.$eol;
		$sText .= '					uncheck = true;	// default'.$eol;
		$sText .= '					for (j=0; j<aCheckedUsers.length; j++) {	// prufen, ob User nicht manuell gecheckt wurde'.$eol;
		$sText .= '						if (cbx[i].value == aCheckedUsers[j]) { uncheck = false; }'.$eol;
		$sText .= '					}'.$eol;
		$sText .= '					if (uncheck == true) { cbx[i].checked = false; }'.$eol;
		$sText .= '					cbx[i].disabled = false;'.$eol;
		$sText .= '				}'.$eol;
		$sText .= '			}'.$eol.$eol;
		$sText .= '			var cbx = elements["group"];'.$eol;
		$sText .= '			for (var i=0; i<cbx.length; i++) {	// alle Team-Checkboxen ueberpruefen, ob gecheckt'.$eol;
		$sText .= '				if (cbx[i].checked == true) {	// wenn gecheckt, alle zugehoerigen User auch checken'.$eol;
		$sText .= '					teamId = cbx[i].value'.$eol;
		$sText .= '					for (var j=0; j<aTeamUsers[teamId].length; j++) {'.$eol;
		$sText .= '						fieldId = "user" + aTeamUsers[teamId][j];'.$eol;
		$sText .= '						field = document.getElementById(fieldId);'.$eol;
		$sText .= '						field.checked = true;'.$eol;
		$sText .= '						field.disabled = true;'.$eol;
		$sText .= '					}'.$eol;
		$sText .= '				}'.$eol;
		$sText .= '			}'.$eol;
		$sText .= '		}'.$eol;
		$sText .= '	}'.$eol;
		$sText .= '	formatHidden("user");'.$eol;
		$sText .= '}'.$eol.$eol;
	
		$sText .= 'function switchAccessLayer(show) {'.$eol;
		$sText .= '	if (bTeams == true) {'.$eol;
		$sText .= '		if (show == "teams") {'.$eol;
		$sText .= '			getLayer("DivTeams").display = "block";'.$eol;
		$sText .= '			getLayer("DivUser").display = "none";'.$eol;
		$sText .= '			document.getElementById("linkTeams").className = "cnavihi";'.$eol;
		$sText .= '			document.getElementById("linkUser").className = "cnavi";'.$eol;
		$sText .= '		} else {'.$eol;
		$sText .= '			getLayer("DivTeams").display = "none";'.$eol;
		$sText .= '			getLayer("DivUser").display = "block";'.$eol;
		$sText .= '			document.getElementById("linkTeams").className = "cnavi";'.$eol;
		$sText .= '			document.getElementById("linkUser").className = "cnavihi";'.$eol;
		$sText .= '		}'.$eol;
		$sText .= '	}'.$eol;
		$sText .= '}'.$eol.$eol;
	 
	 	$sText .= '// init'.$eol;
		// beim Laden der Seite die richtigen Layer einblenden (je nachdem ob es Teams gibt oder nicht)
		if ($userteam == 'users') { $sText .= 'switchAccessLayer("user");'.$eol; }
		else {
			if ($oTeam->countTeams() > 0) { $sText .= 'switchAccessLayer("teams");'.$eol; }
		}
		$sText .= 'if (bTeams == true) { checkUsers("init"); }'.$eol;
		$sText .= 'else { formatHidden("user"); }'.$eol;
		if (count($aCheckedTeams) > 0) {
			$sText .= 'document.getElementById("group").value = "'.implode(",", $aCheckedTeams).'";	// bei Vorbelegung Hidden-Field "group" fuellen'.$eol;
		}
		$sText .= '//-->'.$eol;
		$sText .= '</script>'.$eol;
			}
		return $sText;
  	}
  	
  	/**
  	 * Abstraktion der Methode setRightForFolder(). Diese Methode hilft dabei die Rechte zu setzen und ist in der Regel zu Empfehlen.
  	 * @param String $group
  	 * @param String $user
  	 * @param int $accessid - FKID
  	 * @param String $module - Modul
  	 * @param Boolean $bWithAutor
  	 * @param int $userid
  	 * @return void
  	 */
	function addRights($group, $user, $accessid,$module='memo',$bWithAutor = false,$userid='') {

	// TEAM
		$oTeam =& new Team($this->oDb);
		$aUsergroup = $oTeam->getAllTeamNames(); // params: [$bWithUsers=false] 

		if(!empty($group)) {
			
			$groups = explode(',',$group);
			unset($group);
			for($i=0;!empty($groups[$i]);$i++) {
				$group[$groups[$i]] = 1;
			}
		}
		else {
			$groups = array();
		}

		foreach($aUsergroup as $key => $val) {
			if(!in_array($key,$groups)) {
				$group[$key] = 0;	
			}	
		}
		
		if(!empty($group)) {
			$this->setRightForFolder($group,$accessid,'','gid',$module);
		}
		// geht es hier um User?
		if(!empty($user)) {
			
			$users = explode(',',$user);
			unset($user);
			
			for($i=0;!empty($users[$i]);$i++) {
			
				$user[$users[$i]] = 1;
				
			}
			
			$uright = $this->getUserGroupInfos($accessid,'uid',$module);
			$gright = $this->getUserGroupInfos($accessid,'gid',$module);
			$users = array();
			// Wenn ein Datensatz bereits vorhanden ist...
			if(is_array($uright) && is_array($gright)) {
	
				 foreach ($uright as $uid => $val) {
					
					$ret = '0';
					if(is_array($gright)) {
						foreach($gright as $gid => $values) {
							if($oTeam->isUserInTeam($gid,$uid)) {
								$ret = '1';
							}
						}
					}
					if($ret == '1') {
						$user[$uid] = $uid;
					}
				 }
						
			
				if($bWithAutor && count($user) == 0) {
					$this->setRightForFolder(array(),$accessid,$userid,'uid',$module,true);
				}
				else {
					$this->setRightForFolder($user,$accessid,'','uid',$module,false);
				}
			}
			else {
				if($bWithAutor && count($user) == 0) {
					$this->setRightForFolder(array(),$accessid,$userid,'uid',$module,true);
				}
				else {
					$this->setRightForFolder($user,$accessid,'','uid',$module,false);
				}
			}
		}
		// hier nicht mehr ausschliesslich...
		else {
				
			$uright = $this->getUserGroupInfos($accessid,'uid',$module);
			$gright = $this->getUserGroupInfos($accessid,'gid',$module);
					
			if(is_array($uright)) {
				
				$users = array();
				foreach ($uright as $uid => $val) {
					
					$ret = '0';
					if(is_array($gright)) {
						foreach($gright as $gid => $values) {
							if($oTeam->isUserInTeam($gid,$uid)) {
								$ret = '1';
							}
						}
					}
					if($ret == '1') {
						$users[$uid] = $uid;
					}
				}
				if($bWithAutor && count($users) == 0) {
					$this->setRightForFolder(array(),$accessid,$userid,'uid',$module,true);
				}
				else {
					$this->setRightForFolder($users,$accessid,'','uid',$module,false);
				}
			}
			else {
				if($bWithAutor) {
					$this->setRightForFolder(array(),$accessid,$userid,'uid',$module,$bWithAutor);
				}
			}
		}
		
		
	}
	
	/**
	 * Diese Methode ruft die Methoden auf, die beim Loeschen ausgefuehrt werden sollen. In diesem Fall nur die Loeschen der Datensaetze Methode
	 * @param int $fkid
	 * @param String $mod - Modul
	 * @return Boolean
	 */
	function onDeleteAction($fkid,$mod) {
		return $this->delRightForTree($fkid,$mod);
	}
	
	/**
	 * Diese Methode ermittelt alle Team, User in einem Array mit Namen. Doppelte werden automatisch ausgeschlossen
	 * @param object $oTeam
	 * @param object $oUser
	 * @param String $mod
	 * @param int $fkid
	 * @return array
	 */
	function getParticipants(&$oTeam,&$oUser,$mod,$fkid,$asAssozArray=false) {
	// Ausgabe der Teams und User (wenn User in einem Team sind, werden sie nicht mehr zusaetzlich als Einzelpersonen angezeigt)
		$participants = array();
		$excludeduser = array();
		$gright = $this->getUserGroupInfos($fkid,'gid',$mod);
		$parti = array();
		if(is_array($gright)) {
					
			reset($gright);
			foreach($gright as $gid => $vals) {
					
				$aTeamdata = $oTeam->getTeamNames($gid);
				if(!$asAssozArray) {
						
					$parti[] = $aTeamdata[$gid];
					$excludeduser = array_merge($oTeam->getTeamUserIds($gid),$excludeduser);
				}
				else {
					
					$parti['gid'][$gid] = $aTeamdata[$gid];
					
				}					
			}
					
			if(is_array($parti) && !$asAssozArray) {
				sort($parti);
				$participants = array_merge($participants,$parti);
			}
			else {
				$participants = array_merge($participants,$parti);
			}
			unset($parti);
		}
			
		$uright = $this->getUserGroupInfos($fkid,'uid',$mod);
							
		if(is_array($uright)) {
					
			reset($uright);
			foreach($uright as $uid => $vals) {
				$aUserdata = $oUser->getUserdata($uid);
				if($aUserdata['flag_da_service'] == 0 && !in_array($uid,$excludeduser)) {
					
					if(!$asAssozArray) {
						$parti[] = $aUserdata['name'];
					}
					else {
						$parti['uid'][$uid] = $aUserdata['name'];
					}						
				}
						
			}
					
			if(is_array($parti) && !$asAssozArray) {
				sort($parti);
				$participants = array_merge($participants,$parti);
			}
			else {
				$participants = array_merge($participants,$parti);
			}
			unset($parti);
		}
		return $participants;	
	}
  }
?>