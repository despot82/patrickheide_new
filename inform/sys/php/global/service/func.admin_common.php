<?php
/**
* globale Funktionsbibliothek fuer ADMIN-Seiten (Overview- UND Detailseiten)
*
* @access	public
* @package  Content
* @author   Andy Fehn <af@design-aspekt.com>
* @version  2.7 / 2006-01-18 [NEU: "checkBrowser()"]
*/

/*	----------------------------------------------------------------------------
		Funktionen dieser Funktionsbibliothek:
		----------------------------------------------------------------------------
		function stripForFlash($str)
		function checkBrowser()
		function setBenchmark()
		function getBenchmark($bmsek,$bsek=0)
		function sendSystemErrorMail($sMsg)
		function sendUserErrorMail($sMsg)
		----------------------------------------------------------------------------
		HISTORY:
		2.8 / 2006-09-11 [NEU: "sendUserErrorMail()"]
		2.7 / 2006-01-18 [NEU: "checkBrowser()"]
		2.6 / 2005-04-06 ["is_str_in_field()" GELOESCHT!]
		2.5 / 2005-03-14 ["showUserNames()" GELOESCHT. Ist jetzt in "class.user.php"!]
		2.4 / 2004-11-03 [NEU: "sendSystemErrorMail()"]
		2.3 / 2004-10-28 ["write_xml_linkStructure()" GELOESCHT. Ist jetzt in "class.cms_content.php"!]
		2.2 / 2004-09-16 [NEU: "write_xml_linkStructure()"]
		2.1 / 2004-08-02 ["write_navi_array()" GELOESCHT. Ist jetzt in Navi-Klasse -> NICHT MEHR ABWAERTSKOMPATIBEL!!!]
		2.0 / 2004-04-23 [$aENV in $aENV geaendert -> NICHT MEHR ABWAERTSKOMPATIBEL!!!]
		1.0 / 2003-05-26
*/


# -------------------------------------------------------------------------------- FLASH

/**
* Formatiere string fuer Flash (z.B. UTF-8-encodieren und url-encodieren usw.).
*
* @access   public
* @param	string	$str
* @return	encoded string
*/
	function stripForFlash($str) {
		#$str = strip_tags($str);
		#$str = htmlspecialchars($str);
		$str = preg_replace('/\r\n|\r|\n/', '<br>', $str);
		global $aENV;
		$str_search = array("Œ", "œ", "Æ", "æ", "–", "—", "­", "·", "€", "„", "“", "”");
		$str_replace = array("&OElig;", "&oelig;", "&aelig", "&AElig;", "&ndash;", "&mdash;", "&shy;", "&middot;", "&euro;", "&#132;", "&#147;", "&#148;");
		$str = str_replace($str_search, $str_replace, $str);
		if (!isset($aENV['charset']) || $aENV['charset'] != 'UTF-8') {
			$str = utf8_encode($str);
		}
		$str = urlencode($str);
		return $str;
	}

/**
* Formatiere string fuer Flash (z.B. UTF-8-encodieren und url-encodieren usw.).
*
* @access   public
* @param	string	$str
* @return	encoded string
*/
	function prepareHtmlTagsForFlashEditor($str) {
		if (empty($str)) return;
		// Sonderzeichen-Behandlung damit Flash unbekannte Tags NICHT entfernt
		$str_search = array("&lt;", "<", "&gt;", ">", "&Ouml;", "&ouml;", "&Uuml;", "&uuml;", "&Auml;", "&auml;", "&szlig;");
		$str_replace = array("&amp;lt;", "&lt;", "&amp;gt;", "&gt;", "Ö", "ö", "Ü", "ü", "Ä", "ä", "ß");
		$str = str_replace($str_search, $str_replace, $str);
		// die Flash bekannten Tags wieder zuruckwandeln...
		$str = preg_replace("/&lt;(\/?)(b|u|i|br|sub|sup|ul|li)&gt;/i", "<\$1$2>", $str); // erst mal ohne font (was auch moeglich waere...)
		// Links mit "target=self" oder "kein target"
		$str = preg_replace("/\&lt;a href=\"([^\"]*)\"( target=\"\")?&gt;(.*?)\&lt;\/a\&gt;/i", "<a href=\"$1\" target=\"\">$3</a>", $str);
		// Links mit "target=[irgendwas]"
		$str = preg_replace("/\&lt;a href=\"([^\"]*)\"( target=\"([^\"]*)\")&gt;(.*?)\&lt;\/a&gt;/i", "<a href=\"$1\"$2>$4</a>", $str);

		return $str;
	}


# -------------------------------------------------------------------------------- COMMON

/**
* checkt den Browser des aktuellen Benutzers und leitet ggf. zur disallow-seite.
* 
* @access	public
* @global	object	$oBrowser	Browser-Objekt
* @global	array	$aENV		gloabales Environment-Array
* @return	void()
*/
	function checkBrowser() {
		// vars
		global $aENV, $oBrowser;
		
		$isValid = false;
		if ($oBrowser->isWin() && $oBrowser->isIE() && $oBrowser->getVersion() > 5) {
			$isValid = true; // identify Win IE 6+
		}
		if ($oBrowser->isFirefox()) {
			$isValid = true; // identify Firefox
		}
		if ($oBrowser->isNS() && $oBrowser->getVersion() > 6) {
			$isValid = true; // identify Netscape 7+
		}
		if ($oBrowser->isMac()) { // MAC
			if ($oBrowser->isMacOSX()) { // OS X
				if ($oBrowser->isSafari() && $oBrowser->getVersion() > 1.2) {
					$isValid = true; // identify Safari 1.2+
				}
			} else { // OS 9
				if ($oBrowser->isIE() && $oBrowser->getVersion() == 5) {
					$isValid = true; // identify IE5
				}
			}
		}
		if ($oBrowser->isOpera() && $oBrowser->getVersion() > 7) {
			$isValid = true; // identify Opera 8+
		}
		if ($isValid == false) {
		// ungueltige browser ausfiltern
			header("Location: ".$aENV['page']['disallow']."?msg=oldbrowser"); exit;
		}
	}

# -------------------------------------------------------------------------------- BENCHMARK

/**
* gibt ein Array-Zurueck, das aus 2 Werten besteht, die von getBenchmark() erwartet werden.
* 
* @access	public
* @return	array	Array mit der Struktur Array(Wert1, Wert2);
*/
	function setBenchmark() {return explode(" ",microtime());}

/**
* gibt eine Array-Zurueck, das aus den beiden Werten von setBenchmark() die Laufzeit in Sekunden und Mirosekunden zwischen den beiden Funktionsaufrufen enthaelt!
* 
* @access	public
* @param	integer	$pri	Wert 1 von setBenchmark oder das komplette Array (Wert 2 ist dann hinfaellig)
* @param	integer	$sek	Wert 2 von setBenchmark
* @return	array	Array mit der Struktur Array("sec" => ..Sekunden der Laufzeit.., "micro" => Microsekunden der Laufzeit);
*/
	function getBenchmark($bmsek, $bsek=0) {
		if (is_array($bmsek)) {
			list($bmsek, $bsek) = $bmsek;
		}
		list($cmsek, $csek) = explode(" ", microtime());
		$sek = $csek - $bsek;
		$msek = (!$sek) ? ($cmsek - $bmsek) : (1 - $bmsek + $cmsek);
		if ($msek > 1) { $sek++; $msek--; }
		return Array("sec" => $sek, "micro" => round($msek*10000));
	}

# -------------------------------------------------------------------------------- SYSTEM ERROR

/**
* Versendet eine Error-Mail an design aspekt.
* 
* @access	public
* @param	string	$sMsg	Errormessage
* @return	void
*/
	function sendSystemErrorMail($sMsg) {
		global $aENV;
		$aMail = array();
		$aMail['to']		= "inform@design-aspekt.com";
		$aMail['subject']	= "System-Error: ".$_SERVER['SERVER_NAME']."";
		$aMail['body']		= "Client: ".$aENV['client_name']."\n";
		$aMail['body']		.= "Server: ".$_SERVER['SERVER_NAME']."\n";
		$aMail['body']		.= "Page: ".$aENV['PHP_SELF']."\n";
		$aMail['body']		.= "----------------------------------\n";
		$aMail['body']		.= "Message: \n".$sMsg."\n";
		@mail($aMail['to'], $aMail['subject'], $aMail['body']);
	}

/**
* Sends an Email to the client users with user admin rights and a copy to DA
* 
* @access	public
* @param	string	$sMsg	Errormessage
* @return	void
*/	
	function sendUserErrorMail($sMsg,$to) {
		global $aENV;
		
		sendSystemErrorMail($sMsg);
		$header  = 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/plain; charset=utf-8' . "\r\n";
		$aMail	= array();
		$aMail['to']		= implode(',',$to);
		$aMail['subject']	= "System-Error: ".$_SERVER['SERVER_NAME']."";
		$aMail['body']		= "Message: \n".$sMsg."\n";
		@mail($aMail['to'], $aMail['subject'], $aMail['body'],$header);
	}	

?>