<?php
/**
* globale Funktionsbibliothek fuer ADMIN-Overview-Seiten -> NICHT MEHR ABWAERTSKOMPATIBEL!!!
*
* @access	public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version  2.5 / 2006-04-12	[NEU: Zeitsteuerung bei "get_status_img()" + NEU: "compareOnlineFromToWithToday()"]
*/

/*		----------------------------------------------------------------------------
		HISTORY:
		2.5 / 2006-04-12	[NEU: Zeitsteuerung bei "get_status_img()" + NEU: "compareOnlineFromToWithToday()"]
		2.41 / 2006-02-17	[BUGFIX: Leerzeichen bei "get_dbnavi_status()"]
		2.4 / 2005-04-06	["get_mediatypes_dropdown()" in MEDIA-DB-Klasse ausgelagert!]
		2.31 / 2005-03-03	[BUGFIX bei "get_button()"]
		2.3 / 2005-02-14	[Usergroup entfernt + zusaetzlicher parameter "$bEditMode" bei "get_select()"]
		2.2 / 2004-12-03	[zusaetzlicher parameter "$sAddSqlConstraint" bei "movePrio()"]
		2.1 / 2004-11-12	[blaetterpfeile weiterentwickelt bei "get_dbnavi_links()"]
		2.05 / 2004-10-19	[BUGFIX: rundungsfehler bei "get_dbnavi_status_pages()" + "get_dbnavi_links()"]
		2.04 / 2004-09-14	["back" + "new" bei "get_button()" jetzt als verlinktes image ausgeben]
		2.03 / 2004-06-23	(NEU: reload der seite ohne move-params bei "move_prio()")
		2.02 / 2004-06-02	("get_mediatypes_dropdown()" um die moeglichkeit "anzugeigende typen einschraenken" erweitert)
		2.01 / 2004-05-03	(alle write_* entfernt und $CMS_* umbenannt)
		2.0 / 2004-04-23	(alle write_* funktionen entfernt und $CMS_ arrays umbenannt -> NICHT MEHR ABWAERTSKOMPATIBEL!!!)
		1.81 / 2004-04-15	--letzte v1 version!--
		1.0 / 2003-05-26
*/

/**
* Zeigt den Freigabestatus in Form eines Img-Tags an 
* und verlinkt ggf. das Icon mit dem uebergebenen Link. 
* (Funktion kann zur Listendarstellung von Datensaetzen einer Overview-page verwendet werden)
*
* Beispiel:
* <pre><code>
* get_status_img($aData['flag_online_de'], $aData['title_de'], $sEditPage.$sGEToptions, 'hspace="3"', $aData['flag_navi']); // params: $nStatus,$sData[,$sHref][,$sExtra][,$nNaviStatus]
* </code></pre>
*
* @access	public
* @global	array	$Userdata
* @global	array	$aENV
* @param	integer	$nStatus [ 0 fuer "nicht Freigegeben" | 1 fuer Freigegeben | ALT: 2 fuer Freigegeben und erscheint in der Navigation ]
* @param	string	$sData		(z.B. die Headline): wenn $sData leer ist, wird ein Image fuer "noch nicht angelegt" angezeigt
* @param	string	[$sHref]	komplette uri zur edit-page (inkl. get-params) - sonst kein link!	(optional)
* @param	string	[$sExtra]	kann fuer etra-angaben im img-tag verwendet werden	(optional)
* @param	string	[$nNaviStatus]	kann fuers einblenden des "btn_on_innavi.gif" in der seite "navi.php" verwendet werden	(optional)
* @param	string	[$sOnlineFrom]	Start-Datum bei Zeitsteuerung (optional)
* @param	string	[$sOnlineTo]	End-Datum bei Zeitsteuerung (optional)
* @return	html-code
*/
	function get_status_img($nStatus, $sData='', $sHref='', $sExtra='', $nNaviStatus='', $sOnlineFrom='', $sOnlineTo='') {
		// vars
		global $Userdata, $aENV, $syslang;
		$sExtra = str_replace("'",'"',$sExtra); // einfache in doppelte anfuehrungszeichen tauschen
		// syslang
		if (!isset($syslang)) {
			$syslang = (isset($Userdata['system_lang_default'])) 
				? $Userdata['system_lang_default'] 
				: $aENV['system_language']['default'];
		}
		// texte
		$MSG = array();
		$MSG['edit']['de'] = "Editieren";
		$MSG['edit']['en'] = "edit";
		$alt = $MSG['edit'][$syslang];
		
		// status
		if ($nNaviStatus == 1 && $nStatus != 0) { // case "cms-navi: in-navi"
			$nStatus = 2;
		}
		if ($sData == '') { // case "empty"
			$nStatus = -1; // -> gehe in "default" im switch-statement
		}
		if ($sOnlineFrom != '' || $sOnlineTo != '') { // zeitsteuerung
			$isValid = compareOnlineFromToWithToday($sOnlineFrom, $sOnlineTo);
			if (!$isValid && $nStatus == 1) $nStatus = 10;
			if (!$isValid && $nStatus == 2) $nStatus = 20;
		}
		switch ($nStatus) {
			case "0":  $img = "icn_off.gif";				break;
			case "1":  $img = "icn_on.gif"; 				break;
			case "10": $img = "icn_on_timer.gif"; 			break;
			case "2":  $img = "icn_on_innavi.gif";			break;
			case "20": $img = "icn_on_innavi_timer.gif";	break;
			default:   $img = "icn_empty.gif";				break;
		}
		
		// build HTML
		$sStr =  '';
		if (file_exists($aENV['path']['pix']['unix'].$img)) {
			$imgsize = getimagesize($aENV['path']['pix']['unix'].$img);
			if ($sHref) {$sStr .= '<a href="'.$sHref.'" title="'.$alt.'">';}
			$sStr .= '<img src="'.$aENV['path']['pix']['http'].$img.'" '.$imgsize[3].' alt="'.$alt.'" class="btn" '.$sExtra.'>';
			if ($sHref) {$sStr .= '</a>';}
		}
		// output
		return $sStr;
	}
/**
* @deprecated
*/
	function get_online_status($nStatus, $sData='', $sHref='', $sExtra='') {
		return get_status_img($nStatus, $sData, $sHref, $sExtra);
	}

/**
* Vergleicht zwei uebergebene ISO-Datumswerte mit dem heutigen Datum.
* Wird fuer den Freigabestatus von Datensaetzen mir ZEITSTEUERUNG benoetigt!
* NOTE: Nutzt zur Berechnung den Unix-Timestamp - geht also nur im Bereich von 1970 - 2029!
*
* Beispiel:
* <pre><code>
* $isValid = compareOnlineFromToWithToday($aData['online_from_de'], $aData['online_to_de']); // params: $sOnlineFrom,$sOnlineTo
* </code></pre>
*
* @access	public
* @param	string	$sOnlineFrom	Start-Datum
* @param	string	$sOnlineTo		End-Datum
* @return	boolean	TRUE, wenn heute im gueltigen bereich, sonst FALSE.
*/
	function compareOnlineFromToWithToday($sOnlineFrom, $sOnlineTo) {
		/* ANFORDERUNGEN:
		0) heute: 2005-01-01, start: 0000-00-00, end: 0000-00-00 -> kein start, kein end: TRUE
		1) heute: 2005-01-01, start: 0000-00-00, end: 2007-01-01 -> kein start, end in der zukunft: TRUE
		2) heute: 2005-01-01, start: 0000-00-00, end: 2004-01-01 -> kein start, end in der vergangenheit: FALSE
		3) heute: 2005-01-01, start: 2004-01-01, end: 0000-00-00 -> start in der vergangenheit, kein end: TRUE
		4) heute: 2005-01-01, start: 2006-01-01, end: 0000-00-00 -> start in der zukunft, kein end: FALSE
		5) heute: 2005-01-01, start: 2003-01-01, end: 2004-01-01 -> start + end in der vergangenheit: FALSE
		6) heute: 2005-01-01, start: 2006-01-01, end: 2007-01-01 -> start + end in der zukunft: FALSE
		7) heute: 2005-01-01, start: 2004-01-01, end: 2006-01-01 -> startin der vergangenheit, end in der zukunft: TRUE
		*/
		
		// check vars
		if ($sOnlineFrom == '0000-00-00' && $sOnlineTo == '0000-00-00') return true;
		if ($sOnlineFrom == '') $sOnlineFrom = '0000-00-00'; // fallback
		if ($sOnlineTo == '')   $sOnlineTo   = '0000-00-00'; // fallback
		
		// wenn start = 0 -> dann mit heute fuellen
		if ($sOnlineFrom == '0000-00-00') {
			$ts_start = mktime(00, 00, 00, date('m'), date('d'), date('Y'));
		}
		else {
			$aStart = explode('-', $sOnlineFrom);
			$ts_start = mktime(0, 0, 0, $aStart[1], $aStart[2], $aStart[0]);
		}
		// wenn end = 0 -> dann mit (heute plus ein jahr) fuellen
		if ($sOnlineTo == '0000-00-00') {
			$ts_end = mktime(23, 59, 59, date('m'), date('d'), (date('Y')+1));
		}
		else {
			$aEnd = explode('-', $sOnlineTo);
			$ts_end = mktime(23, 59, 59, $aEnd[1], $aEnd[2], $aEnd[0]);
		}
		// datum heute
		$ts_today = date('U');
		
		// bedingungen pruefen
		return ($ts_start > $ts_today || $ts_end < $ts_today) ? false : true;
	}


/**
* Verlinkt den uebergebenen String mit einem ggf. uebergebenen Link 
* und verkuerzt ihn ggf. (nutzt in diesem Fall die Funktion "shorten_text()").
*
* Beispiel:
* <pre><code>
* echo view_link($aData['title_de'], $sEditPage.$sGEToptions, $aData['flag_online_de']); // params: $sText[,$sHref=''][,$nStatus=0][,$nLength=80][,$bFallback=true]
* </code></pre>
*
* @access	public
* @param	string	$sText			Langtext
* @param	string	[$sHref]		Link (i.d.R. zur Detailseite)	(optional -> default:'')
* @param	int		[$nStatus]		Freigabestatus des Datensatzes	(optional -> default: 0 | 1 | 2)
* @param	int		[$nLength]		Anzahl Zeichen, nachdenen abgeschnitten wird	(optional -> default:'80')
* @param	boolean	[$bFallback]	["true" (default) gibt bei "keinem text" eine entsprechende meldung aus | "false" nicht ]
* @return 	string					(verlinkter/gekuerzter) Text
*/
	function view_link($sText, $sHref='', $nStatus=0, $nLength=80, $bFallback=true) {
		// vars
		global $Userdata, $syslang, $aENV, $aMSG;
		$sStr = '';
		// syslang
		if (!isset($syslang)) {
			$syslang = (isset($Userdata['system_lang_default'])) ? $Userdata['system_lang_default'] : $aENV['system_language']['default'];
		}
		// fallback
		if ($bFallback==true && empty($sText)) { $sText = $aMSG['std']['no_entries_lang'][$syslang]; }
		
		// code
		$sText = strip_tags(trim($sText));
		if (strlen($sText) > $nLength) {
			$sText = substr($sText, 0, ($nLength-3))."...";
		}
		
		// build string
		if ($sHref != '') { $sStr .= '<a href="'.$sHref.'" title="'.$aMSG['mode']['edit'][$syslang].'">'; }
		$sStr .= $sText;
		if ($sHref != '') { $sStr .= '</a>'; }
		
		// output
		return $sStr;
	}


/**
* Baut komplette Statusinformation (Pages + Hits) der DB-Abfrage (formatiert).
* @deprecated 1.5 - 16.08.2006
* Wrapper für $oDBNavi->getDbnaviStatus()
* Beispiel:
* <pre><code>
* write_dbnavi_status($nEntries,$nStart,$nLimit); // params: $nEntries[,$nStart=0][,$nLimit=20]
* </code></pre>
*
* @access	public
* @param	string	$nEntries	Gesamtanzahl gefundener Datensaetze
* @param	integer	[$nStart]	Datensatz bei dem die Ausgabe begonnen wird	(optional -> default:'0')
* @param 	integer	[$nLimit]	Anzahl anzuzeigender Datensaetze	(optional -> default:'20')
* @return	html-code
*/
	function get_dbnavi_status($nEntries, $nStart=0, $nLimit=20) {
		global $Userdata,$aENV;
		require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");
		$oDBNavi = new DbNavi($Userdata,$aENV,$nEntries, $nStart, $nLimit);
		return $oDBNavi->getDbnaviStatus();
	}

/**
* Baut Statusinformation (Hits) der DB-Abfrage (zeigt also "wieviele von wieviel treffer").
* Wrapper für $oDBNavi->getDbnaviStatusHits();
*
* @deprecated 1.5 - 16.08.2006
* @access	private
* @global	array	$Userdata
* @global	array	$aENV
* @param	string	$nEntries	Gesamtanzahl gefundener Datensaetze
* @param	integer	[$nStart]	Datensatz bei dem die Ausgabe begonnen wird	(optional -> default:'0')
* @param 	integer	[$nLimit]	Anzahl anzuzeigender Datensaetze	(optional -> default:'20')
* @return	html-code
*/
	function get_dbnavi_status_hits($nEntries, $nStart=0, $nLimit=20) {
		global $Userdata,$aENV;
		require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");
		$oDBNavi = new DbNavi($Userdata,$aENV,$nEntries, $nStart, $nLimit);
		return $oDBNavi->getDbnaviStatusHits();
	}

/**
* Baut Statusinformation (Pages) der DB-Abfrage (zeigt also "Page {soundsoviel} of {anzahlpages}").
* Wrapper für $oDBNavi->getDbnaviStatusPages();
* 
* @deprecated 1.5 - 16.08.2006
* @access	private
* @global	array	$Userdata
* @global	array	$aENV
* @param	string	$nEntries	Gesamtanzahl gefundener Datensaetze
* @param	integer	[$nStart]	Datensatz bei dem die Ausgabe begonnen wird	(optional -> default:'0')
* @param 	integer	[$nLimit]	Anzahl anzuzeigender Datensaetze	(optional -> default:'20')
* @return	html-code (naja, eher einen kurzen Satz ohne Formatierung!)
*/
	function get_dbnavi_status_pages($nEntries, $nStart=0, $nLimit=20) {
		global $Userdata,$aENV;
		require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");
		$oDBNavi = new DbNavi($Userdata,$aENV,$nEntries, $nStart, $nLimit);
		return $oDBNavi->getDbnaviStatusPages();
	}


/**
* Baut Navigationslinks zu DB-Abfrage - Unterteilungsseiten. 
* Die Ausgabe der Seitenzahlen wird ab $nPageLimit abgebrochen und durch Pfeile ersetzt!
*
* Beispiel:
* <pre><code>
* get_dbnavi_links($nEntries,$nStart,$nLimit,"&navi_id=".$navi_id,5); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
* </code></pre>
*
* Wrapper für $oDBNavi->getDbnaviLinks();
*
* @deprecated 1.5 - 16.08.2006
* @access	public
* @global	array	$Userdata
* @global	array	$aENV
* @param	integer	$nEntries		Gesamtanzahl gefundener Datensaetze
* @param	integer	[$nStart]		Datensatz bei dem die Ausgabe begonnen wird	(optional -> default:'0')
* @param 	integer	[$nLimit]		Anzahl anzuzeigender Datensaetze	(optional -> default:'20')
* @param 	string	[$sGEToptions]	Zusaetzlich zu uebergebende GET-Parameter (mit fuehrendem '&')	(optional -> default:'')
* @param 	integer	[$nPageLimit]	Anzahl der angezeigten Seiten	(optional -> default:'10')
* @return	html-code
*/


	function get_dbnavi_links($nEntries, $nStart=0, $nLimit=20, $sGEToptions='', $nPageLimit=10) {
		
		global $Userdata,$aENV;
		require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");
		$oDBNavi = new DbNavi($Userdata,$aENV,$nEntries, $nStart, $nLimit);
		return $oDBNavi->getDbnaviLinks($sGEToptions,$nPageLimit);
	}

/**
* Erzeugt die Prio-Buttos [Pfeile top/hoch/runter/bottom].
*
* dazu wird die funktion "movePrio(...)" und ein counter (beim ersten ausgegebenen datensatz: '1') sowie 
* die Gesamtzahl aller gefundenen Datensaetze auf dieser Seite und die ID des zu veschiebenden Datensatzes 
* zwingend benoetigt (da fuer diese funktion ein reload gemacht wird, muessen alle parameter, die nicht 
* verlorengehen sollen, in der var $sGEToptions mituebergeben werden). 
*
* Beispiel:
* <pre><code>
* get_prio_buttons($counter,$entries,$aData['id'],$aData['parent_id'],"&show=".$show); // params: $nCounter,$nEntries,$sID[,$sCat=''][,$sGEToptions='']
* </code></pre>
*
* @access	public
* @global	integer	$start			Startnummer des ersten angezeigten Datensatzes auf der aktuellen Seite (Blaettermechanismus!)
* @global	array	$Userdata
* @global	array	$aENV
* @param	integer	$nCounter		Wert des Priofeldes
* @param	integer	$nEntries		Gesamtanzahl der Datensaetze
* @param	string	$sID			ID des zu veschiebenden Datensatzes
* @param	string	[$sCat]			ggf. einschraenkende Kategorie des zu veschiebenden Datensatzes	(optional -> default:'')
* @param	string	[$sGEToptions]	Zusaetzlich zu uebergebende GET-Parameter (mit fuehrendem '&')	(optional -> default:'')
* @return	html-code				(Wert des Priofeldes interessiert hier gar nicht)
*/
	function get_prio_buttons($nCounter, $nEntries, $sID, $sCat='', $sGEToptions='') {
		// check vars
		if (!$nCounter) die("Error: NO nCounter!");
		if (!$nEntries) die("Error: NO nEntries!");
		if (!$sID) die("Error: NO sID!");
		$sGEToptions = preg_replace("/^\?/","&",$sGEToptions);
		
		// NEU: RECHTEMANAGEMENT
		global $oPerm;
		if (!$oPerm->hasPriv('edit')) return '';
		
		global $start;
		if (!$start) { $start = 0; }
		#if ($start > 0) { $nEntries = $nEntries + $start; }// BUGFIX 2004-05-25af
		$nCounter = $nCounter + $start;						// BUGFIX 2004-05-25af
		
		$sStr = "";
		if ($sCat != '') {$sCat = "&moveCat=".$sCat;}
		global $Userdata, $aENV, $aMSG, $syslang;
		if (empty($syslang)) {
			$syslang = (isset($Userdata['system_lang_default'])) ? $Userdata['system_lang_default'] : $aENV['system_language']['default'];
		}
		
		if ($nCounter > 1) 			{ $sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=top&moveId=".$sID.$sCat.$sGEToptions.'" title="'.$aMSG['btn']['prio_top'][$syslang].'">'; $di=""; }	else { $di="d_"; }
		$sStr .= '<img src="'.$aENV['path']['pix']['http'].$di.'pf_abstop.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_top'][$syslang].'" border="0">';
		if ($nCounter > 1)			{ $sStr .= "</a>"; }
		
		if ($nCounter > 1) 			{ $sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=up&moveId=".$sID.$sCat.$sGEToptions.'" title="'.$aMSG['btn']['prio_up'][$syslang].'">'; $di=""; }		else { $di="d_"; }
		$sStr .= '<img src="'.$aENV['path']['pix']['http'].$di.'pf_up.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_up'][$syslang].'" border="0">';
		if ($nCounter > 1)			{ $sStr .= "</a>"; }
		
		if ($nCounter < $nEntries)	{ $sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=dwn&moveId=".$sID.$sCat.$sGEToptions.'" title="'.$aMSG['btn']['prio_dwn'][$syslang].'">'; $di=""; }	else { $di="d_"; }
		$sStr .= '<img src="'.$aENV['path']['pix']['http'].$di.'pf_down.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_dwn'][$syslang].'" border="0">';
		if ($nCounter < $nEntries)	{ $sStr .= "</a>"; }
		
		if ($nCounter < $nEntries)	{ $sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=btm&moveId=".$sID.$sCat.$sGEToptions.'" title="'.$aMSG['btn']['prio_btm'][$syslang].'">'; $di=""; }	else { $di="d_"; }
		$sStr .= '<img src="'.$aENV['path']['pix']['http'].$di.'pf_absbottom.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_btm'][$syslang].'" border="0">';
		if ($nCounter < $nEntries)	{ $sStr .= "</a>"; }
		
		return $sStr;
	}


/**
* ist der Ausfuehrende Part des Prioeditors.
*
* Aendert die Anordnung von Datensaetzen, die nach einem Feld namens "prio" sortiert sind 
* (das Feld muss exakt "prio" heissen! (und sollte UNIQUE sein)). 
* Wenn nur eine untermenge der tabelle (bspw. nur die datensaetze einer kategorie von presse) umsortiert werden 
* soll, muss der name des feldes uebergeben werden. 
* diese funktion muss VOR der query eingebunden werden, die die (neu sortierten) daten ausgibt. 
*
* WICHTIG: wenn ein datensatz neu angelegt wird muss vorher die hoechste prio ermittelt und in das feld gespeichert werden! 
*
* Beispiel detailseite:
* <pre><code>
* // find highest prio
* $oDb->query("SELECT MAX(prio) FROM ".$sTable);
* $newPrio = $oDb->fetch_row();
* $aData['prio'] = ($newPrio[0]+1);
* // make insert ...
* </code></pre>
*
* Beispiel overviewseite: 
* <pre><code>
* // Integration in eine Overwiew-Adminpage muss exakt so aussehen (funktion erwartet die angegebenen GET-vars!)
* movePrio("meine_tabelle"); // params: "meine_tabelle"
* // oder bei priorisierung von daten nur innerhalb einer kategorie:
* movePrio("meine_tabelle", "mein_kategorie_feld"); // params: "meine_tabelle"[, "mein_kategorie_feld"][,$sAddSqlConstraint='']
* 
* $oDb->query("SELECT ....");
* $entries = floor($oDb->num_rows());	// noetig fuer DB-Navi und PRIO!
* if (!$start) {$start=0;}	// noetig fuer DB-Navi!
* $oDb->query($sQuery." LIMIT $start,$limit");
* $counter = 0; // noetig fuer PRIO!
* while ($aData = $oDb->fetch_array()) {
* 	$counter++; // zaehle hoch - noetig fuer PRIO!
* 	write_prio_buttons($counter,$entries,$aData['id'],$aData['parent_id'],"&show=".$show); // params: $nCounter,$nEntries,$sID[,$sCat=''][,$sGEToptions='']
* }
* </code></pre>
*
* @access	public
* @global	object	$oDb
* @param	string	$sMoveDir		einer der Befehle (top|up|dwn|btm)		-> wird von der funktion write_prio_buttons() gegeben
* @param	integer	$sMoveId		Id des zu verschiebenden Datensatzes	-> wird von der funktion write_prio_buttons() gegeben
* @param	string	$sMoveCat		Id der DB-column						-> wird ggf. von der funktion write_prio_buttons() gegeben
* @param	string	$sTable			Name der Tabelle
* @param	string	$sCatFeldname	Name der DB-column, in dem die Unterteilung gespeichert ist (nur angeben, wenn unterteilt werden soll!)
* @param	string	$sAddSqlConstraint	fuer evtl. zusaetzliche DB-Bedingung (in der Form "AND field='value'")
*/
	function movePrio($sTable, $sCatFeldname='', $sAddSqlConstraint='', $sField='') {
		
		$sMoveDir	= $_GET['moveDir'];
		$sMoveId	= $_GET['moveId'];
		$sMoveCat	= $_GET['moveCat'];
		if(empty($sField)) $sField	= "id";
				
		if ($sMoveDir 
		&& $sMoveId 
		&& $sTable) { // nur was machen, wenn das richtige ankommt!
			
			global $oDb;
			
			// wenn $sCatFeldname angegeben wurde, bilde sql-constraint
			if ($sCatFeldname != '' 
			&& $sMoveCat == 0) { $sMoveCat = '0'; } // fallback bei '0'
			
			$sConstraint = '';
			
			if ($sCatFeldname != '') { $sConstraint = " AND ".$sCatFeldname."='".$sMoveCat."'"; }
			if ($sAddSqlConstraint != '') { $sConstraint .= ' '.$sAddSqlConstraint; }
			
			// 'top' und 'btm' werden nicht getauscht, sondern ganz an anfang / ende gesetzt!
			if ($sMoveDir == 'top' || $sMoveDir == 'btm') {
				// get min and max prio
				$oDb->query("SELECT MAX(prio) as max, MIN(prio) as min FROM ".$sTable);
				$aDataPrio = $oDb->fetch_array();
				$nMaxPrio = ($aDataPrio['max']);
				$nMinPrio = ($aDataPrio['min']);
				// move
				if ($sMoveDir == 'top') { $oDb->query("UPDATE ".$sTable." SET prio=".($nMaxPrio+1)." WHERE ".$sField."='".$sMoveId."'"); }
				if ($sMoveDir == 'btm') { $oDb->query("UPDATE ".$sTable." SET prio=".($nMinPrio-1)." WHERE ".$sField."='".$sMoveId."'"); }
			}
			// 'up' und 'dwn' werden mit naechst hoeherem datensatz (ggf. des gleichen bereichs) getauscht
			else {

				// zu verschiebender datensatz:
				$sql = "SELECT ".$sField.",prio FROM ".$sTable." WHERE ".$sField."='".$sMoveId."'";
				$oDb->query($sql);
				$aPrioSource = $oDb->fetch_array();

				// zu tauschender datensatz
				if ($sMoveDir == 'up')	{ $sql = "SELECT ".$sField.", prio FROM ".$sTable." WHERE prio>".$aPrioSource['prio'].$sConstraint." ORDER BY prio ASC"; }
				if ($sMoveDir == 'dwn')	{ $sql = "SELECT ".$sField.", prio FROM ".$sTable." WHERE prio<".$aPrioSource['prio'].$sConstraint." ORDER BY prio DESC"; }
				$oDb->query($sql);
				$aPrioTarget = $oDb->fetch_array();
				// move
				$oDb->query("UPDATE ".$sTable." SET prio=".$aPrioTarget['prio']." WHERE ".$sField."=".$aPrioSource[$sField]);
				$oDb->query("UPDATE ".$sTable." SET prio=".$aPrioSource['prio']." WHERE ".$sField."=".$aPrioTarget[$sField]);
			}
			// reload der seite ohne move-params!
			$aFilter = array("moveDir=".$sMoveDir, "&moveId=".$sMoveId, "&moveCat=".$sMoveCat); // diese GET-params muessen gefiltert werden!!!
			$query_string = "?".str_replace($aFilter, '', $_SERVER['QUERY_STRING']);
			$query_string = str_replace('?&', '?', $query_string);
			header("Location: ".$_SERVER['PHP_SELF'].$query_string);
		}
	}


# --------------------------------------------------------------------------------SELECT

/**
* Baut komplettes Dropdown (Select-Field) aus array (mit beachtung der nutzer-rechte). 
* Der 3. Parameter (in der Form "array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...) erzeugt die
* <option>-tags. Im optionalen 4. Parameter kann man zusaetzliche Attribute (js-eventhandler,class,...)
* uebergeben.
*
* Beispiel:
* <pre><code>
* $aOptions = array('key1'=>'value1','key2'=>'value2','key3'=>'value3')
* write_select("select_bsp",$aData['select_bsp'],$aOptions,'onchange="tuwas(this)"'); // params: $sFieldname,$sValue,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false][$bEditMode=true]
* </code></pre>
*
* @access   public
* @global	array	$Userdata
* @global	array	$aENV
* @param	string	$sFieldname
* @param	string	$sValue
* @param	array	$aValues		(required: array('key1'=>'value1','key2'=>'value2','key3'=>'value3',...)
* @param	int		[$nSize]		(optional: Attribut "size" -> default: 1)
* @param	boolean	[$bMultiple]	(optional: Attribut "multiple" -> default: false)
* @param	boolean	[$bEditMode]	(optional: bei false -> schreibe nur text, kein select-field -> default: true)
* @return	html-code
*/
	function get_select($sFieldname, $sValue, $aValues, $sExtra='', $nSize=1, $bMultiple=false, $bEditMode=true) {
		
		global $Userdata, $aENV;
		$syslang = $Userdata['system_lang_default'];
		if (!$syslang) { $syslang = $aENV['system_language']['default']; }
		if ($bMultiple==true) { $sMultiple = " multiple"; } else { $sMultiple = ''; }
		
		// check vars
		if (!$sFieldname) die("NO sFieldname!");
		if (!$aValues) die("NO aValues!");
		
		if ($bEditMode) { // build select-field
			$sStr = '<select name="aData['.$sFieldname.']" size="'.$nSize.'" '.$sExtra.$sMultiple.">\n";
			foreach ($aValues as $key => $val) {
				if ($key == $sValue) { $sel = " selected"; } else { $sel=""; }
				$sStr .= "<option value=\"$key\"$sel>$val</option>\n";
			}
			$sStr .= "</select>\n";
		} else { // print only "selected" value
			foreach ($aValues as $key => $val) {
				if ($key == $sValue) { $sStr = '<span class="text">'.$val.'</span> '; }
			}
		}
		
		return $sStr;
	}

# --------------------------------------------------------------------------------BUTTONS

/**
* Baut kompletten <input type="button"> - SPEZIELL fuer view- und detail-seiten!
* ... im Moment nur die Standard-Buttons... und braucht ziemlich viele globals... geht vielleicht besser?.
*
* Beispiel:
* <pre><code>
* get_button("DELETE",$syslang); // params: $sType[,$sLang='de'][,$sClass='but']
* </code></pre>
*
* @access   public
* @global	array	$aMSG
* @global	array	$sViewerPage
* @global	array	$sEditPage
* @global	array	$aBnParts
* @global	string	$sGEToptions
* @global	string	$sViewerPage
* @param	string	$sType			[SAVE|DELETE|CANCEL|RESET|BACK|NEW]
* @param	string	$sLang			[de|en]
* @param	string	$sClass			(optional)	(default: "but")
* @return	html-code
*/
	function get_button($sType, $sLang='de', $sClass='but') {
		// vars
		if (!$sType) die("NO sType!");
		$sType = strToLower($sType);
		$valid = false;
		if ($sLang!="de" && $sLang!="en") die("WRONG $sLang!");
		#global $aMSG,$sViewerPage,$sEditPage,$aBnParts,$sGEToptions;
		global $aENV,$aMSG,$sViewerPage,$sEditPage,$sNewPage,$aBnParts,$oPerm;
		
		switch ($sType) {
			// sonderfall images
			case "back":		$valid=true; $t="image"; $image='btn_list'; $js='javascript:location.replace(\''.$sViewerPage.'\');'; break;
			case "new":			$valid=true; $t="image"; $image='btn_add'; $js='javascript:location.replace(\''.$sNewPage.'\');'; break;
			// html-buttons
			#case "back":		$valid=true; $t="button"; $extra=' onClick="location.replace(\''.$sViewerPage.'\');"'; break;
			#case "new":		$valid=true; $t="button"; $extra=' onClick="location.replace(\''.$sNewPage.'\');"'; break;
			case "save":		$valid=true; $t="submit"; $extra=' onClick="return checkReqFields()"'; break;
			case "delete":		$valid=true; $t="submit"; $extra=' onClick="return confirmDelete()"'; break;
			case "cancel":		$valid=true; $t="button"; $extra=' onClick="location.href=\''.$sViewerPage.'\';"'; break;
			case "reset":		$valid=true; $t="reset";  $extra=''; break;
			case "filter":		$valid=true; $t="submit"; $extra=''; break;
			case "search":		$valid=true; $t="submit"; $extra=''; break;
			case "close_win":	$valid=true; $t="button"; $extra=' onClick="javascript:self.close()"'; break;
		}
		if ($valid == false) die("WRONG $sType!");
		
		if ($t == "image") { // kein html-form-element sondern verlinktes image!
			$sStr = '<a href="'.$js.'" title="'.$aMSG['btn'][$sType][$sLang].'" title="'.$aMSG['btn'][$sType][$sLang].'">';
			$sStr .= '<img src="'.$aENV['path']['pix']['http'].$image.'.gif" alt="'.$aMSG['btn'][$sType][$sLang].'" class="btn"></a>';
		} else {
			$sStr = '<input type="'.$t.'" value="'.$aMSG['btn'][$sType][$sLang].'" name="btn['.$sType.']" class="'.$sClass.'"'.$extra.'>';
		}
		
		// NEU: RECHTEMANAGEMENT
		if ($sType == "delete" && !$oPerm->hasPriv('delete')) {
			$sStr = '';
		}
		if (($sType == "save" || $sType == "cancel") 
			&& (!$oPerm->hasPriv('create') && !$oPerm->hasPriv('edit') && !$oPerm->hasPriv('user_self'))) {
			$sStr = '';
		}
		if ($sType == "new" && (!$oPerm->hasPriv('create') && !$oPerm->hasPriv('admin'))) {
			$sStr = '';
		}
		
		return $sStr;
	}
	/**
	 * 
	 * Diese Methode ist ein Wrapper für Tools::combineMultipart($sFieldname,$aData)
	 * @param string $sFieldname
	 * @param array $aData
	 * @return array
	 * 
	 */
	function combineMultipart($sFieldname,$aData) {
		return Tools::combineMultipart($sFieldname,$aData);
	}
	/**
	 * 
	 * Diese Methode liefert für die Ausgabeseiten das Multipartfeld als Array zurück.
	 * @param string $sFieldname
	 * @param array $aData
	 * @return $array
	 * 
	 */
	function getMultiPartAsArray($sFieldname,$aData) {
		$aData2 = explode("\n",$aData[$sFieldname]);
		for($i=0;!empty($aData2[$i]);$i++) {
		   /* 
			* Splittet auf in 	[0] => mdb_id
			*					[1] => Beschreibungstext
			*/
			$aTmp = explode('||',$aData2[$i]);
			$aData[$aTmp[0]] = $aTmp[1];
			unset($aTmp);

		}
		return $aData;
	}
?>