<?php
/*
 * Created on 03.11.2005
 *
 */
/**
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * Diese Klasse stellt Methoden zum EasyAccess auf die Folder Bean bereit.
 * @version 0.1
 * 
 */
 
   class FolderEA extends BeanService {
   	
   		var $oDb = null;
   		var $limit = 20;
   		var $entries = 0;
   		var $order = "ASC";
   		var $orderby = "id";
   		var $aENV = null;
   		var $table = "";
   		var $flagtree = "";
   		var $uid = "";
   		var $onlyauthed = false;
   		var $weblang = 'de';
   		var $bWithFlagnavi = false;
   	
		function FolderEA(&$oDb) {
			$this->oDb =& $oDb;
		}
   	
   		function setUid($uid) {
   			$this->uid = $uid;
   		}   	
   		
   		function getUid() {
   			return $this->uid;
   		}
   		
   		function getODb() {
   			return $this->oDb;
   		}
   		
   		function setTable($table) {
   			$this->table = $table;
   		}
   		
   		function setFlagtree($flagtree) {
   			$this->flagtree = $flagtree;
   		}
   		
		function setOnlyauthed($onlyAuthed) {
			$this->onlyAuthed = $onlyAuthed;
		}
		
   		function setAENV($aenv) {
   			$this->aENV = $aenv;
   		}
   	
   		function setLimit($limit) {
   			$this->limit = $limit;
   		}
   		
   		function getLimit() {
   			return $this->limit;
   		}
   		
   		function setOrder($order) {
   			$this->order = $order;
   		}
   		
   		function setOrderBy($orderby) {
   			$this->orderby = $orderby;
   		}
   		
   		function setWeblang($weblang) {
   			$this->weblang = $weblang;
   		}
   		
   		function setBWithflagnavi($bWithFlagnavi) {
	   		$this->bWithFlagnavi = $bWithFlagnavi;	
   		}
   		
   		/**
   		 * Diese Methode initialisiert mit Hilfe der Serviceklasse die Bean
   		 * @version 0.11
   		 * @param Int $start Der Startwert fuer das DB-Limit (Seitenauflistung)
   		 * 
   		 */
   		
   		function initialize($start=0,$priomove,$naviid) {
   			
   			if(!empty($priomove) && !empty($naviid)) {
   			
   				$this->save($priomove,$naviid);
   				
   			}
   			
   			$this->bean = $this->setBeanValues($this->getValuesFromDb($start),new FolderBean());
   			$this->setGetBeanValues();

   		}
		/**
		 * Diese Methode befuellt das Array, welches nachtraeglich dazu dient den Pointer zu bewegen
		 *
		 */

		function setGetBeanValues() {
			
			$arbva = array('prio','parentid','title','id');
			
			$this->getBeanValues($arbva);
				
		}
   		
   		/**
   		 * 
   		 * Diese Methode wird von der NaviToggle Klasse benoetigt. (req!)
   		 * @return Object
   		 * 
   		 */
   	
   		function getBean() {
   		
   			return $this->bean;
   			
   		}
   	
   		/**
   		 * Diese Methode fragt alle Notwendigen Daten von der DB ab und speichert sie in einem Array
   		 * @param Int $start (Limit per DB)
   		 * @return String[]
   		 */
   		 
   		function getValuesFromDb($start) {
			global $oPerm;
			
			if($this->onlyAuthed && !$oPerm->hasPriv('admin')) {

				$TACEA =& new TeamAccessControllEA($this->oDb);
				$TACEA->setTable($this->aENV['table']['sys_team_access']);
				
				$TACEA->setAENV($this->aENV);
				$TACEA->initialize();
				$oTeam =& new Team($this->oDb);
				
			}
			$table1 = $this->table;
			// Datensaetze pro Ausgabeseite einstellen
			$sql = "SELECT $table1.id,$table1.prio,$table1.parent_id,$table1.title";
			if($this->bWithFlagnavi) {
				$sql .= ",$table1.flag_navi";	
			}
			$sql .= " FROM $table1 ";
		
			$sql .= "WHERE flag_tree='".$this->flagtree."'";
			
			if($this->orderby == "id") {
			
			$sql .= " ORDER BY $table1.id ".$this->order;
			
			}
			else {
			
				$sql .= " ORDER BY ".$this->orderby." ".$this->order;
				
			}
	
			$this->oDb->query($sql);
			
			$this->entries = floor($this->oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)

   			if($this->onlyAuthed && !$oPerm->hasPriv('admin')) {	
   				   				   			
   				for($i=0,$lastinsert=0;$row=$this->oDb->fetch_object();$i++) {

					$show = 0;
				
						$usrright = $TACEA->getRightForID($row->id,$this->flagtree);
					
						if(is_array($usrright) && !empty($usrright[$this->uid]['id'])) {
		
							$show = 1;
	
						
						}
						if(!is_array($usrright)) $show = 1;
					
					if($show == 1) {
						
						$i = $lastinsert;
						
   						$ret["id"][$i] 	 		= $row->id;
   						$ret["prio"][$i]		= $row->prio;
   						$ret["parentid"][$i]  	= $row->parent_id;
   						$ret["title"][$i] 		= $row->title;
   						if($this->bWithFlagnavi) {
   							$ret['flagnavi'][$i]=$row->flag_navi;
   						}
						$lastinsert++;
					}
   				
   				}
   			}
   			else {
   				
   				for($i=0;$row=$this->oDb->fetch_object();$i++) {
   				
   					$ret["id"][$i] 	 		= $row->id;
   					$ret["prio"][$i]		= $row->prio;
   					$ret["parentid"][$i]  	= $row->parent_id;
   					$ret["title"][$i] 		= $row->title;
   					if($this->bWithFlagnavi) {
   						$ret['flagnavi'][$i]=$row->flag_navi;
   					}
   				}
   			}

   			return $ret;
   			
   		}
	/**
	 * 
	 * Liefert die aktuelle Ergebnismenge zurueck
	 * @return int
	 */
	function getEntries() {
		
		return $this->entries;
		
	}
	
	function getLastPrio($parent_id) {
		$sql = "SELECT id,prio FROM ".$this->table." WHERE parent_id='".$parent_id."' AND flag_tree='".$this->flagtree."' ORDER BY prio DESC LIMIT 0,1";
		$rs = $this->oDb->query($sql);
		$row = $this->oDb->fetch_object();
		if($this->oDb->num_rows() != 0) {
			$prio = ($row->prio+1);
		}
		else {
			$prio = 1;
		}
		$this->oDb->free_result();
		return $prio;
	}
	
	function doUpdate(&$aData) {
		$aData['flag_tree'] = $this->flagtree;
		return $this->oDb->make_update($aData,$this->table,$aData['id']);	
	}
	
	function doInsert(&$aData) {
		$aData['prio'] = $this->getLastPrio($aData['parent_id']);
		$aData['flag_tree'] = $this->flagtree;
		return $this->oDb->make_insert($aData,$this->table);
	}
	
	function doDelete($delid,$deltable) {
		if($this->oDb->make_delete(array('id' => $delid,'flag_tree' => $this->flagtree),$this->table) && $this->oDb->make_delete(array('tree_id' => $delid),$deltable)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Diese Methode führt einen Move der Prio eines Datensatzes auf der Datenbank durch.
	 * 
	 * @param String $priomove
	 * @param int $naviid
	 * @return void
     * @access public
	 */
	function save($priomove,$naviid) {
		
		$rs = $this->oDb->fetch_by_id($naviid,$this->table);
		
		$parentid = $rs['parent_id'];
		
		$sql = "SELECT * FROM ".$this->table." WHERE parent_id='$parentid' AND flag_tree='".$this->flagtree."' ORDER BY prio ASC";
		
		$this->oDb->query($sql);
		
		$num = $this->oDb->num_rows();
		
		for($i=0;$row = $this->oDb->fetch_object();$i++) {
		
			$prios[$i] = $row;
			
		}
		
		$parentdataset = $this->oDb->fetch_by_id($parentid,$this->table);
		
		for($i=0;!empty($prios[$i]);$i++) {
		
			if($prios[$i]->id == $naviid) {
			
				switch(true) {
				
					case ($priomove == 'up') :
					 
					  $icount = (count($prios)-1);
					  
					  if($i==$icount) {
					 
					 	$priothis = $prios[$i]->prio;
					 	$prioother = $prios[($i-1)]->prio;
					 	
					 }
					 else {
					 
						 $priothis = $prios[($i+1)]->prio;
						 $prioother = $prios[$i]->prio;
						 
					 }
					 					 
					 if($priothis > $parentdataset['prio'] && $prios[$i]->parent_id != 0) {
					 
					 	$priothis = $parentdataset['prio']-1;
					 
					 }
					 if($prioother > $parentdataset['prio'] && $prios[$i]->parent_id != 0) {
					 
					 	$prioother = $parentdataset['prio']-2;
					 	
					 }
					 $sql = "UPDATE ".$this->table." SET prio='$priothis' WHERE id='$naviid' AND flag_tree='".$this->flagtree."'";
					 $sql2 = "UPDATE ".$this->table." SET prio='$prioother' WHERE id='".$prios[($i+1)]->id."' AND flag_tree='".$this->flagtree."'";
					 
					 $this->oDb->query($sql);
					 $this->oDb->query($sql2);
					 
					 break;

					case ($priomove == 'down') :
					 
					 if($i==0) {
					 
					 	$icount = (count($prios)-1);
					 	$priothis = $prios[$i]->prio;
					 	$prioother = $prios[($i+1)]->prio;
					 	
					 }
					 else {
					 
					 	$icount = $i-1;
					 	$priothis = $prios[$icount]->prio;
					 	$prioother = $prios[$i]->prio;
					 }
					 					 
					 if($priothis > $parentdataset['prio'] && $prios[$i]->parent_id != 0) {
					 	
					 	$priothis = $parentdataset['prio']-1;

					 }

					 if($prioother > $parentdataset['prio'] && $prios[$i]->parent_id != 0) {
					 
					 	$prioother = $parentdataset['prio']-2;
					 	
					 }

					 $sql = "UPDATE ".$this->table." SET prio='$priothis' WHERE id='$naviid' AND flag_tree='".$this->flagtree."'";
					 $sql2 = "UPDATE ".$this->table." SET prio='$prioother' WHERE id='".$prios[($i-1)]->id."' AND flag_tree='".$this->flagtree."'";
					 $this->oDb->query($sql);
					 $this->oDb->query($sql2);
					 break;

					case ($priomove == 'last') :
		 
					 for($i=0;!empty($prios[$i]);$i++) {
					
						if($prios[$i]->id == $naviid) {

							$priothis = $prios[0]->prio;
							
						}
						else {
			
							$priothis  = $prios[$i]->prio+1; 
						
						}

						 if($priothis > $parentdataset['prio'] && $prios[$i]->parent_id != 0) {
						 
						 	$priothis = $parentdataset['prio']-1;
					 
						 }
					 	$sql = "UPDATE ".$this->table." SET prio='$priothis' WHERE id='".$prios[$i]->id."' AND flag_tree='".$this->flagtree."'";
					 	$this->oDb->query($sql);
					 	
					 }
					 break;

					case ($priomove == 'first') :
					
					 for($i=0;!empty($prios[$i]);$i++) {
					
						if($prios[$i]->id == $naviid) {
							
							$priothis = $prios[($num-1)]->prio;
							
						}
						else {
							
							$priothis  = $prios[$i]->prio-1;
						
						}

						 if($priothis > $parentdataset['prio'] && $prios[$i]->parent_id != 0) {
						 
						 	$priothis = $parentdataset['prio']-1;
					 
						 }
					 	$sql = "UPDATE ".$this->table." SET prio='$priothis' WHERE id='".$prios[$i]->id."' AND flag_tree='".$this->flagtree."'";
					 	$this->oDb->query($sql);

					 }	
					 break;
					
				}
			}
			
		}
		if($this->flagtree == 'mdb') {
			if (file_exists(MDB_STRUCTURE_FILE)) { $delete = unlink(MDB_STRUCTURE_FILE); }		
		}

		if($this->flagtree == 'frm') {
			if (file_exists(FORUM_STRUCTURE_FILE)) { $delete = unlink(FORUM_STRUCTURE_FILE); }		
		}

		if($this->flagtree == 'arc') {
			if (file_exists(ARCHIV_STRUCTURE_FILE)) { $delete = unlink(ARCHIV_STRUCTURE_FILE); }		
		}

	}
     
    /**
     * Sortiert einen Folder neu. Nach Anfangsbuchstaben aufsteigend oder absteigend.
     * 
     * @param int $parentid
     * @param String $sortway
     * @return void
     * @access public
     * 
     */
    function sortFolder($parentid,$sortway='ASC') {
    
    	if($sortway == 'ASC') $sortway = 'DESC'; else $sortway = 'ASC';
    
    	$sql = 'SELECT * FROM '.$this->table." WHERE parent_id='$parentid' ORDER BY title ".$sortway;
    	
    	$this->oDb->query($sql);
    	
    	for($i=0;$row = $this->oDb->fetch_object();$i++) {
    		$sqls[] = "UPDATE ".$this->table." SET prio='".($i+1)."' WHERE id='".$row->id."'";
    	}
    	for($i=0;!empty($sqls[$i]);$i++) {
    		$this->oDb->query($sqls[$i]);	
    	}
    }
    
   }
?>