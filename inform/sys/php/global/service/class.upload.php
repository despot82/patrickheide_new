<?php // Class upload
/**
* provide form-based file-upload functions.
* wenn nicht anders gewuenscht ('false' als 2. Konstruktor-Parameter uebergeben) wird eine (nur 8-stellige) Unique-ID zwischen 
* filename und fileextension gepflanzt, was versehentliches ueberschreiben verhindern soll. 
* 
* Example: 
* <pre><code> 
* // init with 'get name_of_formfield' 
* $upload1 =& new upload("name_of_formfield", false);
* // set syslang for alerts
* $upload1->set_syslang($syslang);
* // set filesize-constraint to max. 500 kByte
* $upload1->check_max_filesize(500000); // mind the bytes!
* // if file is an image, set size-constraints to exact width 600px and height to 800px
* if (strstr($uploadfile_type,"image")) $upload1->check_imagesize(600, 800);
* // if file is an image, set size-constraints to max. width 600px and height to 800px
* if (strstr($uploadfile_type,"image")) $upload1->check_max_imagesize($maxwidth, $maxheight, $error_msg=NULL);
* // filename: max 40 chars!
* $upload1->check_filename_length(40, array('de'=>'Spezial-Fehlermeldung als array'));
* // filename: keine sonderzeichen!
* $upload1->check_filename(array('de'=>'Spezial-Fehlermeldung als array'));
* // make upload and get new filename for DB (-> 2. Konstruktor-Parameter: true)
* if (!($aData['filename'] = $upload1->make_upload($uploadpath))) {$upload1->script_alert(); $alert=true;} // = z.B.: 'imagename075b7ef3.jpg'
* // or the same without unique filename (-> 2. Konstruktor-Parameter: false)
* if (!($aData['filename'] = $upload1->make_upload($uploadpath))) {$upload1->script_alert(); $alert=true;} // = z.B.: 'imagename.jpg'
*
* // ODER neuen filename vergeben
* $upload1 =& new upload("name_of_formfield", false);
* $upload1->set_filename($different_filename);
* if (!($aData['filename'] = $upload1->make_upload($uploadpath))) {$upload1->script_alert(); $alert=true;}
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.2 / 2006-02-17	[NEU: chmod 777 nach move_uploaded_file bei "make_upload()"]
*/
class upload {
	
	/*	----------------------------------------------------------------------------
		HISTORY:
		2.2 / 2006-02-17	[NEU: chmod 777 nach move_uploaded_file bei "make_upload()"]
		2.1 / 2005-08-31	[NEU: "set_filename()"]
		2.06 / 2005-05-03	[BUGFIX: unerlaubte Zeichen gleich mit entfernen bei "_make_unique_filename()"]
		2.05 / 2005-04-20	[BUGFIX: strtolower bei filename-/file-extension-ermittlung]
		2.04 / 2005-02-02	[BUGFIX: OS9-31-zeichen-limit bei "_make_unique_filename()"]
		2.03 / 2005-01-11	[BUGFIX: file-extension-ermittlung bei "upload()"]
		2.02 / 2005-01-03	[NEU: _check_directory]
		2.01 / 2004-12-16	[NEU: effizienter die file-extension in "upload()" generieren]
		2.0 / 2004-10-20	[NEU: Datei-Parameter aus "$_FILE" und nicht mehr aus "$GLOBALS" generieren]
		1.9 / 2004-09-29	[NEU: 2. Parameter $unique_filename=true bei "upload()" + DEPRECATED: "set_unique_false()"]
		1.8 / 2004-04-06	[NEU: pruefmethoden "check_*()" + DEPRECATED: "set_rights()"]
		1.7 / 2003-10-13	[NEU und BUGFIX: alerts in 'make_upload()' + NEU: 'set_syslang()']
		1.6 / 2003-09-29	[NEU: Limitierung auf insgesamt 31 Zeichen in 'make_unique_filename()']
		1.5 / 2003-08-12	[NEU: change_jpg_fileext()]
		1.41 / 2003-08-12	[strToLower($this->fileext) in upload() hinzugenommen]
		1.4 / 2003-06-13	[check auf "mustfit" in make_upload() integriert]
		1.31 / 2003-06-13	[nur kommentare fuer PHPDoc ueberarbeitet]
		1.3 / 2003-03-03
	*/

#-----------------------------------------------------------------------------

	// class vars
	var $name = '';
	var $source = '';
	var $source_size = 0;
	var $source_filename = '';
	var $filename = '';
	var $fileext = '';
	var $max_width = 0;
	var $max_height = 0;
	var $max_filesize = 0;
	var $max_filename_length = 0;
	var $check_filename = false;
	var $filename_regex = "/[\s|\^|\!|\%|\&|\#|\§|\$|\/|\\|\=|\?|\:|\;|\,|\<|\>||\*|\+|\"|\'|\!|ä|Ä|ü|Ü|ö|Ö]/";
	var $mustfit = false;
	var $mustimage = false;
	var $is_image = false;
	var $unique_filename = true;
	var $alert = '';
	var $syslang = 'de'; // [de|en]
	// alert-texte
	var $aAlert = array(
		'mustimage' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Das Bild ist nicht in einem internetkonformen Bildformat gespeichert!',
			'en' => 'Upload failed. Image has incorrect filetype!'),
		'imagesizes' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Das Bild ist größer als die erlaubten {MAXWIDTH} x {MAXHEIGHT} Pixel!', // {MAXWIDTH} und {MAXHEIGHT} -> Platzhalter wird ersetzt!
			'en' => 'Upload failed. Image dimensions are bigger than permitted {MAXWIDTH} x {MAXHEIGHT} pixel!'),
		'mustfit' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Das Bild entspricht nicht den vorgegebenen Abmessungen ({MAXWIDTH} x {MAXHEIGHT} Pixel)!', // {MAXWIDTH} und {MAXHEIGHT} -> Platzhalter wird ersetzt!
			'en' => 'Upload failed. Image dimensions are not exactly {MAXWIDTH} x {MAXHEIGHT} pixel!'),
		'filesize' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Das Dateivolumen ist größer als die erlaubten {MAXFILESIZE} kb!', // {MAXFILESIZE} -> Platzhalter wird ersetzt!
			'en' => 'Upload failed. Filesize is bigger than permitted  {MAXFILESIZE} kb!'),
		'unknownFile' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Das Bild liegt in einem ungültigem Format vor!',
			'en' => 'Upload failed. Image has illegal filetype!'),
		'unableToCopy' => array(
			'de' => 'Der Upload ist fehlgeschlagen! Überprüfen Sie die Dateirechte auf dem Server!',
			'en' => 'Upload failed. Check directory/file permissions on the webserver!'),
		'corruptFile' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Datei ist leer. Überprüfen Sie die Datei!',
			'en' => 'Upload failed. File is empty!'),
		'filename_length' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Der Dateiname ist zu lang!',
			'en' => 'Upload failed. Filename is too long!'),
		'check_filename' => array(
			'de' => 'Der Upload ist fehlgeschlagen: Der Dateiname enthält ungültige (Sonder-)Zeichen!',
			'en' => 'Upload failed. Filename has invalid characters!')
	);

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das upload-Objekt
*
* hier wird der Klasse durch Angabe des Namens des Formular-Feldes (type=file) die Quelle (Name, Groesse,etc) 
* der hochzuladenden Datei uebergeben. Setzt ausserdem ein paar interne Vars. 
* Wenn als optionaler 2. Parameter "false" uebergeben wird, wird das hochzuladende bild nicht mit einr unique-ID versehen, 
* sondern mit dem originalnamen auf dem Server gespeichert (VORSICHT: Files koennen so ueberschrieben werden!)
*
* @access   public
* @param 	string	$name (name als string, nicht variable)
* @param 	boolean	$unique_filename (auf false setzen wenn der original name verwendet werden soll!)
*/
	function upload($name, $unique_filename=true) {
		$this->name = $name;
		$this->source = ($_FILES[$name]['tmp_name']) ? $_FILES[$name]['tmp_name'] : $GLOBALS[$name];
		$this->source_size = ($_FILES[$name]['size']) ? $_FILES[$name]['size'] : $GLOBALS[$name."_size"];
		$this->source_filename = ($_FILES[$name]['name']) ? $_FILES[$name]['name'] : $GLOBALS[$name."_name"];
		$this->unique_filename = $unique_filename;
		$this->fileext = strToLower(substr(strrchr($this->source_filename, "."), 0));
		#OLD: $this->fileext = strToLower(substr($this->source_filename, strrpos($this->source_filename, "."), strlen($this->source_filename)));
		if ($this->unique_filename == true) {
			$this->filename = $this->_make_unique_filename();
		} else {
			/* Workover by David Aurelio <da@design-aspekt.com> 2007-10-17:
			 * - remove non alphanumeric characters
			 */
			$fn = strtolower($this->source_filename);
			$this->filename = preg_replace('/[^a-z0-9_\-\.]/i', '_', $fn);
		}
		$this->is_image = in_array($this->fileext, array(".gif",".png",".jpg",".jpeg",".swf"));
	}

/**
* diese interne Methode bastelt eine (nur) 8-stellige Unique-ID in die hochzuladende Datei.
* Wird ggf. vom Konstruktor verwendet.
*
* @access 	private
* @return	string	Behandelter Dateiname
*/
	function _make_unique_filename() {
		
		// make unique id out of filename
		$tmp_uniqueID = str_replace($this->source_filename, '', uniqid($this->source_filename));
		$tmp_uniqueID = substr($tmp_uniqueID, 5); // shorten unique-id to 8 digits
		// make temp-filename without strange characters
		$tmp_filename = preg_replace('/[^a-z0-9_\-\.]/i', '_', $this->source_filename);
		// remove extension from temp-filename
		$tmp_filename = str_replace($this->fileext, '', strtolower($tmp_filename));
		// shorten temp-filename (according to Mac OS9 31-character limitation for filenames...)
		$tmp_filename = substr($tmp_filename, 0, 18); // 31 - 1(dot) - 8(uniqueID) - 4(fileextension)
		// merge this - like: [tmp_filename].[tmp_uniqueID].[fileext]
		return $tmp_filename.'.'.$tmp_uniqueID.$this->fileext;
		/* Workover by David Aurelio <da@design-aspekt.com> 2007-10-17: 
		 * - removed shortening: may render a unique id non-unique
		$aFn = explode('.', $this->source_filename);
		$uniqueId = uniqid();
		if (sizeof($aFn) > 1) {
			array_splice($aFn, -1, 0, array($uniqueId));
		} else {
			$aFn[1] = $uniqueId;
		}
		$filename = implode(".", $aFn);
		// remove non-alphanumeric characters from filename.
		$filename = preg_replace('/[^a-z0-9_\-\.]/i', '_', $filename);
		return $filename;

		 */
	}

#----------------------------------------------------------------------------- Methoden

/**
* diese Methode setzt die Ausgabesprache der alert-Texte.
*
* @access 	public
* @param	string	[$syslang]		Ausgabe-Sprache [de|en] (default: 'de')
*/
	function set_syslang($syslang='de') {
		if (in_array($syslang, array('de','en'))) {$this->syslang = $syslang;} // bei unsinn nichts tun
	}

/**
* diese Methode gibt der hochgeladenen Datei einen neuen Dateinamen.
*
* @access 	public
* @param	string	[$syslang]		Ausgabe-Sprache [de|en] (default: 'de')
*/
	function set_filename($different_filename) {
		if (!empty($different_filename)) {$this->filename = $different_filename;} // bei unsinn nichts tun
	}

/**
* Checkt die hochzuladende Datei auf eine maximale Dateigroesse.
*
* @access 	public
* @param	string	$maxwidth	Maximale erlaubte Dateigroesse des hochzuladenden Files in BYTES!!! (nicht KB!)
* @param	array	$error_msg	[customized error-message (optional - default: default-message aus $aAlert)
*/
	function check_max_filesize($maxfilesize, $error_msg=NULL) {
		$this->max_filesize = $maxfilesize;
		if (is_array($error_msg)) {$this->aAlert['filesize'] = $error_msg;} // ggf. default-error-msg ueberschreiben
	}

/**
* Checkt die hochzuladende (Bild-)Datei auf eine EXAKTE Hoehe + Breite.
* Bei Bedarf kann die Default-Error-Message mit einem Array wie folgt ueberschrieben werden.
* -> $error_msg = array('de'=>'blah', 'en'=>'blur');
*
* @access 	public
* @param	string	$width		Exakte Breite des Bildes
* @param	string	$height		Exakte Hoehe des Bildes
* @param	array	$error_msg	[customized error-message (optional - default: STD-message aus $aAlert)
*/
	function check_imagesize($width, $height, $error_msg=NULL) {
		$this->max_width	= $width;
		$this->max_height	= $height;
		$this->mustfit		= true;
		if (is_array($error_msg)) {$this->aAlert['mustfit'] = $error_msg;} // ggf. default-error-msg ueberschreiben
	}

/**
* Checkt die hochzuladende (Bild-)Datei auf eine MAXIMALE Hoehe + Breite.
*
* @access 	public
* @param	string	$maxwidth	Maximale Breite des Bildes
* @param	string	$maxheight	Maximale Hoehe des Bildes
* @param	array	$error_msg	[customized error-message (optional - default: default-message aus $aAlert)
*/
	function check_max_imagesize($maxwidth, $maxheight, $error_msg=NULL) {
		$this->max_width	= $maxwidth;
		$this->max_height	= $maxheight;
		$this->mustfit		= false;
		if (is_array($error_msg)) {$this->aAlert['imagesizes'] = $error_msg;} // ggf. default-error-msg ueberschreiben
	}

/**
* Checkt die hochzuladende Datei auf eine (uebergebene) maximale Dateinamen-Laenge.
* NOTE: wenn dem Konstruktor $unique_filename=true uebergeben wurde (= default), ist diese Pruefung eigentlich ueberfluessig!
*
* @access 	public
* @param	string	$maxlength	Maximale Dateinamen-Laenge
* @param	array	$error_msg	[customized error-message (optional - default: STD-message aus $aAlert)
*/
	function check_filename_length($maxlength=40, $error_msg=NULL) {
		$this->max_filename_length = $maxlength;
		if (is_array($error_msg)) {$this->aAlert['filename_length'] = $error_msg;} // ggf. default-error-msg ueberschreiben
	}

/**
* Checkt den Dateinamen der hochzuladenden Datei auf ungueltige Sonderzeichen.
* NOTE: wenn dem Konstruktor $unique_filename=true uebergeben wurde (= default), ist diese Pruefung eigentlich ueberfluessig!
*
* @access 	public
* @param	string	$maxlength	Maximale Dateinamen-Laenge
* @param	array	$error_msg	[customized error-message (optional - default: STD-message aus $aAlert)
*/
	function check_filename($error_msg=NULL) {
		$this->check_filename = true;
		if (is_array($error_msg)) {$this->aAlert['check_filename'] = $error_msg;} // ggf. default-error-msg ueberschreiben
	}

/**
* diese Methode ist nur dazu da, um bei jpgs die Datei-Endung in eine andere (default: 'jjj') zu aendern. 
* ...und das auch nur aus dem Grund, dass AOL das .jpg nicht in ein .art umwandelt und damit fuer 
* dynamische flash-nachladeoperationen unbrauchbar machen wuerde... :(
*
* @access 	public
* @param	string	[$sNewExt]		Neue Datei-Endung (default: 'jjj')
*/
	function change_jpg_fileext($sNewExt='jjj') {
		$sNewExt = str_replace('.', '', $sNewExt); // evtl uebergebene punkte entfernen
		$this->filename = preg_replace('/(jpg|jpeg)$/i', $sNewExt, $this->filename); // jp(e)g gegen neue ext austauschen
	}

/**
* Checkt auf Vorhandensein (und Beschreibbarkeit) des uebergebenen Verzeichnisses. Bei Bedarf wird dieses erstellt bzw. dessen Schreibrechte geaendert.
*
* @access 	private
* @return	string	Upload-Verzeichnis
*/
	function _check_directory($path) {
		if (!is_dir($path)) {
			#$oldumask = umask(0);
			mkdir($path, 0777);
			#umask($oldumask);
		}
		@chmod($path, 0777);
	}
#----------------------------------------------------------------------------- Upload-Methode

/**
* Hier wird der eigentliche Upload-Vorgang ausgefuehrt. Bei einem Fehler wird ein JS-Alert ausgegeben, 
* bei Erfolg der Dateiname der hochgeladenen Datei.
*
* @access 	public
* @param	string	$path	Pfad zum Upload-Verzeichnis
* @return	string	Dateiname der hochgeladenen Datei
*/
	function make_upload($path) {
		// ggf. Check des (original-)Filenamens auf unerlaubte Sonderzeichen
		if ($this->check_filename == true && preg_match($this->filename_regex, $this->filename)) {
		#if ($this->check_filename == true && preg_match($this->filename_regex, $this->source_filename)) {
			$this->alert = $this->aAlert['check_filename'][$this->syslang];
		}
		// ggf. Check der (original-)Filename-Laenge
		if ($this->max_filename_length > 0 && strlen($this->filename) > $this->max_filename_length) {
		#if ($this->max_filename_length > 0 && strlen($this->source_filename) > $this->max_filename_length) {
			$this->alert = $this->aAlert['filename_length'][$this->syslang];
		}
		// ggf. Check des Formats
		if ($this->mustimage && !$this->is_image) {
			$this->alert = $this->aAlert['mustimage'][$this->syslang];
		}
		// ggf. Check der Dateigroesse
		if (($this->source_size > $this->max_filesize) && $this->max_filesize > 0) {
			$kb = number_format(strval($this->max_filesize/1024));
			$this->alert = str_replace("{MAXFILESIZE}", $kb, $this->aAlert['filesize'][$this->syslang]);
		}
		// ggf. Check der Bildgroesse
		if  (($this->max_width > 0 && $this->max_height > 0) && $this->source && $this->is_image) {
			list($width,$height,$typ) = GetImageSize($this->source);
			// groesser als erlaubt
			if ($width > $this->max_width || $height > $this->max_height) {
				$tmp = str_replace("{MAXWIDTH}", $this->max_width, $this->aAlert['imagesizes'][$this->syslang]);
				$this->alert = str_replace("{MAXHEIGHT}", $this->max_height, $tmp);
			}
			// anders als erlaubt
			if ($this->mustfit && ($width != $this->max_width || $height != $this->max_height)) {
				$tmp = str_replace("{MAXWIDTH}", $this->max_width, $this->aAlert['mustfit'][$this->syslang]);
				$this->alert = str_replace("{MAXHEIGHT}", $this->max_height, $tmp);
			}
			// Dateityp (von PHP) nicht erkannt
			if (!$typ) { $this->alert = $this->aAlert['unknownFile'][$this->syslang]; }
		}
		// Check Datei selbst
		if (!$this->source_size) {$this->alert = $this->aAlert['corruptFile'][$this->syslang];}
		
		// Check uebergebenen path auf "/" am ende
		if (!substr($path,-1)=="/") {$path = $path."/";}
		// Check path auf vorhandensein und schreibrechte
		$this->_check_directory($path);
		
		// Wenn alles OK
		if ($this->alert=='' && $this->source_size && is_uploaded_file($this->source)) {
			// "move_uploaded_file()" statt "copy()" -> weil sicherer!
			/* Workover by David Aurelio <da@design-aspekt.com> 2007-10-17:
			 * - check if file already exist on filesystem and append suffix
			 */
			$i = 1;
			while(file_exists($path . $this->filename)) {
				$aFn = explode('.', $this->filename);
				$idx = (sizeof($aFn) > 1) ? sizeof($aFn)-2 : 0;
				if ($i == 1) {
					$aFn[$idx] .= "_1";
				} else {
					$aFn[$idx] = preg_replace('/_\d$/', "_$i", $aFn[$idx]);
				}
				$this->filename = implode('.', $aFn);				
				$i++;
			}
			
			if (move_uploaded_file($this->source, $path.$this->filename)) {
				@chmod($path.$this->filename, 0777);
				return $this->filename;
			}
			// Serverproblem?
			else { $this->alert = $this->aAlert['unableToCopy'][$this->syslang]; return false; }
		} else {return false;}
	}

#----------------------------------------------------------------------------- Alert-Methode

/**
* Erzeugt im Fehlerfall ein JS-Alert script.
*
* @access 	public
*/
	function script_alert() {
		if ($this->alert) echo '<script language="JavaScript" type="text/javascript">escape(\''.$this->alert.'\'); alert(unescape(\''.$this->alert.'\'))</script>';
	}

#-----------------------------------------------------------------------------
} // END of class
?>