<?php // Class filesystem
/**
* Diese Klasse stellt gaengige Filesystem-Funktionen zur Verfuegung. 
*
* Example: 
* <pre><code>
* // init 
* $oFile = new filesystem; 
* // erzeuge datei 
* if(!$datei_inhalt = $oFile->write_str_in_file("test.txt", "hallo")) echo "Fehler!"; 
* // fuege was hinten dran 
* $oFile->add_str_in_file("test.txt", " welt");
* // zeige filesize in KB 
* echo $oFile->get_filesize("test.txt");
* // lese datei 
* echo $oFile->read_file("test.txt");
* </code></pre>
*
* NOTE: In diesem Zusammenhang ist ganz interessant festzuhalten, wie man die PHP-Funktion "chmod" 
* nutzen kann, um die Dateirechte einer Datei oder eines Verzeichnisses NACHTRAEGLICH zu aendern: 
* - Lesen und Schreiben fuer den Eigentuemer, nichts fuer alle anderen:		chmod ($sFileName, 0600); 
* - Lesen und Schreiben fuer den Eigentuemer, Lesen fuer alle anderen:		chmod ($sFileName, 0644); 
* - Alles fuer den Eigentuemer, Lesen und Ausfuehren fuer andere:			chmod ($sFileName, 0755); 
* - Alles fuer alle (meist nicht empfehlenswert!):							chmod ($sFileName, 0777); 
*
* 	TODO: write_str_in_file() ohne Race Condition (mit tmp-file)!
* 
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.66 / 2006-02-17	[NEU: "set_perm()" bei "write_str_in_file()" + "add_str_in_file()"]
*/

class filesystem {
	
	/*	----------------------------------------------------------------------------
		HISTORY:
		1.66 / 2006-02-17	[NEU: "set_perm()" bei "write_str_in_file()" + "add_str_in_file()"]
		1.65 / 2005-05-31	[BUGFIX: Debug-Handling in "format_filesize()" + "script_alert()"]
		1.64 / 2005-01-03	[NEU: set_perm() + check_perm()]
		1.63 / 2004-12-16	[NEU: 'AUTO' als default-ext. in get_filesize()]<br>
		1.62 / 2004-06-07	[NEU: get_perm($sFile)]
		1.61 / 2004-05-05	[BUGFIX: alert in get_filesize()]
		1.6 / 2004-04-20	[NEU: add_str_in_file() + format_filesize() + get_filesize() + get_dirsize() + unlock() + script_alert() + alerts ueberarbeitet]
		1.52 / 2003-12-05	[BUGFIX: in clear_folder()]
		1.51 / 2003-09-26	[return_good_filename() eliminiert jetzt auch "-" (dashes)]
		1.5 / 2003-08-29	[NEU: clear_folder() + read_file() um $bReturnArray erweitert]
		1.43 / 2003-08-20	[TODOs notiert]
		1.42 / 2003-07-23	[function return_good_filename() verbessert]
		1.41 / 2003-06-13	[nur kommentare fuer PHPDoc ueberarbeitet]
		1.4 / 2003-05-20
	*/
	
	// vars
	var $sFileHandle = '';
	var $alert = '';
	var $debug = false;

/**
* NO konstruktor filesystem()
*/
	function filesystem() {}

#----------------------------------------------------------------------------- Grundfunktionen

/**
* Oeffnet eine beliebige Datei. Die Datei kann auf dem eigenen Server liegen, oder auf einem anderen. 
* Die Funktion gibt bei Erfolg true, sonst false zurueck. 
*
* Hierzu wird eine http- oder ftp-Verbindung aufgebaut werden, wenn der Dateiname mit [http://] oder 
* [ftp://] beginnt. Die Funktion gibt eine Dateinummer zurueck, die anderen Funktionen sagt, welche 
* der geoeffneten Dateien gemeint ist. "modus" gibt an, in welchem Modus die Datei geoeffnet werden 
* soll. Zur Auswahl stehen: 
* 'r'  - oeffnet eine Datei nur zum Lesen. 
* 'r+' - oeffnet eine Datei zum Lesen und Schreiben. 
* 'w'  - oeffnet eine Datei nur zum Schreiben 
* 'w+' - oeffnet die Datei zum Lesen und Schreiben und loescht den 
*        Inhalt der Datei, bzw. erstellt sie, wenn sie noch nicht 
*        existiert. 
* 'a'  - oeffnet die Datei nur zum Schreiben und plaziert den Cursor 
*        am Ende der Datei, die erstellt wird, wenn sie nicht 
*        existiert. 
* 'a+' - oeffnet die Datei nur zum Lesen und Schreiben und plaziert 
*        den Cursor am Ende der Datei, die erstellt wird, wenn sie 
*        nicht existiert. 
*
* Beispiel:
* <pre><code>
* $oFile->open_file($n[,$m]); // params: $sFileName[,$sMode="w+"]
* </code></pre>
*
* @access   public
* @param	string	$sFileName	Name des zu oeffnenden files
* @param	string	$sMode		Modus
* @return	string	filehandle	Dateinummer
*/
	function open_file($sFileName, $sMode="w+") {
		if ($this->sFileHandle = @fopen($sFileName, $sMode)) {
			if ($this->sFileHandle == '') {
				$this->script_alert("FEHLER: Kann Datei nicht oeffnen/erstellen!");
				trigger_error("FEHLER: Kann Datei: $sFileName nicht oeffnen/erstellen!",E_USER_ERROR);
				return false;
			} else {
				return $this->sFileHandle;
			}
		}
		else {
			trigger_error("FEHLER: Kann Datei: $sFileName nicht oeffnen/erstellen!",E_USER_ERROR);
		}
	}

/**
* Schuetzt die Datei vor dem SCHREIB-Zugriff (es darf gelesen werden) von anderen Usern fuer die Dauer der Bearbeitung. 
* Die Funktion gibt bei Erfolg true, sonst false zurueck. 
*
* Beispiel:
* <pre><code>
* $oFile->lock_shared();
* </code></pre>
*
* @access   public
* @return	boolean	[true|false]
*/
	function lock_shared() {
		if (flock($this->sFileHandle, (LOCK_SH|LOCK_NB))) {
			return true;
		} else {
			$this->script_alert("FEHLER: Kann Datei nicht locken!");
			trigger_error("FEHLER: Kann Datei nicht locken!",E_USER_WARNING);
			return false;
		}
	}

/**
* Schuetzt die Datei vor dem SCHREIB- und LESE-Zugriff von anderen Usern fuer die Dauer der Bearbeitung. 
* Die Funktion gibt bei Erfolg true, sonst false zurueck. 
*
* Beispiel:
* <pre><code>
* $oFile->lock_exclusive();
* </code></pre>
*
* @access   public
* @return	boolean	[true|false]
*/
	function lock_exclusive() {
		if (flock($this->sFileHandle, (LOCK_EX|LOCK_NB))) {
			return true;
		} else {
			$this->script_alert("FEHLER: Kann Datei nicht locken!");
			trigger_error("FEHLER: Kann Datei nicht locken!",E_USER_WARNING);
			return false;
		}
	}

/**
* Gibt eine geschuetzte Datei wieder frei. 
* Die Funktion gibt bei Erfolg true, sonst false zurueck. 
*
* Beispiel:
* <pre><code>
* $oFile->unlock();
* </code></pre>
*
* @access   public
* @return	boolean	[true|false]
*/
	function unlock() {
		if (flock($this->sFileHandle, LOCK_UN)) {
			return true;
		} else {
			$this->script_alert("FEHLER: Kann gelockte Datei nicht freigeben!");
			trigger_error("FEHLER: Kann gelockte Datei nicht freigeben!",E_USER_WARNING);
			return false;
		}
	}

/**
* Schliesst eine mit open_file() geoeffnete Datei. 
* Die Funktion gibt bei Erfolg true, sonst false zurueck. 
*
* Beispiel:
* <pre><code>
* $oFile->close_file();
* </code></pre>
*
* @access   public
* @return	boolean	[true|false]
*/
	function close_file() {
		if ($this->sFileHandle) {
			if(@fclose($this->sFileHandle)) {
				return true;
			}
			else {
				trigger_error('Kann Filehandle nicht schliessen!',E_USER_WARNING);
			}
		} else {
			$this->script_alert("FEHLER: Kann Datei nicht schliessen, da kein Filehandle zur Verfuegung steht!");
			trigger_error("FEHLER: Kann Datei nicht schliessen, da kein Filehandle zur Verfuegung steht!",E_USER_WARNING);
			return false;
		}
	}


#----------------------------------------------------------------------------- kombinierte File-Funktionen

/**
* Oeffnet die Datei zum lesen ("r"), lockt sie ("lock_shared") und liest Zeile fuer Zeile 
* (max. 4096 Zeichen) aus, bis zum Ende der Datei und schliesst sie wieder. Wenn als 2. Parameter 
* TRUE uebergeben wird, werden Zeilen, die mit "#" oder "//" anfangen NICHT ausgelesen. Tritt 
* einFehler auf, wird FALSE zurueckgegeben. Wenn nicht wird der Inhalt der Datei als String 
* (oder wenn als 3. Parameter TRUE uebergeben wird) als Array zurueckgegeben. 
*
* Beispiel:
* <pre><code>
* $datei_inhalt = $oFile->read_file($n, true); // params: $sFileName[,$bIngnoreRemarks(default:false)][,$bReturnArray(default:false)]
* </code></pre>
*
* @access   public
* @param	string	$sFileName			Name des zu lesenden files (ggf. inkl. Pfad!)
* @param	boolean	$bIngnoreRemarks	Kommentare ausfiltern [J=true|N=false(default)]
* @param	boolean	$bReturnArray		Rueckgabewert als Array [J=true|N=false(default)]
* @return	mixed	[$sContent(string|array)|false(boolean)]
*/
	function read_file($sFileName, $bIngnoreRemarks=false, $bReturnArray=false) {
		$aContent = array();
		// open file
		if (!$this->open_file($sFileName, "r")) { return false; }
		// lock file (shared)
		if (!$this->lock_shared()) { return false; }
		// read file
		while (!feof($this->sFileHandle)) {
			$sLine = fgets($this->sFileHandle, 4096);
			// Remarks filtern?
			if ($bIngnoreRemarks) {
				$sTempLine = trim($sLine);
				if ($sTempLine[0] == "#" || ($sTempLine[0] == "/" && $sTempLine[1] == "/")) { continue; }
			}
			// add line to array
			$aContent[] = $sLine;
		}	###...alternativ: $sContent = fread($this->sFileHandle, filesize($sFileName)); // bleiben die zeilen so wie im original???
		// unlock file
		$this->unlock();
		// close file
		$this->close_file();
		// output
		if ($bReturnArray == false) {
			return implode('', $aContent); // als string
		} else { return $aContent; } // als array
	}

/**
* Oeffnet die Datei zum schreiben ("w+"), lockt sie ("lock_exclusive") und schreibt den 
* String rein und schliesst sie wieder. Tritt ein Fehler auf, wird FALSE zurueckgegeben. 
*
* Beispiel:
* <pre><code>
* if(!$datei_inhalt = $oFile->write_str_in_file($f, $s)) echo "Fehler!";
* </code></pre>
*
* @access   public
* @param	string	$sFileName	File in das geschrieben werden soll
* @param	string	$sString	String der in das File geschrieben werden soll
* @param	int		$perm		Gewuenschte Rechte als Oktalwert (default: 755)
* @return	boolean	[true|false]
*/
	function write_str_in_file($sFileName, $sString='', $perm=755) {
		// if handle file == false return false
		if (!$this->open_file($sFileName, "w+"))	return false;

		// if lock file == false return false
		if (!$this->lock_exclusive())	return false;

		// if write string in file == false return error and false
		if (!fwrite($this->sFileHandle, $sString)) {
			$this->script_alert("FEHLER: Kann Datei nicht beschreiben!");
			return false;
		}
		// unlock file
		$this->unlock();
		// close file
		$this->close_file();
		// ggf. set file-permissions
		$this->check_perm($sFileName, $perm);
		// output
		return true;
	}
/*
Beispiel ohne Race Condition: 
  $datei = '/home/frank/datei';
  
  // temporaere Datei anlegen
  $tempnam = tempnam ('/home/frank/tmp', 'foo');
	
  // Fehler: Datei konnte nicht angelegt werden
  if ($tempnam === false)
    die ("Could not create $tempnam!");
	
  // temporaere Datei zum Schreiben oeffnen
  $fp = fopen ($tempnam, 'w') or die ("Could not open $tempnam!");
	
  // andere Zugriffe abblocken
  flock ($fp, LOCK_EX);
	
  // Daten in die Datei schreiben
  fputs ($fp, $data);
	
  // Datei freigeben
  flock ($fp, LOCK_UN);
  fclose ($fp);
	
  // $datei zu temporaerer Datei hardlinken:
  // schlaegt fehl, wenn $datei schon vorhanden ist
  if (!link ($tempnam, $datei)) {
    // Fehlschlag: Datei bereits vorhanden
  }
  // temporaere Datei loeschen
  unlink ($tempnam);
*/

/**
* Oeffnet die Datei zum schreiben ("a"), lockt sie ("lock_exclusive") und haengt den 
* String ans ende des ggf. schon vorhandenen inhalts und schliesst sie wieder. 
* Tritt ein Fehler auf, wird FALSE zurueckgegeben. 
*
* Beispiel:
* <pre><code>
* if(!$datei_inhalt = $oFile->add_str_in_file($f, $s)) echo "Fehler!";
* </code></pre>
*
* @access   public
* @param	string	$sFileName	File in das geschrieben werden soll
* @param	string	$sString	String der in das File angehaengt werden soll
* @param	int		$perm		Gewuenschte Rechte als Oktalwert (default: 755)
* @return	boolean	[true|false]
*/
	function add_str_in_file($sFileName, $sString='', $perm=755) {
		// check if file
		if (!is_writable($sFileName)) {
			$this->script_alert("FEHLER: Kann Datei nicht finden oder beschreiben!");
			trigger_error("FEHLER: Kann Datei nicht finden oder beschreiben!",E_USER_ERROR);
			return false;
		}
		// open file
		if (!$this->open_file($sFileName, "a")) { return false; }
		// lock file (exclusive)
		if (!$this->lock_exclusive()) { return false; }
		// write file
		if (!fwrite($this->sFileHandle, $sString)) {
			$this->script_alert("FEHLER: Kann Datei nicht beschreiben!");
			trigger_error("FEHLER: Kann Datei nicht beschreiben!",E_USER_ERROR);
			return false;
		}
		// unlock file
		$this->unlock();
		// close file
		$this->close_file();
		// ggf. set file-permissions
		if ($this->get_perm($sFileName) != $perm) {
			$this->set_perm($sFileName, $perm);
		}
		// output
		return true;
	}

/**
* Gibt die Dateigroesse formatiert (mit Groessenangabe) in [Bytes|KB|MB|GB] (case-insensitive! - default: AUTO-ERKENNUNG) zurueck. 
* Die Funktion gibt bei Erfolg die Dateigroesse im gewuenschten Format (inkl. Extension), sonst false zurueck. 
*
* @access   private
* @param	int		$nBytes		Dateigroesse in bytes
* @param	string	$sExt		Extension (Groessen-Format) [AUTO|Bytes|KB|MB|GB] (optional - default: 'AUTO')
* @param	int		$nPrecision	Anzahl Stellen hinter dem Komma (optional - default: 2)
* @return	mixed	[(string)filesize|false]
*/
	function format_filesize($nBytes, $sExt='AUTO', $nPrecision=2) {
		// vars
		$aExtension = array('BYTES'=>' Bytes', 'KB'=>' KB', 'MB'=>' MB', 'GB'=>' GB'); // moegliche groessen (keys ohne leerzeichen und in uppercase!)
		$aBuffer = array(); // assioziatives array der filesizes (keys => $aExtension)
		// check bytes
		if (!is_numeric($nBytes) || $nBytes < 0) {
			$this->script_alert("FEHLER: nBytes ist nicht numerisch oder < 0!");
			trigger_error("FEHLER: nBytes ist nicht numerisch oder < 0!",E_USER_ERROR);
			return false;
		}
		// check auf keine(!) Extension
		if (empty($sExt) || $sExt == NULL) {
			$sExt = 'AUTO';
		}
		// proceed: fuelle filesize-array mit allen verfuegbaren extensions
		$aBuffer['BYTES'] = $nBytes; // filesize in bytes
		foreach ($aExtension as $extKey => $val) {
			if ($extKey != 'BYTES') { $aBuffer[$extKey] = $nBytes = $nBytes / 1024; }
		}
		// wenn eine extension uebergeben wurde:
		$sExt = trim(strToUpper($sExt));
		if ($sExt != 'AUTO') {
			$checkExt = false; // check extension
			foreach($aExtension as $possibleKey => $val) {
				if ($sExt == $possibleKey) { $checkExt = true; break(0); }
			}
			if ($checkExt == false) {
				$this->script_alert("FEHLER: sExt ist nicht im array der verfuegbaren extensions vorhanden!");
				trigger_error("FEHLER: sExt ist nicht im array der verfuegbaren extensions vorhanden!",E_USER_ERROR);
				return false;
			}
		// ansonsten ermittle die "beste" extension...
		} else {
			foreach ($aBuffer as $key => $val) {
				if ($val < 1000) { $sExt = $key; break(0); }
			}
		}
		// output
		return round($aBuffer[$sExt], $nPrecision).$aExtension[$sExt];
	}

/**
* Gibt die Dateigroesse eines Files in [Bytes|KB|MB|GB] (default: AUTO-ERKENNUNG) zurueck. 
* Die Funktion gibt bei Erfolg die Dateigroesse im (gewuenschten / besten) Format, sonst false zurueck. 
* (Uebergibt man dem 2. Parameter einen leerer String -> unformatiert in Bytes, ohne Extension!)
*
* Beispiel:
* <pre><code>
* echo $oFile->get_filesize($sFile); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
* </code></pre>
*
* @access   public
* @param	string	$sFile		File (inkl. Pfad!) dessen Groesse ermittelt werden soll
* @param	string	$sExt		Extension (Groessenangabe) [Bytes|KB|MB|GB|''] (optional - default: KB)
* @param	int		$nPrecision	Anzahl Stellen hinter dem Komma (optional - default: 2)
* @return	mixed	[(string)filesize|false]
*/
	function get_filesize($sFile, $sExt='AUTO', $nPrecision=2) {
		// check vars
		if (empty($sFile) || !file_exists($sFile)) {
			$this->script_alert("FEHLER: Datei ist leer oder nicht vorhanden!");
			trigger_error("FEHLER: Datei ist leer oder nicht vorhanden!",E_USER_ERROR);
			return false;
		}
		// proceed
		$nFilesizeInBytes = filesize($sFile);
		// output
		return ($sExt == '') ? $nFilesizeInBytes : $this->format_filesize($nFilesizeInBytes, $sExt, $nPrecision);
	}

/**
* Get the complete size (in bytes) of a given directory $dir (recursiv) inclusive the amount of scanned files and dirs. Returns an array with: 
* $array[0] = amount of files scanned; 
* $array[1] = amount of dirs scanned; (starts with 0 (first dir = 0)!)
* $array[2] = complete size in bytes; 
* NOTE: simply skips dirs which aren't allowed to red (by permission e.g.)! 
*
* Beispiel:
* <pre><code>
* $a = $oFile->get_dirsize($sDir); // params: $sDir
* </code></pre>
*
* @access   public
* @param	string	$sDir	Verzeichnis dessen Groesse ermittelt werden soll
* @return	array
*/
	function get_dirsize($sDir) {
		// vars
		$nFiles = 0;
		$nDirs = 0;
		$nDirSize = 0;
		// proceed
		if ($fh = @opendir($sDir)) {
			while ($sFile = readdir($fh)) {
				if ($sFile == "." || $sFile == "..") { continue; }
				if (@is_dir($sDir."/".$sFile)) {
					// recoursion
					$buf = $this->get_dirsize($sDir."/".$sFile);
					$nDirSize += $buf[2];
					$nFiles += $buf[0];
					$nDirs += $buf[1];
					$nDirs++;
				} else {
					$nDirSize += @filesize($sDir."/".$sFile);
					$nFiles++;
				}
			}
			closedir($fh);
		}
		$aBuffer[0] = $nFiles;
		$aBuffer[1] = $nDirs;
		$aBuffer[2] = $nDirSize;
		// output
		return $aBuffer;
	}


#----------------------------------------------------------------------------- Verzeichnis-Funktionen

/**
* Namen der Subdirectories eines Directory auslesen. 
*
* Optional kann ein preg_match()-Pattern uebergeben werden, um bestimmte Directory-Namen auszusortieren. 
* Per Voreingestellung werden alle Subdirectories gelesen und im Array zurueckgegeben. 
* Um nur bestimmte Direcory-Namen, die z.B. mit "module" anfangen, zu erhalten, gibt man 
* 'get_subdirs(getenv("DOCUMENT_ROOT"),"/^module/i")' ein. 
*
* Diese Methode arbeitet nicht rekursiv, und verzichtet mit Absicht auf "clearstatcache()". 
*
* @access   public
* @param	string	$sPath		Pfad des zulesenden Directory
* @param	string	$sPattern	preg_match()-Pattern
* @return	Array mit Directory-Namen
*/	
	function get_subdirs($sPath, $sPattern="/./") {
		$aDirectories = array();
		
		// Abschliessende Slashes ("/") entfernen
		$aMatches = array();
		$dummy = preg_match("/(.*)(?<!\/)/", $sPath, $aMatches);
        $sPath = $aMatches[0]."/";
		
		$sDirHandle	= @opendir($sPath);
		while ($sDir = @readdir($sDirHandle)) {
			if ($sDir == "." || $sDir == "..") { continue; }
			
			if (is_dir($sPath.$sDir) && preg_match($sPattern,$sDir)) {
				$aDirectories[]	= $sDir;
			}
		}
		closedir($sDirHandle);
		
		return $aDirectories;
	}

/**
* TODO: UNDER CONSTRUCTION!!!
*
* Wie kann ich den Inhalt eines Verzeichnisses samt dem Inhalt aller Unterverzeichnisse ausgeben: 
* Um nicht nur den Inhalt des aktuellen Verzeichnisses, sondern auch den Inhalt aller Unterverzeichnisse 
* ausgeben zu koennen, muss man eine rekursive Funktion verwenden. Diese ruft sich bei Bedarf selbst auf. 
* Im nachfolgenden Beispiel durchlaeuft die Funktion show_dir jeweils das aktuelle Verzeichnis. Wird Datei 
* gefunden, wird der Dateiname ausgegeben. Findet die Funktion ein Verzeichnis, dann wird der Verzeichnisname 
* fett ausgegeben und die Funktion ruft sich mit dem Unterverzeichnis als Parameter selbst wieder auf.
*
* @access   public
* @param:	string	dir		Verzeichnis das rekursiv durchsucht wird (default: aktuelles Verzeichnis)
* @param:	int		pos		Anzahl Leerzeichen um die die erste ebene eingerueckt wird (default: 2)
* @param:	string	regex	Regulaerer Ausfruck um Files zu filtern
*/
	function show_dir($dir, $pos=2, $regex='') {
		if (empty($dir) || $dir == $_SERVER["PHP_SELF"]) { // erzeuge aktuelles Verzeichnis
			$dir = $_SERVER['DOCUMENT_ROOT'].str_replace(basename($_SERVER["PHP_SELF"]), '', $_SERVER["PHP_SELF"]);
		}
		$sStr = "";
		if($pos == 2) { $sStr .= HR."<pre>"; }
		
		$handle = @opendir($dir);
		while ($file = @readdir($handle)) {
			if ($file == '.' || $file == '..') { continue; }
			if (!empty($regex) && preg_match($regex, $file)) { continue; }
			
			if(is_dir($dir.$file)) {
			    #printf ("% ".$pos."s <b>%s</b>\n", "|-", $file);
			    $sStr .= "|-".$pos."|-".$file."\n";
				#show_dir($dir.$file."/", $pos + 3);
			} else {
			    #printf ("% ".$pos."s %s\n", "|-", $file);
				$sStr .= "|-".$pos."|-".$file."\n";
			   # echo $file."\n";
			}
		}
		@closedir($handle);
		
		if($pos == 2) { $sStr .= "</pre>".HR; }
		
		return $sStr;
	}
// END


#----------------------------------------------------------------------------- Loesch-Funktionen

/**
* Entfernt einen Ordner(inkl. Dateien) -> kein Rueckgabewert!
*
* @access   public
* @param	string	Name des zu loeschenden Directories
*/
	function delete_folder($sFolder) {
		$aFiles = Array();
		exec('find "'.$sFolder.'" -type f -maxdepth 1', $aFiles); // get files/dirs (recoursiv!)
		foreach($aFiles as $sTodelete) { @unlink($sTodelete); } // loesche dateien
		@rmdir($sFolder); // loesche verzeichnis
	}

/**
* Leert einen Ordner (loescht alle darin enthaltenen Dateien aber nicht den Ordner) -> kein Rueckgabewert!
*
* @access   public
* @param	string	Name des zu leerenden Directories
*/
	function clear_folder($sFolder) {
		if (!$sFolder) return;
		if (!is_dir($sFolder)) return "'".$sFolder."' is no dir!";
		$dir = opendir($sFolder); // oeffnet das Verzeichnis
		while ($file = readdir($dir)) {
			if ($file=='.' || $file=='..') { continue; }
			if (is_dir($sFolder.$file)) { continue; }
			@unlink($sFolder.$file); // loescht die datei
		}
		closedir($dir); // schliesst das Verzeichnis
	}

	/**
	 * 
	 * Löscht einen Folder, samt seiner Files rekursiv.
	 * 
	 * @access public
	 * @param String Name des zu leerenden Directories
	 * @return array Anzahl gelöschter Files und Folder
	 */

	function recursivFolderDel($sFolder) {
	   if(!$dh = @opendir($sFolder)) { 
	   	trigger_error('Folder doesn\'t exist!',E_USER_ERROR);
	   	return false; 
	   }
	   if(!is_array($aFiFo)) $aFiFo = array();
	   while (($obj = readdir($dh))) {
	     if($obj=='.' || $obj=='..') continue;
	     if (!@unlink($sFolder.'/'.$obj)) {
	         $aFiFo = array_merge($this->recursivFolderDel($sFolder.'/'.$obj),$aFiFo);
	     } else {
	         $aFiFo['file']++;
	     }
	   }
	   if (@rmdir($sFolder)) $aFiFo['dir']++;
	   return $aFiFo;	
	}

/**
* Entfernt die in einem Array uebergebenen Dateien oder eine als String uebergebene Datei. -> kein Rueckgabewert!
*
* @access   public
* @param	mixed	$aFiles	Namen der zu loeschenden Dateien als numerisches Array, oder einzelne Datei als string
*/
	function delete_files($aFiles) {
		if(!is_array($aFiles)) { $aFiles = Array($aFiles); }
		foreach($aFiles as $sFile) {
			if (file_exists($sFile)) { @unlink($sFile); }
		}
	}


#----------------------------------------------------------------------------- Permissions

/**
* Zeigt die aktuelle (FIle-/Dir-)Infos als string. 
*
* @access   public
* @return	string	Permissions
*/
	function get_info($sFile) {
		if (!file_exists($sFile)) {
			trigger_error("You must create the file: ".$sFile." before running this script",E_USER_WARNING);
			exit("You must create the file: ".$sFile." before running this script");
		}
		$atime = date("r",fileatime($sFile));
		$mtime = date("r",filemtime($sFile));
		$ctime = date("r",filectime($sFile));
		$fs = filesize($sFile);
		$owner = $this->get_owner($sFile);
		$perm = $this->get_perm($sFile, 'unixstr');
		$perm .= ' ('.$this->get_perm($sFile, 'oktal').')';
		$ft = filetype($sFile);
		
		return "File $sFile <br>\n".
			"Last accessed: $atime <br>\n".
			"Last modified: $mtime <br>\n".
			"Last changed: $ctime <br>\n".
			"File size: $fs bytes <br>\n".
			"Owner: $owner <br>\n".
			"Permissions: $perm <br>\n".
			"File type: $ft <br>\n";
	}

/**
* Zeigt die aktuelle (File-/Dir-)Owner-einstellung defaultmaessig als Name (string). 
* Wenn man als zweiten Parameter 'uid' eingibt, wird die UserID (uid) als integer zurueckgegeben.
*
* @access   public
* @param	string	$type		['name'(default) | 'uid']
* @return	string	Permissions
*/
	function get_owner($sFile, $type='name') {
		$uid = fileowner($sFile);
		if ($type == 'name') {
			$pwd = posix_getpwuid($uid);
			$owner = $pwd['name'];
		} else {
			$owner = $uid;
		}
		return $owner;
	}

/**
* Zeigt die aktuelle (File-/Dir-)Permissions-einstellung defaultmaessig als dreistelligen Oktalwert (z.B. "755"). 
* Wenn man als zweiten Parameter 'unixstr' eingibt, werden die permissions im Format "rwxrwxrwx" zurueckgegeben. 
* Wenn man als zweiten Parameter '' eingibt, werden die permissions im PHP-Format (integer) zurueckgegeben.
*
* @access   public
* @param	string	$type		['oktal'(default) | 'unixstr' | '']
* @return	string	Permissions
*/
	function get_perm($sFile, $type='oktal') {
		clearstatcache();
		if ($type == 'oktal') {
			$perms = base_convert(fileperms($sFile), 10, 8);
			$permstr = substr($perms, (strlen($perms) - 3));
		} elseif ($type == 'unixstr')  {
			$perms = fileperms($sFile);
			$permstr = $perms & 0x0100 ? 'r' : '-';
			$permstr .= $perms & 0x0080 ? 'w' : '-';
			$permstr .= $perms & 0x0040 ? 'x' : '-';
			$permstr .= $perms & 0x0020 ? 'r' : '-';
			$permstr .= $perms & 0x0010 ? 'w' : '-';
			$permstr .= $perms & 0x0008 ? 'x' : '-';
			$permstr .= $perms & 0x0004 ? 'r' : '-';
			$permstr .= $perms & 0x0002 ? 'w' : '-';
			$permstr .= $perms & 0x0001 ? 'x' : '-';
		} else {
			$permstr = fileperms($sFile);
		}
		return $permstr;
	}

/**
* Setzt (File-/Dir-)Permissions-einstellung. 
*
* @access   public
* @param	string	$sFile		Pfad des Directories bzw. der Datei
* @param	int		$perm		Gewuenschte Rechte als Oktalwert
*/
	function set_perm($sFile, $perm=755) {
		if (strlen($perm) == 3) {
			$perm = '0'.$perm;
		}
		$chk = @chmod($sFile, octdec($perm)); // Darf nicht in "if (!...)" stehen!!!
		if (!$chk) {
			$this->script_alert("FEHLER: Cannot change the mode of file ($sFile)!");
			trigger_error("FEHLER: Cannot change the mode of file ($sFile)!",E_USER_ERROR);
		}
	}

/**
* Ueberprueft die aktuelle (File-/Dir-)Permissions-einstellung und korrigiert sie ggf. 
*
* @access   public
* @param	string	$sFile		Pfad des Directories bzw. der Datei
* @param	int		$perm		Gewuenschte Rechte als Oktalwert
* @see		set_perm()
*/
	function check_perm($sFile, $perm='') {
		if ($perm == '') {
			if (!is_writable($sFile)) {
				$this->set_perm($sFile, 755);
			}
		} else {
			if ($this->get_perm($sFile) != $perm) {
				$this->set_perm($sFile, $perm);
			}
		}
	}


/**
* entfernt kritische Umlaute und Sonderzeichen aus einem Dateinamen-Vorschlag und wandelt in Kleinbuchstaben. 
*
* Beispiel:
* <pre><code>
* $better_filename = $oFile->return_good_filename($sStr);
* </code></pre>

* @access   public
* @param	string	$sStr
* @return	string	good filename
*/
	function return_good_filename($sStr) {
		// make the string lowercase
		$sStr	= strToLower($sStr);
		
		// array with replace tokens
		$aTranslate	= array('ä'	=> "ae", 
							'ö'	=> "oe",
							'ü'	=> "ue",
							'ß'	=> "ss",
							" "	=> "_",
							"..."	=> "");
							
		// replace action						
		$sStr	= strtr($sStr, $aTranslate);
		
		// remove all digits except for 0-9, a-z, dot and underscore 
		$sStr = preg_replace("/[^a-z0-9\._]/", "", $sStr);	
		
		// return string
		return $sStr;
	}

/**
* Zeigt die aktuelle Debug-Mode-einstellung. 
*
* @access   public
* @return	boolean	[true|false]
*/
	function get_debug_mode() {
		return (!$this->debug) ? 'false' : 'true';
	}

/**
* Setzt die Debug-Mode-Einstellung auf true. 
*
* Beispiel:
* <pre><code>
* $oFile->set_debug_mode_true();
* </code></pre>
*
* @access   public
*/
	function set_debug_mode_true() {
		$this->debug = true;
	}

/**
* Erzeugt im Fehlerfall ein JS-Alert script. Es kann direkt eine Fehlermeldung uebergeben werden.
*
* @access 	public
* @param	string	$sAlert	Fehlermeldung (optional - default: '')
*/
	function script_alert($sAlert='') {
		// if sAlert != empty set this alert
		if (!empty($sAlert)) $this->alert = 'CLASS "'.__CLASS__.'" '.$sAlert;
		// if sAlert != empty && this debug == true output jscript message
		if (!empty($this->alert) && $this->debug == true) {
			echo '<script language="JavaScript" type="text/javascript">escape(\''.$this->alert.'\'); alert(unescape(\''.$this->alert.'\'))</script>';
		} // end if
	}

} // END of class
?>