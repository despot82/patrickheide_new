<?php // Class archive
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung ein ZIP-Archiv zu erstellen. 
* ACHTUNG: Voraussetzung ist ein UNIX-Rechner mit installiertem Programm "zip"! 
*
* Example: 
* <pre><code> 
* // archiviere das gesamte aktuelle verzeichnis inkl. unterverzeichnisse in eine datei "neues_archiv.zip" 
* $oArchive =& new archive("neues_archiv.zip");
* $zip = $oArchive->zip(); 
* if ($zip == false) { $oArchive->print_error(); } else { print_r($zip); }
* </code></pre>
*
* @access   public
* @package  service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2003-10-20
*/
class archive {
	/*
	TODO: Auto-Erkennung ob UNIX bzw. zip ueberhaupt unterstuetzt wird!
	TODO: tar einbauen
	TODO: getList of zippedFiles...
		if (($file_list=$tar->ListContent()) != 0) { 
			foreach($file_list as $v) { 
				printf("Name: %s  Size: %d   modtime: %s mode: %s<br>", $v['filename'], $v['size'], $v['mtime'], $v['mode']); 
			} 
		}
	*/
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse archive:
		----------------------------------------------------------------------------
		konstruktor archive($sArchiveFile='')
		function includeFiles($file)
		function excludeFiles($file)
		function zip($sSource='.', $bRecoursive=true)
		function unzip($sUnzipPath='.', $bJunkPaths=false)
		function print_error()
		----------------------------------------------------------------------------
		HISTORY:
		1.0 / 2003-10-20
	*/

#-----------------------------------------------------------------------------

/**
* @access   public
* @var	 	string	Zielverzeichnis und Dateiname des Archives
*/
	var $sArchiveFile = '';
/**
* @access   private
* @var	 	string	fertiger String mit Dateien, die explitit nicht mit gezippt werden sollen
*/
	var $sIncludeFiles = '';
/**
* @access   private
* @var	 	string	fertiger String mit Dateien, die explitit gezippt werden sollen
*/
	var $sExcludeFiles = '';
/**
* @access   public
* @var	 	string	Fehlermeldung
*/
	var $zipE_string = '';

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das archive-Objekt und setzt den (Pfad und) Dateinamen des Archives "$sArchiveFile" (relativ!) 
*
* Beispiel: 
* <pre><code> 
* $oArchive =& new archive("relativer/pfad/zum/archiv/neu.zip"); // params: $sArchiveFile [default: date("YmdHis").".zip"] 
* </code></pre>
*
* @access   public
* @param 	string	$sArchiveFile	Zielverzeichnis und Dateiname des Archives (default: "./timestamp.zip")
* @return   string	$this->sArchiveFile
*/
	function archive($sArchiveFile='') {
		if ($sArchiveFile == '') { $sArchiveFile = date("YmdHis").".zip"; }
		$this->sArchiveFile = $sArchiveFile;
		
		return $this->sArchiveFile;
	}

#-----------------------------------------------------------------------------

/**
* fuegt Datei(en) zu einer Sammlung ($this->sIncludeFiles), die explizit inkludiert werden sollen. 
* ACHTUNG: In diesem Fall werden NUR die angegebenen File(s) archiviert (wird diese Methode nicht 
* verwendet, werden alle Dateien im angegebenen Verzeichnis archiviert)!
* NOTE: Diese Methode kann vor der ausfuehrenden Methode [z.B. "zip()"] auch mehrfach verwendet werden.
*
* (ACHTUNG: "file.jpg" fuegt "file.jpg" aber auch "irgendein_file.jpg" hinzu, sofern es existiert! 
* d.h. man kann auch nur eine Datei-Endung - bspw. ".jpg" - angeben, um alle JPGs hinzuzufuegen!) 
* Mehrere Dateien kann man auch auf einmal als Array uebergeben. 
*
* Beispiel: 
* <pre><code> 
* $oArchive->includeFiles("import.sql"); // params: $file 
* $oArchive->includeFiles(".jpg"); 
* $oArchive->includeFiles(array(".jpg",".gif")); 
* </code></pre>
*
* @access   public
* @param	mixed (string|array)	$file	file(s)/path/wildcard als string oder array
* @return   string	$this->sIncludeFiles
*/
	function includeFiles($file) {
		
		if (!$file) return; // check vars
		if (is_string($file)) { $file = array($file); }
		
		// add
		foreach ($file as $f) { $this->sIncludeFiles .= " -i \*".$f; }
		
		return $this->sIncludeFiles;
	}

/**
* gibt die Anweisung Dateien explizit NICHT mit in das Archiv zu nehmen (exkludieren). 
*
* (Konventionen siehe "includeFiles()"). 
* Mehrere Dateien kann man auch auf einmal als Array uebergeben. 
*
* Beispiel: 
* <pre><code> 
* $oArchive->excludeFiles(array(".jpg",".gif")); // params: $file 
* </code></pre>
*
* @access   public
* @see		"includeFiles()"
* @param	mixed (string|array)	$file	file(s)/path/wildcard als string oder array
* @return   string	$this->sExcludeFiles
*/
	function excludeFiles($file) {
		
		if (!$file) return; // check vars
		if (is_string($file)) { $file = array($file); }
		
		// add
		#foreach ($file as $f) { $this->sExcludeFiles .= " -x \*".$f; }
		foreach ($file as $f) { $this->sExcludeFiles .= " -x *".$f; }
		
		return $this->sExcludeFiles;
	}

/**
* erstellt das Archiv als ZIP-File. Benoetigt optional:
* - Datei-Name oder Verzeichnis "$sSource" (relativ!) (default: aktuelles Verzeichnis)
* - Unterverzeichnisse mit einschliessen? "$bRecoursive" (default: true wenn sSource==Verzeichnis). 
*
* Beispiel: 
* <pre><code> 
* $oArchive->zip(); 
* </code></pre>
*
* @access   public
* @param 	string	$sSource		Datei-Name oder Verzeichnis-Pfad, welche(r/s) archiviert werden soll (default: "." -> also aktuelles Verzeichnis)
* @param 	boolean	$bRecoursive	wenn $sSource ein Verzeichnis, dann auch alle Unterverzeichnisse mit einschliessen
* @return   mixed (false bei fehler | array bei erfolg)
*/
	function zip($sSource='.', $bRecoursive=true) {
		// initialize error-vars
		$zipE_array = "";
		$zipE_integer = "";
		// build zip-command
		$sZIPcommand = "zip ";
		if ($bRecoursive == true && is_dir($sSource)) { $sZIPcommand .= "-r "; } // -r => recoursive
		$sZIPcommand .= $this->sArchiveFile." ".$sSource;
		if ($this->sIncludeFiles != '') { $sZIPcommand .= " ".$this->sIncludeFiles; }
		if ($this->sExcludeFiles != '') { $sZIPcommand .= " ".$this->sExcludeFiles; }
		// do it
		$zipE_string = exec($sZIPcommand, $zipE_array, $zipE_integer); // store messages in vars
		
		if ($zipE_integer == 0) {
			return $zipE_array;	// bei erfolg return msg-array
		} else {
			$this->zipE_string = $zipE_string;
			return false;		// im fehlerfall return false
		}
		/*
		SYNOPSIS:
		zip [-aABcdDeEfFghjklLmoqrRSTuvVwXyz!@$] [-b path] [-n suffixes] [-t mmddyyyy] [-tt mmddyyyy] [zipfile [file1 file2 ...]] [-xi list]
		*/
	}

/**
* entpackt das ZIP-File. 
* Defaultmaessig wird ein Verzeichnis mit dem Namen des Archives erstellt und das Archiv darin entpackt. 
* Soll das Archiv im aktuellen Verzeichnis entpackt werden, muss ein Punkt "." uebergeben werden.
*
* Beispiel: 
* <pre><code> 
* $oArchive->unzip(); 
* </code></pre>
*
* @access   public
* @param 	string	$sUnzipPath		Pfad wohin das Archive entpackt werden soll (Zielverzeichnis)
* @param 	boolean	$bJunkPaths		Pfadstruktur wird bei true verworfen + alle Dateien in das Zielverzeichnis entpackt
* @return   mixed	(false bei fehler | array bei erfolg)
*/
	function unzip($sUnzipPath='', $bJunkPaths=false) {
		// check vars
		if ($sUnzipPath == '') { // wenn kein Zielverzeichnis, dann name des archivs verwenden
			$sUnzipPath = substr(basename($this->sArchiveFile), 0, strrpos($this->sArchiveFile, ".")); // name ohne pfad + ext.
		}
		// initialize error-vars
		$zipE_array = "";
		$zipE_integer = "";
		// build unzip-command
		$sZIPcommand = "unzip ";
		if ($bJunkPaths == true) { $sZIPcommand .= "-j "; } // -j => junk paths
		$sZIPcommand .= $this->sArchiveFile." ";
		if ($this->sExcludeFiles != '') { $sZIPcommand .= " ".$this->sExcludeFiles; }
		if ($sUnzipPath != '.') { $sZIPcommand .= " -d ".$sUnzipPath; }
		// do it
		$zipE_string = exec($sZIPcommand, $zipE_array, $zipE_integer); // store messages in vars
		
		if ($zipE_integer == 0) {
			return $zipE_array;	// bei erfolg return msg-array
		} else {
			$this->zipE_string = $zipE_string;
			return false;		// im fehlerfall return false
		}
		/*
		SYNOPSIS:
		unzip [-Z] [-cflptuvz[abjnoqsCLMVX$/]] file[.zip] [file(s) ...] [-x xfile(s) ...] [-d exdir]
		-j	junk paths. The archive's directory structure is not recreated; all files 
		 	are deposited in the extraction directory (by default, the current one).
		
		HIER: "unzip [-j] $this->sArchiveFile [$this->sExcludeFiles] [-d $sUnzipPath]"
		*/
	}

#-----------------------------------------------------------------------------

/**
* gibt die Fehlermeldung direkt als String aus. 
*
* Beispiel: 
* <pre><code> 
* $oArchive->print_error(); 
* </code></pre>
*
* @access   public
* @return   string	$this->zipE_string
*/
	function print_error() {
		if ($this->zipE_string != '') { echo $this->zipE_string; }
	}


#-----------------------------------------------------------------------------
} // END of class

?>