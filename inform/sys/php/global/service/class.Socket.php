<?php
/**
 * Diese Klasse stellt einen vereinfachten Zugriff auf die PHP-Socket-Funktionen bereit.
 * 
 * Beispiele:
 * <pre><code>
 * $oSocket =& new Socket();
 * $aResponse = $oSocket->transfer($sToUrl, $sRequest);
 * // oder
 * $aFileLines = $oSocket->getFile($sUrl);
 * // oder
 * $bIsValidUrl = $oSocket->validateUrl($sUrl);
 * // oder
 * $sState = $oSocket->getICQ($uin);
 * // print all class vars
 * $oSocket->debug()
 * </code></pre>
 *
 * @access   public
 * @package  service
 * @author Andy Fehn <af@design-aspekt.com>
 * @version 1.0
 */

class Socket {

/**
 * @access	private
 * @var		integer	Port ueber den mit dem Server verbunden werden soll (optional, default: 80)
 */
	var $port		= 80;
/**
 * @access	private
 * @var		integer	Timeout in Sekunden, nachdem abgebrochen werden soll (optional, default: 30)
 */
	var $timeout	= 30;
/**
 * @access	private
 * @var		string	URL des targets - kann ein Server oder eine Datei sein
 */
	var $url		= '';
/**
 * @access	private
 * @var		string	URL des Servers
 */
	var $server		= '';
/**
 * @access	private
 * @var		string	URL der Datei
 */
	var $file		= '';
/**
 * @access	private
 * @var		integer	Datei-Zeiger => Rueckgabewert von fsockopen()
 */
	var $fp			= false;
/**
 * @access	private
 * @var		boolean	Check Internet-Connection vor fsockopen() mittels fopen()
 */
	var $bCheckConn = true;

# -------------------------------------------------------------------------------------

/**
 * Konstruktor initialisiert das Object und setzt default-vars.
 * 
 * @access	public
 * @param	string	$port		Port, ueber den connected werden soll (optional, default: 80)
 * @param	string	$timeout	Timeout in Sekunden, nachdem abgebrochen werden soll (optional, default: 30)
 * @param	string	$bCheckConn	Check Internet-Connection vor fsockopen() mittels fopen() (optional, default: true)
 * @return	void
 */
	function Socket($port=80, $timeout=30, $bCheckConn=true) {
		$this->port			= $port;
		$this->timeout		= $timeout;
		$this->bCheckConn	= $bCheckConn;
	}

# -------------------------------------------------------------------------------------

/**
 * Versucht eine Verbindung zum als URL uebergebenen File aufzubauen. 
 * Ggf. wird vorher versucht mittels fopen() zu ermitteln, ob eine Internet-Verbindung besteht.
 * Bei Misserfolg wird false zurueckgegeben.
 * 
 * @access	private
 * @param	string	$url 		Datei, die gelesen werden soll
 * @return	array
 */
	function open($url) {
		// extract some url vars
		$this->url		= $url;
		$this->server	= str_replace('http://', '', $url);
		$this->server	= str_replace('https://', 'ssl://', $this->server); // connect to a SSL server by HTTPS -> Port 443...
		$this->file		= strstr($this->server, "/");
		$this->server	= str_replace($this->file, '', $this->server);
		
		// check internet-connection (da fsockopen() bei fehlender Internet-Verbindung sehr lange braucht und viele Fehler generiert!)
		if ($this->bCheckConn && strstr($this->url, 'http://')) {
			if (!$check = @fopen($this->url, "r")) {
				trigger_error("Fehler bei der Verbindung zur URL: ".$this->url." (keine Internet-Verbindung?)!",E_USER_ERROR);
				return false; 
			}
		}
		// get file via socket connection
		$this->fp = fsockopen($this->server, $this->port, &$errno, &$errstr, $this->timeout);
		if (!$this->fp) {
			trigger_error("Fehler bei der Verbindung mit fsockopen() zum Server: ".$this->server."!" .
						"ERROR: $errstr ($errno)",E_USER_ERROR);
			return false;
		}
	}

/**
 * Schliesst eine Verbindung.
 * Bei Misserfolg wird false zurueckgegeben.
 * 
 * @access	private
 */
	function close() {
		if (!$this->fp) return false;
		fclose($this->fp);
	}

# -------------------------------------------------------------------------------------

/*
	// format data as string
	$sData = '';
	$trenner = '';
	foreach ($aData as $key => $value) {
		$sData .=  $trenner.$key."=".trim(str_replace("\t", ' ', $value));
		$trenner = "\t"; // tab als trennzeichen
	}
	$sData .= "\n";
*/

/**
 * Uebermittelt dem uebergebenen Rechner den uebergebenen String und gibt die Antwort als einfaches Array zurueck.
 * Bei Misserfolg wird false zurueckgegeben.
 * 
 * @access	public
 * @param	string	$to 		Rechner, zu dem uebermittelt werden soll
 * @param	string	$request 	Daten, die uebermittelt werden sollen
 * @return	array
 */
	function transfer($to, $request) {
		// connect
		$this->open($to);
		if (!$this->fp) return false;
		
		// send request
		fputs($this->fp, $request);
		
		// get answer
		$lines = array();
		while ($line = fgets($this->fp, 4096)) {
			$lines[] = $line;
		}
		
		// close
		$this->close();
		
		// output
		return $lines;
	}

# -------------------------------------------------------------------------------------

/**
 * Liest das als URL uebergebene File aus und gibt die gelesenen Zeilen als einfaches Array zurueck.
 * Bei Misserfolg wird false zurueckgegeben.
 * 
 * @access	public
 * @param	string	$url 		Datei, die gelesen werden soll
 * @return	array
 */
	function getFile($url) {
		// connect
		$this->open($url);
		if (!$this->fp) return false;
		
		// get file
		$lines = array();
		$request = "GET ".$this->file." HTTP/1.0\r\nHost: ".$this->server."\r\n\r\n";
		fputs($this->fp, $request);
		while ($line = fgets($this->fp, 4096)) {
			$lines[] = $line;
		}
		$this->close();
		
		// output
		return $lines;
	}

/**
 * Checkt eine Webseite/URL auf Erreichbarkeit.
 * Bei Unerreichbar wird false zurueckgegeben.
 * 
 * @access	public
 * @param	string	$url 		Datei, die validiert werden soll
 * @return	boolean
 */
	function validateUrl($url) {
		// check vars
		$url_parts = @parse_url($url);
		if (empty($url_parts["host"])) return false;
		$documentpath = (!empty($url_parts["path"])) ? $url_parts["path"] : "/";
		if (!empty($url_parts["query"])) {
			$documentpath .= "?".$url_parts["query"];
		}
		$host = $url_parts["host"];
		$port = $url_parts["port"];
		// Now (HTTP-)GET $documentpath at $host";
		if (empty($port)) $port = "80";
		$socket = @fsockopen($host, $port, $errno, $errstr, 30);#$this->open($url);
		if (!$socket) return false;
		fwrite ($socket, "HEAD ".$documentpath." HTTP/1.0\r\nHost: $host\r\n\r\n");
		$http_response = fgets( $socket, 22 );
		if (ereg("200 OK", $http_response, $regs)) {
			return true;
			fclose($socket);#$this->close();
		} else {
			// echo "HTTP-Response: $http_response<br>";
			return false;
		}
	}

/**
 * Checkt den Online-Status eines ICQ-Users.
 * Bei Misserfolg wird false zurueckgegeben.
 * 
 * @access	public
 * @param	string	$uin 		ICQ-User-ID, dessen Status ermittelt werden soll
 * @return	array
 */
	function getICQ($uin) {
		// check vars
		if (!is_numeric($uin)) return false;
		$this->bCheckConn = false; // hier kein check!
		$this->open('status.icq.com');
		
		// send request
		$request = "HEAD /online.gif?icq=$uin HTTP/1.0\r\n"
					."Host: web.icq.com\r\n"
					."Connection: close\r\n\r\n";
		fputs($this->fp, $request);
		
		// get response
		do {
			$response = fgets($this->fp, 1024);
		}
		while (!feof($this->fp) && !stristr($response, 'Location'));
		$this->close();
		
		// check status
		if (strstr($response, 'online1')) return 'Online';
		if (strstr($response, 'online0')) return 'Offline';
		if (strstr($response, 'online2')) return 'N/A';
		// N/A means, this User set the Option, his Online
		// Status cannot be shown over the Internet
		
		// default
		return false;
	}

# -------------------------------------------------------------------------------------

/**
 * Gebe alle Klassenvariablen zurueckn.
 * 
 * @access	public
 * @return	string
 */
	function debug() {
		$nl = '<br />';
		echo 'port: '	. $this->port	. $nl;
		echo 'timeout: '. $this->timeout. $nl;
		echo 'url: '	. $this->url	. $nl;
		echo 'server: '	. $this->server	. $nl;
		echo 'file: '	. $this->file	. $nl;
		echo 'fp: '		. $this->fp		. $nl;
	}

# -------------------------------------------------------------------------------------
} // END class
?>