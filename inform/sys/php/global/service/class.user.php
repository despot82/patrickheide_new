<?php // Class user
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung die Daten eines oder mehrerer Home-/Intranet-Nutzer zu erlangen. 
*
* Example: 
* <pre><code> 
* // init
* require_once("php/class.user.php"); // ist schon in der _include_all.php passiert!
* $oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL) 
* // Den Namen eines Users aufgrund seiner Id ermitteln
* echo $oUser->getUserName($aData['userid']); // params: $userid
* // eDen Namen mehrerer User aufgrund ihrer Id ermitteln
* $aUsername = $oUser->getUserNames('1,5,12,17'); // params: $userid[,$bValidOnly=true] 
* $aUsername = $oUser->getUserNames(array('1','5','12','17')); // params: $userid[,$bValidOnly=true] 
* // Alle Daten eines Users (ohne Premissions!) aufgrund seiner Id ermitteln
* $aUser = $oUser->getUserdata($aData['userid']); // params: $userid 
* // Die Namen eines oder mehreren Offices aufgrund ihrer ID ermitteln
* $aOffice = $oUser->getOfficeNames($aData['office_id']); // params: [$officeid] 
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.5 / 2006-04-04 [NEU: Backticks + $entries / Pagination + "getOfficeUserIds()"]
*/
class user {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse user:
		----------------------------------------------------------------------------
		konstruktor user(&$oDb, $config=NULL) // params: &$oDb[,$aConfig=NULL)
		
		function countAllUsers($bValidOnly=true)
		function getAllUserData($bValidOnly=true) // params: [$bValidOnly=true]
		function getAllUserNames($bValidOnly=true) // params: [$bValidOnly=true]
		function getUserNames($userid, $bValidOnly=true) // params: $userid[,$bValidOnly=true]
		function getUserName($userid) // params: $userid
		function getUserdata($userid) // params: $userid
		
		function getOfficeName($officeid) // params: [$officeid]
		function getOfficeNames($officeid) // params: [$officeid]
		function getAllOfficeNames($bWithUsers=false) // params: [$bWithUsers=false] 
		function getAllOffices($bWithUsers=false) // params: [$bWithUsers=false]
		function countAllOffices($bWithUsers=false)
		function getOfficeUserIds($officeid) // params: $officeid
		
		function getUsergroupName($ugid) // params: [$ugid]
		function getUsergroupNames($ugid) // params: [$ugid]
		function getAllUsergroupNames($bWithUsers=false) // params: [$bWithUsers=false] 
		function getAllUsergroups($bWithUsers=false) // params: [$bWithUsers=false]
		function countAllUsergroups($bWithUsers=false)
		function getAllUserIdsByUsergroup($ugid)
		----------------------------------------------------------------------------
		HISTORY:
		1.5 / 2006-04-04 [NEU: Backticks + $entries / Pagination + "getOfficeUserIds()"]
		1.4 / 2005-06-08 [NEU: alle "*Usergroup*()" Methoden]
		1.31 / 2005-04-28 [BUGFIX: in "getUserName()"]
		1.3 / 2005-04-26 [NEU: "getOfficeName()" + "countAllOffices()" + "countAllUsers()"]
		1.21 / 2005-04-25 [NEU: "$buffer['name']" in "getUserdata()" hinzugefuegt]
		1.2 / 2005-04-05 [NEU: "getOfficeNames()", "getAllOfficeNames()"]
		1.1 / 2005-03-29 [NEU: "getAllUserData()", "getOffice()", "getAllOffices()"]
		1.0 / 2005-03-11
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	array	Konfiguration
*/
	var $aConfig = array();
/**
* @access   private
* @var	 	array	importiertes globales Setup-Array
*/
	var $aENV = '';
/**
* @access   private
* @var	 	object	DB-Connect-Objekt
*/
	var $oDb = '';
/**
* @access   private
* @var	 	string	Fehlermeldung
*/
	var $error_string = '';
/**
* @access   private
* @var	 	int		Anzahl Datensaetze der letzten Query
*/
	var $entries = 0;

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das user-Objekt und interne Variablen. 
* Mit dem zweiten (optionalen) Parameter kann man z.B. die Ausgabe des Namens bei "getUserName()" beeinflussen.
* Beispiel: $oUser =& new user(array('nameformat' => "surname, firstname"));
* -> Die erkannten DB-Felder ('surname' und 'firstname') werden durch ihre values erstzt.
* Der Defaultwert sollte in der "setup.php" als "$aENV['username_format']" definiert sein.
*
* Beispiel: 
* <pre><code> 
* $aConfig['nameformat'] = "surname, firstname";
* $oUser =& new user($oDb, $aConfig); // params: &$oDb[,$aConfig=NULL) 
* </code></pre>
*
* @access   public
* @param 	object	$oDb		DB-Objekt
* @param 	array	$aConfig	Konfiguration z.B. des Namensformates (optional)
* @return   array	$this->aConfig
*/
	function user(&$oDb, $aConfig=NULL) {
		// DB
		global $aENV;
		$this->aENV	=& $aENV;
		$this->oDb	= $oDb;
		// init config
		$this->aConfig = (!is_null($aConfig)) ? $aConfig : array();
		if (!isset($this->aConfig['nameformat'])) {
			$this->aConfig['nameformat'] = (isset($aENV['username_format'])) ? $aENV['username_format'] : "firstname"; // fallback
		}
		// ERRORS
		if (!is_object($this->oDb)) {
			die('"'.basename(__FILE__).'"-ERROR: Es wurde kein globales DB-Objekt gefunden!');
		}
		if (!isset($this->aENV['table']['sys_user']) || empty($this->aENV['table']['sys_user'])) {
			die("class.user.php -> ERROR: aENV['table']['sys_user'] nicht definiert!");
		}
		// output
		return $this->aConfig;
	}

#-----------------------------------------------------------------------------

/**
* Liefert die Gesamt-Anzahl Datensaetze der zuletzt ausgefuehrten Query (ggf. VOR LIMIT!). 
* 
* @access   public
* @return   int		Anzahl Datensaetze
*/
	function getEntries() {
		return $this->entries;
	}

#-----------------------------------------------------------------------------

/**
* Ermittelt die Anzahl ALLER user. 
* Optional kann als zweiter Parameter "false" uebergeben werden, um auch die geloeschten sowie den DA_Service-Zugang einzubeziehen. 
*
* Beispiel: 
* <pre><code> 
* echo $oUser->countAllUsers(); // params: [$bValidOnly=true]
* </code></pre>
*
* @access   public
* @param	boolean	$bValidOnly	(false, wenn auch die geloeschten sowie der DA_Service-Zugang ermittelt werden soll - default: true)
* @return   int		Anzahl der User
*/
	function countAllUsers($bValidOnly=true) {
		// SQL
		$sConstraint	= ($bValidOnly == true) ? "WHERE `flag_da_service` = '0' AND `flag_deleted` = '0'" : '';
		$this->oDb->query("SELECT `id` FROM `".$this->aENV['table']['sys_user']."` ".$sConstraint);
		// output
		return $this->oDb->num_rows();
	}

/**
* Ermittelt alle DATEN ALLER user und gibt sie als ARRAY zurueck. 
* Optional kann als zweiter Parameter "false" uebergeben werden, um auch die geloeschten sowie den DA_Service-Zugang zu ermitteln. 
*
* Beispiel: 
* <pre><code> 
* $aUser = $oUser->getAllUserData(); // params: [$bValidOnly=true][,$start=0][,$limit=0][,$sOverwriteConstraint='']
* </code></pre>
*
* @access   public
* @param	boolean	$bValidOnly	(false, wenn auch die geloeschten sowie der DA_Service-Zugang ermittelt werden soll - default: true)
* @param	int		$start		(optional, default: 0)
* @param	int		$limit		(optional, default: 0)
* @param	string	$sOverwriteConstraint	(optional, default: '')
* @return   array	Daten der User als Array (key=ID, val=Name)
*/
	function getAllUserData($bValidOnly=true, $start=0, $limit=0, $sOverwriteConstraint='') {
		// vars
		$buffer = array();
		// SQL
		$sConstraint	= ($bValidOnly == true) ? "WHERE `flag_da_service` = '0' AND `flag_deleted` = '0'" : '';
		if ($sOverwriteConstraint != '') $sConstraint = $sOverwriteConstraint;
		$sOrderBy		= (strpos($this->aConfig['nameformat'], 'surname') === false) 
			? 'ORDER BY `firstname` ASC' 
			: 'ORDER BY `surname` ASC, `firstname` ASC';
		$sql = "SELECT * 
				FROM `".$this->aENV['table']['sys_user']."` 
				".$sConstraint." ".$sOrderBy;
		$this->oDb->query($sql);
		$this->entries = floor($this->oDb->num_rows());	// noetig fuer DB-Navi und PRIO!
		if ($limit > 0 && $this->entries > $limit) {
			$this->oDb->limit($sql, $start, $limit);
    	}
		while ($tmp = $this->oDb->fetch_array()) {
			// gesamtes array kopieren
			$buffer[$tmp['id']] = $tmp;
			// virtuelles Feld "name" hinzufuegen
			$buffer[$tmp['id']]['name'] = str_replace(array('surname', 'firstname'), 
											array($tmp['surname'], $tmp['firstname']), 
											$this->aConfig['nameformat']);
		}
		// output
		return $buffer;
	}

/**
* Ermittelt die Namen ALLER user und gibt sie als ARRAY zurueck. 
* Optional kann als zweiter Parameter "false" uebergeben werden, um auch die geloeschten sowie den DA_Service-Zugang zu ermitteln. 
*
* Beispiel: 
* <pre><code> 
* $aUsername = $oUser->getAllUserNames(); // params: [$bValidOnly=true]
* </code></pre>
*
* @access   public
* @param	boolean	$bValidOnly	(false, wenn auch die geloeschten sowie der DA_Service-Zugang ermittelt werden soll - default: true)
* @return   array	Namen der User als Array (key=ID, val=Name)
*/
	function getAllUserNames($bValidOnly=true, $bWithJscript=true) {
		// vars
		$buffer = array();
		// SQL
		$sConstraint	= ($bValidOnly == true) ? "WHERE `flag_da_service` = '0' AND `flag_deleted` = '0'" : '';
		$sOrderBy		= (strpos($this->aConfig['nameformat'], 'surname') === false) 
			? 'ORDER BY `firstname` ASC' 
			: 'ORDER BY `surname` ASC, `firstname` ASC';
		$this->oDb->query("SELECT `id`, `surname`, `firstname` 
						FROM `".$this->aENV['table']['sys_user']."` 
						".$sConstraint." ".$sOrderBy);
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']] = str_replace(array('surname', 'firstname'), 
											array($tmp['surname'], $tmp['firstname']), 
											$this->aConfig['nameformat']);
			if($bWithJscript==true) {
				$buffer[$tmp['id']]	= '<a href="javascript:userinfoWin('.$tmp['id'].',\'sysuser\')">'.$buffer[$tmp['id']].'</a>';
			}								
		}
		// output
		return $buffer;
	}

/**
* Ermittelt die Namen des/der uebergebenen userid(s) und gibt sie als ARRAY zurueck. 
* Optional kann als zweiter Parameter "false" uebergeben werden, um auch die geloeschten sowie den DA_Service-Zugang zu ermitteln. 
*
* Beispiel: 
* <pre><code> 
* $aUsername = $oUser->getUserNames($aData['userid']); // params: $userid[,$bValidOnly=true]
* </code></pre>
*
* @access   public
* @param	mixed	$userid		ID(s) des/der User als String oder Array
* @param	boolean	$bValidOnly	(false, wenn auch die geloeschten sowie der DA_Service-Zugang ermittelt werden soll - default: true)
* @return   array	Name(n) des/der User als Array (key=ID, val=Name)
*/
	function getUserNames($userid, $bValidOnly=true, $bWithJscript=true) {
		// check vars
		$buffer = array();
		if (is_array($userid)) { $userid = implode(',', $userid); }
		if (empty($userid)) return $buffer; // fallback
		// SQL
		$sConstraint = ($bValidOnly == true) ? "AND `flag_da_service` = '0' AND `flag_deleted` = '0'" : '';
		$this->oDb->query("SELECT `id`, `surname`, `firstname` 
						FROM `".$this->aENV['table']['sys_user']."` 
						WHERE `id` IN (".$userid.") 
						".$sConstraint." 
						ORDER BY `firstname` ASC, `surname` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']] = str_replace(array('surname', 'firstname'), 
											array($tmp['surname'], $tmp['firstname']), 
											$this->aConfig['nameformat']);
								
			if($bWithJscript==true) {
				$buffer[$tmp['id']]	= '<a href="javascript:userinfoWin('.$tmp['id'].',\'sysuser\')">'.$buffer[$tmp['id']].'</a>';
			}								
		}
		// output
		return $buffer;
	}

/**
* Ermittelt den Namen EINER uebergebenen userid und gibt sie als STRING zurueck (Wrapper fuer "getUserNames()"). 
*
* Beispiel: 
* <pre><code> 
* echo $oUser->getUserName($aData['userid']); // params: $userid
* </code></pre>
*
* @access   public
* @param	string	$userid		ID) des User als String
* @return   string	Name des User als String
*/
	function getUserName($userid, $bValidOnly=false, $bWithJscript=false) {
		// check vars
		if (empty($userid)) return;
		// SQL
		$buffer = $this->getUserNames($userid, $bValidOnly, $bWithJscript);
		// output
		return $buffer[$userid]; //'<a href="javascript:userinfoWin('.$userid.',\'sysuser\')">'.$buffer[$userid].'</a>';
	}

/**
* Alle Daten EINES(!) Users (ohne Permissions/Usergroups!) aufgrund seiner Id ermitteln
*
* Beispiel: 
* <pre><code> 
* $aUserdata = $oUser->getUserdata($aData['id']); // params: $userid 
* </code></pre>
*
* @access   public
* @param	string	$userid		ID des User (als String)
* @return   array	Userdata
*/
	function getUserdata($userid) {
		// check vars
		if (empty($userid)) return array();
		// output
		$buffer = $this->getUserdatas($userid);
		return $buffer[$userid]; // flatten array
	}

/**
* Alle Daten MEHRERER User (ohne Permissions/Usergroups!) aufgrund ihrer Ids ermitteln
*
* Beispiel: 
* <pre><code> 
* $aUserdata = $oUser->getUserdatas($userid); // params: $userid 
* </code></pre>
*
* @access   public
* @param	string	$userid		ID des/der User (als String/Array)
* @return   array	Userdata
*/
	function getUserdatas($userid) {
		// check vars
		$buffer = array();
		if (!is_null($userid) && is_array($userid)) { $userid = implode(',', $userid); }
		if (empty($userid)) return $buffer; // fallback
		// SQL
		$this->oDb->query("SELECT * 
							FROM `".$this->aENV['table']['sys_user']."` 
							WHERE `id` IN (".$userid.") 
							ORDER BY `firstname` ASC, `surname` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			// zusaetzliches feld 'name' (beinhaltet den formatierten Namen - wie in der setup.php konfiguriert)
			$tmp['name'] = str_replace(array('surname', 'firstname'), 
										array($tmp['surname'], $tmp['firstname']), 
										$this->aConfig['nameformat']);
			// gesamtes array kopieren
			$buffer[$tmp['id']] = $tmp;
		}
		// output
		return $buffer;
	}

#----------------------------------------------------------------------------- OFFICE 

/**
* EIN bestimmtes Office ermitteln und als STRING zurueckgeben. (Wrapper fuer "getOfficeNames()"). 
*
* Beispiel: 
* <pre><code> 
* echo $oUser->getOfficeName($aData['office_id']); // params: [$officeid] 
* </code></pre>
*
* @access   public
* @param	string	$officeid		ID des/der Offices (als String/Array)
* @return   string	Name des Office als String
*/
	function getOfficeName($officeid) {
		// check vars
		if (empty($officeid)) return ''; // fallback
		// output
		return implode('', $this->getOfficeNames($officeid));
	}

/**
* Ein oder mehrere bestimmte ($officeid als kommaseparierter string oder als array) Offices ermitteln und als ARRAY zurueckgeben.
*
* Beispiel: 
* <pre><code> 
* $aOffice = $oUser->getOfficeNames($aData['office_id']); // params: [$officeid] 
* </code></pre>
*
* @access   public
* @param	mixed	$officeid		ID des/der Offices (als String/Array)
* @return   array	Name(n) des/der Offices als Array (key=ID, val=Name)
*/
	function getOfficeNames($officeid) {
		// check vars
		$buffer = array();
		if (!is_null($officeid) && is_array($officeid)) { $officeid = implode(',', $officeid); }
		if (empty($officeid)) return $buffer; // fallback
		// SQL
		$this->oDb->query("SELECT `id`, `name` 
							FROM `".$this->aENV['table']['sys_office']."` 
							WHERE `id` IN (".$officeid.") 
							ORDER BY `name` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['name'];
		}
		// output
		return $buffer;
	}

/**
* Alle Office-Namen (mit "true" nur die mit Usern) ermitteln.
*
* Beispiel: 
* <pre><code> 
* $aOffice = $oUser->getAllOfficeNames(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(true, wenn nur die mit Usern ermittelt werden sollen - default: false)
* @return   array	Name(n) des/der Offices als Array (key=ID, val=Name)
*/
	function getAllOfficeNames($bWithUsers=false) {
		// vars
		$buffer = array();
		// SQL
		$sQuery = ($bWithUsers == true) 
			? "SELECT `o`.`id`, `o`.`name` 
				FROM `".$this->aENV['table']['sys_office']."` `o`, `".$this->aENV['table']['sys_user']."` `u` 
				WHERE `o`.`id` = `u`.`office_id`" 
			: "SELECT `id`, `name` 
				FROM `".$this->aENV['table']['sys_office']."`";
		$this->oDb->query($sQuery." ORDER BY `name` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['name'];
		}
		// output
		return $buffer;
	}

/**
* Alle Daten aller Offices (mit "true" nur die mit Usern) ermitteln.
*
* Beispiel: 
* <pre><code> 
* $aOffice = $oUser->getAllOffices(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(true, wenn nur die mit Usern ermittelt werden sollen - default: false)
* @return   array	Name(n) des/der Offices als Array (key=ID, val=Name)
*/
	function getAllOffices($bWithUsers=false) {
		// vars
		$buffer = array();
		// SQL
		$sQuery = ($bWithUsers == true) 
			? "SELECT `o`.* 
				FROM `".$this->aENV['table']['sys_office']."` `o`, `".$this->aENV['table']['sys_user']."` `u` 
				WHERE `o`.`id` = `u`.`office_id`" 
			: "SELECT * FROM `".$this->aENV['table']['sys_office']."`";
		$this->oDb->query($sQuery." ORDER BY `name` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			// gesamtes array kopieren
			$buffer[$tmp['id']] = $tmp;
		}
		// output
		return $buffer;
	}

/**
* Ermittle die Anzahl aller Offices (mit "true" nur die mit Usern).
*
* Beispiel: 
* <pre><code> 
* echo $oUser->countAllOffices(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(true, wenn nur die mit Usern ermittelt werden sollen - default: false)
* @return   int		Anzahl aller Offices
*/
	function countAllOffices($bWithUsers=false) {
		// SQL
		$sQuery = ($bWithUsers == true) 
			? "SELECT `o`.`id` 
				FROM `".$this->aENV['table']['sys_office']."` `o`, `".$this->aENV['table']['sys_user']."` `u` 
				WHERE `o`.`id` = `u`.`office_id`" 
			: "SELECT `id` FROM `".$this->aENV['table']['sys_office']."`";
		$this->oDb->query($sQuery);
		// output
		return $this->oDb->num_rows();
	}

/**
* Gibt die User-IDs der Offices als Array zurueck, deren IDs (kommasepariert oder als flaches Array) uebergeben werden.
*
* Beispiel: 
* <pre><code> 
* $aOfficeUserId = $oUser->getOfficeUserIds(); // params: $officeid
* </code></pre>
*
* @access   public
* @param	mixed	$officeid	ID des Offices / IDs der Offices - kommasepariert oder als array
* @return	array	User-ID(s) des/der Offices
*/
	function getOfficeUserIds($officeid) {
		// init vars
		$buffer = array();
		// check vars
		if (is_array($officeid)) { implode($officeid, ','); }
		if (empty($officeid)) return;
		// SQL: ermittle alle User-IDs
		$sql = "SELECT `id` 
				FROM `".$this->aENV['table']['sys_user']."` 
				WHERE `office_id` IN (".$officeid.")";
		$this->oDb->query($sql);
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[] = $tmp['id'];
		}
		// output
		return $buffer;
	}


#----------------------------------------------------------------------------- USERGROUPS 

/**
* EINE bestimmte Usergroup ermitteln und als STRING zurueckgeben. (Wrapper fuer "getUsergroupNames()"). 
*
* Beispiel: 
* <pre><code> 
* echo $oUser->getUsergroupName($aData['ug_id']); // params: [$ugid] 
* </code></pre>
*
* @access   public
* @param	string	$ugid		ID des/der Offices (als String/Array)
* @return   string	Name des Office als String
*/
	function getUsergroupName($ugid) {
		// check vars
		if (empty($ugid)) return ''; // fallback
		// output
		return implode('', $this->getUsergroupNames($ugid));
	}

/**
* Ein oder mehrere bestimmte ($officeid als kommaseparierter string oder als array) Offices ermitteln und als ARRAY zurueckgeben.
*
* Beispiel: 
* <pre><code> 
* $aUsergroup = $oUser->getUsergroupNames($aData['ug_id']); // params: [$ugid] 
* </code></pre>
*
* @access   public
* @param	mixed	$ugid		ID des/der Usergroups (als String/Array)
* @return   array	Name(n) des/der Offices als Array (key=ID, val=Name)
*/
	function getUsergroupNames($ugid) {
		// check vars
		$buffer = array();
		if (!is_null($ugid) && is_array($ugid)) { $ugid = implode(',', $ugid); }
		if (empty($ugid)) return $buffer; // fallback
		// SQL
		$this->oDb->query("SELECT `id`, `title` 
							FROM `".$this->aENV['table']['sys_usergroup']."` 
							WHERE `id` IN (".$ugid.") 
							ORDER BY `title` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['title'];
		}
		// output
		return $buffer;
	}

/**
* Alle Usergroup-Namen (mit "true" nur die mit Usern) ermitteln.
*
* Beispiel: 
* <pre><code> 
* $aUsergroup = $oUser->getAllUsergroupNames(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(true, wenn nur die mit Usern ermittelt werden sollen - default: false)
* @return   array	Name(n) des/der Usergroups als Array (key=ID, val=Name)
*/
	function getAllUsergroupNames($bWithUsers=false) {
		// vars
		$buffer = array();
		// SQL
		if ($bWithUsers == true) {
			$this->oDb->query("SELECT DISTINCT `usergroup_id` 
								FROM `".$this->aENV['table']['sys_user_usergroup']."`");
			$ugid = array();
			while ($tmp = $this->oDb->fetch_array()) {
				$ugid[] = $tmp['usergroup_id'];
			}
			$sQuery = "SELECT `id`, `title` 
						FROM `".$this->aENV['table']['sys_usergroup']."` 
						WHERE `id` IN(".implode(',', $ugid).")";
		} else {
			$sQuery = "SELECT `id`, `title` 
						FROM `".$this->aENV['table']['sys_usergroup']."`";
		}
		$this->oDb->query($sQuery." ORDER BY `title` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['title'];
		}
		// output
		return $buffer;
	}

/**
* Alle Daten aller Usergroups (mit "true" nur die mit Usern) ermitteln.
*
* Beispiel: 
* <pre><code> 
* $aUsergroup = $oUser->getAllUsergroups(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(true, wenn nur die mit Usern ermittelt werden sollen - default: false)
* @return   array	Name(n) des/der Usergroups als Array (key=ID, val=Name)
*/
	function getAllUsergroups($bWithUsers=false) {
		// vars
		$buffer = array();
		// SQL
		if ($bWithUsers == true) {
			$this->oDb->query("SELECT DISTINCT `usergroup_id` 
								FROM `".$this->aENV['table']['sys_user_usergroup']."`");
			$ugid = array();
			while ($tmp = $this->oDb->fetch_array()) {
				$ugid[] = $tmp['usergroup_id'];
			}
			$sQuery = "SELECT * 
						FROM `".$this->aENV['table']['sys_usergroup']."` 
						WHERE `id` IN(".implode(',', $ugid).")";
		} else {
			$sQuery = "SELECT * FROM `".$this->aENV['table']['sys_usergroup']."`";
		}
		$this->oDb->query($sQuery." ORDER BY `title` ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			// gesamtes array kopieren
			$buffer[$tmp['id']] = $tmp;
		}
		// output
		return $buffer;
	}

/**
* Ermittle die Anzahl aller Usergroups (mit "true" nur die mit Usern).
*
* Beispiel: 
* <pre><code> 
* echo $oUser->countAllUsergroups(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(true, wenn nur die mit Usern ermittelt werden sollen - default: false)
* @return   int		Anzahl aller Usergroups
*/
	function countAllUsergroups($bWithUsers=false) {
		// SQL
		if ($bWithUsers == true) {
			$this->oDb->query("SELECT DISTINCT `usergroup_id` 
								FROM `".$this->aENV['table']['sys_user_usergroup']."`");
			$ugid = array();
			while ($tmp = $this->oDb->fetch_array()) {
				$ugid[] = $tmp['usergroup_id'];
			}
			$sQuery = "SELECT `id` 
						FROM `".$this->aENV['table']['sys_usergroup']."` 
						WHERE `id` IN(".implode(',', $ugid).")";
		} else {
			$sQuery = "SELECT `id` FROM `".$this->aENV['table']['sys_usergroup']."`";
		}
		$this->oDb->query($sQuery);
		// output
		return $this->oDb->num_rows();
	}

/**
* Alle IDs der User ermitteln, die einer oder mehreren Usergroups angehoeren.
*
* Beispiel: 
* <pre><code> 
* $aUID = $oUser->getAllUserIdsByUsergroup($usergroup_id); // params: $ugid 
* </code></pre>
*
* @access   public
* @param	mixed	$ugid		ID des/der Usergroups (als String/Array)
* @return   array	IDs des/der Usergroups als Array (val=ID)
*/
	function getAllUserIdsByUsergroup($ugid) {
		// check vars
		$buffer = array();
		if (!is_null($ugid) && is_array($ugid)) { $ugid = implode(',', $ugid); }
		if (empty($ugid)) return $buffer; // fallback
		// SQL
		$this->oDb->query("SELECT `user_id` 
							FROM `".$this->aENV['table']['sys_user_usergroup']."` 
							WHERE `usergroup_id` IN (".$ugid.")");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[] = $tmp['user_id'];
		}
		// output
		return $buffer;
	}
	
/**
* Alle Group IDs des Users ermitteln.
*
* Beispiel: 
* <pre><code> 
* $aGID = $oUser->getUsersGroups($uid); // params: $uid 
* </code></pre>
*
* @access   public
* @param	int	$uid		ID des/der Usergroups (als int)
* @return   array	GIDs des Users als inkrementelles Array 
*/
	function getUsersGroups($uid) {
	
			$groups = $this->getAllUsergroups();
   				
   			foreach($groups as $key => $val) {
   			
   				$user[$key] = $this->getAllUserIdsByUsergroup($key);
   			}
 			
   			foreach($user as $key => $val) {
   			
   				for($i=0;!empty($val[$i]);$i++) {
   				
   					if($val[$i] == $uid) {
   					
   						$usersgroup[] = $key;
   					
   					}
   				}
   				
   			}
   			return $usersgroup;
		
	}

#-----------------------------------------------------------------------------
} // END of class

?>