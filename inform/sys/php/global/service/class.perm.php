<?php
/**
 * Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung die Rechte des aktuellen Nutzers abzufragen. 
 * ("Rechte" sind "permissions", bestehend aus einzelnen "privileges") 
 * Diese Klasse wird gleich in der inc.login.php initialisiert!
 *
 * Example: 
 * <pre><code> 
 * $oPerm =& new perm();
 * // das Privileg 'edit' im aktuellen Modul abfragen
 * $b1 = $oPerm->hasPriv('edit'); // param: $sPrivilege[,$sModule=''] 
 * // das Privileg 'edit' in einem anderen Modul abfragen
 * $b2 = $oPerm->hasPriv('edit', 'pms'); // param: $sPrivilege[,$sModule=''] 
 * </code></pre>
 *
 * @access   public
 * @package  service
 * @author	Andy Fehn <af@design-aspekt.com>
 * @version	1.1 / 2006-03-06 [NEU: "hasCmsPreviewRightOnly()"]
 */
class perm { // Class perm

	/*	----------------------------------------------------------------------------
		Funktionen der Klasse perm:
		----------------------------------------------------------------------------
		konstruktor perm()
		function authModule()
		function hasPriv($sPrivilege, $sModule='')
		function hasCmsPreviewRightOnly()
		function addPriv($sPrivilege, $sModule='')
		function remPriv($sPrivilege, $sModule='')
		function getModule()
		function isDaService()
		----------------------------------------------------------------------------
		HISTORY:
		1.1 / 2006-03-06 [NEU: "hasCmsPreviewRightOnly()"]
		1.01 / 2006-01-30 [BUGFIX: in "authModule()"]
		1.0 / 2005-01-13
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	array	importiertes globales Setup-Array
*/
	var $aENV = '';
/**
* @access   private
* @var	 	array	importiertes globales Userdata-Session-Array
*/
	var $Userdata = '';
/**
* @access   private
* @var	 	string	aktuelles Modul [cms|pms|...]
*/
	var $sCurrentModule = '';

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das perm-Objekt und bestimmt das aktuelle Modul. 
*
* Beispiel: 
* <pre><code> 
* $oPerm =& new perm();
* </code></pre>
*
* @access   public
* @return   void
*/
	function perm() {
		// import global vars
		global $aENV, $Userdata;
		$this->aENV		= $aENV;
		$this->Userdata = $Userdata;
		// check
		if (!is_array($this->aENV)) die("class.perm::ERROR: NO aENV!");
		if (!is_array($this->Userdata)) die("class.perm::ERROR: NO Userdata!");
		
		// Aktuelles Modul bestimmen
		$this->sCurrentModule = 'sys'; // set default
		foreach ($aENV['module'] as $dir => $title) {
			if (strstr($_SERVER['PHP_SELF'], $dir.'/')) {
				$this->sCurrentModule = $dir; break;
			}
		}
	}

#-----------------------------------------------------------------------------

/**
* Pruefe, ob der aktuelle Benutzer das aktuelle Modul ueberhaupt sehen darf (mittels Privileg "view"). 
* Wenn nicht, wird er zur disallow-page weitergeleitet! Diese Methode wird gleich in der inc.login.php ausgefuehrt!
* NOTE: Das Modul "sys" wird nicht ueberprueft, da jeder Benutzer auf das Portal darf!
* Weiterfuehrende Beschraenkungen muessen dann in den jeweiligen sys-pages gemacht werden!
* NOTE: Die Seite "cms_preview*.php" wird nicht ueberprueft, da es Benutzergibt, die NUR das preview-Privileg
* fuer das CMS haben und trotzdem auf die Preview-Seiten kommen koennen muessen!
*
* Beispiel: 
* <pre><code> 
* $oPerm->authModule(); // params: - 
* </code></pre>
*
* @access   public
* @return   string	[array-key des aktuellen modules]
*/
	function authModule() {
		if (empty($this->sCurrentModule)) return false;
		// keine ueberpruefung bei SYS
		if ($this->sCurrentModule == "sys") return true;
		// keine ueberpruefung bei cms_preview*
		if (strpos($_SERVER['PHP_SELF'], 'cms_preview')) return true;
		
		// aktuelles modul authentifizieren
		if (!$this->hasPriv('view')) {
			// ansonsten braucht man immer view-rechte!
			header("Location: ".$this->aENV['page']['disallow']."?msg=userrights"); exit;
		}
	}

#----------------------------------------------------------------------------- Methoden fuer pages

/**
* Funktion, die nachsieht, ob der aktuelle Nutzer in aktuellen Modul ein bestimmtes Recht hat. 
* Wenn das Privileg in einem anderen als dem aktuellen Modul angefragt wird, muss dieses als zweiter Parameter uebergeben werden.
* NOTE: Wenn der aktuelle Nutzer den da-service-login benutzt, liefert die Methode IMMER TRUE zurueck!
*
* Beispiel: 
* <pre><code> 
* if ($oPerm->hasPriv('edit')) { doSomething(); } // params: $sPrivilege[,$sModule=''] 
* if ($oPerm->hasPriv('edit', 'pms')) { doSomethingElse(); } // params: $sPrivilege[,$sModule=''] 
* </code></pre>
*
* @access   public
* @param 	string	$sPrivilege		Angefragtes Privileg
* @param 	string	$sModule		(optional: Modul (Array-Key) wenn nicht das aktuelle)
* @return   boolean	[true, wenn der user das angefragte privileg besitzt, sonst false]
*/
	function hasPriv($sPrivilege, $sModule='') {
		if (empty($sPrivilege)) return false;
		// aktuelles Modul oder anderes?
		$currentModule = ($sModule == '') ? $this->sCurrentModule : $sModule;
		// nachsehen ob das angefragte privileg in der Userdata-Session existiert
		return (isset($this->Userdata['permission'][$currentModule][$sPrivilege]) || $this->isDaService()) ? true : false;
	}

/**
* ueberpruefe, ob der aktuelle Nutzer NUR das cms::preview-privileg hat. 
*
* Beispiel: 
* <pre><code> 
* if ($oPerm->hasCmsPreviewRightOnly()) { doSomething(); } // params: - 
* </code></pre>
*
* @access   public
* @return   boolean	[true, wenn der user das cms::preview-privileg besitzt, sonst false]
*/
	function hasCmsPreviewRightOnly() {
		$hasRight = false;
		if (count($this->Userdata['permission']) == 1 && isset($this->Userdata['permission']['cms'])) {
			if (count($this->Userdata['permission']['cms']) == 1 && isset($this->Userdata['permission']['cms']['preview'])) {
				$hasRight = true;
			}
		}
		return $hasRight; 
	}

/**
* Funktion, die voruebergehend (d.h. nur auf der aktuellen Seite und im Folgenden) ein Recht des aktuellen Nutzers 
* im aktuellen (oder im optionalen zweiten Parameter uebergebenen) Modul hinzufuegt. 
* NOTE: Wenn der aktuelle Nutzer den da-service-login benutzt, ist diese Methode WIRKUNGSLOS!
*
* Beispiel: 
* <pre><code> 
* $oPerm->addPriv('delete'); // params: $sPrivilege[,$sModule=''] 
* $oPerm->addPriv('delete', 'pms'); // params: $sPrivilege[,$sModule=''] 
* </code></pre>
*
* @access   public
* @param 	string	$sPrivilege		Hinzuzufuegendes Privileg
* @param 	string	$sModule		(optional: Modul (Array-Key) wenn nicht das aktuelle)
* @return   void
*/
	function addPriv($sPrivilege, $sModule='') {
		if (empty($sPrivilege)) return;
		if ($this->isDaService()) return;
		// aktuelles Modul oder anderes?
		$currentModule = ($sModule == '') ? $this->sCurrentModule : $sModule;
		// neues privileg in der Userdata-Session hizufuegen
		$this->Userdata['permission'][$currentModule][$sPrivilege] = 1;
	}

/**
* Funktion, die voruebergehend (d.h. nur auf der aktuellen Seite und im Folgenden) ein Recht des aktuellen Nutzers 
* im aktuellen (oder im optionalen zweiten Parameter uebergebenen) Modul entfernt. 
* NOTE: Wenn der aktuelle Nutzer den da-service-login benutzt, ist diese Methode WIRKUNGSLOS!
*
* Beispiel: 
* <pre><code> 
* $oPerm->removePriv('delete'); // params: $sPrivilege[,$sModule=''] 
* $oPerm->removePriv('delete', 'pms'); // params: $sPrivilege[,$sModule=''] 
* </code></pre>
*
* @access   public
* @param 	string	$sPrivilege		Hinzuzufuegendes Privileg
* @param 	string	$sModule		(optional: Modul (Array-Key) wenn nicht das aktuelle)
* @return   void
*/
	function removePriv($sPrivilege, $sModule='') {
		if (empty($sPrivilege)) return;
		if ($this->isDaService()) return;
		// aktuelles Modul oder anderes?
		$currentModule = ($sModule == '') ? $this->sCurrentModule : $sModule;
		// neues privileg in der Userdata-Session entfernen
		if (isset($this->Userdata['permission'][$currentModule][$sPrivilege])) {
			unset($this->Userdata['permission'][$currentModule][$sPrivilege]);
		}
	}

/**
* Gebe aktuelles Modul als array-key (string) zurueck. 
* Wenn dieses nicht ermittelt werden konnte, wird FALSE zurueckgegeben!
*
* Beispiel: 
* <pre><code> 
* echo $oPerm->getModule(); // params: - 
* </code></pre>
*
* @access   public
* @return   string	[array-key des aktuellen mudules]
*/
	function getModule() {
		if (empty($this->sCurrentModule)) return false;
		// aktuelles Modul zurueckgeben
		return $this->sCurrentModule;
	}

/**
* Gebe TRUE zurueck, wenn der aktuelle Benutzer den design aspekt-service-zugang nutzt. 
*
* Beispiel: 
* <pre><code> 
* echo $oPerm->isDaService(); // params: - 
* </code></pre>
*
* @access   public
* @return   boolean	[true, wenn aspekt-service-zugang, sonst false]
*/
	function isDaService() {
		return (isset($this->Userdata['flag_da_service']) && $this->Userdata['flag_da_service'] == '1') ? true : false;
	}

} // END of class
?>