<?php
/*
 * Created on 25.10.2005
 *
 */
/**
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * 
 * Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung in einer Struktur die Auf- und Zuklapp-Pfeile einzubinden! 
 *
 * @access   public
 * @package  Content
 * @version	1.0 / 2005-10-25
 *
 * Example: 
 * <pre><code> 
 * $oToggle =& new NaviToggle(); // params: -
 * $oToggle->initialize($aENV,$aMSG,$syslang,$EA,$iTid); // params: $aENV, $aMSG
 * // show/hide subnavi
 * echo $oToggle->getToggleIcon();
 * </code></pre>
 *
 */
 
class NaviToggle {
	
	// Class Vars
	var $aENV			= array();		// System-Variable
	var $aMSG			= array();		// System-Variable
	var $syslang		= 'de';			// System-Variable
	var $aPrioIcon		= array();		// Icons fuer die Prioritaet
	var $aHtml			= array();		// HTML-Tags fuer Icons/Buttons/Links
	var $iToggleId		= '';			// private Variable (aktuelle id)
	var $EA				= null;			// initialisierter EasyAccess fuer die jeweilige Navigations Bean
	var $navi			= null;
	var $active			= null;
	var $subsnottodel	= null;

	var $bDebug = false;
	
	function setAENV($aENV) {
		
		$this->aENV	= $aENV;
		
	}

	function setAMSG($aMSG) {
		
		$this->aMSG	= $aMSG;
		
	}

	function setSyslang($syslang) {
	
		$this->syslang = $syslang;
		
	}
	
	function setEA($ea) {
	
		$this->EA = $ea;
		
	}
	
	function setIToggleid($itid) {
	
		$this->iToggleId = $itid;
		
	}
		
	function setBDebug($bDebug) {
	
		$this->bDebug = $bDebug;
		
	}
	
	function initialize($aENV,$aMSG,$syslang,$EA,$iTid) {
	
		$this->setAENV($aENV);
		$this->setAMSG($aMSG);
		$this->setSyslang($syslang);
		$this->setEA($EA);
		
		// Container fuer HTML-Tag Templates ({VAR_XY} muss erstzt werden)
		$this->aHtml = array();
		$this->aHtml['a_start']			= '<a href="{HREF}" title="{TITLE}"{EXTRA}>';
		$this->aHtml['a_end']			= '</a>';
		$this->aHtml['toggleIcon'] 		= '<img src="'.$this->aENV['path']['pix']['http'].'{IMG}" alt="{TITLE}" class="btnsmall">';
		$this->aHtml['prioIcon'] 		= '<img src="'.$this->aENV['path']['pix']['http'].'{IMG}" hspace="1" width="9" height="9" alt="{TITLE}" border="0">';
		$this->aHtml['button']			= '<img src="'.$this->aENV['path']['pix']['http'].'{IMG}" alt="{TITLE}" class="btn">';
		
	  
		$this->aPrioIcon['first']		= '<img src="'.$this->aENV['path']['pix']['http'].'pf_abstop.gif" hspace="1" width="9" height="9" alt="'.$this->aMSG['btn']['prio_top'][$this->syslang].'" border="0">';
		$this->aPrioIcon['last']		= '<img src="'.$this->aENV['path']['pix']['http'].'pf_absbottom.gif" hspace="1" width="9" height="9" alt="'.$this->aMSG['btn']['prio_btm'][$this->syslang].'" border="0">';
		$this->aPrioIcon['up']			= '<img src="'.$this->aENV['path']['pix']['http'].'pf_up.gif" hspace="1" width="9" height="9" alt="'.$this->aMSG['btn']['prio_up'][$this->syslang].'" border="0">';
		$this->aPrioIcon['down']		= '<img src="'.$this->aENV['path']['pix']['http'].'pf_down.gif" hspace="1" width="9" height="9" alt="'.$this->aMSG['btn']['prio_dwn'][$this->syslang].'" border="0">';
		$this->aPrioIcon['downdeak']	= '<img src="'.$this->aENV['path']['pix']['http'].'d_pf_down.gif" hspace="1" width="9" height="9" alt="" border="0">';
		$this->aPrioIcon['updeak']		= '<img src="'.$this->aENV['path']['pix']['http'].'d_pf_up.gif" hspace="1" width="9" height="9" alt="" border="0">';
		$this->aPrioIcon['firstdeak']	= '<img src="'.$this->aENV['path']['pix']['http'].'d_pf_abstop.gif" hspace="1" width="9" height="9" alt="" border="0">';
		$this->aPrioIcon['lastdeak']	= '<img src="'.$this->aENV['path']['pix']['http'].'d_pf_absbottom.gif" hspace="1" width="9" height="9" alt="" border="0">';
		
		$this->aPrioIcon['az']			= '<img src="'.$this->aENV['path']['pix']['http'].'btn_az.gif" alt="'.$this->aMSG['btn']['prio_top'][$this->syslang].'" class="btn">';
		
		$this->setIToggleid($iTid);
		$this->createNavi();
		if(!$this->bDebug) {
			$this->addActive($this->iToggleId);
			$this->sortNaviDisplay();
		}
	}
	/**
	 * 
	 * Methode zum aufbereiten der Informationen als Navigation
	 * @version 1.0
	 * @access private
	 * 
	 */
	function createNavi() {
	
		$bean = $this->EA->getBean();
	
		$id			= $bean->getId();
		$parentid	= $bean->getParentid();
		$prio		= $bean->getPrio();
		$title		= $bean->getTitle();
		$flagnavi	= $bean->getFlagnavi();
	
		$navi = array();
		
		for($i=0;!empty($id[$i]);$i++) {
			$navi[$id[$i]]['id'] = $id[$i];
			$navi[$id[$i]]['parentid'] = $parentid[$i];
			$navi[$id[$i]]['prio'] = $prio[$i];
			$navi[$id[$i]]['title'] = $title[$i];
		}
				
		$anewnavi = array();
		if(!$this->bDebug) {
			$anewnavi = $this->readRecursiv($navi,0);
			$this->navi = $anewnavi;
		}
		else {
			$this->navi = $navi;
		}
	}
	
	/**
	 * 
	 * Auslesen der Ordnerstruktur. Recursion ist angesagt...
	 * @param array $navi
	 * @param int $parentid
	 * @param int $ebene
	 * @param boolean $bSubread
	 * @param array $anewnavi
	 * 
	 */
	
	function readRecursiv($navi,$parentid,$ebene=0,$bSubread=false,$anewnavi=null) {
		
		if (!is_array($navi)) $navi = array();
		
		reset($navi);
		$originalnavi = $navi;
		
		if(is_null($anewnavi)) {
		
			$anewnavi = array();
			
		}
		
		foreach($navi as $id => $val) {
			
			if($val['parentid']==$parentid && !$bSubread) {
				
				$anewnavi[$id] = $val;
				$anewnavi[$id]['ebene'] = $ebene;
				
				$anewnavi[$id]['sub'] = $this->readRecursiv($originalnavi,$id,($ebene+1),true);
				if(count($anewnavi[$id]['sub']) > 0) {
					
					for($i=0;!empty($anewnavi[$id]['sub'][$i]);$i++) {
						if(empty($anewnavi[$anewnavi[$id]['sub'][$i]])) {
							$anewnavi = $this->readRecursiv($originalnavi,$id,($ebene+1),false,$anewnavi);
						}
					}	
				}		
			}
			else {
				
				if($val['parentid'] == $parentid && $bSubread) {
					$anewnavi[] = $id;
				}
			}
	
		}
		/*
		for($i=0;!empty($id[$i]);$i++) {
			
			if($parentid[$i] == 0) {
			
				$ebenenav = 0;
				
			}
			else {
				
				if(is_array($navi[$parentid[$i]])) {
					$anewnavi[$parentid[$i]]['sub'][] = $id[$i];
				}
				$ebenenav = $anewnavi[$parentid[$i]]['ebene']+1;
					
			}
			
			$anewnavi[$id[$i]]['id'] = $id[$i];
			$anewnavi[$id[$i]]['parentid'] = $parentid[$i];
			$anewnavi[$id[$i]]['prio'] = $prio[$i];
			$anewnavi[$id[$i]]['title'] = $title[$i];
			$anewnavi[$id[$i]]['ebene'] = $ebenenav;
		
		}*/	
		return $anewnavi;	
	}
	/**
	 * 
	 * Methode zum Zuordnen der Subnavigation zu einer Subnavigation
	 * @version 1.0
	 * @access private
	 * @param int $subid
	 * @param String[] $anavi
	 * @return String[] $anavi
	 */
	function sortSubsToSubs($subid,$anavi) {
		
		$parent = $anavi[$subid];
		
		for($i=0;!empty($parent['sub'][$i]);$i++) {
			
			unset($anavi[$parent['sub'][$i]]);
			
			$anavi[$parent['sub'][$i]] = $this->navi[$parent['sub'][$i]];
				
			if(array_key_exists($parent['sub'][$i],$this->active['togs'])) {
			
				$anavi = $this->sortSubsToSubs($parent['sub'][$i],$anavi);
				
				$this->subsnottodel[] = $parent['sub'][$i];	
			
			}
			$substodel[] = $parent['sub'][$i];
			
		}

		return $anavi;

	}
	
	/**
	 * 
	 * Aufbereiten der Navigation zur Ausgabe
	 * @version 1.0
	 * @access private
	 * 
	 */
	function sortNaviDisplay() {
	
		$this->subnottodel = array();
		$anavi = array();
		
		if($this->iToggleId != 0) {

			foreach($this->navi as $key => $val) {
	
				$activetogs = $this->active['togs'];
				asort($activetogs,SORT_NUMERIC);
				$this->active['togs'] = $activetogs;
					
				if(in_array($this->navi[$key]['ebene'],$this->active['ebenen'])) {
					
					$anavi[$key] = $this->navi[$key];
					
					foreach($this->active['togs'] as $ackey => $acval) {
						
						if($ackey == $key && $ackey != 'ebenen') {
					
							for($i=0;!empty($this->navi[$key]['sub'][$i]);$i++) {
						
								$subid = $this->navi[$key]['sub'][$i];
						
								if($ackey == $this->navi[$subid]['parentid']) {

									$subkey = $this->navi[$key]['sub'][$i];

									if(empty($anavi[$subkey])) {
										
										$anavi[$subkey] = $this->navi[$subkey];

										if(array_key_exists($subid,$this->active['togs'])) {
									
											$anavi = $this->sortSubsToSubs($subid,$anavi);
									
										}

									}
								}

								$this->subsnottodel[] = $subid;
							}
						}
					}
				}
			}
			
			foreach($this->navi as $key => $val) {
				
				if(is_array($this->subsnottodel)) {
					
					if(!in_array($key,$this->subsnottodel)) {
						if(($val['ebene'] >= $anavi[$this->subsnottodel[0]]['ebene']) || !in_array($val['parentid'],$this->subsnottodel) && $val['ebene'] != 0) {
							unset($anavi[$key]);
						}
					
					}
				}
			}
			
		}
		else {
		
			foreach($this->navi as $key => $val) {
			
				if($val['ebene'] == 0) {
					$anavi[$key] = $this->navi[$key];
				}			
			}
			
		}
		$this->navi = $anavi;
	}
	
	/**
	 * 
	 * Methode die ueberprueft ob eine Navigation eine Unternavigation besitzt
	 * @version 1.0
	 * @access private
	 * @param int $naviid
	 * @return boolean 
	 */
	function hasSubMenue($naviid) {
	
		if(!empty($this->navi[$naviid]['sub'][0])) {
		
			return true;
			
		}
		else {
		
			return false;
			
		}
		
	}

	/**
	 * 
	 * Diese Methode sorgt dafuer, dass die aktiven Toggles offen gelassen werden.
	 * @version 1.0
	 * @param int $togId
	 * @access private
	 * 
	 */
	function addActive($togId) {
	
		if($togId == 0) {
	
			unset($this->active);
			
		}
		else {
		 
			$this->active = array();

		}
		
		if(is_array($this->active)) {
		
			if($this->navi[$togId]['parentid'] != 0 && is_array($this->active['togs'])) {
		
				foreach($this->active['togs'] as $key => $val) {
					
					if($val == $this->navi[$togId]['parentid']) {
					
						unset($this->active['togs'][$key]);
					
					}
				
				}
				if(!isset($this->active['togs'][$togId])) {
			
					$this->active['togs'][$togId] = $this->navi[$togId]['parentid'];
					
				}
			}
			else {

				unset($this->active);
				$this->active['togs'][$togId] = $this->navi[$togId]['parentid'];

			}
			$ebenen = array();

			foreach($this->navi as $key => $val) {
			
				if(!in_array($this->navi[$key]['ebene'],$ebenen)) {
				
					if($this->navi[$this->navi[$this->iToggleId]['sub'][0]]['ebene'] >= $this->navi[$key]['ebene']) {
				
						$ebenen[] = $this->navi[$key]['ebene'];
					
					}
				}
				
			}

			$this->active['ebenen'] = $ebenen;

		}
		else {
		
			$this->active = array('ebenen' => array('0' => '0'));

		}
		
		if(is_array($this->active['togs'])) {
			
			$eid = each($this->active['togs']);
			
			$togparent = $eid['value'];
			
			for($i=0;$togparent != 0;$i++) {
				
				$togp = $this->navi[$togparent]['parentid'];
			
				if(empty($togp)) {
				
					$togp = 0;
					
				}
				$this->active['togs'][$togparent] = $togp;
				$togparent = $togp;			
			}
			
		}
	}

	/**
	 * 
	 * Diese Methode dient dazu in der Ausgabe einen speziellen Wert aus der Navigation zu bekommen
	 * @version 1.0
	 * @access public
	 * @param String $name
	 * @return String
	 * 
	 */
	function getValueByName($name) {
		
		$current = current($this->navi);
		
		return $current[$name];
		
	}
	
	/**
	 * 
	 * Methode die den internen ArrayZeiger vorrueckt. Diese Methode wird in der Ausgabe benoetigt um die Daten abzufragen (while...)
	 * @version 1.0
	 * @access public
	 * @param int Zaehler
	 * @return boolean
	 */
	
	function nextDataset($i) {
		if($this->EA->getEntries() != 0) {
			if($i!=0) {
			
				if(next($this->navi)) {
				
					return true;
				}
				else {
				
					return false;
				
				}
			
			}
			else {
			
				reset($this->navi);
				return true;
			
			}
		}
		else {
			// liefer false zurueck, da keine Datensaetze vorhanden (fuer die Schleifen...)
			return false;	
		}	
	}
	/**
	 * 
	 * Diese Methode setzt den internen Array Zeiger auf die Ausgangslage zurueck
	 * @access public
	 * @version 1.0
	 * 
	 */
	function resetDataset() {
	
		return reset($this->navi);
		
	}
	/**
	 * 
	 * Diese Methode liefert den Datensatz der vor dem derzeitigen liegt
	 * @access public
	 * @version 1.0
	 * 
	 */
	function prevDataset() {
	
		return prev($this->navi);
		
	}
	
	// Ausgabefunktionen
	/**
	 * 
	 * Gibt das aktuelle Icon fuer die Navigation aus (geschlossen, offen, keine subnavi)
	 * @version 1.0
	 * @access public
	 * @param string $href	href-attribut fuer den a-tag (optional)
	 * @return String
	 * 
	 */
	function getToggleIcon($href='') {
		
		$current = current($this->navi);

		$sub = $current['sub'][0];

		$ebene = $current['ebene'];
		
		if(!$this->hasSubMenue($current['id'])) {
			
			#return $this->aToggleIcon['here'];
			return $this->getImageLink('here');
			
		}
		
		if($this->hasSubMenue($current['id']) && $this->iToggleId != $current['id'] && !isset($this->active['togs'][$current['id']])) {
		
			#return $this->aToggleIcon['show'];
			return $this->getImageLink('show', $href);
			
		}

		if($this->hasSubMenue($current['id']) && array_key_exists($current['id'],$this->active['togs'])) {
		
			#return $this->aToggleIcon['hide'];
			return $this->getImageLink('hide', $href);
			
		}
	}
	/**
	 * 
	 * Gibt das aktuelle Icon und den dazugehuerigen Link fuer die Navigation aus (geschlossen, offen, keine subnavi)
	 * @version 1.0
	 * @access public
	 * @return String
	 * 
	 */
	function getToggleLink() {
	
		$current = current($this->navi);
		
		$ebene = $current['ebene'];
		
		if(!isset($this->active['togs'][$current['id']])) {
			
			$id = $current['id'];
			
		}
		else {
			
			if($current['parentid'] == 0) {
					
				$id = 0;
				
			}
			else {
				
				$id = $current['parentid'];
					
			}
				
		}
		if(!empty($current['ebene'])) {
			
			$width = $current['ebene']*17;
			
			$link = '<img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="'.$width.'" height="1" alt="" border="0">';
				
		}
		
		$link .= $this->getToggleIcon($_SERVER['PHP_SELF']."?nToggleId=".$id);
		
		return $link;
	}
	/**
	 * 
	 * Gibt den aktuellen Titel fuer den Navigationspunkt aus
	 * @version 1.0
	 * @access public
	 * @return String
	 * 
	 */
	function getToggleTitle() {
	
		$current = current($this->navi);
		
		return $current['title'];
		
	}
	/**
	 * 
	 * Gibt die Anzahl der Entries in der Navigation aus und beruecksichtigt welcher Punkt offen ist
	 * @version 1.0
	 * @access public
	 * @return String
	 * 
	 */
	function getEntries() {
	
		$i=0;
	
		foreach($this->navi as $key) {
		
			$i++;
			
		}
		return $i;
	}
	/**
	 * 
	 * Gibt die Prioritaetenparameter aus
	 * @access private
	 * @version 1.0
	 * @param String $priomove
	 * @return String
	 * 
	 */
	function getTogglePrioLink($priomove) {
		
		$current = current($this->navi);
		
		$params = 'naviid='.$current['id'].'&priomove='.$priomove.'&nToggleId='.$this->iToggleId;

		return $params;
	}
	
	/**
	 * 
	 * Liefert die Prioritaeten Icons und Links fuer das aktuelle Element zurueck
	 * @access public
	 * @version 1.0
	 * @return String
	 * 
	 */
	
	function getTogglePrio() {
	
		$current = current($this->navi);
				
		$bean = $this->EA->getBean();
		
		$navi = $this->navi;
		
		for($i=0;next($navi);$i++) {
		
			if($i==0) {
			
				reset($navi);
				
			}
		
			$ncurrent = current($navi);
			
			$ebenen[$ncurrent['ebene']][] = $ncurrent['id'];
			
		}
		
		for($i=0;!empty($ebenen[$i][0]);$i++) {
		
			for($j=0;!empty($ebenen[$i][$j]);$j++) {
			
				if($current['id'] == $ebenen[$i][$j]) {
				
					$lastid = $ebenen[$i][($j-1)];
					$nextid = $ebenen[$i][($j+1)];
					
					if($lastid != 0 && $current['ebene'] == $i) {
				
						if($j!=0) {
							
							#$img .= '<a href="'.$this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('first').'">';
							#$img .= $this->aPrioIcon['first'];
							#$img .= '</a>';
							$img .= $this->getImageLink('first', $this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('first'));
						
						}
						else {
						
							#$img .= $this->aPrioIcon['firstdeak'];
							$img .= $this->getImageLink('firstdeak');
						
						}
						#$img .= '<a href="'.$this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('up').'">';
						#$img .= $this->aPrioIcon['up'];
						#$img .= '</a>';
						$img .= $this->getImageLink('up', $this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('up'));
						
					}
					else {
				
						#$img .= $this->aPrioIcon['firstdeak'];
						#$img .= $this->aPrioIcon['updeak'];
						$img .= $this->getImageLink('firstdeak');
						$img .= $this->getImageLink('updeak');
				
					}
					
					if($nextid != 0 && $current['ebene'] == $i) {
				
						#$img .= '<a href="'.$this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('down').'">';
						#$img .= $this->aPrioIcon['down'];
						#$img .= '</a>';
						#$img .= '<a href="'.$this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('last').'">';
						#$img .= $this->aPrioIcon['last'];
						#$img .= '</a>';
						$img .= $this->getImageLink('down', $this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('down'));
						$img .= $this->getImageLink('last', $this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?'.$this->getTogglePrioLink('last'));
						
					}
					else {
				
						#$img .= $this->aPrioIcon['lastdeak'];
						#$img .= $this->aPrioIcon['downdeak'];
						$img .= $this->getImageLink('lastdeak');
						$img .= $this->getImageLink('downdeak');
						
					}
					
				}
				
			}
			
		}
		
		return $img;
	}
	
	/**
	 * Baut das HTML f. d. entsprechende Image (ggf. inkl. Link)
	 * @access public
	 * @version 1.0
	 * @param String	Welches Image (key)
	 * @param String	href-Attribut des optionalen anker-tags
	 * @return String
	 */
	function getImageLink($which, $href='') {
		$buffer = '';
		// fallunterscheidung welches image -> img-src, title + template ermitteln
		switch (strtolower($which)) {
			case 'hide':
				$html = 'toggleIcon';
				$img = 'btn_hide.gif';
				$title = $this->aMSG['navi']['hide_subsections'][$this->syslang];
				break;
			case 'show':
				$html = 'toggleIcon';
				$img = 'btn_show.gif';
				$title = $this->aMSG['navi']['show_subsections'][$this->syslang];
				break;
			case 'here':
				$html = 'toggleIcon';
				$img = 'btn_here.gif';
				$title = '';
				break;
			case 'first':
				$html = 'prioIcon';
				$img = 'pf_abstop.gif';
				$title = $this->aMSG['btn']['prio_top'][$this->syslang];
				break;
			case 'last':
				$html = 'prioIcon';
				$img = 'pf_absbottom.gif';
				$title = $this->aMSG['btn']['prio_btm'][$this->syslang];
				break;
			case 'up':
				$html = 'prioIcon';
				$img = 'pf_up.gif';
				$title = $this->aMSG['btn']['prio_up'][$this->syslang];
				break;
			case 'down':
				$html = 'prioIcon';
				$img = 'pf_down.gif';
				$title = $this->aMSG['btn']['prio_dwn'][$this->syslang];
				break;
			case 'downdeak':
				$html = 'prioIcon';
				$img = 'd_pf_down.gif';
				$title = '';
				break;
			case 'updeak':
				$html = 'prioIcon';
				$img = 'd_pf_up.gif';
				$title = '';
				break;
			case 'firstdeak':
				$html = 'prioIcon';
				$img = 'd_pf_abstop.gif';
				$title = '';
				break;
			case 'lastdeak':
				$html = 'prioIcon';
				$img = 'd_pf_absbottom.gif';
				$title = '';
				break;
			case 'az':
				$html = 'button';
				$img = 'btn_az.gif';
				$title = $this->aMSG['btn']['prio_az'][$this->syslang];
				break;
		}
		// ggf. link
		if ($href != '') {
			$buffer .= str_replace(array('{HREF}','{TITLE}','{EXTRA}'), array($href, $title, ''), $this->aHtml['a_start']);
		}
		// image
		$buffer .= str_replace(array('{IMG}','{TITLE}'), array($img, $title), $this->aHtml[$html]);
		// ggf. link
		if ($href != '') {
			$buffer .= $this->aHtml['a_end'];
		}
		// output
		return $buffer;
	}

	/**
	 * Baut das HTML f. d. Sortierbutton (inkl. Link)
	 * @access public
	 * @version 1.0
	 * @return String
	 */
	function getNewSortLink() {
		$current = current($this->navi);
		
		#$img .= '<a href="'.$this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?sorttoggle='.$current['id'].'&sortway=ASC&nToggleId='.$current['id'].'">';
		#$img .= $this->getPrioIcons('sort A-Z','down');
		#$img .= '</a>';
		#$img .= '<a href="'.$this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?sorttoggle='.$current['id'].'&sortway=DESC&nToggleId='.$current['id'].'">';
		#$img .= $this->getPrioIcons('sort Z-A','up');
		#$img .= '</a>';
		#return $img;
		
		return $this->getImageLink('az', $this->aENV['SELF_PATH'].basename($_SERVER['SCRIPT_NAME']).'?sorttoggle='.$current['id'].'&sortway=ASC&nToggleId='.$current['id']);
	}
}
?>