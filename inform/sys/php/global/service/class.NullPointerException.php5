<?php
/*
 * Created on 02.02.2006
 *
 */
/**
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * 
 */
	class NullPointerException extends Exception {
		// constructor
  	 // Die Ausnahmemitteilung neu definieren, damit diese nicht optional ist
 	  public function __construct($message, $code = 0) {
       // etwas Code
  
       // sicherstelen, dass alles korrekt zugewiesen wird
       parent::__construct($message, $code);
  	 }
	
  	 // maßgeschneiderte Stringdarstellung des Objektes
		public function __toString() {
			return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
		}

		public function customFunction() {
			echo "Eine eigene Funktion dieses Ausnahmetypen\n";
		}
	}
?>