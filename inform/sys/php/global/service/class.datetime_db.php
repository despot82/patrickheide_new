<?php // Class datetime_db
/**
* This class extends class datetime and provides common datetime-functions for DB-usage
* in following country-modes (languages):
*	- [de]		german			(default!)
*	- [en-gb]	british english
*	- [en-us]	american english
*	- [fr]		french
*
* MySQL-DATE = ISO-Datum (nach DIN 5008)
*
* Example:
* <pre><code>
* // init
* $oDate = new datetime_db;
* // format MySQL-DATE (ISO-Datum) in traditional english/german date and checks if there is no day (or no month or even no year)
* echo $oDate->date_mysql2trad($sDate);
* </code></pre>
*
* @access   public
* @package  service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.91 / 2006-03-03	[BUGFIX: Punkt nach $day in "date_long_from_isodate()"]
*/

class datetime_db extends da_DateTime {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse datetime_db:
		----------------------------------------------------------------------------
		!NO konstruktor
		function date_mysql2trad($sDate)
		function date_long_from_isodate($sDate)
		function time_mysql2trad($sTime)
		function datetime_mysql2trad($sDatetime)
		function date_trad2mysql($sDate)
		
		function year_only_from_isodate($sDate)
		function monthname_only_from_isodate($sDate)
		function month_only_from_isodate($sDate)
		function dayname_only_from_isodate($sDate)
		function day_only_from_isodate($sDate)
		
		function timestamp_from_date($sDate)
		function date_from_mysql_timestamp($sTimestamp,$iso=false)
		function time_from_mysql_timestamp($t,$sec=false)
		function convert_timestamp($ts)
		function get_mysql_timestamp()
		----------------------------------------------------------------------------
		HISTORY:
		1.91 / 2006-03-03	[BUGFIX: Punkt nach $day in "date_long_from_isodate()"]
		1.9 / 2005-08-18	[NEU: "_check_mysql_timestamp()": interne Formatueberpruefung b.d. Timestamp-Methoden]
		1.8 / 2004-11-03	[NEU: "convert_timestamp()"]
		1.7 / 2004-08-11	[NEU: "dayname_only_from_isodate()"]
		1.61 / 2004-01-26	[nur kommentare fuer PHPDoc ueberarbeitet + alte Funktion entfernt]
		1.6 / 2003-07-31	[NEU: function "get_mysql_timestamp()" + 
							NEU: function "gtime_from_mysql_timestamp()" + 
							"get_date_from_timestamp()" in "date_from_mysql_timestamp()" umbenannt + 
							"get_timestamp_from_date()" in "timestamp_from_date()" umbenannt!]
		1.51 / 2003-06-13	[nur kommentare fuer PHPDoc ueberarbeitet]
		1.5 / 2003-05-27
	*/

#-----------------------------------------------------------------------------

/**
* Formatiert ein MySQL-DATE (ISO-Datum) in ein traditionelles (engl./deut.) Datum und checkt, 
* ob Tag, Monat oder Jahr ausgegeben werden soll. 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* // format MySQL-DATE (ISO-Datum) in traditional english/german date and checks if there is no day (or no month or even no year)
* echo $oDate->date_mysql2trad("2002-12-24"); // (=>24.12.2002)
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum
* @return	string	trad.Datum (laenderspezifisch)
*/
	function date_mysql2tradshort($sDate, $bWithWeekdayname=false) {
		
		if (!$sDate) return; // check vars
		
		/* OHNE unix-timestamp!
		$timestamp = $this->timestamp_from_date($sDate);
		$fullyear = false;
		return $this->get_today($fullyear, $timestamp, $bWithWeekdayname);
		*/
		
		// $weekdayname muss leider mittels unix-timestamp ermittelt werden...
		$weekdayname = ($bWithWeekdayname) ? $this->get_dayname($this->timestamp_from_date($sDate), false) : '';
		if (!empty($weekdayname)) $weekdayname .= ', ';
		
		$trenner = ($this->sLang == 'fr') ? '/' : '.';
		list($year, $month, $day) = explode('-', $sDate);
		$day	= ($day == '00') ? '' : $day.$trenner;
		$month	= ($month == '00') ? '' : $month.$trenner;
		$year	= ($year == '0000') ? '' : substr($year,-2);
		
		if ($this->sLang == 'de')		{ $sDateFormatted = $day.$month.$year; }
		if ($this->sLang == 'en-gb')	{ $sDateFormatted = $day.$month.$year; }
		if ($this->sLang == 'en-us')	{ $sDateFormatted = $month.$day.$year; }
		if ($this->sLang == 'fr')		{ $sDateFormatted = $day.$month.$year; }
		
		return $weekdayname.$sDateFormatted;
	}
	
/**
* Formatiert ein MySQL-DATE (ISO-Datum) in ein traditionelles (engl./deut.) Datum und checkt, 
* ob Tag, Monat oder Jahr ausgegeben werden soll. 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* // format MySQL-DATE (ISO-Datum) in traditional english/german date and checks if there is no day (or no month or even no year)
* echo $oDate->date_mysql2trad("2002-12-24"); // (=>24.12.2002)
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum
* @return	string	trad.Datum (laenderspezifisch)
*/
	function date_mysql2trad($sDate, $bWithWeekdayname=false) {
		
		if (!$sDate) return; // check vars
		
		/* OHNE unix timestamp!
		$timestamp = $this->timestamp_from_date($sDate);
		$fullyear = true;
		return $this->get_today($fullyear, $timestamp, $bWithWeekdayname);
		*/
		
		// $weekdayname muss leider mittels unix-timestamp ermittelt werden...
		$weekdayname = ($bWithWeekdayname) ? $this->get_dayname($this->timestamp_from_date($sDate), false) : '';
		if (!empty($weekdayname)) $weekdayname .= ', ';
		
		$trenner = ($this->sLang == 'fr') ? '/' : '.';
		list($year, $month, $day) = explode('-', $sDate);
		$day	= ($day == '00') ? '' : $day.$trenner;
		$month	= ($month == '00') ? '' : $month.$trenner;
		$year	= ($year == '0000') ? '' : $year;
		
		if ($this->sLang == 'de')		{ $sDateFormatted = $day.$month.$year; }
		if ($this->sLang == 'en-gb')	{ $sDateFormatted = $day.$month.$year; }
		if ($this->sLang == 'en-us')	{ $sDateFormatted = $month.$day.$year; }
		if ($this->sLang == 'fr')		{ $sDateFormatted = $day.$month.$year; }
		
		return $weekdayname.$sDateFormatted;
	}

/**
* Zeigt das ISO-Datum mit ausgeschriebenem Monatsnamen und Tag ohne fuehrende Null  (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* echo $oDate->date_long_from_isodate($sDate); // -> z.B.: '1. Januar 2003'
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum
* @return	string	trad.Datum (laenderspezifisch) (mit Monatsname)
*/
	function date_long_from_isodate($sDate, $bWithWeekdayname=false) {
		
		if (!$sDate) return; // check vars
		
		/*
		$timestamp = $this->timestamp_from_date($sDate);
		return $this->get_today_long($timestamp, $bWithWeekdayname);
		*/
		
		// $weekdayname muss leider mittels unix-timestamp ermittelt werden... (hier voller name!)
		$weekdayname = ($bWithWeekdayname) ? $this->get_dayname($this->timestamp_from_date($sDate), true) : '';
		if (!empty($weekdayname)) $weekdayname .= ', ';
		
		list($year, $month, $day) = explode('-', $sDate);
		$day	= ($day == '00') ? '' : $day;
		$month	= ($month == '00') ? '' : $month;
		$year	= ($year == '0000') ? '' : $year;
		
		if ($day < 10) { $day = substr($day, 1, 2); }
		
		if ($this->sLang == 'de')		{
			if ($day != '') $sDateFormatted = $day.'. '; // behandlung day
			$sDateFormatted .= $this->aMonthnameFull['de'][$month].' '.$year; // plus month & year
		}
		if ($this->sLang == 'en-gb')	{ $sDateFormatted = $day.' '.$this->aMonthnameFull['en'][$month].' '.$year; }
		if ($this->sLang == 'en-us')	{ $sDateFormatted = $this->aMonthnameFull['en'][$month].' '.$day.', '.$year; }
		if ($this->sLang == 'fr')		{ $sDateFormatted = $day.' '.$this->aMonthnameFull['fr'][$month].' '.$year; }
		
		return $weekdayname.$sDateFormatted;
	}

/**
* Formatiert ein MySQL-TIME in eine traditionelle (engl./deut.) Zeit 
* (mit der Option: mit- oder ohne-Sekunden Ausgabe) 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* echo $oDate->time_mysql2trad("23:47:16",true); // (=>23:47:16)
* echo $oDate->time_mysql2trad("23:47:16"); // (=>23:47)
* </code></pre>
*
* @access   public
* @param	string	$sTime	MySQL TIME
* @param	boolean	$sec	[true=mit|false=ohne] sekunden
* @return	string	trad.Time (laenderspezifisch)
*/
	function time_mysql2trad($sTime, $sec=false) {
		
		if (!$sTime) return; // check vars
		
		list($h, $m, $s) = explode(":", $sTime);
		
		if ($this->sLang == "de")		{
			$sTimeFormatted = $h.":".$m;
			if ($sec == true) { $sTime .= ":".$s; }
		}
		if ($this->sLang == "en-gb")	{
			$sTimeFormatted = $h.":".$m;
			if ($sec == true) { $sTime .= ":".$s; }
		}
		if ($this->sLang == "en-us")	{
			if ($h > 12) { $h -= 12; $e = " pm"; } else { $e = " am"; }
			$sTimeFormatted = $h.":".$m;
			if ($sec == true) { $sTimeFormatted .= ":".$s; }
			$sTimeFormatted .= $e;
		}
		if ($this->sLang == "fr")		{
			$sTimeFormatted = $h.":".$m;
			if ($sec == true) { $sTime .= ":".$s; }
		}
		
		return $sTimeFormatted;
	}
	
	
/**
* Formatiert ein MySQL-DATETIME in ein traditionelles (engl./deut.) Datum und Uhrzeit und checkt, 
* ob Tag, Monat oder Jahr ausgegeben werden soll. 
* (mit der Option: mit- oder ohne-Sekunden Ausgabe) 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* echo $oDate->datetime_mysql2trad("2002-12-24 23:47:16"); // (=>24.12.2002 23:47)
* </code></pre>
*
* @access   public
* @param	string	$sDatetime	ISO-Datum
* @param	boolean	$sec	[true=mit|false=ohne] sekunden
* @return	string	trad.Datum Uhrzeit (laenderspezifisch)
*/
	function datetime_mysql2trad($sDatetime, $sec=false, $bWithWeekdayname=false) {
		
		if (!$sDatetime) return; // check vars
		
		list($sDate, $sTime) = explode(" ", $sDatetime);
		$sDate = $this->date_mysql2trad($sDate, $bWithWeekdayname);
		$sTime = $this->time_mysql2trad($sTime, $sec);
		
		return "<b>".$sDate."</b> ".$sTime;
	}

/**
* Formatiert ein traditionelles (engl./deut.) Datum in ein MySQL-DATE (ISO-Datum). 
* Das Datum muss in der richtigen Laender-Mode-Einstellung eingegeben werden 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* // format traditional english/german date in MySQL-DATE (ISO-Datum) and checks if there is no day (or no month or even no year)
* $oDate->set_lang_to_us();
* echo $oDate->date_trad2mysql("12.24.2002"); // (=>2002-12-24)
* </code></pre>
*
* @access   public
* @param	string	$sDate	trad.Datum (laenderspezifisch)
* @return	string	ISO-Datum
*/
	function date_trad2mysql($sDate) {
		
		if (!$sDate) return; // check vars
		$sDate = preg_replace('/\D/', '', $sDate); // alles ausser Ziffern  entfernen
		
		if ($this->sLang == "en-us")	{
			$m = substr($sDate, 0, 2); $d = substr($sDate, 2, 2); $y = substr($sDate, 4);
		} else {
			$d = substr($sDate, 0, 2); $m = substr($sDate, 2, 2); $y = substr($sDate, 4);
		}
		if (strlen($y) == 2) { $y = '20'.$y; }	// ggf. einem 2-stelligem Jahr "20" voranstellen
		
		return sprintf("%04d-%02d-%02d", $y, $m, $d);
	}


#-----------------------------------------------------------------------------

/**
* Gibt aus einem MySQL-DATE (ISO-Datum) NUR das Jahr aus (falls es nicht 0000 ist).
*
* Beispiel:
* <pre><code>
* $oDate->set_lang_to_us();
* echo $oDate->year_only_from_isodate("2002-12-24"); // (=>2002)
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum
* @return	string	Jahr
*/
	function year_only_from_isodate($sDate) {
		
		if (!$sDate) return; // check vars
		
		list($year, $month, $day) = explode("-", $sDate);
		$year = ($year == "0000") ? '' : $year;
		return $year;
	}

/**
* Gibt aus einem MySQL-DATE (ISO-Datum) NUR den Monatsnamen aus (falls es nicht 00 ist). 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* $oDate->set_lang_to_us();
* echo $oDate->monthname_only_from_isodate("2002-12-24"); // (=>December)
* </code></pre>
*
* @access   public
* @param	string	$sDate		ISO-Datum
* @param	boolean	$fullname	voller Name oder Kurzname (optional, default: true = voller Name)
* @return	string	Monatsname (laenderspezifisch)
*/
	function monthname_only_from_isodate($sDate, $fullname=true) {
		
		if (!$sDate) return; // check vars
		
		list($y, $m, $d) = explode("-", $sDate);
		if ($fullname) {
			if ($this->sLang == "de")		{ return $this->aMonthnameFull['de'][$m]; }
			if ($this->sLang == "en-gb")	{ return $this->aMonthnameFull['en'][$m]; }
			if ($this->sLang == "en-us")	{ return $this->aMonthnameFull['en'][$m]; }
			if ($this->sLang == "fr")		{ return $this->aMonthnameFull['fr'][$m]; }
		} else {
			if ($this->sLang == "de")		{ return $this->aMonthnameShort['de'][$m]; }
			if ($this->sLang == "en-gb")	{ return $this->aMonthnameShort['en'][$m]; }
			if ($this->sLang == "en-us")	{ return $this->aMonthnameShort['en'][$m]; }
			if ($this->sLang == "fr")		{ return $this->aMonthnameShort['fr'][$m]; }
		}
	}


/**
* Gibt aus einem MySQL-DATE (ISO-Datum) NUR den Monat aus (falls es nicht 00 ist).
*
* Beispiel:
* <pre><code>
* $oDate->set_lang_to_us();
* echo $oDate->month_only_from_isodate("2002-12-24"); // (=>12)
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum
* @return	string	Monat
*/
	function month_only_from_isodate($sDate) {
		
		if (!$sDate) return; // check vars
		
		list($y, $m, $d) = explode("-", $sDate);
		$m = ($m == "00") ? '' : $m;
		return $m;
	}

/**
* Gibt aus einem MySQL-DATE (ISO-Datum) NUR den Wochentagsnamen aus (falls es nicht 00 ist). 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* $oDate->set_lang_to_us();
* echo $oDate->dayname_only_from_isodate("2002-12-24"); // (=>Tuesday)
* </code></pre>
*
* @access   public
* @param	string	$sDate		ISO-Datum
* @param	boolean	$fullname	voller Name oder Kurzname (optional, default: true = voller Name)
* @return	string	Tagname (laenderspezifisch)
*/
	function dayname_only_from_isodate($sDate, $fullname=true) {
		
		if (!$sDate) return; // check vars
		
		$d = date("w", $this->timestamp_from_date($sDate)); // mum. Tag der Woche ermitteln (0(So)-6(Sa))
		if ($fullname) {
			if ($this->sLang == "de")		{ return $this->aDaynameFull['de'][$d]; }
			if ($this->sLang == "en-gb")	{ return $this->aDaynameFull['en'][$d]; }
			if ($this->sLang == "en-us")	{ return $this->aDaynameFull['en'][$d]; }
			if ($this->sLang == "fr")		{ return $this->aDaynameFull['fr'][$d]; }
		} else {
			if ($this->sLang == "de")		{ return $this->aDaynameShort['de'][$d]; }
			if ($this->sLang == "en-gb")	{ return $this->aDaynameShort['en'][$d]; }
			if ($this->sLang == "en-us")	{ return $this->aDaynameShort['en'][$d]; }
			if ($this->sLang == "fr")		{ return $this->aDaynameShort['fr'][$d]; }
		}
	}

/**
* Gibt aus einem MySQL-DATE (ISO-Datum) NUR den Tag aus (falls es nicht 00 ist).
*
* Beispiel:
* <pre><code>
* $oDate->set_lang_to_us();
* echo $oDate->day_only_from_isodate("2002-12-24"); // (=>24)
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum
* @return	string	Tag
*/
	function day_only_from_isodate($sDate) {
		
		if (!$sDate) return; // check vars
		
		list($y, $m, $d) = explode("-", $sDate);
		$d = ($d == "00") ? '' : $d;
		return $d;
	}


#-----------------------------------------------------------------------------

/**
* Formatiert ein MySQL-DATE (ISO-Datum) in ein MySQL-TIMESTAMP
*
* Beispiel:
* <pre><code>
* // generate timestamp out of ISO-date
* echo $oDate->timestamp_from_date("2002-12-24"); // (=>1040684400)
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum
* @return	string	timestamp
*/
	function timestamp_from_date($sDate) {
		
		if (!$sDate) return; // check vars
		
		list($y, $m, $d) = explode("-", $sDate);
		$timestamp = mktime(0, 0, 0, $m, $d, $y);
		return $timestamp;
	}

/**
* Formatiert ein MySQL-DATETIME (ISO-Datum/Zeit) in ein MySQL-TIMESTAMP
*
* Beispiel:
* <pre><code>
* // generate timestamp out of ISO-date-time
* echo $oDate->timestamp_from_date("2002-12-24 00:00:00"); // (=>1040684400)
* </code></pre>
*
* @access   public
* @param	string	$sDate	ISO-Datum/Zeit
* @return	string	timestamp
*/
	function timestamp_from_datetime($sDate) {
		
		if (!$sDate) return; // check vars
		$aData	= explode(" ",$sDate);

		list($y, $m, $d) = explode("-", $aData[0]);
		list($h, $i, $s) = explode(":", $aData[1]);
		
		$timestamp = mktime($h, $i, $s, $m, $d, $y);

		return $timestamp;
	}

/**
* Formatiert ein MySQL-TIMESTAMP in ein traditionelles Datum (default) 
* (oder in ein MySQL-DATE (ISO-Datum) (bei optional 'true))
*
* Beispiel:
* <pre><code>
* // generate ISO-date out of mysql-timestamp
* echo $oDate->date_from_mysql_timestamp("20021224121200"); // (=>2002-12-24)
* </code></pre>
*
* @access   public
* @param	string	$sTimestamp	Timestamp
* @param	boolean	$iso	[true=ISO-Datum|false=trad.Datum]
* @return	string	Datum
*/
	function date_from_mysql_timestamp($t, $iso=false) {
		
		if (!$t) return; // check vars
		$t = $this->_check_mysql_timestamp($t);
		
		$sDate = sprintf("%04d-%02d-%02d", substr($t, 0, 4), substr($t, 4, 2), substr($t, 6, 2));
		if ($iso == false) { $sDate = $this->date_mysql2trad($sDate); }
		return $sDate;
	}

/**
* Formatiert ein MySQL-TIMESTAMP in eine traditionelle Uhrzeit
* (wenn der optionale zweite Parameter "true" uebergeben wird, werden acuh die Sekunden angezeigt)
*
* Beispiel:
* <pre><code>
* // generate time out of mysql-timestamp
* echo $oDate->time_from_mysql_timestamp("20021224121200"); // (=>12:12)
* </code></pre>
*
* @access   public
* @param	string	$sTimestamp	Timestamp
* @param	boolean	$iso	[true=ISO-Datum|false=trad.Datum]
* @return	string	Datum
*/
	function time_from_mysql_timestamp($t, $sec=false) {
		
		if (!$t) return; // check vars
		$t = $this->_check_mysql_timestamp($t);
		
		$sTime = sprintf("%02d:%02d", substr($t, 8, 2), substr($t, 10, 2));
		$sSec = sprintf(":%02d", substr($t, 12, 2));
		if ($sec == true) { $sTime .= $sSec; }
		return $sTime;
	}

/**
* Formatiert ein MySQL-TIMESTAMP in ein UNIX-Timestamp oder umgekehrt.
*
* Beispiel:
* <pre><code>
* // generate unix-timestamp out of mysql-timestamp
* echo $oDate->convert_timestamp("20021224121200"); // (=>1040728320)
* </code></pre>
*
* @access   public
* @param	string	$sTimestamp	Timestamp
* @param	boolean	$iso	[true=ISO-Datum|false=trad.Datum]
* @return	string	Datum
*/
	function convert_timestamp($ts) {
		
		$ts = $this->_check_mysql_timestamp($ts);
		if (!$ts || !is_numeric($ts)) return; // check vars
		
		if (strlen($ts) == 14) {
		// mysql -> unix
			$h = substr($ts, 8, 2);
			$m = substr($ts, 10, 2);
			$s = substr($ts, 12, 2);
			$m = substr($ts, 4, 2);
			$d = substr($ts, 6, 2);
			$y = substr($ts, 0, 4);
			return mktime($h, $m, $s, $m, $d, $y);
		} else {
		// unix -> mysql
			return date("YmdHis", $ts);
		}
	}

/**
* Interne Hilfsfunktion: Checkt das MySQL-TIMESTAMP und konvertiert sie ggf. ins ALTE Format. 
* Das ALTE MySQL-TIMESTAMP ist nach folgendem Muster formatiert: YYYYMMDDhhmmss (NEU: YYYY-MM-DD hh:mm:ss).
*
* @access   private
* @param	string	$sTimestamp	Timestamp
* @return	string	MySQL-TIMESTAMP (ALTES Format)
*/
	function _check_mysql_timestamp($ts) {
		return (strpos($ts, '-')) ? str_replace(array('-', ' ', ':'), '', $ts) : $ts;
	}

/**
* Erstellt eine MySQL-TIMESTAMP fuer die aktuelle Uhrzeit. 
* Eine MySQL-TIMESTAMP ist nach folgendem Muster formatiert: YYYYMMDDhhmmss.
*
* Beispiel:
* <pre><code>
* echo $oDate->get_timestamp();
* </code></pre>
*
* @access   public
* @return	string	aktueller Zeitstempel
*/
	function get_mysql_timestamp() {
		return date("YmdHis");
	}


#-----------------------------------------------------------------------------
} // END of class
?>