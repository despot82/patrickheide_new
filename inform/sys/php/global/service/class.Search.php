<?php // Class Search
/**
* Diese Klasse vereinfacht das Verwalten eines Such-Datensatzes.
*
* NOTE: benoetigt die Klasse "class.db_mysql.php"!
*
* Beispiel:
* <pre><code>
* $oSearch =& new Search($oDb, $aENV, 'cms', $Userdata['id']);
* $oSearch->setRefTable($sTable);
* $oSearch->setRefId($sRefId);
* $oSearch->setTitle($sTitle);
* $oSearch->setText($sText);
* $oSearch->setLang($weblang);
* $oSearch->save();
* # ODER einfach:
* $oSearch->save($sRefId, $sRefTable, $sTitle, $sText, $sLang);
* </code></pre>
*
* @access   public
* @package  service
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 / 2006-04-28	[NEU]
*/
class Search {

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	object	DB-Objekt (muss beim initialisieren der Klasse uebergeben werden)
*/
	var $oDb;
/**
* @access   private
* @var	 	array	globales Environment-Array
*/
	var $aENV;
/**
* @access   public
* @var	 	string	Search-Tabelle
*/
	var $sTable;
/**
* @access   public
* @var	 	string	Sprachversion des search-datensatzes (optional)
*/
	var $sLang;
/**
* @access   public
* @var	 	string	der Teil des Textes, der in der Suchausgabe als Titel angezeigt wird
*/
	var $sTitle;
/**
* @access   public
* @var	 	string	der Text, der in die search-table in das feld searchtext geschrieben wird
*/
	var $sText;
/**
* @access   public
* @var	 	int		ID des aktuellen Users aus der Session (wird beim initialisieren der Klasse importiert)
*/
	var $nUserId;
/**
* @access   private
* @var	 	string	Name des Moduls dieser Suche [cms|adr|...]
*/
	var $sModule;
	
/**
 * @access	private 
 * @var		int		Anzahl der enthaltenden Fulltextergebnisse
 */
	var $numFulltext;
	
/**
 * @access	private 
 * @var		int		Anzahl der enthaltenden Searchtextergebnisse
 */
	var $numSearch;
	
/**
 * @access	private
 * @var		array	Ergebnismenge der Datensätze
 */
	var $aSearchResult;
	
	var $entries;
	
#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das Objekt und bekommt ein DB-Objekt uebergeben
*
* @access   public
* @param	object	$oDb		Datenbank-Objekt, welches fuer die Klasse "class.db_mysql.php" benoetigt wird
* @param	array	$aENV		globales Inform Environment Array
* @param	string	$sModule	Modul [cms|adr|...]
* @param	int		$nUserId	User-Id
* @return   void
*/
	function Search(&$oDb, &$aENV, $sModule='cms', $nUserId=0) {
		// DB-Objekt importieren
		if (!is_object($oDb)) {
			//echo "<script>alert('".basename(__FILE__)."-ERROR: \\n -> Es wurde kein DB-Objekt uebergeben!')</script>";
			trigger_error('".basename(__FILE__)."-ERROR: \\n -> Es wurde kein DB-Objekt uebergeben!',E_USER_ERROR);
			return;
		}
		$this->oDb			= $oDb;
		// globales ENV-Array importieren
		if (!is_array($aENV)) die(trigger_error("ERROR: NO aENV!",E_USER_ERROR));
		$this->aENV			= $aENV;
		// vars
		$this->sTable		= (isset($aENV['table']['sys_global_search']) && !empty($aENV['table']['sys_global_search'])) ? $aENV['table']['sys_global_search'] : 'sys_global_search';
		$this->sModule		= $sModule; // Modul [cms|adr|...]
		if(!empty($sModule) && !is_null($sModule)) {
			$this->setFilter(" module='$sModule'");
		}
		$this->nUserId		= $nUserId; // System-User-ID
	}

#----------------------------------------------------------------------------- SET

	function getNumfulltext() {
		return $this->numFulltext;
	}

	function getNumsearchtext() {
		return $this->numSearch;
	}
	
	function getSearchresult() {
		return $this->aSearchResult;
	}
	
	function getEntries() {
		return $this->entries;
	}
/**
* ADMIN: Setzt den Tabellennamen in der der Suchdatensatz gespeichert ist
* 
* @access   public
* @param	string	$sTable		Tablename 
* @return	void
*/
	function setRefTable($sTable) {
		$this->sRefTable	= trim($sTable);
	}

/**
* ADMIN: Setzt die ID unter der der Suchdatensatz gespeichert ist
* 
* @access   public
* @param	string	$sId		ID 
* @return	void
*/
	function setRefId($sId) {
		$this->sRefId	= trim($sId);
	}

/**
* ADMIN: Setzt den Teil des Textes, der in der Suchausgabe als Titel angezeigt wird
*
* @access   public
* @param	string	$sTitle			der Teil des Textes, der in der Suchausgabe als Titel angezeigt wird
* @return	void
*/
	function setTitle($sTitle) {
		$this->sTitle	= trim($sTitle);
	}

/**
* ADMIN: Setzt den searchstring (ohne Titel! -> Der Suchtext ist der Teil des Textes der durchsucht wird.
*
* @access   public
* @param	string	sText		der Text, der in die search-table in das feld searchtext geschrieben wird
* @return	void
*/
	function setText($sText) {
		$this->sText	= trim($sText);
	}

/**
* ADMIN: Setzt als Bedingung ein Sprachkuerzel (wird in das Feld "lang" eingetragen bzw. abgefragt)
* Gibt es fuer einen Datensatz zwei (oder mehr) Sprachversionen, muss die jeweilige aktuelle Sprache dieser Methode uebergeben werden.
* In der Search-Tabelle wird dann fuer jede Sprachversion ein eigener Datensatz angelegt (und entsprechend in der Ausgabe abgefragt bzw. durchsucht).
*
* @access   public
* @param	string	$sLang			optionale Kennung der Sprachversion des Suchdatensatzes
* @return	void
*/
	function setLang($sLang) {
		$this->sLang	= trim($sLang);
	}

	function setFilter($sFilter) {
		if(!empty($this->oDb->soptions['extra'])) $this->oDb->soptions['extra'] .= ' AND ';
		$this->oDb->soptions['extra'] .= $sFilter;
	}
#------------------------------------------------------------------------------- SELECT

	/**
	 * 
	 * Diese Methode führt erst eine Fulltextsuche und im Anschluß direkt eine Patter-Suche durch (Like).
	 * Die Ergebnismengen werden zwischengespeichert in aSearchResult und die NumFulltextsearch und NumSearchtext werden gefüllt.
	 * 
	 * @access public
	 * @param string $sSearch
	 * @param array $aFields
	 * @param int $iStart
	 * @param int $iLimit
	 * @param string $sOrder
	 * @return void
	 * 
	 */
	function select($sSearch, $iStart, $iLimit, $sOrder='',$bDoFulltext=true,$bDoWordpart=true) {

		if(empty($sSearch)) {
			trigger_error('Searchstring is empty!',E_USER_NOTICE);
			return false;
		}

		$this->oDb->soptions['search_umlauts'] = false;
		
		$sFields = 'searchtext_mod';
		$this->oDb->soptions['select'] = 'id, ref_table, ref_id, searchtext, searchtext_mod, title, lang';
		$aIds = array();
		
		if($bDoFulltext) {
			if($this->numFulltext==0) {
				$this->oDb->fulltext_search($sFields,$sSearch,$this->sTable, $sOrder); // , $iLimit, $iStart
				if($this->oDb->num_rows() > 0) {
					$this->numFulltext += $this->oDb->num_rows();
					$this->entries = $this->numFulltext;

					for($i=0;$row = $this->oDb->fetch_array();$i++) {
					
						if($i>=$iStart && $i <= ($iStart+$iLimit)) {
							if(isset($row['ref_id']) && isset($row['id'])) {
								$this->aSearchResult[] = array('id' => $row['ref_id'],'ref_table' => $row['ref_table'], 'title' => $row['title'], 'searchtext' => $row['searchtext'], 'type' => 'fulltext', 'lang' => $row['lang']);
								$aIds[] = $row['id'];
							}
						}
					}
					$this->oDb->free_result();
				}
			}
		}

		if($bDoWordpart) {
			if(($this->numSearch==0 && ($this->entries-$iStart) < $iLimit)) {
				$this->oDb->make_search($sFields, $sSearch, $this->sTable, $sOrder);
				if($this->oDb->num_rows() > 0) {
					$this->numSearch += $this->oDb->num_rows();
					$entries=0;
					for($i=0;$row = $this->oDb->fetch_array();$i++) {
						if($i>=$iStart && $i < ($iStart+$iLimit)) {
							if(isset($row['ref_id']) && isset($row['id'])) {
								if(!in_array($row['id'],$aIds)) {
									$this->aSearchResult[] = array('id' => $row['ref_id'],'ref_table' => $row['ref_table'], 'title' => $row['title'], 'searchtext' => $row['searchtext'], 'type' => 'wordpart', 'lang' => $row['lang']);
								}
								else {
									$entries++;
								}
							}
						}
					}
					$this->entries += ($this->numSearch-$entries);
					$this->oDb->free_result();
				}
			}
			
		}
		/*if(!is_null($iLimit) && $iLimit != 0) {
			for($i=$iStart;$i < ($iStart+$iLimit);$i++) {
				$aData[] = $this->aSearchResult[$i];
			}
			$this->aSearchResult =& $aData;
		}*/
		/* dies ist wohl die Alternative, welche noch in einen Join umzusetzen wäre ...
		$sFields = (!is_array($aFields)) ? '*' : implode(',',$aFields);
		$sql = "SELECT DISTINCT(`ref_table`) FROM `".$this->sTable."` WHERE `module`='".$this->sModule."'";
		$oDb = $this->oDb;
		$oDb->query($sql);
		if($oDb->num_rows() > 0) {
			for($i=0;$aRow = $oDb->fetch_array();$i++) {
				$this->oDb->fulltext_search($sFields,$sSearch,$aRow['ref_table'], $sOrder, $iLimit, $iStart);
				if($this->oDb->num_rows() > 0) {
					$this->numFulltext += $this->oDb->num_rows();
					while($row = $this->oDb->fetch_array()) {
						$this->aSearchResult[] = array('id' => $row['id'],'ref_table' => $row['ref_table'], 'title' => $row['title']);
					}
				}
				$this->oDb->make_search($sFields,$sSearch,$aRow['ref_table'], $sOrder, $iLimit, $iStart);
				if($this->oDb->num_rows() > 0) {
					$this->numFulltext += $this->oDb->num_rows();
					while($row = $this->oDb->fetch_array()) {
						$this->aSearchResult[] = array('id' => $row['id'],'ref_table' => $row['ref_table'], 'title' => $row['title']);
					}
				}
			}
		}
		else {
			return false;
		}
		*/
	}
#------------------------------------------------------------------------------- SAVE/INSERT/UPDATE

/**
* ADMIN: Traegt die gewuenschten Inhalte in die Search-Tabelle ein (kuemmert sich selbst um jeweiliges insert/update...).
* Die insert/update Unterscheidung im search-Datensatz wird mittels einer SQL-Query ermittelt.
* Die Daten f.d. Suchdatensatz koennen mit den jeweiligen Set-Methoden gesetzt werden oder 
* dieser Methode als Parameter uebergeben werden. 
*
* Beispiel:
* <pre><code>
* $oSearch->save($sRefId, $sRefTable, $sTitle, $sText, $sLang);
* </code></pre>
*
* @access   public
* @param	string	$sRefId			optional: Reference-Id
* @param	string	$sRefTable		optional: Reference-Table
* @param	string	$sTitle			optional: Search-Title
* @param	string	$sText			optional: Search-Text
* @param	string	$sLang			optional: Language-Version
* @return	boolean	Return-Werte der DB-Insert/Update-Methoden
*/
	function save($sRefId='', $sRefTable='', $sTitle='', $sText='', $sLang='') {
		// ggf. set vars
		if ($sRefId != '')		$this->setRefId($sRefId);
		if ($sRefTable != '')	$this->setRefTable($sRefTable);
		if ($sTitle != '')		$this->setTitle($sTitle);
		if ($sText != '')		$this->setText($sText);
		if ($sLang != '')		$this->setLang($sLang);
		
		// check auf insert oder update
		if ($this->_is_in_searchtable()) {
			// update
			return $this->_update();
		} else {
			// insert
			return $this->_insert();
		}
	}


/**
* Private Funktion, die das Inserten in die search-table vornimmt.
*
* @access   private
* @return	boolean	Return-Wert der DB-Insert-Methode
*/
	function _insert() {
		// get search-data
		$aData = $this->_get_searchdata();
		if (!is_array($aData)) return false; // wenn keine Daten: exit
		
		// make insert
		return $this->oDb->make_insert($aData, $this->sTable);
	}

/**
* Private Funktion, die das Updaten in der search-table vornimmt.
*
* @access   private
* @return	boolean	Return-Wert der DB-Update-Methode
*/
	function _update() {
		// get search-data
		$aData = $this->_get_searchdata();
		if (!is_array($aData)) return false; // wenn keine Daten: exit
		
		// update bedingung definieren
		$aUpdate['ref_table']	= $this->sRefTable;
		$aUpdate['ref_id']		= $this->sRefId;
		if (!empty($this->sLang)) { // fuege ggf. $sLang zum constraint
			$aUpdate['lang']	= $this->sLang;
		}
		// make update
		return $this->oDb->make_update($aData, $this->sTable, $aUpdate);
	}

/**
* Private Funktion, die ueberprueft, ob ein bestimmter Datensatz schon in der search-table vorhanden ist oder nicht.
*
* @access   private
* @return	boolean	(true wenn datensatz vorhanden ist, sonst false)
*/
	function _is_in_searchtable() {
		
		// check auf insert/update
		$sql = "SELECT `id` 
				FROM `".$this->sTable."` 
				WHERE `ref_id`='".$this->sRefId."' 
					AND `ref_table`='".$this->sRefTable."'";
		if (!empty($this->sLang)) { // fuege ggf. $sLang zur ueberpruefung hinzu
			$sql .= " AND `lang`='".$this->sLang."'";
		}
		if (!empty($this->sModule)) { // fuege ggf. $sModule zur ueberpruefung hinzu
			$sql .= " AND `module`='".$this->sModule."'";
		}
		$this->oDb->query($sql);
		
		// output (true wenn datensatz vorhanden ist)
		return ($this->oDb->num_rows() == 0) ? false : true;
	}

/**
* Private Funktion, die die insert/update Daten fuer die search-table zusammenbaut und zurueckgibt.
*
* @access   private
* @return	array	insert/update Daten fuer die search-table
*/
	function _get_searchdata() {
		
		// searchtext zusammenbauen
		$sSearchtext .= $this->sTitle.' '.$this->sText; // title mit in den searchtext nehmen
		$sSearchtext .= ' '.time(); // timestamp hinten dran haengen
		
		// definiere insert/update-values
		$aData = array(
			"module"		=> $this->sModule,
			"ref_table"		=> $this->sRefTable,
			"ref_id"		=> $this->sRefId,
			"searchtext"	=> trim($sSearchtext),
			"searchtext_mod" => trim($this->modifySearchString($sSearchtext)),
			"title"			=> $this->sTitle
		);
		if (!empty($this->sLang)) { $aData['lang'] = $this->sLang; } // fuege ggf. sLang hinzu
		if ($this->sUserId > 0) { $aData['last_mod_by'] = $this->sUserId; } // fuege ggf. sUserId hinzu
		
		// output
		return $aData;
	}

/**
 * Diese Methode bearbeitet einen uebergebenen string fuer das speichern in die suche-table oder das vergleichen bei einer suche.
 * Dazu werden umlaute gegen doppellaute ersetzt und alle anderen sonderzeichen entfernt.
 * 
 * @access	public
 * @param	string	$sString
 * @return	string
 */
	function modifySearchString($sString) {
		if (empty($sString))  return $sString;
		// link hrefs extrahieren
		#$pattern = '/(<a href=")([^>"\s]+)"/i';
		#$replace = '\2 \1\2';
		#$sString = preg_replace($pattern, $replace, $sString);
		// html-tags entfernen
		$sString = strip_tags($sString);
		// umlaut-entities wandeln
		$umls	= array ('&Auml;','&Ouml;','&Uuml;','&auml;','&ouml;','&uuml;','&szlig;');
		$dpls	= array ('Ae','Oe','Ue','ae','oe','ue','ss');
		$sString = str_replace($umls, $dpls, $sString);
		// umlaute wandeln
		$umls	= array ('Ä','Ö','Ü','ä','ö','ü','ß');
		$dpls	= array ('Ae','Oe','Ue','ae','oe','ue','ss');
		$sString = str_replace($umls, $dpls, $sString);
		// sonderzeichen-entities zurueck-wandeln
		if (function_exists('html_entity_decode')) { // erst ab PHP 4.3
			$sString = html_entity_decode($sString);
		} else {
			// replace numeric entities
			$sString = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $sString);
			$sString = preg_replace('~&#([0-9]+);~e', 'chr(\\1)', $sString);
			// replace literal entities
			$trans_tbl = get_html_translation_table(HTML_ENTITIES);
			$trans_tbl = array_flip($trans_tbl);
			$sString =  strtr($sString, $trans_tbl);
			###OLD
			###$umls	= array ('&lt;', '&gt;', '&amp;', '&quot;', '&apos;');
			###$dpls	= array ('<', '>', '&', '', '');
			###$sString = str_replace($umls, $dpls, $sString);
		}
		// spezielle zeichen wandeln
		$sString = str_replace('&apos;', '', $sString); // das wird irgendwie nicht von den vorherigen funcs erfasst...
		$sString = str_replace('-', ' ', $sString);
		// alles andere (ausser buchstaben, zahlen und leerzeichen) rausschmeissen
		$sString = preg_replace("/[^A-Za-z0-9\s]/", '', $sString);
		
		// output
		return $sString;
	}

#------------------------------------------------------------------------------- MIGRATION

/**
* Funktion, die in der kompletten Suchtabelle das neue Feld "searchtext_mod" neu befuellt.
*
* @access   public
* @return	void
*/
	function updateSearchtextModField() {
		// get all ids
		$sql = 'SELECT `id` FROM `'.$this->sTable.'` ORDER BY `id` ASC';
		$aId = $this->oDb->fetch_in_array($sql);
		if (!is_array($aId) || count($aId) == 0) return false; // wenn keine Daten: exit
		
		// alle table-rows updaten
		foreach ($aId as $id) {
			// feld befuellen
			$aData = $this->oDb->fetch_by_id($id, $this->sTable, 'searchtext');
			$aData['searchtext_mod']	= $this->modifySearchString($aData['searchtext']);
			// make update
			$this->oDb->make_update($aData, $this->sTable, $id);
		}
	}

/**
* Funktion, die alle Daten der CMS-Suchtabelle in die neue systemweite Suchtabelle migriert.
*
* @access   public
* @return	void
*/
	function migrateCmsSearchTable() {
		// get all ids (hier noch nicht searchtext zwischenspeichern, da das das PHP-array ggf. sprengen wuerde!)
		$aId = array();
		$sql = 'SELECT `ref_id`,`ref_table`,`lang` 
				FROM `cms_search` 
				ORDER BY `ref_table` ASC, `ref_id` ASC';
		$this->oDb->query($sql);
		while ($aData = $this->oDb->fetch_array()) {
			$aId[] = array(
				'ref_id'	=>$aData['ref_id'],
				'ref_table'	=>$aData['ref_table'],
				'lang'		=>$aData['lang']
			);
		}
		if (count($aId) == 0) return false; // wenn keine Daten: exit
		
		// alle eintrage migrieren
		foreach ($aId as $tmp) {
			// alte werte ermitteln
			$sql = "SELECT `ref_id`,`ref_table`,`searchtext`,`lang` 
					FROM `cms_search` 
					WHERE `ref_id` = '".$tmp['ref_id']."' 
						AND `ref_table` = '".$tmp['ref_table']."'";
			if (!empty($tmp['lang'])) { // addiere ggf. lang zum constraint
				$sql .= " AND `lang` = '".$tmp['lang']."'";
			}
			$this->oDb->query($sql);
			$aDataOld = $this->oDb->fetch_array();
			
			// neuen datensatz befuellen
			$aData['module']			= 'cms';
			$aData['ref_table']			= $aDataOld['ref_table'];
			$aData['ref_id']			= $aDataOld['ref_id'];
			$aData['searchtext']		= addslashes($aDataOld['searchtext']);
			$aTitle						= explode(' || ',$aDataOld['searchtext']);
			$aData['title']				= addslashes($aTitle[0]);
			$aData['searchtext_mod']	= addslashes($this->modifySearchString($aDataOld['searchtext']));
			$aData['lang']				= $aDataOld['lang'];
			$aData['last_mod_by']		= $this->nUserId;
		
			// neuen datensatz speichern
			$this->oDb->make_insert($aData, $this->sTable);
		}
	}
/**
* Funktion, die alle Daten der CMS-Suchtabelle in die neue systemweite Suchtabelle migriert.
*
* @access   public
* @return	void
*/
	function migrateAdrSearchTable() {
		// get all ids (hier noch nicht searchtext zwischenspeichern, da das das PHP-array ggf. sprengen wuerde!)
		$oDb = $this->oDb;
		global $aENV;
		$oDb2 = new dbconnect($aENV['db']);
		$aId = array();
		$sql = 'SELECT `searchtext`,`id` 
				FROM `adr_contact` 
				ORDER BY `id` ASC';
		$this->oDb->query($sql);
		
		while ($aData = $this->oDb->fetch_array()) {
			$aId = array(
				'ref_id'	=>$aData['id'],
				'ref_table'	=>'adr_contact',
				'searchtext'=>addslashes($aData['searchtext']),
				'searchtext_mod'=>$this->modifySearchString($aData['searchtext']),
				'last_mod_by' => $this->nUserId,
				'module' => 'adr'
			);
			$oDb2->make_insert($aId, $this->sTable);
		}
		
	}
#------------------------------------------------------------------------------- DELETE

/**
* ADMIN: Loescht einen Eintrag aus der Search-Tabelle.
* Soll nur eine Sprachversion geloescht werden, muss als dritter Parameter das Sprachkuerzel 
* uebergeben werden, ansonsten werden ALLE Sprachversionen dieses Suchdatensatzes geloescht!
*
* Beispiel:
* <pre><code>
* $oSearch->delete($aData['id'], $sTable);
* </code></pre>
*
* @access   public
* @param	string	$sRefId		Datensatz-ID
* @param	string	$sRefTable	Datensatz-Tabelle
* @param	string	$sLang		optional: Sprachversion
* @return	void
*/
	function delete($sRefId, $sRefTable, $sLang='') {
		// check vars
		if (empty($sRefId))		die('No $sRefId!');
		if (empty($sRefTable))	die('No $sRefTable!');
		
		// set constraint
		$aData2delete['ref_id']		= $sRefId;
		$aData2delete['ref_table']	= $sRefTable;
		if ($this->sModule != '') {
			$aData2delete['module']	= $this->sModule;
		}
		if ($sLang != '') { // ggf. loesche NUR eine sprachversion
			$aData2delete['lang']	= $sLang;
		}
		
		// make delete
		$this->oDb->make_delete($aData2delete, $this->sTable);
		
	}

#-----------------------------------------------------------------------------
} // END of class

?>