<?php
/*
 * Created on 31.01.2006
 *
 */
/**
 * 
 * @version 1.0
 * Diese Klasse holt sich die Werte fuer einen Waehrungsumrechner und speichert diese Informationen in einem Array File ab.
 * @param array $aENV
 */
 	class CurrencyConverter {
		
		var $aRate = null;
		var $aFilestodl = array();
		
		function CurrencyConverter(&$aENV) {
			// Konfigurations CSV File Path
			$this->aFilestodl['eurusd'] = 'http://download.finance.yahoo.com/d/quotes.csv?s=EURUSD=X&f=sl1d1t1ba&e=.csv';
			$this->aFilestodl['eurgbp'] = 'http://download.finance.yahoo.com/d/quotes.csv?s=EURGBP=X&f=sl1d1t1ba&e=.csv';
			$this->aFilestodl['eurchf'] = 'http://download.finance.yahoo.com/d/quotes.csv?s=EURCHF=X&f=sl1d1t1ba&e=.csv';
			$this->aFilestodl['chfusd'] = 'http://download.finance.yahoo.com/d/quotes.csv?s=CHFUSD=X&f=sl1d1t1ba&e=.csv';
			$this->aFilestodl['chfgbp'] = 'http://download.finance.yahoo.com/d/quotes.csv?s=CHFGBP=X&f=sl1d1t1ba&e=.csv';
			$this->aFilestodl['usdgbp'] = 'http://download.finance.yahoo.com/d/quotes.csv?s=USDGBP=X&f=sl1d1t1ba&e=.csv';
			
			$this->initialize($aENV);
			$this->check($aENV);
		}
		/**
		 * Befuellt die internen Arrays
		 * @access private
		 * @param array $aENV
		 * @return void
		 * 
		 */
		function initialize(&$aENV) {
			
			$urilocal = $aENV['path']['sys_data']['unix'].'array.currency.php';
		
			if (is_file($urilocal)) {
				$this->getRates($urilocal);
			}
			else {
				$this->writeArrayFile($urilocal);	
			}
		}
		/**
		 * Prueft ob ein taggenaues Array File vorhanden ist. Wenn nicht, wird eins generiert.
		 * @access public
		 * @param array $aENV
		 * @return void
		 */
		function check($aENV) {
		
			$urilocal = $aENV['path']['sys_data']['unix'].'array.currency.php';
			
			if (is_file($urilocal) 
			&& date("YmdH", filectime($urilocal)) == date("YmdH")) {
				$this->getRates($urilocal);
			}
			else {
				$this->writeArrayFile($urilocal);	
			}
			
		}
		/**
		 * Generiert die Werte fuer das Array. Geht via Socketverbindung auf die angegebenen Resourcen
		 * und sortiert die nicht benoetigten Werte raus.
		 * @access private
		 * @return void
		 * 
		 */
		function getRateFromServer() {
			
			$aAllLines = array();
			foreach($this->aFilestodl as $cur => $path) {
				$aAllLines = array_merge($this->_get_file_via_socket($path),$aAllLines);// fopen zugriff muss erlaubt sein!
			}

			// Durchgehen des Arrays (der eingelesenen Zeilen)
			foreach ($aAllLines as $line_num => $line) {
				// finde richtige Zeile
				$aBuffer = explode(',',str_replace('"','',$line));
				if(count($aBuffer) == 6) {
					$aBuffer[0] = str_replace('=X','',$aBuffer[0]);
					$this->aRate[$aBuffer[0]]['rate'] = $aBuffer[1];
					
					if(strstr($aBuffer[3],'pm')) {
						$aTime = explode(':',$aBuffer[3]);
						$aTime[1] = (int)$aTime[1]+12;	
					}
					$aDate = explode('/',$aBuffer[2]);
					$ts = mktime($aTime[0],$aTime[1],0,$aDate[0],$aDate[1],$aDate[2]);
					$this->aRate[$aBuffer[0]]['ts'] = $ts;
					// Rate umkehren fuer Umkehrrechnung.
					$scur = substr($aBuffer[0],0,3);
					$scur2 = substr($aBuffer[0],3,6);
					$this->aRate[$scur2.$scur]['rate'] = round((1 / $aBuffer[1]),4);
					$this->aRate[$scur2.$scur]['ts'] = $ts;	
				}
			}
		}
		/**
		 * 
		 * Liefert ein File via Socketverbindung.
		 * @access private
		 * @param String $url
		 * @return void
		 */
		function _get_file_via_socket($url) {
			return (is_array($r = file($url))) ? $r : array();
//			$server = str_replace("http://", '', $url);
//			$file = strstr($server, "/");
//			$server = str_replace($file, '', $server);
			
			// check internet connection (da fsockopen() bei fehlender Internet-Verbindung sehr lange braucht und viele Fehler generiert!)
//			if (!$check = @fopen($url, "r")) { return; }#echo "Kein Internet!";
			// get file via socket connection
//			$fp = fsockopen($server, 80, &$errno, &$errstr);
//			$fp = fopen($url, "r");
//			if ($fp) {
//				fputs($fp, "GET $file HTTP/1.0\nHost: $server\n\n");
//				$lines = array();
//				while ($line = fgets($fp, 4096)) {
//					$lines[] = $line;
//				}
//				fclose($fp);
//				return $lines;
//			} else {
//				return array();
//			}
			
		}
		/**
		 * Befuellt das Klasseninterne Array mit den Werten aus dem Array File.
		 * @access private
		 * @param String $urilocal
		 * @return void
		 */
		function getRates($urilocal) {
			$aRate = array();
			require($urilocal); // stellt das array $aRate zur verfuegung
			if(!is_array($aRate)) $aRate = array();
			$this->aRate = $aRate;
		}
		/**
		 * 
		 * Diese Methode generiert ein neues Tag-genaues-Arrayfile.
		 * @access private
		 * @param String $urilocal
		 * @return void
		 */
		function writeArrayFile($urilocal) {
			$oFile =& new filesystem();
			$this->getRateFromServer();
			$insert = '<? // Automatic generated File by Inform - do not change! '."\r\n";
			foreach($this->aRate as $cur => $values) {
				$insert .= '$aRate["'.$cur.'"]["rate"] = '.$values['rate'].';'."\r\n";
				$insert .= '$aRate["'.$cur.'"]["ts"] = '.$values['ts'].';'."\r\n";
			}
			$insert .= '?>';
			$oFile->write_str_in_file($urilocal,$insert);
		}
		/**
		 * Liefert die aktuellen Rates zurueck.
		 * @access public
		 * @return array
		 */
		function getRate() {
			return $this->aRate;	
		}
	}
?>