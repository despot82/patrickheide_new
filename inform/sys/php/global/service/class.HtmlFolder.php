<?php
/**
* Diese Klasse stellt alle noetigen HTML-Elemente zur Verfuegung um eine Verzeichnisstruktur in HTML auszugeben. 
* 
* Example: 
* <pre><code> 
* require_once($aENV['path']['global_service']['unix'].'class.HtmlFolder.php'); 
* $oHtmlFolder =& new HtmlFolder();
* // JavaScript Icon-Preloader
* echo $oHtmlFolder->getJsIconPreloader();
* // Icon (File) + Link
* echo $oHtmlFolder->getIconFile().$oHtmlFolder->getLink($href, $class, $link);
* // Default-Aufklappen
* echo $oHtmlFolder->getJsOpenDefault()
* </code></pre>
*
* @access   public
* @package  service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2006-01-31
*/
class HtmlFolder {

	/*	----------------------------------------------------------------------------
		Funktionen der Klasse HtmlFolder:
		----------------------------------------------------------------------------
		konstruktor HtmlFolder()
		function getLayerStart($id)
		function getLayerEnd()
		function getDivider($class)
		function getSpacer($width)
		function getIconFile()
		function getIconFolder($id)
		function getLink($href, $class, $link)
		function getJsIconPreloader()
		function getJsOpenDefault($highlight_ids)
		----------------------------------------------------------------------------
		HISTORY:
		1.0 / 2006-02-01
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	array	globales Environment-Array
*/
	var $aENV = array();
/**
* @access   private
* @var	 	array	HTML-Elemente
*/
	var $aHtml = array();

#-----------------------------------------------------------------------------

/**
* konstruktor HtmlFolder() -> Initialisiert das Objekt.
*
* @access   public
* @global	array	$aENV	globales Environment-Array
* @return	void
*/
	function HtmlFolder() {
		// Set vars
		global $aENV;
		$this->aENV =& $aENV;
		$this->aHtml = array();
		
		// DIV zum auf- und zuklappen
		$this->aHtml['layer_start']	= '<div id="nLayer_{ID}" style="visibility:visible;display:none">'."\n";
		$this->aHtml['layer_end']	= '</div>'."\n";
		// Trennlinie
		$this->aHtml['divider']		= '<div class="{CLASS}"><img src="'.$this->aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"></div>'."\n";
		// Spacer fuer die Einrueckung
		$this->aHtml['spacer']		= '<img src="'.$this->aENV['path']['pix']['http'].'onepix.gif" width="{WIDTH}" height="1" alt="" border="0">';
		// Icons
		$this->aHtml['icon_file']	= '<img src="'.$this->aENV['path']['pix']['http'].'btn_here.gif" alt="" class="btnsmall">';
		$this->aHtml['icon_folder']	= '<a href="javascript:subnaviSwitch(\'{ID}\')"><img src="'.$this->aENV['path']['pix']['http'].'btn_show.gif" name="nImg_{ID}" alt="" class="btnsmall"></a>';
		// Link zur Edit-Seite
		$this->aHtml['link']		= '<a href="{HREF}" class="{CLASS}">{LINK}</a><br>'."\n";
		// JavaScript Icon-Preloader-Block
		$this->aHtml['js_preloader'] = '<script language="JavaScript" type="text/javascript">'."\n";
		$this->aHtml['js_preloader'] .= '// Vorlader fuer Toggle-Icon-tausch'."\n";
		$this->aHtml['js_preloader'] .= 'var naviIcons = new Array();'."\n";
		$this->aHtml['js_preloader'] .= 'naviIcons["hide"] = new Image();'."\n";
		$this->aHtml['js_preloader'] .= 'naviIcons["hide"].src = "'.$this->aENV['path']['pix']['http'].'btn_hide.gif";'."\n";
		$this->aHtml['js_preloader'] .= 'naviIcons["show"] = new Image();'."\n";
		$this->aHtml['js_preloader'] .= 'naviIcons["show"].src = "'.$this->aENV['path']['pix']['http'].'btn_show.gif";'."\n";
		$this->aHtml['js_preloader'] .= '</script>'."\n";
		// JavaScript js_open-default-Block
		$this->aHtml['js_opendefault'] = '<script language="JavaScript" type="text/javascript">';
		$this->aHtml['js_opendefault'] .= 'subnaviSwitch("{HIGHLIGHT_IDS}");';
		$this->aHtml['js_opendefault'] .= '</script>';
	}

#-----------------------------------------------------------------------------

/**
* gibt den oeffnenden DIV-Tag zurueck.
* @access   public
* @param	string	$id			Layer-Id (= Datensatz-Id)
* @return	string	HTML
*/
	function getLayerStart($id) {
		return str_replace('{ID}', $id, $this->aHtml['layer_start']);
	}

/**
* gibt den schliessenden DIV-Tag zurueck.
* @access   public
* @return	string	HTML
*/
	function getLayerEnd() {
		return $this->aHtml['layer_end'];
	}

/**
* gibt den Divider zurueck.
* @access   public
* @param	string	$class		CSS-Klasse fuer den DIV
* @return	string	HTML
*/
	function getDivider($class) {
		return str_replace('{CLASS}', $class, $this->aHtml['divider']);
	}

/**
* gibt den Spacer fuer die Einrueckung zurueck.
* @access   public
* @param	string	$width		Width-Attribut in Pixel
* @return	string	HTML
*/
	function getSpacer($width) {
		return str_replace('{WIDTH}', $width, $this->aHtml['spacer']);
	}

/**
* gibt das Icon fuer ein File zurueck.
* @access   public
* @return	string	HTML
*/
	function getIconFile() {
		return $this->aHtml['icon_file'];
	}

/**
* gibt das Icon fuer einen Folder zurueck.
* @access   public
* @param	string	$id			Layer-Id (= Datensatz-Id)
* @return	string	HTML
*/
	function getIconFolder($id) {
		return str_replace('{ID}', $id, $this->aHtml['icon_folder']);
	}

/**
* gibt den verlinkten Foldernamen zurueck (ohne Einrueckung und ohne Icon).
* @access   public
* @param	string	$href			Anker-Tag-Attribut "href" (inlusive aller Parameter, insbesondere der ID!)
* @param	string	$class			Anker-Tag-Attribut "class" (CSS-Klasse)
* @param	string	$link			Anker-Text (Foldername)
* @return	string	HTML
*/
	function getLink($href, $class, $link) {
		return str_replace(
			array('{HREF}', '{CLASS}', '{LINK}'), 
			array($href, $class, $link), 
			$this->aHtml['link']
		);
	}

/**
* gibt den kompletten JavaScript-Block zum Vorladen der Icons zurueck.
* @access   public
* @return	string	HTML
*/
	function getJsIconPreloader() {
		return $this->aHtml['js_preloader'];
	}

/**
* gibt den kompletten JavaScript-Block zum defaultmaessigen Highlighten der/des aktuellen Folders beim Laden der Seite zurueck.
* @access   public
* @param	mixed	$highlight_ids		Datensatz-Id(s), mehrere als kommagetrennter string oder als array
* @return	string	HTML
*/
	function getJsOpenDefault($highlight_ids) {
		if (is_array($highlight_ids)) $highlight_ids = implode(',', $highlight_ids);
		return str_replace('{HIGHLIGHT_IDS}', $highlight_ids, $this->aHtml['js_opendefault']);
	}

#-----------------------------------------------------------------------------

} // END class
?>