<?php // Class DatetimeHoliday
/**
* This class extends class datetime and provides date-functions for holidays.
*
*
* Example:
* <pre><code>
* // init
* $oHoliday = new DatetimeHoliday(2001);
* echo $oHoliday->getEaster();
* </code></pre>
*
* @access   public
* @package  service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2006-05-01	[NEU]
*/

class DatetimeHoliday extends da_DateTime {

#----------------------------------------------------------------------------- CLASS VARS

/**
* @access   private
* @var	 	int		Aktuelles Jahr (4-stellig)
*/
	var $nYear = 0;
/**
* @access   private
* @var	 	int		Ostern als Unix-timestamp
*/
	var $nEasterTimestamp = 0;
/**
* @access   private
* @var	 	boolean	Rueckgabewert der Methoden als Unix-Timestamp oder ISO-Date (default: ISO-Date)
*/
	var $bReturnIsodate = true;

#----------------------------------------------------------------------------- MAIN

/**
* Constructor. If no date is provided, the current year will be used
*
* @access   public
* @param	string		$year			optional: aktuell zu verwendendes Jahr (default: aktuelles Jahr)
* @param	boolean		$bReturnIsodate	optional: Rueckgabewert als Unix-Timestamp oder ISO-Date (default: ISO-Date)
*/
	function DatetimeHoliday($year=0, $bReturnIsodate=true) {
		if ($year == 0) $year = date("Y");
		$this->nYear = $year;
		$this->nEasterTimestamp = easter_date($year);
		$this->bReturnIsodate = $bReturnIsodate;
	}

/**
* Hilfsmethode: Returns the number of seconds for a number of days
*
* @access   private
* @param	int		$days	Anzahl Tage, die in Sekunden umgewandelt werden sollen.
* @return	int		Anzahl Tage in Sekunden.
*/
	function _daysToSec($days)    {
		return $days * 86400;
	}

/**
* Setzt das Rueckgabeformat auf Unix-Timestamp oder ISO-Date (default: ISO-Date).
*
* @access   public
* @param	boolean	[true | false]
* @return	void
*/
	function setReturnIsodate($bIsodate=true) {
		$this->bReturnIsodate = $bIsodate;
	}

//---------------------------------------------------------------------------- VON OSTERN ABHAENGIG

/**
* Gibt das Datum auf das Ostern (en: Easter, fr: Pâques) faellt zurueck.
*
* @access   public
* @param	string	$year	Jahreszahl (4stellig)
* @return	string	Oster-Datum im ISO-Format
*/
	function getEaster() {
		return ($this->bReturnIsodate) ? date("Y-m-d", $this->nEasterTimestamp) : $this->nEasterTimestamp;
	}

/**
* Gibt Aschermittwoch (en: Ash, fr: Cendres) zurueck.
* Note: Dieser Feiertag ist immer genau 45 Tage VOR Ostern. 
*
* @access   public
* @return	string	Datum im ISO-Format (default) oder Unix-Timestamp
*/
	function getAsh() {
		$ts = $this->nEasterTimestamp - $this->_daysToSec(45);
		return ($this->bReturnIsodate) ? date("Y-m-d", $ts) : $ts;
	}

/**
* Gibt ??? (Fasten...? -> KEINE Uebersetztung gefunden!!!) (en: Lent, fr: Carême) zurueck.
* Note: Dieser Feiertag ist immer genau 41 Tage VOR Ostern. 
*
* @access   public
* @return	string	Datum im ISO-Format (default) oder Unix-Timestamp
*/
	function getLent() {
		$ts = $this->nEasterTimestamp - $this->_daysToSec(41);
		return ($this->bReturnIsodate) ? date("Y-m-d", $ts) : $ts;
	}

/**
* Gibt Christi Himmelfahrt (en: Ascension Day, fr: Ascension) zurueck.
* Note: Dieser Feiertag ist immer genau 39 Tage NACH Ostern (Wikipedia: 39 Tage nach Ostern). 
*
* @access   public
* @return	string	Datum im ISO-Format (default) oder Unix-Timestamp
*/
	function getAscensionDay() {
		$ts = $this->nEasterTimestamp + $this->_daysToSec(39);
		return ($this->bReturnIsodate) ? date("Y-m-d", $ts) : $ts;
	}

/**
* Gibt Pfingsten (Pfingst-Sonntag) (en: Whitsun, fr: Pentecôte) zurueck.
* Note: Dieser Feiertag ist immer genau 49 Tage NACH Ostern (Wikipedia: 50. Tag nach Ostern, 10. Tag nach Christi Himmelfahrt).
*
* @access   public
* @return	string	Datum im ISO-Format (default) oder Unix-Timestamp
*/
	function getWhitsun() {
		$ts = $this->nEasterTimestamp + $this->_daysToSec(49);
		return ($this->bReturnIsodate) ? date("Y-m-d", $ts) : $ts;
	}

/**
* Gibt Dreifaltigkeitsfest (auch Dreieinigkeitsfest) (en: Trinity, fr: Trinité) zurueck.
* Note: Dieser Feiertag ist immer genau 56 Tage NACH Ostern (Wikipedia: am ersten Sonntag nach Pfingsten).
*
* @access   public
* @return	string	Datum im ISO-Format (default) oder Unix-Timestamp
*/
	function getTrinity() {
		$ts = $this->nEasterTimestamp + $this->_daysToSec(56);
		return ($this->bReturnIsodate) ? date("Y-m-d", $ts) : $ts;
	}

/**
* Gibt Fronleichnam (en: Corpus Christi, fr: Fête-Dieu) zurueck.
* Note: Dieser Feiertag ist immer genau 63 Tage NACH Ostern (Wikipedia: 60. Tag nach Ostern, am zweiten Donnerstag nach Pfingsten).
*
* @access   public
* @return	string	Datum im ISO-Format (default) oder Unix-Timestamp
*/
	function getCorpusChristi() {
		$ts = $this->nEasterTimestamp + $this->_daysToSec(60);
		return ($this->bReturnIsodate) ? date("Y-m-d", $ts) : $ts;
	}

#----------------------------------------------------------------------------- FESTES DATUM

/**
* Gibt Heiligabend (en: Christmas Eve, fr: 	réveillon / nuit de Noël / veille de Noël) zurueck.
* Note: Dieser Feiertag ist immer genau am 24. Dezember.
*
* @access   public
* @return	string	Datum im ISO-Format (default) oder Unix-Timestamp
*/
	function getChristmasEve() {
		return ($this->bReturnIsodate) ? $this->nYear."-12-24" : mktime(0, 0, 0, 12, 24, $this->nYear);
	}

#-----------------------------------------------------------------------------
} // END of class
?>