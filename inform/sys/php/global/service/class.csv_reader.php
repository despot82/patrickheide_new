<?php // Class csv_reader
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung ein CSV-File auszulesen. 
*
* Example: 
* <pre><code> 
* $oCsv =& new csv_reader("relativer/oder_abs_unix/pfad/zum/csvfile.txt"); // params: $sCsvFile[,$aConfig=NULL]
* $oCsv->setCsvFields($aCsvField); // params: $aCsvField
* $aTitle = $oCsv->openFile(); // params: [$bReadFirstline=true]
* while ($aCsvLine = $oCsv->getLine()) { // params: [$bPrepareData=true]
* 	#dosomething...
* }
* $oCsv->closeFile(); // params: -
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-09-06
*/
class csv_reader {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse csv_reader:
		----------------------------------------------------------------------------
		konstruktor csv_reader($sCsvFile='', $aConfig=NULL) // params: $sCsvFile[,$aConfig=NULL]
		function setCsvFields($aCsvField); // params: $aCsvField
		function openFile(); // params: [$bReadFirstline=true]
		function getLine(); // params: [$bPrepareData=true]
		function prepareCsvLine(&$aCsvLine)
		function closeFile() // params: -
		----------------------------------------------------------------------------
		HISTORY:
		1.0 / 2005-09-06
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	int		FilePointer
*/
	var $fp;
/**
* @access   public
* @var	 	string	Dateiname des CSV-Files
*/
	var $sCsvFile;
/**
* @access   private
* @var	 	string	Trennzeichen, welches die Felder des CSV-Felder voneinander trennt
*/
	var $sFieldDivider;
/**
* @access   private
* @var	 	string	Zeichen, welches die Values einschliesst
*/
	var $sEnclosureChar;
/**
* @access   private
* @var	 	string	Trennzeichen, welches die Zeilen des CSV-Files voneinander trennt
*/
	var $sLineDivider;
/**
* @access   private
* @var	 	array	Values der ersten Zeile im CSV-File
*/
	var $aCsvField;
/**
* @access   private
* @var	 	array	Array-Keys f.d. CSV-File Values
*/
	var $aCsvFieldKey;
/**
* @access   private
* @var	 	array	Auf welcher Position befindet sich welches Feld
*/
	var $aPos;
/**
* @access   private
* @var	 	int		Anzahl Felder im Position-/Feld-Relation-Array
*/
	var $nCountPos;
/**
* @access   public
* @var	 	boolean	Debug-Mode
*/
	var $bDebugMode;

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das archive-Objekt und setzt den (Pfad und) Dateinamen des Archives "$sArchiveFile" (relativ!) 
*
* Beispiel: 
* <pre><code> 
* $oCsv =& new csv_reader("relativer/oder_abs_unix/pfad/zum/csvfile.txt"); // params: $sCsvFile[,$aConfig=NULL]
* </code></pre>
*
* @access   public
* @param 	string	$sCsvFile	Dateiname des CSV-Files
* @param 	array	$aConfig	Konfigurations-Vars in einem assioziativen Array
* @return   void
*/
	function csv_reader($sCsvFile='', $aConfig=NULL) {
		if ($sCsvFile == '' || !file_exists($sCsvFile)) {
			die('ERROR: $sCsvFile does not exist!');#return;
		}
		$this->sCsvFile		= $sCsvFile;
		$this->sFieldDivider	= (is_array($aConfig) && isset($aConfig['field_divider'])) ? $aConfig['field_divider'] : ';';
		$this->sEnclosureChar	= (is_array($aConfig) && isset($aConfig['enclosure_char'])) ? $aConfig['enclosure_char'] : '"';
		#$this->sLineDivider		= (is_array($aConfig) && isset($aConfig['line_divider'])) ? $aConfig['line_divider'] : '\n';
		// set defaults
		$this->fp			= 0;
		$this->aCsvField	= array();
		$this->aCsvFieldKey	= array();
		$this->aPos			= array();
		$this->nCountPos	= 0;
		$this->bDebugMode	= false;
	}

#-----------------------------------------------------------------------------

/**
* bestimmt die Felder, die im CSV-File erwartet werden (sofern im CSV-File i.d. ersten Zeile ebensolche Namen sind). 
* (Werden die Felder mit nichtnumerischen (also assioziativen) Keys uebergeben, werden diese erkannt und mitgespeichert.)
*
* Beispiel: 
* <pre><code> 
* $aCsvField = array('ID', 'Name', 'First Name');
* // ODER assioziativ:
* $aCsvField = array('id'=>'ID', 'surname'=>'Name', 'firstname'=>'First Name');
* $oCsv->setCsvFields($aCsvField); // params: $aCsvField
* </code></pre>
*
* @access   public
* @param	array	$aCsvField	Bezeichnungen der Felder im CSV-File
* @return   void
*/
	function setCsvFields($aCsvField) {
		if (!is_array($aCsvField)) return; // check vars
		if (array_keys($aCsvField) !== range(0, count($aCsvField) - 1)) {
			// array = assioziativ: Keys sind gleich dabei!
			$this->_setCsvFieldKeys(array_keys($aCsvField));
			$this->aCsvField	= array_values($aCsvField);
		} else {
			$this->aCsvField	= $aCsvField;
		}
	}

/**
* bestimmt die assioziativen Keys der Felder, die die Values des CSV-Files bekommen sollen. 
* Dies dient dem einfacheren Ansprechen eines CSV-File Values.
* (NOTE: MUSS die gleiche Reihenfolge und Anzahl des Arrays "$aCsvField" haben.)
*
* @access   private
* @param	array	$aCsvFieldKey	Array-Keys der Felder im CSV-File
* @return   void
* @see		setCsvFields()
*/
	function _setCsvFieldKeys($aCsvFieldKey) {
		if (!is_array($aCsvFieldKey)) return; // check vars
		$this->aCsvFieldKey = $aCsvFieldKey;
	}

/**
* Oeffnet das File. Liest ggf. die erste Zeile aus + gibt sie als Array zurueck
* + stellt Vergleiche bezueglich der Position jeder Spalte an.
*
* Beispiel: 
* <pre><code> 
* $aTitle = $oCsv->openFile(); // params: [$bReadFirstline=true]
* </code></pre>
*
* @access   public
* @param 	boolean	$bReadFirstline		Erste Zeile der CSV-Datei auslesen und auswerten
* @return   array	$aFirstLine			Erste Zeile der CSV-Datei als Array
*/
	function openFile($bReadFirstline=true) {
		// CSV-Datei oeffnen und locken
		#$fsize		= filesize($this->sCsvFile) + 1;
		$this->fp	= fopen($this->sCsvFile, 'r');
		if (!$this->fp) die("Kann CSV-Datei nicht oeffnen.");
		if (!flock($this->fp, LOCK_SH | LOCK_NB)) die("Kann CSV-Datei nicht locken.");
		
		if ($bReadFirstline == true) {
			// Erste Zeile der CSV-Datei auslesen
			$aFirstLine			= fgetcsv($this->fp, 4096, $this->sFieldDivider, $this->sEnclosureChar); // sEnclosureChar ab PHP 4.3.0!
			$nCountFirstLine	= count($aFirstLine);
			// zum Vergleichen saeubern + lowercase
			$aHeadline			= array();
			for ($j = 0; $j < $nCountFirstLine; $j ++) {
				###$aFirstLine[$j] = preg_replace("/[^A-Za-z0-9;]/", '', $aFirstLine[$j]); // Nur Buchstaben und ";" behalten
				### ggf. TODO enclosure fuer aeltere PHP-Versionen bauen...
				$aHeadline[$j] = strtolower(trim($aFirstLine[$j])); // lowercase!
			}
			
			// Das so gewonnene Array mit dem Array $aCsvField vergleichen 
			// und erkennen, auf welcher Position der jeweilige Eintrag ist
			$nCountCsvField		= count($this->aCsvField);
			if ($nCountCsvField > 0) {
				for ($i = 0; $i < $nCountCsvField; $i ++) {
					for ($j = 0; $j < $nCountFirstLine; $j ++) {
						if (strtolower($this->aCsvField[$i]) == $aHeadline[$j]) { // lowercase!
							$this->aPos[$i] = $j;
						}
					}
				}
			} else {
				for ($j = 0; $j < $nCountFirstLine; $j ++) {
					$this->aPos[$j] = $j; // Fake-Array...
				}
			}
			// gefundene Matches zaehlen
			$this->nCountPos = count($this->aPos);
		}
		// output
		return $aFirstLine;
	}

/**
* Oeffnet das File und liest die erste Zeile aus (und setzt den Dateizeiger um eins weiter).
*
* Beispiel: 
* <pre><code> 
* while ($aCsvLine = $oCsv->getLine()) { // params: [$bPrepareData=true]
* 	#dosomething...
* }
* </code></pre>
*
* @access   public
* @param 	boolean	$bReadFirstline		Erste Zeile der CSV-Datei auslesen und auswerten
* @return   void
*/
	function getLine($bPrepareData=true) {
		// Zeile der CSV-Datei auslesen
		$aCsvLine = fgetcsv($this->fp, 4096, $this->sFieldDivider, $this->sEnclosureChar); // sEnclosureChar ab PHP 4.3.0!
		if ($aCsvLine == false) {
			return false;
		}
		// ggf. bearbeiten
		if ($bPrepareData == true) {
			$aCsvLine = $this->prepareCsvLine($aCsvLine);
		}
		// output
		return $aCsvLine;
	}

/**
* bereitet die aktuelle Zeile des CSV-Files auf und macht ein assioziatives array daraus.
*
* @access   public
* @param	(array) $aLine
* @global	(int) 	$nCountPos
* @global	(array)	$aPos
* @global	(array)	$aDbField
* @return	(array)	$aCsvLine
*/
	function prepareCsvLine(&$aCsvLine) {
		// aufbereitung + assioziative keys
		$buffer = array();
		for ($i = 0; $i <$this->nCountPos; $i ++) {
			if (count($this->aCsvFieldKey) > 0) {
				// wenn "setCsvFields()" ein assioziatives array uebergeben wurde
				$buffer[$this->aCsvFieldKey[$i]]	= addslashes(trim($aCsvLine[$this->aPos[$i]]));
			} else {
				// wenn "setCsvFields()" ein numerisches Array uebergeben wurde
				// oder wenn "setCsvFields()" gar nicht aufgerufen wurde
				$buffer[$i]							= addslashes(trim($aCsvLine[$this->aPos[$i]]));
			}
		}
		// output
		return $buffer;
	}

/**
* Schliesst den geoeffneten Dateizeiger.
*
* @access   public
* @return   boolean	(Gibt bei Erfolg TRUE zurueck, im Fehlerfall FALSE)
*/
	function closeFile() {
		return fclose($this->fp);
	}

#-----------------------------------------------------------------------------
} // END of class

?>