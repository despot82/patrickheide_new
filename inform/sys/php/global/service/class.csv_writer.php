<?php // Class csv_writer
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung ein CSV-File zu schreiben. 
*
* Example: 
* <pre><code> 
* $oCsv =& new csv_writer("relativer/oder_abs_unix/pfad/zum/csvfile.txt"); // params: $sCsvFile[,$aConfig=NULL]
* $oCsv->setCsvFields($aCsvField); // params: $aCsvField
* $oCsv->openFile(); // params: [$bWriteFirstline=true]
* while ($aCsvLine = $oDb->fetch_array()) {
* 	$oCsv->addLine($aCsvLine); // params: $aCsvLine
* }
* $oCsv->closeFile(); // params: -
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-09-06
*/
class csv_writer {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse csv_writer:
		----------------------------------------------------------------------------
		konstruktor csv_writer($sCsvFile='', $aConfig=NULL) // params: $sCsvFile[,$aConfig=NULL]
		function setCsvFields($aCsvField); // params: $aCsvField
		function openFile(); // params: [$bWriteFirstline=true]
		function addLine($aCsvLine); // params: $aCsvLine
		function closeFile() // params: -
		----------------------------------------------------------------------------
		HISTORY:
		1.0 / 2005-09-06
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	int		FilePointer
*/
	var $fp;
/**
* @access   public
* @var	 	string	Dateiname des CSV-Files
*/
	var $sCsvFile;
/**
* @access   private
* @var	 	string	Trennzeichen, welches die Felder des CSV-Felder voneinander trennt
*/
	var $sFieldDivider;
/**
* @access   private
* @var	 	string	Zeichen, welches die Values einschliesst
*/
	var $sEnclosureChar;
/**
* @access   private
* @var	 	string	Trennzeichen, welches die Zeilen des CSV-Files voneinander trennt ("\r\n"=Win)
*/
	var $sLineDivider;
/**
* @access   private
* @var	 	array	Values der ersten Zeile im CSV-File als numerisches Array
*/
	var $aCsvField;
/**
* @access   private
* @var	 	array	Array-Keys f.d. CSV-File Values
*/
	var $aCsvFieldKey;
/**
* @access   private
* @var	 	int		Anzahl Felder im Position-/Feld-Relation-Array
*/
	var $nCountFields;
/**
* @access   public
* @var	 	boolean	Debug-Mode
*/
	var $bDebugMode;

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das archive-Objekt und setzt den (Pfad und) Dateinamen des Archives "$sArchiveFile" (relativ!) 
*
* Beispiel: 
* <pre><code> 
* $oCsv =& new csv_writer("relativer/oder_abs_unix/pfad/zum/csvfile.txt"); // params: $sCsvFile[,$aConfig=NULL]
* </code></pre>
*
* @access   public
* @param 	string	$sCsvFile	Dateiname des CSV-Files
* @param 	array	$aConfig	Konfigurations-Vars in einem assioziativen Array
* @return   void
*/
	function csv_writer($sCsvFile='', $aConfig=NULL) {
		#if ($sCsvFile == '') return;
		$this->sCsvFile		= $sCsvFile;
		$this->sFieldDivider	= (is_array($aConfig) && isset($aConfig['field_divider'])) ? $aConfig['field_divider'] : ';';
		$this->sEnclosureChar	= (is_array($aConfig) && isset($aConfig['enclosure_char'])) ? $aConfig['enclosure_char'] : '"';
		$this->sLineDivider		= (is_array($aConfig) && isset($aConfig['line_divider'])) ? $aConfig['line_divider'] : "\r\n";
		// set defaults
		$this->fp			= 0;
		$this->aCsvField	= array();
		$this->aCsvFieldKey	= array();
		$this->nCountFields	= 0;
		$this->bDebugMode	= false;
	}

#-----------------------------------------------------------------------------

/**
* bestimmt die Felder, die im CSV-File erwartet werden (sofern im CSV-File i.d. ersten Zeile ebensolche Namen sind). 
* (Werden die Felder mit nichtnumerischen (also assioziativen) Keys uebergeben, werden diese erkannt und mitgespeichert.)
*
* Beispiel: 
* <pre><code> 
* $aCsvField = array('ID', 'Name', 'First Name');
* // ODER assioziativ:
* $aCsvField = array('id'=>'ID', 'surname'=>'Name', 'firstname'=>'First Name');
* $oCsv->setCsvFields($aCsvField); // params: $aCsvField
* </code></pre>
*
* @access   public
* @param	array	$aCsvField	Bezeichnungen der Felder im CSV-File
* @return   void
*/
	function setCsvFields($aCsvField) {
		if (!is_array($aCsvField)) return; // check vars
		if (array_keys($aCsvField) !== range(0, count($aCsvField) - 1)) {
			// array = assioziativ: Keys sind gleich dabei!
			$this->_setCsvFieldKeys(array_keys($aCsvField));
			$this->aCsvField	= array_values($aCsvField);
		} else {
			$this->aCsvField	= $aCsvField;
			$this->nCountFields	= count($aCsvField);
		}
	}

/**
* Hilfsfunktion fuer "setCsvFields()": bestimmt die assioziativen Keys der Felder, die die Values des CSV-Files bekommen sollen. 
* Dies dient dem einfacheren Ansprechen eines CSV-File Values.
* (NOTE: MUSS die gleiche Reihenfolge und Anzahl des Arrays "$aCsvField" haben.)
*
* @access   private
* @param	array	$aCsvFieldKey	Array-Keys der Felder im CSV-File
* @return   void
* @see		setCsvFields()
*/
	function _setCsvFieldKeys($aCsvFieldKey) {
		if (!is_array($aCsvFieldKey)) return; // check vars
		$this->aCsvFieldKey = $aCsvFieldKey;
		$this->nCountFields	= count($aCsvFieldKey);
	}

/**
* Oeffnet das File. Liest ggf. die erste Zeile aus + gibt sie als Array zurueck
* + stellt Vergleiche bezueglich der Position jeder Spalte an.
*
* Beispiel: 
* <pre><code> 
* $oCsv->openFile(); // params: [$bWriteFirstline=true]
* </code></pre>
*
* @access   public
* @param 	boolean	$bWriteFirstline	Erste Zeile der CSV-Datei schreiben
* @return   void
*/
	function openFile($bWriteFirstline=true) {
		// CSV-Datei oeffnen und locken
		#$fsize		= filesize($this->sCsvFile) + 1;
		$this->fp	= fopen($this->sCsvFile, 'w+');
		if (!$this->fp) die("Kann CSV-Datei '".$this->sCsvFile."' nicht oeffnen.");
		#if (!flock($this->fp, LOCK_SH | LOCK_NB)) die("Kann CSV-Datei nicht locken.");
		
		if ($bWriteFirstline == true && count($this->aCsvField)) {
			// Erste Zeile der CSV-Datei einfuegen
			$this->addLine($this->aCsvField, true);
		}
	}

/**
* Schreibt das uebergebene Daten-Array als Zeile an das Ende des CSV-Files.
*
* Beispiel: 
* <pre><code> 
* $oCsv->addLine($aCsvLine); // params: $aCsvLine
* </code></pre>
*
* @access   public
* @param 	array	$aCsvLine		Zeile der CSV-Datei als Array
* @return   boolean	(Bei Erfolg TRUE, sonst FALSE)
*/
	function addLine($aCsvLine, $bIsFirstline=false) {
		if (!is_array($aCsvLine)) return false;
		// als Zeile i.d. CSV-Datei einfuegen
		return (fwrite($this->fp, $this->_prepareLine($aCsvLine, $bIsFirstline)) === false) ? false : true;
	}

/**
* Hilfsfunktion fuer "addLine()": bereitet das Array der aktuellen Zeile auf und macht einen string daraus.
*
* @access   private
* @param	(array)		$aLine
* @return	(string)	$sCsvLine
* @see		addLine()
*/
	function _prepareLine(&$aCsvLine, $bIsFirstline=false) {
		// aufbereitung
		$buffer = array();
		if ($this->nCountFields == 0) {
			$this->nCountFields = count($aCsvLine);
		}
		for ($i = 0; $i < $this->nCountFields; $i ++) {
			if (count($this->aCsvFieldKey) == 0 || $bIsFirstline == true) {
				// wenn "setCsvFields()" ein numerisches Array uebergeben wurde
				// oder wenn "setCsvFields()" gar nicht aufgerufen wurde
				// oder wenn es sich um die Spaltentitel handelt
				$buffer[$i] = $this->_prepareField($aCsvLine[$i]);
			} else {
				// wenn "setCsvFields()" ein assioziatives array uebergeben wurde
				$buffer[$this->aCsvFieldKey[$i]] = $this->_prepareField($aCsvLine[$this->aCsvFieldKey[$i]]);
			}
		}
		// output
		return implode($this->sFieldDivider, $buffer) . $this->sLineDivider;
	}

/**
* Hilfsfunktion fuer "_prepareLine()": bereitet das aktuelle Feld auf.
*
* @access   private
* @param	(string)	$sField
* @return	(string)	$sField
* @see		_prepareLine()
*/
	function _prepareField(&$sField) {
		// aufbereitung
		$sField = addslashes(trim($sField));
		// ggf. in Anfuehrungszeichen einschliessen
		if (!empty($this->sEnclosureChar) && !empty($sField)) {
			$sField = $this->sEnclosureChar . $sField . $this->sEnclosureChar;
		}
		// output
		return $sField;
	}

/**
* Schliesst den geoeffneten Dateizeiger.
*
* @access   public
* @return   boolean	(Gibt bei Erfolg TRUE zurueck, im Fehlerfall FALSE)
*/
	function closeFile() {
		return fclose($this->fp);
	}

#-----------------------------------------------------------------------------
} // END of class

?>