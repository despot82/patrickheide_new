<?php // Class session
/**
* Diese Klasse abstrahiert die in PHP4 eingebauten Session-Funktionen. 
*
* Example: 
* <pre><code>
* // init 
* $oSess =& new session("CLIENTNAME");  // params: $sName 
* // get var from superglobal array "$_SESSION" 
* echo $oSess->get_name(); 
* // set var in superglobal array "$_SESSION" 
* $oSess->set_var('test', 'hallo'); // params: $sName,$value 
* // get var from superglobal array "$_SESSION" or default if no var is in session 
* echo $oSess->get_var('test', 'hi'); // params: $sName[,$default=null] 
* // prints out complete $_SESSION array for DEBUGGING 
* $oSess->debug(); 
* // unset var in superglobal array "$_SESSION" 
* $oSess->unset_var('test'); // params: $sName 
* // unset all vars in superglobal array "$_SESSION" 
* $oSess->unset_all();
* // destroy komplete session (i.e. for "logout") 
* $oSess->destroy();
* </code></pre>
* 
* @access   public
* @package  Content
* @see		http://de.php.net/manual/de/ref.session.php
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2004-11-25 [NEU: "is_registered()"]
*/

class session {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse session:
		----------------------------------------------------------------------------
		konstruktor session($sSessionName) // params: [$sSessionName='PhpSessID']
		function get_name()
		function is_registered($sName) // params: $sName
		function &get_var($sName, $default = null) // params: $sName[,$default=null]
		function set_var($sName, $value) // params: $sName, $value
		function unset_var($sName) // params: $sName
		function unset_all()
		function destroy()
		function debug()
		----------------------------------------------------------------------------
		HISTORY:
		1.1 / 2004-11-25 [NEU: "is_registered()"]
		1.0 / 2004-04-23
	*/

/**
* konstruktor session() -> Initialisiert das session-Objekt. 
* Initialisiert die Session und setzt ggf. einen eigenen Session-Namen 
*
* @access   public
* @param	string	$sSessionName	Session-Name (optional - default: 'SessionID')
* @return	void
* @see		session_name()
* @see		session_start()
*/
	function session($sSessionName = 'PhpSessID') {
		// Disable auto-start
		ini_set('session.auto_start', 0);
		// Set name
		session_name($sSessionName);
		// Start session
		@session_start();
	}

#-----------------------------------------------------------------------------

/**
* Returns name of current session. 
*
* @static
* @access	public
* @return	string	Name of session
* @see		session_name()
*/
	function get_name() {
		return session_name();
	}

/**
* returns if session variable is already registered. 
*
* @access	public
* @param	string	$sName  Name of variable
* @return	boolean	true if var is already registered, else false
*/
	function is_registered($sName) {
		return (isset($_SESSION[$sName])) ? true : false;
	}

/**
* Returns session variable. 
*
* @static
* @access	public
* @param	string	$sName		Name of a variable
* @param	mixed	$default	Default value of a variable if not set
* @return	mixed	Value of a variable
*/
	function &get_var($sName, $default = null) {
		if (!isset($_SESSION[$sName]) && isset($default)) {
			$_SESSION[$sName] = $default;
		}
		return $_SESSION[$sName];
	}

/**
* Sets session variable. (Unsets, if 2. param = null!) 
*
* @access	public
* @param	string	$sName  Name of  variable
* @param	mixed	$value Value of variable
* @return	mixed	Old value of variable
*/
	function set_var($sName, $value) {
		$buffer = @$_SESSION[$sName];
		if (null === $value) {
			unset($_SESSION[$sName]);
		} else {
			$_SESSION[$sName] = $value;
		}
		return $buffer;
	}

/**
* Unsets session variable. (Wrapper for "set_var($sName, null)"...) 
*
* @access	public
* @param	string	$sName  Name of variable
* @return	mixed	Old value of variable
*/
	function unset_var($sName) {
		$buffer = @$_SESSION[$sName];
		$this->set_var($sName, null);
		return $buffer;
	}

/**
* Unsets all session variables.
*
* @access	public
* @return	void
*/
	function unset_all() {
		session_unset();
	}

/**
* Frees all session variables and destroys all data registered to this session. 
*
* This method resets the $_SESSION variable and destroys all of the data associated 
* with the current session in its storage (file or DB). It forces new session to be 
* started after this method is called. It does not unset the session cookie.
*
* @static
* @access	public
* @return	void
* @see		session_unset()
* @see		session_destroy()
*/
	function destroy() {
		session_destroy();
	}

#-----------------------------------------------------------------------------

/**
* DEGUG: show complete $_SESSION array. 
*
* @access	public
* @return	HTML
*/
	function debug() {
		print_r($_SESSION);
	}

#-----------------------------------------------------------------------------
} // END of class
?>