<?php // Class gallery
/**
* Diese Klasse stellt die gaengigen Elemente einer Bilder-Gallerie (mit oder ohne Overview-Seite) zur Verfuegung.
*
* Beim initialisieren der Klasse kann man ein Verzeichnis angeben, in dem die Bilder liegen. Liegt in diesem Verzeichnis auch 
* eine CSV-Datei im Format "filename;comment\n", dann werden nur die Bilder beruecksichtigt, die in der Datei angegeben sind 
* (Zusaetzlich zu der Moeglichkeit nur bestimmte Bilder zu zeigen, kann man hier noch Kommentare den Bildern zuordnen). 
* Ansonsten wird das gesamte Verzeichnis nach Bildern [gif|jpg|png] durchsucht. 
* Wenn man als ersten Parameter einen LEEREN String uebergibt, werden zunaechst keine Bilder geladen. Man hat dadurch die 
* Moeglichkeit das Klassen-Image-Array mir eigenen Bildern nachzuladen.
*
* Example Overview-page: 
* <pre><code> 
* // init 
* require_once($aENV['path']['global_service']['unix']."class.gallery.php"); 
* $oGallery =& new gallery("thumbs/"); // params: [$pixdir='/'][,$csvfile='gallery.csv']
* // modifiziere oeffnenden table-tag 
* $oGallery->set_table_tag('< table width="300" border="1" cellpadding="3" cellspacing="2" class="table">'); 
* // show overview-table (mit link zur detail-page)
* $oGallery->get_overview('gallery_detail.php',5); // params: [$detailpage=''][,$cols=''][,$alt=''][,$fixwidth=''][,$fixheight=''][,$spacer=0][,$thumbs=false]
* </code></pre>
*
* Example Detail-page: 
* <pre><code> 
* // init 
* require_once($aENV['path']['global_service']['unix']."class.gallery.php"); 
* $oGallery =& new gallery("pix/"); // params: [$pixdir='/'][,$csvfile='gallery.csv']
* // show act. image 
* $oGallery->get_image_tag(); // params: [$alt=''][,$fixwidth=''][,$fixheight='']
* // show previous/first-signs 
* $oGallery->get_previous(); // params: [$all=true][,$sign_one='&lt;'][,$sign_all='&laquo;']
* // show status 
* $oGallery->get_status(); // params: [$trenner=' / ']
* // show next/last-signs 
* $oGallery->get_next(); // params: [$all=true][,$sign_one='&gt;'][,$sign_all='&raquo;']
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.52 / 2005-01-20 (BUGFIX: path end-shlash-check bei "_load_images()")
*/

class gallery {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse gallery:
		----------------------------------------------------------------------------
		constructor gallery($pixdir='/', $csvfile='gallery.csv')
		private _load_images($pixdir, $csvfile)
		function get_image_tag($alt='', $fixwidth='', $fixheight='')
		function get_previous($all=true, $sign_one='&lt;', $sign_all='&laquo;')
		function get_next($all=true, $sign_one='&gt;', $sign_all='&raquo;')
		function get_status($sign=' / ')
		function get_comment()
		function get_autoslide($nSec=4)
		
		function get_overview($detailpage='', $cols='', $alt='', $fixwidth='', $fixheight='', $spacer=0)
		function get_overview_table($detailpage='', $cols='', $alt='', $fixwidth='', $fixheight='')
		private _get_img_link($detailpage, $picnr, $alt, $fixwidth, $fixheight, $thumbs)
		set_table_tag($str)
		set_tr_tag($str)
		set_td_tag($str)
		----------------------------------------------------------------------------
		HISTORY:
		1.52 / 2005-01-20 (BUGFIX: path end-shlash-check bei "_load_images()")
		1.51 / 2005-01-12 (NEU: "_get_img_link()" + bessere bildgroessen-behandlung)
		1.5 / 2004-07-28 (alle Methoden auf "get_" statt "write_" umgestellt! -> NICHT MEHR ABWAERTSKOMPATIBEL!!!)
		1.41 / 2004-07-14 ("web/" vor den pfad zu den "pix" gestellt)
		1.4 / 2003-08-08 (NEU: "get_autoslide()")
		1.3 / 2003-07-30 (NEU: CSV-Datei mit filename;kommentar im angegebenen Verzeichnis wird erkannt + "function get_comment()")
		1.2 / 2003-07-30 ("get_overview" in "*_table" umbenannt + "get_overview" neu gemacht(ohne table!))
		1.1 / 2003-07-29 (bugfixes + kleine verbesserungen + "var $sGEToptions = '';" + kommentare verbessert)
		1.0 / 2003-07-21
	*/
	
	// vars
	var $sPixDir		= '';		// Verzeichnis, in dem die Bilder liegen
	var $aPix			= array();	// Array, in dem die Bilder gespeichert werden
	var $aTxt			= array();	// Array, in dem ggf. die Kommentare zu den Bildern gespeichert werden (nur bei CSV-Datei!) // key = filename!!!
	var $nAnzPix		= 0;		// Anzahl gefundener Bilder
	var $nActPic		= 1;		// Aktuelle Bild-Nummer
	var $sGEToptions	= '';		// eventuelle GET-Parameter (ausser aktuelle Bildnummer) (-> werden bei Links hinten drangehaengt)
	var $sTAG_table	= '<table border="0" cellpadding="3" cellspacing="2">';	// oeffnender Table-Tag fuer overview-table
	var $sTAG_tr	= '<tr valign="top">';									// oeffnender TR-Tag fuer overview-table
	var $sTAG_td	= '<td align="center">';								// oeffnender TD-Tag fuer overview-table

/**
* Konstruktor -> Initialisiert das gallery-Objekt
*
* Beispiel:
* <pre><code>
* $oGallery =& new gallery(); // params: [$pixdir='/'][,$csvfile='gallery.csv']
* </code></pre>
*
* hier wird der Klasse die Konfiguration (z.B. der Verzeichnispfad, in dem die Bilder liegen) uebergeben: 
* - Wird als erster Parameter ein LEERER String uebergeben, werden keine Bilder geladen.
* - Wird als erster Parameter KEIN String uebergeben, werden alle Bilder aus dem aktuellen Verzeichnis geladen.
* - Wird als zweiter Parameter eine CSV-Datei uebergeben, werden nur diese Bilder geladen.
*
* @access   public
* @param 	string	$pixdir			Verzeichnis, in dem die Bilder liegen (optional -> default: aktuelles Verzeichnis)
* @param	string	$csvfile		CSV-Datei im Format "filename;comment\n" (optional -> default: gallery.csv)
* @param	int		$_GET['i']		Aktuelle Bildnummer (optional -> default: 0)
*/
	function gallery($pixdir='/', $csvfile='gallery.csv') {
		// aktuelle Bildnummer wird gespeichert, wenn eine per GET uebergeben wird
		if (isset($_GET['i']) && !empty($_GET['i'])) { $this->nActPic = $_GET['i']; }
		
		// ggf. andere GET-vars speichern und bei links hintendranhaengen
		$tmp = '&'.str_replace("i=".$_GET['i'], '', $_SERVER['QUERY_STRING']);	// 'i' aus query-string rausfiltern
		if (substr($tmp,-1) == '&') { $tmp = substr($tmp, 0, -1); }				// ggf. '&' am ende des query-strings rausfiltern
		$this->sGEToptions = preg_replace('/(&)+/', '&', $tmp);					// ggf. doppelte '&' aus query-string rausfiltern und speichern
		
		// ggf. Bilder laden
		if (!empty($pixdir)) {
			$this->_load_images($pixdir, $csvfile);
		}
	}

/**
* Private Methode des Konstruktors: Laedt die Bilder
*
* Liest je nach Konfiguration das Verzeichnis aus oder ermittelt die Daten aus der MediaDB 
* und speichert die Bilder in einem Array.
*
* @access   private
* @param 	string	$pixdir			Verzeichnis, in dem die Bilder liegen (optional -> default: aktuelles Verzeichnis)
* @param	string	$csvfile		CSV-Datei im Format "filename;comment\n" (optional -> default: gallery.csv)
* @param	int		$_GET['i']		Aktuelle Bildnummer (optional -> default: 0)
*/
	function _load_images($pixdir, $csvfile) {
		// aktuelles Verzeichnis wird genommen, wenn kein path uebergeben wird
		if ($pixdir == '') {
			$this->sPixDir = getcwd();
		} else {
			$this->sPixDir = $pixdir;
		}
		if (substr($this->sPixDir, -1) != '/') { $this->sPixDir .= '/'; } // ggf. slash hinten dran machen
		
		// oeffnet das Verzeichnis
		$dat = opendir($this->sPixDir);
		
		// CSV-Datei nehmen, wenn im Verzeichnis vorhanden ---------------------------------------
		if (file_exists($this->sPixDir.$csvfile)) {
			$aCSVlines = file($this->sPixDir.$csvfile);
			foreach ($aCSVlines as $z => $sCSVline) {
				list($filename, $description) = explode(';', $sCSVline);
				$this->aPix[$z] = $filename;			// addiere filename zum array "aPix"
				$this->aTxt[$filename] = $description;	// addiere filename zum array "aTxt" ($filename als key!!!)
			}
			$z = $z + 1; // zaehler beginnt bei 0, deshalb +1
		}
		// ansonsten Verzeichnis auslesen und alphabetisch ordnen --------------------------------
		else {
			$z = 0;							// Zaehler
			while ($pix = readdir($dat)) {	//liest die Eintraege aus dem Verzeichnis
				if (preg_match("/.[gif|jpg|png]$/i", $pix)) {
					$this->aPix[] = $pix;	// wenn bilder, addiere zum array
					$z++;
				}
			}
		} // -------------------------------------------------------------------------------------
		
		// array nach dem inhalt der elemente alphabetisch aufsteigend sortieren und index neu schreiben
		sort($this->aPix);
		// da nach sort() das array mit dem key '0' anfaengt, muss ein dummy-eintrag vorne dran gehaengt 
		//werden, um den index um eins nach oben zu schieben!
		array_unshift($this->aPix, 'dummy_fuer_nullten_arrayeintrag');
		unset($this->aPix[0]); // dummy eintrag nach getaner arbeit loeschen
		
		// anzahl gefundene bilder speichern
		$this->nAnzPix = $z;
		
		// schliesst das Verzeichnis
		closedir($dat);
	}

#----------------------------------------------------------------------------- // DETAIL-SEITE

/**
* mit dieser Methode wird der < img ...>-tag gebaut und zurueckgegeben. 
*
* Es kann dazu optional ein ALT-Tag uebergeben werden sowie width und/oder height auf bestimmte werte gezwungen werden.
*
* Beispiel:
* <pre><code>
* // show act. image
* echo $oGallery->get_image_tag(); // params: [$alt=''][,$fixwidth=''][,$fixheight='']
* </code></pre>
*
* @access 	public
* @param	string	$alt		ALT-TAG (optional - default: '')
* @param	string	$fixwidth	Bild auf feste Breite zwingen (optional - default: '')
* @param	string	$fixwidth	Bild auf feste Hoehe zwingen (optional - default: '')
*/
	function get_image_tag($alt='', $fixwidth='', $fixheight='') {
		if (file_exists($this->sPixDir.$this->aPix[$this->nActPic])) {
			$imgsize	= getimagesize($this->sPixDir.$this->aPix[$this->nActPic]);
			$width		= ($fixwidth != '') ? ' width="'.$fixwidth.'"' : '';
			$height		= ($fixheight != '') ? ' height="'.$fixheight.'"' : '';
			if ($width == '' && $height == '') {
				$width	= $imgsize[2]; // fallback
			}
			if ($alt == '' && $this->aTxt[$this->aPix[$this->nActPic]] != '') {
				$alt	= $this->aTxt[$this->aPix[$this->nActPic]];
			}
			// output
			return '<img src="'.$this->sPixDir.$this->aPix[$this->nActPic].'"'.$width.$height.' border="0" alt="'.$alt.'">';
		}
	}

/**
* mit dieser Methode wird ggf. der "vorheriges Bild"-Pfeil gebaut und zurueckgegeben.
* Fuer die Pfeilekoennen auch Bilder (kompletter Image-Tag!) ubergeben werden. 
* Ebenso kann z.B. das Zeichen fuer "erstes Bild" ausgeblendet werden, indem 
* man "false" als ersten Parameter uebergibt.
*
* Beispiel:
* <pre><code>
* // show previous/first-signs
* echo $oGallery->get_previous(); // params: [$all=true][,$sign_one='&lt;'][,$sign_all='&laquo;']
* </code></pre>
*
* @access 	public
* @param	boolean		$all		Zeichen fuer "erstes Bild" anzeigen (optional - default: true)
* @param	string		$sign_one	anzuzeigendes Zeichen fuer "vorheriges Bild" (optional - default: '&lt;')
* @param	string		$sign_all	anzuzeigendes Zeichen fuer "erstes Bild" (optional - default: '&laquo;')
*/
	function get_previous($all=true, $sign_one='&lt;', $sign_all='&laquo;') {
		if ($this->nActPic != 1) { // nur wenn nicht erstes
			if ($all == true) {
				$sStr = '<a href="'.$_SERVER['PHP_SELF'].'?i=1'.$this->sGEToptions.'">'.$sign_all.'</a> ';
			}
			$sStr .= '<a href="'.$_SERVER['PHP_SELF'].'?i='.($this->nActPic-1).$this->sGEToptions.'">'.$sign_one.'</a>';
			// output
			return $sStr;
		}
	}

/**
* mit dieser Methode wird ggf. der "naechstes Bild"-Pfeil gebaut und zurueckgegeben.
* Fuer die Pfeilekoennen auch Bilder (kompletter Image-Tag!) ubergeben werden. 
* Ebenso kann das Zeichen fuer "letztes Bild" ausgeblendet werden, indem man "false" 
* als ersten Parameter uebergibt.
*
* Beispiel:
* <pre><code>
* // show next/last-signs
* echo $oGallery->get_next(); // params: [$all=true][,$sign_one='&gt;'][,$sign_all='&raquo;']
* </code></pre>
*
* @access 	public
* @param	boolean		$all		Zeichen fuer "letztes Bild" anzeigen (optional - default: true)
* @param	string		$sign_one	anzuzeigendes Zeichen fuer "naechstes Bild" (optional - default: '&gt;')
* @param	string		$sign_all	anzuzeigendes Zeichen fuer "letztes Bild" (optional - default: '&raquo;')
*/
	function get_next($all=true, $sign_one='&gt;', $sign_all='&raquo;') {
		if ($this->nActPic != $this->nAnzPix) { // nur wenn nicht letztes
			$sStr = '<a href="'.$_SERVER['PHP_SELF'].'?i='.($this->nActPic+1).$this->sGEToptions.'">'.$sign_one.'</a>';
			if ($all == true) {
				$sStr .= ' <a href="'.$_SERVER['PHP_SELF'].'?i='.$this->nAnzPix.$this->sGEToptions.'">'.$sign_all.'</a>';
			}
			// output
			return $sStr;
		}
	}

/**
* mit dieser Methode wird "soundsovieltes/soundsoviel" gebaut und zurueckgegeben.
*
* Beispiel:
* <pre><code>
* // show status
* echo $oGallery->get_status(); // params: [$trenner=' / ']
* </code></pre>
*
* @access 	public
* @param	string		$trenner		anzuzeigendes Zeichen fuer "Trennerzeichen" (optional - default: ' / ')
*/
	function get_status($trenner=' / ') {
		echo '<b>'.$this->nActPic.'</b>'.$trenner.$this->nAnzPix;
	}

/**
* mit dieser Methode wird eine art Paging-Link-Leiste gebaut und zurueckgegeben.
*
* Beispiel:
* <pre><code>
* // show paging-links
* echo $oGallery->get_paging_links(); // params: [$trenner=' | ']
* </code></pre>
*
* @access 	public
* @param	string		$trenner		anzuzeigendes Zeichen fuer "Trennerzeichen" (optional - default: ' | ')
*/
	function get_paging_links($trenner=' | ') {
		$buffer = array();
		foreach ($this->aPix as $i => $pic) {
			$buffer[] = ($this->nActPic == $i) 
				? '<b>'.$i.'</b>'
				: '<a href="'.$_SERVER['PHP_SELF'].'?i='.$i.$this->sGEToptions.'">'.$i.'</a>';
		}
		// output
		return implode($trenner, $buffer);
	}

/**
* mit dieser Methode wird ggf. ein in der CSV-Datei angegebener Kommentar zurueckgegeben.
*
* Beispiel:
* <pre><code>
* // show comment
* echo $oGallery->get_comment();
* </code></pre>
*
* @access 	public
*/
	function get_comment() {
		if ($this->aTxt[$this->aPix[$this->nActPic]] != '') {
			return $this->aTxt[$this->aPix[$this->nActPic]];
		}
	}

/**
* mit dieser Methode wird ein JavaScript geschrieben, welches ein automatisches Blaettern gebaut und zurueckgegeben.
*
* Beispiel:
* <pre><code>
* // let it slide
* echo $oGallery->get_autoslide(3); // params: [$nSec=4]
* </code></pre>
*
* @access 	public
* @param	int		$nSec		Anzahl Sekunden nachdem das naechste Bild geladen wird (optional - default: 4)
*/
	function get_autoslide($nSec=4) {
		if ($this->nActPic != $this->nAnzPix) { // nur wenn nicht letztes
			$sStr = '<script language="JavaScript" type="text/javascript">function gallerySlide() {';
			$sStr .= "location.href='".$_SERVER['PHP_SELF']."?i=".($this->nActPic+1).$this->sGEToptions."';}";
			$sStr .= 'setTimeout(gallerySlide(),'.$nSec.'000);</script>';
			// output
			return $sStr;
		}
	}

#----------------------------------------------------------------------------- // OVERVIEW-SEITE

/**
* mit dieser Methode werden die Vorschaubilder fuer eine Uebersichtsseite gebaut und zurueckgegeben (ohne Tabelle!).
*
* Besonderheiten: 
* Wird der 2. Parameter "$cols" angegeben, z.B. '4', bricht die Darstellung (in diesem Beispiel) nach jedem vierten Bild um. 
* Wird der 6. Parameter "$spacer" angegeben, z.B. '4', wird zwischen jedem Bild  ein Spacer-GIF (in diesem Beispiel) mit 
* der Breite von 4 Pixeln geschrieben. Ausserdem wird bei jedem Zeilenumbruch ein Spacer-GIF in der selben Hoehe geschrieben. 
*
* Beispiel:
* <pre><code>
* // show overview
* echo $oGallery->get_overview('gallery_detail.php', 5); // params: [$detailpage=''][,$cols=''][,$alt=''][,$fixwidth=''][,$fixheight=''][,$spacer=0][,$thumbs=false]
* </code></pre>
*
* @access 	public
* @param	string	$detailpage		[URI/]Dateiname der Detailseite (optional - default: "$_SERVER['PHP_SELF']")
* @param	int		$cols			Anzahl der Bilder, nach denen umgebrochen werden soll (optional - default: sovielwienoetigumalleineinerreihezuzeigen)
* @param	string	$alt			alt-TAG ALLER(!) Vorschaubilder (optional - default: aktuelle Nr. der bilder)
* @param	string	$fixwidth		ALLE(!) Vorschaubilder auf angegebene Breite zwingen (optional - default: aktuelle Breite der Bilder)
* @param	string	$fixheight		ALLE(!) Vorschaubilder auf angegebene Hoehe zwingen (optional - default: aktuelle Hoehe der Bilder)
* @param	int		$spacer			Anzahl Pixel, die zwischen jedem Vorschaubild gelassen werden soll (optional - default: 0)
* @param	boolean	$thumbs			Moeglichkeit anzugeben, ob die Vorschaubilder als Thumbnails generiert werden sollen
*/
	function get_overview($detailpage='', $cols='', $alt='', $fixwidth='', $fixheight='', $spacer=0, $thumbs=false) {
		
		// vars
		if ($detailpage == '') { $detailpage = $_SERVER['PHP_SELF']; }
		$row_count = 1; // counter
		$sStr = '';
		
		// build table
		foreach ($this->aPix as $picnr => $filename) {
			if (file_exists($this->sPixDir.$this->aPix[$picnr]) && $picnr <= $this->nAnzPix) {
				
				// a + img
				$sStr .= $this->_get_img_link($detailpage, $picnr, $alt, $fixwidth, $fixheight, $thumbs);
				// spacer
				if ($spacer != 0) { 
					if (($cols != '' && ($row_count * $cols != $picnr)) || $cols == '') { // nur wenn hier nicht auch umgebrochen wird
						$sStr .= '<img src="'.$aENV['path']['root']['http'].'web/pix/onepix.gif" width="'.$spacer.'" height="1" border="0" alt="">';
					}
				}
				// break
				if ($cols != '') {
					if ($row_count * $cols == $picnr) { // "$row_count * $cols" = bildnummer, nach der umgebrochen werden soll
						$sStr .= "<br />\n";
						// spacer
						if ($spacer != 0) {
							$sStr .= '<img src="'.$aENV['path']['root']['http'].'web/pix/onepix.gif" width="1" height="'.$spacer.'" border="0" alt=""><br />'."\n";
						}
						$row_count++;
					}
				}
			}
		}
		
		// output
		return $sStr."\n";
	}

/**
* mit dieser Methode wird eine HTML-Tabelle benutzt, um eine Uebersichtsseite zu bauen und zurueckzugeben. (z.B. falls "get_overview()" nicht geht). 
*
* Um die Default-table zu modifizieren, muss vorher eine oder mehrere der Methoden "set_table_tag", "set_tr_tag", "set_td_tag" aufgerufen werden.
*
* Beispiel:
* <pre><code>
* // show overview
* echo $oGallery->get_overview_table('gallery_detail.php',5); // params: [$detailpage=''][,$cols=''][,$alt=''][,$fixwidth=''][,$fixheight=''][,$thumbs=false]
* </code></pre>
*
* @access 	public
* @param	string	$detailpage		[URI/]Dateiname der Detailseite (optional - default: "$_SERVER['PHP_SELF']")
* @param	int		$cols			Anzahl der Zellen in einer TR fixieren (optional - default: sovielwienoetigumalleineinerreihezuzeigen)
* @param	string	$alt			alt-TAG ALLER(!) Vorschaubilder (optional - default: aktuelle Nr. der bilder)
* @param	string	$fixwidth		ALLE(!) Vorschaubilder auf angegebene Breite zwingen (optional - default: aktuelle Breite der Bilder)
* @param	string	$fixheight		ALLE(!) Vorschaubilder auf angegebene Hoehe zwingen (optional - default: aktuelle Hoehe der Bilder)
* @param	boolean	$thumbs			Moeglichkeit anzugeben, ob die Vorschaubilder als Thumbnails generiert werden sollen
*/
	function get_overview_table($detailpage='', $cols='', $alt='', $fixwidth='', $fixheight='', $thumbs=false) {
		
		// vars
		if ($detailpage == '') { $detailpage = $_SERVER['PHP_SELF']; }
		$rows = ($cols == '') ? 1 : ceil($this->nAnzPix/$cols);
		$anz_cols = ($cols == '') ? $this->nAnzPix : $cols;
		
		// build table
		$sStr = $this->sTAG_table."\n";
		for ($r=1; $r <= $rows; $r++) {
			// tr
			$sStr .= $this->sTAG_tr."\n";
			for ($c=1; $c <= $anz_cols; $c++) {
				if ($r == 1) { $picnr = $c; } else { $picnr = ($r - 1) * $anz_cols + $c; }
				// td
				$sStr .= $this->sTAG_td;
				if (file_exists($this->sPixDir.$this->aPix[$picnr]) && $picnr <= $this->nAnzPix) {
					// a + img
					$sStr .= $this->_get_img_link($detailpage, $picnr, $alt, $fixwidth, $fixheight, $thumbs);
				}
				$sStr .= "</td>\n";
			}
			$sStr .= "</tr>\n";
		}
		$sStr .= "</table>\n";
		
		// output
		return $sStr;
		#print_r($this->aPix); // DEBUG
	}

/**
* diese interne Methode erstellt nur die Tags der einzelnen Bilder fuer die overview-methoden und kuemmert sich um die bildgroessen-behandlung.
*
* @access 	private
* @see		get_overview_table(), get_overview()
* @param	string	$detailpage
* @param	int		$picnr
* @param	string	$alt
* @param	string	$fixwidth
* @param	string	$fixheight
* @param	boolean	$thumbs
*/
	function _get_img_link($detailpage, $picnr, $alt, $fixwidth, $fixheight, $thumbs) {
		
		// vars
		$sStr = '';
		if ($detailpage == '') { $detailpage = $_SERVER['PHP_SELF']; }
		
		// attribute
		$width_img		= ($fixwidth != '') ? ' width="'.$fixwidth.'"' : '';
		$width_thumb	= ($fixwidth != '') ? '&x='.$fixwidth : '';
		$height_img		= ($fixheight != '') ? ' height="'.$fixheight.'"' : '';
		$height_thumb	= ($fixheight != '') ? '&y='.$fixheight : '';
		if ($height_img == '' && $height_img == '') {
			$imgsize = getimagesize($this->sPixDir.$this->aPix[$picnr]);
			$width_img = ' '.$imgsize[3]; // fallback = originalgroesse
			$thumbs = false; // ist dann auch nicht noetig!
		}
		if ($alt == '') { $alt = $picnr; }
		
		// build tag
		$sStr .= '<a href="'.$detailpage.'?i='.$picnr.$this->sGEToptions.'">';
		if ($thumbs == true && file_exists("thumb.php") && function_exists("gd_info")) {
			$sStr .= '<img src="thumb.php?image='.$this->sPixDir.$this->aPix[$picnr].$width_thumb.$height_thumb.'" border="0" alt="'.$alt.'">'."\n";
		} else {
			$sStr .= '<img src="'.$this->sPixDir.$this->aPix[$picnr].'"'.$width_img.$height_img.' border="0" alt="'.$alt.'">';
		}
		$sStr .= "</a>";
		
		// output
		return $sStr;
	}

#----------------------------------------------------------------------------- // SET-METHODEN

/**
* mit dieser Methode wird der Default-TABLE-TAG ueberschrieben.
*
* Beispiel:
* <pre><code>
* $oGallery->set_table_tag('< table width="300" border="1" cellpadding="3" cellspacing="2" class="table">');
* </code></pre>
*
* @access 	public
* @param	string	$str	kompletter oeffnender table-tag
*/
	function set_table_tag($str) {
		if (!$str) return;
		$this->sTAG_table = $str;
	}

/**
* mit dieser Methode wird der Default-TR-TAG ueberschrieben.
*
* Beispiel:
* <pre><code>
* $oGallery->set_tr_tag('< tr class="myTr" valign="bottom">');
* </code></pre>
*
* @access 	public
* @param	string	$str	kompletter oeffnender tr-tag
*/
	function set_tr_tag($str) {
		if (!$str) return;
		$this->sTAG_tr = $str;
	}

/**
* mit dieser Methode wird der Default-TD-TAG ueberschrieben.
*
* Beispiel:
* <pre><code>
* $oGallery->set_td_tag('< td width="100" class="myTd">');
* </code></pre>
*
* @access 	public
* @param	string	$str	kompletter oeffnender td-tag
*/
	function set_td_tag($str) {
		if (!$str) return;
		$this->sTAG_td = $str;
	}

} // END class

?>