<?php
/*
 * Created on 04.05.2006
 *
 */
/**
 * 
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * 
 * @param object $oDb
 * @param array $aENV
 * @param string $flag_type
 * @param string $parent
 */
	class Label {
		
		var $aENV = null;
		var $oDb = null;
		var $sFlagtype = '';
		var $sParent = '';
		
		function Label(&$oDb,&$aENV,$flag_type,$parent) {
			$this->oDb = $oDb;
			$this->aENV =& $aENV;
			$this->sFlagtype = $flag_type;
			$this->sParent = $parent;
		}
		
		/**
		 * Liefere alle verfuegbaren Labels fuer das jeweilige modul zurueck
		 * @access public
		 * @param object $oDb
		 * @param array $aENV
		 * @param String $flag_type (modul)
		 * @param String parent
		 * @return array
		 */
		function getLabels($sOrder="") {
			$aLabels = array();
			
			$sql = "SELECT * 
					FROM `".$this->aENV['table']['sys_labels']."` 
					WHERE `flag_type`='".$this->sFlagtype."'";
			if(!empty($this->sParent)) {
			 $sql .= "AND `parent`='".$this->sParent."'";
			}
			$sql .= " ORDER BY ";
			if(empty($sOrder)) {
				$sql .= "`name` ASC";
			}
			else {
				$sql .= $sOrder;
			}
			$this->oDb->query($sql);
			
			for($i=0;$row = $this->oDb->fetch_object();$i++) {
		
				$aLabels[$row->id]['name'] = $row->name;
				$aLabels[$row->id]['color'] = $row->color;
				$aLabels[$row->id]['treeid'] = $row->parent;
			
			}
			$this->oDb->free_result();	
			return $aLabels;
		}
		/**
		 * 
		 * Diese Methode speichert alle Datensätze der Label. Aufruf von popup_editlabel.php und popup_textmodule.php
		 * @param array $aData
		 * @param array $Userdata
		 * @return void
		 * 
		 */
		function save ($aData,$Userdata) {
				
			foreach($aData as $key => $val) {
	 		
	 			if(strstr($key,'label_')) {
	 				$aTmpLabel = explode('_',$key);
	 				if(!empty($aData['label_'.(int)$aTmpLabel[1]])) {
	 					if(strstr($key,'field')) {
	 						$aLabelFields[(int)$aTmpLabel[1]] = $aData['label_'.$aTmpLabel[1]];		
	 					}
	 					else {
	 					
	 						$aLabels[(int)$aTmpLabel[1]] = $val;
	 					}
	 				}
	 			}
	 		}
	 		$icou = count($aLabels);
	 		#print_r($aLabelFields);
	 		for($i=0;list($id,$name) = each($aLabels);$i++) {
	 			$sql = "UPDATE ".$this->aENV['table']['sys_labels']." SET name='$name',color='".$aLabelFields[$id]."',last_mod_by='".$Userdata['id']."' WHERE parent='".$this->sParent."' AND id='$id' AND flag_type='$this->sFlagtype'";
				$this->oDb->query($sql);
	 		}
	 		
	 		if(!empty($aData['newlabel'])) {
	 			$sql = "INSERT INTO ".$this->aENV['table']['sys_labels']." (name,parent,color,flag_type,created,created_by) VALUES ('".$aData['newlabel']."','".$this->sParent."','".$aData['newlabelfield']."','".$this->sFlagtype."','".date('YmdHis')."','".$Userdata['id']."')";
	 			$this->oDb->query($sql);
	 		}
		}
		/**
		 * 
		 * Diese Methode löscht entweder ein Label oder alle.
		 * @param int $delid
		 * @param array $btn
		 * 
		 */
		function delete($delid,$btn) {
			if(!isset($btn['delete_all'])) {
				$this->oDb->make_delete(array('id' => $delid),$this->aENV['table']['sys_labels']);
			}
			else {
				$this->oDb->make_delete(array('flag_type' => $this->sFlagtype),$this->aENV['table']['sys_labels']);
			}
		}
	}
?>
