<?php // Class formchecker
/**
* Ueberprueft Variablen auf korrekten Inhalt. 
* Die "make_secure_xxx() Methoden geben gepruefte Inhalte zurueck, die anderen Methoden geben [true|false] zurueck.
*
* @access	public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>, Andreas Seebald <andreas.seebald@bluemars.de>
* @version	1.61 / 2004-03-24	[BUGFIX "make_secure_string()" -> "trim()" hinzugefuegt]
*/

class formchecker {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse formchecker:
		----------------------------------------------------------------------------
		! NO konstruktor formchecker()
		
		function make_secure_string($sStr)
		function make_secure_int($nIntNumber)
		
		function is_between_number($nBetweenNumber,$nMinimal,$nMaximal)
		function is_german_zip($nGermanZip)
		function is_int_number($nIntNumber)
		function is_number($nNumber)
		function is_phone_number($nPhoneNumber)
		function is_something_in($sFilledOut)
		function is_valid_email($sValidEmail)
		function is_valid_creditcard($sValidCreditCard)
		function is_valid_creditcard_by_type($inCardNumber,$sCardType) (mittels Lohn-Pruefsumme - basiert auf O'Reilly)
		function is_visacard_number($nVisaCardNumber)
		function is_americanexpress_number($nAmericanExpressCardNumber)
		function is_mastercard_number($nMasterCardNumber)
		function is_creditcard($nCreditCardNumber)
		function get_creditcard_type($nCreditCardNumber)
		----------------------------------------------------------------------------
		HISTORY:
		1.61 / 2004-03-24	[BUGFIX "make_secure_string()" -> "trim()" hinzugefuegt]
		1.6 / 2003-09-12	[function "is_valid_creditcard_by_type()" hinzugefuegt]
		1.51 / 2003-06-13	[nur kommentare fuer PHPDoc ueberarbeitet]
		1.5 / 2003-06-13	[chop_trim() entfernt & make_secure_string() + make_secure_int() hizugefuegt]
		1.4 / 2003-01-24 af	[aufgeraeumt und make_secure() hizugefuegt]
		1.2 / 2000-11-15 as	[kreditkarten-methoden hizugefuegt]
		1.1 / 2000-05-29 af
	*/

#----------------------------------------------------------------------------- MAKE-SECURE

/**
* Diese Funktion bitte NICHT MEHR VERWENDEN, ... ist nur noch aus kompatibilitaetsgruenden hier!
*
* @access   public
* @param	string	$sStr	Zu behandelnder String
* @return	string	$sStr	Nicht-behandelter String
*/
	function make_secure($sStr) { return $sStr; }

/**
* Pruefung der Variable auf legale Werte (entfernt "boese" Tags und Zeichen, maskiert einfache und doppelte Anfuehrungszeichen, etc...) 
* Geprueft werden nur Strings, bei einem uebergebenen Array passiert einfach nichts.
*
* @access   public
* @param	string	$sStr	Zu behandelnder String
* @return	string	$sStr	Behandelter String
*/
	function make_secure_string($sStr) {
		if (is_array($sStr)) { return; } // wenn array, dann aussteigen
		if (!get_magic_quotes_gpc()) {$val = addslashes($val);} // maskiert einfache und doppelte Anfuehrungszeichen (mit pruefung der server-einstellung)
		$sStr = str_replace("..", "", $sStr);	// Punkte entfernen  (kein Zugriff auf top-folders moeglich)
		$sStr = str_replace('/','', $sStr);		// Slashes entfernen (keine Pfadangaben moeglich)
		$sStr = str_replace(';','', $sStr);		// Semicolon entfernen (kein SQL abbrechen und ein boeses hinten dranhaengen moeglich)
		$sStr = strip_tags($sStr);				// Entfernt alle HTML-Tags aus einer Zeichenkette.
		
		return trim($sStr);
	}

/**
* Pruefung der Variable auf eine Integer Nummer (ohne Punkt und Komma). 
* Bei Uebergabe eines nicht gueltigen Wertes, wird 0 zurueckgegeben, ausser es ist ein gueltiger Wert darin: Dann wird dieser zurueckgegeben. 
* Geprueft werden nur Strings, bei einem uebergebenen Array passiert einfach nichts.
*
* @access   public
* @param	int	$nIntNumber	Zu behandelnde Integer-Nummer
* @return	int	$nIntNumber	Behandelte Integer-Nummer
*/
	function make_secure_int($nIntNumber) {
		$nIntNumber = isset($nIntNumber) ? $nIntNumber+0 : 0; // Konvertierung auf Integer durch Addition von 0
		// -> "123abc" wird also zu 123, "fasel" zu 0. Ein fehlender Wert wird durch isset() ebenfalls zu 0.
		
		return $nIntNumber;
	}

/* folgende funktion ist leider unpraktikabel:
	make_secure2($var) {	# muss auch mit arrays funktionieren!
		if (!is_array($var)) { $var = array($var); } // mach array aus string
		$newvar = array();
		foreach($var as $key => $val) {
			if (!get_magic_quotes_gpc()) {$val = addslashes($val);} // maskiert einfache und doppelte Anfuehrungszeichen (mit pruefung der server-einstellung)
			$newvar[$key] = $val;
		}
		return $newvar;
	}*/

#----------------------------------------------------------------------------- FORM-CHECKS

/**
* Checkt ob die Eingabe eine Integer Nummer ist und in einen vorgegebenen Zahlenrahmen passt.
*
* @access   public
* @param	int	$nBetweenNumber	Zu pruefende Nummer
* @param	int	$nMinimal		Minimaler Wert der Zahl
* @param	int	$nMaximal		Maximaler Wert der Zahl
* @return	boolean|int			(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_between_number($nBetweenNumber,$nMinimal,$nMaximal) {
		if (($this->is_number($nBetweenNumber)) && ($this->is_filled_out($nBetweenNumber)) && ($nBetweenNumber >= $nMinimal) && ($nBetweenNumber <= $nMaximal)) {
			return true;
		} else {
			return false;
		}
	}

/**
* Checkt ob die Eingabe eine 5 stellige Integer-Nummer ist.
*
* Kann zB fuer Postleitzahlen benutzt werden.
*
* @access   public
* @param	int	$nGermanZip	Zu pruefende Nummer
* @return	boolean|int		(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_german_zip($nGermanZip) {
		if ((preg_match('/[0-9]{5}/',$nGermanZip)) && (strlen($nGermanZip) == 5)) {
			return true;
		} else {
			return false;
		}
	}

/**
* Checkt ob die Eingabe eine Integer Nummer ist.
*
* @access   public
* @param	int	$nIntNumber	Zu pruefende Nummer
* @return	boolean|int		(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_int_number($nIntNumber) {
		if ((preg_match('/([^-])([^0-9])/',$nIntNumber)) || (!$this->is_something_in($nIntNumber))) {
			return false;
		} else {
			return true;
		}
	}

/**
* Checkt ob die Eingabe eine Nummer ist.
*
* , und . koennen fuer die Kommastelle benutzt werden.
*
* @access   public
* @param	float	$nNumber	Zu pruefende Nummer
* @return	boolean|int			(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_number($nNumber) {
		$nLaengeEins	= strlen($nNumber);
		$nNumber		= ereg_replace(',','.',$nNumber);
		$nNumber		= doubleval($nNumber);
		if (($nLaengeEins == strlen($nNumber)) && ($nNumber != 0)) {
		    return true;
		} else {
			return false;
		}
	}

/**
* Checkt ob die Eingabe eine Telefonnummer ist.
*
* Auch internationale Nummern zB mit +49 koennen eingegeben werden. 
* Trennzeichen zwischen den Zahlen duerfen - / und Leerzeichen sein.
*
* @access   public
* @param	int	$nPhoneNumber	Zu pruefende Nummer
* @return	boolean|int			(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_phone_number($nPhoneNumber) {
		if (preg_match('/(^[0-9+])([0-9\\/\\- ]{5,})([0-9]$)/',$nPhoneNumber)) {
			return true;
		} else {
			return false;
		}
	}

#-----------------------------------------------------------------------------

/**
* Checkt ob etwas in der Variable steht.
*
* Leerzeichen werden als kein Zeichen gewertet.
*
* @access   public
* @param	string	$sFilledOut	Zu pruefender String
* @return	boolean|int			(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_something_in($sFilledOut) {
		if (strlen(trim($sFilledOut)) == 0) {
			return false;
		} else {
			return true;
		}
	}

#-----------------------------------------------------------------------------

/**
* Checkt auf eine gueltige E-Mail Syntax.
*
* @access   public
* @param	string	$sValidEmail	Zu pruefenden E-Mail Adresse
* @return	boolean|int				(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_valid_email($sValidEmail) {
        if (preg_match('/(^[0-9a-zA-Z])([0-9a-zA-Z_\.\-\\\[\]]*)([0-9a-zA-Z])(@)([0-9a-zA-Z])([0-9a-zA-Z_\.\-]*)(\.)([0-9a-zA-Z]{2,}$)/',$sValidEmail)) {
            return true;
        } elseif (preg_match('/(^[0-9a-zA-Z\"])([0-9a-zA-Z_\.\-\ \\\[\]]*)([0-9a-zA-Z\"])(@)([0-9a-zA-Z])([0-9a-zA-Z_\.\-]*)(\.)([0-9a-zA-Z]{2,}$)/',$sValidEmail)) {
            return true;
        } else {
            return false;
        }
   }

#----------------------------------------------------------------------------- Kreditkarten

/**
* Checkt auf eine gueltige Kreditkarten Syntax.
*
* @access   public
* @param	string	$sValidCreditCard	Zu pruefenden Kreditkartennummer
* @return	boolean	(true = is_valid | false = not_valid)
*/
	function is_valid_creditcard($sValidCreditCard) {
		if (!$this->is_creditcard($sValidCreditCard)) {
			return false;
		}
		if (($this->is_visacard_number($sValidCreditCard)) || ($this->is_americanexpress_number($sValidCreditCard)) || ($this->is_mastercard_number($sValidCreditCard))) {
			return true;
		} else {
			return false;
		}
	}
/**
* O'Reilly: Programmieren mit PHP, Beispiel 4.1 (Seite 11): Kreditkarten-Validierung: 
* ##################################################################################
* 
* The Luhn checksum determines whether a credit-card number is syntactically 
* correct; it cannot, however, tell if a card with the number has been issued, 
* is currently active, or has enough space left to accept a charge. 
* (->Die Luhn-Pruefsumme bestimmt, ob eine Kreditkartennummer syntaktisch korrekt ist. 
* Sie kann aber nicht ermitteln, ob die Nummer vergeben wurde, momentan aktiv ist und 
* ein ausreichendes Guthaben aufweist.) 
* Original Funktionsname: IsValidCreditCard($inCardNumber,$inCardType)
*
* @access   public
* @param	int		$inCardNumber	Zu pruefende Kreditkartennummer
* @param	int		$sCardType		Kreditkartentyp
* @return	boolean	(true = is_valid | false = not_valid)
*/
	function is_valid_creditcard_by_type($inCardNumber,$sCardType) {
		
		if (!$inCardNumber) return;
		if (!$sCardType) return;
		
		// Assume it's okay
		$isValid = true;
		
		// Strip all non-numbers from the string
		$inCardNumber = ereg_replace('[^[:digit:]]', '', $inCardNumber);
		
		// Make sure the card number and type match
		switch($sCardType) {
			case 'mastercard':	$isValid = ereg('^5[1-5].{14}$', $inCardNumber);				break;
			case 'visa':		$isValid = ereg('^4.{15}$|^4.{12}$', $inCardNumber);			break;
			case 'amex':		$isValid = ereg('^3[47].{13}$', $inCardNumber);					break;
			case 'discover':	$isValid = ereg('^6011.{12}$', $inCardNumber);					break;
			case 'diners':		$isValid = ereg('^30[0-5].{11}$|^3[68].{12}$', $inCardNumber);	break;
			case 'jcb':			$isValid = ereg('^3.{15}$|^2131|1800.{11}$', $inCardNumber);	break;
		}
		
		// It passed the rudimentary test; let's check it against the Luhn this time
		if ($isValid) {
			// Work in reverse
			$inCardNumber = strrev($inCardNumber);
			
		    // Total the digits in the number,doubling those in odd-numbered positions
			// (->Ziffern der Zahl aufsummieren und bei ungeraden Positionen verdoppeln)
			$theTotal = 0;
			for ($i=0; $i < strlen($inCardNumber); $i++) {
				$theAdder = (int) $inCardNumber{$i};
				
				//Double the numbers in odd-numbered positions
				if ($i % 2) {
					$theAdder << 1;
					if ($theAdder > 9) { $theAdder -=9; }
				}
				$theTotal +=$theAdder;
			}
			
			// Valid cards will divide evenly by 10 (->Gueltige Karten lassen sich durch 10 dividieren)
			$isValid =(($theTotal %10)==0);
		}
		
		return $isValid;
	}
//######################################################### END - O'Reilly Beispiel


/**
* Prueft ob es eine Visa Karte ist
*
* @access   private
* @param	int	$nVisaCardNumber	Kreditkarten Nummer
* @return	boolean|int				(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_visacard_number($nVisaCardNumber) {
		$nFirstDigit = substr($nVisaCardNumber,0,1);
		if (((strlen($nVisaCardNumber) == 16) || (strlen($nVisaCardNumber) == 13)) && ($nFirstDigit == 4)) {
			return $this->is_creditcard($nVisaCardNumber);
		} else {
			return false;
		}
	}

/**
* Prueft ob es eine American Express ist
*
* @access   private
* @param	int	$nAmericanExpressCardNumber	Kreditkarten Nummer
* @return	boolean|int						(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_americanexpress_number($nAmericanExpressCardNumber) {
		$nFirstDigit	= substr($nAmericanExpressCardNumber,0,1);
		$nSecondDigit	= substr($nAmericanExpressCardNumber,1,1);
		if ((strlen($nAmericanExpressCardNumber) == 15) && ($nFirstDigit == 3) && (($nSecondDigit == 4) || ($nSecondDigit == 7))) {
			return $this->is_creditcard($nAmericanExpressCardNumber);
		} else {
			return false;
		}
	}

/**
* Prueft ob es eine Mastercard ist
*
* @access   private
* @param	int	$nMasterCardNumber	Kreditkarten Nummer
* @return	boolean|int				(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_mastercard_number($nMasterCardNumber) {
		$nFirstDigit	= substr($nMasterCardNumber,0,1);
		$nSecondDigit	= substr($nMasterCardNumber,1,1);
		if ((strlen($nMasterCardNumber) == 16) && ($nFirstDigit == 5) && ($nSecondDigit >= 1) && ($nSecondDigit <= 5)) {
			return $this->is_creditcard($nMasterCardNumber);
		} else {
			return false;
		}
	}

/**
* Prueft ob es eine Kreditkarte ist
*
* @access   private
* @param	int	$nCreditCardNumber	Kreditkarten Nummer
* @return	boolean|int				(1 - erfolgreich ; 0 - fehlgeschlagen)
*/
	function is_creditcard($nCreditCardNumber) {
		$nSum		= 0;
		$nMul		= 1;
		$nLen		= strlen($nCreditCardNumber);
		$nTProduct	= 0;
		$nDigit		= 0;
		
	// Alle Nummern mit mehr als 19 oder weniger als 13 Zeichen sind falsch
		if (($nLen > 19) || ($nLen < 13)) {
			return false;
		}
		for ($i=0; $i<$nLen; $i++) {
			$nDigit = substr($nCreditCardNumber,$nLen-$i-1,1);
			$nTProduct = $nDigit * $nMul;
			if ($nTProduct >= 10) {
				$nSum += ($nTProduct % 10) + 1;
			} else {
				$nSum += $nTProduct;
			}
			if ($nMul == 1) {
				$nMul++;
			} else {
				$nMul--;
			}
		}
		if (($nSum % 10) == 0) {
			return true;
		} else {
			return false;
		}
	}
	

/**
* Gibt den Typ der Kreditkarte zurueck.
*
* @access   public
* @param	int		$nCreditCardNumber		Kreditkarten Nummer
* @return	string	Kartenname (VISACARD, AMERICAN EXPRESS, MASTERCARD) - INVALID CARD
*/
	function get_creditcard_type($nCreditCardNumber) {
		if ($this->is_visacard_number($nCreditCardNumber)) {
			return 'VISACARD';
		}
		if ($this->is_americanexpress_number($nCreditCardNumber)) {
			return 'AMERICAN EXPRESS';
		}
		if ($this->is_mastercard_number($nCreditCardNumber)) {
			return 'MASTERCARD';
		}
		return 'INVALID CARD';
	}


#-----------------------------------------------------------------------------
} // END of class
?>