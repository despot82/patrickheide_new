<?php // Class dbconnect
/**
* This class provides common MySQL-db-functions (> PHP 5!).
* NOTE: Funktioniert mit MySQL-Versionen >= 4.1, sofern PHP mit der mysqli-Erweiterung 
* kompliliert wurde UND die Standard-MySQL-Erweiterung deaktiviert wurde.
*
* Example:
* <pre><code>
* // init
* $db = new dbconnect($aDBvars);
* // fuehre eine query aus
* $sql = "SELECT id FROM $table";
* $db->query($sql);
* // speichere die anzahl ergebnisse
* $nAnz = $db->num_rows();
* // speichere das ergebnis in einem array und lese es zeilenweise aus
* while ($aResult = ->fetch_array()) { echo $aResult['id']; }
* $db->free_result();
* $db->close();
* </code></pre>
*
* erwartet folgende Variablen aus der Datei "inc.db_variablen.php":
* $aDBvars = array(	'host' => DB-Hostname,
*					'db'   => DB-Name,
*					'user' => Username,
*					'pass' => Password
*					);
*
* PHP 5 Version
* @access   public
* @package  Service
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2006-05-11	[NEU: function "select_db()"]
*/

class dbconnect {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse dbconnect:
		----------------------------------------------------------------------------
		constructor dbconnect($aDBvars)
		function set_debug_true()
		
		function query($sql)
		function limit($sql, $start=0, $limit=0)
		function free_result($result=0)
		function num_rows()
		function affected_rows()
		function fetch_row()
		function fetch_array($type=MYSQL_ASSOC)
		function fetch_object()
		function fetch_in_array($sql, $recover_id=0)
		function fetch_in_aarray($sql, $arraykey=false)
		function fetch_in_flatarray($sql, $trennzeichen=",")
		function fetch_field($field)
		function fetch_by_id($id, $table)
		function insert_id()
		function get_fields($table="")
		function get_tables()
		
		function get_field_name($field_index)
		function get_field_type($field_index)
		function get_field_flags($field_offset)
		
		function save($array, $table)
		function make_insert($array, $table)
		function make_update($array, $table, $updt_array)
		function make_delete($array, $table)
		function make_search($fields, $needle, $table, $orderby='', $limit=null, $start=0)
		function fulltext_search($fields, $needle, $table, $orderby='', $limit=null, $start=0)
		
		function make_tbl_stucture_backup($sTable)
		function make_tbl_content_backup($sTable,$sSqlConstraint='',$sTblFields='')
		function db_backup_in_file($sFile)
		function db_restore_from_file($sFile)
		
		private _convert_ms_word_crap()
		----------------------------------------------------------------------------
		HISTORY:
		2.4 / 2005-09-05	[NEU: function "affected_rows()"]
		2.3 / 2005-06-15	["fetch_array()" auf NUR assioziativ umgestellt! + NEU: "fetch_object()"]
		2.22 / 2005-05-19	["_convert_ms_word_crap()" deaktiviert, weil scheinbar kein Problem mehr]
		2.21 / 2005-02-22	[NEU: Feld "relevance" bei "fulltext_search()"]
		2.2 / 2004-09-02	[NEU: "save()"]
		2.1 / 2004-09-01	[NEU: "fulltext_search()"]
		2.0 / 2004-08-23	[NEU: "db_backup_in_file()" + "db_restore_from_file()"]
		1.9 / 2004-08-20	[NEU: "make_search()" um Funktionalitaeten erweitert]
		1.82 / 2004-07-21	[NEU: "query()" um Stoppuhr erweitert]
		1.81 / 2004-07-14	["_convert_ms_word_crap()" erweitert]
		1.8 / 2004-07-06	[NEU: (private) "_convert_ms_word_crap()" (in "make_insert()" + "make_update()" eingebaut)]
		1.73 / 2004-04-29	[UMBAU in "limit()" (direkter aufruf von "query()")]
		1.72 / 2003-10-09	[BUGFIX in "make_tbl_content_backup()" (backslashes + singlequotes nochmals maskieren)]
		1.71 / 2003-09-16	[BUGFIX in "make_tbl_content_backup()" (addslashes)]
		1.7 / 2003-08-29	[backup-funktionen hinzugefuegt]
		1.6 / 2003-08-28	[get_field_name/type/flags() hinzugefuegt]
		1.51 / 2003-06-13	[nur kommentare fuer PHPDoc ueberarbeitet]
		1.5 / 2003-05-23
	*/
	
	// Class Vars.
	private $result = '';
	private $db_cid = 0;
	public $debug = false;
	public $soptions = array();
	private $host = '';
	private $db = '';
	private $user = '';
	private $pass = '';

/**
* Konstruktor -> Initialisiert das dbconnect-Objekt
*
* Beispiel:
* <pre><code>
* $oDb = new dbconnect($aDBvars);
* </code></pre>
*
* @access   public
* @param 	array	$aDBvars	DB-Connect-Variablen.
*/
	public function dbconnect($aDBvars) {
		
		$this->host = $aDBvars['host'];
		$this->db	= $aDBvars['db'];
		$this->user = $aDBvars['user'];
		$this->pass = $aDBvars['pass'];
		
		// Search Options
		$this->soptions['goperator']		= "AND";	// verbindet mehrere Suchbegriffe
		$this->soptions['operator'] 		= "OR";		// verbindet einzelne Felder
		$this->soptions['sign'] 			= "LIKE";	// wird zur Abfrage verwendet [ "LIKE" | "=" ]
		$this->soptions['pre'] 				= "%";		// wird einem Suchbegriff vorangestellt
		$this->soptions['post']				= "%";		// wird einem Suchbegriff angehaengt
		$this->soptions['select']			= "*";		// was selektiert wird
		$this->soptions['extra']			= "";		// wird dem SQL-Statement angehaengt
		$this->soptions['ignore_case']		= true;		// Findet sowohl grossgeschriebene als auch kleingeschriebene Varianten der Suchbegriffe
		$this->soptions['keep_wordgroups']	= true;		// Erkennt und erhaelt "Mehrwoerter-Begriffe" (Suche nach Wortgruppen)
		$this->soptions['search_umlauts']	= false;	// Findet bei Suchwoertern mit Umlauten auch deren Pendants mit Doppellauten (und umgekehrt)
		$this->soptions['passive_mode']		= false;	// Passiv-Modus liefert nur das SQL-Statement, OHNE es auszufuehren
		
		// Blacklist Filter
//		$this->blacklist	= array("und", "oder", "der", "die", "das", "ein", "eine", "1", "2", "3", "4", "5", "6", "7", "8", "9");
		$this->blacklist	= array();
	}
	
	/*
	Alternativer Aufbau der Klasse mit folgendem Kontruktor, der sich die DB-Vars selbst holt
	und NICHT die "inc.db_variablen.php" benoetigt!
	Das haette zur Folge, dass eine neue Instanz des Objektes ohne Parameteruebergabe erfolgen 
	koennte ($db = new dbconnect(); STATT $db = new dbconnect($aDBvars);)
	
	function dbconnect() {
		
		switch($_SERVER['SERVER_NAME']) {
			case "10.1.1.5":			// Dev-Server
				$this->host = "localhost";
				$this->db = "ift";
				$this->user = "root";
				$this->pass = "";
				break;
			case "www.design-aspekt.com":	// Preview-Server
				$this->host = "dbhost:336";
				$this->db = "vs2_b6";
				$this->user = "vs2-b";
				$this->pass = "aepa5Goo";
				break;
		}
		
		// mit DB verbinden!
		... usw...
	}
	*/

#-----------------------------------------------------------------------------

/**
* verbindet mit der DB [aus dem konstruktor ausgelagert, um unnoetige connects (auf seiten die keine db nutzen) zu vermeiden]
*
* @access	private
*/
	private function connect() {
		// mit DB verbinden + DB auswaehlen
		$this->db_cid = mysqli_connect($this->host, $this->user, $this->pass, $this->db);
		if (!$this->db_cid) {
			trigger_error("Fehler bei der Verbindung zur Datenbank. ".
				"MySQL Error: ".mysqli_connect_errno()." - ".mysqli_connect_error()."!",E_USER_ERROR);
		}
	}

/**
* Selektiert die aktuelle Datenbank
*
* @access	public
*/
	function select_db() {
		return mysqli_select_db($this->db_cid, $this->db);
	}

/**
* Schliesst eine zuvor geoeffnete DB-Verbindung.
*
* @access	public
* @param	integer result Result-Handler (optional)
*/
	public function close() {
		if ($this->db_cid) {
			mysqli_close($this->db_cid);
			$this->result = 0;
		}
	}

#-----------------------------------------------------------------------------

/**
* Schaltet den Debug-Modus an (-> Debug-Messages bei query werden ausgegeben)
*
* @access	public
*/
	public function set_debug_true() {
		$this->debug = true;
	}


/**
* private Query-Stoppuhr Funktion um die Startzeit (in Mikrosekunden) zu ermitteln
*
* NOTE: nur bei aktiviertem debug-modus! 
* (-> ergebnis wird dann bei Debug-Messages ausgegeben)
*
* @access	private
*/
	private function _getmicrotime(){
		list($usec, $sec) = explode(" ",microtime());
		return ((float)$usec + (float)$sec);
	}

#-----------------------------------------------------------------------------

/**
* Fhrt Abfragen auf der Datenbank aus (inkl. Debug-Message bei aktiviertem debug-modus)
*
* @access	public
* @param	string	$sql	SQL-Befehl
* @return	result set
*/
	public function query($sql) {
		if (!$sql) return;
		if (!$this->db_cid) $this->connect(); // Konnektiere DB
		if ($this->debug) {
			// Stoppuhr starten
			list($usec, $sec) = explode(" ",microtime());
			$watch_start = ((float)$usec + (float)$sec);
		}
		$this->result = mysqli_query($this->db_cid, $sql);
		$this->id	= mysqli_insert_id();
		
		if (mysqli_errno($this->db_cid)) {
			$error = "MySQL Error: ".mysqli_errno()." - ".mysqli_error($this->db_cid)."!";
			// zeige ggf. sql
			if ($this->debug) { echo "<b>".$sql."</b><br>".$error."<br><br>"; }
			// trigger mysql fehler
			trigger_error($error,E_USER_ERROR);
		}
		if ($this->debug) {
			// zeige sql (im fehlerfall nicht nochmal)
			if (!mysqli_errno($this->db_cid)) echo "<b>".$sql."</b><br>";
			//  Stoppuhr beenden und Ergebnis ausgeben
			$elapse = sprintf('%.4f', $this->_getmicrotime() - $watch_start);
			echo "(Die Abfrage ben&ouml;tigte ".$elapse." Sekunden)<br>";
		}
		return $this->result;
	}

/**
* Limitiert die Datensaetze eines Select-Statements auf $limit Eintrge (ab dem $start-ten-Ergebnis).
*
* @access	public
* @param	string	$sql	SQL-Befehl
* @param	integer	$start	Offset fr die Abfrage. Die ersten start-1 Ergebnisse werden nicht ausgegeben.
* @param	integer	$limit	Gibt an, wieviele Ergebnisse zurckgegeben werden sollen.
* @return	result set
*/
	public function limit($sql, $start=0, $limit=0) {
		if (!$sql) return;
		if ($start == 0 && $limit == 0) {
			return $this->query($sql);
		} else {
			return $this->query($sql." LIMIT $start, $limit");
		}
	}

/**
* Gibt den (von der letzten Abfrage) verwendeten Speicher wieder frei.
*
* @access	public
* @param	integer result Result-Handler (optional)
*/
	public function free_result($result=0) {
		if ($result == 0) { $result = $this->result; }
		if ($result != 0) {
			mysqli_free_result($result);
			$this->result = 0;
		}
	}

/**
* Liefert die Anzahl von Zeilen, die mit dem letzten SELECT-Statement gefunden wurden.
*
* @access	public
* @return	integer Anzahl der gefunden Zeilen oder 0
*/
	public function num_rows() {
		return mysqli_num_rows($this->result);
	}

/**
* Liefert die Anzahl von Zeilen, die mit dem letzten INSERT, UPDATE oder DELETE-Statement gefunden wurden.
*
* @access	public
* @return	integer Anzahl der gefunden Zeilen oder 0
*/
	public function affected_rows() {
		return mysqli_affected_rows($this->db_cid);
	}

/**
* Liefert eine Zeile des Results als Array mit Spaltennummer => Wert, 
* oder False wenn keine Daten mehr vorhanden 
* Bei jedem Aufruf wird der interne Zeiger auf den nchsten Datensatz gesetzt.
*
* @access	public
* @return	mixed Array, oder False, wenn nichts mehr vorhanden
*/
	public function fetch_row() {
		return mysqli_fetch_row($this->result);
	}

/**
* Liefert eine Zeile des Results als assoziatives Array, als numerisches Array oder beides 
* mit Spaltenname => Wert (und/oder Num-Key => Wert), oder False wenn keine Daten mehr vorhanden. 
* Bei jedem Aufruf wird der interne Zeiger auf den nchsten Datensatz gesetzt.
* NOTE: Als optionaler Parameter kann MYSQL_ASSOC, MYSQL_NUM und MYSQL_BOTH uebergeben werden, um 
* entweder nur ein assoziatives (=default), nur ein numerisches, oder beide Arrays zurueckzubekommen.
* NOTE: In der nativen PHP-Funktion ist der default: MYSQL_BOTH.
*
* @access	public
* @param	string	$type	(optional) PHP-Konstante [MYSQLI_ASSOC (default) | MYSQLI_NUM | MYSQLI_BOTH]
* @return	mixed	assoziatives Array, oder False, wenn nichts mehr vorhanden
*/
	public function fetch_array($type=MYSQLI_ASSOC) {
		return @mysqli_fetch_array($this->result, $type);
	}

/**
* Liefert eine Zeile des Results als Objekt (er Value wird mittels Spaltenname ermittelt,
* z.B. $row->Spaltenname), oder False wenn keine Daten mehr vorhanden. 
* Bei jedem Aufruf wird der interne Zeiger auf den nchsten Datensatz gesetzt.
*
* @access	public
* @return	object	(assoziativ), oder False, wenn nichts mehr vorhanden
*/
	public function fetch_object() {
		return @mysqli_fetch_object($this->result);
	}

/**
* diese Funktion speichert den Result der Abfrage in einem 2 dimensionalem (nicht assioziativem) Array.
*
* @access	public
* @param	string	$sql		SQL-Statement
* @param	integer	$recover_id	Position der ID in der Abfrage (wird dann als Key verwendet)
* @return	array	2 dimensionales Array mit den Werten des Results
*/
	public function fetch_in_array($sql, $recover_id=0) {
		$buffer = array();
		$this->query($sql);
		while($r = $this->fetch_row()) {
			if ($recover_id) {
				$buffer[$r[$recover_id-1]] = $r;
			} else {
				$buffer[] = $r;
			}
		}
		$this->free_result();
		return $buffer;
	}

/**
* diese Funktion ist vergleichbar mit fetch_in_array() speichert die Daten aber in einem assioziativem Array!
*
* @see		fetch_in_array();
* @access	public
* @param	string	$sql	SQL-Statement
* @param	mixed	$arraykey	 Name des Schluesselfeldes (Arrayschluessel)
* @return	array	2 dimensionales Array mit den Werten des Results
*/
	public function fetch_in_aarray($sql, $arraykey=false) {
		$buffer = array();
		$this->query($sql);
		while ($r = $this->fetch_array(MYSQLI_ASSOC)) {
			if ($arraykey) {
				$buffer[$r[$arraykey]] = $r;
			} else {
				$buffer[] = $r;
			}
		}
		return $buffer;
	}

/**
* diese Funktion speichert den Result der Abfrage in einem 1 dimensionalem (nicht assioziativem) Array! 
* Das Trennzeichen wird dazu verwendet den einzelnen Felder zu trennen.
*
* @access	public
* @param	string	$sql	SQL-Statement
* @param	string	$trennzeichen	Trennzeichen
* @return	array	2 dimensionales Array mit den Werten des Results
*/
	public function fetch_in_flatarray($sql, $trennzeichen=',') {
		$buffer = array();
		$this->query($sql);
		while ($r = $this->fetch_row()) {
			$buffer[] = implode($trennzeichen, $r);
		}
		return $buffer;
	}

/**
* Liefert den Wert eines bel. Feldes, aus dem aktuellen Result.
*
* @access	public
* @param	string	$field	Spaltenname der zu liefernden Spalte des Results
* @return	mixed	Wert der Spalte
*/
	public function fetch_field($field) {
		$r = $this->fetch_array(MYSQLI_ASSOC);
		return $r[$field];
	}

/**
* Liefert einen Datensatz als assoziatives Array unter Angabe des zugehrigen Primrschlssels ($id).
*
* @access	public
* @param	string	$id		Primrschlssel des Datensatzes
* @param	string	$table	Tabellenname
* @param	string	$fields	[optional: Feldname(n) als kommaseparierter string (default: *)]
* @return	array	Datendatz als assoziatives Array
*/
	public function fetch_by_id($id, $table, $fields='') {
		if ($fields == '') $fields = '*';
		$this->query("SELECT ".$fields." FROM `".$table."` WHERE `id` = '".$id."'");
		$buffer = $this->fetch_array(MYSQLI_ASSOC);
		#$this->free_result();
		return $buffer;
	}

/**
* Liefert (den) Primrschlssel des letzten Insert-Statements zurck (wichtig bei automatischen Primrschlsseln).
*
* @access public
* @return mixed	Primrschlssel
*/
	public function insert_id() {
		return mysqli_insert_id(); //$this->result
	}

/**
* Diese Funktion liefert ein Array mit den Spaltennamen von $table.
*
* @access	private
* @param	string	$table	Tabellenname
* @return	array	Spaltennamen
*/
	public function get_fields($table='') {
		if ($table == '') return;
		$this->query("SELECT * FROM `".$table."` LIMIT 0,2");
		$fieldnames = array();
		/* Get field information for all columns */
		$fieldinfo = mysqli_fetch_fields($this->result);
		foreach ($fieldinfo as $field) {
			$fieldnames[] = $field->name;
		}
		#$this->free_result();
		return $fieldnames;
	}

/**
* Diese Funktion liefert ein Array mit allen Tabellennamen (von dieser DB).
*
* @access	private
* @return	array	Tabellennamen
*/
	public function get_tables() {
		$tables = array();
		$oldresult = $this->result; // aktuelles Result merken
		$this->query("show tables");
		while($t = $this->fetch_row()) {
			$tables[] = $t[0];
		}
		#$this->free_result();
		$this->result = $oldresult; // aktuelles Result wiederherstellen
		
		return $tables;
	}

#-----------------------------------------------------------------------------

/**
* Diese Funktion liefert ein String mit dem Namen des Tablefields. 
* (Davor muss eine Query abgesetzt worden sein, da diese Funktion den "result" benoetigt).
*
* @access	public
* @param	integer	$field_index	feldnummer
* @return	string	Fieldname
*/
    public function get_field_name($field_index) {
        $fieldinfo = mysqli_fetch_field_direct($$this->result, $field_index);
        return $fieldinfo->name;
    }

/**
* Diese Funktion liefert ein String mit dem Typ des Tablefields. 
* (Davor muss eine Query abgesetzt worden sein, da diese Funktion den "result" benoetigt).
*
* @access	public
* @param	integer	$field_index	feldnummer
* @return	string	Typ
*/
    public function get_field_type($field_index) {
        $fieldinfo = mysqli_fetch_field_direct($$this->result, $field_index);
        return $fieldinfo->type;
    }

/**
* Diese Funktion liefert ein String mit den Attributen (durch Leerzeichen getrennt) des Tablefields. 
* (Davor muss eine Query abgesetzt worden sein, da diese Funktion den "result" benoetigt).
*
* @access	public
* @param	integer	$field_offset	feldnummer
* @return	string	Attribute
*/
    public function get_field_flags($field_index) {
        $fieldinfo = mysqli_fetch_field_direct($$this->result, $field_index);
        return $fieldinfo->flags;
    }

#-----------------------------------------------------------------------------

/**
* Vereinfacht die Save-Prozedur eines Datensatzes in die Tabelle.
* ACHTUNG: Vereinfacht heisst:
* Ist in dem uebergebenen Datensatz $array ein "Feld" namens "id" (und ist dieses nicht 0),
* wird ein update mit der Bedingung "WHERE id=$array['id']", ansonsten ein insert gemacht!
*
* @access 	public
* @param	array	$array	assoziatives Array mit den Werten, die Eingetragen werden sollen
* @param	string	$table	Tabellenname
* @return	array 	in die methode uebergebenes array mit ggf. neuer id
*/
	public function save($array, $table) {
		if (isset($array['id']) && $array['id'] != 0 && !empty($array['id'])) {
			$this->make_update($array, $table, $array['id']);
		} else {
			$this->make_insert($array, $table);
			$array['id'] = $this->insert_id();
		}
		return $array;
	}

/**
* Trgt den in $array beschriebenen Datensatz in die Tabelle $table ein, 
* erzeugt also sebstndig das ntige InsertSQL-statement aus einem Assioziativen Array.
*
* @access 	public
* @param	array	$array	assoziatives Array mit den Werten, die Eingetragen werden sollen
* @param	string	$table	Tabellenname
* @return	mixed 	result
*/
	public function make_insert($array, $table, $bReturn = false) {
		if (isset($array['id']) && $array['id'] == 0) { unset($array['id']); }
		$sql	= "INSERT INTO `".$table."` (";
		$values	= ") VALUES (";
		$komma	= '';
		$count	= count($array);
		for ($i = 0; $i < $count; $i++) {
			$value = current($array);
			$value = $this->_convert_ms_word_crap($value);
			if (!get_magic_quotes_gpc()) { $value = addslashes($value); }
			$sql .= $komma.'`'.key($array).'`';
			$values .= $komma."'".$value."'";
			$komma = ", ";
			next($array);
		}
		$sql .= $values.")";
		
		return ($bReturn == false ) ? $this->query($sql): $sql;
	}

/**
* Ersetzt die in $array beschriebenen Daten in allen Datensaetzen in Tabelle $table, 
* fr die die angegebenen Bedingungen zutreffen - erzeugt also sebstndig das ntige 
* UpdateSQL-statement aus einem Assioziativen Array. Das assoziative Array "$updt_array" 
* bildet die Bedingugen ab: Array("userid" => 345) bedeutet "WHERE userid = 345". 
* Hinweis: 
* Statt einem assoziativem Array kann auch NUR der Primrschlssel angegeben werden,
* wodurch auschlielich ein Datensatz verndert wird: 345 bedeutet "WHERE id = 345".
*
* @access 	public
* @param	array	$array	assoziatives Array mit den Werten, die Eingetragen werden sollen
* @param	string	$table	Tabellenname
* @param	mixed	$updt_array	assoziatives Array mit den Updatebedingungen, oder Primrschlssel
* @return	mixed 	result
*/
	public function make_update($array, $table, $update, $bReturn = false) {
		$updt_array = (!is_array($update)) ? Array("id" => $update) : $update;
		
		$sql	= "UPDATE `".$table."` SET ";
		$komma	= '';
		$count	= count($array);
		for ($i = 0; $i < $count; $i++) {
			$value = current($array);
			$value = $this->_convert_ms_word_crap($value);
			if (!get_magic_quotes_gpc()) { $value = addslashes($value); }
			$sql .= $komma.'`'.key($array)."` = '".$value."' ";
			$komma = ", ";
			next($array);
		}
		$komma	 = '';
		$sql	.= " WHERE ";
		$count	 = count($updt_array);
		for ($i = 0; $i < $count; $i++) {
			$value = current($updt_array);
			if (!get_magic_quotes_gpc()) { $value = addslashes($value); }
			$sql .= $komma.'`'.key($updt_array)."` = '".$value."' ";
			$komma = "AND ";
			next($updt_array);
		}
		return ($bReturn == false ) ? $this->query($sql): $sql;
	}

/**
* Entfernt die Datensaetze, die als Bedingungen in dem assoziativem Array "$array" uebergeben werden. 
* Das uebergebene assoziative Array "$array" bildet folgendermassen die SQL-Bedingungen ab: 
* Array("userid" => 345) bedeutet "WHERE userid = '345'" 
* Hinweis: 
* Statt einem assoziativem Array kann auch NUR der Primrschlssel angegeben werden,
* wodurch auschlielich ein Datensatz verndert wird: 345 bedeutet "WHERE id = '345'".
*
* @access 	public
* @param	mixed	$array	assoziatives Array mit den Delete-Bedingungen, oder nur Primrschlssel als string
* @param	string	$table	Tabellenname
* @return	mixed 	result
*/
	public function make_delete($array, $table, $bReturn = false) {
		if(!is_array($array)) { $array = array("id" => $array); }
		
		$sql	= "DELETE FROM `".$table."` WHERE ";
		$komma	= '';
		$count	= count($array);
		for ($i = 0; $i < $count; $i++) {
			$value = current($array);
			if (!get_magic_quotes_gpc()) { $value = addslashes($value); }
			$sql .= $komma.'`'.key($array)."` = '".$value."' ";
			$komma = "AND ";
			next($array);
		}
		return ($bReturn == false ) ? $this->query($sql): $sql;
	}

#-----------------------------------------------------------------------------

/**
* Diese Funktion sucht in den angegebenen Feldern nach den Suchbegriffen. 
* Ist fields - leer werden alle felder in der $table benutzt!
* Optional kann die Sortierung (ohne "ORDER BY") angegeben werden.
* Optional kann die Ausgabe auf $limit Datensetze beschraenkt werden.
* Optional kann die Ausgabe bei Datensatz $start anfangen.
* Ausserdem kann das Verhalten der Funktion durch Ueberschreiben der Klassenvariablen $this->soptions beeinflusst werden:
* So wird z.B. standardmaessig in lowercase mittels "LIKE" nach "%needle%" gesucht, mehrere Suchbegriffe mit "AND", mehrere Felder mit "OR" verbunden...
* Zum Testen kann $this->soptions['passive_mode'] auf true gesetzt werden um die query auszugeben aber nicht auszufuehren!
*
* Beispiel:
* <pre><code>
* $db->soptions['goperator']		= "OR";
* $db->soptions['select']			= "id";
* $db->soptions['search_umlauts']	= true;
* $db->blacklist = array("nazi", "ein", "eine", "der", "die", "das");
* $db->make_search("answer_de", $searchstring, "cms_help");
* while($db->fetch_array()) { ... }
* </code></pre>
*
* @access 	public
* @param	mixed	$fields		Array mit Spaltennamen, Spaltenliste, oder ein leerer String (=> alle Spalten der Tabelle)
* @param	string	$needle		Suchwort/durch Leerzeichen getrennte Wrter
* @param	string	$table		Tabellenname
* @param	string	$orderby	Oderby-Clause (z.b. "name DESC, email") (default: '')
* @param	integer	$limit		Limitiert die Abfrage auf x Datensaetze (default: NULL)
* @param	integer	$start		Beginnt bei y mit der Ausgabe und zeigt x Datensaetze an (default: 0)
* @return	mixed 	result
*/
	public function make_search($fields, $needle, $table, $orderby='', $limit=null, $start=0) {
	// needles
		###if (empty($needle)) return false; // <-- UNSINN!
		$needle		= stripslashes($needle); // escape-shlashes entfernen!
		$needle		= str_replace('  ', ' ', trim($needle)); // doppelte leerzeichen durch einfache ersetzen
		// 1. ggf. "Mehrwort-Begriffe" erhalten
		if ($this->soptions['keep_wordgroups']) {
			// doppelte gegen einfache quotes tauschen
			$needle		= str_replace('"', "'", $needle);
			$needle		= ' '.$needle.' '; // zum besseren isolieren vorne und hinten spacer dran
			// ist ein single-quote drin?
			if (strstr($needle, "'")) {
				// zusammenhaengende woerter isolieren
				$preg_search = "/ '(.*)' /i";
				preg_match_all($preg_search, $needle, $needles_a);
				// zusammenhaengende woerter aus original string entfernen
				foreach ($needles_a[0] as $v) { // $needles_a[0] hat noch die quotes drin
					$needle	= str_replace(trim($v), '', $needle);
				}
				// doppelte leerzeichen gegen einzelne tauschen
				$needle		= str_replace('  ', ' ', trim($needle));
				// uebrige woerter des originalstring splitten
				$needles_b	= (!empty($needle)) ? explode(' ', $needle) : array();
				// arrays zu fertigem sucharray verbinden
				$needles	= array_merge($needles_a[1], $needles_b); // $needles_a[1] hat keine quotes
				// wieder zu string zusammenfuehren
				###$needle		= implode(' ', $needles); // <-- UNSINN!
			} else {
				// ...ansonsten hinzugefuegte spacer wieder entfernen
				$needle		= str_replace('  ', ' ', trim($needle));
			}
		}
//		// 2. ggf. Blacklist-Filter
//		if (is_array($this->blacklist) && count($this->blacklist) > 0) {
//			### das geht leider nicht, da $needle hier fuer immer in lowercase gewandelt wird...
//			###$needle		= str_replace($this->blacklist, '', strToLower($needle));
//			###$needle		= str_replace('  ', ' ', trim($needle));
//			if (!is_array($needles)) { $needles = explode(' ', $needle); }
//			foreach ($needles as $k => $v) {
//				$v		= strToLower($v);
//				if (in_array($v, $this->blacklist)) { unset($needles[$k]); } 
//			}
//			// wieder zu string zusammenfuehren
//			###$needle		= implode(' ', $needles); // <-- UNSINN!
//		}
		if (!is_array($needles)) { $needles = explode(' ', $needle); }
		// 3. ggf. Suche nach Umlaut-Pendents (und umgekehrt)
		if ($this->soptions['search_umlauts']) {
			if (!is_array($needles)) { $needles	= explode(' ', $needle); }
			$umls		= array ('ä','Ä','ö','Ö','ü','Ü','ß');
			$dpls		= array ('ae','Ae','oe','Oe','ue','Ue','ss');
			// doppellaut- und umlaut-arrays
			$needles_doppel	= array();
			$needles_umlaut	= array();
			foreach ($needles as $v) {
				$needles_doppel[] = str_replace($umls, $dpls, $v);
				$needles_umlaut[] = str_replace($dpls, $umls, $v);
			}
			### das geht nicht wenn suchbegriffe mit AND verbunden werden!
			###$tmp		= array_merge($tmp_doppel, $tmp_umlaut);// doppellaut und umlaut tmp-arrays zusammenfuehren
			###$needles	= array_unique($tmp);// doppelte loeschen!
			###$needle		= implode(' ', $needles);// wieder zu string zusammenfuehren
			// original-array mit doppellaut-array ueberschreiben
			###$needles		= $needles_doppel; // <-- UNSINN!
		}
		###$needles	= explode(' ', $needle); // <-- UNSINN!
		$count_needles	= count($needles);
	// fields
		if (!is_array($fields)) { $fields = explode(',', $fields); }
		if (!is_array($fields)) { $fields = $this->get_fields($table); }
		$count_fields	= count($fields);
	// Feldsuchstring SQL
		$gkomma	= '';
		$sql	= '';
		for ($k = 0; $k < $count_needles; $k++) {
			$komma		 = '';
			$sql		.= $gkomma." ( ";
			for ($i = 0; $i < $count_fields; $i++) {
				$sql	.= $komma;
				// ggf. nochmal gruppieren (wenn mit Umlauten)
				if ($this->soptions['search_umlauts'] && 
					($needles[$k] != $needles_umlaut[$k] || $needles[$k] != $needles_doppel[$k])
					) { $sql .= " ( "; }
				
				// field (ggf. lowercase) (Original)
				$sql	.= ($this->soptions['ignore_case']) 
					? "LOWER(`".$fields[$i]."`)" 
					: "`".$fields[$i]."`";
				// value
				$value	 = ($this->soptions['ignore_case']) 
					? strToLower($needles[$k]) 
					: $needles[$k];
				#if (!get_magic_quotes_gpc()) { $value = addslashes($value); }
				$value = addslashes($value); // am anfang der methode wurden alle slashes entfernt!!!
				$sql	.= " ".$this->soptions['sign']." '".$this->soptions['pre'].$value.$this->soptions['post']."' ";
				
			// ggf. das Selbe nochmal mit Umlauten
				if ($this->soptions['search_umlauts'] && $needles[$k] != $needles_umlaut[$k]) {
					// field (ggf. lowercase)
					$sql	.= ($this->soptions['ignore_case']) 
						? "OR LOWER(`".$fields[$i]."`)" 
						: "OR `".$fields[$i]."`";
					// value_umlaut
					$value = ($this->soptions['ignore_case']) 
						? strToLower($needles_umlaut[$k]) 
						: $needles_umlaut[$k];
					$value = addslashes($value); // am anfang der methode wurden alle slashes entfernt!!!
					$sql .= " ".$this->soptions['sign']." '".$this->soptions['pre'].$value.$this->soptions['post']."' ";
				} // END if Umlaute
				
			// ggf. das Selbe nochmal mit Doppellauten
				if ($this->soptions['search_umlauts'] && $needles[$k] != $needles_doppel[$k]) {
					// field (ggf. lowercase)
					$sql	.= ($this->soptions['ignore_case']) 
						? "OR LOWER(`".$fields[$i]."`)" 
						: "OR `".$fields[$i]."`";
					// value_umlaut
					$value = ($this->soptions['ignore_case']) 
						? strToLower($needles_doppel[$k]) 
						: $needles_doppel[$k];
					$value = addslashes($value); // am anfang der methode wurden alle slashes entfernt!!!
					$sql .= " ".$this->soptions['sign']." '".$this->soptions['pre'].$value.$this->soptions['post']."' ";
				} // END if Doppellaute
				
				// ggf. gruppierung aufheben (wenn mit Umlauten)
				if ($this->soptions['search_umlauts'] && 
					($needles[$k] != $needles_umlaut[$k] || $needles[$k] != $needles_doppel[$k])
					) { $sql .= ") "; }
				
				$komma	 = " ".$this->soptions['operator']." ";
			}
			$sql		.= ") ";
			$gkomma		 = " ".$this->soptions['goperator']." ";
		}
	// ggf. extra SQL anhaengen
		if (!empty($this->soptions['extra'])) {
			$sql = (!empty($sql) ? "(".$sql.") AND ":"").$this->soptions['extra']; //"(".$sql.") AND ".$this->soptions['extra'];
		}
	// OrderBy & Limit!
		if (!empty($orderby))	{ $sql .= " ORDER BY $orderby "; }
		if (!is_null($limit))	{ $sql .= " LIMIT $start, $limit "; }
		$sql = "SELECT ".$this->soptions['select']." FROM `".$table."` WHERE ".$sql;
	// output (nur sql als string oder query ausfuehren)
		return ($this->soptions['passive_mode']) ? $sql : $this->query($sql);
	}

/**
* Diese Funktion sucht mittels MySQL-fulltext-search in den angegebenen kommaseparierten Feldern nach den Suchbegriffen (immer in lowercase!). 
* Optional kann die Sortierung (ohne "ORDER BY") angegeben werden.
* Optional kann die Ausgabe auf $limit Datensetze beschraenkt werden.
* Optional kann die Ausgabe bei Datensatz $start anfangen.
* Standardmaessig werden alle Felder der Tabelle zurueckgegeben. Dies kann jedoch mittels $db->soptions['select'] auf kommaseparierte Felder eingegrenzt werden.
* Zum Testen kann $this->soptions['passive_mode'] auf true gesetzt werden um die query auszugeben aber nicht auszufuehren!
* (die Klassenvariablen ['goperator'], ['operator'], ['sign'], ['pre'], ['post'], ['ignore_case'], ['keep_wordgroups'] sind bei dieser Suche nicht implementiert!)
*
* NOTES: 
* - Die Funktion sucht immer in lowercase. 
* - Der Suchbegriff muss mindestens 4 Buchstaben lang sein.
* - Wenn der Suchbegriff in mehr als 50% der Datensaetze vorkommt, ist das Suchergebnis 0 (leer!).
* - Die zu durschuchenden Felder muessen exakt die sein, die einen fulltext-index haben (und bei zusammengesetzten, genau in dieser Reihenfolge!)
* - Bis 3.23.x gibt es keine Moeglichkeit die Konfiguration zu beeinflussen (ausser neukompilieren); "Mehrwort-Begriffe" werden nicht erkannt.
* (- Ab 4.x kann man Wildcards [*] und Steuerzeichen [+|-|()|<>] einsetzen, um das Suchergebnis zu beeinflussen.)
* Die Suchtreffer werden in "Relevanz"-Reihenfolge ausgegeben: Der Suchbegriff weiter vorn im Text, oder der oefter in einem Datensatz vorkommt ist relevanter.
* (Es wird ausserdem ein zusatzliches Feld "relevance" zurueckgegeben, in dem die Relevanz als float-Wert (0 = keine, 1.0 = grosse Relevanz) dargestellt ist!)
*
* Beispiel:
* <pre><code>
* $db->soptions['select']	= "id";
* $db->blacklist = array("nazi", "ein", "eine", "der", "die", "das");
* $db->fulltext_search("answer_de", $searchstring, "cms_search");
* while($db->fetch_array()) { ... }
* </code></pre>
*
* @access 	public
* @param	mixed	$fields		Array mit Spaltennamen oder kommaseparierte Spaltenliste
* @param	string	$needle		Suchwort/durch Leerzeichen getrennte Wrter
* @param	string	$table		Tabellenname
* @param	string	$orderby	Oderby-Clause (z.b. "name DESC, email") (default: '')
* @param	integer	$limit		Limitiert die Abfrage auf x Datensaetze (default: NULL)
* @param	integer	$start		Beginnt bei y mit der Ausgabe und zeigt x Datensaetze an (default: 0)
* @return	mixed 	result
*/
	public function fulltext_search($fields, $needle, $table, $orderby='', $limit=null, $start=0) {
	// fields
		if (is_array($fields)) { $fields = implode(',', $fields); }
	// needle
		###if (empty($needle)) return false; // <-- UNSINN!
		$needle		= stripslashes($needle); // escape-shlashes entfernen!
		$needle		= str_replace('  ', ' ', trim($needle)); // doppelte leerzeichen durch einfache ersetzen
		// 1. GEHT NICHT: "Mehrwort-Begriffe" erhalten
		// 2. ggf. Blacklist-Filter
		if (is_array($this->blacklist) && count($this->blacklist) > 0) {
			$needles = explode(' ', $needle);
			foreach ($needles as $k => $v) {
				$v		= strToLower($v);
				if (in_array($v, $this->blacklist)) { unset($needles[$k]); } 
			}
			// wieder zu string zusammenfuehren
			$needle		= implode(' ', $needles);
		}
		// 3. ggf. Suche nach Umlaut-Pendents (und umgekehrt)
		if ($this->soptions['search_umlauts']) {
			// nur lowercase, da die fulltext-search sowieso immer in lowercase ausgefuehrt wird...
			if (!is_array($needles)) { $needles	= explode(' ', strToLower($needle)); }
			$umls		= array ('ä','ö','ü','ß');
			$dpls		= array ('ae','oe','ue','ss');
			// doppellaut- und umlaut-arrays bilden
			$needles_doppel	= array();
			$needles_umlaut	= array();
			foreach ($needles as $v) {
				$needles_doppel[] = str_replace($umls, $dpls, $v);
				$needles_umlaut[] = str_replace($dpls, $umls, $v);
			}
			// doppellaut und umlaut arrays zusammenfuehren
			$needles	= array_merge($needles_doppel, $needles_umlaut);
			$needles	= array_unique($needles);	// doppelte loeschen!
			$needle		= implode(' ', $needles);	// wieder zu string zusammenfuehren
		}
	// Feldsuchstring SQL
		$sql = "SELECT ".$this->soptions['select'].", 
						MATCH (".$fields.") AGAINST ('".$needle."') AS relevance 
				FROM `".$table."` 
				WHERE	MATCH (".$fields.") AGAINST ('".$needle."')";
	// ggf. extra SQL anhaengen
		if (!empty($this->soptions['extra'])) {
			$sql .= " AND ".$this->soptions['extra'];
		}
	// OrderBy & Limit!
		if (!empty($orderby))	{ $sql .= " ORDER BY $orderby "; }
		if (!is_null($limit))	{ $sql .= " LIMIT $start, $limit "; }
	// output (nur sql als string oder query ausfuehren)
		return ($this->soptions['passive_mode']) ? $sql : $this->query($sql);
	}

#-----------------------------------------------------------------------------

/**
* diese Funktion erstellt einen string mit einem CREATE-statement um die Struktur einer Table abzubilden (Backup).
*
* @access 	public
* @param	string	$sTable		Tabellenname
* @return	string 	CREATE-statement
*/
	public function make_tbl_stucture_backup($sTable) {
		
		// vars
		if (!$sTable) return;
		$crlf = "\r\n";
		
		// comment
		$sStr =  "#".$crlf;
		$sStr .= "# Structure for table `".$sTable."` [".date("Y-m-d H:i:s")."]".$crlf;
		$sStr .= "#".$crlf;
		
		// build DROP/CREATE TABLE-statement
		$sStr .= 'DROP TABLE IF EXISTS '.$sTable.';'.$crlf;
		$result = $this->query('SHOW CREATE TABLE '.$this->db.'.'.$sTable);
		
		if ($result != FALSE && mysqli_num_rows($result) > 0) {
			$tmpres        = $this->fetch_array(MYSQLI_ASSOC);

			// Fix for case problems with winwin, thanks to
			// Pawe Szczepaski <pauluz at users.sourceforge.net>
			$pos           = strpos($tmpres[1], ' (');
			$tmpres[1]     = substr($tmpres[1], 0, 13)
			               . $tmpres[0]
			               . substr($tmpres[1], $pos);
			$sStr .= trim(str_replace("\n", $crlf, $tmpres['Create Table']));
		}
		
		$this->free_result();

		return $sStr.";".$crlf;
	}

/**
* diese Funktion erstellt einen string mit INSERT-statements um den Inhalt einer Table abzubilden (Backup)
* (fuer nur einen Teil kann man die Variable '$sSqlConstraint' benutzen). 
* ist '$sTblFields' - leer werden alle felder in der '$sTable' benutzt!
*
* @access 	public
* @param	string	$sTable			Tabellenname
* @param	string	$sSqlConstraint	SQL-Einschraenkung im Format " WHERE flag_topnavi='0' ")
* @param	string	$fields			Komma-getrennter String mit Spaltennamen - oder ein leerer String (=> alle Spalten der Tabelle)
* @return	string 	INSERT-statements
*/
	public function make_tbl_content_backup($sTable, $sSqlConstraint='', $sTblFields='') {
		
		// vars
		if (!$sTable) return;
		$crlf = "\r\n";
		$searchValue	= array("\x00", "\x0a", "\x0d", "\x1a"); //\x08\\x09, not required
		$replaceValue	= array('\0', '\n', '\r', '\Z');
		
		// count table-field-names
		if ($sTblFields == '') {
			// get table-field-names
			$aTblFields = $this->get_fields($sTable);
			$sTblFields = '`'.implode("`,`", $aTblFields).'`';
		} else {
			$aTblFields = explode(",", $sTblFields);
		}
		$nTblFldCount = count($aTblFields);
		
		// comment
		$sStr =  "#".$crlf;
		$sStr .= "# Dumping data for table `".$sTable."` [".date("Y-m-d H:i:s")."]".$crlf;
		$sStr .= "#".$crlf;
		
		// get table-content (rows)
		$sQuery = "SELECT ".$sTblFields." FROM `".$sTable."` ".$sSqlConstraint; //." ORDER BY id ASC";
		$aTbl = array();
		$this->query($sQuery);
		while ($r = mysqli_fetch_array($this->result)) {
			$aTbl[] = $r;
		}
		
		// build INSERT-statements (pro reihe)
		foreach ($aTbl as $nr => $row) {
			// set/clear vars
			$field_is_num = array();
			$aTblField = array();
			$aTblValue = array();
			// gehe durch jedes feld
			for ($j = 0; $j < $nTblFldCount; $j++) {
				// ermittle name
				$aTblField[$j] = $this->get_field_name($j);
				// Checks whether the field is an integer or not
				$type = $this->get_field_type($j);
				if ($type=='tinyint' || $type=='smallint' || $type=='mediumint' || $type=='int' || $type=='bigint') {
                    $field_is_num[$j] = true; } else { $field_is_num[$j] = false;
                }
				// values
				if (!isset($row[$j])) {
					$aTblValue[] = 'NULL';
				} else if ($row[$j] == '0' || $row[$j] != '') {
					
					if ($field_is_num[$j]) {
						$aTblValue[] = $row[$j]; // a number
					} else {
						if (!get_magic_quotes_gpc()) {$row[$j] = addslashes($row[$j]);}
						#### aus der phpMyAdmin-function "PMA_sqlAddslashes()" [common.lib.php] in dieser Reihenfolge(!):
						$row[$j] = str_replace('\\', '\\\\', $row[$j]); // backslash: addslashes alleine reicht offenbar nicht!
						$row[$j] = str_replace('\'', '\\\'', $row[$j]); // single-quote: addslashes alleine reicht offenbar nicht!
						$aTblValue[] = "'".str_replace($searchValue, $replaceValue, $row[$j])."'";
					}
				} else {
					$aTblValue[] = "''";
				} // end if
			} // end for
			$sTblValues = implode(",",$aTblValue);
			$sTblFields = '`'.implode("`,`",$aTblField).'`';
			
			// concat INSERT strings
			$sStr .= "INSERT INTO `".$sTable."` (".$sTblFields.") VALUES (".$sTblValues.");".$crlf;
		} // end foreach
		
		return $sStr;
	}

### TODO: RESTORE BACKUP...


/**
* diese Funktion erstellt eine Datei $sFile mit CREATE- und INSERT-statements um die Struktur UND den Inhalt ALLER (oder der als kommasep. String uebergebenen) Tables dieser DB abzubilden (Backup).
* Die Funktion gibt im Erfolgsfall "true", sonst "false" zurueck. Wenn $this->debug gesetzt wurde, wird im Misserfolgsfall die Fehlernummer ausgegeben.
* NOTE: Pfad entweder relativ oder Unix-absolut angeben!
*
* @access 	public
* @param	string	$sFile	Dateiname
* @return	boolean
*/
	public function db_backup_in_file($sFile, $sTables='') { // NOTE: nach -p KEIN Leerzeichen!
		$line = 'mysqldump -u '.$this->user.' -p'.$this->pass.' '.$this->db.' '.$sTables.' > '.$sFile;
		$error_array = array(); $error_nr = 0;
		$error_string = exec($line, $error_array, $error_nr);
		chmod($sFile, 0777);
		if ($error_nr != 0) {
			$e_tables = (!empty($sTables)) ? ': '.$sTables : '';
			trigger_error("Die Datenbank '".$this->db."'".$e_tables." konnte nicht gesichert werden. ".
				"mysqldump Error-Nr: ".$error_nr()." - ".implode(',', $error_array)."!",E_USER_ERROR);
			return false;
		} else {
			return true;
		}
	}

/**
* diese Funktion liest eine Datei $sFile (die von "db_backup_in_file()" erzeugt wurde) wieder ein und stellt somit einen Snapshot wiederher.
* Die Funktion gibt im Erfolgsfall "true", sonst "false" zurueck. Wenn $this->debug gesetzt wurde, wird im Misserfolgsfall die Fehlernummer ausgegeben.
* NOTE: Pfad entweder relativ oder Unix-absolut angeben!
*
* @access 	public
* @param	string	$sFile	Dateiname
* @return	boolean
*/
	public function db_restore_from_file($sFile) {
		#$line = 'mysql --debug=d:t:o,/tmp/mysql.trace '.$this->db.' < '.$sFile;
		#$line = 'mysql --user='.$this->user.' --password='.$this->pass.' '.$this->db.' <  '.$sFile;
		$line = 'mysql --user='.$this->user.' --password='.$this->pass.' -e "source '.$sFile.'" '.$this->db;
		$error_array = array(); $error_nr = 0;
		$error_string = exec($line, $error_array, $error_nr);
		if ($error_nr != 0) {
			trigger_error("Die Datenbank '".$this->db."' konnte nicht wiederhergestellt werden. ".
				"mysqldump Error-Nr: ".$error_nr()." - ".implode(',', $error_array)."!",E_USER_ERROR);
			return false;
		} else {
			return true;
		}
	}

#-----------------------------------------------------------------------------

/**
* diese Funktion maskiert typische MS-Word Sonderzeichen in einem string.
*
* NOTE: die Word-Bindestriche in HTML-Entities umzuwandeln REICHT NICHT! 
* Sie muessen in stumpfe Tastatur-Bindestriche (minus) gewandelt werden, 
* da sonst wirklich seltsame Phaenomene beim PC-IE auftauchen (z.B.: geht 
* das erste hidden-field verloren, wenn man das formular per javascript abschickt...!?!)
*
* @access 	private
* @param	string	$sStr			Zu bearbeitender String
* @return	string 	Bearbeiteter String
* @deprecated version - 2005-05-19
*/
	private function _convert_ms_word_crap($sStr) {
		return $sStr;
		/* deactivated 2005-05-19af: -> scheint mittlerweile kein Problem mehr zu sein!?!
		$str_search = array("", "", "", "", 
							"", "", "", "", "", 
							"", "", "");
		$str_replace = array("&OElig;", "&oelig;", "&aelig", "&AElig;", 
							"-", "-", "-", "&middot;", "&euro;", 
							"&#132;", "&#147;", "&#148;");
		// "&ndash;", "&mdash;", "&shy;" <= HTML-Entities: DAS REICHT NICHT!
		return str_replace($str_search, $str_replace, $sStr);*/
	}

/**
* Schreibt einen SQL Dump in eine Datei
* @param String $sFile
* @param array $aExcludedTables
*/
	public function doPhpDump($sFile,$aExcludedTables = array()) {
		$oFile = new filesystem();
		$aExcludedTables[] = 'ip_country';
		$aTables = $this->get_tables();
		#print_r($aTables);
		for($i=0;!empty($aTables[$i]);$i++) {
			if(!in_array($aTables[$i],$aExcludedTables)) {
				$backupstr .= $this->make_tbl_stucture_backup($aTables[$i]);
				$backupstr .= $this->make_tbl_content_backup($aTables[$i]);
			}
		}
		return $oFile->write_str_in_file($sFile,$backupstr);
	}
/**
* Restore-Version von "doPhpDump()".
* @todo
*/
	public function doRestorePhpDump($sFile) {
		$fo = @fopen($sFile, 'r');
		#
		$aSql = file($sFile);
		echo "asql:";print_r($aSql);	
	}

#-----------------------------------------------------------------------------
} // END of class

?>