<?php
class team extends user { // Class team
/**
* Diese Klasse erweitert die User-Klasse um Moeglichkeiten die Team-Zugehoerigkeit etc. des (aktuellen/uebergebenen) Nutzers abzufragen. 
*
* Example: 
* <pre><code> 
* $oTeam =& new team($oDb);
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.3 / 2006-01-04 [NEU: isUserInTeamOrInAuthor(); isUserInIdChain() geloescht]
* */
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse team:
		----------------------------------------------------------------------------
		konstruktor team(&$oDb, $aConfig=NULL)
		function getValidUserIds($sModule='', $sPriv='view')
		function getValidUserNames($sModule='', $sPriv='view', $userid=null)
		function getTeamUserIds($teamid)
		function isUserInTeam($teamid, $sCurrentUserId=null)
		function isUserInTeamOrInAuthor($teamid=0, $sUserId='', $currentUserId=0)
		function getTeamNames($teamid)
		function getAllTeamNames($bWithUsers=false)
		function countTeams($bWithUsers=false)
		----------------------------------------------------------------------------
		HISTORY:
		1.3 / 2006-01-04 [NEU: isUserInTeamOrInAuthor(); isUserInIdChain() geloescht]
		1.2 / 2006-01-03 [getAllTeamNames() & countTeams() um $bWithUsers ergaenzt]
		1.1 / 2005-03-15 [komplett umgebaut / von PMS getrennt und auf class.user.php aufbauend]
		1.0 / 2005-01-24
	*/

#-----------------------------------------------------------------------------

/**
* @access   private
* @var	 	array	importiertes globales Userdata-Session-Array
*/
	var $Userdata = '';
/**
* @access   private
* @var	 	array	importiertes globales Userdata-Session-Array
*/
	var $aTeam = '';

#-----------------------------------------------------------------------------

/**
* Konstruktor -> Initialisiert das team-Objekt und importiert ein paar Variablen. 
*
* Beispiel: 
* <pre><code> 
* $oTeam =& new team($oDb);
* </code></pre>
*
* @access   public
* @return   void
*/
	function team(&$oDb, $aConfig=NULL) {
		// import global session-userdata
		global $Userdata;
		$this->Userdata = $Userdata;
		if (!is_array($this->Userdata)) die("ERROR: NO Userdata!");
		// Invoke the constructor of the parent.
		parent::user($oDb, $aConfig);
		
		$this->_getTeams();
		
		#$this->oDb		=&  new dbconnect($this->aENV['db']);
		#if (!is_object($this->oDb)) die("ERROR: NO oDb!");
	}

/**
* Hilfsfunktion: Speichert die wesentlichen Felder der Team-Tabelle in einem privaten Array -> um DB-Connects/-Abfragen zu reduzieren.
*
* @access   private
*/
	function _getTeams() {
		// vars
		$buffer = array();
		// ermittle alle Teams
		$this->oDb->query("SELECT `id`, `title`, `user`, `flag_deleted`, `description` FROM ".$this->aENV['table']['sys_team']." WHERE flag_deleted='0' ORDER BY id ASC");
		while ($tmp = $this->oDb->fetch_array()) {
			$buffer[$tmp['id']]['title']		= $tmp['title'];
			$buffer[$tmp['id']]['user']			= $tmp['user'];
			$buffer[$tmp['id']]['description']	= $tmp['description'];
			$buffer[$tmp['id']]['deleted']		= $tmp['flag_deleted'];
		}
		// store array
		$this->aTeam = $buffer;
	}

#-----------------------------------------------------------------------------

/**
* Funktion, die nur die IDs der User ermittelt, die UGs zugeordnet sind, die zumindest 'view'-Privilegien fuer das uebergebene Modul haben. 
* Optional kann ein anderes Mindest-Privileg uebergeben werden.
*
* Beispiel: 
* <pre><code> // alle PMS-User mit 'view'-Privileg
* $aUser = $oTeam->getValidUserIds('pms'); // params: [$sModule='sys'][,$sPriv='view']
* </code></pre>
*
* @access   public
* @param 	string	$sModule	(optional: Modul-key - default: 'sys')
* @param 	string	$sPriv		(optional: Privileg-key - default: 'view')
* @return   array
*/
	function getValidUserIds($sModule='', $sPriv='view') {
		// vars
		$aUg = array();
		$aUser = array();
		
		$sTrim = ($sModule!='') ? "module_key = '".trim($sModule)."' AND" : ""; 
		
		// 1. ermittle ID des pms/view privilegs
		$this->oDb->query("SELECT id 
						FROM ".$this->aENV['table']['sys_privilege']." 
						WHERE ".$sTrim."
							privilege_key = '".trim($sPriv)."'");
		$priv_id = ($this->oDb->num_rows() > 0) ? $this->oDb->fetch_field('id') : '';
		if (empty($priv_id)) return $aUser;#false
		// 2. ermittle UG-ID(s) denen das entsprechende privileg zugewiesen wurde
		$this->oDb->query("SELECT id, permission 
						FROM ".$this->aENV['table']['sys_usergroup']." 
						WHERE permission <> ''");
		while ($tmp = $this->oDb->fetch_array()) {
			$aPerm = (explode(',', $tmp['permission']));	// permissions der UGs teilen
			if (!in_array($priv_id, $aPerm)) continue;		// wenn nicht dabei, ueberspringen
			$aUg[] = $tmp['id'];	// wenn dabei, zum array der interessanten UGs hinzufuegen
		}
		if (count($aUg) == 0) return $aUser;#false
		// 3. ermittle User-ID(s) die den interessanten UGs zugewiesen wurden
		$this->oDb->query("SELECT DISTINCT user_id 
						FROM ".$this->aENV['table']['sys_user_usergroup']." 
						WHERE usergroup_id IN (".implode(',', $aUg).")");
		while ($tmp = $this->oDb->fetch_array()) {
			$aUser[] = $tmp['user_id'];	// zum array der user hinzufuegen
		}
		#if (count($aUser) == 0) return false;
		// output
		return $aUser;
	}

/**
* Funktion, die nur die die Namen der ubergeben oder aller User ermittelt, die UGs zugeordnet sind, die zumindest 'view'-Privilegien fuer das uebergebene Modul haben!
*
* Beispiel: 
* <pre><code> 
* $aUsername = $oTeam->getValidUserNames('pms'); // params: [$sModule='sys'][,$sPriv='view'][,$userid=null] 
* </code></pre>
*
* @access   public
* @param 	string	$sModule	(optional: Modul-key - default: 'sys')
* @param 	string	$sPriv		(optional: Privileg-key - default: 'view')
* @param 	mixed	$userid		(optional: User-IDs als string/array - default: alle gueltigen Modul-Nutzer)
* @return   array
*/
	function getValidUserNames($sModule='', $sPriv='view', $userid=null) {
		// vars
		$buffer = array();
		// default: alle gueltigen Modul-Nutzer ermitteln
		if (is_null($userid)) $userid = $this->getValidUserIds($sModule);
		// output User-Daten
		return parent::getUserNames($userid);
	}

/**
* Gibt die User-IDs der Team-Mitglieder als Array zurueck, deren IDs (kommasepariert oder als flaches Array) uebergeben werden.
*
* Beispiel: 
* <pre><code> 
* $aTeamUserId = $oTeam->getTeamUserIds(['pTeam']); // params: $teamid=null 
* </code></pre>
*
* @access   public
* @param	mixed	$teamid		ID des Teams / IDs der Teams - kommasepariert oder als array
* @return	array	User-ID(s) des/der Teams
*/
	function getTeamUserIds($teamid) {
		// init vars
		$sUserId = '';
		// check vars
		if (empty($teamid)) return;
		if (!is_array($this->aTeam) || count($this->aTeam) == 0) return;
		// ermittle alle User-IDs
		if (!is_array($teamid) && !strpos($teamid, ',')) {
			// 1) wenn $teamid = string (schnelleren Zugriff!)
			$sUserId = $this->aTeam[$teamid]['user'];
		} else {
			// 2) wenn $teamid = array
			if (!is_array($teamid)) $teamid = explode(',', $teamid);
			// durchlaufe dazu $this->aTeam...
			foreach ($this->aTeam as $t_id => $t_val) {
				if (!in_array($t_id, $teamid)) continue;
				$sUserId .= $t_val['user']; // ... Treffer hinzufuegen
			}
		}
		$aUserId = explode(",", $sUserId); // umwandeln in array
		$aUserId = array_unique($aUserId); // doppelte entfernen
		// output array
		return $aUserId;
	}

/**
* Gibt true zurueck, wenn der aktuelle oder uebergebene User Mitglied in einem der uebergebenen Teams ist.
*
* Beispiel: 
* <pre><code> 
* $bIsInTeam = $oTeam->isUserInTeam(['pTeam']); // params: $teamid[,$sCurrentUserId=null] 
* </code></pre>
*
* @access   public
* @param	mixed	$teamid		ID des Teams / IDs der Teams - kommasepariert oder als array
* @param	string	$sCurrentUserId		(optional: ID des Users - default: selbst!)
* @return	boolean	(true, wenn im Team, sonst false)
*/
	function isUserInTeam($teamid, $sCurrentUserId=null) {
		// fehler! @todo: throw Exception => PHP 5
		if (empty($teamid) && empty($sUserId)) return false;
		// welcher user wird geprueft (uebergeben oder selbst?)
		$sCurrentUserId = (is_null($sCurrentUserId)) ? $this->Userdata['id'] : $sCurrentUserId;
		// alle user des/der uebergebenen team(s) ermitteln
		$aTeamUser = (!empty($teamid)) ? $this->getTeamUserIds($teamid) : array();
		// ist er im team?
		return (is_array($aTeamUser) && in_array($sCurrentUserId, $aTeamUser)) ? true : false;
	}
/**
* Gibt true zurueck, wenn der aktuelle oder uebergebene User Mitglied in einem der uebergebenen Teams oder einer der uebergebenen Autoren-Ids ist.
*
* Beispiel: 
* <pre><code> 
* $bIsInAuthor = $oTeam->isUserInTeamOrInAuthor($aData['pTeam'],$aData['pAuthor']); // params: $teamid[,$sUserId=''][,$sCurrentUserId=null] 
* </code></pre>
*
* @access   public
* @param	mixed	$teamid			ID des Teams / IDs der Teams - kommasepariert oder als array
* @param	string	$sUserId		kommaseparierte User-IDs
* @param	string	$currentUserId	(optional: ID des Users - default: selbst!)
* @return	boolean	(true, wenn im Team, sonst false)
*/
	function isUserInTeamOrInAuthor($teamid=0, $sUserId='', $currentUserId=0) {
		// fehler! @todo: throw Exception => PHP 5
		if (empty($sUserId) && $teamid == 0) return false;
		// user (uebergeben oder selbst?)
		$currentUserId = ($currentUserId > 0) ? $currentUserId : $this->Userdata['id'];
		// team
		$a = ($teamid > 0) ? $this->isUserInTeam($teamid, $currentUserId) : false;
		// author
		$b = false;
		if (!empty($sUserId)) {
			$b = Tools::isStringInHaystack($currentUserId, $sUserId);
		}
		return ($b == true || $a == true) ? true : false;
	}


/**
* Gibt die Namen der Teams als Array zurueck, deren IDs (kommasepariert oder als flaches Array) uebergeben werden.
*
* Beispiel: 
* <pre><code> 
* $aTeam = $oTeam->getTeamNames(['pTeam']); // params: $teamid 
* </code></pre>
*
* @access   public
* @param	mixed	$teamid		ID des Teams / IDs der Teams - kommasepariert oder als array
* @return	array	Namen des/der Teams (key: ID, value: Name)
*/
	function getTeamNames($teamid) {
		// vars
		$buffer = array();
		if (empty($teamid)) return array();
		if (!is_array($teamid)) $teamid = explode(",", $teamid);
		// ermittle alle Team-Names (durchlaufe dazu $this->aTeam)
		if (!is_array($this->aTeam) || count($this->aTeam) == 0) return $buffer;
		foreach ($this->aTeam as $t_id => $t_val) {
			if (!in_array($t_id, $teamid)) continue;
			$buffer[$t_id] = $t_val['title'];
		}
		// output array
		return $buffer;
	}

/**
* Gibt die Namen ALLER Teams als Array zurueck.
*
* Beispiel: 
* <pre><code> 
* $aTeam = $oTeam->getAllTeamNames(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(optional: bei true nur Teams MIT usern - default: false!)
* @return	array	Namen aller Teams (key: ID, value: Name)
*/
	function getAllTeamNames($bWithUsers=false) {
		// vars
		$buffer = array();
		// durchlaufe $this->aTeam und vereinfache das array
		if (!is_array($this->aTeam) || count($this->aTeam) == 0) return $buffer;
		foreach ($this->aTeam as $t_id => $t_val) {
			if ($bWithUsers == true && $t_val['user'] == '') continue;
			$buffer[$t_id] = $t_val['title'];
		}
		// output array
		return $buffer;
	}

/**
* Gibt die Anzahl ALLER Teams als Integer zurueck.
*
* Beispiel: 
* <pre><code> 
* echo $oTeam->countTeams(); // params: [$bWithUsers=false] 
* </code></pre>
*
* @access   public
* @param	boolean	$bWithUsers		(optional: bei true nur Teams MIT usern - default: false!)
* @return	int		Anzahl aller Teams
*/
	function countTeams($bWithUsers=false) {
		// vars
		$buffer = array();
		// durchlaufe $this->aTeam und vereinfache das array
		foreach ($this->aTeam as $t_id => $t_val) {
			if ($bWithUsers == true && $t_val['user'] == '') continue;
			$buffer[] = $t_id;
		}
		// output
		return count($buffer);
	}

/**
* Gibt die Description der Teams als Array zurueck, deren IDs (kommasepariert oder als flaches Array) uebergeben werden.
*
* Beispiel: 
* <pre><code> 
* $aTeam = $oTeam->getTeamDescription(pTeam); // params: $teamid 
* </code></pre>
*
* @access   public
* @param	mixed	$teamid		ID des Teams / IDs der Teams - kommasepariert oder als array
* @return	array	Description des/der Teams (key: ID, value: Description)
*/
	function getTeamDescription($teamid) {
		// vars
		$buffer = array();
		if (empty($teamid)) return array();
		if (!is_array($teamid)) $teamid = explode(",", $teamid);
		// ermittle alle Team-Names (durchlaufe dazu $this->aTeam)
		if (!is_array($this->aTeam) || count($this->aTeam) == 0) return $buffer;
		foreach ($this->aTeam as $t_id => $t_val) {
			if (!in_array($t_id, $teamid)) continue;
			$buffer[$t_id] = $t_val['description'];
		}
		// output array
		return $buffer;
	}
	
/**
* Gibt den Deletestatus der Teams als Array zurueck, deren IDs (kommasepariert oder als flaches Array) uebergeben werden.
*
* Beispiel: 
* <pre><code> 
* $aTeam = $oTeam->getTeamFlagDeleted(pTeam); // params: $teamid 
* </code></pre>
*
* @access   public
* @param	mixed	$teamid		ID des Teams / IDs der Teams - kommasepariert oder als array
* @return	array	Deletestatus des/der Teams (key: ID, value: Description)
*/
	function getTeamFlagDeleted($teamid) {
		// vars
		$buffer = array();
		if (empty($teamid)) return array();
		if (!is_array($teamid)) $teamid = explode(",", $teamid);
		// ermittle alle Team-Names (durchlaufe dazu $this->aTeam)
		if (!is_array($this->aTeam) || count($this->aTeam) == 0) return $buffer;
		foreach ($this->aTeam as $t_id => $t_val) {
			if (!in_array($t_id, $teamid)) continue;
			$buffer[$t_id] = $t_val['deleted'];
		}
		// output array
		return $buffer;
	}
#-----------------------------------------------------------------------------

} // END of class
?>