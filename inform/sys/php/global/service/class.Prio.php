<?php
/*
 * Created on 27.03.2006
 *
 */
/**
 * Diese Klasse stellt eine Möglichkeit bereit, für Arraylisten (cs) die Priorisierung zu benutzen.
 * 
 * @uses ArrayTools Elternklasse
 * @author Frederic Hoppenstock <fh@design-aspekt.com>
 * @param array $_GET
 */
	class Prio extends ArrayTools {
		
		var $aGet = null;
		
		function Prio($_GET) {
			
			$this->aGet = $_GET;
			
		}
		
		/**
		 * Führt einen Move der Priorität durch.
		 * 
		 */
		function doMove($sSlideshow) {

			$move = $this->aGet['moveDir'];
			$id = $this->aGet['moveId'];
			if(!empty($move)) {

				switch($move) {
	
					case 'up' :
						$sSlideshow = $this->moveUp($sSlideshow,$id);
						break;
	
					case 'down' :
						$sSlideshow = $this->moveDown($sSlideshow,$id);
						break;
	
					case 'top' :
						$sSlideshow = $this->moveTop($sSlideshow,$id);
						break;
	
					case 'bottom' :
						$sSlideshow = $this->moveBottom($sSlideshow,$id);
						break;
	
				}
				
			}
			
			return $sSlideshow;
		}
		
		/**
		 * Diese Methode schreibt die Richtungspfeile für die Priolisten.
		 * 
		 * @param array $aSlideIds
		 * @param int $inum
		 * @param array $aENV
		 * @param array $aMSG
		 * @param String $syslang
		 * @param String $sGEToptions
		 * @access public
		 * @return String
		 * 
		 */
		function getPrioButton($aSlideIds,$inum,$aENV,$aMSG,$syslang,$sGEToptions='') {
			$anz = count($aSlideIds);
			
			if($inum!=0) {
				$sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=top&moveId=".$inum.$sGEToptions.'" title="'.$aMSG['btn']['prio_top'][$syslang].'">';
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'pf_abstop.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_top'][$syslang].'" border="0">';
				$sStr .= '</a>';
				$sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=up&moveId=".$inum.$sGEToptions.'" title="'.$aMSG['btn']['prio_up'][$syslang].'">';
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'pf_up.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_up'][$syslang].'" border="0">';
				$sStr .= '</a>';
			}
			else {
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'d_pf_abstop.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_top'][$syslang].'" border="0">';
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'d_pf_up.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_up'][$syslang].'" border="0">';
			}
			if($inum!=($anz-1)) {
				$sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=down&moveId=".$inum.$sGEToptions.'" title="'.$aMSG['btn']['prio_dwn'][$syslang].'">';
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'pf_down.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_dwn'][$syslang].'" border="0">';
				$sStr .= '</a>';
				$sStr .= '<a href="'.$_SERVER['PHP_SELF']."?moveDir=bottom&moveId=".$inum.$sGEToptions.'" title="'.$aMSG['btn']['prio_btm'][$syslang].'">';
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'pf_absbottom.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_btm'][$syslang].'" border="0">';
				$sStr .= '</a>';
			}
			else {
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'d_pf_down.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_dwn'][$syslang].'" border="0">';
				$sStr .= '<img src="'.$aENV['path']['pix']['http'].'d_pf_absbottom.gif" width="9" height="9" hspace="1" alt="'.$aMSG['btn']['prio_btm'][$syslang].'" border="0">';
			}
			return $sStr;
		}
		
	}
?>