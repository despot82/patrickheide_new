<?php // Class datetime
/**
* This class provides common datetime-functions in following country-modes (languages): 
*	- [de]		german			(default!) 
*	- [en-gb]	british english 
*	- [en-us]	american english 
*	- [fr]		french 
*
* Example: 
* <pre><code>
* // init
* $oDate = new datetime;
* // schreibe {tagname}, {datum} {uhrzeit} auf deutsch
* $oDate->get_lang();
* echo $oDate->get_dayname().", der ";
* echo $oDate->get_today()." ";
* echo $oDate->get_now($sec=false)." Uhr<br>";
* // schreibe {tagname}, {datum} {uhrzeit} auf englisch
* $oDate->set_lang('en');
* echo $oDate->get_dayname().", ";
* echo $oDate->get_today(false)." ";
* echo $oDate->get_now();
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.8 / 2006-03-13	[NEU: franz. sprachversion]
*/

class da_DateTime {
	
	/*	----------------------------------------------------------------------------
		Funktionen der Klasse datetime:
		----------------------------------------------------------------------------
		constructor datetime()
		function get_lang()
		function set_lang($sLang)
		function get_dayname()
		function get_monthname()
		function get_now($sec=true)
		function get_today($fullyear=true)
		function get_today_long()
		// without country-format:
		function get_php_date($sFormat)
		function get_timestamp()
		function get_isodate()
		function get_isodatetime()
		function get_easter_isodate($year)
		function is_leapyear($year)
		function get_kw($timestamp=NULL)
		function get_mondaykw_timestamp($kw,$jahr)
		function add_days($nDays)
		function add_months($nMonths)
		----------------------------------------------------------------------------
		HISTORY:
		1.8 / 2006-03-13	[NEU: franz. sprachversion]
		1.71 / 2005-03-30	[BUGFIX: immer den 1. des Monats bei "add_months()" zur Berechnung nehmen]
		1.7 / 2005-03-04	[NEU: ueberall "$timestamp" als optionalen param hinzugefuegt]
		1.62 / 2004-09-22	[BUGFIX (nochmal): monthname[de] in "get_today_long()"]
		1.61 / 2004-09-16	[BUGFIX: monthname[de] in "get_today_long()"]
		1.6 / 2004-08-11	[NEU: "add_days()" + "add_months()"; "write_calendar()" entfernt!]
		1.5 / 2004-08-11	[NEU: "$aDaynameFull['en']" + set_timestamp() + alle Methoden (wo's geht) $this->sTimestamp berucksichtigen lassen]
		1.41 / 2004-01-26	[nur kommentare fuer PHPDoc ueberarbeitet ]
		1.4 / 2004-01-21	[NEU: "function get_mondaykw_timestamp()" + "write_calendar()"]
		1.31 / 2004-01-19	["get_isodate($t)" um timestamp ewrweitert]
		1.3 / 2003-10-23	[NEU: "get_easter_isodate($year)" + "is_leapyear($year)"]
		1.2 / 2003-06-13	[NEU: "get_date_from_timestamp($t,$iso=false)"]
		1.11 / 2003-06-13	[nur kommentare fuer PHPDoc ueberarbeitet]
		1.1 / 2003-01-31
	*/
	
	// vars
	var $sLang;
	var $aValidLang	= array('de', 'en-gb', 'en-us', 'fr'); // unterstuetzte sprachversionen
	var $sTimestamp;
	var $aDaynameShort = array(
		"de" 	=> array("So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"),
		"en" 	=> array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"),
		"fr" 	=> array("Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam")
		);
	var $aDaynameFull = array(
		"de" 	=> array("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"),
		"en" 	=> array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"),
		"fr" 	=> array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi")
		);
	var $aMonthnameShort = array(
		"de" 	=> array("00" => "","01" => "Jan","02" => "Feb","03" => "Mär","04" => "Apr","05" => "Mai","06" => "Jun",
						"07" => "Jul","08" => "Aug","09" => "Sep","10" => "Okt","11" => "Nov","12" => "Dez"),
		"en" 	=> array("00" => "","01" => "Jan","02" => "Feb","03" => "Mar","04" => "Apr","05" => "May","06" => "Jun",
						"07" => "Jul","08" => "Aug","09" => "Sep","10" => "Oct","11" => "Nov","12" => "Dec"),
		"fr" 	=> array("00" => "","01" => "Jan","02" => "Fév","03" => "Mars","04" => "Avr","05" => "Mai","06" => "Juin",
						"07" => "Juil","08" => "Août","09" => "Sep","10" => "Oct","11" => "Nov","12" => "Déc")
		);
	var $aMonthnameFull = array(
		"de" 	=> array(	"00" => "",			// No-Month
							"01" => "Januar",
							"02" => "Februar",
							"03" => "März",
							"04" => "April",
							"05" => "Mai",
							"06" => "Juni",
							"07" => "Juli",
							"08" => "August",
							"09" => "September",
							"10" => "Oktober",
							"11" => "November",
							"12" => "Dezember"
						),
		"en" 	=> array(	"00" => "",
							"01" => "January",
							"02" => "February",
							"03" => "March",
							"04" => "April",
							"05" => "May",
							"06" => "June",
							"07" => "July",
							"08" => "August",
							"09" => "September",
							"10" => "October",
							"11" => "November",
							"12" => "December"
						),
		"fr" 	=> array(	"00" => "",
							"01" => "Janvier",
							"02" => "Février",
							"03" => "Mars",
							"04" => "Avril",
							"05" => "Mai",
							"06" => "Juin",
							"07" => "Juillet",
							"08" => "Août",
							"09" => "Septembre",
							"10" => "Octobre",
							"11" => "Novembre",
							"12" => "Décembre"
						)
		);


/**
* Konstruktor -> Initialisiert das datetime-object
*
* Setzt die Standard Laender-Mode-Einstellung ('de').
*
* @access   public
*/
	function da_DateTime() {
		$this->sLang		= "de";
		$this->sTimestamp	= NULL;
	}

#-----------------------------------------------------------------------------

/**
* Zeigt die aktuelle Laender-Mode-Einstellung
*
* Beispiel:
* <pre><code>
* echo $oDate->get_lang();
* </code></pre>
*
* @access   public
* @return	string	[ 'de' | 'en-gb' | 'en-us' | 'fr' ]
*/
	function get_lang() {
		return $this->sLang;
	}

/**
* Setzt die Laender-Mode-Einstellung auf den uebergebenen wert (sofern von dieser klasse unterstuetzt)
*
* Beispiel:
* <pre><code>
* $oDate->set_lang('en');
* $oDate->get_lang();
* $oDate->set_lang('de');
* $oDate->get_lang();
* </code></pre>
*
* @access   public
* @param	string		$Lang	aktuell zu verwendende Sprachversion
*/
	function set_lang($sLang) {
		// check var
		$sLang = strtolower($sLang);
		if ($sLang == 'en') $sLang = 'en-gb';
		if ($sLang == 'us') $sLang = 'en-us';
		if (!in_array($sLang, $this->aValidLang)) {
			trigger_error("FEHLER: Ungueltiges Sprachkuerzel!",E_USER_ERROR);
			return;
		}
		// set
		$this->sLang = $sLang;
	}

/**
* Setzt die Laender-Mode-Einstellung auf deutsch (=default)
* @deprecated version - 13.03.2006 -> NEW: set_lang()
*/
	function set_lang_to_de() {
		$this->sLang = "de";
	}
/**
* Setzt die Laender-Mode-Einstellung auf britisches englisch
* @deprecated version - 13.03.2006 -> NEW: set_lang()
*/
	function set_lang_to_en() {
		$this->sLang = "en-gb";
	}
/**
* Setzt die Laender-Mode-Einstellung auf anerikanisches englisch
* @deprecated version - 13.03.2006 -> NEW: set_lang()
*/
	function set_lang_to_us() {
		$this->sLang = "en-us";
	}


/**
* Setzt die die interne timestamp variable. 
* Dadurch werden alle Methoden (sofern moeglich) die Ausgabe auf diesen timestamp beziehen.
*
* @access   public
* @param	string		$timestamp
*/
	function set_timestamp($timestamp) {
		$this->sTimestamp = $timestamp;
	}

/**
* Loescht die die interne timestamp variable.
* Dadurch werden alle Methoden die Ausgabe auf jetzt und heute beziehen. (default)
*
* @access   public
*/
	function unset_timestamp() {
		$this->sTimestamp = NULL;
	}

/**
* Ueberprueft die uebergebene timestamp variable.
*
* @access   public
* @param	string		$timestamp
* @return	boolean		[true, wenn ok | false, wenn ungueltig PLUS fehlermeldung generieren!]
*/
	function check_timestamp($timestamp) {
		if ($timestamp && $timestamp == -1) {
			trigger_error("FEHLER: Ungueltiger Timestamp!",E_USER_ERROR);
			return false;
		} else {
			return true;
		}
	}

#-----------------------------------------------------------------------------

/**
* Zeigt den aktuellen Tag (Formatierung ist abhngig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* echo $oDate->get_dayname();
* </code></pre>
* 
* @access   public
* @param	string	$timestamp	UNIX-timestamp (optional)
* @param	boolean	$fullname	voller Name oder Kurzname (optional, default: true = voller Name)
* @return	string	Tagname
*/
	function get_dayname($timestamp=NULL, $fullname=true) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		
		$d = date("w", $timestamp);
		if ($fullname) {
			if ($this->sLang == "de")		{ return $this->aDaynameFull['de'][$d]; }
			if ($this->sLang == "en-gb")	{ return $this->aDaynameFull['en'][$d]; }
			if ($this->sLang == "en-us")	{ return $this->aDaynameFull['en'][$d]; }
			if ($this->sLang == "fr")		{ return $this->aDaynameFull['fr'][$d]; }
		} else {
			if ($this->sLang == "de")		{ return $this->aDaynameShort['de'][$d]; }
			if ($this->sLang == "en-gb")	{ return $this->aDaynameShort['en'][$d]; }
			if ($this->sLang == "en-us")	{ return $this->aDaynameShort['en'][$d]; }
			if ($this->sLang == "fr")		{ return $this->aDaynameShort['fr'][$d]; }
		}
	}

/**
* Zeigt das aktuelle Datum (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
*
* Beispiel:
* <pre><code>
* $oDate->get_monthname();
* </code></pre>
*
* @access   public
* @param	string	$timestamp	UNIX-timestamp (optional)
* @param	boolean	$fullname	voller Name oder Kurzname (optional, default: true = voller Name)
* @return	string	Monatname
*/
	function get_monthname($timestamp=NULL, $fullname=true) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		
		$m = date("m", $timestamp);
		if ($fullname) {
			if ($this->sLang == "de")		{ return $this->aMonthnameFull['de'][$m]; }
			if ($this->sLang == "en-gb")	{ return date("F", $timestamp); }
			if ($this->sLang == "en-us")	{ return date("F", $timestamp); }
			if ($this->sLang == "fr")		{ return $this->aMonthnameFull['fr'][$m]; }
		} else {
			if ($this->sLang == "de")		{ return $this->aMonthnameShort['de'][$m]; }
			if ($this->sLang == "en-gb")	{ return date("M", $timestamp); }
			if ($this->sLang == "en-us")	{ return date("M", $timestamp); }
			if ($this->sLang == "fr")		{ return $this->aMonthnameShort['fr'][$m]; }
		}
	}


#-----------------------------------------------------------------------------

/**
* Zeigt die aktuelle Zeit (Formatierung ist abhaengig von der Laendereinstellung - Default = de) 
* (auf Wunsch auch ohne Sekunden durch die Angabe 'false')
*
* Beispiel:
* <pre><code>
* echo $oDate->get_now(false);	// z.B.: 10:33
* echo $oDate->get_now();		// z.B.: 10:33:07
* </code></pre>
*
* @access   public
* @param	boolean	$sec	(optional: false, wenn mann keine Sekunden geliefert bekommen mchte)
* @param	string	$timestamp	UNIX-timestamp (optional)
* @return	string	aktuelle Zeit
*/
	function get_now($sec=true, $timestamp=NULL) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		
		$now = ($this->sLang == "de" || $this->sLang == "fr") 
			? date("H:i", $timestamp) 
			: date("h:i", $timestamp);
		if ($sec == true) { $now .= date(":s", $timestamp); }
		
		return $now;
	}

/**
* Zeigt das aktuelle Datum (Formatierung ist abhaengig von der Laendereinstellung - Default = de) 
* (auf Wunsch auch mit zweistelligem Jahr durch die Angabe 'false' im ersten Parameter)
* (auf Wunsch auch mit vorangestelltem Wochentagsnamen durch die Angabe 'true' im dritten Parameter)
*
* Beispiel:
* <pre><code>
* $oDate->get_today(false);	// z.B.: 01.01.03
* $oDate->get_today();		// z.B.: 01.01.2003
* $oDate->get_today(true, null, true);	// z.B.: Mo, 01.01.2003
* </code></pre>
*
* @access   public
* @param	boolean	$fullyear	(optional: false, wenn mann ein kurzes (zweistelliges) Jahr geliefert bekommen mchte)
* @param	string	$timestamp	UNIX-timestamp (optional)
* @param	boolean	$bWithWeekdayname	(optional, default: false)
* @return	string	aktuelles Datum
*/
	function get_today($fullyear=true, $timestamp=NULL, $bWithWeekdayname=false) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		$weekdayname = ($bWithWeekdayname) ? $this->get_dayname($timestamp, false).', ' : '';
		
		$y = ($fullyear == true) ? date("Y", $timestamp) : date("y", $timestamp);
		if ($this->sLang == "de")		{ $today = date("d.m.", $timestamp).$y; }
		if ($this->sLang == "en-gb")	{ $today = date("d.m.", $timestamp).$y; }
		if ($this->sLang == "en-us")	{ $today = date("m.d.", $timestamp).$y; }
		if ($this->sLang == "fr")		{ $today = date("d/m/", $timestamp).$y; }
		
		return $weekdayname.$today;
	}

/**
* Zeigt das aktuelle Datum mit ausgeschriebenem Monatsnamen und Tag ohne fuehrende Null 
* (Formatierung ist abhaengig von der Laendereinstellung - Default = de)
* (auf Wunsch auch mit vorangestelltem Wochentagsnamen durch die Angabe 'true' im zweiten Parameter)
*
* Beispiel:
* <pre><code>
* $oDate->get_today_long();
* </code></pre>
*
* @access   public
* @param	string	$timestamp	UNIX-timestamp (optional)
* @param	boolean	$bWithWeekdayname	(optional, default: false)
* @return	string	aktuelles Datum (mit Monatsname)
*/
	function get_today_long($timestamp=NULL, $bWithWeekdayname=false) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		$weekdayname = ($bWithWeekdayname) ? $this->get_dayname($timestamp, false).', ' : '';
		
		if ($this->sLang == "de") {
			$m = date("m", $timestamp);
			$today = date("j. ", $timestamp).$this->aMonthnameFull['de'][$m].date(" Y", $timestamp);
		}
		if ($this->sLang == "en-gb") {
			$today = date("j F Y", $timestamp);
		}
		if ($this->sLang == "en-us") {
			$today = date("F j, Y", $timestamp);
		}
		if ($this->sLang == "fr") {
			$m = date("m", $timestamp);
			$today = date("j ", $timestamp).$this->aMonthnameFull['fr'][$m].date(" Y", $timestamp);
		}
		
		return $weekdayname.$today;
	}


#-----------------------------------------------------------------------------
# following functions don't worry about country-format:
#-----------------------------------------------------------------------------

/**
* Schreibt die aktuelle Zeit oder Datum mit der Mglichkeit die Formatierung zu bestimmen
*
* Beispiel:
* <pre><code>
* echo $oDate->get_php_date("G:i:s");
* </code></pre>
*
* Beschreibung: 
* (default ist das ISO-datetime-Format "Y-m-d H:i:s" - also YYYY-MM-TT HH:MM:SS (HH im 24 Std.-Modus mit fhrenden Nullen))
*
* Folgende Formatierungen drfen verwendet werden:
*	a - "am" oder "pm" ("Vormittag" oder "Nachmittag")
*	A - "AM" oder "PM"
*	B - Swatch Internet-Zeit(1000 Beats in 24 h; Mitternacht in Biel = 000 Beats)
*	d - Tag des Monats (z.B. "09")
*	j - Tag des Monats ohne fhrende Null (z.B. "9")
*	t - Anzahl der Tage im Monat (z.B. "30")
*	z - Tag seit dem Jahresbeginn (z.B. "156")
*	w - Nummerischer Wochentag (von "0" fr Sonntag bis "6" fr Samstag)
*	D - Abkrzung des Wochentages(z.B. "Thu")
*	l - Vollstndiger Wochentag (z.B. "Thursday")
*	F - vollstndiger Monatsname (z.B. "December")
*	M - Abkrzung des Monatnamen (z.B. "Dec")
*	m - Nummerische Monatsangabe (z.B. "05")
*	n - Nummerische Monatsangabe ohne fhrende Null (z.B. "5")
*	h - Stunde in 12-Stunden Formatierung (z.B. "05")
*	H - Stunde in 24-Stunden Formatierung (z.B. "18")
*	g - Stunde in 12-Stunden Formatierung ohne fhrende Null (z.B. "5")
*	G - Stunde in 24-Stunden Formatierung ohne fhrende Null (z.B. "7")
*	i - Minute (z.B. "07")
*	U - Vergangene Sekunden seit dem 01.01.1970 (z.B. "4890729")
*	s - Sekunden (z.B. "02")
*	S - Englische Ordnungszahl (z.B. "th", "nd")
*	L - Schaltjahr. Ausgabe entweder "0" oder "1"
*	Y - 4-stellige Jahreszahl (z.B. "1999")
*	y - 2-stellige Jahreszahl (z.B. "99")
*	Z - Zeitabweichung von der GMT (von "-43200" bis "43200")
*
* @access   public
* @param	string	$sFormat	Formatierungsangabe
* @param	string	$timestamp	UNIX-timestamp (optional)
* @return	string	aktuelles Datum/Uhrzeit (je nach Formatierungsanweisung)
*/
	function get_php_date($sFormat="Y-m-d H:i:s", $timestamp=NULL) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		
		return date("$sFormat", $timestamp);
	}


#-----------------------------------------------------------------------------

/**
* Erstellt eine Unix-Timestamp fr die aktuelle Uhrzeit. Eine Unix-Timestamp zhlt die Anzahl der Sekunden seit dem Beginn der Unix-Epoche (01.01.1970).
*
* Beispiel:
* <pre><code>
* echo $oDate->get_timestamp();
* </code></pre>
*
* @access   public
* @return	string	aktueller Zeitstempel (UNIX-timestamp)
*/
	function get_timestamp() {
		return time();
	}

/**
* BITTE NICHT MEHR VERWENDEN!
* get_isodate() ist aktueller!
*/
	function date_from_timestamp($t, $iso=false) {
		return $this->get_isodate($t);
	}

/**
* Erstellt ein Datum im ISO-Format (z.B. fr eine Datenbankeingabe)
* ODER wenn als optionaler Parameter $t ein UNIX-TIMESTAMP uebergeben wird,
* wird eben dieses in ein ISO-Datum formatiert.
*
* Beispiel:
* <pre><code>
* // generate ISO-date (optional: out of UNIX-timestamp)
* echo $oDate->get_isodate(); // param: [$timestamp]
* </code></pre>
*
* @access   public
* @param	string	$timestamp	UNIX-timestamp (optional)
* @return	string	aktuelles ISO-Datum
*/
	function get_isodate($timestamp=NULL) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		return date("Y-m-d", $timestamp);
	}

/**
* Erstellt eine DatumZeit im ISO-Format (z.B. fr eine Datenbankeingabe)
* ODER wenn als optionaler Parameter $t ein UNIX-TIMESTAMP uebergeben wird,
* wird eben dieses in ein ISO-Datetime formatiert.
*
* Beispiel:
* <pre><code>
* echo $oDate->get_isodatetime(); // param: [$timestamp]
* </code></pre>
*
* @access   public
* @param	string	$timestamp	UNIX-timestamp (optional)
* @return	string	aktuelles ISO-DatumZeit
*/
	function get_isodatetime($timestamp=NULL) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		return date("Y-m-d H:i:s", $timestamp);
	}

# ----------------------------------------------------------------- FEIERTAGE

/**
* Gibt das Datum auf das Ostern (en: Easter, fr: Pâques) faellt im ISO-Format zurueck, wenn man eine Jahreszahl uebergibt.
*
* Beispiel:
* <pre><code>
* echo $oDate->get_easter_isodate("2006"); // param: [$year]
* </code></pre>
*
* @access   public
* @param	string	$year	Jahreszahl (4stellig)
* @return	string	Oster-Datum im ISO-Format
*/
	function get_easter_isodate($year) {
		return date("Y-m-d", easter_date($year));
	}

# ----------------------------------------------------------------- SPECIAL

/**
* Gibt bei Uebergabe einer Jahreszahl true zurueck, wenn diese ein Schaltjahr ist (false, wenn nicht).
* (Alle durch 4 teilbaren Jahre sind Schaltjahre, ausser sie sind durch 100 teilbar (ausnahme: doch, wenn durch 400 teilbar)!)
*
* Beispiel:
* <pre><code>
* echo ($oDate->is_leapyear("2006")) ? "Schaltjahr!" : "";
* </code></pre>
*
* @access   public
* @param	string	$year	Jahreszahl (4stellig)
* @return	boolean	(true, wenn Schaltjahr - false, wenn nicht)
*/
	function is_leapyear($year) {
		return ($year % 4 == 0 && ($year % 100 != 0 || $year % 400 == 0));
	}

/**
* Gibt die aktuelle Kalenderwoche (oder optional die des uebergebenen UNIX-timestamp) im ISO-Format zurueck. 
*
* Zur Theorie: die 1. Kalenderwoche eines Jahres ist in ISO8601 als die Kalenderwoche definiert, die den ersten Donnerstag des Jahres enthlt.
*
* Beispiel:
* <pre><code>
* echo $oDate->get_kw(); // params: [$timestamp]
* </code></pre>
*
* @access   public
* @param	string	$timestamp	UNIX-timestamp (optional)
* @return	string	Oster-Datum im ISO-Format
*/
	function get_kw($timestamp=NULL) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return '';
		if (!$timestamp) { $timestamp = time(); }
		return date('W', $timestamp);
	}

/**
* Hilfsfunktion: Gibt bei Uebergabe einer Jahreszahl den Zeitstempel des Montages der ersten Kalenderwoche als UNIX-timestamp zurueck.
*
* @access   private
* @param	string	$year	Jahreszahl (4stellig)
* @return	string	Zeitstempel des Montages der ersten KW (UNIX-timestamp)
*/
	function get_firstmondaykw($year) {
	    $first	= mktime(0, 0, 0, 1, 1, $year);
	    $wday	= date('w', $first);
	    if ($wday <= 4) {
	        // Donnerstag oder kleiner: auf den Montag zurckrechnen.
	        $monday = mktime(0, 0, 0, 1, 1 - ($wday - 1), $year);
	    } else {
	        // auf den Montag nach vorne rechnen.
	        $monday = mktime(0, 0, 0, 1, 1 + (7 - $wday + 1), $year);
	    }
	    return $monday;
	}

/**
* Gibt bei den Zeitstempel des Montages der uebergebenen Kalenderwoche des uebergebenen Jahres zurueck.
*
* Beispiel:
* <pre><code>
* echo $oDate->get_mondaykw_timestamp("14", "2006"); // params: [$kw, $jahr]
* </code></pre>
*
* @access   private
* @param	string	$year	Jahreszahl (4stellig)
* @return	string	Zeitstempel des Montages der ersten KW (UNIX-timestamp)
*/
	function get_mondaykw_timestamp($kw, $year) {
	    //  ermittle den Montag der 1. KW des gegebenen Jahres ...
	    $firstmonday	= $this->get_firstmondaykw($year);
	    $mon_month		= date('m', $firstmonday);
	    $mon_year		= date('Y', $firstmonday);
	    $mon_day		= date('d', $firstmonday);
		// ... und addiere ($kw-1)*7 Tage darauf
	    $days 			= ($kw - 1) * 7;
		// bei Datumsberechnungen aller Art liefert mktime() einen groen Nutzen! Man kann z.B. zum 1.1.2004 
		// einfach 178 Tage drauf rechnen, und landet im richtigen Monat im richtigen Jahr. 
		// Auf Basis des Timestamps, den mondaykw() zurckliefert, koennte man nun einen Kalender 
		// fr die aktuell (z.B. per $_GET bergebene) ausgewhlte Kalenderwoche aufbauen.
	    $mondaykw		= mktime(0, 0, 0, $mon_month, $mon_day + $days, $mon_year);
	    return $mondaykw;
	}

#-----------------------------------------------------------------------------

/**
* Addiert (oder subtrahiert bei Uebergabe einer negativen Zahl) die uebergebene Anzahl Tage und gibt das errechnete Datum als Timestamp zurueck.
* (Anmerkung PHP.net: mktime(): Dieses Vorgehen kann zu verlaesslicheren Ergebnissen fuehren, als simples addieren oder subtrahieren der Anzahl von Sekunden in Tagen oder Monaten zu einem Timestamp, da Sommer- und Winterzeit beruecksichtigt werden.)
*
* Beispiel:
* <pre><code>
* echo $prev_day_ts = $oDate->add_days(-1); // params: $nDays
* echo $next_day_ts = $oDate->add_days(1); // params: $nDays
* </code></pre>
*
* @access   public
* @param	string	$nDays	Anzahl Tage die addiert oder subtrahiert werden sollen
* @param	string	$timestamp	UNIX-timestamp (optional)
* @return	string	Timestamp des errechneten Datums
*/
	function add_days($nDays, $timestamp=NULL) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return -1;
		if (!$timestamp) { $timestamp = time(); }
		$day = date("d", $timestamp);
		$day = ($day + $nDays);
		return mktime(0, 0, 0, date("m", $timestamp), $day, date("Y", $timestamp));
	}

/**
* Addiert (oder subtrahiert bei Uebergabe einer negativen Zahl) die uebergebene Anzahl Monate und gibt das errechnete Datum als Timestamp zurueck.
* (Analog zu "add_days($nDays)")
*
* @access   public
* @param	string	$nMonths	Anzahl Monate die addiert oder subtrahiert werden sollen
* @param	string	$timestamp	UNIX-timestamp (optional)
* @return	string	Timestamp des errechneten Datums
*/
	function add_months($nMonths, $timestamp=NULL) {
		if (!$timestamp) { $timestamp = $this->sTimestamp; }
		if (!$this->check_timestamp($timestamp)) return -1;
		if (!$timestamp) { $timestamp = time(); }
		$month = date("m", $timestamp);
		$month = ($month + $nMonths);
		return mktime(0, 0, 0, $month, 1, date("Y", $timestamp)); // immer den 1. des Monats einsetzen!!!
	}

#-----------------------------------------------------------------------------
} // END of class
?>