<?php
/**				***** CAL *****
*
* mbl_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.0 / 2006-03-10
*/

// Fehlermeldungen
	// ... sind alle in sys...

// MBL
	$aMSG['mbl']['memo']['de'] = "Memos";
	$aMSG['mbl']['memo']['en'] = "Memos";
	$aMSG['mbl']['adress']['de'] = "Addressen";
	$aMSG['mbl']['adress']['en'] = "Adresses";
	$aMSG['mbl']['calendar']['de'] = "Kalendar";
	$aMSG['mbl']['calendar']['en'] = "Calendar";
	$aMSG['mbl']['more']['de'] = "mehr";
	$aMSG['mbl']['more']['en'] = "more";
	$aMSG['mbl']['delete']['de'] = "löschen";
	$aMSG['mbl']['delete']['en'] = "delete";
	$aMSG['mbl']['new_memo']['de'] = "Neues Memo";
	$aMSG['mbl']['new_memo']['en'] = "New memo";
	$aMSG['mbl']['edit_memo']['de'] = "Memo bearbeiten";
	$aMSG['mbl']['edit_memo']['en'] = "Edit memo";
	$aMSG['mbl']['overview']['de'] = "Übersicht";
	$aMSG['mbl']['overview']['en'] = "Overview";
	$aMSG['mbl']['new_event']['de'] = "Neuer Termin";
	$aMSG['mbl']['new_event']['en'] = "New event";
	$aMSG['mbl']['edit_event']['de'] = "Termin bearbeiten";
	$aMSG['mbl']['edit_event']['en'] = "Edit event";
	$aMSG['mbl']['month_overview']['de'] = "Monatsansicht";
	$aMSG['mbl']['month_overview']['en'] = "Month view";
	$aMSG['mbl']['day_overview']['de'] = "Tagesansicht";
	$aMSG['mbl']['day_overview']['en'] = "Day view";
	$aMSG['mbl']['tomorrow']['de'] = "morgen";
	$aMSG['mbl']['tomorrow']['en'] = "tomorrow";
	$aMSG['mbl']['yesterday']['de'] = "gestern";
	$aMSG['mbl']['yesterday']['en'] = "yesterday";
	$aMSG['mbl']['sys']['de'] = "Systemnutzer";
	$aMSG['mbl']['sys']['en'] = "System user";
	
// Formulare
	$aMSG['form']['subject']['de'] = "Betreff";
	$aMSG['form']['subject']['en'] = "Subject";
	
	
// Button-Beschriftungen
	$aMSG['btn']['notify_users']['de'] = "Teilnehmer benachrichtigen";
	$aMSG['btn']['notify_users']['en'] = "Notify participants";

?>