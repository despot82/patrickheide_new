<?php
/**				***** ADM *****
*
* adm_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.1 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	2.0 / 2004-04-27 (v2)
* #history	1.0 intranet
*/

	$aMSG['err']['confirm_delete_invoice']['de'] = "Wollen Sie diese Rechnung löschen? \\nZugeordnete Aufwände und Kosten werden wieder als offene Posten gebucht.";
	$aMSG['err']['confirm_delete_invoice']['en'] = "Do you want to delete this invoice? \\nThe assigned time sheet entries and expenses will be reset to the open positions.";
	$aMSG['err']['confirm_duplicate_invoice']['de'] = "Wollen Sie diese Rechnung duplizieren? \\nEinmal erzeugte Rechnungen können nicht wieder gelöscht werden.";
	$aMSG['err']['confirm_duplicate_invoice']['en'] = "Do you want to duplicate this invoice? \\nExisting invoices cannot be deleted.";
	$aMSG['err']['confirm_dunning_letter']['de'] = "Wollen Sie diese Rechnung mit einer Mahnung versehen?";
	$aMSG['err']['confirm_dunning_letter']['en'] = "Do you want to create a dunning letter for this invoice?";
	$aMSG['err']['confirm_clear_prjstate']['de'] = "Wollen sie den Projekt-Status auf abgeschlossen setzen?";
	$aMSG['err']['confirm_clear_prjstate']['en'] = "Do you want to change the project state to cleared?";
	

	$aMSG['adm']['timesheet']['de'] = "Time sheet";
	$aMSG['adm']['timesheet']['en'] = "Time sheet";
	$aMSG['adm']['proposal']['de'] = "Angebot";
	$aMSG['adm']['proposal']['en'] = "Proposal";
	$aMSG['adm']['proposaldetails']['de'] = "Angebot";
	$aMSG['adm']['proposaldetails']['en'] = "Proposal";
	$aMSG['adm']['open']['de'] = "Offen";
	$aMSG['adm']['open']['en'] = "Open";
	$aMSG['adm']['cleared']['de'] = "Abgerechnet";
	$aMSG['adm']['cleared']['en'] = "Cleared";
	$aMSG['adm']['cleared_ts']['de'] = "Abgerechnete Aufwände";
	$aMSG['adm']['cleared_ts']['en'] = "Cleared time sheets";
	$aMSG['adm']['cleared_exp']['de'] = "Abgerechnete Fremdkosten";
	$aMSG['adm']['cleared_exp']['en'] = "Cleared expenses";
	$aMSG['adm']['cleared_ts_ni']['de'] = "Abgerechnete Aufwände";
	$aMSG['adm']['cleared_ts_ni']['en'] = "Cleared time sheets";
	$aMSG['adm']['cleared_exp_ni']['de'] = "Abgerechnete Fremdkosten";
	$aMSG['adm']['cleared_exp_ni']['en'] = "Cleared expenses";
	$aMSG['adm']['open_items']['de'] = "Offene Posten";
	$aMSG['adm']['open_items']['en'] = "Open entries";
	$aMSG['adm']['project_statistic']['de'] = "Projektstatistik";
	$aMSG['adm']['project_statistic']['en'] = "Project statistic";
	
	$aMSG['adm']['project']['de'] = "Projekt";
	$aMSG['adm']['project']['en'] = "Project";
	$aMSG['adm']['newproject']['de'] = "Neues Projekt";
	$aMSG['adm']['newproject']['en'] = "New project";
	$aMSG['adm']['projectname']['de'] = "Projektname";
	$aMSG['adm']['projectname']['en'] = "Project name";
	$aMSG['adm']['project_meta']['de'] = "Allgemein";
	$aMSG['adm']['project_meta']['en'] = "Global";
	$aMSG['adm']['count_prj']['de'] = "Projekte";
	$aMSG['adm']['count_prj']['en'] = "Projects";
	$aMSG['adm']['client']['de'] = "Kunde";
	$aMSG['adm']['client']['en'] = "Client";
	$aMSG['adm']['client_nr']['de'] = "Kundennr.";
	$aMSG['adm']['client_nr']['en'] = "Client no.";
	$aMSG['adm']['prj_status']['de'] = "Status";
	$aMSG['adm']['prj_status']['en'] = "Status";
	$aMSG['adm']['prj_nr']['de'] = "Prj. Nr.";
	$aMSG['adm']['prj_nr']['en'] = "Prj. nr.";
	$aMSG['adm']['contact_person']['de'] = "Projektleitung";
	$aMSG['adm']['contact_person']['en'] = "Project management";
	$aMSG['adm']['po_number']['de'] = "PO-Nummer";
	$aMSG['adm']['po_number']['en'] = "PO number";
	
	$aMSG['adm']['hours']['de'] = " Std.";
	$aMSG['adm']['hours']['en'] = " Hrs.";
	$aMSG['adm']['days']['de'] = " MT";
	$aMSG['adm']['days']['en'] = " Days";
	
	$aMSG['adm']['proposaltext1']['de'] = "Einleitung";
	$aMSG['adm']['proposaltext1']['en'] = "Introduction";
	$aMSG['adm']['proposaltext2']['de'] = "Resume";
	$aMSG['adm']['proposaltext2']['en'] = "Resume";

	$aMSG['adm']['proposaldet']['de'] = "Angebotdetails";
	$aMSG['adm']['proposaldet']['en'] = "Budget summary";
	$aMSG['adm']['proposalauthor']['de'] = "Dieses Angebot wurde erstellt von ";
	$aMSG['adm']['proposalauthor']['en'] = "This estimate was prepared by ";
	$aMSG['adm']['invoiceauthor']['de'] = "Diese Rechnung wurde erstellt von ";
	$aMSG['adm']['invoiceauthor']['en'] = "This invoice was prepared by ";
	$aMSG['adm']['invoicedet']['de'] = "Rechnungsdetails";
	$aMSG['adm']['invoicedet']['en'] = "Invoice summary";
	$aMSG['adm']['newinvoice']['de'] = "Neue Rechnung";
	$aMSG['adm']['newinvoice']['en'] = "New invoice";
	$aMSG['adm']['replace']['de'] = "Ersetzen";
	$aMSG['adm']['replace']['en'] = "Replace";
	$aMSG['adm']['select_client']['de'] = "Bitte zuerst einen Kunden auswählen.";
	$aMSG['adm']['select_client']['en'] = "Please select a client.";
	$aMSG['adm']['expense_cat']['de'] = "Art";
	$aMSG['adm']['expense_cat']['en'] = "Type";
	$aMSG['adm']['job_cat']['de'] = "Phase";
	$aMSG['adm']['job_cat']['en'] = "Phase";
	$aMSG['adm']['quantity']['de'] = "Aufwand";
	$aMSG['adm']['quantity']['en'] = "Time";
	$aMSG['adm']['fees']['de'] = "Honorar";
	$aMSG['adm']['fees']['en'] = "Fees";
	$aMSG['adm']['allfees']['de'] = "Honorare";
	$aMSG['adm']['allfees']['en'] = "Fees";
	$aMSG['adm']['totalfees']['de'] = "Gesamthonorar";
	$aMSG['adm']['totalfees']['en'] = "Fees";
	$aMSG['adm']['cost']['de'] = "Kosten";
	$aMSG['adm']['cost']['en'] = "Cost";
	$aMSG['adm']['currency']['de'] = "Währung";
	$aMSG['adm']['currency']['en'] = "Currency";
	$aMSG['adm']['tax']['de'] = "MwSt.";
	$aMSG['adm']['tax']['en'] = "VAT";
	$aMSG['adm']['discount']['de'] = "Rabatt";
	$aMSG['adm']['discount']['en'] = "Discount";
	$aMSG['adm']['incl']['de'] = "inkl.";
	$aMSG['adm']['incl']['en'] = "incl.";
	$aMSG['adm']['price']['de'] = "Preis";
	$aMSG['adm']['price']['en'] = "Price";
	$aMSG['adm']['flag_extern']['de'] = "Externe Kosten";
	$aMSG['adm']['flag_extern']['en'] = "Costs";
	$aMSG['adm']['price_net']['de'] = "Netto Betrag";
	$aMSG['adm']['price_net']['en'] = "Net cash";
	$aMSG['adm']['price_gross']['de'] = "Brutto Betrag";
	$aMSG['adm']['price_gross']['en'] = "Gross";
	$aMSG['adm']['total']['de'] = "Gesamt";
	$aMSG['adm']['total']['en'] = "Total";
	$aMSG['adm']['handling']['de'] = "Handling";
	$aMSG['adm']['handling']['en'] = "Handling";
	$aMSG['adm']['price_final']['de'] = "Gesamt";
	$aMSG['adm']['price_final']['en'] = "Total";
	$aMSG['adm']['sum']['de'] = "Summe";
	$aMSG['adm']['sum']['en'] = "Sum";
	$aMSG['adm']['estimate']['de'] = "Gesamt";
	$aMSG['adm']['estimate']['en'] = "Estimate";
	$aMSG['adm']['total']['de'] = "Gesamt";
	$aMSG['adm']['total']['en'] = "Total";
	$aMSG['adm']['last_expenses']['de'] = "Deine letzten 5 Einträge";
	$aMSG['adm']['last_expenses']['en'] = "Your last 5 entries";
	
	$aMSG['adm']['perhour']['de'] = "Satz";
	$aMSG['adm']['perhour']['en'] = "Rate";
	$aMSG['adm']['valid_from']['de'] = "Gültig";
	$aMSG['adm']['valid_from']['en'] = "Valid from";
	$aMSG['adm']['all']['de'] = "Insgesamt";
	$aMSG['adm']['all']['en'] = "All";
	$aMSG['adm']['permonth']['de'] = "Monat";
	$aMSG['adm']['permonth']['en'] = "Month";
	
	$aMSG['adm']['createdby']['de'] = "Eingetragen von";
	$aMSG['adm']['createdby']['en'] = "Created by";
	
	$aMSG['adm']['validdate']['de'] = "Bitte geben Sie ein gültiges Datum ein.";
	$aMSG['adm']['validdate']['en'] = "Please enter a valid date.";
	$aMSG['adm']['validrate']['de'] = "Bitte geben Sie einen Betrag ein.";
	$aMSG['adm']['validrate']['en'] = "Please enter a rate.";

	$aMSG['adm']['site']['de'] = "Seite";
	$aMSG['adm']['site']['en'] = "Page";

	$aMSG['adm']['invoice']['de'] = "Rechnung";
	$aMSG['adm']['invoice']['en'] = "Invoice";
	$aMSG['adm']['invoices']['de'] = $aMSG['adm']['invoice']['de'].'en';
	$aMSG['adm']['invoices']['en'] = $aMSG['adm']['invoice']['en'].'s';
	$aMSG['adm']['create']['de'] = "Erzeugen";
	$aMSG['adm']['create']['en'] = "Create";
	$aMSG['adm']['invoice_overview']['de'] = 'Rechnungsübersicht';
	$aMSG['adm']['invoice_overview']['en'] = 'Invoice overview';
	$aMSG['adm']['notavail']['de'] = 'n.a.';
	$aMSG['adm']['notavail']['en'] = 'n.a.';
	$aMSG['adm']['stornotext']['de'] = 'Stornotext';
	$aMSG['adm']['stornotext']['en'] = 'Cancellation text';
	$aMSG['adm']['cancelled']['de'] = 'Storniert';
	$aMSG['adm']['cancelled']['en'] = 'Cancelled';
	$aMSG['adm']['cancelledin']['de'] = 'Storniert in Beleg ';
	$aMSG['adm']['cancelledin']['en'] = 'Cancelled in invoice ';
	$aMSG['adm']['cancelledinvoice']['de'] = 'Stornorechnung';
	$aMSG['adm']['cancelledinvoice']['en'] = 'Credit note';
	$aMSG['adm']['cancelledinvoiceto']['de'] = $aMSG['adm']['cancelledinvoice']['de'].' zur Rechnung';
	$aMSG['adm']['cancelledinvoiceto']['en'] = $aMSG['adm']['cancelledinvoice']['en'].' to invoice';
	$aMSG['adm']['invoice_nr']['de'] = 'Rechnungs-Nr.';
	$aMSG['adm']['invoice_nr']['en'] = 'Invoice no.';
	$aMSG['adm']['close_invoice']['de'] = "Abschließen";
	$aMSG['adm']['close_invoice']['en'] = "Close";
	$aMSG['adm']['save_in_invoice']['de'] = 'in Rechnung Nummer';
	$aMSG['adm']['save_in_invoice']['en'] = 'in invoice no.';
	$aMSG['adm']['check_in_invoice']['de'] = 'In Rechnungnummer';
	$aMSG['adm']['check_in_invoice']['en'] = 'In invoice no.';
	$aMSG['adm']['not_closed']['de'] = 'nicht abgeschlossen';
	$aMSG['adm']['not_closed']['en'] = 'not closed';
	$aMSG['adm']['closed']['de'] = 'abgeschlossen';
	$aMSG['adm']['closed']['en'] = 'closed';
	// neu mit 1.1.6-1.1.7
	$aMSG['adm']['dunning_letter']['de'] = 'Mahnung';
	$aMSG['adm']['dunning_letter']['en'] = 'dunning letter';
	$aMSG['adm']['dunning_level']['de'] = 'Stufe ';
	$aMSG['adm']['dunning_level']['en'] = 'level ';
	
	$aMSG['adm']['dunning_letterto']['de'] = $aMSG['adm']['dunning_letter']['de'].' zur Rechnung';
	$aMSG['adm']['dunning_letterto']['en'] = $aMSG['adm']['dunning_letter']['en'].' to invoice';
	$aMSG['adm']['dunning_letterto_short']['de'] = $aMSG['adm']['dunning_letter']['de'].' zu';
	$aMSG['adm']['dunning_letterto_short']['en'] = $aMSG['adm']['dunning_letter']['en'].' to';
	$aMSG['adm']['dunnedin']['de'] = 'Ermahnt in Beleg ';
	$aMSG['adm']['dunnedin']['en'] = 'Dunned in invoice ';
	$aMSG['adm']['stornodate']['de'] = 'Stornodatum: ';
	$aMSG['adm']['stornodate']['en'] = 'Cancelation date: ';
	$aMSG['adm']['invoicedate']['de'] = 'Rechnungsdatum: ';
	$aMSG['adm']['invoicedate']['en'] = 'Invoice date: ';
	$aMSG['adm']['dunningdate']['de'] = 'Mahnungsdatum: ';
	$aMSG['adm']['dunningdate']['en'] = 'Dunning letter date: ';
	$aMSG['adm']['current_month']['de'] = 'Aktueller Monat';
	$aMSG['adm']['current_month']['en'] = 'Current month';
	$aMSG['adm']['all_month']['de'] = 'Alle Monate';
	$aMSG['adm']['all_month']['en'] = 'All months';
	$aMSG['adm']['dunneddate']['de'] = '{DATE} Mahnstufe {NUMBER}';
	$aMSG['adm']['dunneddate']['en'] = '{DATE} dunning level {NUMBER}';	
	
	$aMSG['btn']['vcard']['de'] = "Kontakt";
	$aMSG['btn']['vcard']['en'] = "Contact";
	$aMSG['btn']['filter']['de'] = "&raquo;";
	$aMSG['btn']['filter']['en'] = "&raquo;";
	$aMSG['btn']['savechanges']['de'] = "Änderungen speichern";
	$aMSG['btn']['savechanges']['en'] = "Save changes";
	$aMSG['btn']['saveall']['de'] = "Alle abhaken und speichern";
	$aMSG['btn']['saveall']['en'] = "Check all and save";
	$aMSG['btn']['cancel_invoice']['de'] = "Stornieren";
	$aMSG['btn']['cancel_invoice']['en'] = "Cancel";
	$aMSG['btn']['copy_invoice']['de'] = "Duplizieren";
	$aMSG['btn']['copy_invoice']['en'] = "Copy";
	$aMSG['btn']['close_invoices']['de'] = "Abschließen";
	$aMSG['btn']['close_invoices']['en'] = "Close invoices";
	$aMSG['btn']['printviewplainletterhead']['de'] = $aMSG['btn']['printview']['de'].' ohne Briefkopf';
	$aMSG['btn']['printviewplainletterhead']['en'] = 'Printview with plain letterhead';
	
	$aMSG['form']['fremdkosten']['de'] = 'Alle Projekte';
	$aMSG['form']['fremdkosten']['en'] = 'All projects';
	$aMSG['form']['fremdkostenclients']['de'] = 'Alle Kunden';
	$aMSG['form']['fremdkostenclients']['en'] = 'All clients';
	$aMSG['form']['invoiceaddress']['de'] = 'Rechnungsaddresse';
	$aMSG['form']['invoiceaddress']['en'] = 'Invoice address';
	$aMSG['form']['used_in_invoice']['de'] = 'Abgerechnet in';
	$aMSG['form']['used_in_invoice']['en'] = 'Cleared in';
	
	$aMSG['adm']['prj_jumper']['de'] = "Quicklink";
	$aMSG['adm']['prj_jumper']['en'] = "Quicklink";
	$aMSG['btn']['go']['de'] = "Go";
	$aMSG['btn']['go']['en'] = "Go";
	$aMSG['btn']['dunning_letter']['de'] = 'Mahnung';
	$aMSG['btn']['dunning_letter']['en'] = 'Dunning letter';
?>