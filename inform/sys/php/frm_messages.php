<?php
/**				***** FRM *****
*
* frm_messages.php
*
* stellt die benoetigten woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode|...].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.0 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	1.0 / 2005-01-07
*/

// Forum
	$aMSG['forum']['forum']['de'] = "Forum";
	$aMSG['forum']['forum']['en'] = "Forum";
	$aMSG['forum']['wiki']['de'] = "Wiki";
	$aMSG['forum']['wiki']['en'] = "Wiki";

	$aMSG['forum']['folder']['de'] = "Thema";
	$aMSG['forum']['folder']['en'] = "Topic";
	$aMSG['forum']['folders']['de'] = "Themen";
	$aMSG['forum']['folders']['en'] = "Topics";
	$aMSG['forum']['root_folder']['de'] = "Unsortierte Beiträge";
	$aMSG['forum']['root_folder']['en'] = "Unsorted articles";
	$aMSG['forum']['all_folders']['de'] = "Alle Themen";
	$aMSG['forum']['all_folders']['en'] = "All topics";
	$aMSG['forum']['recent_entries']['de'] = "Neueste Beiträge";
	$aMSG['forum']['recent_entries']['en'] = "Latest articles";
	$aMSG['forum']['posting']['de'] = "Beitrag";
	$aMSG['forum']['posting']['en'] = "Article";
	$aMSG['forum']['mode_comment']['de'] = "kommentieren";
	$aMSG['forum']['mode_comment']['en'] = "comment";
	$aMSG['forum']['comments']['de'] = "Kommentare";
	$aMSG['forum']['comments']['en'] = "Comments";
	$aMSG['forum']['noassignment']['de'] = "Keine Zuordnung";
	$aMSG['forum']['noassignment']['en'] = "No assignment";
	$aMSG['forum']['content']['de'] = "Inhalt";
	$aMSG['forum']['content']['en'] = "Content";
	$aMSG['forum']['withComments']['de'] = "Kommentare erlauben";
	$aMSG['forum']['withComments']['en'] = "Allow comments";
	$aMSG['forum']['editAll']['de'] = "Editierbar";
	$aMSG['forum']['editAll']['en'] = "Editable";
	$aMSG['forum']['subscribe']['de'] = "Bookmark";
	$aMSG['forum']['subscribe']['en'] = "Bookmark";
	$aMSG['forum']['unsubscribe']['de'] = "Bookmark löschen";
	$aMSG['forum']['unsubscribe']['en'] = "Remove bookmark";
	$aMSG['forum']['subscriptions']['de'] = "Gemerkte Beiträge";
	$aMSG['forum']['subscriptions']['en'] = "Bookmarked articles";
	
	$aMSG['navi']['sections']['de'] = "Hauptpunkte";
	$aMSG['navi']['sections']['en'] = "Main sections";
	$aMSG['navi']['subsections']['de'] = "Unterpunkte";
	$aMSG['navi']['subsections']['en'] = "Subsections";
	
	$aMSG['form']['choose']['de'] = "Bitte wählen";
	$aMSG['form']['choose']['en'] = "Please select";
// Button-Beschriftungen
	$aMSG['btn']['comment']['de'] = "Beitrag kommentieren";
	$aMSG['btn']['comment']['en'] = "Comment article";
	
?>