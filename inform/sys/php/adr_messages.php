<?php
/**				***** ADR *****
*
* adr_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.1 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	2.0 / 2004-04-27 (v2)
* #history	1.0 intranet
*/

// Fehlermeldungen
	// speziell
	$aMSG['err']['names_missing']['de'] = "Bitte Vorname oder Nachname angeben!";
	$aMSG['err']['names_missing']['en'] = "Please insert either firstname or surname!";
	$aMSG['err']['contact_exists']['de'] = "Anmerkung: Ein gleichnamiger Kontakt existiert bereits.";
	$aMSG['err']['contact_exists']['en'] = "Note: An identical contact already exists.";
	$aMSG['err']['project_assigned']['de'] = "Der Eintrag konnte nicht gelöscht werden, da er einem Projekt zugewiesen ist.";
	$aMSG['err']['project_assigned']['en'] = "This entry could not be deleted, because it is assigned to a project.";
	
// Formulare
	$aMSG['form']['web']['de'] = "Webadresse (http://...)";
	$aMSG['form']['web']['en'] = "Internet (http://...)";
	$aMSG['form']['gender']['de'] = "Anrede";
	$aMSG['form']['gender']['en'] = "Gender";
	$aMSG['form']['gender_m']['de'] = "Herr";
	$aMSG['form']['gender_m']['en'] = "Male";
	$aMSG['form']['gender_f']['de'] = "Frau";
	$aMSG['form']['gender_f']['en'] = "Female";
	$aMSG['form']['birthday']['de'] = "Geburtstag";
	$aMSG['form']['birthday']['en'] = "Birthday";
	$aMSG['form']['academic_title']['de'] = "Titel";
	$aMSG['form']['academic_title']['en'] = "Title";
	$aMSG['form']['recipient']['de'] = "Empfänger";
	$aMSG['form']['recipient']['en'] = "Recipient";
	$aMSG['form']['mobile']['de'] = "Handy";
	$aMSG['form']['mobile']['en'] = "Mobile";
	$aMSG['form']['fax']['de'] = "Telefax";
	$aMSG['form']['fax']['en'] = "Fax";
	$aMSG['form']['number']['de'] = "Nummer";
	$aMSG['form']['number']['en'] = "Number";
	$aMSG['form']['extension']['de'] = "Durchwahl";
	$aMSG['form']['extension']['en'] = "Extension";
	$aMSG['form']['building']['de'] = "Haus/Stockwerk";
	$aMSG['form']['building']['en'] = "Building/Floor";
	$aMSG['form']['region']['de'] = "Region";
	$aMSG['form']['region']['en'] = "Region";
	$aMSG['form']['town']['de'] = "PLZ/Ort";
	$aMSG['form']['town']['en'] = "ZIP/Town";
	$aMSG['form']['po_box']['de'] = "Postfach";
	$aMSG['form']['po_box']['en'] = "P.O. Box";
	$aMSG['form']['country']['de'] = "Land";
	$aMSG['form']['country']['en'] = "Country";
	$aMSG['form']['address']['de'] = "Adresse";
	$aMSG['form']['address']['en'] = "Address";
	$aMSG['form']['company']['de'] = "Firma";
	$aMSG['form']['company']['en'] = "Company";
	$aMSG['form']['department']['de'] = "Abteilung";
	$aMSG['form']['department']['en'] = "Department";
	$aMSG['form']['position']['de'] = "Position";
	$aMSG['form']['position']['en'] = "Position";
	$aMSG['form']['client_nr']['de'] = "Kundennummer";
	$aMSG['form']['client_nr']['en'] = "Client number";
	$aMSG['form']['client_shortname']['de'] = "Kurzname";
	$aMSG['form']['client_shortname']['en'] = "Short name";
	$aMSG['form']['staff']['de'] = "Mitarbeiter";
	$aMSG['form']['staff']['en'] = "Staff";
	// Web-CUG
	$aMSG['form']['login']['de'] = "Login";
	$aMSG['form']['login']['en'] = "Login";
	$aMSG['form']['cugusergroup']['de'] = "Web Nutzergruppen";
	$aMSG['form']['cugusergroup']['en'] = "Web usergroups";
	$aMSG['form']['edit_cugusergroup']['de'] = "Web Nutzergruppen editieren";
	$aMSG['form']['edit_cugusergroup']['en'] = "Edit web usergroups";
	$aMSG['form']['new_cugusergroup']['de'] = "Neue Web Nutzergruppe anlegen";
	$aMSG['form']['new_cugusergroup']['en'] = "New web usergroup";
	$aMSG['form']['opentemplate']['de'] = 'E-Mail Vorlage';
	$aMSG['form']['opentemplate']['en'] = 'E-Mail template';
	$aMSG['form']['emailnotexists']['de'] = 'Keine E-Mail vorhanden';
	$aMSG['form']['emailnotexists']['en'] = 'No email address';
	
	$aMSG['form']['template']['de'] = "Seitentyp";
	$aMSG['form']['template']['en'] = "Pagetype";
	
	$aMSG['form']['thumb']['de'] = "Vorschau-Bild";
	$aMSG['form']['thumb']['en'] = "Thumbnail";
	$aMSG['form']['dimensions']['de'] = "Maße";
	$aMSG['form']['dimensions']['en'] = "Dimensions";
	$aMSG['form']['directory']['de'] = "Verzeichnisname";
	$aMSG['form']['directory']['en'] = "Directory";
	$aMSG['form']['type']['de'] = "Typ";
	$aMSG['form']['type']['en'] = "Type";
	$aMSG['form']['group']['de'] = "Gruppe";
	$aMSG['form']['group']['en'] = "Group";
	
	$aMSG['form']['please_select']['de'] = "- Bitte wählen -";
	$aMSG['form']['please_select']['en'] = "- Please select -";

// Standard-Saetze
	$aMSG['std']['required']['de'] = "Pflichtfelder";
	$aMSG['std']['required']['en'] = "Mandatory fields";

// Button-Beschriftungen
	$aMSG['btn']['clear_save']['de'] = "Leeren und sichern";
	$aMSG['btn']['clear_save']['en'] = "Clear and save";
	$aMSG['btn']['overwrite']['de'] = "Überschreiben";
	$aMSG['btn']['overwrite']['en'] = "Overwrite";
	$aMSG['btn']['save_close_win']['de'] = "Sichern und schließen";
	$aMSG['btn']['save_close_win']['en'] = "Save and close";
	$aMSG['btn']['select_company']['de'] = "Firma auswählen";
	$aMSG['btn']['select_company']['en'] = "Select company";
	$aMSG['btn']['select_contact']['de'] = "Kontakt auswählen";
	$aMSG['btn']['select_contact']['en'] = "Select contact";
	$aMSG['btn']['replace_company']['de'] = "Firma ersetzen";
	$aMSG['btn']['replace_company']['en'] = "Replace company";
	$aMSG['btn']['new_company']['de'] = "Neue Firma anlegen";
	$aMSG['btn']['new_company']['en'] = "New company";
	$aMSG['btn']['new_contact']['de'] = "Neuen Kontakt anlegen";
	$aMSG['btn']['new_contact']['en'] = "New contact";
	$aMSG['btn']['delete_connection']['de'] = "Entfernen";
	$aMSG['btn']['delete_connection']['en'] = "Remove";
	$aMSG['btn']['next_client_nr']['de'] = "Vorschlag";
	$aMSG['btn']['next_client_nr']['en'] = "Suggestion";
	$aMSG['btn']['edit_mode']['de'] = "Edit";
	$aMSG['btn']['edit_mode']['en'] = "Edit";
	$aMSG['btn']['view_mode']['de'] = "Sichern und schliessen";
	$aMSG['btn']['view_mode']['en'] = "Save and close";
	$aMSG['btn']['random_password']['de'] = "Passwort generieren";
	$aMSG['btn']['random_password']['en'] = "Generate password";
	
	$aMSG['btn']['print_page']['de'] = "Diese Seite drucken";
	$aMSG['btn']['print_page']['en'] = "Print this page";
	


// SEITEN-SPEZIFISCH #########################################################################################
	
// Address Seiten
	$aMSG['address']['headline']['de'] = "Kontakte";
	$aMSG['address']['headline']['en'] = "Contacts";
	$aMSG['address']['search']['de'] = "Suche";
	$aMSG['address']['search']['en'] = "Search";
	$aMSG['address']['all_groups']['de'] = "Alle Gruppen";
	$aMSG['address']['all_groups']['en'] = "All groups";
	$aMSG['address']['all_types']['de'] = "Alle Einträge";
	$aMSG['address']['all_types']['en'] = "All entries";
	$aMSG['address']['type']['de'] = "Kontakt-Typ";
	$aMSG['address']['type']['en'] = "Contact type";
	$aMSG['address']['choose_type']['de'] = "Welchen Kontakt-Typ wollen Sie erstellen?";
	$aMSG['address']['choose_type']['en'] = "Please choose the contact type:";
	$aMSG['address']['contact']['de'] = "Kontakt";
	$aMSG['address']['contact']['en'] = "Contact";
	$aMSG['address']['contacts']['de'] = "Kontakte";
	$aMSG['address']['contacts']['en'] = "Contacts";
	$aMSG['address']['new_contact']['de'] = "Neuer Kontakt";
	$aMSG['address']['new_contact']['en'] = "New contact";
	$aMSG['address']['company']['de'] = "Firma";
	$aMSG['address']['company']['en'] = "Company";
	$aMSG['address']['companies']['de'] = "Firmen";
	$aMSG['address']['companies']['en'] = "Companies";
	$aMSG['address']['new_company']['de'] = "Neue Firma";
	$aMSG['address']['new_company']['en'] = "New company";
	$aMSG['address']['view']['de'] = "Ansicht";
	$aMSG['address']['view']['en'] = "View";
	$aMSG['address']['edit']['de'] = "Editiere:";
	$aMSG['address']['edit']['en'] = "Edit:";
	$aMSG['address']['d']['de'] = "Allgemein";
	$aMSG['address']['d']['en'] = "Global";
	$aMSG['address']['p']['de'] = "Privat";
	$aMSG['address']['p']['en'] = "Private";
	$aMSG['address']['o1']['de'] = "1. Office";
	$aMSG['address']['o1']['en'] = "1. Office";
	$aMSG['address']['o2']['de'] = "2. Office";
	$aMSG['address']['o2']['en'] = "2. Office";
	$aMSG['address']['c']['de'] = "Adresse";
	$aMSG['address']['c']['en'] = "Address";
	$aMSG['address']['s']['de'] = "Kontakte";
	$aMSG['address']['s']['en'] = "Contacts";
	$aMSG['address']['w']['de'] = "Website Zugang";
	$aMSG['address']['w']['en'] = "Website Access";
	$aMSG['address']['description']['de'] = "Beschreibung";
	$aMSG['address']['description']['en'] = "Description";
	$aMSG['address']['comment']['de'] = "Kommentar";
	$aMSG['address']['comment']['en'] = "Comment";
	
	$aMSG['address']['settings']['de'] = "Einstellungen";
	$aMSG['address']['settings']['en'] = "Settings";
	$aMSG['address']['mailing']['de'] = "Postsendung";
	$aMSG['address']['mailing']['en'] = "Mailing";
	$aMSG['address']['mailinglist']['de'] = "Liste";
	$aMSG['address']['mailinglist']['en'] = "Mailinglist";
	$aMSG['address']['in_list']['de'] = "Auswahl";
	$aMSG['address']['in_list']['en'] = "Shortlist";
	$aMSG['address']['printshortlist']['de'] = "Druckansicht";
	$aMSG['address']['printshortlist']['en'] = "Print view";
	
	$aMSG['address']['labels']['de'] = "Labels";
	$aMSG['address']['labels']['en'] = "Labels";
	
	$aMSG['address']['excelsettings']['de'] = "Felder für die Excel Datei";
	$aMSG['address']['excelsettings']['en'] = "Fields for excel";
?>