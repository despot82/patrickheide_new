<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html lang="de">
<head>
	<title></title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<meta name="content-language" content="de">
	<style type="text/css">
		.red { color:red; }
		.green { color:green; }
	</style>

</head>

<body>
<div align="center">
<h2>CMS-Info</h2>
<span class="red">Bitte nach erfolgtem Debugging vom Live-Server LOESCHEN!</span>
</div>
<hr noshade><br>

<i>1. Die wichtigsten Servervariablen:</i><br>
<?php	echo "_SERVER['SERVER_NAME']: <b>".$_SERVER['SERVER_NAME'] . "</b><br>\n";
		echo "_SERVER['PHP_SELF']: ".$aENV['PHP_SELF'] . "<br>\n";
		echo "_SERVER['DOCUMENT_ROOT']: ".$_SERVER['DOCUMENT_ROOT'] . "<br>\n";
		
		echo 'display_errors = ' . ini_get('display_errors') . "<br>\n";
		echo 'include_path = ' . ini_get('include_path') . "<br>\n";
		echo 'post_max_size = ' . ini_get('post_max_size') . "<br>\n";
		echo 'register_globals = ' . ini_get('register_globals') . "<br>\n";
		echo 'safe_mode = ' . ini_get('safe_mode') . "<br>\n";
		echo 'upload_max_filesize = ' . ini_get('upload_max_filesize') . "<br>\n";
		echo 'variables_order = ' . ini_get('variables_order') . "<br>\n";
		
		echo 'Aktuelle PHP-Version: ' . phpversion() . "<br>\n";
 ?>
<hr noshade><br>

<i>2. Include "_include_all.php":</i><br>
<?php	if (file_exists("../sys/php/_include_all.php")) {
			require_once ("_include_all.php");
			echo '<span class="green">"_include_all.php" wurde inkludiert.</span>';
		} else {
			die ('<span class="red">"_include_all.php" existiert hier nicht!</span>');
		} ?>
<hr noshade><br>

<i>3. Teste DB-Connect:</i><br><span class="red">
<?php	$oDbTest = new dbconnect($aENV['db']);
		$oDbTest -> set_debug_true();
		$oDbTest -> connect(); ?>
<br></span><span class="green">Wenn hier nichts rotes drueber steht, hat der DB-Connect geklappt.</span>
<hr noshade><br>

<i>4. Das aENV-Array:</i><br><pre>
<?php	print_r($aENV); ?>
</pre>
<hr noshade><br>

<i>5. Alle PHP-Server-Variablen:</i><br><pre>
<?php	print_r($_SERVER); ?>
</pre>
<hr noshade><br>

<i>6. Alle PHP_INI-Variablen:</i><br><pre>
<?php	if (function_exists('ini_get_all')) {
			$inis = ini_get_all();
			print_r($inis);
		} else {
			echo '<span class="red">function "ini_get_all()" existiert nicht!<br></span>';
		} ?>
</pre>
<hr noshade><br>

</body>
</html>
