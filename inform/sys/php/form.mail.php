<?php
/**
* form.mail.php
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2003-02-23
**/

// Initialisiere
	if (!is_array($_POST)) exit;

// hole erforderliche Variablen
	
#// CMS-erforderliche Vars // wenn man das mal braucht, diese var(s) an den header (am ende der seite) anhaengen!
#	// Navi-ID des Formulars				-> input name: sNaviId
#	$sNaviId	 = $_POST['sNaviId'];	unset($_POST['sNaviId']);
#	...
	
// Mail-erforderliche Vars
	// Recipients aufloesen
	$arr_recipients = 	explode(",",$_POST['recipients']);
				  		unset($_POST['recipients']);
	
	// Pflichtfelder aufloesen
	if ($_POST['forcefields'])
		$arr_forcefields = 	explode(",",$_POST['forcefields']);
					   		unset($_POST['forcefields']);
	
	// Pfad zu der ReplyOK-Seite		-> input name: replyok
	$replyok	 = $_POST['replyok'];
				   unset($_POST['replyok']);
	
	// Pfad zu der ReplyKO-Seite		-> input name: replyok
	$replyko	 = $_POST['replyko'];
				   unset($_POST['replyko']);
	
	// Subject der Email				-> input name: subject
	$subject	 = $_POST['subject'];
				   unset($_POST['subject']);
	
	// Subject der Email, wenn Subject-Feld leer ist		-> input name: defaultSubject
	if($subject == "") { $subject = $_POST['defaultSubject']; }
					unset($_POST['defaultSubject']);
	
	// Absender der Email				-> input name: sender
	$sender		 = $_POST['sender'];
				   unset($_POST['sender']);
	
// Mail-optionale Vars
	// Optional: Pfad zu einer Logdatei	-> input name: logpath
	$logpath	 = $_POST['logpath'];
				   unset($_POST['logpath']);
	
	// Optional: Felder der CSV-Datei	-> input name: logfields
	$logfields	 = $_POST['logfields'];
				   unset($_POST['logfields']);
	
	// Optional: Logrotate (month/week/day)	-> input name: logrotate
	$logrotate	 = $_POST['logrotate'];
				   unset($_POST['logrotate']);
	
	// Optional: mimetype	-> input name: mimetype
	$mimetype	 = $_POST['mimetype'];
				   unset($_POST['mimetype']);
	
	// Optional: template	-> input name: template
	$template	 = $_POST['template'];
				   unset($_POST['template']);
	
	// wenn ein oder mehrere attachments angfuegt werden sollen, muss das/die felder 'attachment[]' heissen!


// includiere Mail-Klasse
	require_once ("cms/php/class.mail.php");


// Funktionen-------------------------------------------------------------------------------

/**
* Erzeugt eine Html-Mail aus einem Template
*
* @access	public
* @param    string  Name des Templates
*/ 
	function parseTemplate($templateurl) {
		#global $_POST;
		$counter = 0;
		do {
			$fp = fopen($templateurl, "r");
			if($counter++ > 10)
				return false;
		} while(!is_resource($fp));
		$template = fread($fp, 128000);
		$template = preg_replace(
			Array(
				'/\<ifexists:([^ \>]+?)\>([^\^]*?)\<\/ifexists\>/ei', 
				'/\<formmail:([^ \>]+?) ([^\>]+?)\/\>/ei',
				'/\<formmail:([^ \>]+?)\/\>/ei'
			),
			Array(
			'parseIfExists("\\1", "\\2")', 
			'parseValue("\\1", "\\2")', 
			'parseValue("\\1", "\\2")'
			),
			$template
		);
		return $template;
	}
	
	function parseIfExists($key, $tagcontent) { #global $_POST;
		if($_POST[$key])
			return $tagcontent;
	}
	
	function parseValue($key, $options) { global $_POST;
			$value = $_POST[$key];
			if(strpos(strtolower($options), "returnkey") !== false && (
				(is_numeric($value) && $value > 0) ||
				(!is_numeric($value) && trim($value) != "") )
			) {
				return $key;
			}
		return $value;
	}

// Mailbody generieren
	if(isset($mimetype) && $mimetype != "plain/text" && !empty($template)) {
		$mailbody = parseTemplate($template);
	} else {
		$mailbody .= "\n-----------------------------------------------".date("d.m.Y")."\n\n";
		reset ($_POST);
		while (list ($key, $val) = each ($_POST)) {
		    $key = "[$key]:\n";
			if(is_array($val)) {
				$tmp = "";
				for($i=0;$i<count($val);$i++) { $tmp .= $val[$i]."\n"; }
				$tmp = substr($tmp,0,-1);
				$val = $tmp;
			}
			if (strlen($val)>30) $val = wordwrap($val);
			$mailbody .= $key.$val."\n\n";
		}
		$mailbody .= "\n---------------------------------------------------------------\n\n";
	}

// Check -----------------------------------------------------------
	unset($checkfailed);
	if (is_array($arr_forcefields)) 
		for ($i=0;$i<count($arr_forcefields);$i++)
			if ($_POST[$arr_forcefields[$i]]==null) $checkfailed=true;


// Mailsenden ------------------------------------------------------
	if (!$checkfailed) {
		// Loggen
		if($logpath) {
			switch($logrotate) {
			 case "month": 	$logpath = date("M")."_".$logpath; break;
			 case "week": 	$logpath = ceil(date("z")/7)."_".$logpath; break;
			 case "day": 	$logpath = date("z")."_".$logpath; break;
			}
			if($logfields)	$fieldlist = preg_split('/[ ,;\/]/', $logfields, -1, PREG_SPLIT_NO_EMPTY);
			else			$fieldlist = array_keys($_POST);
			if(!file_exists($logpath)) {
				$fp = fopen($logpath,"w");
				fwrite($fp,'"'.implode('";"',$fieldlist).'"');
			} else {
				$fp = fopen($logpath,"a");
			}
			$buffer = Array();
			foreach($fieldlist as $field) 
				$buffer[] = $_POST[$field];
			fwrite($fp, '"'.implode('";"',$buffer).'"');
			fclose($fp);
		}

// Versenden
	if(is_array($arr_recipients) && count($arr_recipients)) {
		if(isset($mimetype) && $mimetype != "plain/text") {
		  $mail = new mime_mail(MIMEMAIL_HTML);
		} else { 
		  $mail = new mime_mail(MIMEMAIL_TEXTONLY);
		}
		
		$mail->from 	= $sender;
		$mail->subject 	= $subject;
		$mail->body 	= $mailbody;
		
		// eventuell Attachments anfuegen
		if(is_array($_FILES["attachment"])) {			
			foreach($_FILES["attachment"]["tmp_name"] as $filekey => $attach)
				if ($attach != "none") {
					$mail -> add_attachment(
						fread(
							fopen($attach,"r"), $_FILES["attachment"]["size"][$filekey]
						), 
						$_FILES["attachment"]["name"][$filekey]
					);
				}
		}
		
		for ($i=0;$i<count($arr_recipients);$i++) {
			$mail -> to = $arr_recipients[$i];
			$mail -> send(0.5);
		}
	}
	HEADER("Location:$replyok");
} else {
	HEADER("Location:$replyko");
}
?>