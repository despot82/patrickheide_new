<?php
/**				***** PMS *****
*
* pms_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.1 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	2.0 / 2004-05-04 (komplett NEU)
* #history	1.0 presentation_tool
*/

// Fehlermeldungen
	// speziell
	$aMSG['err']['delete_all_presentation']['de'] = "Sind Sie sicher, dass Sie diese Folie mit allen ihren Unterpunkten löschen möchten?";
	$aMSG['err']['delete_all_presentation']['en'] = "Do you really want to delete this slide incl. all subsections?";
	$aMSG['err']['clear_all_images']['de'] = "Möchten Sie alle Bilder entfernen?";
	$aMSG['err']['clear_all_images']['en'] = "Do you want to clear all images?";
	$aMSG['err']['no_formfield']['de'] = "Bitte bestimmen Sie zunächst das zu füllende Daten-Feld.";
	$aMSG['err']['no_formfield']['en'] = "Please select the required field first.";
	$aMSG['err']['zip_not_possible']['de'] = "Die Archivierungsfunktion wird gerade von jemand anderem benuzt. Bitte probieren Sie es in wenigen Sekunden noch einmal.";
	$aMSG['err']['zip_not_possible']['en'] = "This function is currently in use by another user. Please try again in a few seconds.";
	$aMSG['err']['slot_max_exeeded']['de'] = "Die maximal mögliche Anzahl an gleichzeitig editierbaren Präsentationen ist erreicht.";
	$aMSG['err']['slot_max_exeeded']['en'] = "Maximum editable presentations is reached.";

// Formulare
	$aMSG['form']['state']['de'] = "Navigation";
	$aMSG['form']['state']['en'] = "Navigation";
	$aMSG['form']['label']['de'] = "Folientitel";
	$aMSG['form']['label']['en'] = "Slidetitle";
	$aMSG['form']['comment']['de'] = "Archiv-Notiz";
	$aMSG['form']['comment']['en'] = "Archive-notes";
	$aMSG['form']['template']['de'] = "Template";
	$aMSG['form']['template']['en'] = "Template";
	$aMSG['form']['folder']['de'] = "Ordner";
	$aMSG['form']['folder']['en'] = "Folder";
	
	$aMSG['form']['pTitle']['de'] = "Titel der Präsentation";
	$aMSG['form']['pTitle']['en'] = "Presentation title";
	$aMSG['form']['pClient']['de'] = "Firma/Sprecher";
	$aMSG['form']['pClient']['en'] = "Company/Speaker";
	$aMSG['form']['pAuthor']['de'] = "Autor";
	$aMSG['form']['pAuthor']['en'] = "Author";
	$aMSG['form']['pTeam']['de'] = "Team";
	$aMSG['form']['pTeam']['en'] = "Team";
	$aMSG['form']['pDate']['de'] = "Datum";
	$aMSG['form']['pDate']['en'] = "Date";
	$aMSG['form']['pComment']['de'] = "Kommentar";
	$aMSG['form']['pComment']['en'] = "Comment";
	$aMSG['form']['pCommentAlt']['de'] = "Kommentar editieren";
	$aMSG['form']['pCommentAlt']['en'] = "Edit comment";
	$aMSG['form']['pCommentText']['de'] = "Diese Folie wurde kommentiert!";
	$aMSG['form']['pCommentText']['en'] = "A comment has been written for this slide!";
	$aMSG['form']['pAutorun']['de'] = "Autorun";
	$aMSG['form']['pAutorun']['en'] = "Autorun";
	$aMSG['form']['pSlideSet']['de'] = "Slide Einstellungen";
	$aMSG['form']['pSlideSet']['en'] = "Slide settings";
	$aMSG['form']['pHeadline']['de'] = "Überschrift";
	$aMSG['form']['pHeadline']['en'] = "Headline";
	$aMSG['form']['pImage']['de'] = "Mediafile";
	$aMSG['form']['pImage']['en'] = "Media file";
	$aMSG['form']['pFLV']['de'] = "Externes FLV-Video";
	$aMSG['form']['pFLV']['en'] = "External FLV video";
	$aMSG['form']['pFLV_file']['de'] = "Dateiname";
	$aMSG['form']['pFLV_file']['en'] = "Filename";
	$aMSG['form']['pFLV_size']['de'] = "Weite x Höhe";
	$aMSG['form']['pFLV_size']['en'] = "Width x Height";
	$aMSG['form']['pFLV_controls']['de'] = "Videosteuerung";
	$aMSG['form']['pFLV_controls']['en'] = "Video controls";
	$aMSG['form']['pFLV_controls_txt']['de'] = "benötigt 40px zusätzliche Höhe";
	$aMSG['form']['pFLV_controls_txt']['en'] = "requires additional 40px in height";
	$aMSG['form']['pMediaflag']['de'] = "Bildebene";
	$aMSG['form']['pMediaflag']['en'] = "Image layer";
	$aMSG['form']['pText']['de'] = "Textebene";
	$aMSG['form']['pText']['en'] = "Text layer";
	$aMSG['form']['pList']['de'] = "Liste";
	$aMSG['form']['pList']['en'] = "List";
	$aMSG['form']['pMultipart']['de'] = "Listenelement";
	$aMSG['form']['pMultipart']['en'] = "List element";
	$aMSG['form']['new_multipart']['de'] = "Neues Listenelement";
	$aMSG['form']['new_multipart']['en'] = "New list element";
	
	$aMSG['form']['description']['de'] = "Beschreibung";
	$aMSG['form']['description']['en'] = "Description";
	$aMSG['form']['type']['de'] = "Typ";
	$aMSG['form']['type']['en'] = "Type";
	
	$aMSG['form']['slideshow']['de'] = "Slideshow";
	$aMSG['form']['slideshow']['en'] = "Slideshow";

// Subnavi
	$aMSG['subnavi']['content']['de'] = "Inhalt";
	$aMSG['subnavi']['content']['en'] = "Content";
	$aMSG['subnavi']['archive']['de'] = "Archiv";
	$aMSG['subnavi']['archive']['en'] = "Archive";
	$aMSG['subnavi']['meta']['de'] = "Einstellungen";
	$aMSG['subnavi']['meta']['en'] = "Properties";
	$aMSG['subnavi']['mediadb']['de'] = "Media-DB";
	$aMSG['subnavi']['mediadb']['en'] = "Media-DB";
	$aMSG['subnavi']['preview']['de'] = "Vorschau";
	$aMSG['subnavi']['preview']['en'] = "Preview";
	$aMSG['subnavi']['fullsize']['de'] = "Groß";
	$aMSG['subnavi']['fullsize']['en'] = "Full";
	$aMSG['subnavi']['halfsize']['de'] = "Klein";
	$aMSG['subnavi']['halfsize']['en'] = "Half";
	$aMSG['subnavi']['teams']['de'] = "Teams";
	$aMSG['subnavi']['teams']['en'] = "Teams";

// Standard-Saetze
	$aMSG['std']['presentation']['de'] = "Präsentation";
	$aMSG['std']['presentation']['en'] = "Presentation";
	$aMSG['std']['slide']['de'] = "Folie";
	$aMSG['std']['slide']['en'] = "Slide";
	$aMSG['std']['download']['de'] = "Downloaden";
	$aMSG['std']['download']['en'] = "Download";

// Button-Beschriftungen
	$aMSG['btn']['continue']['de'] = "Weiter";
	$aMSG['btn']['continue']['en'] = "Continue";
	$aMSG['btn']['delete_all']['de'] = "Alle löschen";
	$aMSG['btn']['delete_all']['en'] = "Delete all";
	$aMSG['btn']['clear']['de'] = "Leeren";
	$aMSG['btn']['clear']['en'] = "Clear";
	$aMSG['btn']['overwrite']['de'] = "Überschreiben";
	$aMSG['btn']['overwrite']['en'] = "Overwrite";
	$aMSG['btn']['export']['de'] = "Exportieren zum downloaden";
	$aMSG['btn']['export']['en'] = "Export for download";
	$aMSG['btn']['download']['de'] = "Downloaden";
	$aMSG['btn']['download']['en'] = "Download";


// SEITEN-SPEZIFISCH #########################################################################################
	
// Portal
	$aMSG['portal']['js_alert']['de'] = "Die Präsentation wird jetzt gelöscht. Stellen Sie sicher, dass Sie gesichert wurde, oder dass sie nicht mehr benötigt wird.  \\n Es wird keine Sicherungskopie erzeugt!";
	$aMSG['portal']['js_alert']['en'] = "The presentation will be deleted - please make sure you have saved it or that you don\'t need it any more. \\n No backup copy will be created automatically!";
	$aMSG['archive']['last_archived']['de'] = "Letzte Archivierung";
	$aMSG['archive']['last_archived']['en'] = "Last archived";
	
// Archiv-Seiten
	$aMSG['archive']['hl_archive']['de'] = "Präsentation exportieren";
	$aMSG['archive']['hl_archive']['en'] = "Export presentation";
	$aMSG['archive']['hl_details']['de'] = "Archiv-Details";
	$aMSG['archive']['hl_details']['en'] = "Archive details";
	$aMSG['archive']['archive']['de'] = "Präsentation für Download erstellen";
	$aMSG['archive']['archive']['en'] = "Create presentation for download";
	$aMSG['archive']['archived_long']['de'] = "Die Präsentation wurde erfolgreich exportiert.";
	$aMSG['archive']['archived_long']['en'] = "The presentation has been exported successfully.";
	$aMSG['archive']['restored']['de'] = "Version wurde wiederhergestellt";
	$aMSG['archive']['restored']['en'] = "Version has been restored";
	$aMSG['archive']['of_presentation']['de'] = "zur aktuellen Präsentation";
	$aMSG['archive']['of_presentation']['en'] = "of the current presentation";
	// in JS Fehlermeldungen KEIN einfaches anfuehrungszeichen verwenden!!!
	$aMSG['archive']['js_alert']['de'] = "Möchten Sie die aktuelle Version überschreiben?";
	$aMSG['archive']['js_alert']['en'] = "Do you want to overwrite the current presentation?";
	$aMSG['archive']['js_alert_overwrite']['de'] = "Ein Archiv mit diesem Dateinamen existiert bereits. \\n Bitte wählen Sie einen anderen Namen.";
	$aMSG['archive']['js_alert_overwrite']['en'] = "This file exists allready. \\n Please choose a different name.";
	$aMSG['archive']['js_confirm_overwrite']['de'] = "Ein Archiv mit diesem Dateinamen existiert bereits. \\n Möchten Sie es überschreiben?";
	$aMSG['archive']['js_confirm_overwrite']['en'] = "This file exists allready. \\n Do you want to overwrite it?";
	$aMSG['archive']['js_alert_entities']['de'] = "Archivieren gescheitert. \\n Der Dateiname beinhaltet ungültige Zeichen: Bitte stellen Sie sicher dass der Dateiname keine Leer- oder Sonderzeichen außer dem Unterstrich (_) hat.";
	$aMSG['archive']['js_alert_entities']['en'] = "Archive failed. \\n Please make sure you are not using spaces or symbols, except underscore (_) in your filename.";
	$aMSG['archive']['js_alert_length']['de'] = "Archivieren gescheitert. \\n Der Dateiname ist zu lang: Bitte stellen Sie sicher dass der Dateiname inklusive der Dateiendung \".zip\" höchstens 31 Zeichen lang ist.";
	$aMSG['archive']['js_alert_length']['en'] = "Archive failed. \\n Please check the filename length and make sure you do not use more than 27 characters plus file-extension \".zip\".";
	$aMSG['btn']['restore_archive']['de'] = "Archiv wiederherstellen";
	$aMSG['btn']['restore_archive']['en'] = "Restore archive";
	$aMSG['btn']['click_restore_archive']['de'] = "Klicken um dieses Archiv wiederherzustellen";
	$aMSG['btn']['click_restore_archive']['en'] = "Click to restore this archive";
	$aMSG['btn']['delete_archive']['de'] = "Archiv löschen";
	$aMSG['btn']['delete_archive']['en'] = "Delete archive";
	// popup_selectarchive
	$aMSG['archive']['hl']['de'] = "Präsentation auswählen";# aus Archiv
	$aMSG['archive']['hl']['en'] = "Select presentation";# from archive
	
// Content Seiten
	$aMSG['content']['edit']['de'] = "editieren";
	$aMSG['content']['edit']['en'] = "edit";
	$aMSG['content']['slides']['de'] = "Folien";
	$aMSG['content']['slides']['en'] = "Slides";
	$aMSG['content']['add_slide']['de'] = "Neue Folie";
	$aMSG['content']['add_slide']['en'] = "New slide";
	$aMSG['content']['show_slides']['de'] = "Zeige Unterpunkte";
	$aMSG['content']['show_slides']['en'] = "Show subsections";
	$aMSG['content']['hide_slides']['de'] = "Verstecke Unterpunkte";
	$aMSG['content']['hide_slides']['en'] = "Hide subsections";
	$aMSG['content']['btn_topnavi']['de'] = "Topnavi editieren";
	$aMSG['content']['btn_topnavi']['en'] = "Edit topnavi";
	$aMSG['content']['pause']['de'] = "Pausen-Animation";
	$aMSG['content']['pause']['en'] = "Break animation";
	// detail
	$aMSG['content']['edit_prev']['de'] = "Vorheriger Datensatz";
	$aMSG['content']['edit_prev']['en'] = "Previous record";
	$aMSG['content']['edit_next']['de'] = "Nächster Datensatz";
	$aMSG['content']['edit_next']['en'] = "Next record";
	#$aMSG['content']['subheadline']['de'] = "Bitte geben Sie erst den Titel ein und wählen den Folientyp aus.";
	#$aMSG['content']['subheadline']['en'] = "Please enter the title and select the slide type first.";
	$aMSG['content']['assignment']['de'] = "Zuordnung";
	$aMSG['content']['assignment']['en'] = "Order";
	$aMSG['content']['noassignment']['de'] = "Keine Zuordnung";
	$aMSG['content']['noassignment']['en'] = "No assignment";
	$aMSG['content']['new_section']['de'] = "Neues Kapitel";
	$aMSG['content']['new_section']['en'] = "New section";
	$aMSG['content']['use_on_navi']['de'] = "In der Navigation zeigen";
	$aMSG['content']['use_on_navi']['en'] = "Show in the navigation";
	$aMSG['content']['layout']['de'] = "Layout";
	$aMSG['content']['layout']['en'] = "Layout";

// TextEdit-Popup
	// buttons
	$aMSG['txtedit']['bold']['de'] = "Fett";
	$aMSG['txtedit']['bold']['en'] = "bold";
	$aMSG['txtedit']['remove_styles']['de'] = "Formatierungen löschen";
	$aMSG['txtedit']['remove_styles']['en'] = "remove all styles";
	$aMSG['txtedit']['accept_changes']['de'] = "Änderungen übernehmen";
	$aMSG['txtedit']['accept_changes']['en'] = "accept changes";
	// headlines
	$aMSG['txtedit']['format']['de'] = "Auszeichnung";
	$aMSG['txtedit']['format']['en'] = "Format";
	// tool-tips
	$aMSG['txtedit']['tt_headline']['de'] = "Texteditor-Tipps";
	$aMSG['txtedit']['tt_headline']['en'] = "Tool-Tips";
	$aMSG['txtedit']['tt_subheadline']['de'] = "Hilfe zur Benutzung";
	$aMSG['txtedit']['tt_subheadline']['en'] = "How to use this text editor";
	$aMSG['txtedit']['tt_copytext']['de'] = "Bitte markieren Sie zunächst den gewünschten Textabschnitt und klicken dann den entsprechenden Button. Bitte beachten Sie, dass verschachtelte Formatierungen u.U. zu Problemen führen können.";
	$aMSG['txtedit']['tt_copytext']['en'] = "Please make a selection in the textarea first and then click the required button. Please mind that nested formatting may cause problems.";
	$aMSG['txtedit']['tt_def_bold']['de'] = "Fettschrift";
	$aMSG['txtedit']['tt_def_bold']['en'] = " text in bold";
	$aMSG['txtedit']['tt_error_format']['de'] = "Teile des ausgewählten Textes sind bereits bearbeitet!\\nDie Änderungen werden nicht übernommen.";
	$aMSG['txtedit']['tt_error_format']['en'] = "Parts of the selected text have already been formatted!\\nThe new changes will not be applied.";
	$aMSG['txtedit']['tt_error_selection']['de'] = "Bitte markieren Sie einen Textabschnitt!";
	$aMSG['txtedit']['tt_error_selection']['en'] = "Please make a selection!";
	// form
	$aMSG['txtedit']['subject']['de'] = "Betreff";
	$aMSG['txtedit']['subject']['en'] = "Subject";

?>