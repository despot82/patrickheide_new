<?php
#---------------------------------------------------------------------------------
# 	Include-SETUP
#	definiert alle benoetigten- und standard-VARS
#	includiert alle projektspezifischen VARS + class.* + inc.* Dateien in der richtigen Reihenfolge
#		
#		1a. global system VARS (default)
#		 b. global environment VARS (default)
#		 c. global module VARS: (default)
#		 d. global environment VARS (not customer-specific)
#		2a. global MESSAGES-ARRAY (default)
#		 b. global MESSAGES-ARRAY (customer-specific)
#		3.  global CLASSES (vars needed)
#		4a. global FUNCTIONS (default) (vars and classes needed)
#		 b. global FUNCTIONS (customer-specific) (vars and classes needed)
#		(5. NOTES)
#---------------------------------------------------------------------------------
	// define ROOT
	if(strstr($_SERVER['PHP_SELF'],'vitsoe') || strstr($_SERVER['SERVER_NAME'],'vitsoe')) {
		define('INFORMROOT','wiese/',true);
	}
	else {
		define('INFORMROOT','',true);
	}
#---------------------------------------------------------------------------------
	if($_SERVER['SERVER_NAME'] != "rhsrv2" && $_SERVER['SERVER_NAME'] != "10.1.1.3" && !strstr($_SERVER['PHP_SELF'],'DEV')) {
		// ERROR HANDLING | file
		define('ERRORHANDLER','file',true);
	}
	else {
		define('ERRORHANDLER','screen',true);
	}	
	error_reporting(E_ALL ^ E_NOTICE); // DEBUG: alle Warnmeldungen AUSSER leichte "Bemerkungen" ;-)
	#error_reporting(E_ALL); // DEBUG: ALLE Warnmeldungen
	#error_reporting(0); // no error-messages on live-systems
	
	// ab hier nichts mehr aendern
	$bIncludedErrorHandling = false;
	
//	if (file_exists('../sys/php/global/service/class.ErrorHandling.php')) {
//		include_once('../sys/php/global/service/class.ErrorHandling.php');
//		$bIncludedErrorHandling = true;
//	}

	if (file_exists('global/service/class.ErrorHandling.php')) {
		include_once('global/service/class.ErrorHandling.php');
		$bIncludedErrorHandling = true;
	}
	
	$oError = null;
	if($bIncludedErrorHandling && ERRORHANDLER != 'screen') {
		$oError = @new ErrorHandling();
		set_error_handler(array(&$oError,'errorReport')); // ,E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE
		if(ERRORHANDLER == 'file') {
			register_shutdown_function(array(&$oError,'writeErrors'));
		}
		if(ERRORHANDLER == 'mail') {
			register_shutdown_function(array(&$oError,'writeErrorsToPerMail'));
		}
	}
#-----------------------------------------------------------------------------
	// OUTPUT BUFFERING
	function ob_zstart() { // komprimierungsfunktion von html-seiten
		if (eregi('gzip', $_SERVER['HTTP_ACCEPT_ENCODING']) && $_SERVER['REQUEST_METHOD'] != 'POST') {
			ob_start();
		} else { ob_start(); }
	}
	ob_zstart();

#-----------------------------------------------------------------------------
	// BENCHMARK
	$masterbench = explode(' ', microtime()); // -> wird i.d. "inc.footer.php" abgefragt
#-----------------------------------------------------------------------------

	// INIT
	$aENV = array(); // globales inform-environment array

#-----------------------------------------------------------------------------
	// Checken, ob es ein PHP 5 Server ist. Dann werden andere Klassen eingebunden!
	if(version_compare(phpversion(), "5.0.0", 'ge')) {
		$aENV['php5'] = true;
		if ($bIncludedErrorHandling && is_object($oError)) {
			set_exception_handler(array($oError,'errorReport'));
		}
	}
	else {
		$aENV['php5'] = false;
	}
	
#-----------------------------------------------------------------------------
# 1a. global system VARS (do not touch!):
#-----------------------------------------------------------------------------

// system tables (default)
	$aENV['table'] = array();
	
	$aENV['table']['global_tree']		= 'sys_global_tree';
	$aENV['table']['sys_global_search']	= 'sys_global_search';
	$aENV['table']['sys_memo']			= 'sys_memo';
	$aENV['table']['sys_privilege']		= 'sys_privilege';
	$aENV['table']['sys_user']			= 'sys_user';
	$aENV['table']['sys_usergroup']		= 'sys_usergroup';
	$aENV['table']['sys_user_usergroup']= 'sys_user_usergroup';
	$aENV['table']['sys_team']			= 'sys_team';
	$aENV['table']['sys_office']		= 'sys_office';
	$aENV['table']['sys_team_access']	= 'sys_team_access_right';
	$aENV['table']['sys_labels']		= 'sys_labels';
	
// frm tables (default)
	$aENV['table']['frm_forum']			= 'frm_forum';
	
// mdb tables (default)
	$aENV['table']['mdb_media']			= 'mdb_media';
	
// cal tables (default)
	$aENV['table']['cal_event']			= 'cal_event';
	
// arc tables (default)
	$aENV['table']['arc_data']			= 'arc_data';
	
// pms tables (default)
	$aENV['table']['pms_content']['1']	= 'pms_content_1';	// slot 1
	$aENV['table']['pms_content']['2']	= 'pms_content_2';	// slot 2
	$aENV['table']['pms_content']['3']	= 'pms_content_3';	// slot 3
	$aENV['table']['pms_content']['4']	= 'pms_content_4';	// slot 4
	$aENV['table']['pms_content']['5']	= 'pms_content_5';	// slot 5
	$aENV['table']['pms_content']['6']	= 'pms_content_6';	// slot 6
	$aENV['table']['pms_content']['7']	= 'pms_content_7';	// slot 7
	$aENV['table']['pms_content']['8']	= 'pms_content_8';	// slot 8
	$aENV['table']['pms_content']['9']	= 'pms_content_9';	// slot 9
	$aENV['table']['pms_content']['10']	= 'pms_content_10';	// slot 10
	$aENV['table']['pms_archive']		= 'pms_archive';
	// Konstante fuer das SQL-File (zum wiederherstellen) [ist keine existente tabelle!]
	define('PMS_CONTENT', 'pms_content', true);
	
// cms tables (default)
	$aENV['table']['cms_navi']		= 'cms_navi';
	$aENV['table']['cms_standard']	= 'cms_standard';

#-----------------------------------------------------------------------------
# 1b. global environment VARS (default):
#		-->> sollte von den Angaben in der "/config/setup.php" ueberschrieben werden!
#-----------------------------------------------------------------------------

// standart title (default)
	#$aENV['client_name']		= '_INFORM v1 Vorlage'; // used for title/etc.
	#$aENV['client_shortname']	= 'dummy'; // used for db-name/session-name/etc. (NO spaces etc!)
	#$aENV['web_url']			= 'www.url.de'; // URL der mit diesem CMS verwalteten Website (ohne "http://")

// DB-zugangsdaten ($aENV['db']) werden NUR in der "setup.php" definiert! Muster:
	#switch ($_SERVER['SERVER_NAME']) {
	#	case 'rhsrv':				// Dev-Server
	#		$aENV['db']['host'] = 'localhost';
	#		$aENV['db']['db']   = 'pms';
	#		$aENV['db']['user'] = 'root';
	#		$aENV['db']['pass'] = '';
	#		break;
	#	case 'www.kundenserver.com':	// Live-Server
	#		$aENV['db']['host'] = '';
	#		$aENV['db']['db']   = '';
	#		$aENV['db']['user'] = '';
	#		$aENV['db']['pass'] = '';
	#		break;
	#}

// NOTE: die jeweilige reihenfolge ist auch ausgabe-reihenfolge (bspw. in einem select-field)

// Navigation
	$aENV['navigation_open']	= false;
//Copyright
	$aENV['system_copyright'] = true;

// available system-languages (default)
	$aENV['system_language'] = array('default' => 'de'); // set default [ de | en ]
	$aENV['system_language']['de'] = 'deutsch';
	#$aENV['system_language']['en'] = 'english';

// available WEBSITE content-languages (default)
	$aENV['content_language'] = array('default' => 'de'); // set default [ de | en | ... ]
	$aENV['content_language']['de'] = 'deutsch';
	#$aENV['content_language']['en'] = 'english';

// Anzeige-Formatierung der Nutzernamen (default)
	$aENV['username_format'] = 'firstname'; // oder z.B. "surname, firstname"... ('firstname' und 'surname' werden durch die entsprechenden values ersetzt)

// available modules
	// diese eintraege werden in ser System-Navigation angezeigt.
	// kann einsprachig (dann ohne sprachkeys ['de'],['en'],...) 
	// ODER mehrsprachig (dann mit sprachkeys fuer jede content-language) verarbeitet werden!
	$aENV['module'] = array();
	#$aENV['module']['adr']['de']	= 'Adressen';
	#$aENV['module']['adr']['en']	= 'Addresses';
	#$aENV['module']['pms']		= 'PMS';
	#$aENV['module']['cms']		= 'CMS';

// Portal-MODULE, die angezeigt werden sollen
	$aENV['portal_module'] = array('L', 'R');
	#$aENV['portal_module']['L'][] = 'adm_prj_jumper';
	#$aENV['portal_module']['R'][] = 'frm_recent';
	#...

// SUB-Module CMS
	// CUG (default)
	// true, wenn CUG(s) angelegt + Seiten fuer diese geschuetzt werden koennen soll
	$aENV['allow_cug']			= false;
	
	// Newsletter (default)
	// true, wenn Newsletter verwaltet werden koennen sollen
	$aENV['allow_newsletter']	= false;
	
	// Mehrsprachenfähigkeit für die Tabellenfelder im Internal linkmanager
	$aENV['linkman']['multilang'] = true;
	###// ClientSites (default)
	###// true, wenn ClientSites verwaltet werden koennen sollen
	###$aENV['allow_clientsite']	= false;
	### -> sind jetzt ein eigenes Modul!

// FTP (default: false)
	// wenn ein FTP Verzeichnis auf dem Server dem Kunden bekannt ist und er per PopUp auf diese Dateien zugreifen koennen soll
	$aENV['allow_ftp']			= false;
	$aENV['ftp_folder']			= "ftp/"; // Name des FTP-Verzeichnisses (von der HTTP-Server-Root aus gesehen)
	
#-----------------------------------------------------------------------------
# 1c. global module VARS: (default):
#		-->> sollte von den Angaben in der "/config/setup.php" ueberschrieben werden!
#-----------------------------------------------------------------------------


// MDB ***********************************************************************
	
// Angezeigte Bezeichnung fuer "slideshow" (default)
	$aENV['slideshow'] = array();
	$aENV['slideshow']['de'] = 'Slideshow';
	$aENV['slideshow']['en'] = 'slideshow';
	
// maximale anzahl der bilder, die in die slide geladen werden duerfen (default)
	$aENV['max_slide_images'] = 5;
	
// thumb-groesse (nur breite), die das Script "mdb/thumb.php" einheitlich erzeugen soll
	define('MDB_THUMB_WIDTH', 50, true);

// PMS ***********************************************************************
if (strstr($_SERVER['PHP_SELF'], 'pms/')) {
// available mediatypes (default)
	// sind bewusst im singular und sprachneutral gehalten (ums einfacher zu halten)
	// wird z.b. zur erzeugung von dropdownlisten benutzt (als 'key' NUR mimetype-keys verwenden)
	$aENV['mediatype'] = array('default' => 'image'); // set default [ image | swf | ... ]
	$aENV['mediatype']['image']	= 'image';
	$aENV['mediatype']['flash']	= 'flash';
	
// Name des Presentation-Abspiel-tools (Flash-Film) (default)
	$aENV['template'] = array();
	$aENV['template']['1']['filename']		= 'template_default.swf';
	$aENV['template']['1']['title']			= 'Default';
	$aENV['template']['1']['imgwidth_full']		= '1024';	// max-width	(larger image 1)
	$aENV['template']['1']['imgheight_full']	= '744';	// max-height	(larger image 1)
	$aENV['template']['1']['imgwidth_half']		= '512';	// max-width	(smaller image 2)
	$aENV['template']['1']['imgheight_half']	= '744';	// max-height	(smaller image 2)
	$aENV['template']['1']['imgwidth_small']	= '480';	// max-width	(thumbnail image 3)
	$aENV['template']['1']['imgheight_small']	= '160';	// max-height	(thumbnail image 3)
	
	$aENV['imgwidth_bleed']		= '1024';	// max-width	(fullscreen)
	$aENV['imgheight_bleed']	= '768';	// max-height	(fullscreen)
	
// Komprimierungs-Verfahren, das zur Archivierung benutzt wird (moeglich ist "pclzip", "zip" oder "tar")
	$aENV['archive_type']			= 'pclzip';		// [pclzip|zip|tar]	(default: pclzip)
	$aENV['archive_ext']['pclzip']	= '.zip';		// Dateiendung fuer zip (inkl. "." (Punkt) vor der Dateiendung)
	$aENV['archive_ext']['zip']		= '.zip';		// Dateiendung fuer zip (inkl. "." (Punkt) vor der Dateiendung)
	$aENV['archive_ext']['tar']		= '.tar.gz';	// Dateiendung fuer tar (inkl. "." (Punkt) vor der Dateiendung)

} // END if PMS

// CMS ***********************************************************************
if (strstr($_SERVER['PHP_SELF'], 'cms/') 
|| !strstr($_SERVER['PHP_SELF'], INFORMROOT)) {
	
// Anzahl + Bezeichnungen fuer die Content-Navigations-Baeume (default)
	$aENV['content_navi'][0]['de'] = 'Content-Navigation';
	$aENV['content_navi'][0]['en'] = 'Content navigation';
	
// Anzahl moeglicher Navigations-Level bzw. Navigationstiefe (default)
	$aENV['cms_navi_sublevel'] = 2; // [0 = nur HauptNavi | 1 = HauptNavi+SubNavi | 2 = HauptNavi+SubNavi+SubSubNavi ]
	
// Zeitgesteuert Navigationspunkte ein-/auszublenden (default)
	$aENV['cms_navi_onlinefromto'] = false; // [ true | false ]
	// default-start-value bei eingeblendeter Zeitsteuerung (default: null [=> '0000-00-00'])
	$aENV['cms_navi_onlinefromto_default_start'] = 'null'; // [ 'null' | 'today' ]
	// Art das Datum auszuwaehlen bei eingeblendeter Zeitsteuerung (default: dropdown [erzeugt '$oForm->date_dropdown()'])
	$aENV['cms_navi_onlinefromto_dateselect'] = 'dropdown'; // [ 'dropdown' | 'popup' ]
	
// available mediatypes (default)
	// sind bewusst im singular und sprachneutral gehalten (ums einfacher zu halten)
	// wird z.b. zur erzeugung von dropdownlisten benutzt (als 'key' NUR mimetype-keys verwenden)
	$aENV['mediatype'] = array('default' => 'image'); // set default [ image | swf | ... ]
	$aENV['mediatype']['image']	= 'image';
	$aENV['mediatype']['flash']	= 'flash';
	$aENV['mediatype']['pdf']	= 'pdf';
	$aENV['mediatype']['zip']	= 'zip';
	#$aENV['mediatype']['txt']	= 'txt';
	#$aENV['mediatype']['audio']	= 'audio';
	#$aENV['mediatype']['video']	= 'video';

// ein bild einem navigationspunkt zuweisen koennen? (default)
	// (taucht auf "navi_detail.php" auf - oder nicht)
	$aENV['pic_on_navi'] = 'all'; // [ 'all'=alle duerfen | 'da'=nur da darf | 'no'=keiner darf ]
	$aENV['pic_on_navi_type'] = 'mdb'; // [ 'mdb'=bild aus der mediadb zuweisen | 'txt'=textfeld ]

// in den meta-angaben eine standard email-adresse eintragen koennen? (default)
	// (taucht auf "meta.php" auf - oder nicht)
	$aENV['email_im_meta'] = true; // [ true (default) | false ]

// available templates (default)
	// (zusaetzlich zu den basis-templates 'standard','home','static','no_page')
	// bitte mehrsprachig angeben (mit sprachkey(s) ['de'],['en'],...) - fuer jede content-language!
	$aENV['template'] = array();
	#$aENV['template']['press']['de']	= 'Presse Seite'; // 2. array-key sollte DB-table sein
	#$aENV['template']['press']['en']	= 'press page';

// don't allow (invisible) subpages within these templates:
	$aENV['no_sub_templates'] = array();

// available layouts (default)
	// zusaetzliche moeglichkeit ein template in max.4(!) versionen (A|B|C|D) anzubieten. 
	// z.b. mit 1 oder 2 spaltigem text, oder die overviewseite nach datum oder prio sortiert,...
	// diese eintraege werden in "navi_detail.php" unterhalb von "template" angezeigt.
	// (auswirkungen muesssen in den jew. templates programmiert werden! z.b. fallunterscheidung bei "ORDER BY",...)
	// kann einsprachig (dann ohne sprachkeys ['de'],['en'],...) oder mehrsprachig (dann mit sprachkeys fuer jede content-language) verarbeitet werden!
	$aENV['layout'] = array();
	#$aENV['layout']['standard']['A']	= 'Content (mit Inhalt)';
	#$aENV['layout']['standard']['B']	= 'Overview (ohne Inhalt)';
	# oder
	#$aENV['layout']['press']['A']['de']	= 'PRESSE: neustes Datum oben';
	#$aENV['layout']['press']['A']['en']	= 'PRESS: newest date on top';
	#$aENV['layout']['press']['B']['de']	= 'PRESSE: NUR Letzte Meldung';
	#$aENV['layout']['press']['B']['en']	= 'PRESS: newest press report ONLY';

// templates, die im internal-linkmanager ausgegeben werden sollen [ADDED: 2003-0716af]
	// (ACHTUNG: mit einer variable zusammengestzte feldbezeichner (z.B.'title_'.$weblang) gehen hier nicht!!!)
	$aENV['int_linkman'] = array();
	#$aENV['int_linkman']['BEISPIEL']['fieldname']	= 'name';		// fieldname: nur angeben wenn nicht default ('title_'.$weblang) !
	#$aENV['int_linkman']['BEISPIEL']['orderby']	= 'name DESC';	// orderby: nur angeben wenn nicht default ('title_'.$weblang.' DESC') !

} // END if CMS

// root path
	// automatische pfaderkennung - funktioniert NUR fuer dateien in den verzeichnissen "/(projekt-webroot)" und "/(projekt-webroot)/cms/" !
	$aENV['path'] = array();
	#
	# NOTE: Unterscheidung ['unix'] | ['http']
	# wenn man ein file auf den server speichern moechte, braucht man die absolute adresse (file-protokoll)
	# dazu wird hier $_SERVER['DOCUMENT_ROOT'] benutzt (ausgabe-bsp: /usr/local/apache/htdocs/).
	# Wenn man hingegen ein gespeichertes file im browser anzeigen moechte, braucht man den pfad dorthin
	# im http-protokoll. die root muss also die webserver-root sein: (ausgabe-bsp: /index.php oder http://www.dom.de/index.php)
	#
	$aReplace = array(	'/'.INFORMROOT,				// fuer INFORM-Verzeichnis
						'/sys/',				// fuer system-Verzeichnis
						'/php/',				// fuer sonderfall: "info.php"
						'/mdb/',				// fuer MediaDB-Verzeichnis
						'/arc/',				// fuer ARCHIV-Verzeichnis
						'/adr/',				// fuer ADR-Verzeichnis
						'/cal/',				// fuer CAL-Verzeichnis
						'/frm/',				// fuer FORUM-Verzeichnis
						'/adm/',				// fuer ADM-Verzeichnis
						'/pms/',				// fuer PMS-Verzeichnis
						'/cms/',				// fuer CMS-Verzeichnis
						'/qst/',				// fuer QST-Verzeichnis
						'/clientsites/',		// fuer CMS-clientsites-Verzeichnis
						'/mobile/',				// fuer WAP/PDA/...
						'/dbgui/'				// fuer phpMyAdmin
				);

	$path	=  str_replace($aReplace, '/', $_SERVER['PHP_SELF']);
	$path	=  str_replace(basename($_SERVER['PHP_SELF']), '', $path);
	
	$aENV['path']['root']['unix']	= $_SERVER['DOCUMENT_ROOT'].$path;
	$aENV['path']['root']['http']	= $path;

#-----------------------------------------------------------------------------

// CHARSET default
	$aENV['charset']	= 'UTF-8';
	 
// HTTPS default
	$aENV['https']['inform']	= false; // [true|false] (bei true wird unterhalb von "/inform/" allen http-pfaden "https://" vorangestellt)
	$aENV['https']['web']		= false; // [true|false] (bei true wird i.d. Webausgabe allen http-pfaden "https://" vorangestellt)

	// Systemnutzer unter MBL einblenden
	$aENV['mbl']['showSystemUser'] = false;
	
	// Cal
	$aENV['flag_confirmed']	= false;
	$aENV['mapLink']		= false;

#-----------------------------------------------------------------------------

//	customer-specific VARS ueberschreiben die default-werte!
	require_once ($aENV['path']['root']['unix'].INFORMROOT.'sys/config/setup.php'); // $aENV['path']['root']['unix'] muss vorhanden sein!

	// cal setup has to be required for all modules
	if(file_exists($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."cal.config.php")) {
		require_once($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."cal.config.php");
	}

	if(file_exists($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."adr.config.php")) {
		require_once($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."adr.config.php");
	}
		
	if(strstr($_SERVER['PHP_SELF'], "adm")
	|| (strstr($_SERVER['PHP_SELF'], "sys")
	&& isset($aENV['module']['adm']))) {
		require_once($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."adm.config.php");
	}


	if(strstr($_SERVER['PHP_SELF'], "pms")
	&& file_exists($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."pms.config.php")) {
		require_once($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."pms.config.php");
	}

	if(file_exists($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."cms.config.php")) {
		require_once($aENV['path']['root']['unix'].INFORMROOT.'sys/config/'."cms.config.php");
	}

	
#-----------------------------------------------------------------------------
	// ggf. UTF-8 header senden (kann unterbunden werden, indem in der jeweiligen seite die konstante 'DONT_SEND_HEADERS' definiert wird)
	if ($aENV['charset'] == 'UTF-8' && !defined('DONT_SEND_HEADERS')) {
		header('Content-Type: text/html; charset="UTF-8"');
	}
#-----------------------------------------------------------------------------
	
	// shortcuts
	$aENV['PHP_SELF']						= $_SERVER['PHP_SELF'];
	$aENV['SELF_PATH']						= str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);
	
	// in INFORM alles absolut machen (fuer HTTP/1.1-konforme header-anweisungen)
	if ($aENV['https']['inform'] == false && strstr($_SERVER['PHP_SELF'], INFORMROOT)) {
		// root
		$aENV['path']['root']['http']		= 'http://'.$_SERVER['HTTP_HOST'].$path;
		// shortcuts
		$aENV['PHP_SELF']					= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		$aENV['SELF_PATH']					= 'http://'.$_SERVER['HTTP_HOST'].$aENV['SELF_PATH'];
	}
	
	// HTTPS-Varianten
	if ( ($aENV['https']['web'] == true && !strstr($_SERVER['PHP_SELF'], INFORMROOT))
		|| ($aENV['https']['inform'] == true && strstr($_SERVER['PHP_SELF'], INFORMROOT)) ) {
		// root
		$aENV['path']['root']['http']		= 'https://'.$_SERVER['HTTP_HOST'].$path;
		// shortcuts
		$aENV['PHP_SELF']					= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		$aENV['SELF_PATH']					= 'https://'.$_SERVER['HTTP_HOST'].$aENV['SELF_PATH'];
	}
	unset($path);
	
// standard paths
	// INFORM-ROOT
	$aENV['path']['inform']['unix']			= $aENV['path']['root']['unix'].INFORMROOT;
	$aENV['path']['inform']['http']			= $aENV['path']['root']['http'].INFORMROOT;
	// system:
	$aENV['path']['sys']['unix']			= $aENV['path']['inform']['unix'].'sys/';
	$aENV['path']['sys']['http']			= $aENV['path']['inform']['http'].'sys/';
	$aENV['path']['config']['unix']			= $aENV['path']['sys']['unix'].'config/';
	$aENV['path']['config']['http']			= $aENV['path']['sys']['http'].'config/';
	$aENV['path']['php']['unix']			= $aENV['path']['sys']['unix'].'php/';
	$aENV['path']['php']['http']			= $aENV['path']['sys']['http'].'php/';
	$aENV['path']['3rd_party']['unix']		= $aENV['path']['php']['unix'].'3rdparty/';
	$aENV['path']['3rd_party']['http']		= $aENV['path']['php']['http'].'3rdparty/';
	$aENV['path']['php_global']['unix']		= $aENV['path']['php']['unix'].'global/';
	$aENV['path']['php_global']['http']		= $aENV['path']['php']['http'].'global/';
	$aENV['path']['global_module']['unix']	= $aENV['path']['php_global']['unix'].'module/';
	$aENV['path']['global_module']['http']	= $aENV['path']['php_global']['http'].'module/';
	$aENV['path']['global_service']['unix']	= $aENV['path']['php_global']['unix'].'service/';
	$aENV['path']['global_service']['http']	= $aENV['path']['php_global']['http'].'service/';
	$aENV['path']['global_bean']['unix']	= $aENV['path']['php_global']['unix'].'bean/';
	$aENV['path']['global_bean']['http']	= $aENV['path']['php_global']['http'].'bean/';
	$aENV['path']['pear']['unix']			= $aENV['path']['php']['unix'].'PEAR/';
	$aENV['path']['pear']['http']			= $aENV['path']['php']['http'].'PEAR/';
	$aENV['path']['js']['unix']				= $aENV['path']['sys']['unix'].'scripts/';
	$aENV['path']['js']['http']				= $aENV['path']['sys']['http'].'scripts/';
	$aENV['path']['css']['unix']			= $aENV['path']['sys']['unix'].'styles/';
	$aENV['path']['css']['http']			= $aENV['path']['sys']['http'].'styles/';
	$aENV['path']['pix']['unix']			= $aENV['path']['sys']['unix'].'pix/';
	$aENV['path']['pix']['http']			= $aENV['path']['sys']['http'].'pix/';
	$aENV['path']['pix_global']['unix']		= $aENV['path']['pix']['unix'].'global/';
	$aENV['path']['pix_global']['http']		= $aENV['path']['pix']['http'].'global/';
	// nachdem pixglobal definiert ist, wird ggf. pix ueberschrieben...
	if (file_exists($aENV['path']['config']['unix']."/pix")) {
	$aENV['path']['pix']['unix']			= $aENV['path']['config']['unix'].'pix/';
	$aENV['path']['pix']['http']			= $aENV['path']['config']['http'].'pix/';
	}
	// neue data ordner struktur
	$aENV['path']['data']['unix']			= $aENV['path']['root']['unix'].'data/';
	$aENV['path']['data']['http']			= $aENV['path']['root']['http'].'data/';

	$aENV['path']['sys_data']['unix']			= $aENV['path']['data']['unix'].'sys/';
	$aENV['path']['sys_data']['http']			= $aENV['path']['data']['http'].'sys/';
	
	$aENV['path']['dbgui']['unix']			= $aENV['path']['inform']['unix'].'dbgui/';
	$aENV['path']['dbgui']['http']			= $aENV['path']['inform']['http'].'dbgui/';
	$aENV['path']['ftp']['unix']			= $aENV['path']['root']['unix'].$aENV['ftp_folder'];
	$aENV['path']['ftp']['http']			= $aENV['path']['root']['http'].$aENV['ftp_folder'];

// MediaDB ***********************************************************************
	$aENV['path']['mdb']['unix']				= $aENV['path']['inform']['unix'].'mdb/';
	$aENV['path']['mdb']['http']				= $aENV['path']['inform']['http'].'mdb/';
	// neue data ordner struktur
	$aENV['path']['mdb_data']['unix']			= $aENV['path']['data']['unix'].'mdb/';
	$aENV['path']['mdb_data']['http']			= $aENV['path']['data']['http'].'mdb/';
	
	$aENV['path']['mdb_upload']['unix']			= $aENV['path']['root']['unix'].'media/';
	$aENV['path']['mdb_upload']['http']			= $aENV['path']['root']['http'].'media/';
	$aENV['path']['mdb_upload_cache']['unix']	= $aENV['path']['mdb_upload']['unix'].'cache/';
	$aENV['path']['mdb_upload_cache']['http']	= $aENV['path']['mdb_upload']['http'].'cache/';
	$aENV['path']['mdb_xml']['unix']			= $aENV['path']['mdb_data']['unix'].'xml/';
	$aENV['path']['mdb_xml']['http']			= $aENV['path']['mdb_data']['http'].'xml/';
	// Konstante fuer das Array-File das die Media-Tree-Struktur abbildet
	define('MDB_STRUCTURE_FILE', $aENV['path']['mdb_data']['unix'].'array.tree.php', true);

// ADR ***********************************************************************
	$aENV['path']['adr']['unix']			= $aENV['path']['inform']['unix'].'adr/';
	$aENV['path']['adr']['http']			= $aENV['path']['inform']['http'].'adr/';
	// neue data ordner struktur
	$aENV['path']['adr_data']['unix']			= $aENV['path']['data']['unix'].'adr/';
	$aENV['path']['adr_data']['http']			= $aENV['path']['data']['http'].'adr/';

// ARC ***********************************************************************
	$aENV['path']['arc']['unix']			= $aENV['path']['inform']['unix'].'arc/';
	$aENV['path']['arc']['http']			= $aENV['path']['inform']['http'].'arc/';
	// neue data ordner struktur
	$aENV['path']['arc_data']['unix']			= $aENV['path']['data']['unix'].'arc/';
	$aENV['path']['arc_data']['http']			= $aENV['path']['data']['http'].'arc/';
	
	// Konstante fuer das Array-File das die Archiv-Tree-Struktur abbildet
	define('ARCHIV_STRUCTURE_FILE', $aENV['path']['arc_data']['unix'].'array.tree.php', true);

// CAL ***********************************************************************
	$aENV['path']['cal']['unix']			= $aENV['path']['inform']['unix'].'cal/';
	$aENV['path']['cal']['http']			= $aENV['path']['inform']['http'].'cal/';
	// neue data ordner struktur
	$aENV['path']['cal_data']['unix']			= $aENV['path']['data']['unix'].'cal/';
	$aENV['path']['cal_data']['http']			= $aENV['path']['data']['http'].'cal/';

// MOBILE ********************************************************************
	$aENV['path']['mobile']['unix']			= $aENV['path']['inform']['unix'].'mobile/';
	$aENV['path']['mobile']['http']			= $aENV['path']['inform']['http'].'mobile/';
	// neue data ordner struktur
	$aENV['path']['mobile_data']['unix']			= $aENV['path']['data']['unix'].'mobile/';
	$aENV['path']['mobile_data']['http']			= $aENV['path']['data']['http'].'mobile/';

// FORUM ***********************************************************************
	$aENV['path']['frm']['unix']			= $aENV['path']['inform']['unix'].'frm/';
	$aENV['path']['frm']['http']			= $aENV['path']['inform']['http'].'frm/';
	// neue data ordner struktur
	$aENV['path']['frm_data']['unix']		= $aENV['path']['data']['unix'].'frm/';
	$aENV['path']['frm_data']['http']		= $aENV['path']['data']['http'].'frm/';
	// Konstante fuer das Array-File das die Forum-Tree-Struktur abbildet
	define('FORUM_STRUCTURE_FILE', $aENV['path']['frm_data']['unix'].'array.tree.php', true);

// ADM ***********************************************************************
	$aENV['path']['adm']['unix']			= $aENV['path']['inform']['unix'].'adm/';
	$aENV['path']['adm']['http']			= $aENV['path']['inform']['http'].'adm/';
	// neue data ordner struktur
	$aENV['path']['adm_data']['unix']		= $aENV['path']['data']['unix'].'adm/';
	$aENV['path']['adm_data']['http']		= $aENV['path']['data']['http'].'adm/';

// PMS ***********************************************************************
	$aENV['path']['pms']['unix']			= $aENV['path']['inform']['unix'].'pms/';
	$aENV['path']['pms']['http']			= $aENV['path']['inform']['http'].'pms/';
	
	$aENV['path']['pms_presentation']['unix']	= $aENV['path']['pms']['unix'].'presentation/';
	$aENV['path']['pms_presentation']['http']	= $aENV['path']['pms']['http'].'presentation/';

	// neue data ordner struktur
	$aENV['path']['pms_data']['unix']		= $aENV['path']['data']['unix'].'pms/';
	$aENV['path']['pms_data']['http']		= $aENV['path']['data']['http'].'pms/';
	$aENV['path']['pms_archive']['unix']	= $aENV['path']['pms_data']['unix'].'archive/';
	$aENV['path']['pms_archive']['http']	= $aENV['path']['pms_data']['http'].'archive/';
	$aENV['path']['pms_xml']['unix']		= $aENV['path']['pms_data']['unix'].'xml/';
	$aENV['path']['pms_xml']['http']		= $aENV['path']['pms_data']['http'].'xml/';

// CMS ***********************************************************************
	$aENV['path']['cms']['unix']			= $aENV['path']['inform']['unix'].'cms/';
	$aENV['path']['cms']['http']			= $aENV['path']['inform']['http'].'cms/';

	// WEBSITE
	$aENV['path']['web']['unix']			= $aENV['path']['root']['unix'].'web/';
	$aENV['path']['web']['http']			= $aENV['path']['root']['http'].'web/';

	// CLIENTSITES
	$aENV['path']['clientsites']['unix']	= $aENV['path']['inform']['unix'].'clientsites/';
	$aENV['path']['clientsites']['http']	= $aENV['path']['inform']['http'].'clientsites/';
	$aENV['path']['cms_data']['unix']		= $aENV['path']['data']['unix'].'cms/';
	$aENV['path']['cms_data']['http']		= $aENV['path']['data']['http'].'cms/';

	// tools
if (strstr($_SERVER['PHP_SELF'], 'cms/') 
|| !strstr($_SERVER['PHP_SELF'], INFORMROOT) 
|| strstr($_SERVER['PHP_SELF'], 'clientsites/')) {
	$aENV['path']['upload']['unix']			= $aENV['path']['cms_data']['unix'].'upload/';
	$aENV['path']['upload']['http']			= $aENV['path']['cms_data']['http'].'upload/';
	$aENV['path']['upload_cache']['unix']	= $aENV['path']['upload']['unix'].'cache/';
	$aENV['path']['upload_cache']['http']	= $aENV['path']['upload']['http'].'cache/';
	$aENV['path']['protected']['unix']		= $aENV['path']['cms_data']['unix'].'protected/';
	$aENV['path']['protected']['http']		= $aENV['path']['cms_data']['http'].'protected/';
	$aENV['path']['protected_cache']['unix']= $aENV['path']['protected']['unix'].'cache/';
	$aENV['path']['protected_cache']['http']= $aENV['path']['protected']['http'].'cache/';
	$aENV['path']['xml']['unix']			= $aENV['path']['cms_data']['unix'].'xml/';
	$aENV['path']['xml']['http']			= $aENV['path']['cms_data']['http'].'xml/';
	$aENV['path']['templates']['unix']		= $aENV['path']['web']['unix'].'templates/';
	$aENV['path']['templates']['http']		= $aENV['path']['web']['http'].'templates/';
}
// QST ***********************************************************************
	$aENV['path']['qst']['unix']			= $aENV['path']['inform']['unix'].'qst/';
	$aENV['path']['qst']['http']			= $aENV['path']['inform']['http'].'qst/';

// for PEAR compatibility: add PEAR directory to default include path!
	ini_set('include_path', '.:'.$aENV['path']['php']['unix'].'PEAR');

// standard pages (default)
	$aENV['page'] = array();
	$aENV['page']['sys_welcome']= $aENV['path']['sys']['http'].'sys_portal.php';
	$aENV['page']['pms_welcome']= $aENV['path']['pms']['http'].'pms_portal.php';
	$aENV['page']['cms_welcome']= $aENV['path']['cms']['http'].'cms_portal.php';
	$aENV['page']['adr_welcome']= $aENV['path']['adr']['http'].'adr.php';
	$aENV['page']['frm_welcome']= $aENV['path']['frm']['http'].'frm.php';
	$aENV['page']['arc_welcome']= $aENV['path']['arc']['http'].'arc.php';
	$aENV['page']['cal_welcome']= $aENV['path']['cal']['http'].'cal_calendar.php';
	$aENV['page']['adm_welcome']= $aENV['path']['adm']['http'].'adm_portal.php';
	$aENV['page']['mdb_welcome']= $aENV['path']['mdb']['http'].'mdb.php';
	$aENV['page']['mdb_detail']	= $aENV['path']['mdb']['http'].'mdb_detail.php';
	$aENV['page']['logout']		= $aENV['path']['sys']['http'].'sys_portal.php?logout=true';
	$aENV['page']['disallow']	= $aENV['path']['sys']['http'].'sys_disallow.php';
	$aENV['page']['user']		= $aENV['path']['sys']['http'].'sys_user.php';
	$aENV['page']['mobile_welcome']	= $aENV['path']['mobile']['http'].'index.php';
	/* Pfad muss absolut (mit host) sein, sonst fehler des location-redirects in "/cms/index.php" */

#-----------------------------------------------------------------------------
# 1d. global environment VARS: (not customer-specific)
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
#	ALLES WEITERE NICHT IM dbgui-VERZEICHNIS!
if (!strstr($_SERVER['PHP_SELF'], "dbgui/")) {
	
#-----------------------------------------------------------------------------
#	2a. global MESSAGES-ARRAY (default):
	require_once ($aENV['path']['php']['unix']."sys_messages.php");
		### das Array ist $aMSG benannt (vorsicht: nicht ueberschreiben!) ###
	
#-----------------------------------------------------------------------------
#	2b. modules MESSAGES-ARRAY:

//	MDB
	if (file_exists($aENV['path']['php']['unix']."mdb_messages.php")) {
		require_once ($aENV['path']['php']['unix']."mdb_messages.php");
	}
	
//	ADR
	if ((strstr($_SERVER['PHP_SELF'], "adr/") 
		|| strstr($_SERVER['PHP_SELF'], "mobile/"))  
		&& file_exists($aENV['path']['php']['unix']."adr_messages.php")) {
			require_once ($aENV['path']['php']['unix']."adr_messages.php");
			require_once($aENV['path']['config']['unix']."adr.config.php");
	}
	
//	CAL
	if ((strstr($_SERVER['PHP_SELF'], "cal/") 
		|| strstr($_SERVER['PHP_SELF'], "mobile/")) 
		&& file_exists($aENV['path']['php']['unix']."cal_messages.php")) {
			require_once ($aENV['path']['php']['unix']."cal_messages.php");
	}
	
//	FORUM
	if (strstr($_SERVER['PHP_SELF'], "frm/") 
		&& file_exists($aENV['path']['php']['unix']."frm_messages.php")) {
			require_once ($aENV['path']['php']['unix']."frm_messages.php");
	}
	
//	ADM
	if ((strstr($_SERVER['PHP_SELF'], "adm/") 
		|| strstr($_SERVER['PHP_SELF'], "mobile/")) 
		&& file_exists($aENV['path']['php']['unix']."adm_messages.php")) {
			require_once ($aENV['path']['php']['unix']."adm_messages.php");
	}
	
//	CMS
	if ((strstr($_SERVER['PHP_SELF'], "cms/") 
		|| strstr($_SERVER['PHP_SELF'], "clientsites/") 
		|| !strstr($_SERVER['PHP_SELF'], INFORMROOT)) 
		&& file_exists($aENV['path']['php']['unix']."cms_messages.php")) {
			require_once ($aENV['path']['php']['unix']."cms_messages.php");
	}
	
//	PMS
	if (strstr($_SERVER['PHP_SELF'], "pms/") 
		&& file_exists($aENV['path']['php']['unix']."pms_messages.php")) {
			require_once ($aENV['path']['php']['unix']."pms_messages.php");
	}
	
//	ARC
	if (strstr($_SERVER['PHP_SELF'], "arc/") 
		&& file_exists($aENV['path']['php']['unix']."arc_messages.php")) {
			require_once ($aENV['path']['php']['unix']."arc_messages.php");
	}
	
//	MBL
	if (strstr($_SERVER['PHP_SELF'], "mobile/") 
		&& file_exists($aENV['path']['php']['unix']."mbl_messages.php")) {
			require_once ($aENV['path']['php']['unix']."mbl_messages.php");
	}

#-----------------------------------------------------------------------------
#	2c. customer-specific MESSAGES-ARRAY:
	
	if (file_exists($aENV['path']['config']['unix']."messages.php")) {
		require_once ($aENV['path']['config']['unix']."messages.php");
	}
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# 	3. global CLASSES:
#-----------------------------------------------------------------------------

// Browser-/Platform-Detection
	require_once ($aENV['path']['global_service']['unix']."class.browsercheck.php");
		$oBrowser =& new browsercheck();
// DB
	if($aENV['php5'] && extension_loaded('mysqli')) {
		require_once ($aENV['path']['global_service']['unix']."class.db_mysql.php"); // edited to mysql class because php5 doesn;t work
		$oDb = new dbconnect($aENV['db']);
	} else {
		require_once ($aENV['path']['global_service']['unix']."class.db_mysql.php");
		$oDb =& new dbconnect($aENV['db']);
	}
	
// DEBUG FOR DB by receiving Parameter via URL
	if($_GET['debug']=='all'
	|| $_GET['debug']=='mysql') {
		$oDb->debug	= true;	
	}
			
// User
	require_once ($aENV['path']['global_service']['unix']."class.user.php");
// File-Upload
	require_once ($aENV['path']['global_service']['unix']."class.upload.php");
	#### $oUpload1 =& new upload("picture1"); // nur bei bedarf
// File-System
	require_once ($aENV['path']['global_service']['unix']."class.filesystem.php");
		$oFile =& new filesystem();
// Form-Checker
	require_once ($aENV['path']['global_service']['unix']."class.form_checker.php");
		$oFc =& new formchecker();
// Datum
	require_once ($aENV['path']['global_service']['unix']."class.datetime.php");
	require_once ($aENV['path']['global_service']['unix']."class.datetime_db.php");
		$oDate =& new datetime_db();
	
// CMS-Navi (baut auf "class.db_mysql.php" und $aENV auf!)
	if (strstr($_SERVER['PHP_SELF'], "cms/") || !strstr($_SERVER['PHP_SELF'], INFORMROOT) || strstr($_SERVER['PHP_SELF'], "clientsites/")) {
		require_once ($aENV['path']['global_module']['unix']."class.cms_navi.php");
		$oNav =& new cms_navi(); // params: -
	}
// Session
	require_once($aENV['path']['global_service']['unix']."class.session.php");

// PHPLIB-Templatesystem
	include_once $aENV['path']['php']['unix']."PEAR/PHPLIB.php";

// Forms
	require_once ($aENV['path']['global_service']['unix']."class.form_web.php");

// Formular-Klasse fuer Admin-Seiten (baut auf anderen Klassen auf!)
	if (strstr($_SERVER['PHP_SELF'], INFORMROOT)) {
		require_once ($aENV['path']['global_service']['unix']."class.form_admin.php");

	}
	
	if(strstr($_SERVER['PHP_SELF'],'arc/')) {
		if(!isset($aENV['php5']) || !$aENV['php5']) {
			require_once($aENV['path']['global_module']['unix'].'class.archiv.php');
			$oArchiv =& new Archiv($oDb,$aENV);	
		} else {
			require_once($aENV['path']['global_module']['unix'].'class.archiv.php5');
			$oArchiv = new Archiv($oDb,$aENV);
		}
			
		$oArchiv->setUserdata($Userdata);
	}
	
// Beans
	require_once ($aENV['path']['global_service']['unix']."class.BeanService.php");

#-----------------------------------------------------------------------------
#	4a. global FUNCTIONS (default):
#-----------------------------------------------------------------------------
	
	// Funktionen fuer Admin-Seiten (allgemein - baut auf anderen Klassen auf!)
	require_once ($aENV['path']['global_service']['unix']."func.admin_common.php");

	// Funktionen fuer Admin-Viewer-Seiten (baut auf anderen Klassen auf!)
	require_once ($aENV['path']['global_service']['unix']."func.admin_view.php");

	// functions for web-/admin-pages
	require_once ($aENV['path']['global_service']['unix']."func.common.php");
	
	// PMS ***********************************************************************
	if (strstr($_SERVER['PHP_SELF'], "pms/")) {
		// Funktionen fuer PMS-Seiten (baut auf anderen Klassen auf!)
		require_once ($aENV['path']['global_module']['unix']."func.pms_common.php");
	}

	if(!$aENV['php5']) {
		include_once($aENV['path']['global_service']['unix'].'class.TeamAccessControllEA.php');
		include_once($aENV['path']['global_bean']['unix'].'class.TeamAccessControllBean.php');
		include_once($aENV['path']['global_service']['unix'].'class.team.php');
		include_once($aENV['path']['global_service']['unix'].'class.FSFolder.php');
		include_once($aENV['path']['global_service']['unix'].'class.perm.php');
		include_once($aENV['path']['global_service']['unix'].'class.Label.php');
		include_once($aENV['path']['global_service']['unix'].'class.Tools.php');
	} else {
		include_once($aENV['path']['global_service']['unix'].'class.Label.php');
		include_once($aENV['path']['global_service']['unix'].'class.NullPointerException.php5');
		include_once($aENV['path']['global_service']['unix'].'class.Tools.php5');
		include_once($aENV['path']['global_service']['unix'].'class.TeamAccessControllEA.php');
		include_once($aENV['path']['global_bean']['unix'].'class.TeamAccessControllBean.php');
		include_once($aENV['path']['global_service']['unix'].'class.team.php');
		include_once($aENV['path']['global_service']['unix'].'class.FSFolder.php');
		include_once($aENV['path']['global_service']['unix'].'class.perm.php');
	}
	
	include_once($aENV['path']['3rd_party']['unix'].'charsetconverter/ConvertCharset.class.php'); // stellt $aSearch + $aReplace zur verfuegung
	
#-----------------------------------------------------------------------------
#	4b. global FUNCTIONS (customer-specific):
#-----------------------------------------------------------------------------
	
	if (file_exists($aENV['path']['config']['unix']."functions.php")) {
		require_once ($aENV['path']['config']['unix']."functions.php");
	}
	
#-----------------------------------------------------------------------------
} // END if !dbgui
#-----------------------------------------------------------------------------

if (strstr($_SERVER['PHP_SELF'], 'qst/')) { 
	if(file_exists($aENV['path']['qst']['unix'].'default.config.php')) { 
		require_once($aENV['path']['qst']['unix'].'default.config.php');
	}
}
?>