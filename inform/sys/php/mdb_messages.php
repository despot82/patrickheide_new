<?php
/**				***** MDB *****
*
* mdb_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.0 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	1.0 / 2005-01-07
*/

// Fehlermeldungen
	$aMSG['err']['delete_media']['de'] = "Diese Datei kann nicht gelöscht werden, da sie noch verwendet wird!";
	$aMSG['err']['delete_media']['en'] = "This file cannot be deleted, because it is still connected to a page on the website!";
	$aMSG['err']['clear_all_images']['de'] = "Möchten Sie alle Bilder entfernen?";
	$aMSG['err']['clear_all_images']['en'] = "Do you want to clear all images?";
	
	$aMSG['media']['alert_length']['de'] = "Der Dateiname ist zu lang. Bitte stellen Sie sicher, dass der Dateiname der hochzuladenden Datei nicht länger als 40 Zeichen ist!";
	$aMSG['media']['alert_length']['en'] = "The filename is too long. Please make sure that the filename does not exceed 40 characters!";
	$aMSG['media']['alert_chars']['de'] = "Der Dateiname beinhaltet ungültige Zeichen. Bitte stellen Sie sicher dass der Dateiname der hochzuladenden Datei keine Leer- oder Sonderzeichen hat!";
	$aMSG['media']['alert_chars']['en'] = "Please make sure you are not using spaces or symbols in your filename!";

// Formulare
	$aMSG['form']['zipentpacken']['de'] = "Zip Archiv";
	$aMSG['form']['zipentpacken']['en'] = "Zip archive";
	$aMSG['form']['zipentpacken1']['de'] = "Entpacken und einzeln sichern";
	$aMSG['form']['zipentpacken1']['en'] = "Extract and save the files";
	$aMSG['form']['zipentpacken2']['de'] = "Zip beibehalten";
	$aMSG['form']['zipentpacken2']['en'] = "Keep the Zip file";

// Hints zum Upload ("{SYMBOLS}" + "{UPLOAD_MAX_FILESIZE}" wird bei der Ausgabe ersetzt!)
	$aMSG['media']['hint_max_filesize']['de'] = 'Maximale Upload-Größe: {UPLOAD_MAX_FILESIZE}';
	$aMSG['media']['hint_max_filesize']['en'] = 'Maximum upload filesize: {UPLOAD_MAX_FILESIZE}';
	$aMSG['media']['hint_jpeg']['de'] = 'Für Präsentationen werden JPEGs benötigt. Diese müssen in RGB sowie nicht mit der Option "progressive" oder "interlaced" gespeichert sein.';
	$aMSG['media']['hint_jpeg']['en'] = 'For presentations please use jpgs saved as "RGB" and not "progressive" or "interlaced".';


// ############# Seitenspezifisch ##################################
// Media-DB
	$aMSG['mdb']['chooseall']['de'] = 'Alle auswählen';
	$aMSG['mdb']['chooseall']['en'] = 'Select all';
	$aMSG['mdb']['all_filetypes']['de'] = '- alle Dateitypen -';
	$aMSG['mdb']['all_filetypes']['en'] = '- all filetypes -';
	
	// media-tree
	$aMSG['media']['folder']['de'] = "Ordner";
	$aMSG['media']['folder']['en'] = "Folder";
	$aMSG['media']['folders']['de'] = "Ordner";
	$aMSG['media']['folders']['en'] = "Folders";
	$aMSG['media']['newfolder']['de'] = "Neuer Ordner";
	$aMSG['media']['newfolder']['en'] = "New folder";
	$aMSG['media']['all_folders']['de'] = "Alle Ordner";
	$aMSG['media']['all_folders']['en'] = "All folders";
	$aMSG['media']['root_folder']['de'] = "Unsortierte Medien";#Desktop
	$aMSG['media']['root_folder']['en'] = "Unsorted files";
	$aMSG['media']['upload_folder']['de'] = "Upload Medien";#Desktop
	$aMSG['media']['upload_folder']['en'] = "Uploaded files";
	$aMSG['media']['noassignment']['de'] = "Keine Zuordnung";
	$aMSG['media']['noassignment']['en'] = "No assignment";
	// import
	$aMSG['media']['source']['de'] = "Quelle";
	$aMSG['media']['source']['en'] = "Source";
	$aMSG['media']['destination']['de'] = "Ziel";
	$aMSG['media']['destination']['en'] = "Destination";
	$aMSG['media']['path_info']['de'] = "Relativ zum MDB-Verzeichnis oder Absolut zur Server-Root (UNIX!).";
	$aMSG['media']['path_info']['en'] = "Relative to MDB-directory or absolute to server-root (UNIX!).";
	$aMSG['media']['type_info']['de'] = "Nur Dateien des ausgewählten Dateityps werden importiert.";
	$aMSG['media']['type_info']['en'] = "Only files of selected filetype will be imported.";
	$aMSG['media']['delete_orig']['de'] = "Original-Datei löschen";
	$aMSG['media']['delete_orig']['en'] = "Delete orig. file";
	$aMSG['media']['short_filename']['de'] = "Dateinamen kürzen <br><small>(auf 31 Zeichen für OS9)</small>";
	$aMSG['media']['short_filename']['en'] = "Shorten filename(s) <br><small>(to 31 chars for OS9)</small>";
	$aMSG['media']['unique_filename']['de'] = "&quot;Unique&quot; Dateinamen";
	$aMSG['media']['unique_filename']['en'] = "Unique filename(s)";
	$aMSG['media']['imported']['de'] = "Media-Dateien wurden importiert";
	$aMSG['media']['imported']['en'] = "Media-files imported";
	$aMSG['media']['js_alert']['de'] = "Wirklich jetzt ausführen?";
	$aMSG['media']['js_alert']['en'] = "Are you sure?";
	// popup_selectmedia
	$aMSG['media']['clicktocopy']['de'] = "Zum kopieren anklicken";
	$aMSG['media']['clicktocopy']['en'] = "Click to copy this link";
	// popup_slide -> hat Texte in der Seite (wegen Variablen)!
	$aMSG['media']['mdbfilename']['de'] = "Dateiname";
	$aMSG['media']['mdbfilename']['en'] = "Filename";

?>