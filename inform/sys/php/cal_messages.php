<?php
/**				***** CAL *****
*
* cal_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.0 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	1.0 / 2005-03-04
*/

// Fehlermeldungen
	// ... sind alle in sys...

// CAL
	$aMSG['cal']['schedule']['de'] = "Termin";
	$aMSG['cal']['schedule']['en'] = "Appointment";
	$aMSG['cal']['view_day']['de'] = "Tag";
	$aMSG['cal']['view_day']['en'] = "Day";
	$aMSG['cal']['view_week']['de'] = "Woche";
	$aMSG['cal']['view_week']['en'] = "Week";
	$aMSG['cal']['view_month']['de'] = "Monat";
	$aMSG['cal']['view_month']['en'] = "Month";
	$aMSG['cal']['filter_all']['de'] = "Alle";
	$aMSG['cal']['filter_all']['en'] = "All";
	$aMSG['cal']['filter_my']['de'] = "Meine";
	$aMSG['cal']['filter_my']['en'] = "Mine";
	$aMSG['cal']['days']['de'] = "Tage";
	$aMSG['cal']['days']['en'] = "Days";
	$aMSG['cal']['hours']['de'] = "Stunden";
	$aMSG['cal']['hours']['en'] = "Hours";
	$aMSG['cal']['d']['de'] = "T";
	$aMSG['cal']['d']['en'] = "D";
	$aMSG['cal']['h']['de'] = "Std.";
	$aMSG['cal']['h']['en'] = "H";
	
	$aMSG['cal']['alllabel']['de'] = "Filtern nach...";
	$aMSG['cal']['alllabel']['en'] = "Filter by...";
	
// Formulare
	$aMSG['form']['subject']['de'] = "Betreff";
	$aMSG['form']['subject']['en'] = "Subject";
	$aMSG['form']['location']['de'] = "Ort";
	$aMSG['form']['location']['en'] = "Location";
	$aMSG['form']['date_start']['de'] = "Beginn";
	$aMSG['form']['date_start']['en'] = "Start";
	$aMSG['form']['date_end']['de'] = "Ende";
	$aMSG['form']['date_end']['en'] = "End";
	$aMSG['form']['arrival']['de'] = "Anreise";
	$aMSG['form']['arrival']['en'] = "Journey time";
	$aMSG['form']['return']['de'] = "Rückreise";
	$aMSG['form']['return']['en'] = "Return journey";
	$aMSG['form']['flag_allday']['de'] = "Ganztägig";
	$aMSG['form']['flag_allday']['en'] = "All day";
	$aMSG['form']['participant']['de'] = "Teilnehmer";
	$aMSG['form']['participant']['en'] = "Participant";
	$aMSG['form']['visibility']['de'] = "Sichtbar für";
	$aMSG['form']['visibility']['en'] = "Visible for";
	$aMSG['form']['flag_private_event']['de'] = "Ausgewählte Teilnehmer";
	$aMSG['form']['flag_private_event']['en'] = "Selected participants only";
	$aMSG['form']['label']['de'] = "Beschriftung";
	$aMSG['form']['label']['en'] = "Label";
	$aMSG['form']['kw']['de'] = "KW";
	$aMSG['form']['kw']['en'] = "Week";
	$aMSG['form']['flag_confirmed']['de'] = "Termin bestätigt";
	$aMSG['form']['flag_confirmed']['en'] = "Confirmed";
	$aMSG['form']['MapLink']['de'] = "Map";
	$aMSG['form']['MapLink']['en'] = "Map";
	
	
	$aMSG['cal']['sick']['de'] = "Krank";
	$aMSG['cal']['sick']['en'] = "Sick";
	$aMSG['cal']['holiday']['de'] = "Urlaub";
	$aMSG['cal']['holiday']['en'] = "Holiday";
	$aMSG['cal']['lieu']['de'] = "Springertage";
	$aMSG['cal']['lieu']['en'] = "Lieu days";
// Button-Beschriftungen
	$aMSG['btn']['notify_users']['de'] = "Teilnehmer benachrichtigen";
	$aMSG['btn']['notify_users']['en'] = "Notify participants";

?>