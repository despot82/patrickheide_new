<?php
/**				***** ARC *****
*
* arc_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt.
*
* @author Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.1 / 2006-02-06 (in UTF-8 umgewandelt)
*/

	$aMSG['archiv']['folder']['de'] = 'Kategorie';
	$aMSG['archiv']['folder']['en'] = 'Category';
	$aMSG['archiv']['folders']['de'] = 'Kategorien';
	$aMSG['archiv']['folders']['en'] = 'Categories';
	$aMSG['archiv']['posting']['de'] = 'Eintrag';
	$aMSG['archiv']['posting']['en'] = 'Entry';
	$aMSG['archiv']['archiv']['de'] = 'Archiv';
	$aMSG['archiv']['archiv']['en'] = 'Archive';
	$aMSG['archiv']['root_folder']['de'] = "Unsortierte Beiträge";
	$aMSG['archiv']['root_folder']['en'] = "Unsorted articles";
	$aMSG['archiv']['all_folders']['de'] = 'Alle Kategorien';
	$aMSG['archiv']['all_folders']['en'] = 'All categories';
	$aMSG['archiv']['recent_entries']['de'] = 'Neueste Einträge';
	$aMSG['archiv']['recent_entries']['en'] = 'Latest entries';
	$aMSG['archiv']['notavail']['de'] = 'nicht vorhanden';
	$aMSG['archiv']['notavail']['en'] = 'not available';
	$aMSG['archiv']['avail']['de'] = 'vorhanden';
	$aMSG['archiv']['avail']['en'] = 'available';
	
	$aMSG['archiv']['dbbackups']['de'] = 'Datenbank-Backup';
	$aMSG['archiv']['dbbackups']['en'] = 'Database backups';
	$aMSG['archiv']['dbbackup']['de'] = 'Datenbank-Backup';
	$aMSG['archiv']['dbbackup']['en'] = 'Database backup';
	$aMSG['archiv']['dblastbackup']['de'] = 'Letzte '.$aENV['MaxDbDump'].' backups';
	$aMSG['archiv']['dblastbackup']['en'] = 'Last '.$aENV['MaxDbDump'].' backups';

	$aMSG['form']['archivstatus']['de']	= 'Status';
	$aMSG['form']['archivstatus']['en']	= 'State';
	$aMSG['form']['nr']['de'] = 'Nr.';
	$aMSG['form']['nr']['en'] = 'Nr.';
	$aMSG['form']['intnr']['de'] = 'Nr.';
	$aMSG['form']['intnr']['en'] = 'Nr.';
	$aMSG['form']['link']['de'] = 'Link';
	$aMSG['form']['link']['en'] = 'Link';
	$aMSG['form']['avail']['de'] = 'Verfügbarkeit';
	$aMSG['form']['avail']['en'] = 'Availability';
	$aMSG['form']['content']['de'] = 'Inhalt';
	$aMSG['form']['content']['en'] = 'Content';
	$aMSG['form']['restore']['de'] = 'Möchten Sie den aktuellen Stand der Datenbank mit der Sicherung überschreiben?';
	$aMSG['form']['restore']['en'] = 'Do you want to replace the current contents with the saved version?';
	$aMSG['form']['fileeditcopy']['de'] = 'Datei zum Editorfeld upload';
	$aMSG['form']['fileeditcopy']['en'] = 'Datei zum Editorfeld upload';
	
	$aMSG['btn']['download']['de'] = "Downloaden";
	$aMSG['btn']['download']['en'] = "Download";
?>