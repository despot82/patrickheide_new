<?php
/*******************************************************************************
* Software: FPDF_EPS
* Version:  1.2
* Date:     2006-07-21
* Author:   Valentin Schmidt
*
* Last Changes:
* - fixed positioning and BoundingBox handling code (was totally screwed)
* - fix for BoundingBox detection without space after : (now Corel-compatible)
* - added some dirty code to handle compound paths
* - support for custom colors (x operator)
*******************************************************************************/

class FPDF_CUSTOM extends HTML2FPDF{

	var $outlines=array();
	var $OutlineRoot;
	var $DisplayPreferences=''; //EDITEI - added
	
	function File_PDF_CUSTOM($orientation='P', $unit='mm', $format='A4'){
	    parent::HTML2FPDF($orientation, $unit, $format);
	}
	
    function SetCharSpacing( $s_w , $s_l = 0) { 
        $this->s_w = $s_w;
        $this->s_l = $s_l;
        $this->_out(
            'BT '.
            sprintf('%.3f Tc ', $this->s_w * $this->k).
            sprintf('%.3f Tw ', $this->s_l * $this->k).
            'ET '
            ); 
	}
	
	function Footer() {
		
	}
	
	  function _iso2ascii($s){
	     $iso="�����������������������ť��ة���ݮ����";
	     $asc="acdeeillnorstuuyzaeouACDEEILLNORSTUUYZAEOU";
	    return strtr($s,$iso,$asc);
	  }

	function _makeFileName($title) {
		$title = $this->_iso2ascii(strip_tags(trim($title)));
		preg_match_all('/[a-zA-Z0-9]+/', $title, $nt);
		return implode('-',$nt[0]);
	}	
	
	function ImageEps ($file, $x, $y, $w=0, $h=0, $link='', $useBoundingBox=true){
	    $data = file_get_contents($file);
	    if ($data===false) $this->Error('EPS file not found: '.$file);
	
	    # find BoundingBox params
	    ereg ("%%BoundingBox:([^\r\n]+)", $data, $regs);
	    if (count($regs)>1){
	        list($x1, $y1, $x2, $y2) = explode(' ', trim($regs[1]));
	    }
	    else $this->Error('No BoundingBox found in EPS file: '.$file);
	
	    $start = strpos($data, '%%EndSetup');
	    if ($start===false) $start = strpos($data, '%%EndProlog');
	    if ($start===false) $start = strpos($data, '%%BoundingBox');
	
	    $data = substr($data, $start);
	
	    $end = strpos($data, '%%PageTrailer');
	    if ($end===false) $end = strpos($data, 'showpage');
	    if ($end) $data = substr($data, 0, $end);
	
	    # save the current graphic state
	    $this->_out('q');
	
	    $k = $this->k;
	
	    if ($useBoundingBox){
	        $dx = $x*$k-$x1;
	        $dy = $y*$k-$y1;
	    }else{
	        $dx = $x*$k;
	        $dy = $y*$k;
	    }
	    
	    # translate
	    $this->_out(sprintf('%.3f %.3f %.3f %.3f %.3f %.3f cm', 1, 0, 0, 1, $dx, $dy+($this->hPt - 2*$y*$k - ($y2-$y1))));
	    
	    if ($w>0){
	        $scale_x = $w/(($x2-$x1)/$k);
	        if ($h>0){
	            $scale_y = $h/(($y2-$y1)/$k);
	        }else{
	            $scale_y = $scale_x;
	            $h = ($y2-$y1)/$k * $scale_y;
	        }
	    }else{
	        if ($h>0){
	            $scale_y = $h/(($y2-$y1)/$k);
	            $scale_x = $scale_y;
	            $w = ($x2-$x1)/$k * $scale_x;
	        }else{
	            $w = ($x2-$x1)/$k;
	            $h = ($y2-$y1)/$k;
	        }
	    }
	    
	    # scale    
	    if (isset($scale_x))
	        $this->_out(sprintf('%.3f %.3f %.3f %.3f %.3f %.3f cm', $scale_x, 0, 0, $scale_y, $x1*(1-$scale_x), $y2*(1-$scale_y)));
	    
	    # handle pc/unix/mac line endings
	    $lines = split ("\r\n|[\r\n]", $data);
	
	    $u=0;
	    
	    $cnt = count($lines);
	    for ($i=0;$i<$cnt;$i++){
	        $line = $lines[$i];
	        if ($line=='' || $line{0}=='%') continue;
	        $len = strlen($line);
	        if ($len>2 && $line{$len-2}!=' ') continue;
	        $cmd = $line{$len-1};

	        switch ($cmd){
	            case 'k':
	            case 'm':
	            case 'l':
	            case 'v':
	            case 'y':
	            case 'c':
	
	            case 'K':
	            case 'g':
	            case 'G':
	
	            case 's':
	            case 'S':
	
	            case 'J':
	            case 'j':
	            case 'w':
	            case 'M':
	            case 'd' :
	            
	            case 'n' :
	            case 'v' :
	                $this->_out($line);
	                break;
	                                        
	            case 'x': # custom colors
	                list($c, $m, $y, $k) = explode(' ', $line);
	                $this->_out("$c $m $y $k k");
	                break;
	                
	            case 'Y':
	                $line{$len-1}='y';
	                $this->_out($line);
	                break;
	
	            case 'N':
	                $line{$len-1}='n';
	                $this->_out($line);
	                break;
	        
	            case 'V':
	                $line{$len-1}='v';
	                $this->_out($line);
	                break;
	                                                
	            case 'L':
	                $line{$len-1}='l';
	                $this->_out($line);
	                break;
	
	            case 'C':
	                $line{$len-1}='c';
	                $this->_out($line);
	                break;
	
	            case 'b':
	            case 'B':
	                $this->_out($cmd . '*');
	                break;
	
	            case 'f':
	            case 'F':
	                if ($u>0){
	                    $isU = false;
	                    $max = min($i+5, $cnt);
	                    for ($j=$i+1;$j<$max;$j++)
	                        $isU = ($isU || ($lines[$j]=='U' || $lines[$j]=='*U'));
	                    if ($isU) $this->_out("f*");
	                }else
	                    $this->_out("f*");
	                break;
	
	            case 'u':
	                if ($line{0}=='*') $u++;
	                break;
	
	            case 'U':
	                if ($line{0}=='*') $u--;
	                break;
	            
	            #default: echo "$cmd"; #just for debugging
	        }
	
	    }
	
	    # restore previous graphic state
	    $this->_out('Q');
	    if ($link)
	        $this->Link($x, $y, $w, $h, $link);
	}

	//Thanks to Ron Korving for the WordWrap() function
	function WordWrap(&$text, $maxwidth)
	{
	    $biggestword=0;//EDITEI
	    $toonarrow=false;//EDITEI
	
	    $text = trim($text);
	    if ($text==='') return 0;
	    $space = $this->GetStringWidth(' ');
	    $lines = explode("\n", $text);
	    $text = '';
	    $count = 0;
	
	    foreach ($lines as $line)
	    {
	        $words = preg_split('/ +/', $line);
	        $width = 0;
	
	        foreach ($words as $word)
	        {
	            $wordwidth = $this->GetStringWidth($word);
	
		          //EDITEI
		          //Warn user that maxwidth is insufficient
		          if ($wordwidth > $maxwidth)
		          {
	  		         if ($wordwidth > $biggestword) $biggestword = $wordwidth;
	    		       $toonarrow=true;//EDITEI
		          }
	            if ($width + $wordwidth <= $maxwidth)
	            {
	                $width += $wordwidth + $space;
	                $text .= $word.' ';
	            }
	            else
	            {
	                $width = $wordwidth + $space;
	                $text = rtrim($text)."\n".$word.' ';
	                $count++;
	            }
	        }
	        $text = rtrim($text)."\n";
	        $count++;
	    }
	    $text = rtrim($text);
	
	    //Return -(wordsize) if word is bigger than maxwidth 
	    if ($toonarrow) return -$biggestword;
	    else return $count;
	}

	//EDITEI
	//Thanks to Seb(captainseb@wanadoo.fr) for the _SetTextRendering() and SetTextOutline() functions
	/** 
	* Set Text Rendering Mode 
	* @param int $mode Set the rendering mode.<ul><li>0 : Fill text (default)</li><li>1 : Stroke</li><li>2 : Fill & stroke</li></ul> 
	* @see SetTextOutline() 
	*/ 
	//This function is not being currently used
	function _SetTextRendering($mode) { 
	if (!(($mode == 0) || ($mode == 1) || ($mode == 2))) 
	$this->Error("Text rendering mode should be 0, 1 or 2 (value : $mode)"); 
	$this->_out($mode.' Tr'); 
	} 
	
	/** 
	* Set Text Ouline On/Off 
	* @param mixed $width If set to false the text rending mode is set to fill, else it's the width of the outline 
	* @param int $r If g et b are given, red component; if not, indicates the gray level. Value between 0 and 255 
	* @param int $g Green component (between 0 and 255) 
	* @param int $b Blue component (between 0 and 255) 
	* @see _SetTextRendering() 
	*/ 
	function SetTextOutline($width, $r=0, $g=-1, $b=-1) //EDITEI
	{ 
	  if ($width == false) //Now resets all values
	  { 
	    $this->outline_on = false;
	    $this->SetLineWidth(0.2); 
	    $this->SetDrawColor(0); 
	    $this->_setTextRendering(0); 
	    $this->_out('0 Tr'); 
	  }
	  else
	  { 
	    $this->SetLineWidth($width); 
	    $this->SetDrawColor($r, $g , $b); 
	    $this->_out('2 Tr'); //Fixed
	  } 
	}
	
	//function Circle() thanks to Olivier PLATHEY
	//EDITEI
	function Circle($x,$y,$r,$style='')
	{
	    $this->Ellipse($x,$y,$r,$r,$style);
	}
	
	//function Ellipse() thanks to Olivier PLATHEY
	//EDITEI
	function Ellipse($x,$y,$rx,$ry,$style='D')
	{
	    if($style=='F') $op='f';
	    elseif($style=='FD' or $style=='DF') $op='B';
	    else $op='S';
	    $lx=4/3*(M_SQRT2-1)*$rx;
	    $ly=4/3*(M_SQRT2-1)*$ry;
	    $k=$this->k;
	    $h=$this->h;
	    $this->_out(sprintf('%.2f %.2f m %.2f %.2f %.2f %.2f %.2f %.2f c',
	        ($x+$rx)*$k,($h-$y)*$k,
	        ($x+$rx)*$k,($h-($y-$ly))*$k,
	        ($x+$lx)*$k,($h-($y-$ry))*$k,
	        $x*$k,($h-($y-$ry))*$k));
	    $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c',
	        ($x-$lx)*$k,($h-($y-$ry))*$k,
	        ($x-$rx)*$k,($h-($y-$ly))*$k,
	        ($x-$rx)*$k,($h-$y)*$k));
	    $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c',
	        ($x-$rx)*$k,($h-($y+$ly))*$k,
	        ($x-$lx)*$k,($h-($y+$ry))*$k,
	        $x*$k,($h-($y+$ry))*$k));
	    $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c %s',
	        ($x+$lx)*$k,($h-($y+$ry))*$k,
	        ($x+$rx)*$k,($h-($y+$ly))*$k,
	        ($x+$rx)*$k,($h-$y)*$k,
	        $op));
	}	

	//EDITEI - Done after reading a little about PDF reference guide
	function DottedRect($x=100,$y=150,$w=50,$h=50)
	{
	  $x *= $this->k ;
	  $y = ($this->h-$y)*$this->k;
	  $w *= $this->k ;
	  $h *= $this->k ;// - h?
	   
	  $herex = $x;
	  $herey = $y;
	
	  //Make fillcolor == drawcolor
	  $bak_fill = $this->FillColor;
	  $this->FillColor = $this->DrawColor;
	  $this->FillColor = str_replace('RG','rg',$this->FillColor);
	  $this->_out($this->FillColor);
	 
	  while ($herex < ($x + $w)) //draw from upper left to upper right
	  {
	  $this->DrawDot($herex,$herey);
	  $herex += (3*$this->k);
	  }
	  $herex = $x + $w;
	  while ($herey > ($y - $h)) //draw from upper right to lower right
	  {
	  $this->DrawDot($herex,$herey);
	  $herey -= (3*$this->k);
	  }
	  $herey = $y - $h;
	  while ($herex > $x) //draw from lower right to lower left
	  {
	  $this->DrawDot($herex,$herey);
	  $herex -= (3*$this->k);
	  }
	  $herex = $x;
	  while ($herey < $y) //draw from lower left to upper left
	  {
	  $this->DrawDot($herex,$herey);
	  $herey += (3*$this->k);
	  }
	  $herey = $y;
	
	  $this->FillColor = $bak_fill;
	  $this->_out($this->FillColor); //return fillcolor back to normal
	}
	
	//EDITEI - Done after reading a little about PDF reference guide
	function DrawDot($x,$y) //center x y
	{
	  $op = 'B'; // draw Filled Dots
	  //F == fill //S == stroke //B == stroke and fill 
	  $r = 0.5 * $this->k;  //raio
	  
	  //Start Point
	  $x1 = $x - $r;
	  $y1 = $y;
	  //End Point
	  $x2 = $x + $r;
	  $y2 = $y;
	  //Auxiliar Point
	  $x3 = $x;
	  $y3 = $y + (2*$r);// 2*raio to make a round (not oval) shape  
	
	  //Round join and cap
	  $s="\n".'1 J'."\n";
	  $s.='1 j'."\n";
	
	  //Upper circle
	  $s.=sprintf('%.3f %.3f m'."\n",$x1,$y1); //x y start drawing
	  $s.=sprintf('%.3f %.3f %.3f %.3f %.3f %.3f c'."\n",$x1,$y1,$x3,$y3,$x2,$y2);//Bezier curve
	  //Lower circle
	  $y3 = $y - (2*$r);
	  $s.=sprintf("\n".'%.3f %.3f m'."\n",$x1,$y1); //x y start drawing
	  $s.=sprintf('%.3f %.3f %.3f %.3f %.3f %.3f c'."\n",$x1,$y1,$x3,$y3,$x2,$y2);
	  $s.=$op."\n"; //stroke and fill
	
	  //Draw in PDF file
	  $this->_out($s);
	}	
	
	function Bookmark($txt, $level=0, $y=0)
	{
	    if($y==-1)
	        $y=$this->GetY();
	    $this->outlines[]=array('t'=>$txt, 'l'=>$level, 'y'=>$y, 'p'=>$this->PageNo());
	} // end method Bookmark
	

	function SetDash($black=false,$white=false)
	{
	        if($black and $white) $s=sprintf('[%.3f %.3f] 0 d',$black*$this->k,$white*$this->k);
	        else $s='[] 0 d';
	        $this->_out($s);
	}	
		
	
	function saveFont()
	{
	   $saved = array();
	   $saved[ 'family' ] = $this->FontFamily;
	   $saved[ 'style' ] = $this->FontStyle;
	   $saved[ 'sizePt' ] = $this->FontSizePt;
	   $saved[ 'size' ] = $this->FontSize;
	   $saved[ 'curr' ] =& $this->CurrentFont;
	   $saved[ 'color' ] = $this->TextColor; //EDITEI
	   $saved[ 'bgcolor' ] = $this->FillColor; //EDITEI
	   $saved[ 'HREF' ] = $this->HREF; //EDITEI
	   $saved[ 'underline' ] = $this->underline; //EDITEI
	   $saved[ 'strike' ] = $this->strike; //EDITEI
	   $saved[ 'SUP' ] = $this->SUP; //EDITEI
	   $saved[ 'SUB' ] = $this->SUB; //EDITEI
	   $saved[ 'linewidth' ] = $this->LineWidth; //EDITEI
	   $saved[ 'drawcolor' ] = $this->DrawColor; //EDITEI
	   $saved[ 'is_outline' ] = $this->outline_on; //EDITEI
	
	   return $saved;
	}
	
	function restoreFont( $saved )
	{
	   $this->FontFamily = $saved[ 'family' ];
	   $this->FontStyle = $saved[ 'style' ];
	   $this->FontSizePt = $saved[ 'sizePt' ];
	   $this->FontSize = $saved[ 'size' ];
	   $this->CurrentFont =& $saved[ 'curr' ];
	   $this->TextColor = $saved[ 'color' ]; //EDITEI
	   $this->FillColor = $saved[ 'bgcolor' ]; //EDITEI
	   $this->ColorFlag = ($this->FillColor != $this->TextColor); //Restore ColorFlag as well
	   $this->HREF = $saved[ 'HREF' ]; //EDITEI
	   $this->underline = $saved[ 'underline' ]; //EDITEI
	   $this->strike = $saved[ 'strike' ]; //EDITEI
	   $this->SUP = $saved[ 'SUP' ]; //EDITEI
	   $this->SUB = $saved[ 'SUB' ]; //EDITEI
	   $this->LineWidth = $saved[ 'linewidth' ]; //EDITEI
	   $this->DrawColor = $saved[ 'drawcolor' ]; //EDITEI
	   $this->outline_on = $saved[ 'is_outline' ]; //EDITEI
	
	   if( $this->page > 0)
	      $this->_out( sprintf( 'BT /F%d %.2f Tf ET', $this->CurrentFont[ 'i' ], $this->FontSizePt ) );
	}
	
	function newFlowingBlock( $w, $h, $b = 0, $a = 'J', $f = 0 , $is_table = false )
	{
	   // cell width in points
	   if ($is_table)  $this->flowingBlockAttr[ 'width' ] = ($w * $this->k);
	   else $this->flowingBlockAttr[ 'width' ] = ($w * $this->k) - (2*$this->cMargin*$this->k);
	   // line height in user units
	   $this->flowingBlockAttr[ 'is_table' ] = $is_table;
	   $this->flowingBlockAttr[ 'height' ] = $h;
	   $this->flowingBlockAttr[ 'lineCount' ] = 0;
	   $this->flowingBlockAttr[ 'border' ] = $b;
	   $this->flowingBlockAttr[ 'align' ] = $a;
	   $this->flowingBlockAttr[ 'fill' ] = $f;
	   $this->flowingBlockAttr[ 'font' ] = array();
	   $this->flowingBlockAttr[ 'content' ] = array();
	   $this->flowingBlockAttr[ 'contentWidth' ] = 0;
	}
	
	function finishFlowingBlock($outofblock=false)
	{
	   if (!$outofblock) $currentx = $this->x; //EDITEI - in order to make the Cell method work better
	   //prints out the last chunk
	   $is_table = $this->flowingBlockAttr[ 'is_table' ];
	   $maxWidth =& $this->flowingBlockAttr[ 'width' ];
	   $lineHeight =& $this->flowingBlockAttr[ 'height' ];
	   $border =& $this->flowingBlockAttr[ 'border' ];
	   $align =& $this->flowingBlockAttr[ 'align' ];
	   $fill =& $this->flowingBlockAttr[ 'fill' ];
	   $content =& $this->flowingBlockAttr[ 'content' ];
	   $font =& $this->flowingBlockAttr[ 'font' ];
	   $contentWidth =& $this->flowingBlockAttr[ 'contentWidth' ];
	   $lineCount =& $this->flowingBlockAttr[ 'lineCount' ];
	
	   // set normal spacing
	   $this->_out( sprintf( '%.3f Tw', 0 ) );
	   $this->ws = 0;
	
	   // the amount of space taken up so far in user units
	   $usedWidth = 0;
	
	   // Print out each chunk
	   //EDITEI - Print content according to alignment
	   $empty = $maxWidth - $contentWidth;
	   $empty /= $this->k;
	   $b = ''; //do not use borders
	   $arraysize = count($content);
	   $margins = (2*$this->cMargin);
	   if ($outofblock)
	   {
	      $align = 'C';
	      $empty = 0;
	      $margins = $this->cMargin;
	   }
	   switch($align)
	   {
	      case 'R':
	          foreach ( $content as $k => $chunk )
	          {
	              $this->restoreFont( $font[ $k ] );
	              $stringWidth = $this->GetStringWidth( $chunk ) + ( $this->ws * substr_count( $chunk, ' ' ) / $this->k );
	              // determine which borders should be used
	              $b = '';
	              if ( $lineCount == 1 && is_int( strpos( $border, 'T' ) ) ) $b .= 'T';
	              if ( $k == count( $content ) - 1 && is_int( strpos( $border, 'R' ) ) ) $b .= 'R';
	                      
	              if ($k == $arraysize-1 and !$outofblock) $skipln = 1;
	              else $skipln = 0;
	
	              if ($arraysize == 1) $this->Cell( $stringWidth + $margins + $empty, $lineHeight, $chunk, $b, $skipln, $align, $fill, $this->HREF , $currentx ); //mono-style line
	              elseif ($k == 0) $this->Cell( $stringWidth + ($margins/2) + $empty, $lineHeight, $chunk, $b, 0, 'R', $fill, $this->HREF );//first part
	              elseif ($k == $arraysize-1 ) $this->Cell( $stringWidth + ($margins/2), $lineHeight, $chunk, $b, $skipln, '', $fill, $this->HREF, $currentx );//last part
	              else $this->Cell( $stringWidth , $lineHeight, $chunk, $b, 0, '', $fill, $this->HREF );//middle part
	          }
	          break;
	      case 'L':
	      case 'J':
	          foreach ( $content as $k => $chunk )
	          {
	              $this->restoreFont( $font[ $k ] );
	              $stringWidth = $this->GetStringWidth( $chunk ) + ( $this->ws * substr_count( $chunk, ' ' ) / $this->k );
	              // determine which borders should be used
	              $b = '';
	              if ( $lineCount == 1 && is_int( strpos( $border, 'T' ) ) ) $b .= 'T';
	              if ( $k == 0 && is_int( strpos( $border, 'L' ) ) ) $b .= 'L';
	
	              if ($k == $arraysize-1 and !$outofblock) $skipln = 1;
	              else $skipln = 0;
	
	              if (!$is_table and !$outofblock and !$fill and $align=='L' and $k == 0) {$align='';$margins=0;} //Remove margins in this special (though often) case
	
	              if ($arraysize == 1) $this->Cell( $stringWidth + $margins + $empty, $lineHeight, $chunk, $b, $skipln, $align, $fill, $this->HREF , $currentx ); //mono-style line
	              elseif ($k == 0) $this->Cell( $stringWidth + ($margins/2), $lineHeight, $chunk, $b, $skipln, $align, $fill, $this->HREF );//first part
	              elseif ($k == $arraysize-1 ) $this->Cell( $stringWidth + ($margins/2) + $empty, $lineHeight, $chunk, $b, $skipln, '', $fill, $this->HREF, $currentx );//last part
	              else $this->Cell( $stringWidth , $lineHeight, $chunk, $b, $skipln, '', $fill, $this->HREF );//middle part
	          }
	          break;
	      case 'C':
	          foreach ( $content as $k => $chunk )
	          {
	              $this->restoreFont( $font[ $k ] );
	              $stringWidth = $this->GetStringWidth( $chunk ) + ( $this->ws * substr_count( $chunk, ' ' ) / $this->k );
	              // determine which borders should be used
	              $b = '';
	              if ( $lineCount == 1 && is_int( strpos( $border, 'T' ) ) ) $b .= 'T';
	
	              if ($k == $arraysize-1 and !$outofblock) $skipln = 1;
	              else $skipln = 0;
	
	              if ($arraysize == 1) $this->Cell( $stringWidth + $margins + $empty, $lineHeight, $chunk, $b, $skipln, $align, $fill, $this->HREF , $currentx ); //mono-style line
	              elseif ($k == 0) $this->Cell( $stringWidth + ($margins/2) + ($empty/2), $lineHeight, $chunk, $b, 0, 'R', $fill, $this->HREF );//first part
	              elseif ($k == $arraysize-1 ) $this->Cell( $stringWidth + ($margins/2) + ($empty/2), $lineHeight, $chunk, $b, $skipln, 'L', $fill, $this->HREF, $currentx );//last part
	              else $this->Cell( $stringWidth , $lineHeight, $chunk, $b, 0, '', $fill, $this->HREF );//middle part
	          }
	          break;
	     default: break;
	   }
	}
	
	function WriteFlowingBlock( $s , $outofblock = false )
	{
	    if (!$outofblock) $currentx = $this->x; //EDITEI - in order to make the Cell method work better
	    $is_table = $this->flowingBlockAttr[ 'is_table' ];
	    
	    // width of all the content so far in points
	    $contentWidth =& $this->flowingBlockAttr[ 'contentWidth' ];
	    
	    // cell width in points
	    $maxWidth =& $this->flowingBlockAttr[ 'width' ];
	    $lineCount =& $this->flowingBlockAttr[ 'lineCount' ];
	    
	    // line height in user units
	    $lineHeight =& $this->flowingBlockAttr[ 'height' ];
	    $border =& $this->flowingBlockAttr[ 'border' ];
	    $align =& $this->flowingBlockAttr[ 'align' ];
	    $fill =& $this->flowingBlockAttr[ 'fill' ];
	    $content =& $this->flowingBlockAttr[ 'content' ];
	    $font =& $this->flowingBlockAttr[ 'font' ];
	
	    $font[] = $this->saveFont();
	    $content[] = '';
	
	    $currContent =& $content[ count( $content ) - 1 ];
	
	    // where the line should be cutoff if it is to be justified
	    $cutoffWidth = $contentWidth;
	
	    // for every character in the string
	    for ( $i = 0; $i < strlen( $s ); $i++ )
	    {
	       // extract the current character
	       $c = $s{$i};
	    
	       // get the width of the character in points
	       $cw = $this->CurrentFont[ 'cw' ][ $c ] * ( $this->FontSizePt / 1000 );
	
	       if ( $c == ' ' )
	       {
	           $currContent .= ' ';
	           $cutoffWidth = $contentWidth;
	           $contentWidth += $cw;
	           continue;
	       }
	    
	       // try adding another char
	       if ( $contentWidth + $cw > $maxWidth )
	       {
	           // it won't fit, output what we already have
	           $lineCount++;
	    
	           //Readjust MaxSize in order to use the whole page width
	           if ($outofblock and ($lineCount == 1) ) $maxWidth = $this->pgwidth * $this->k;
	    
	           // contains any content that didn't make it into this print
	           $savedContent = '';
	           $savedFont = array();
	    
	           // first, cut off and save any partial words at the end of the string
	           $words = explode( ' ', $currContent );
	           
	           // if it looks like we didn't finish any words for this chunk
	           if ( count( $words ) == 1 )
	           {
	              // save and crop off the content currently on the stack
	              $savedContent = array_pop( $content );
	              $savedFont = array_pop( $font );
	
	              // trim any trailing spaces off the last bit of content
	              $currContent =& $content[ count( $content ) - 1 ];
	              $currContent = rtrim( $currContent );
	           }
	           else // otherwise, we need to find which bit to cut off
	           {
	              $lastContent = '';
	              for ( $w = 0; $w < count( $words ) - 1; $w++) $lastContent .= "{$words[ $w ]} ";
	
	              $savedContent = $words[ count( $words ) - 1 ];
	              $savedFont = $this->saveFont();
	              // replace the current content with the cropped version
	              $currContent = rtrim( $lastContent );
	           }
	    
	           // update $contentWidth and $cutoffWidth since they changed with cropping
	           $contentWidth = 0;
	           foreach ( $content as $k => $chunk )
	           {
	              $this->restoreFont( $font[ $k ] );
	              $contentWidth += $this->GetStringWidth( $chunk ) * $this->k;
	           }
	           $cutoffWidth = $contentWidth;
	    
	           // if it's justified, we need to find the char spacing
	           if( $align == 'J' )
	           {
	              // count how many spaces there are in the entire content string
	              $numSpaces = 0;
	              foreach ( $content as $chunk ) $numSpaces += substr_count( $chunk, ' ' );
	              // if there's more than one space, find word spacing in points
	              if ( $numSpaces > 0 ) $this->ws = ( $maxWidth - $cutoffWidth ) / $numSpaces;
	              else $this->ws = 0;
	              $this->_out( sprintf( '%.3f Tw', $this->ws ) );
	           }
	           // otherwise, we want normal spacing
	           else $this->_out( sprintf( '%.3f Tw', 0 ) );
	
	           //EDITEI - Print content according to alignment
	           if (!isset($numSpaces)) $numSpaces = 0;
	           $contentWidth -= ($this->ws*$numSpaces);
	           $empty = $maxWidth - $contentWidth - 2*($this->ws*$numSpaces);
	           $empty /= $this->k;
	           $b = ''; //do not use borders
	           /*'If' below used in order to fix "first-line of other page with justify on" bug*/
	           if($this->y+$this->divheight>$this->PageBreakTrigger and !$this->InFooter and $this->AcceptPageBreak())
		         {
	           		$bak_x=$this->x;//Current X position
	             	$ws=$this->ws;//Word Spacing
			          if($ws>0)
			          {
				         $this->ws=0;
				         $this->_out('0 Tw');
			          }
			          $this->AddPage($this->CurOrientation);
			          $this->x=$bak_x;
			          if($ws>0)
			          {
				         $this->ws=$ws;
				         $this->_out(sprintf('%.3f Tw',$ws));
	            	}
		         }
	           $arraysize = count($content);
	           $margins = (2*$this->cMargin);
	           if ($outofblock)
	           {
	              $align = 'C';
	              $empty = 0;
	              $margins = $this->cMargin;
	           }
	           switch($align)
	           {
	             case 'R':
	                 foreach ( $content as $k => $chunk )
	                 {
	                     $this->restoreFont( $font[ $k ] );
	                     $stringWidth = $this->GetStringWidth( $chunk ) + ( $this->ws * substr_count( $chunk, ' ' ) / $this->k );
	                     // determine which borders should be used
	                     $b = '';
	                     if ( $lineCount == 1 && is_int( strpos( $border, 'T' ) ) ) $b .= 'T';
	                     if ( $k == count( $content ) - 1 && is_int( strpos( $border, 'R' ) ) ) $b .= 'R';
	
	                     if ($arraysize == 1) $this->Cell( $stringWidth + $margins + $empty, $lineHeight, $chunk, $b, 1, $align, $fill, $this->HREF , $currentx ); //mono-style line
	                     elseif ($k == 0) $this->Cell( $stringWidth + ($margins/2) + $empty, $lineHeight, $chunk, $b, 0, 'R', $fill, $this->HREF );//first part
	                     elseif ($k == $arraysize-1 ) $this->Cell( $stringWidth + ($margins/2), $lineHeight, $chunk, $b, 1, '', $fill, $this->HREF, $currentx );//last part
	                     else $this->Cell( $stringWidth , $lineHeight, $chunk, $b, 0, '', $fill, $this->HREF );//middle part
	                 }
	                break;
	             case 'L':
	             case 'J':
	                 foreach ( $content as $k => $chunk )
	                 {
	                     $this->restoreFont( $font[ $k ] );
	                     $stringWidth = $this->GetStringWidth( $chunk ) + ( $this->ws * substr_count( $chunk, ' ' ) / $this->k );
	                     // determine which borders should be used
	                     $b = '';
	                     if ( $lineCount == 1 && is_int( strpos( $border, 'T' ) ) ) $b .= 'T';
	                     if ( $k == 0 && is_int( strpos( $border, 'L' ) ) ) $b .= 'L';
	
	                     if (!$is_table and !$outofblock and !$fill and $align=='L' and $k == 0)
	                     {
	                         //Remove margins in this special (though often) case
	                         $align='';
	                         $margins=0;
	                     }
	
	                     if ($arraysize == 1) $this->Cell( $stringWidth + $margins + $empty, $lineHeight, $chunk, $b, 1, $align, $fill, $this->HREF , $currentx ); //mono-style line
	                     elseif ($k == 0) $this->Cell( $stringWidth + ($margins/2), $lineHeight, $chunk, $b, 0, $align, $fill, $this->HREF );//first part
	                     elseif ($k == $arraysize-1 ) $this->Cell( $stringWidth + ($margins/2) + $empty, $lineHeight, $chunk, $b, 1, '', $fill, $this->HREF, $currentx );//last part
	                     else $this->Cell( $stringWidth , $lineHeight, $chunk, $b, 0, '', $fill, $this->HREF );//middle part
	
	                     if (!$is_table and !$outofblock and !$fill and $align=='' and $k == 0)
	                     {
	                         $align = 'L';
	                         $margins = (2*$this->cMargin);
	                     }
	                 }
	                 break;
	             case 'C':
	                 foreach ( $content as $k => $chunk )
	                 {
	                     $this->restoreFont( $font[ $k ] );
	                     $stringWidth = $this->GetStringWidth( $chunk ) + ( $this->ws * substr_count( $chunk, ' ' ) / $this->k );
	                     // determine which borders should be used
	                     $b = '';
	                     if ( $lineCount == 1 && is_int( strpos( $border, 'T' ) ) ) $b .= 'T';
	
	                     if ($arraysize == 1) $this->Cell( $stringWidth + $margins + $empty, $lineHeight, $chunk, $b, 1, $align, $fill, $this->HREF , $currentx ); //mono-style line
	                     elseif ($k == 0) $this->Cell( $stringWidth + ($margins/2) + ($empty/2), $lineHeight, $chunk, $b, 0, 'R', $fill, $this->HREF );//first part
	                     elseif ($k == $arraysize-1 ) $this->Cell( $stringWidth + ($margins/2) + ($empty/2), $lineHeight, $chunk, $b, 1, 'L', $fill, $this->HREF, $currentx );//last part
	                     else $this->Cell( $stringWidth , $lineHeight, $chunk, $b, 0, '', $fill, $this->HREF );//middle part
	                 }
	                 break;
	                 default: break;
	           }
	           // move on to the next line, reset variables, tack on saved content and current char
	           $this->restoreFont( $savedFont );
	           $font = array( $savedFont );
	           $content = array( $savedContent . $s{ $i } );
	
	           $currContent =& $content[ 0 ];
	           $contentWidth = $this->GetStringWidth( $currContent ) * $this->k;
	           $cutoffWidth = $contentWidth;
	       }
	       // another character will fit, so add it on
	       else
	       {
	           $contentWidth += $cw;
	           $currContent .= $s{ $i };
	       }
	    }
	}
	//----------------------END OF FLOWING BLOCK------------------------------------//

	
	// 
	function _putbookmarks()
	{
	    $nb=count($this->outlines);
	    if($nb==0)
	        return;
	    $lru=array();
	    $level=0;
	    foreach($this->outlines as $i=>$o)
	    {
	        if($o['l']>0)
	        {
	            $parent=$lru[$o['l']-1];
	            //Set parent and last pointers
	            $this->outlines[$i]['parent']=$parent;
	            $this->outlines[$parent]['last']=$i;
	            if($o['l']>$level)
	            {
	                //Level increasing: set first pointer
	                $this->outlines[$parent]['first']=$i;
	            }
	        }
	        else
	            $this->outlines[$i]['parent']=$nb;
	        if($o['l']<=$level and $i>0)
	        {
	            //Set prev and next pointers
	            $prev=$lru[$o['l']];
	            $this->outlines[$prev]['next']=$i;
	            $this->outlines[$i]['prev']=$prev;
	        }
	        $lru[$o['l']]=$i;
	        $level=$o['l'];
	    }
	    //Outline items
	    $n	=$this->n+1;
	    
	    // foreach outlines
	    foreach($this->outlines as $i=>$o)
	    {
	        $this->_newobj();
	        $this->_out('<</Title '.$this->_textstring($o['t']));
	        $this->_out('/Parent '.($n+$o['parent']).' 0 R');
	        if(isset($o['prev']))
	            $this->_out('/Prev '.($n+$o['prev']).' 0 R');
	        if(isset($o['next']))
	            $this->_out('/Next '.($n+$o['next']).' 0 R');
	        if(isset($o['first']))
	            $this->_out('/First '.($n+$o['first']).' 0 R');
	        if(isset($o['last']))
	            $this->_out('/Last '.($n+$o['last']).' 0 R');
	        $this->_out(sprintf('/Dest [%d 0 R /XYZ 0 %.2f null]', 1+2*$o['p'], ($this->h-$o['y'])*$this->k));
	        $this->_out('/Count 0>>');
	        $this->_out('endobj');
	    } // end foreach 
	    
	    //Outline root
	    $this->_newobj();
	    $this->OutlineRoot=$this->n;
	    $this->_out('<</Type /Outlines /First '.$n.' 0 R');
	    $this->_out('/Last '.($n+$lru[0]).' 0 R>>');
	    $this->_out('endobj');
	} // end method _putbookmarks
	
	function _putresources()
	{
	    parent::_putresources();
	    $this->_putbookmarks();
	} // end method _putresources
	
	function _putcatalog()
	{
	    parent::_putcatalog();
	    if(count($this->outlines)>0)
	    {
	        $this->_out('/Outlines '.$this->OutlineRoot.' 0 R');
	        $this->_out('/PageMode /UseOutlines');
	    }
	} // end method _putcatalog
	
	function _parsegif($file) 
	{ 
		//Function by J�r�me Fenal
		require_once 'gif.php'; //GIF class in pure PHP from Yamasoft (formerly at http://www.yamasoft.com)
	
		$h=0;
		$w=0;
		$gif=new CGIF();
	
		if (!$gif->loadFile($file, 0))
			$this->Error("GIF parser: unable to open file $file");
	
		if($gif->m_img->m_gih->m_bLocalClr) {
			$nColors = $gif->m_img->m_gih->m_nTableSize;
			$pal = $gif->m_img->m_gih->m_colorTable->toString();
			if($bgColor != -1) {
				$bgColor = $gif->m_img->m_gih->m_colorTable->colorIndex($bgColor);
			}
			$colspace='Indexed';
		} elseif($gif->m_gfh->m_bGlobalClr) {
			$nColors = $gif->m_gfh->m_nTableSize;
			$pal = $gif->m_gfh->m_colorTable->toString();
			if($bgColor != -1) {
				$bgColor = $gif->m_gfh->m_colorTable->colorIndex($bgColor);
			}
			$colspace='Indexed';
		} else {
			$nColors = 0;
			$bgColor = -1;
			$colspace='DeviceGray';
			$pal='';
		}
	
		$trns='';
		if($gif->m_img->m_bTrans && ($nColors > 0)) {
			$trns=array($gif->m_img->m_nTrans);
		}
	
		$data=$gif->m_img->m_data;
		$w=$gif->m_gfh->m_nWidth;
		$h=$gif->m_gfh->m_nHeight;
	
		if($colspace=='Indexed' and empty($pal))
			$this->Error('Missing palette in '.$file);
	
		if ($this->compress) {
			$data=gzcompress($data);
			return array( 'w'=>$w, 'h'=>$h, 'cs'=>$colspace, 'bpc'=>8, 'f'=>'FlateDecode', 'pal'=>$pal, 'trns'=>$trns, 'data'=>$data);
		} else {
			return array( 'w'=>$w, 'h'=>$h, 'cs'=>$colspace, 'bpc'=>8, 'pal'=>$pal, 'trns'=>$trns, 'data'=>$data);
		} 
	}	
	
	function DisplayPreferences($preferences)
	{
	    $this->DisplayPreferences .= $preferences;
	}
	
} // class end

# for backward compatibility
if (!function_exists('file_get_contents')){
    function file_get_contents($filename, $use_include_path = 0){
        $file = @fopen($filename, 'rb', $use_include_path);
        if ($file){
            if ($fsize = @filesize($filename))
                $data = fread($file, $fsize);
            else {
                $data = '';
                while (!feof($file)) $data .= fread($file, 1024);
            }
            fclose($file);
            return $data;
        }else
            return false;
    }
}
?>