<?php
/**				***** CMS *****
*
* cms_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.2 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	2.1 / 2004-06-02 (popups ergaenzt)
* #history	2.0 / 2004-05-04 (komplett NEU)
* #history	1.1 / 2003-07-11
*/

// Preview
	$aMSG['preview']['preview']['de'] = "Vorschau";
	$aMSG['preview']['preview']['en'] = "Preview";
	$aMSG['preview']['live']['de'] = "Live-Auftritt";
	$aMSG['preview']['live']['en'] = "Live view";
	$aMSG['preview']['logout_closewin']['de'] = "Vorschau beenden";
	$aMSG['preview']['logout_closewin']['en'] = "Close Preview";
	$aMSG['mode']['preview_status']['de'] = "IN BEARBEITUNG";
	$aMSG['mode']['preview_status']['en'] = "IN PROCESS";
	$aMSG['mode']['notonline_status']['de'] = "NICHT ONLINE";
	$aMSG['mode']['notonline_status']['en'] = "NOT ONLINE";

// Fehlermeldungen
	$aMSG['err']['no_formfield']['de'] = "Bitte bestimmen Sie zunächst das zu füllende Daten-Feld!";
	$aMSG['err']['no_formfield']['en'] = "Please select the required field first!";
	$aMSG['err']['clear_all_images']['de'] = "Möchten Sie alle Bilder entfernen?";
	$aMSG['err']['clear_all_images']['en'] = "Do you want to clear all images?";
	$aMSG['err']['delete_media']['de'] = "Diese Datei kann nicht gelöscht werden, da sie noch verwendet wird!";
	$aMSG['err']['delete_media']['en'] = "This file cannot be deleted, because it is still connected to a page on the website!";
	$aMSG['err']['notitle']['de'] = "Kein Titel verfügbar.";
	$aMSG['err']['notitle']['en'] = "No title available.";
	
// Formulare
	$aMSG['form']['use_on_website']['de'] = "Für Website freigeben";
	$aMSG['form']['use_on_website']['en'] = "Use on the website";
	$aMSG['form']['isonline_fromto']['de'] = "Freigegeben von - bis";
	$aMSG['form']['isonline_fromto']['en'] = "Used from - to";
	$aMSG['form']['from']['de'] = "von";
	$aMSG['form']['from']['en'] = "from";
	$aMSG['form']['to']['de'] = "bis";
	$aMSG['form']['to']['en'] = "to";
	$aMSG['form']['move_to']['de'] = "Verschiebe nach";
	$aMSG['form']['move_to']['en'] = "Move to";
	
	$aMSG['form']['intlink']['de'] = "Interner Link";
	$aMSG['form']['intlink']['en'] = "Internal link";
	$aMSG['form']['extlink']['de'] = "Externer Link";
	$aMSG['form']['extlink']['en'] = "External link";
	
	$aMSG['form']['client']['de'] = "Kunde";
	$aMSG['form']['client']['en'] = "Client";
	$aMSG['form']['published']['de'] = "Herausgeber";
	$aMSG['form']['published']['en'] = "Publisher";
	
	$aMSG['form']['yes']['de'] = "Ja";
	$aMSG['form']['yes']['en'] = "Yes";
	$aMSG['form']['no']['de'] = "Nein";
	$aMSG['form']['no']['en'] = "No";
	$aMSG['form']['secondnavi']['de'] = "Sekundärnavigation";
	$aMSG['form']['secondnavi']['en'] = "Secondary navigation";
	$aMSG['form']['fallback']['de'] = "Fallback";
	$aMSG['form']['fallback']['en'] = "Fallback";

	$aMSG['form']['copy_of']['de'] = "Kopie von ";
	$aMSG['form']['copy_of']['en'] = "Copy of ";
	
	$aMSG['form']['form1']['de'] = "Formular Seite";
	$aMSG['form']['form1']['en'] = "Form page";
	$aMSG['form']['form2']['de'] = "Bestätigung";
	$aMSG['form']['form2']['en'] = "Confirmation";
	$aMSG['form']['form3']['de'] = "Fehlermeldung";
	$aMSG['form']['form3']['en'] = "Error message";

	$aMSG['form']['gallery']['de'] = "Galerie";
	$aMSG['form']['gallery']['en'] = "Gallery";
// CMS
	$aMSG['cms']['structure']['de'] = "Struktur";
	$aMSG['cms']['structure']['en'] = "Structure";
	$aMSG['cms']['editstructure']['de'] = "Struktur editieren";
	$aMSG['cms']['editstructure']['en'] = "Edit structure";
	$aMSG['cms']['backtocontent']['de'] = "Zurück zum Inhalt";
	$aMSG['cms']['backtocontent']['en'] = "Back to the content pages";
	$aMSG['cms']['meta']['de'] = "Meta Infos";
	$aMSG['cms']['meta']['en'] = "Meta infos";
	$aMSG['cms']['search']['de'] = "Durchsuche Website";
	$aMSG['cms']['search']['en'] = "Search webcontent";
	$aMSG['cms']['preview']['de'] = "Vorschau";
	$aMSG['cms']['preview']['en'] = "Preview";
	$aMSG['cms']['editintro']['de'] = "Globale Infos editieren";
	$aMSG['cms']['editintro']['en'] = "Edit global info";

	$aMSG['navi']['dyn_file_change']['de']	= "Mindestens ein Dateiname war bereits vorhanden und wurde vom System geändert:\n ";
	$aMSG['navi']['dyn_file_change']['en']	= "At least one filename already exists and has been changed by the system :\n ";

// Detail-Seite-Modus
	$aMSG['mode']['edit']['de'] = "editieren";
	$aMSG['mode']['edit']['en'] = "edit";
	$aMSG['mode']['new']['de'] = "neu anlegen";
	$aMSG['mode']['new']['en'] = "new";

// Namen der (Standard-)Templates des CMS
	$aMSG['template']['standard']['de'] = "Standard Seite";
	$aMSG['template']['standard']['en'] = "Standard page";
	$aMSG['template']['home']['de'] = "Startseite";
	$aMSG['template']['home']['en'] = "Homepage";
	$aMSG['template']['static_page']['de'] = "Statische Seite";
	$aMSG['template']['static_page']['en'] = "Static page";
	$aMSG['template']['no_page']['de'] = "Keine Seite";
	$aMSG['template']['no_page']['en'] = "No page";

// ############# Seitenspezifisch ##################################
// Portal
	$aMSG['portal']['recent_changes']['de'] = "Letzte Online Änderungen";
	$aMSG['portal']['recent_changes']['en'] = "Recent online changes";

// Meta-Infos
	$aMSG['form']['website_state']['de'] = "Website-Status";
	$aMSG['form']['website_state']['en'] = "This website is currently";
	$aMSG['form']['language_state']['de']	= "Sprachen-Status";
	$aMSG['form']['language_state']['en']	= "Language is currently";
	
	$aMSG['form']['filter_browser']['de'] = "Diese Browser abfangen";
	$aMSG['form']['filter_browser']['en'] = "Filter this browser";
	$aMSG['form']['filter_js']['de'] = "JavaScript Vorschaltseite";
	$aMSG['form']['filter_js']['en'] = "JavaScript pre-page";
	$aMSG['form']['filter_flash']['de'] = "Flash Vorschaltseite";
	$aMSG['form']['filter_flash']['en'] = "Flash pre-page";

// Search ({TEMPLATE} und {SEARCHTERM} sind Platzhalter, die durch Vars ersetzt werden)
	$aMSG['search']['hl_search']['de'] = "Suche";
	$aMSG['search']['hl_search']['en'] = "Search";
	$aMSG['search']['hl_searchresult']['de'] = "Suchergebnis";
	$aMSG['search']['hl_searchresult']['en'] = "Search result";
	$aMSG['search']['no_searchterm']['de'] = "Bitte geben Sie einen Suchbegriff ein.";
	$aMSG['search']['no_searchterm']['en'] = "Please type in a searchterm.";
	$aMSG['search']['short_searchterm']['de'] = "Der Suchbegriff muss länger als 3 Zeichen sein.";
	$aMSG['search']['short_searchterm']['en'] = "The searchterm has to be longer then 3 characters.";
	$aMSG['search']['searchresult_detail']['de'] = " im Bereich <b>{TEMPLATE}</b>";
	$aMSG['search']['searchresult_detail']['en'] = " in <b>{TEMPLATE}</b>";
	$aMSG['search']['no_searchresult']['de'] = "Kein Suchergebnis für \"<b>{SEARCHTERM}</b>\".";
	$aMSG['search']['no_searchresult']['en'] = "No search result for \"<b>{SEARCHTERM}</b>\".";
	$aMSG['search']['standard_pages']['de'] = $aMSG['template']['standard']['de']."n"; // pural
	$aMSG['search']['standard_pages']['en'] = $aMSG['template']['standard']['en']."s"; // pural
	$aMSG['search']['do_wordpartsearch']['de'] = "Für den Wortbestandteil \"<b>{SEARCHTERM}</b>\"";
	$aMSG['search']['do_wordpartsearch']['en'] = "For the word part \"<b>{SEARCHTERM}</b>\"";
	$aMSG['search']['do_fulltextsearch']['de'] = "Für das Wort \"<b>{SEARCHTERM}</b>\"";
	$aMSG['search']['do_fulltextsearch']['en'] = "For the word \"<b>{SEARCHTERM}</b>\"";
	$aMSG['search']['do_search']['de'] = 'Alle Ergebnisse für "<b>{SEARCHTERM}</b>"';
	$aMSG['search']['do_search']['en'] = 'All entries for "<b>{SEARCHTERM}</b>"';

// Navi/Strukture
	$aMSG['navi']['alert_del']['de'] = "Dieser Hauptnavigationspunkt enthält noch Unterpunkte. \\nSie können einen Hauptnavigationspunkt erst löschen, wenn Sie alle Unterpunkte gelöscht haben!";
	$aMSG['navi']['alert_del']['en'] = "This section contains subsections. \\Only empty sections can be deleted!";
	$aMSG['navi']['alert_nolink']['de'] = "Bitte geben Sie einen Link an";
	$aMSG['navi']['alert_nolink']['en'] = "Please insert the link";
	$aMSG['navi']['subheadline']['de'] = "<b>Hinweis:</b> Seitentyp ist nach dem Sichern nicht mehr änderbar!";
	$aMSG['navi']['subheadline']['en'] = "<b>Note:</b> Page type is not editable after saving!";
	$aMSG['navi']['section']['de'] = "Hauptpunkt";
	$aMSG['navi']['section']['en'] = "Main section";
	$aMSG['navi']['new_section']['de'] = "Neuer Hauptpunkt";
	$aMSG['navi']['new_section']['en'] = "New section";
	$aMSG['navi']['use_on_navi']['de'] = "In der Navigation zeigen";
	$aMSG['navi']['use_on_navi']['en'] = "Show in the navigation";
	$aMSG['navi']['innavi_notallowed']['de'] = "Das geht nur wenn der <b>Hauptpunkt</b> einem der folgenden Seitentypen angehört:";
	$aMSG['navi']['innavi_notallowed']['en'] = "This is only allowed if <b>main section</b> is one of the following Page types:";
	$aMSG['navi']['highlight_subsection']['de'] = "Highlight";
	$aMSG['navi']['highlight_subsection']['en'] = "Highlight";
	$aMSG['navi']['page_type']['de'] = "Seitentyp";
	$aMSG['navi']['page_type']['en'] = "Page type";
	$aMSG['navi']['go_to']['de'] = "Link zu";
	$aMSG['navi']['go_to']['en'] = "Go to";
	$aMSG['navi']['or']['de'] = "oder";
	$aMSG['navi']['or']['en'] = "or";
	$aMSG['navi']['first_sub']['de'] = "Link auf erste Unterseite (sofern Verfügbar)";
	$aMSG['navi']['first_sub']['en'] = "Link to first subpage (if possible)";
	$aMSG['navi']['special_pages']['de'] = "Spezielle Seiten";
	$aMSG['navi']['special_pages']['en'] = "Special pages";
	$aMSG['navi']['needs_include']['de'] = "Benötigt das Include";
	$aMSG['navi']['needs_include']['en'] = "needs the include";
	
	$aMSG['btn']['edit_content']['de'] = "Inhalte dieser Seite editieren";
	$aMSG['btn']['edit_content']['en'] = "Edit content of this page";
	$aMSG['btn']['container_create']['de'] = "Container aktualisieren";
	$aMSG['btn']['container_create']['en'] = "Update containers";
	$aMSG['btn']['search_refresh']['de'] = "Suchtabelle aktualisieren";
	$aMSG['btn']['search_refresh']['en'] = "Refresh Searchtable";	
		
	$aMSG['navi']['sections']['de'] = "Hauptpunkte";
	$aMSG['navi']['sections']['en'] = "Main sections";
	$aMSG['navi']['subsections']['de'] = "Unterpunkte";
	$aMSG['navi']['subsections']['en'] = "Subsections";
	$aMSG['navi']['add_section']['de'] = "Hauptpunkt hinzufügen";
	$aMSG['navi']['add_section']['en'] = "Add section";
	$aMSG['navi']['add_subsection']['de'] = "Unterpunkt hinzufügen";
	$aMSG['navi']['add_subsection']['en'] = "Add subsection";
	$aMSG['navi']['show_subsections']['de'] = "Zeige Unterpunkte";
	$aMSG['navi']['show_subsections']['en'] = "Show subsections";
	$aMSG['navi']['hide_subsections']['de'] = "Verstecke Unterpunkte";
	$aMSG['navi']['hide_subsections']['en'] = "Hide subsections";

// meta 
	$aMSG['meta']['alert_create']['de']	= "Container aktualisiert";
	$aMSG['meta']['alert_create']['en']	= "container updated";
	$aMSG['meta']['sitemap_url']['de']	= "Google Sitemap URL";
	$aMSG['meta']['sitemap_url']['en']	= "Google Sitemap URL";
	$aMSG['meta']['counter']['de']	= "Trackercode";
	$aMSG['meta']['counter']['en']	= "Tracker code";
	$aMSG['meta']['firm_ip']['de']	= "IP filtern";
	$aMSG['meta']['firm_ip']['en']	= "Exclude IP";
		
	
// popup linkmanager
	$aMSG['linkmanager']['hl']['de'] = "Linkmanager";
	$aMSG['linkmanager']['hl']['en'] = "Linkmanager";
	$aMSG['linkmanager']['internal_link']['de'] = "Interner Link";
	$aMSG['linkmanager']['internal_link']['en'] = "Internal link";
	$aMSG['linkmanager']['external_link']['de'] = "Externer Link";
	$aMSG['linkmanager']['external_link']['en'] = "External link";
	$aMSG['linkmanager']['email_link']['de'] = "E-Mail Link";
	$aMSG['linkmanager']['email_link']['en'] = "E-mail link";
	$aMSG['linkmanager']['clicktocopy']['de'] = "Bitte klicken Sie auf die Seite, auf die Sie verlinken möchten. <br>Dieses Fenster schließt dann automatisch.<br>
		<small>(Bitte beachten Sie, dass nur freigegebene Seiten gelistet sind!)</small>";
	$aMSG['linkmanager']['clicktocopy']['en'] = "Please select the page you want to use. This window is closing automatically.<br>
		<small>(Please note, that only links to pages are listed, which should be accessible on the website!)</small>";
	// form
	$aMSG['linkmanager']['subject']['de'] = "Betreff";
	$aMSG['linkmanager']['subject']['en'] = "Subject";
	$aMSG['linkmanager']['webpage_address']['de'] = "Web-Adresse";
	$aMSG['linkmanager']['webpage_address']['en'] = "Webpage address";
	// buttons
	$aMSG['linkmanager']['insert_link']['de'] = "Link einfügen";
	$aMSG['linkmanager']['insert_link']['en'] = "Insert link";
	// sonstiges
	$aMSG['linkmanager']['show_subpages']['de'] = "Zeige Unterseiten";
	$aMSG['linkmanager']['show_subpages']['en'] = "Show subpages";
	$aMSG['linkmanager']['hide_subpages']['de'] = "Verstecke Unterseiten";
	$aMSG['linkmanager']['hide_subpages']['en'] = "Hide subpages";
	$aMSG['linkmanager']['alert']['de'] = "Die Parameter von welcher Seite man dieses PopUp aufgerufen hat und/oder in welches Feld man den Eintrag schreiben möchte fehlen!";
	$aMSG['linkmanager']['alert']['en'] = "Opener-variables are missing!";

// CUG
	$aMSG['form']['company']['de'] = "Firma";
	$aMSG['form']['company']['en'] = "Company";
	$aMSG['form']['country']['de'] = "Land";
	$aMSG['form']['country']['en'] = "Country";
	$aMSG['form']['homepage']['de'] = "Homepage";
	$aMSG['form']['homepage']['en'] = "Homepage";
	$aMSG['form']['login']['de'] = "Login";
	$aMSG['form']['login']['en'] = "Login";
	$aMSG['form']['navi_cug']['de'] = "Seite freischalten für";
	$aMSG['form']['navi_cug']['en'] = "Accessibility";
	$aMSG['form']['cugusergroup']['de'] = "CUG Nutzergruppen";
	$aMSG['form']['cugusergroup']['en'] = "CUG usergroups";
	$aMSG['form']['no_cugusergroup']['de'] = "Es ist noch keine CUG Nutzergruppe erstellt worden. Die Nutzer-Rechte können nicht zugewiesen werden.";
	$aMSG['form']['no_cugusergroup']['en'] = "No CUG usergroup has been created. The user rights can not be assigned.";
	$aMSG['form']['cugusergroup_0']['de'] = "Der Nutzer ist momentan keiner Gruppe zugeordnet und hat somit keine Nutzungsrechte.";
	$aMSG['form']['cugusergroup_0']['en'] = "This user has not been assigned to a CUG usergroup and has therefore no rights of use.";
	
	$aMSG['list']['cugusergroup_0']['de'] = "- Keine Nutzergruppe -";
	$aMSG['list']['cugusergroup_0']['en'] = "- No usergroup -";
	
	$aMSG['checkbox']['no_cug']['de'] = "Alle";
	$aMSG['checkbox']['no_cug']['en'] = "All";

// projectsites
	$aMSG['form']['contact_person']['de'] = "Projektleitung";
	$aMSG['form']['contact_person']['en'] = "Project management";

// texteditor
	// buttons
	$aMSG['txtedit']['bold']['de'] = "Fett";
	$aMSG['txtedit']['bold']['en'] = "bold";
	$aMSG['txtedit']['internal']['de'] = "Intern";
	$aMSG['txtedit']['internal']['en'] = "Internal";
	$aMSG['txtedit']['external']['de'] = "Extern";
	$aMSG['txtedit']['external']['en'] = "External";
	$aMSG['txtedit']['ul']['de'] = ". . .";
	$aMSG['txtedit']['ul']['en'] = ". . .";
	$aMSG['txtedit']['ol']['de'] = "1 2 3";
	$aMSG['txtedit']['ol']['en'] = "1 2 3";
	$aMSG['txtedit']['insert_link']['de'] = "Link einfügen";
	$aMSG['txtedit']['insert_link']['en'] = "Insert link";
	$aMSG['txtedit']['remove_styles']['de'] = "Formatierungen löschen";
	$aMSG['txtedit']['remove_styles']['en'] = "Remove all styles";
	$aMSG['txtedit']['accept_changes']['de'] = "Änderungen übernehmen";
	$aMSG['txtedit']['accept_changes']['en'] = "accept changes";
	// headlines
	$aMSG['txtedit']['linkmanager']['de'] = "Linkmanager";
	$aMSG['txtedit']['linkmanager']['en'] = "Linkmanager";
	$aMSG['txtedit']['internal_link']['de'] = "Interner Link";
	$aMSG['txtedit']['internal_link']['en'] = "Internal link";
	$aMSG['txtedit']['external_link']['de'] = "Externer Link";
	$aMSG['txtedit']['external_link']['en'] = "External link";
	$aMSG['txtedit']['email_link']['de'] = "E-Mail Link";
	$aMSG['txtedit']['email_link']['en'] = "E-mail link";
	$aMSG['txtedit']['links']['de'] = "Links";
	$aMSG['txtedit']['links']['en'] = "Links";
	$aMSG['txtedit']['list']['de'] = "Listen";
	$aMSG['txtedit']['list']['en'] = "List";
	$aMSG['txtedit']['format']['de'] = "Auszeichnung";
	$aMSG['txtedit']['format']['en'] = "Format";
	// tool-tips
	$aMSG['txtedit']['tt_headline']['de'] = "Texteditor-Tipps";
	$aMSG['txtedit']['tt_headline']['en'] = "Tool-Tips";
	$aMSG['txtedit']['tt_subheadline']['de'] = "Hilfe zur Benutzung";
	$aMSG['txtedit']['tt_subheadline']['en'] = "How to use this text editor";
	$aMSG['txtedit']['tt_copytext']['de'] = "Bitte markieren Sie zunächst den gewünschten Textabschnitt und klicken dann den entsprechenden Button. Bitte beachten Sie, dass verschachtelte Formatierungen u.U. zu Problemen führen können.";
	$aMSG['txtedit']['tt_copytext']['en'] = "Please make a selection in the textarea first and then click the required button. Please mind that nested formatting may cause problems.";
	$aMSG['txtedit']['tt_def_ext']['de'] = "Link zu einem anderen Webauftritt";
	$aMSG['txtedit']['tt_def_ext']['en'] = "Link to a different website";
	$aMSG['txtedit']['tt_def_int']['de'] = "Link zu einer Seite dieses Webauftritts";
	$aMSG['txtedit']['tt_def_int']['en'] = "Link to a page in the website";
	$aMSG['txtedit']['tt_def_email']['de'] = "";
	$aMSG['txtedit']['tt_def_email']['en'] = "";
	$aMSG['txtedit']['tt_def_ul']['de'] = "Erstellt eine ungeordnete Liste";
	$aMSG['txtedit']['tt_def_ul']['en'] = "Create an unordered list";
	$aMSG['txtedit']['tt_def_ol']['de'] = "Erstellt eine geordnete Liste";
	$aMSG['txtedit']['tt_def_ol']['en'] = "Create an ordered list";
	$aMSG['txtedit']['tt_def_bold']['de'] = "Fettschrift";
	$aMSG['txtedit']['tt_def_bold']['en'] = " text in bold";
	$aMSG['txtedit']['tt_error_format']['de'] = "Teile des ausgewählten Textes sind bereits bearbeitet!\\nDie Änderungen werden nicht Übernommen.";
	$aMSG['txtedit']['tt_error_format']['en'] = "Parts of the selected text have already been formatted!\\nThe new changes will not be applied.";
	$aMSG['txtedit']['tt_error_selection']['de'] = "Bitte markieren Sie einen Textabschnitt!";
	$aMSG['txtedit']['tt_error_selection']['en'] = "Please make a selection!";
	// form
	$aMSG['txtedit']['subject']['de'] = "Betreff";
	$aMSG['txtedit']['subject']['en'] = "Subject";
	$aMSG['txtedit']['webpage_address']['de'] = "Web-Adresse";
	$aMSG['txtedit']['webpage_address']['en'] = "Webpage address";
	// sonstiges
	$aMSG['txtedit']['show_subpages']['de'] = "Zeige Unterseiten";
	$aMSG['txtedit']['show_subpages']['en'] = " subpages";
	$aMSG['txtedit']['hide_subpages']['de'] = "Verstecke Unterseiten";
	$aMSG['txtedit']['hide_subpages']['en'] = "Hide subpages";

?>