<?php
/**				***** SYSTEM *****
*
* array.sys_messages.php
*
* stellt die gaengigsten standard-woerter/-saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode].
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.1 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	2.0 / 2004-03-31 (komplett NEU)
* #history	1.1 / 2003-07-11
*/

	$aMSG = array('err','form','std','topnavi','btn','mode'); // definiere gruppen
	
// Fehlermeldungen
	// allgemein (fuer Form-Klasse!)
	$aMSG['err']['delete_rec']['de'] = "Möchten Sie diesen Datensatz löschen?";
	$aMSG['err']['delete_rec']['en'] = "Do you want to delete this record?";
	$aMSG['err']['delete_recfromdesk']['de'] = "Möchten Sie diesen Datensatz von Ihren Desktop löschen?";
	$aMSG['err']['delete_recfromdesk']['en'] = "Do you want to delete this record from your desktop?";
	$aMSG['err']['delete_all']['de'] = "Möchten Sie diesen Datensatz mit alle zugehörigen Unterpunkten löschen?";
	$aMSG['err']['delete_all']['en'] = "Do you want to delete this record incl. all subsections?";
	$aMSG['err']['clear']['de'] = "Möchten Sie die Eingaben löschen?";
	$aMSG['err']['clear']['en'] = "Do you want to delete the entries?";
	
	// speziell
	$aMSG['err']['delete_all_folder']['de'] = "Sind Sie sicher, dass Sie diesen Ordner mit alle zugehörigen Unterordnern löschen möchten?";
	$aMSG['err']['delete_all_folder']['en'] = "Do you want to delete this folder incl. all subfolders?";
	$aMSG['err']['part_insert']['de'] = "Bitte machen Sie eine Eingabe im Feld ";
	$aMSG['err']['part_insert']['en'] = "Please insert ";
	$aMSG['err']['part_check']['de'] = "Bitte überprüfen Sie Ihre Eingabe im Feld ";
	$aMSG['err']['part_check']['en'] = "Please check ";
	$aMSG['err']['login_notunique']['de'] = "Bitte vergeben Sie einen anderen Login.";
	$aMSG['err']['login_notunique']['en'] = "Please insert another login.";
	$aMSG['err']['username_notunique']['de'] = "Bitte vergeben Sie einen anderen Nutzernamen.";
	$aMSG['err']['username_notunique']['en'] = "Please insert another username.";
	$aMSG['err']['pw_notold']['de'] = "Bitte kontrollieren Sie ihr altes Passwort.";
	$aMSG['err']['pw_notold']['en'] = "Please check your old password.";
	$aMSG['err']['pw_notunique']['de'] = "Bitte vergeben Sie ein anderes Passwort.";
	$aMSG['err']['pw_notunique']['en'] = "Please insert another password.";
	$aMSG['err']['email_notunique']['de'] = "Diese E-Mail Adresse wird schon verwendet. Bitte wählen Sie eine andere.";
	$aMSG['err']['email_notunique']['en'] = "The e-mail address has already been used. Please chosen a different one.";
	$aMSG['err']['confirm_password']['de'] = "Bitte bestätigen Sie das Passwort.";
	$aMSG['err']['confirm_password']['en'] = "Please confirm your password.";
	$aMSG['err']['confirm_mismatched']['de'] = "Sie haben bei der Passwort Bestätigung ein anderes Passwort eingegeben. Bitte bestätigen Sie erneut Ihr Passwort!";
	$aMSG['err']['confirm_mismatched']['en'] = "Password confirmation mismatched! Please confirm again your password!";
	$aMSG['err']['popup_miss_vars']['de'] = "Die Parameter von welcher Seite man dieses PopUp aufgerufen hat und/oder in welches Feld man den Eintrag schreiben möchte fehlen!";
	$aMSG['err']['popup_miss_vars']['en'] = "Opener-variables are missing!";
	$aMSG['err']['delete_usergroup']['de'] = "<b>Hinweis:</b> Dieser Nutzergruppe gehören noch Nutzer an. <br>Trotzdem löschen?";
	$aMSG['err']['delete_usergroup']['en'] = "<b>Note:</b> Some users assigned to this usergroup. Delete anyway? ";
	$aMSG['err']['title_notunique']['de'] = "Bitte vergeben Sie einen anderen Titel. Dieser wird schon benutzt.";
	$aMSG['err']['title_notunique']['en'] = "Please choose another title. This one is already in use.";
	$aMSG['err']['is_data_saved']['de'] = "Haben Sie diesen Datensatz schon gespeichert?";
	$aMSG['err']['is_data_saved']['en'] = "Have you saved this record?";
	$aMSG['err']['js_is_disabled']['de'] = "Achtung: JavaScript ist deaktiviert!";
	$aMSG['err']['js_is_disabled']['en'] = "Attentions: JavaScript is disabled!";
	$aMSG['err']['false_logins']['de']	= "Es wurde mindestens 3 mal das falsche Passwort fuer den User \"{USER}\" eingegeben. Dieser Nutzer ist jetzt 15 Minuten gesperrt, kann aber im System bei Bedarf wieder freigeschaltet werden.";
	$aMSG['err']['false_logins']['en']	= "3 wrong passwords have been entered for the user \"{USER}\". This user is now locked for 15 minutes and can be unlocked in the system if necessary.";
		
	// CAL
	$aMSG['err']['invalid_day']['de'] = "Kein gültiges Datum: Ein Monat kann maximal 31 Tage haben.";
	$aMSG['err']['invalid_day']['en'] = "Invalid date: A month has a maximum of 31 days.";
	$aMSG['err']['invalid_month']['de'] = "Kein gültiges Datum: Ein Jahr kann maximal 12 Monate haben.";
	$aMSG['err']['invalid_month']['en'] = "Invalid date: A year has a maximum of 12 months.";
	$aMSG['err']['invalid_enddate']['de'] = "Kein gültiges Datum: Das von Ihnen eingegebene Ende liegt vor seinem Beginn.";
	$aMSG['err']['invalid_enddate']['en'] = "Invalid date: The finishing date is before the starting date.";
	$aMSG['err']['invalid_endtime']['de'] = "Keine gültige Uhrzeit: Das von Ihnen eingegebene Ende liegt vor seinem Beginn.";
	$aMSG['err']['invalid_endtime']['en'] = "Invalid time: The finishing time is before the starting time.";
	$aMSG['err']['invalid_date_format']['de'] = "Kein gültiges Datum: Bitte verwenden Sie das Format TT.MM.JJJJ";
	$aMSG['err']['invalid_date_format']['en'] = "Invalid time: Please use the format DD.MM.YYYY";

	$aMSG['err']['pdftemplate_not_exists']['de']	= "Das PDF Template existiert nicht.";
	$aMSG['err']['pdftemplate_not_exists']['en']	= "The PDF Template doesn\'t exists'.";
		
// Formulare
	$aMSG['form']['pdftemplate']['invoice']['de'] = "ADM Pdf Template Rechnung";
	$aMSG['form']['pdftemplate']['invoice']['en'] = "ADM Pdf Template Invoice";
	$aMSG['form']['pdftemplate']['proposal']['de'] = "ADM Pdf Template Angebot";
	$aMSG['form']['pdftemplate']['proposal']['en'] = "ADM Pdf Template Proposal";	
	$aMSG['form']['pdftemplate']['dunning']['de'] = "ADM Pdf Template Mahnung";
	$aMSG['form']['pdftemplate']['dunning']['en'] = "ADM Pdf Template Dunning";
	$aMSG['form']['pdftemplate']['cancellation']['de'] = "ADM Pdf Template Storno";
	$aMSG['form']['pdftemplate']['cancellation']['en'] = "ADM Pdf Template Cancellation";	
	$aMSG['form']['username']['de'] = "Nutzername";
	$aMSG['form']['username']['en'] = "Username";
	$aMSG['form']['password']['de'] = "Passwort";
	$aMSG['form']['password']['en'] = "Password";
	$aMSG['form']['usergroup']['de'] = "Nutzergruppe";
	$aMSG['form']['usergroup']['en'] = "Usergroup";
	$aMSG['form']['userrightgroup']['de'] = "Nutzergruppe/Rechte";
	$aMSG['form']['userrightgroup']['en'] = "Usergroup/Rights";
	$aMSG['form']['oldpassword']['de'] = "Altes Passwort";
	$aMSG['form']['oldpassword']['en'] = "Old password";
	$aMSG['form']['newpassword']['de'] = "Neues Passwort";
	$aMSG['form']['newpassword']['en'] = "New password";
	$aMSG['form']['confirm_password']['de'] = "Passwort bestätigen";
	$aMSG['form']['confirm_password']['en'] = "Confirm password";
	$aMSG['form']['email']['de'] = "E-Mail Adresse";
	$aMSG['form']['email']['en'] = "E-Mail address";
	$aMSG['form']['gender']['de'] = "";
	$aMSG['form']['gender']['en'] = "";
	$aMSG['form']['gender_m']['de'] = "Mann";
	$aMSG['form']['gender_m']['en'] = "Male";
	$aMSG['form']['gender_f']['de'] = "Frau";
	$aMSG['form']['gender_f']['en'] = "Female";
	$aMSG['form']['name']['de'] = "Name";
	$aMSG['form']['name']['en'] = "Name";
	$aMSG['form']['firstname']['de'] = "Vorname";
	$aMSG['form']['firstname']['en'] = "Firstname";
	$aMSG['form']['surname']['de'] = "Nachname";
	$aMSG['form']['surname']['en'] = "Surname";
	$aMSG['form']['phone']['de'] = "Telefon";
	$aMSG['form']['phone']['en'] = "Phone";
	$aMSG['form']['fax']['de'] = "Fax";
	$aMSG['form']['fax']['en'] = "Fax";
	$aMSG['form']['street']['de'] = "Straße";
	$aMSG['form']['street']['en'] = "Address";
	$aMSG['form']['zip']['de'] = "PLZ";
	$aMSG['form']['zip']['en'] = "Zip";
	$aMSG['form']['city']['de'] = "Ort";
	$aMSG['form']['city']['en'] = "City";
	$aMSG['form']['privileges']['de'] = "Rechte";
	$aMSG['form']['privileges']['en'] = "Privileges";
	$aMSG['form']['team']['de'] = "Team";
	$aMSG['form']['team']['en'] = "Team";
	$aMSG['form']['office']['de'] = "Unit";
	$aMSG['form']['office']['en'] = "Unit";
	$aMSG['form']['sender_line']['de'] = "Absender Zeile";
	$aMSG['form']['sender_line']['en'] = "Sender line";
	$aMSG['form']['sender']['de'] = "Addresse";
	$aMSG['form']['sender']['en'] = "Address";	
	$aMSG['form']['client_nr_values']['de'] = "Kundennummer Bereich";
	$aMSG['form']['client_nr_values']['en'] = "Client number range";
	
	$aMSG['form']['state']['de'] = "Status";
	$aMSG['form']['state']['en'] = "State";
	$aMSG['form']['prio']['de'] = "Priorität";
	$aMSG['form']['prio']['en'] = "Priority";
	
	$aMSG['form']['important']['de'] = "Wichtig";
	$aMSG['form']['important']['en'] = "Important";
	$aMSG['form']['normal']['de'] = "Normal";
	$aMSG['form']['normal']['en'] = "Normal";
	$aMSG['form']['useless']['de'] = "Unwichtig";
	$aMSG['form']['useless']['en'] = "Lower Priority";
	
	$aMSG['form']['relevance']['de'] = "Rel.";
	$aMSG['form']['relevance']['en'] = "Rel.";
	$aMSG['form']['language']['de'] = "Sprache";
	$aMSG['form']['language']['en'] = "Language";
	$aMSG['form']['title']['de'] = "Titel";
	$aMSG['form']['title']['en'] = "Title";
	$aMSG['form']['subtitle']['de'] = "Untertitel";
	$aMSG['form']['subtitle']['en'] = "Subtitle";
	$aMSG['form']['shorttext']['de'] = "Kurztext";
	$aMSG['form']['shorttext']['en'] = "Short text";
	$aMSG['form']['teaser']['de'] = "Teaser";
	$aMSG['form']['teaser']['en'] = "Teaser";
	$aMSG['form']['text']['de'] = "Text";
	$aMSG['form']['text']['en'] = "Text";
	$aMSG['form']['date']['de'] = "Datum";
	$aMSG['form']['date']['en'] = "Date";
	$aMSG['form']['foruser']['de'] = "Notiz für...";
	$aMSG['form']['foruser']['en'] = "Memo for...";
	$aMSG['form']['author']['de'] = "Autor";
	$aMSG['form']['author']['en'] = "Author";
	$aMSG['form']['comment']['de'] = "Kommentar";
	$aMSG['form']['comment']['en'] = "Comment";
	
	$aMSG['form']['thumb']['de'] = "Vorschaubild";
	$aMSG['form']['thumb']['en'] = "Small image";
	$aMSG['form']['picture']['de'] = "Bild";
	$aMSG['form']['picture']['en'] = "Picture";
	$aMSG['form']['image']['de'] = "Bild";
	$aMSG['form']['image']['en'] = "Image";
	$aMSG['form']['images']['de'] = "Bilder";
	$aMSG['form']['images']['en'] = "Images";
	$aMSG['form']['drawing']['de'] = "Zeichnung";
	$aMSG['form']['drawing']['en'] = "Drawing";
	$aMSG['form']['photo']['de'] = "Foto";
	$aMSG['form']['photo']['en'] = "Photo";
	$aMSG['form']['slideshow']['de'] = "Slideshow";
	$aMSG['form']['slideshow']['en'] = "Slideshow";
	$aMSG['form']['flash']['de'] = "Flash";
	$aMSG['form']['flash']['en'] = "Flash";
	$aMSG['form']['pdf']['de'] = "PDF";
	$aMSG['form']['pdf']['en'] = "PDF";
	$aMSG['form']['file']['de'] = "Datei";
	$aMSG['form']['file']['en'] = "File";
	$aMSG['form']['filename']['de'] = "Dateiname";
	$aMSG['form']['filename']['en'] = "Filename";
	$aMSG['form']['downloadfile']['de'] = "Attachment";
	$aMSG['form']['downloadfile']['en'] = "Attachment";
	$aMSG['form']['folder']['de'] = "Ordner";
	$aMSG['form']['folder']['en'] = "Folder";
	$aMSG['form']['directory']['de'] = "Verzeichnisname";
	$aMSG['form']['directory']['en'] = "Directory";
	$aMSG['form']['filetype']['de'] = "Dateityp";
	$aMSG['form']['filetype']['en'] = "Filetype";
	$aMSG['form']['filesize']['de'] = "Dateigröße";
	$aMSG['form']['filesize']['en'] = "Filesize";
	$aMSG['form']['dimensions']['de'] = "Größe";
	$aMSG['form']['dimensions']['en'] = "Size";
	$aMSG['form']['comma']['de'] = "Kommagetrennt";
	$aMSG['form']['comma']['en'] = "Comma separated";
	
	$aMSG['form']['assignment']['de'] = "Zuordnung";
	$aMSG['form']['assignment']['en'] = "Assignment";
	$aMSG['form']['details']['de'] = "Details";
	$aMSG['form']['details']['en'] = "Details";
	$aMSG['form']['description']['de'] = "Beschreibung";
	$aMSG['form']['description']['en'] = "Description";
	$aMSG['form']['alttag']['de'] = "Beschreibung/Alt-Tag";
	$aMSG['form']['alttag']['en'] = "Description/Alt-Tag";
	$aMSG['form']['keywords']['de'] = "Keywords";
	$aMSG['form']['keywords']['en'] = "Keywords";
	
	$aMSG['form']['move_to']['de'] = "Verschiebe nach";
	$aMSG['form']['move_to']['en'] = "Move to";
	
	$aMSG['form']['calculate']['de'] = "berechnen";
	$aMSG['form']['calculate']['en'] = "calculate";
	
	$aMSG['form']['relations']['de'] = "Achtung: Es bestehen Verknüpfungen, die auf nicht mehr aktive Nutzer und Teams verweisen. Diese werden beim sichern entfernt.";
	$aMSG['form']['relations']['en'] = "Warning: Some links refer to inactive users and teams. These will be deleted when saved.";
	$aMSG['form']['userrelation']['de'] = "Achtung: Es bestehen Verknüpfungen, die auf nicht mehr aktive Nutzer verweisen. Diese werden beim sichern entfernt.";
	$aMSG['form']['userrelation']['en'] = "Warning: Some links refer to inactive users. These will be deleted when saved.";
	$aMSG['form']['grouprelation']['de'] = "Achtung: Es bestehen Verknüpfungen, die auf nicht mehr existierende Teams verweisen. Diese werden beim sichern entfernt.";
	$aMSG['form']['grouprelation']['en'] = "Warning: Some links refer to deleted teams. These will be deleted when saved.";
	
// Standard-Saetze
	$aMSG['std']['system_name']['de'] = "intranet";
	$aMSG['std']['system_name']['en'] = "intranet";
	$aMSG['std']['currenttime']['de'] = "System Uhrzeit:";
	$aMSG['std']['currenttime']['en'] = "System time:";
	$aMSG['std']['last_login']['de'] = "Letzter Login";
	$aMSG['std']['last_login']['en'] = "Last Login";
	$aMSG['std']['last_mod']['de'] = "Letzte Änderung";
	$aMSG['std']['last_mod']['en'] = "Last modified";
	$aMSG['std']['first_created']['de'] = "Erstellung";
	$aMSG['std']['first_created']['en'] = "Created";
	$aMSG['std']['last_update']['de'] = "Letzte Änderung";
	$aMSG['std']['last_update']['en'] = "Last update";
	$aMSG['std']['required']['de'] = "Pflichtfelder";
	$aMSG['std']['required']['en'] = "required field";
	$aMSG['std']['characters']['de'] = "Zeichen";
	$aMSG['std']['characters']['en'] = "characters";
	$aMSG['std']['no_entries']['de'] = "Keine Einträge.";
	$aMSG['std']['no_entries']['en'] = "No entries.";
	$aMSG['std']['no_entries_lang']['de'] = "-- Noch keine Eingabe in dieser Sprachversion --";
	$aMSG['std']['no_entries_lang']['en'] = "-- No entry in this language version --";
	$aMSG['std']['no_title_lang']['de'] = "-- Noch kein Titel in dieser Sprache --";
	$aMSG['std']['no_title_lang']['en'] = "-- No title in this language --";
	$aMSG['std']['orderby']['de'] = "geordnet nach ";
	$aMSG['std']['orderby']['en'] = "ordered by ";
	$aMSG['std']['online']['de'] = "Online";
	$aMSG['std']['online']['en'] = "Online";
	$aMSG['std']['offline']['de'] = "In Bearbeitung";
	$aMSG['std']['offline']['en'] = "Offline";
	$aMSG['std']['empty']['de'] = "Kein Inhalt";
	$aMSG['std']['empty']['en'] = "Empty";
	$aMSG['std']['no']['de'] = "Nein";
	$aMSG['std']['no']['en'] = "No";
	$aMSG['std']['yes']['de'] = "Ja";
	$aMSG['std']['yes']['en'] = "Yes";
	$aMSG['std']['login']['de'] = "Login";
	$aMSG['std']['login']['en'] = "Login";
	$aMSG['std']['logout']['de'] = "Abmelden";
	$aMSG['std']['logout']['en'] = "Logout";
	$aMSG['std']['edit']['de'] = "Editieren";
	$aMSG['std']['edit']['en'] = "Edit";
	$aMSG['std']['new']['de'] = "Neu";
	$aMSG['std']['new']['en'] = "New";
	$aMSG['std']['copy_reminder']['de'] = "Bitte beachten Sie, dass die Kopie alle Eigenschaften der Vorlage erbt!";
	$aMSG['std']['copy_reminder']['en'] = "Please note, that the copy will inherit all attributes of the original!";
	$aMSG['std']['all']['de'] = "Alle";
	$aMSG['std']['all']['en'] = "All";
	$aMSG['std']['multiple_true']['de'] = "Mehrfachauswahl möglich";
	$aMSG['std']['multiple_true']['en'] = "Multiple choice";
	
	$aMSG['std']['overview']['de'] = "Übersicht";
	$aMSG['std']['overview']['en'] = "Overview";
	$aMSG['std']['current']['de'] = "Aktuell";
	$aMSG['std']['current']['en'] = "Current";
	$aMSG['std']['list']['de'] = "Liste";
	$aMSG['std']['list']['en'] = "List";
	$aMSG['std']['access']['de'] = "Zugriff";
	$aMSG['std']['access']['en'] = "Access";
	$aMSG['std']['locked']['de'] = "Gesperrt";
	$aMSG['std']['locked']['en'] = "Locked";
		
	$aMSG['std']['integer']['de'] = "Bitte nur Ganzzahlen eingeben.";
	$aMSG['std']['integer']['en'] = "Please enter a integer.";
	
	$aMSG['std']['backup']['de'] = "Datenbank-Sicherung wird durchgeführt.";
	$aMSG['std']['backup']['en'] = "Database backup in process.";
	
// Top-Navi
	$aMSG['topnavi']['home']['de'] = "Home";
	$aMSG['topnavi']['home']['en'] = "Home";
	$aMSG['topnavi']['tools']['de'] = "Tools";
	$aMSG['topnavi']['tools']['en'] = "Tools";
	$aMSG['topnavi']['portal']['de'] = "Portal";
	$aMSG['topnavi']['portal']['en'] = "Portal";
	$aMSG['topnavi']['adr']['de'] = "Kontakte";
	$aMSG['topnavi']['adr']['en'] = "Contacts";
	$aMSG['topnavi']['addresses']['de'] = "Adressen";
	$aMSG['topnavi']['addresses']['en'] = "Addresses";
	$aMSG['topnavi']['mailings']['de'] = "Postsendungen";
	$aMSG['topnavi']['mailings']['en'] = "Mailings";
	$aMSG['topnavi']['searchadr']['de'] = "Kontakt...";
	$aMSG['topnavi']['searchadr']['en'] = "Contact...";
	$aMSG['topnavi']['cal']['de'] = "Kalender";
	$aMSG['topnavi']['cal']['en'] = "Diary";
	$aMSG['topnavi']['termine']['de'] = "Termine";
	$aMSG['topnavi']['termine']['en'] = "Schedule";
	$aMSG['topnavi']['yearview']['de'] = "Jahresübersicht";
	$aMSG['topnavi']['yearview']['en'] = "Summary";
	$aMSG['topnavi']['community']['de'] = "Community";
	$aMSG['topnavi']['community']['en'] = "Community";
	$aMSG['topnavi']['forum']['de'] = "Forum";
	$aMSG['topnavi']['forum']['en'] = "Forum";
	$aMSG['topnavi']['users']['de'] = "Nutzer";
	$aMSG['topnavi']['users']['en'] = "Users";
	$aMSG['topnavi']['archiv']['de'] = "Archiv";
	$aMSG['topnavi']['archiv']['en'] = "Archive";
	
	$aMSG['topnavi']['adm']['de'] = "Office";
	$aMSG['topnavi']['adm']['en'] = "Office";
	$aMSG['topnavi']['projects']['de'] = "Projekte";
	$aMSG['topnavi']['projects']['en'] = "Projects";
	$aMSG['topnavi']['overviewbill']['de'] = "Billing";
	$aMSG['topnavi']['overviewbill']['en'] = "Billing";
	$aMSG['topnavi']['timesheet']['de'] = "Time sheet";
	$aMSG['topnavi']['timesheet']['en'] = "Time sheet";
	$aMSG['topnavi']['timesheets']['de'] = "Time sheets";
	$aMSG['topnavi']['timesheets']['en'] = "Time sheets";
	$aMSG['topnavi']['expenses']['de'] = "Fremdkosten";
	$aMSG['topnavi']['expenses']['en'] = "Costs";
	$aMSG['topnavi']['employees']['de'] = "Stundensätze";
	$aMSG['topnavi']['employees']['en'] = "Employees";
	$aMSG['topnavi']['invoices']['de'] = "Rechnungen";
	$aMSG['topnavi']['invoices']['en'] = "Invoices";
	
	$aMSG['topnavi']['mdb']['de'] = "Media";
	$aMSG['topnavi']['mdb']['en'] = "Media";
	$aMSG['topnavi']['import']['de'] = "Datenimport";
	$aMSG['topnavi']['import']['en'] = "Import data";
	
	$aMSG['topnavi']['pms']['de'] = "Präsentationen";
	$aMSG['topnavi']['pms']['en'] = "Presentations";
	$aMSG['topnavi']['archives']['de'] = "Archive";
	$aMSG['topnavi']['archives']['en'] = "Archives";
	
	$aMSG['topnavi']['cms']['de'] = "Website CMS";
	$aMSG['topnavi']['cms']['en'] = "Website CMS";
	$aMSG['topnavi']['website']['de'] = "Website";
	$aMSG['topnavi']['website']['en'] = "Website";
	$aMSG['topnavi']['newsletter']['de'] = "Newsletter";
	$aMSG['topnavi']['newsletter']['en'] = "Newsletter";
	$aMSG['topnavi']['newsletter_subscribers']['de'] = "Newsletter-Abonnenten";
	$aMSG['topnavi']['newsletter_subscribers']['en'] = "Newsletter subscribers";
	$aMSG['topnavi']['clientsites']['de'] = "Kunden Sites";
	$aMSG['topnavi']['clientsites']['en'] = "Client sites";
	$aMSG['topnavi']['cug']['de'] = "Web Nutzergruppen";
	$aMSG['topnavi']['cug']['en'] = "Web usergroups";
	
	# topnavi => subnavi ???
	$aMSG['topnavi']['memos']['de'] = "Notizzettel";
	$aMSG['topnavi']['memos']['en'] = "Memos";
	$aMSG['topnavi']['newmemo']['de'] = "Neuer Notizzettel";
	$aMSG['topnavi']['newmemo']['en'] = "New memo";
	$aMSG['topnavi']['newuser']['de'] = "Neuer Nutzer";
	$aMSG['topnavi']['newuser']['en'] = "New user";
	$aMSG['topnavi']['userrights']['de'] = "Rechteverwaltung";
	$aMSG['topnavi']['userrights']['en'] = "User rights";
	$aMSG['topnavi']['usergroups']['de'] = "Nutzergruppen";
	$aMSG['topnavi']['usergroups']['en'] = "Usergroups";
	$aMSG['topnavi']['newusergroup']['de'] = "Neue Nutzergruppe";
	$aMSG['topnavi']['newusergroup']['en'] = "New usergroup";
	$aMSG['topnavi']['deactivated_users']['de'] = "Inaktive Nutzer";
	$aMSG['topnavi']['deactivated_users']['en'] = "Inactive users";
	$aMSG['topnavi']['active_users']['de'] = "Aktive Nutzer";
	$aMSG['topnavi']['active_users']['en'] = "Active users";
	$aMSG['topnavi']['file']['de'] = "Neue Datei";
	$aMSG['topnavi']['file']['en'] = "New file";
	$aMSG['topnavi']['help']['de'] = "Kontaktformular";
	$aMSG['topnavi']['help']['en'] = "Contact form";
	$aMSG['topnavi']['logout']['de'] = "Abmelden";
	$aMSG['topnavi']['logout']['en'] = "Logout";
	$aMSG['topnavi']['contactsearch']['de'] = "Kontakt suchen";
	$aMSG['topnavi']['contactsearch']['en'] = "Search contacts";
	$aMSG['topnavi']['teams']['de'] = "Teams";
	$aMSG['topnavi']['teams']['en'] = "Teams";
	$aMSG['topnavi']['offices']['de'] = "Units";
	$aMSG['topnavi']['offices']['en'] = "Units";

// Button-Beschriftungen
	$aMSG['btn']['back']['de'] = "Zurück";
	$aMSG['btn']['back']['en'] = "Back";
	$aMSG['btn']['continue']['de'] = "Weiter";
	$aMSG['btn']['continue']['en'] = "Continue";
	$aMSG['btn']['new']['de'] = "Neu";
	$aMSG['btn']['new']['en'] = "New";
	$aMSG['btn']['list']['de'] = "Liste";
	$aMSG['btn']['list']['en'] = "List";
	$aMSG['btn']['edit']['de'] = "Editieren";
	$aMSG['btn']['edit']['en'] = "Edit";
	$aMSG['btn']['send']['de'] = "Senden";
	$aMSG['btn']['send']['en'] = "Send";
	$aMSG['btn']['search']['de'] = " &raquo; ";
	$aMSG['btn']['search']['en'] = " &raquo; ";
	$aMSG['btn']['save']['de'] = "Sichern";
	$aMSG['btn']['save']['en'] = "Save";
	$aMSG['btn']['save_close']['de'] = "Sichern und schließen";
	$aMSG['btn']['save_close']['en'] = "Save and close";
	$aMSG['btn']['delete']['de'] = "Löschen";
	$aMSG['btn']['delete']['en'] = "Delete";
	$aMSG['btn']['deletefromdesk']['de'] = "Vom Desktop löschen";
	$aMSG['btn']['deletefromdesk']['en'] = "Delete from desktop";
	$aMSG['btn']['deleteselected']['de'] = "Ausgewählte Dateien löschen";
	$aMSG['btn']['deleteselected']['en'] = "Delete selected files";
	$aMSG['btn']['deactivate']['de'] = "Deaktivieren";
	$aMSG['btn']['deactivate']['en'] = "Deactivate";
	$aMSG['btn']['delete_all']['de'] = "Löschen";
	$aMSG['btn']['delete_all']['en'] = "Delete";
	$aMSG['btn']['continue_delete']['de'] = "Löschen bestätigen";
	$aMSG['btn']['continue_delete']['en'] = "Confirm delete";
	$aMSG['btn']['clear']['de'] = "Leeren";
	$aMSG['btn']['clear']['en'] = "Clear";
	$aMSG['btn']['clear_all']['de'] = "Alle Leeren";
	$aMSG['btn']['clear_all']['en'] = "Clear all";
	$aMSG['btn']['remove']['de'] = "Entfernen";
	$aMSG['btn']['remove']['en'] = "Remove";
	$aMSG['btn']['replace']['de'] = "Ersetzen";
	$aMSG['btn']['replace']['en'] = "Replace";
	$aMSG['btn']['cancel']['de'] = "Abbrechen";
	$aMSG['btn']['cancel']['en'] = "Cancel";
	$aMSG['btn']['reset']['de'] = "Zurücksetzen";
	$aMSG['btn']['reset']['en'] = "Reset";
	$aMSG['btn']['select']['de'] = "Auswählen";
	$aMSG['btn']['select']['en'] = "Select";
	$aMSG['btn']['import']['de'] = "Importieren";
	$aMSG['btn']['import']['en'] = "Import";
	$aMSG['btn']['restore']['de'] = "Wiederherstellen";
	$aMSG['btn']['restore']['en'] = "Restore";
	$aMSG['btn']['export']['de'] = "Exportieren";
	$aMSG['btn']['export']['en'] = "Export";
	$aMSG['btn']['close']['de'] = "Schließen";
	$aMSG['btn']['close']['en'] = "Close";
	$aMSG['btn']['close_win']['de'] = "Fenster schließen";
	$aMSG['btn']['close_win']['en'] = "Close window";
	$aMSG['btn']['save_close_win']['de'] = "Sichern und schließen";
	$aMSG['btn']['save_close_win']['en'] = "Save and close";
	$aMSG['btn']['edit_tree']['de'] = "Ordner";
	$aMSG['btn']['edit_tree']['en'] = "Folders";
	$aMSG['btn']['add_folder']['de'] = "Ordner hinzufügen";
	$aMSG['btn']['add_folder']['en'] = "New folder";
	$aMSG['btn']['copy']['de'] = "Duplizieren";
	$aMSG['btn']['copy']['en'] = "Copy";
	$aMSG['btn']['save_copy']['de'] = "Kopie sichern";
	$aMSG['btn']['save_copy']['en'] = "Save copy";
	$aMSG['btn']['printview']['de'] = "Druckansicht";
	$aMSG['btn']['printview']['en'] = "Printview";
	$aMSG['btn']['restore']['de'] = "Wiederherstellen";
	$aMSG['btn']['restore']['en'] = "Restore";
	$aMSG['btn']['today']['de'] = "Heute";
	$aMSG['btn']['today']['en'] = "Today";
	$aMSG['btn']['accept_changes']['de'] = "Änderungen übernehmen";
	$aMSG['btn']['accept_changes']['en'] = "Accept changes";
	$aMSG['btn']['discard_changes']['de'] = "Änderungen verwerfen";
	$aMSG['btn']['discard_changes']['en'] = "Discard changes";
	$aMSG['btn']['preview']['de'] = "Diese Seite im Vorschau-Fenster öffnen";
	$aMSG['btn']['preview']['en'] = "Open this page in preview window";
	$aMSG['btn']['exceldwn']['de'] = "Als Excelsheet speichern";
	$aMSG['btn']['exceldwn']['en'] = "Save as excel sheet";
	$aMSG['btn']['vcarddwn']['de'] = "Kontakt als VCard speichern";
	$aMSG['btn']['vcarddwn']['en'] = "Save contact as VCard";
	

	// download-modul
	$aMSG['btn']['presentation']['de'] = "Präsentation";
	$aMSG['btn']['presentation']['en'] = "Presentation";
	$aMSG['btn']['mediafile']['de'] = "Media-File";
	$aMSG['btn']['mediafile']['en'] = "Media file";
	$aMSG['btn']['ftp_path']['de'] = "FTP-Link";
	$aMSG['btn']['ftp_path']['en'] = "FTP link";
	
	// prio-pfeile / folder icons
	$aMSG['btn']['prio_top']['de'] = "Zur ersten Position";
	$aMSG['btn']['prio_top']['en'] = "To the first position";
	$aMSG['btn']['prio_up']['de'] = "Eine Position höher";
	$aMSG['btn']['prio_up']['en'] = "One position up";
	$aMSG['btn']['prio_dwn']['de'] = "Eine Position runter";
	$aMSG['btn']['prio_dwn']['en'] = "One position down";
	$aMSG['btn']['prio_btm']['de'] = "Zur letzten Position";
	$aMSG['btn']['prio_btm']['en'] = "To the last position";
	$aMSG['btn']['prio_az']['de'] = "Unterordner alphabetisch umsortieren";
	$aMSG['btn']['prio_az']['en'] = "Sort subfolders by alphabet";
	$aMSG['navi']['show_subsections']['de'] = "Zeige Unterpunkte";
	$aMSG['navi']['show_subsections']['en'] = "Show subsections";
	$aMSG['navi']['hide_subsections']['de'] = "Verstecke Unterpunkte";
	$aMSG['navi']['hide_subsections']['en'] = "Hide subsections";
	
	// adr - web_cug
	$aMSG['btn']['add_contact']['de'] = "Kontakt hinzufügen";
	$aMSG['btn']['add_contact']['en'] = "Add contact";
	
	//popup Textmodule
	$aMSG['btn']['takeacross']['de'] = "Übernehmen";
	$aMSG['btn']['takeacross']['en'] = "Take across";
	
// Detail-Seite-Modus
	$aMSG['mode']['edit']['de'] = "editieren"; // die keys 'edit' und 'new' duerfen nicht veraendert werden!
	$aMSG['mode']['edit']['en'] = "edit";
	$aMSG['mode']['new']['de'] = "neu anlegen";
	$aMSG['mode']['new']['en'] = "new";
	$aMSG['mode']['copy']['de'] = "kopieren";
	$aMSG['mode']['copy']['en'] = "copy";
	$aMSG['mode']['delete']['de'] = "löschen";
	$aMSG['mode']['delete']['en'] = "delete";

// SEITEN-SPEZIFISCH #########################################################################################
	
// Login
	$aMSG['login']['headline']['de'] = "Login";
	$aMSG['login']['headline']['en'] = "Login";
	$aMSG['login']['headline_failed']['de'] = "Anmeldung nicht erfolgreich";
	$aMSG['login']['headline_failed']['en'] = "Login failed";
	$aMSG['login']['error']['de'] = "Bitte überprüfen Sie Ihre Login-Daten und versuchen Sie es noch einmal.";
	$aMSG['login']['error']['en'] = "Please check your username and password and try again.";
	$aMSG['login']['trials_left']['de'] = "Weitere Versuche: ";
	$aMSG['login']['trials_left']['en'] = "Trials left: ";
	$aMSG['login']['locked']['de'] = "Sie haben drei mal ein falsches Passwort eingegeben. <br><b>Der Zugang ist aus Sicherheitsgründen für die nächsten 15 Minuten gesperrt.</b>";
	$aMSG['login']['locked']['en'] = "You have entered the wrong password three times. <br><b>The access is locked for 15 minutes.</b>";
	$aMSG['login']['deactivated']['de'] = "Ihre Nutzerkennung wurde deaktiviert. Sie können Sich nicht mehr am System anmelden.";
	$aMSG['login']['deactivated']['en'] = "Your user account was been deactivated. You can't login to the system any more.";
	$aMSG['login']['hl_email']['de'] = "Anmelde-Daten vergessen?";
	$aMSG['login']['hl_email']['en'] = "Forgot your username/password?";
	$aMSG['login']['txt_email']['de'] = "Bitte tragen Sie Ihre E-Mail Adresse hier ein. Ihre Anmelde-Daten werden Ihnen zugesendet.";
	$aMSG['login']['txt_email']['en'] = "Please, type in your email address and your login data will be sent to you.";
	
// Portal
	$aMSG['portal']['hl_welcome']['de'] = "Willkommen bei";
	$aMSG['portal']['hl_welcome']['en'] = "Welcome to the home of";
	$aMSG['portal']['birthdays']['de'] = "Geburtstage";
	$aMSG['portal']['birthdays']['en'] = "Birthdays";
	$aMSG['portal']['currencycalculator']['de'] = "Währungsrechner";
	$aMSG['portal']['currencycalculator']['en'] = "Currency calculator";
	$aMSG['portal']['cminchcalculator']['de'] = "CM/Inch Rechner";
	$aMSG['portal']['cminchcalculator']['en'] = "CM/Inch calculator";
	$aMSG['portal']['cal_events_today']['de'] = "Termine heute";
	$aMSG['portal']['cal_events_today']['en'] = "Schedule today";
	$aMSG['form']['nodates']['de'] = "Keine Termine";
	$aMSG['form']['nodates']['en'] = "No appointments";
	$aMSG['form']['mydates']['de'] = "Meine Termine";
	$aMSG['form']['mydates']['en'] = "My appointments";
	$aMSG['form']['today']['de'] = "Heute";
	$aMSG['form']['today']['en'] = "Today";
	$aMSG['form']['participant']['de'] = "Teilnehmer";
	$aMSG['form']['participant']['en'] = "Participant";
	$aMSG['portal']['weeks']['de'] = "Wochen";
	$aMSG['portal']['weeks']['en'] = "weeks";
	
// User

	$aMSG['user']['allowed_ips']['de']	= "Erlaubte IP Adressen";
	$aMSG['user']['allowed_ips']['en']	= "Allowed IP addresses";
	$aMSG['user']['subheadline']['de'] = "Die Nutzerdaten sind für alle Anwendungen identisch!";
	$aMSG['user']['subheadline']['en'] = "All applications use the same userdata!";
	$aMSG['user']['contact']['de'] = "Kontaktdaten";
	$aMSG['user']['contact']['en'] = "Contact";
	$aMSG['user']['system_lang_default']['de'] = "Bevorzugte System-Sprache";
	$aMSG['user']['system_lang_default']['en'] = "Default system language";
	$aMSG['user']['content_lang_default']['de'] = "Bevorzugte Website Sprache";
	$aMSG['user']['content_lang_default']['en'] = "Default website language";
	
	$aMSG['user']['no_usergroup_defined']['de'] = "Es ist noch keine CUG Nutzergruppe erstellt worden. Die Nutzer-Rechte können nicht zugewiesen werden.";
	$aMSG['user']['no_usergroup_defined']['en'] = "No CUG usergroup has been created. The user rights can not be assigned.";
	$aMSG['user']['no_usergroup_assigned']['de'] = "Der Nutzer ist momentan keiner Gruppe zugeordnet und hat somit keine Nutzungsrechte.";
	$aMSG['user']['no_usergroup_assigned']['en'] = "This user has not been assigned to a CUG usergroup and has therefore no rights of use.";
	$aMSG['user']['restore_user']['de'] = "Zum Wiederherstellen des Nutzers eine Nutzergruppe zuweisen und sichern.";
	$aMSG['user']['restore_user']['en'] = "To restore this user reassign a usergroup and save.";
	$aMSG['user']['user_restored']['de'] = "Der Nutzer ist wiederhergerstellt worden!";
	$aMSG['user']['user_restored']['en'] = "This user has been restored!";
	$aMSG['user']['copy_of']['de'] = "Kopie von ";
	$aMSG['user']['copy_of']['en'] = "Copy of ";
	$aMSG['user']['new_user_usergroup']['de'] = "Neue Nutzergruppe";
	$aMSG['user']['new_user_usergroup']['en'] = "New usergroup";
	$aMSG['user']['edit_user_usergroup']['de'] = "Nutzergruppen editieren";
	$aMSG['user']['edit_user_usergroup']['en'] = "Edit usergroups";
	$aMSG['user']['new_user']['de'] = "Neuen Nutzer anlegen";
	$aMSG['user']['new_user']['en'] = "New user";
	$aMSG['user']['edit_user']['de'] = "Nutzer editieren";
	$aMSG['user']['edit_user']['en'] = "Edit user";

	$aMSG['access']['open']['de'] = "Öffentlich";
	$aMSG['access']['open']['en'] = "Public access";
	$aMSG['access']['closed']['de'] = "Eingeschränkter Zugriff";
	$aMSG['access']['closed']['en'] = "Limited access";
	
	$aMSG['form']['forumstatus']['de']	= "Status";
	$aMSG['form']['forumstatus']['en']	= "State";
// Kalender
	$aMSG['cal']['viewday']['de'] = "Tagesansicht";
	$aMSG['cal']['viewday']['en'] = "View day";
	$aMSG['cal']['viewweek']['de'] = "Wochenansicht";
	$aMSG['cal']['viewweek']['en'] = "View week";
	$aMSG['cal']['viewmonth']['de'] = "Monatsansicht";
	$aMSG['cal']['viewmonth']['en'] = "View month";
	
// Help-Popup
	// headline/button
	$aMSG['help']['hl_faq']['de'] = "FAQ";
	$aMSG['help']['hl_faq']['en'] = "FAQ";
	// help-form
	$aMSG['help']['text_form']['de'] = "Frage/Anregung/Kommentar";
	$aMSG['help']['text_form']['en'] = "Comment/suggestion/question";
	$aMSG['help']['text_ok']['de'] = "Ihre Nachricht wurde erfolgreich an uns geschickt und wird von einen unserer Mitarbeiter so schnell wie möglich bearbeitet. <br><br>Vielen Dank.";
	$aMSG['help']['text_ok']['en'] = "Your message has been sent successfully and will be dealt with by a human being. So please be patient. We will get back to you as soon as possible. <br><br>Thank you.";
	$aMSG['help']['alert']['de'] = "Bitte geben Sie Ihre Nachricht ein.";
	$aMSG['help']['alert']['en'] = "Please type in your message.";
	$aMSG['help']['sender_hl']['de'] = "Bitte geben Sie Ihren Namen und Ihre E-Mail Adresse ein.";
	$aMSG['help']['sender_hl']['en'] = "Please enter your name and email address.";
	$aMSG['help']['name_form']['de'] = "Name";
	$aMSG['help']['name_form']['en'] = "Name";
	$aMSG['help']['email_form']['de'] = "E-Mail";
	$aMSG['help']['email_form']['en'] = "Email";
	
// Login-Bookmark-Popup
	$aMSG['lb']['headline']['de'] = "Auto Login";
	$aMSG['lb']['headline']['en'] = "Auto Login";
	$aMSG['lb']['description']['de'] = "Beim Login muss sich der Systembenutzer durch Eingabe der Benutzerkennung und des Passwortes kenntlich machen. Dies erfolgt beim Auto Login automatisch. <br>Sie können sich die Startseite inklusive Login-Parameter bookmarken. Diese sind natürlich verschlüsselt.";
	$aMSG['lb']['description']['en'] = "To login a user has to enter the username and password. With the auto login this happens automatically. You can bookmark your homepage including the login information. This information is certainly encoded. ";
	$aMSG['lb']['oktxt1']['de'] = "Dazu klicken Sie bitte einfach folgenden Link, der Ihre Bookmarkverwaltung öffnet:";
	$aMSG['lb']['oktxt1']['en'] = "Please click this link to bookmark the site.";
	$aMSG['lb']['oktxt2']['de'] = "Oder kopieren Sie den folgenden Link um ein Lesezeichen zu erstellen:";
	$aMSG['lb']['oktxt2']['en'] = "Or copy this link to create the bookmark:";
	$aMSG['lb']['kotxt']['de'] = "Die Funktion zum automatischen Erstellung eines Bookmarks wird von Ihrem Browser nicht unterstützt.<br>Kopieren Sie bitte den folgenden Link in Ihre Bookmarkverwaltung:";
	$aMSG['lb']['kotxt']['en'] = "The functionality to automaticaly create bookmarks is not integrated in your browser. Please copy this link and add it to your bookmark catalog.";
	$aMSG['lb']['sidebar_help']['de'] = "Anmerkung: Wenn sich die Seite in der linken Sidebar öffnet, gehen Sie bitte auf &quot;Eigenschaften&quot;) und deaktivieren Sie die Checkbox &quot;Dieses Lesezeichen in der Sidebar laden&quot;";
	$aMSG['lb']['sidebar_help']['en'] = "Note: I case the site opens in your sidebar, please go to &quot;Properties&quot; and deactivate the checkbox &quot;Load this bookmark in the sidebar&quot; " ;
	$aMSG['lb']['openwin']['de'] = "Auto Login Bookmark";
	$aMSG['lb']['openwin']['en'] = "Auto login bookmark";
	$aMSG['lb']['mobile_login']['de'] = "Mobile-Login: ";
	$aMSG['lb']['mobile_login']['en'] = "Mobile login: ";

// Label | Textmodule
	$aMSG['label']['title']['de'] = "Labels editieren";
	$aMSG['label']['title']['en'] = "Edit labels";
	$aMSG['textmodule']['title']['de'] = "Textbausteine editieren";
	$aMSG['textmodule']['title']['en'] = "Edit text modules";
?>