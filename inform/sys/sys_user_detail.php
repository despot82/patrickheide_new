<?php
/**
* sys_user_detail.php
*
* Detailpage: user (also alle, die das System nutzen wollen)
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aUg
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Nils Hitze <nh@design-aspekt.com>
* @version  2.5 / 2006-10-25 (bugfix) passwortfeld bleibt leer wenn kein neues Passwort
* #history	2.4 / 2005-11-14 (NEUES RECHTEMANAGEMENT - NICHT MEHR ABWAERTSKOMPATIBEL!)
* #history	2.3 / 2004-11-03 (username + pw muss jetzt JEWEILS unique sein!)
* #history	2.2 / 2004-06-29 (session loeschen wenn eigener user geloescht wird)
* #history	2.1 / 2004-01-31 (v2)
* #history	1.0 / 2003-09-01
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_user_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("./php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$sGEToptions	= "?start=".$start;

// 2b. POST-params abholen
	$aUg			= (isset($_POST['aUg'])) ? $_POST['aUg'] : array();
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$locked			= (isset($_POST['locked'])) ? $oFc->make_secure_int($_POST['locked']) : '';	
	// DL-files
	$aDl			= (isset($_POST['aDl'])) ? $_POST['aDl'] : array();

// 2c. Vars:
	$sTable			= $aENV['table']['sys_user'];
	$sViewerPage	= $aENV['SELF_PATH']."sys_user.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// init mediadb (VOR DB!)
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	$oMediadb	=& new mediadb(); // params: - 

// 3. DB
// DB-action: delete
	if (isset($btn['deactivate']) && $aData['id']) {
		// altes Bild in MediaDB loeschen
		$oMediadb->moduleDelete($aData['picture']); // params: $mdbId
		
		// Stamm-Datensatz nicht wirklich loeschen, sondern nur als geloescht markieren
		$oDb->query("UPDATE ".$sTable." SET flag_deleted='1' WHERE id='".$aData['id']."'");

		// UG: loesche alle verknuepfungen mit diesem user zu usergroups
		_removeUgAssignments($aData['id']); // params: $nUserId

		// wenn eigener Eintrag geloescht wurde
		if ($Userdata['id'] == $aData['id']) {
			$oSess->destroy(); // -> loesche meine Userdata
			header("Location: ".$aENV['page']['welcome']); exit;
		}
		// weiterleitung overviewpage
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) || isset($btn['save_close'])) {
		$aOData	= $oDb->fetch_by_id($aData['id'],$sTable);
		
		//[nh] delock User by setting their wrong logincount back to 0
		if($locked!=1) {
			$aData['failed_login_count'] = 0;
		}
		
		// if aData password != empty && aData password != original password set flag new password
		if(!empty($aData['password']) 
		&& $aData['password'] != $aOData['password']
		&& $Userdata['id'] == $aData['id']) {
			$flag['newpassword']	= 1;
		}

		// if aData password == empty old password stays
		if(empty($aData['password'])) {
			$aData['password']			= $aOData['password'];
			$aData['confirm_password']	= $aOData['confirm_password'];
		}

		// if original password != input old password
		if($aData['oldpassword'] == '' 
		&& !$oPerm->hasPriv('admin')) {
			echo js_alert($aMSG['err']['pw_notold'][$syslang]);
			$alert	= true;	
		}	
				
		// if original password != input old password
		if($aOData['password'] != $aData['oldpassword'] 
		&& $aData['oldpassword'] != '' && !$oPerm->hasPriv('admin')) {
			echo js_alert($aMSG['err']['pw_notold'][$syslang]);
			$alert	= true;	
		}	
		
		if(!empty($aData['confirm_password']) && ($aData['oldpassword'] == '' || $aOData['password'] == $aData['oldpassword'])) {
			
		}
		unset($aData['oldpassword']);

		// erst checken ob username = unique
		if (_checkIfUsernameExists($aData['username'], $aData['id'])) {
			echo js_alert($aMSG['err']['username_notunique'][$syslang]);
			$alert	= true;
		}
		// dann checken ob password = unique
		if (!isset($alert)) {
			if (_checkIfPasswordExists($aData['password'], $aData['id'])) {
				echo js_alert($aMSG['err']['pw_notunique'][$syslang]);
				$alert	= true;
			}
		}
		// dann checken ob email = unique
		if (!isset($alert)) {
			if (_checkIfEmailExists($aData['email'], $aData['id'])) {
				echo js_alert($aMSG['err']['email_notunique'][$syslang]);
				$alert	= true;
			} // end if
		} // end if isset alert
	} // end if save || save&close
	
	if ((isset($btn['save']) || isset($btn['save_close'])) && !isset($alert)) {
		// Media-DB: ggf. altes Bild loeschen
		if (isset($_POST['delete_picture']) && !empty($_POST['existing_picture'])) {
			$oMediadb->moduleDelete($_POST['existing_picture']); // params: $mdbId
			$aData['picture']	= '';
		}
		
		// Media-DB: ggf. neues Bild hochladen
		if (isset($_FILES['picture']['name']) && !empty($_FILES['picture']['name'])) {
			$oMediadb->moduleDelete($_POST['existing_picture']); // params: $mdbId
			$aData['picture']	= $oMediadb->moduleUpload('picture', $aData['firstname'].' '.$aData['surname'], $aMSG['topnavi']['users'][$syslang]); // params: $sUploadFieldname[,$sDescription=''][,$sKeywords='']
		}
		
		if ($aData['id']) { // Update eines bestehenden Eintrags
			// geloeschten user wiederherstellen, wenn ihm eine usergroup zugewiesen wurde
			if ($aData['flag_deleted'] == '1' && count($aUg) > 0) {
				$aData['flag_deleted'] = '0';
				$msg_restored	= true;
			}

			$aData['last_mod_by'] = $Userdata['id'];
			$oDb->make_update($aData, $sTable, $aData['id']);
			if($Userdata['id'] == $aData['id']) {
				// Userdaten in der Session aktualisieren
				$Userdata['firstname']	= $aData['firstname'];
				$Userdata['surname']	= $aData['surname'];
				$Userdata['username']	= $aData['username'];
				$Userdata['email']		= $aData['email'];
				$Userdata['system_lang_default']	= $aData['system_lang_default']; 
				$oSess->set_var('Userdata',$Userdata);
				$oSess->set_var('syslang',$aData['system_lang_default']);
			}
		} else { // Insert eines neuen Eintrags
			$aData['created']		= $oDate->get_mysql_timestamp();
			$aData['created_by']	= $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			$aData['id']	= $oDb->insert_id();
		}
		
		// UG: usergroups in verknuepfungstabelle speichern
		if ($oPerm->hasPriv('admin')) { // usergroups kann nur admin sehen (+ damit aendern!)
			_assignUsergroup($aData['id'], $aUg); // params: $nUserId, $usergroup
		}
		
		// if password == new relocate to login page
		if ($flag['newpassword'] == 1) {
			$oSess->destroy();
			header("Location: sys_portal.php"); exit;
		}
		
		// if save&close relocate to headerpage
		if (isset($btn['save_close'])) {
			header("Location: ".$sViewerPage); exit;
		}
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && $id) {
		$aData	= $oDb->fetch_by_id($id, $sTable);
		$mode_key	= "edit"; // do not change!
	
		$aData['password']			= '';
		$aData['confirm_password']	= '';
	} else {
		$mode_key = "new"; // do not change!
		// office_id mit der des users vorbelegen
		$aData['office_id'] = $Userdata['office_id'];
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("firstname", $aMSG['form']['firstname'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	$oForm->check_field("surname", $aMSG['form']['surname'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	$oForm->check_field("username", $aMSG['form']['username'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	$sSonderJScheck = "obj = elements['aData[password]']; obj2 = elements['aData[confirm_password]'];\n";
	$sSonderJScheck .= "if (obj.value != obj2.value) {ok=false;m=' ".$aMSG['err']['confirm_mismatched'][$syslang]." ';mfocus=obj;}\n";
	$oForm->add_js($sSonderJScheck); // params: $sJScode
	$oForm->check_field("email", $aMSG['form']['email'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}

	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] 
?>
	
	<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">
	
	<?php echo $oForm->hidden('flag_deleted', '0'); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['active_users'][$syslang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td align="right"><?php // Buttons 
			echo $oForm->button("BACK"); // params: $sType[,$sClass='']
			echo $oForm->button("NEW"); // params: $sType[,$sClass='']
		?></td>
	</tr>
</table>

<?php echo HR; ?>

<?php include_once("inc.sys_detnavi.php"); 
	echo getSysUserSubnavi(); ?>

<script>
// opens new window for 'loginbookmark'
function loginbookmarkWin() {
	var left = (screen.width - 582);
	var uri = sysPath+'popup_loginbookmark.php?userid=<?=$aData['id']?>';
	lbWin = window.open(uri,'loginbookmark','toolbar=0,status=0,scrollbars=1,width=550,height=280,resizable=1,top=30,left='+left);
	lbWin.focus();
}
</script>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['gender'][$syslang]; ?>*</b></span></td>
		<td>
			<input type="radio" name="aData[gender]" value="m" <?php if ($aData['gender'] == "m") {echo "checked";} ?> title="<?php echo $aMSG['form']['gender_m'][$syslang]; ?>" class="radiocheckbox"><img src="<?php echo $aENV['path']['pix']['http']; ?>m.gif" width="15" height="16" title="<?php echo $aMSG['form']['gender_m'][$syslang]; ?>"  alt="<?php echo $aMSG['form']['gender_m'][$syslang]; ?>" border="0">&nbsp;&nbsp;
			<input type="radio" name="aData[gender]" value="f" <?php if ($aData['gender'] == "f") {echo "checked";} ?> title="<?php echo $aMSG['form']['gender_f'][$syslang]; ?>" class="radiocheckbox"><img src="<?php echo $aENV['path']['pix']['http']; ?>f.gif" width="15" height="16" title="<?php echo $aMSG['form']['gender_f'][$syslang]; ?>" alt="<?php echo $aMSG['form']['gender_f'][$syslang]; ?>" border="0">
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['firstname'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->textfield("firstname",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['surname'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->textfield("surname",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td class="sub2"><span class="text"><b><?php echo $aMSG['form']['username'][$syslang]; ?> *</b></span></td>
		<td class="sub2"><?php echo $oForm->textfield("username",40,33); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		<?php if ($mode_key == "edit") { ?>
		<a href="javascript:loginbookmarkWin();" class="text"><?php echo $aMSG['lb']['openwin'][$syslang]; ?></a></td>
		<?php } ?>
	</tr>
	<? if(!$oPerm->hasPriv('admin')) { ?>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['oldpassword'][$syslang]; ?></span></td>
		<td class="sub2"><input type="password" name="aData[oldpassword]" size="38" maxlength="30"></td>
	</tr>
	<? } ?>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['newpassword'][$syslang]; ?></span></td>
		<td class="sub2"><input type="password" name="aData[password]" value="<?php echo $aData['password']; ?>" size="38" maxlength="30"></td>
	</tr>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['confirm_password'][$syslang]; ?></span></td>
		<td class="sub2"><input type="password" name="aData[confirm_password]" value="<?php echo $aData['confirm_password']; ?>" size="38" maxlength="30"></td>
	</tr>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php // OFFICE
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL) 
	$aOffice = $oUser->getAllOfficeNames(); // params: [$bWithUsers=false]
	// wenn mehr als ein Office -> DropDown anzeigen
	if (count($aOffice) > 1) { ?>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['office'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->select("office_id", $aOffice); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false] ?></td>
	</tr>
<?php } else { // sonst hiddenfield
		$office_id = (count($aOffice) == 1) ? key($aOffice) : 0; ?>
	<input type="hidden" name="aData[office_id]" value="<?php echo $office_id; ?>">
<?php } // END office ?>
	<tr valign="top"><!-- ACHTUNG: email muss pflichtfeld bleiben! -->
		<td><span class="text"><b><?php echo $aMSG['form']['email'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->textfield("email",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['phone'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("phone",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
<?php if (isset($aENV['module']['frm'])) { ?>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['picture'][$syslang]; ?> [ 80 x 80 pixel ]</span></td>
		<td><span class="text"><?php 
		echo $oForm->file_upload('picture'); // params: [$sFieldname='uploadfile'][,$sPathKey='mdb_upload']
		?></span></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['description'][$syslang]; ?></span></td>
		<td><span class="text"><?php 
		echo $oForm->textarea_flash('description', $sTable, $aData['id'],'small'); 
		?></span></td>
	</tr>
<?php } ?>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php if ($oPerm->hasPriv('admin')) { // usergroups soll nur admin sehen ?>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['userrightgroup'][$syslang]; ?> *</b></span></td>
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr valign="top">
			<td><?php 
			// alle Verknuepften Usergroups ermitteln...
			$aUgRelatedId = _getUsersUsergroupIds($aData['id']); // params: $sUserId
			// ... alle Usergroups ermitteln ...
			$aUG = _getAllUsergroups(); // params: -
			foreach ($aUG as $ug_id => $ug_title) {
			// und als checkboxen ausgeben
				$checked = (is_array($aUgRelatedId) && in_array($ug_id, $aUgRelatedId)) ? ' checked' : '';
				echo '<input type="checkbox" name="aUg[]" id="'.$ug_id.'" value="'.$ug_id.'" class="radiocheckbox"'.$checked.'>';
				echo '<label for="'.$ug_id.'" class="text"> '.$ug_title.'<br></label>'."\n";
			}
			// hinweis wenn noch keine usergroups existieren
			if (count($aUG) == 0) {
				echo '<span class="small"><a href="javascript:confirmJumpToUG()">'.$aMSG['user']['no_usergroup_defined'][$syslang].'</a><br></span>';
			}
			// hinweis wenn dem user keine usergroups zugeordnet sind
			/*if (count($aUgRelatedId) == 0) {
				echo '<span class="small">'.$aMSG['user']['no_usergroup_assigned'][$syslang].'<br></span>';
			}*/ ?>
			</td>
			<?php if ($mode_key == "edit") { ?>
			<td align="right">
			<script language="JavaScript" type="text/javascript">
			function confirmJumpToUG(towhere) {
				var chk = window.confirm("<?php echo $aMSG['err']['is_data_saved'][$syslang]; ?>");
				var loc = (towhere == 'new') ? 'sys_usergroup_detail.php?user_id=<?php echo $aData['id']; ?>' : 'sys_usergroup.php';
				if(chk==true){
					location.replace('<?php echo $aENV['path']['sys']['http']; ?>'+loc)
				}
			}
			</script>
			<a href="#" onclick="confirmJumpToUG('edit')" title="<?php echo $aMSG['user']['edit_user_usergroup'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['user']['edit_user_usergroup'][$syslang]; ?>">| <?php echo $aMSG['user']['edit_user_usergroup'][$syslang]; ?>&nbsp;</a>
			<a href="#" onclick="confirmJumpToUG('new')" title="<?php echo $aMSG['user']['new_user_usergroup'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_add.gif" alt="<?php echo $aMSG['user']['new_user_usergroup'][$syslang]; ?>">| <?php echo $aMSG['user']['new_user_usergroup'][$syslang]; ?>&nbsp;</a>
			</td><?php } // END if ?>
			</tr></table>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['user']['allowed_ips'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("allowed_ips",250,75); ?></td>
	</tr>

	
<?php } ?>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php if (count($aENV['system_language']) > 2) {?>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['user']['system_lang_default'][$syslang]; ?></span></td>
		<td><select name="aData[system_lang_default]" size="1">
<?php	if (!isset($aData['system_lang_default']) || empty($aData['system_lang_default'])) {$aData['system_lang_default'] = $aENV['system_language']['default'];} // fallback
		foreach ($aENV['system_language'] as $key => $val) {
			if ($key == $aData['system_lang_default']) {$sel = " selected";} else {$sel="";}
			if ($key != 'default') { echo "<option value=\"$key\"$sel>$val</option>\n"; } // don't show default-language as option!
		} ?>
			</select>
		</td>
	</tr>
<?php } else { // sonst hiddenfield
		$system_lang_default = (isset($aData['system_lang_default']) && !empty($aData['system_lang_default'])) ? $aData['system_lang_default'] : $aENV['system_language']['default'];
		if (empty($system_lang_default)) $system_lang_default = 'en'; // fallback ?>
	<input type="hidden" name="aData[system_lang_default]" value="<?php echo $system_lang_default; ?>">
<?php } ?>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php if($aData['failed_login_count']>= 3) { ?>	
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['forumstatus'][$syslang]; ?></span></td>
		<td class="sub2"><span class="text">
			<input type="checkbox" name="locked" value="1" checked="checked" onclick="checkvalue()"/>
			<lable class="text"><?php echo $aMSG['std']['locked'][$syslang]; ?></lable>			
		</span></td>
	</tr>
<?php } ?>	
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['std']['last_login'][$syslang]; ?></span></td>
		<td class="sub2"><span class="text"><?php echo $oDate->datetime_mysql2trad($aData['last_login']); ?></span></td>
	</tr>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($aData['flag_deleted'] != '1' && $oPerm->hasPriv('admin')) { echo $oForm->button("DEACTIVATE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php /* DEBUG
	echo "<pre>";
	print_r($Userdata);
	echo "</pre>";*/ ?>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");

// HILFSFUNKTIONEN /////////////////////////////////////////////////////////
	function _getUsersUsergroupIds($nUserId) {
		if (empty($nUserId)) return;
		global $oDb, $aENV;
		$buffer = array();
		$oDb->query("SELECT `usergroup_id` 
					FROM `".$aENV['table']['sys_user_usergroup']."` 
					WHERE `user_id` = '".$nUserId."'");
		while ($tmp = $oDb->fetch_array()) {
			$buffer[] = $tmp['usergroup_id'];
		}
		return $buffer;
	}
	
	function _getAllUsergroups() {
		global $oDb, $aENV, $syslang;
		$buffer = array();
		$oDb->query("SELECT `id`,`title` 
					FROM `".$aENV['table']['sys_usergroup']."` 
					ORDER BY `id`");
		while ($tmp = $oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['title'];
		}
		return $buffer;
	}
	function _removeUgAssignments($nUserId) {
		if (empty($nUserId)) return;
		global $oDb, $aENV;
		// alle verknuepfungen mit diesem user und usergroups loeschen
		$aData2delete = array('user_id' => $nUserId);
		return $oDb->make_delete($aData2delete, $aENV['table']['sys_user_usergroup']);
	}
	function _assignUsergroup($nUserId, $usergroup) {
		if (empty($nUserId)) return;
		global $oDb, $aENV, $Userdata;
		// erst alle eintraege mit diesem verknuepfungs-datensatz loeschen...
		_removeUgAssignments($nUserId);
		
		// check value
		if (empty($usergroup)) return;
		if (!is_array($usergroup)) { $usergroup = explode(',', $usergroup); }
		if (!is_array($usergroup)) return;
		
		// dann alle verknuepfungen neu schreiben
		foreach ($usergroup as $ug) {
			$aInsert = array('usergroup_id'	=> $ug, 
							 'user_id'		=> $nUserId, 
							 'created_by'	=> $Userdata['id']);
			$oDb->make_insert($aInsert, $aENV['table']['sys_user_usergroup']);
		}
	}
	function _checkIfUsernameExists($sUsername, $sId) {
		global $oDb, $sTable;
		$sAddQuery = (!is_null($sId) || !empty($sId)) ? " AND `id` != '".$sId."'" : '';
		$oDb->query("SELECT `id` FROM `".$sTable."` WHERE `username`='".$sUsername."'".$sAddQuery);
		$entries = $oDb->num_rows();
		return ($entries > 0) ? true : false;
	}
	function _checkIfPasswordExists($sPassword, $sId) {
		global $oDb, $sTable;
		$sAddQuery = (!is_null($sId) || !empty($sId)) ? " AND `id` != '".$sId."'" : '';
		$oDb->query("SELECT `id` FROM `".$sTable."` WHERE `password`='".$sPassword."'".$sAddQuery);
		$entries = $oDb->num_rows();
		return ($entries > 0) ? true : false;
	}
	function _checkIfEmailExists($sEmail, $sId) {
		global $oDb, $sTable;
		$sAddQuery = (!is_null($sId) || !empty($sId)) ? " AND `id` != '".$sId."'" : '';
		$oDb->query("SELECT `id` FROM `".$sTable."` WHERE `email`='".$sEmail."'".$sAddQuery);
		$entries = $oDb->num_rows();
		return ($entries > 0) ? true : false;
	}

?>
