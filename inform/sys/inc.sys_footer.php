<?php
/**
* inc.sys_footer.php
*
* HTML-Footer-Include (+ Page-Infos) - sollte in jedes system-file includiert sein.
* -> 2sprachig und voll kopierbar!
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.42 / 2005-02-09 (BUGFIX: Ausgabe Uhrzeit bei created + last_modified)
* #history	1.41 / 2005-01-19 (NEU: Flashtest nur wenn im Modul CMS oder PMS)
* #history	1.4 / 2005-01-19 (NEU: auf neues RECHTESYSTEM umgestellt - NICHT MEHR ABWAERTSKOMPATIBEL!!!)
* #history	1.3 / 2004-10-26 (NEU: Unterscheidung "printview" hinzugefuegt)
* #history	1.2 / 2004-10-21 (NEU: Flashtest hinzugefuegt)
* #history	1.1 / 2004-07-07 (NEU: "layout" bei "template" hinzugefuegt)
* #history	1.0 / 2004-04-27
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = $aENV['path']['sys']['unix'].'inc.sys_footer.custom.php';
	if (file_exists($custompage)) {
		include($custompage); 
	}
	else {
	
	if (!isset($_GET['printview']) || $_GET['printview'] != "true") { // bei Druckansicht KEINE INFOS 
		//	page-infos sind datum und user der erstellung und letzten modifikation, DB-table, Datensatz-ID etc.
		//	benoetigt (optional) folgende variablen:
		//	$oDate, $aENV, $aMSG, $syslang, $aData, $Userdata, [$aPageInfo,] [$navi_id,] $id
		
		// BLOCK Ersetzungen vorbereiten
		$aInfo = array();
		if (isset($aData['created']) && !empty($aData['created'])) { // mindest-bedingung
		// 1. first_created
			$sStr = $oDate->date_from_mysql_timestamp($aData['created']).' - ';
			$sStr .= $oDate->time_from_mysql_timestamp($aData['created']);
			$sStr .= ($syslang == 'de') ? ' Uhr ' : ' ';
			// 1b. first_created_by
			if ($aData['created_by'] != '') { // name
				$oDb->query("SELECT `id`,`firstname`,`surname` FROM `".$aENV['table']['sys_user']."` WHERE `id`='".$aData['created_by']."'");
				$aData2 = $oDb->fetch_array();
				$sStr .= ' | '.$aData2['firstname'].' '.$aData2['surname'].'';
			}
			$aInfo[] = array('name' => $aMSG['std']['first_created'][$syslang], 
							'value' => $sStr);
		// 2. last_update
			if (isset($aData['last_modified']) && strstr($aData['last_modified'], '-')) {
				// neues mysql-timestamp-format beachten!
				$aData['last_modified'] = str_replace(array('-',':',' '), '', $aData['last_modified']); // timestamp oder iso-datetime?
			}
			if (isset($aData['last_modified']) && $aData['created'] != $aData['last_modified']) {
				$sStr = $oDate->date_from_mysql_timestamp($aData['last_modified']).' - ';
				$sStr .= $oDate->time_from_mysql_timestamp($aData['last_modified']);
				$sStr .= ($syslang == 'de') ? ' Uhr ' : ' ';
				// 2b. last_update_by
				if (isset($aData['last_mod_by']) && $aData['last_mod_by'] != '') { // name
					$oDb->query("SELECT `id`,`firstname`,`surname` FROM `".$aENV['table']['sys_user']."` WHERE `id`='".$aData['last_mod_by']."'");
					$aData2 = $oDb->fetch_array();
					$sStr .= ' | '.$aData2['firstname'].' '.$aData2['surname'].'';
				}
				$aInfo[] = array('name' => $aMSG['std']['last_update'][$syslang], 
								'value' => $sStr);
			}
		} // END mindest-bedingung
		// ab hier nur noch fuer DA-Service-Login-------------------------------------------
		if (is_object($oPerm) && $oPerm->isDaService()) {
		// 3. filename
			$aInfo[] = array('name' => 'Filename:&nbsp;', 
							'value' => ($aPageInfo['filename']) ? $aPageInfo['filename'] : basename($_SERVER['PHP_SELF']));
		// 4. navi_id
			if (isset($navi_id)) {
				$aInfo[] = array('name' => 'Navi-ID:&nbsp;', 
								'value' => (!empty($navi_id)) ? $navi_id : '--');
			}
		// 5. template (layout)
			if (isset($aPageInfo['template'])) {
				// template
				$value = (!empty($aPageInfo['template'])) ? $aPageInfo['template'] : '--';
				// ggf. layout
				if (count($aENV['layout'][$aPageInfo['template']])) {
					$value .= ' - Layout '.$aPageInfo['layout'];
					$layoutname = (isset($aENV['layout'][$aPageInfo['template']][$aPageInfo['layout']][$syslang])) 
						? strip_tags($aENV['layout'][$aPageInfo['template']][$aPageInfo['layout']][$syslang])
						: strip_tags($aENV['layout'][$aPageInfo['template']][$aPageInfo['layout']]);
				// ggf. layoutname
					if (!empty($layoutname)) {
						$value .= ': "'.$layoutname.'"';
					}
				}
				$aInfo[] = array('name' => 'Template:&nbsp;', 
								'value' => $value);
			}
		// 6. sTable
			if (isset($sTable)) {
				$aInfo[] = array('name' => 'DB-Tabelle:&nbsp;', 
								'value' => $sTable);
			}
		// 7. id
			$id = (empty($id) && isset($aData['id'])) ? $aData['id'] : '';
			if (!empty($id)) {
				$aInfo[] = array('name' => 'Datensatz-ID:&nbsp;', 
								'value' => $id);
			}
		// 8. benchmark
			if (isset($masterbench)) { // in der "_include_all.php"
				$bresult = getBenchmark($masterbench);
				$aInfo[] = array('name' => 'Benchmark:&nbsp;', 
								'value' => $bresult['micro'].' Mikrosekunden');
			}
			$aInfo[]	= array('name'	=> 'PHP Version:&nbsp;',
								'value'	=> phpversion());
		} // END ADMIN---------------------------------------------------------------
		// BLOCK Ersetzungen vorbereiten
		
		$sFooterNavi = '';
		if ( isset($aENV['system_copyright']) && $aENV['system_copyright'] == true ) {
			$sFooterNavi .= '<img src="'.$aENV['path']['config']['http'].'/informlogo.gif" width="56" height="10" alt="INFORM" border="0" style="position:relative; top:1px;"><span class="version">'.$aENV['system_version'].'</span>&nbsp;|&nbsp;';
			$sFooterNavi .= '<a href="javascript:copyrightWin(\''.$aENV['PHP_SELF'].'\');" class="footer" title="Copyright">Copyright</a>&nbsp;|&nbsp;';
		}
		$sFooterNavi .= '<a href="javascript:helpWin(\''.$aENV['PHP_SELF'].'\');" class="footer" title="'.$aMSG['topnavi']['help'][$syslang].'">'.$aMSG['topnavi']['help'][$syslang].'</a>';
		
	// PHPLIB Templatesystem einbinden ############################################
		// Instanz der Templateklasse erzeugen, Pfad zu den Templates angeben
		$tpl =& new Template_PHPLIB($aENV['path']['sys']['unix']."templates/", "comment"); // [keep|remove|comment]
		// Dokumentvorlage laden
		$tpl->setFile(array("main" => "inc.sys_footer.html"));
		$tpl->setBlock("main","infos","i");
		// BLOCK Ersetzungen vornehmen
		foreach($aInfo as $info) {
			$tpl->setVar(array(
				"OPENPAGEINFOS"		=> '<div id="pageinfos"><table border="0" cellpadding="0" cellspacing="0">',
				"FOOTER_NAME"	=> $info['name'],
				"FOOTER_VALUE"	=> $info['value'],
				"CLOSEPAGEINFOS"	=> '</table></div>',
			));
			$tpl->parse("i","infos",true);
		}
			$tpl->setVar(array(
				"COPYRIGHT"		=> $sFooterNavi
			));
		// STD Ersetzungen vornehmen
		$tpl->setVar(array("USER_AGENT" => $oBrowser->getUserAgent()));
		// Seite ausgeben  
		$tpl->pParse("out",array("main")); #######################################
	
	} else {
		echo '</body></html>';
	}
} // END IF CUSTOM!
?>