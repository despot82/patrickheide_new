<?php
/**
* inc.sys_login.php
*
* Login-Include - muss in jedes system-file (wenn es geschuetzt sein soll) zu allererst(!) includiert sein!
* -> 2sprachig und voll kopierbar!
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Nils Hitze <nh@design-aspekt.com>
* @version	2.7 / 2007-07-23 IP Sperre für normales Inform ohne Mobile
* #history	2.5 / 2006-02-27 [NEU: mit mobile-Version zusammengefuehrt]
* #history	2.44 / 2005-06-15 [NEU: CMS-Preview eingebaut]
* #history	2.43 / 2005-04-19 [NEU: $lang in $weblang umbenannt]
* #history	2.42 / 2005-03-16 [NEU: function "_generate_password()" eingebaut (wird noch nicht aufgerufen!)]
* #history	2.41 / 2005-02-21 [BUGFIX: "searchM*" und "searchA*"]
* #history	2.4 / 2005-01-14 [NEUES RECHTEMANAGEMENT - NICHT MEHR ABWAERTSKOMPATIBEL!]
* #history	2.3 / 2004-11-02 [NEU: Zugang sperren nach 3 Fehlversuchen (brute-force) + alle header() absolute gemacht]
* #history	2.25 / 2004-09-23 [BUGFIX: anderer session-name, um ueberschreibungen mit web-/lang-cookies zu vermeiden]
* #history	2.24 / 2004-08-02 [BUGFIX: disallow-pfad korrigiert]
* #history	2.23 / 2004-07-13 [BUGFIX: pfade http-konform gemacht]
* #history	2.22 / 2004-07-05 [BUGFIX: check Userdata: logout statt clear-userdata]
* #history	2.21 / 2004-07-05 [BUGFIX: "require_once("../sys/php/_include_all.php")"]
* #history	2.2 / 2004-05-25 [BUGFIX: "$_GET['lang']" hinzugefuegt!]
* #history	2.1 / 2004-05-05 [NEU: inkludiert nicht mehr die "_include_all.php"!]
* #history	2.0 / 2004-04-27 [NEU: session-klasse / komplett ueberarbeitet]
* #history	1.3 / 2003-05-22
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = $aENV['path']['sys']['unix'].'inc.sys_login.custom.php';
	if (file_exists($custompage)) {
		include($custompage); 
	} else {
	
	// init
		if (file_exists("../sys/php/_include_all.php")) require_once("../sys/php/_include_all.php");
		
		if (!strstr($_SERVER['PHP_SELF'], '/mobile/')) {	// Browsercheck except mobile
			// browser-detection (@see: func.admin_common.php)
			//checkBrowser();
		}
		
		require_once($aENV['path']['global_service']['unix']."class.session.php");
		if (!is_object($oDb)) {
			require_once ($aENV['path']['global_service']['unix']."class.db_mysql.php");
			$oDb	=& new dbconnect($aENV['db']);
		}
	
	// LOGIN-vars abholen
		$btn_login		= false;
		$login_errormsg	= '';
		$login_errornr	= 0;
		
		// bookmark zum direkten einloggen
		if (isset($_GET['l'])) {
			$pub		= base64_decode($_GET['l']);
			$aPub		= explode(';',$pub);
			$username	= $aPub[1];
			$password	= $aPub[0];
			$btn_login	= true;
		}

		// einloggen mit login-form
		if (isset($_POST['btn_login'])) {
			$username	= $_POST['username'];
			$password	= $_POST['password'];
			$btn_login	= true;
		}
	
	// RESCUE MAIL --------------------------------------
		if (isset($_POST['btn_email'])) { // send password-email
			_send_rescue_mail($_POST['email']); // and do nothing...
		}
	
	// SESSION -----------------------------------------------
		$oSess		=& new session($aENV['client_shortname']."_inform");  // params: $sName
		// get session var LOGIN/USER
		$Userdata	= $oSess->get_var('Userdata'); // params: $sName[,$default=null]
		// syslang (if empty, use default from $aENV)
		$syslang	= $oSess->get_var('syslang', $aENV['system_language']['default']); // params: $sName[,$default=null]
		if ($syslang != 'de') $oDate->set_lang($syslang);
		
		// CMS: weblang (if empty, use default from $aENV)
		if (isset($_GET['weblang']))	{ $oSess->set_var('weblang', $_GET['weblang']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		$weblang	= $oSess->get_var('weblang', $aENV['content_language']['default']); // params: $sName[,$default=null]
		
		// CMS: navi_id
		if (isset($_GET['navi_id']))	{ $oSess->set_var('navi_id', $_GET['navi_id']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		$navi_id	= $oSess->get_var('navi_id'); // params: $sName[,$default=null]
		
		// PMS: slot
		if (isset($_GET['slot']))	{ $oSess->set_var('slot', $_GET['slot']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		$slot		= $oSess->get_var('slot'); // params: $sName[,$default=null]
		
		// CMS/PMS: media-db SESSION handling
		if (isset($_GET['searchMTerm']))	{ $oSess->set_var('searchMTerm', $_GET['searchMTerm']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		if (isset($_GET['searchMType']))	{ $oSess->set_var('searchMType', $_GET['searchMType']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		if (isset($_GET['searchMDir']))	{ $oSess->set_var('searchMDir', $_GET['searchMDir']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		
		$searchMTerm	= $oSess->get_var('searchMTerm'); // params: $sName[,$default=null]
		$searchMType	= $oSess->get_var('searchMType'); // params: $sName[,$default=null]
		$searchMDir		= $oSess->get_var('searchMDir'); // params: $sName[,$default=null]
		
		// ADR: ADR SESSION handling
		if (isset($_GET['searchATerm']))	{ $oSess->set_var('searchATerm', $_GET['searchATerm']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		if (isset($_GET['searchAType']))	{ $oSess->set_var('searchAType', $_GET['searchAType']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		if (isset($_GET['orderbyA']))		{ $oSess->set_var('orderbyA', $_GET['orderbyA']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		
		$searchATerm	= $oSess->get_var('searchATerm'); // params: $sName[,$default=null]
		$searchAType	= $oSess->get_var('searchAType'); // params: $sName[,$default=null]
		$orderbyA		= $oSess->get_var('orderbyA'); // params: $sName[,$default=null]
		
		// ADM: prj-status filter
		if (isset($_GET['adm_prjstatus']))	{ $oSess->set_var('adm_prjstatus', $_GET['adm_prjstatus']); } // GET ueberschreibt SESSION (wenn geswitched werden soll)!
		$adm_prjstatus		= $oSess->get_var('adm_prjstatus', '1'); // params: $sName[,$default=null]
	
	// LOGOUT -----------------------------------------------
		
		if (isset($_GET['logout'])) {// ggf. LogOut (NACH SESSION!)
			$oSess->destroy();
			header("Location: ".$aENV['PHP_SELF']); exit;
		}
	
	// LOGIN ------------------------------------------------
		
	// 1. check Userdata (if exists)
		if (isset($Userdata)) {
			_checkIp();

			$sSql	= "SELECT `username`,`password`,`flag_deleted` FROM `".$aENV['table']['sys_user']."` WHERE `username`='".strip_tags(trim($Userdata['username']))."' AND `password`='".strip_tags(trim($Userdata['password']))."'";
			$oDb->query($sSql);

			if ($Userdata['flag_deleted'] == "1" 
			|| $oDb->num_rows() != 1) { 
				$oSess->destroy(); // if no match -> logout!
				
				// fehlermeldung
				$login_errormsg = $aMSG['login']['error'][$syslang];
				$login_errornr	= 1;
				
				header("Location:".$aENV['path']['sys']['http']);
			} // end if
						
		} // end if
		
	// 2. proceed (check) Login
		if ($btn_login) {
			// DB: select
			$sql	= "SELECT * FROM `".$aENV['table']['sys_user']."` WHERE `username`='".strip_tags(trim($username))."'";
			$oDb->query($sql);
			$tmp	= $oDb->fetch_array();

			// 1. ERROR "KEIN treffer"
			if ($oDb->num_rows() == 0) {
				// fehlermeldung
				$login_errormsg = $aMSG['login']['error'][$syslang];
				$login_errornr = 2;
			}

			// 2. ERROR: "mehr als EIN treffer"
			if ($oDb->num_rows() > 1) {
				// system benachrichtigung
				sendSystemErrorMail("Es existiert mehr als ein Eintrag fuer den username \"".strip_tags(trim($username))."\".");
				// fehlermeldung
				$login_errormsg = $aMSG['login']['error'][$syslang];
				$login_errornr = 3;
			}

			// ERROR: "user is deactivated"
			if($tmp['flag_deleted']==1) {
				$login_errormsg = $aMSG['login']['deactivated'][$syslang].'<br>';

				$login_errornr	= 7;
			}
			
			// 3. ERROR: "username is locked (three time the wrong password)"
			if ($login_errornr == 0) {
				$ts_iso		= date("YmdHis", (time() - 900)); // iso-timestamp VOR 15 Minuten
				$lastmod	= (strstr($tmp['last_modified'], '-')) ? str_replace(array('-',':',' '),'',$tmp['last_modified']): $tmp['last_modified']; // mysql-datetime oder iso-datetime?
				if ($tmp['failed_login_count'] > 2 && $lastmod > $ts_iso) {
					// fehlermeldung
					$login_errormsg = $aMSG['login']['locked'][$syslang];
					$login_errornr = 4;
				}
			}
		
			// 4. ERROR "correct username but wrong password"
			if ($login_errornr == 0 && $tmp['password'] != strip_tags(trim($password))) {
				// Update: increment 'failed_login'
				$aUpdateData['failed_login_count'] = $tmp['failed_login_count'] + 1;
				$aUpdate['id'] = $tmp['id'];
				$oDb->make_update($aUpdateData, $aENV['table']['sys_user'], $aUpdate);
					
				if ($aUpdateData['failed_login_count'] > 2) {
					// system benachrichtigung
					$oUser =& new user($oDb);
					$aUser = $oUser->getAllUserData(true);
					foreach($aUser as $key=>$value) {
						$tmp	= _getUserPrivilegesForLogin($value['id']);
	
						if(isset($tmp['sys']['admin']))
							$sReceiveemail[]	= $value['email'];
					}
					$sReceiveemail	= array('inform@design-aspekt.com');
					$sMsg		= str_replace('{USER}',strip_tags(trim($username)),$aMSG['err']['false_logins'][$syslang]);
					sendUserErrorMail($sMsg,$sReceiveemail);
				
					// errormessage
					$login_errormsg = $aMSG['login']['locked'][$syslang];
					$login_errornr = 5;
				} else {
					// errormessage
					$login_errormsg = $aMSG['login']['error'][$syslang].'<br>';
					$login_errormsg .= $aMSG['login']['trials_left'][$syslang].'<b>'.(3-$aUpdateData['failed_login_count']).'</b>';
					$login_errornr = 6;
				}
			}
		
			// OK: "correct username and correct password"
			if ($login_errornr == 0 
			&& $oDb->num_rows() == 1 
			&& $tmp['password'] == strip_tags(trim($password))) {
				
				// get userdata
				$Userdata = $tmp; // copy DB-content in the session
				
				// check IP against allowed IPs
				_checkIp();
				
				// virtuelles feld "permission" mit allen privilegien des users fuellen
				$Userdata['permission'] = _getUserPrivilegesForLogin($tmp['id']);
							
				// Update 'last_login'-field
				$aUpdateData['failed_login_count']	= 0; // ggf. zuruecksetzen!
				$aUpdateData['last_login']			= $oDate->get_php_date();
				$oDb->make_update($aUpdateData, $aENV['table']['sys_user'], $Userdata['id']);
				
				// set session var 'weblang'
				$oSess->set_var('weblang', $Userdata['content_lang_default']); // params: $sName[,$value=null]
				
				// set session var 'syslang'
				$oSess->set_var('syslang', $Userdata['system_lang_default']); // params: $sName[,$value=null]
				
				// set session var 'Userdata'
				$oSess->set_var('Userdata', $Userdata); // params: $sName[,$value=null]
				
				// SONDERFALL: wenn NUR 'cms'::'preview' -> weiter zum preview-frameset!
				if (count($Userdata['permission']) == 1 
				&& isset($Userdata['permission']['cms'])) {
					
					if (count($Userdata['permission']['cms']) == 1 
					&& isset($Userdata['permission']['cms']['preview'])) {
						header("Location: ".$aENV['path']['cms']['http'].'cms_preview.php'); exit;
					} // end if
					
				} // end if
				
				if (strstr($_SERVER['PHP_SELF'], '/mobile/')) {	// keine weiterleitung bei /mobile/
					// syslang (if empty, use default from $aENV)
					$syslang	= $oSess->get_var('syslang', $aENV['system_language']['default']); // params: $sName[,$default=null]
				} else {
					// doppeltes anmelden durch reload vermeiden
					header("Location: ".$aENV['PHP_SELF']."?".$query_string); exit;
				}
			}
		}
		
	// 3. content starts here --------------------------------------------------------
	if (!isset($Userdata) 
	|| empty($Userdata)) { // show login page
		
		// Ersetzungen vorbereiten
		$login_headline		= $aMSG['login']['headline'][$syslang];
		$forgot_headline	= $aMSG['login']['hl_email'][$syslang];
		
		if ($login_errornr > 0) {
			// bei Fehlermeldung -> headline austauschen und passwortmail-funktion hervorheben
			$login_headline = $aMSG['login']['headline_failed'][$syslang];
			$forgot_headline = '<span class="red">'.$aMSG['login']['hl_email'][$syslang].'</span>';
		}
		
		// WAP-/PDA-/...-BROWSER ############################################
		if (strstr($_SERVER['PHP_SELF'], '/mobile/')) {
			require_once ($aENV['path']['mobile']['unix']."inc.mbl_header.php");
			?>
			<p><b><?php echo $login_headline; ?></b></p>
			<p><?php echo $login_errormsg; ?></p>
			<form action="<?php echo isset($_SERVER['QUERY_STRING']) ? $aENV['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'] : $aENV['PHP_SELF']; ?>" method="post" name="loginForm">
			<?php echo $aMSG['form']['username'][$syslang]; ?>:<br />
			<input type="text" name="username" size="18" maxlength="30" class="input" /><br />
			<?php echo $aMSG['form']['password'][$syslang]; ?>:<br />
			<input type="password" name="password" size="18" maxlength="30" class="input" /><br />
			<input type="submit" name="btn_login" value=" Login " class="button" />
			</form>
			<?php
			require_once ($aENV['path']['mobile']['unix']."inc.mbl_footer.php");
		
		} else {	
			
		// WEB-BROWSER ############################################
			require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	
			require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
			// Instanz der Templateklasse erzeugen, Pfad zu den Templates angeben
			$tpl =& new Template_PHPLIB($aENV['path']['sys']['unix']."templates/", "comment"); // [keep|remove|comment]
			// Dokumentvorlage laden
			$tpl->setFile(array("main" => "inc.sys_login.html"));
			// Ersetzungen vornehmen
			$tpl->setVar(array(
				"URL_DISALLOW"		=> $aENV['page']['disallow']."?msg=no_js",
				"LOGIN_FORM_URL"	=> (isset($_SERVER['QUERY_STRING'])) ? $aENV['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'] : $aENV['PHP_SELF'],
				"LOGIN_HEADLINE"	=> $login_headline,
				"LOGIN_ERROR"		=> $login_errormsg,
				"LOGIN_USERNAME"	=> $aMSG['form']['username'][$syslang],
				"LOGIN_PASSWORD"	=> $aMSG['form']['password'][$syslang],
				"EMAIL_FORM_URL"	=> $aENV['PHP_SELF'],
				"EMAIL_HEADLINE"	=> $forgot_headline,
				"EMAIL_TXT"			=> $aMSG['login']['txt_email'][$syslang]
			));
			// Seite ausgeben  
			$tpl->pParse("out", array("main")); #######################################
			
			require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");
		} // END BROWSER
		
		exit;
		
	} // content ends here ------------------------------------------------------------

	// 4. Permissions-Klasse initialisieren (NUR wenn nicht iFrame-header-file!!!)
	if (!strpos($_SERVER['PHP_SELF'], 'inc.reloader.php')) {
		$oPerm	=& new perm(); // params: -

		// aktuelles Modul authentifizieren (->disallow-page, wenn kein "view"-Privileg fuer dieses Modul)
		$oPerm->authModule(); // params: -
	}
} // END IF CUSTOM!

########################################################################### HILFSFUNKTIONEN

	// Hilfsfunktion fuer login-prozedur
	function _getUserPrivilegesForLogin($nUserId) {
		// vars
		if (empty($nUserId)) return;
		global $oDb, $aENV;
		
		// 1. ALLE Privilegien zwischenspeichern
		$aPriv	= array();
		$oDb->query("SELECT `id`,`module_key`,`privilege_key` 
					FROM `".$aENV['table']['sys_privilege']."` 
					ORDER BY `module_key`,`id`");

		while ($tmp = $oDb->fetch_array()) {
			$aPriv[$tmp['id']]['mod'] =  $tmp['module_key'];
			$aPriv[$tmp['id']]['key'] =  $tmp['privilege_key'];
		}

		if (count($aPriv) < 1) { return; }
		
		// 2. alle usergroup-IDs des users ermitteln
		$aUgId	= array();
		$oDb->query("SELECT `usergroup_id` 
					FROM `".$aENV['table']['sys_user_usergroup']."` 
					WHERE `user_id` = '".$nUserId."'");
					
		while ($tmp = $oDb->fetch_array()) {
			$aUgId[] = $tmp['usergroup_id'];
		}
		
		if (count($aUgId) < 1) { return; }
		
		// 3. alle privilegien-IDs (permissions) des users ermitteln (die er durch seine zuordnung zu usergroups (u.U. doppelt) erbt)
		$sUgId	= implode(',', $aUgId);
		
		global $oDb, $aENV;
		
		$aUserPrivId	= array();
		$oDb->query("SELECT `id`, `permission` 
					FROM `".$aENV['table']['sys_usergroup']."` 
					WHERE `id` IN(".$sUgId.")");
					
		while ($tmp = $oDb->fetch_array()) {
			$aPerm			=  explode(',', $tmp['permission']);
			$aUserPrivId	= array_merge($aUserPrivId, $aPerm);
		}
		
		$aUserPrivId	= array_unique($aUserPrivId); // doppelte loeschen
		sort($aUserPrivId); // sortieren
		
		// 4. Durch alle Privilegien durchgehen...
		$buffer	= array();
		foreach ($aPriv as $pId => $pVal) {
		// ... und die des Users in einem assioziativen Array speichern...
			if (in_array($pId, $aUserPrivId)) {
				$buffer[$pVal['mod']][$pVal['key']] = 1;
			} // end if
		} // end foreach
		
		// ... und zurueckgeben
		return $buffer;
	} // end function getUserPrivilegesForLogin

// send password-email
	function _send_rescue_mail($email) {
		global $oDb, $aENV, $syslang;
		
		$oDb->query("SELECT * FROM `".$aENV['table']['sys_user']."` WHERE `email` = '".strip_tags(trim($email))."'");
		while ($tmp = $oDb->fetch_array()) {
		
			if ($syslang == "de") {
				$mailbody =  "Anmelde-Daten vergessen? Kein Problem!\n";
				$mailbody .= "-------------------------------------------------\n";
				$mailbody .= "Ihr Nutzername: ".$tmp['username']."\n";
				$mailbody .= "Ihr Passwort: ".$tmp['password']."\n";
				$mailbody .= "-------------------------------------------------\n";
				$mailbody .= "Bitte merken Sie Sich diese Anmelde-Daten \n";
				$mailbody .= "und löschen Sie diese E-Mail!\n";
				@mail($tmp['email'], "Anmelde-Daten", $mailbody, "From:design aspekt Inform<inform@design-aspekt.com>");
		
			} else {
		
				$mailbody =  "Forget your password...? No problem!\n";
				$mailbody .= "-------------------------------------------------\n";
				$mailbody .= "Your username: ".$tmp['username']."\n";
				$mailbody .= "Your password: ".$tmp['password']."\n";
				$mailbody .= "-------------------------------------------------\n";
				$mailbody .= "Please delete this email after reading!\n";
				@mail($tmp['email'], "Login-data", $mailbody, "From:design aspekt Inform<inform@design-aspekt.com>");
		
			} // END if
		
		} // END while
	
	} // END function

// Function to Generate Random Password
	function _generate_password($nPasswordLength=8) {
		$chars	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabchefghjkmnpqrstuvwxyz0123456789";
		$i		= 0;
		
		$sPassword	= '';
		
		while ($i <= $nPasswordLength) {
			$num	= rand() % 50;
			$sPassword	.= substr($chars, $num, 1);
			$i++;
		}
		
		return $sPassword;
	} // end function _generate_password

	function _checkIp() {	
		global $aENV, $Userdata, $oSess;
		
		// if this is not mobile and an ip adress is set for this user, check against Remote Adr and destroy if no compare
		if(!empty($Userdata['allowed_ips'])
		&& !eregi("mobile", $aENV['PHP_SELF'])) {
		
			// get ips	
			$aAllowedIps	= explode(",", $Userdata["allowed_ips"]);

			if(!in_array(getenv("REMOTE_ADDR"), $aAllowedIps)) {
				$oSess->destroy();
				header("Location: ".$aENV['PHP_SELF']); exit;
			} // end if
		} // end if	
	}
?>