<?php
/*
 * Special Include fuer INFORM
 *
 * Dieses Include muss in der /dbgui/config.inc.php als allererstes inkludiert werden.
 * Damit ist der phpMyAdmin nicht mehr aufzurufen, ohne dass man im Inform-System eingeloggt ist.
 */
	
	// require the setupfile and get $aENV
	require_once("../sys/config/setup.php");
	
	// minimal requirements:
	if (!is_array($aENV['db']))				die("ERROR: Kein aENV['db']!");
	if (empty($aENV['db']['host']))			die("ERROR: Kein aENV['db']['host']!");
	if (empty($aENV['db']['db']))			die("ERROR: Kein aENV['db']['db']!");
	if (empty($aENV['db']['user']))			die("ERROR: Kein aENV['db']['user']!");
	if (empty($aENV['client_shortname']))	die("ERROR: Kein aENV['client_shortname']!");
	
	// absolut path of this directory
	$_SELF_PATH	= "http://".$_SERVER['SERVER_NAME'];
	$_SELF_PATH	.= str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);
	
	// create the DB-object
	require_once ("../sys/php/global/service/class.db_mysql.php");
	$oDb	=& new dbconnect($aENV['db']);
	
	// login!
	require_once("../sys/php/global/service/class.session.php");
	$oSess		=& new session($aENV['client_shortname']."_inform");  // params: $sName
	$Userdata	= $oSess->get_var('Userdata'); // params: $sName[,$default=null]
	if (isset($Userdata)) {
		$sql	= "SELECT `username`,`password`,`flag_deleted` FROM `sys_user` WHERE `username`='".strip_tags(trim($Userdata['username']))."' AND `password`='".strip_tags(trim($Userdata['password']))."'";
		$oDb->query($sql);
		if ($Userdata['flag_deleted'] == "1" || $oDb->num_rows() != 1) { 
			$oSess->destroy(); // if no match -> logout!
			header("Location: ".str_replace("dbgui", "sys", $_SELF_PATH)."index.php?login=wrong"); exit;
		}
	} else {
		echo "Bitte loggen Sie sich zunaechst in das System ein. Laden Sie dann diese Seite neu.<br /><br />";
		echo "Please login first and reload this page.<br /><br />";
		exit;
	}
?>