<?php
/**
* browser-detection-page for system!
*
* checkt os/browser/version und filtert die fuer das System kritischen aus (bzw. schickt sie zur disallow-page).
* -> voll kopierbar!
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.1 / 2004-04-26
* #history	1.0
*/
// init
	require_once ("./php/_include_all.php");

	// TEST
	#echo 'Browserkennung: '.$oBrowser->getUserAgent()."<br>\n"; 
	#echo 'Name: '.$oBrowser->getName()."<br>\n"; 
	#echo 'Version: '.$oBrowser->getVersion()."<br>\n"; 
	
// browser-detection + login ok => weiterleitung
	header("Location: ".$aENV['page']['sys_welcome']); exit;


	// -> HTTP 1.1 verlangt eigentlich eine ABSOLUTE Adresse!
	#else { header("Location: http://".$_SERVER['HTTP_HOST'].$aENV['path']['cms']['http'].$aENV['page']['welcome']); exit; }
	// was aber trotzdem meist geht ist:
	#else { header("Location: portal.php"); exit; }
	// was manchmal NICHT geht ist:
	#else { header("Location: /cms/portal.php"); exit; }
	
	#@see: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.30
?>