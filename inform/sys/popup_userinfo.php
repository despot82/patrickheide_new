<?php
/**
* popup_userinfo.php
*
* Popup, um Details eines (SYS-/CUG-) Users anzuzeigen. 
* -> 2sprachig und kopierbar.
*
* Diese Seite wird aus der JS-function userinfoWin(userid,type) aufgerufen)
*
* @param	string	$id			welcher Datensatz
* @param	string	$type		[ sysuser | cuguser ] 
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2005-07-15	[um $type/cuguser erweitert]
* #history	1.0 / 2004-05-26
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.popup_userinfo.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. USER	// Diese Seite sollen ALLE sehen duerfen
// 1c. init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once ($aENV['path']['sys']['unix'].'inc.sys_login.php');

// 2a. GET-params abholen
	$id		= (isset($_GET['id'])) ? $_GET['id'] : '';
	$type	= (isset($_GET['type'])) ? $_GET['type'] : 'sysuser';
// 2b. POST-params abholen
// 2c. Vars

// 3. HTML
	require_once ($aENV['path']['sys']['unix'].'inc.sys_header.php');

// MEDIA-DB init
	require_once ($aENV['path']['global_service']['unix'].'class.mediadb.php');
	$oMediadb =& new mediadb(); // params: - 

// USER init
	if ($type == 'cuguser') {
		// CUG(-USER)
		require_once($aENV['path']['global_service']['unix'].'class.cug.php');
		$oCug =& new cug($aENV['db'], $syslang); // params: $aDbVars[,$sLang='de'][,$bPasswordRequired=false]
		$aUserdata = $oCug->getUser($id); // params: $userid
		if (!empty($aUserdata['photo'])) {
			$aUserdata['picture'] = $oMediadb->getImageTag($aUserdata['photo'], ' border="0"'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)]
		}
	} else {
		// SYS(-USER)
		$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
		// get userdata
		$aUserdata = $oUser->getUserdata($id); // params: $userid
		if (!empty($aUserdata['picture'])) {
			$aUserdata['picture'] = $oMediadb->getImageTag($aUserdata['picture'], ' border="0"'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)]
		}
	}
	if (empty($aUserdata['photo']) && empty($aUserdata['picture'])) {
		$aUserdata['picture'] = $oMediadb->getImageTag($aENV['path']['adr_data']['http'].'pix/dummy_'.$aUserdata['gender'].'.gif', ' border="0"'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)]
	}
// HILFSFUNKTION
	function _getOfficeName(&$oUser, $office_id) {
		global $syslang, $aMSG;
		$buffer = '';
		// check
		if ($oUser->countAllOffices() < 2) return $buffer;
		// weiter nur wenn mehr als 1 office
		$buffer .= $aMSG['form']['office'][$syslang].': ';
		$buffer .= $oUser->getOfficeName($office_id); // params: $officeid
		$buffer .= '';
		// output
		return $buffer;
	}
	function _getCUGs(&$oCug, $usergroup_id) {
		$buffer = '';
		// check
		if (!is_array($usergroup_id)) return $buffer;
		// weiter nur wenn mindestens 1 cug
		$buffer .= '<b>CUG:</b><br>';
		foreach ($usergroup_id as $ug_id => $ug_title) {
			$buffer .= $ug_title.'<br>';
		}
		// output
		return $buffer;
	}
	/*function _getCompanyName(&$oCug, &$aUserdata) {
		$buffer = '';
		if (!is_array($usergroup_id)) return $buffer;
		$buffer .= '<b>CUG:</b><br>';
		foreach ($usergroup_id as $ug_id => $ug_title) {
			$buffer .= $ug_title.'<br>';
		}
		return $buffer;
	}*/

?>

<div id="contentPopup">
<form>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<tr valign="top">
	<td width="90%">
		<p><span class="title"><?php echo $aUserdata['title'].' '.$aUserdata['firstname'].' '.$aUserdata['surname']; ?></span><br>
		<br>
		<a href="mailto:<?php echo $aUserdata['email']; ?>"><?php echo $aUserdata['email']; ?></a><br>
		<?php echo $aUserdata['phone']; ?><br>
		<?php // details
		if ($type == 'cuguser') {
			if ($aUserdata['company_name']) echo $aUserdata['company_name'].'<br>';
			echo _getCUGs($oCug, $aUserdata['usergroup_id']);
		} else {
			echo _getOfficeName($oUser, $aUserdata['office_id']);
		} ?>
		<br></p>
	</td>
	<td width="10%" align="right">
		<?php echo $aUserdata['picture']; ?>
	</td>
</tr>
<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
<?php if (isset($aUserdata['description']) && $aUserdata['description'] != "") { ?>
<tr>
	<td colspan="2">
		<p>
			<?php echo $aUserdata['description'] ?>
		</p>
	</td>
</tr>
<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
<?php } ?>

</table>
<?php echo get_button('CLOSE_WIN', $syslang, 'smallbut'); // params: $sType,$sLang[,$sClass="but"] ?>
</form>
<br>
</div>

</body>
</html>