// SYS functions
// lastmod: 2006-07-20oh

/**************************************************
bildet die php-replace funktion in javascript ab;
ersetzt jedes Vorkommen von SRCH durch SUBST innerhalb 
des Strings STR [cr]
**************************************************/
function str_replace(STR, SRCH, SUBST) {
	Split = STR.split(SRCH); Out = Split[0];
	for (c=1; c<Split.length; c++) {
		Out += SUBST + Split[c];
	}
	return Out;
}

/**************************************************
Funktionen fuer Layer-Wechsel [oh]
**************************************************/
function getLayer(layerID) {	
	return (document.getElementById?document.getElementById(layerID).style:document.all?document.all[layerID].style:document.layers[layerID]);
}

function layerAn() {
	for(i=0;i<arguments.length;i++) {
		getLayer(arguments[i]).visibility="visible";
	}
}
 	
function layerAus() {
	for(i=0;i<arguments.length;i++) {
		getLayer(arguments[i]).visibility="hidden";
	}
}

function sublayerSwitch() {
	for(i=0;i<arguments.length;i++) {
		layer = getLayer(arguments[i]);
		layer.display = (layer.display == "none") ? "block" : "none";
	}
}

// Version von "sublayerSwitch()" fuer Navi-Baeume, die auch gleich den Bldwechsel vornimmt (CMS/PMS)
function subnaviSwitch() {
	for(i=0;i<arguments.length;i++) {
		// es wird vorausgesetzt, dass der layer "nLayer_" + ID heisst
		if (document.getElementById("nLayer_" + arguments[i])) {
			layer = getLayer("nLayer_" + arguments[i]);
			layer.display = (layer.display == "none") ? "block" : "none";
		}
		// es wird vorausgesetzt, dass der Button "nImg_" + ID heisst
		if (document.images["nImg_" + arguments[i]]) {
			toggleNaviImage("nImg_" + arguments[i],'content');
		}
	}
}
// toggle-image function
function toggleNaviImage(bildName,layerID) {
	if (getImage(bildName,layerID).src == naviIcons["show"].src) {
		getImage(bildName,layerID).src = naviIcons["hide"].src;
	} else {
		getImage(bildName,layerID).src = naviIcons["show"].src;
	}
}

/**************************************************
Funktionen fuer Icon-tausch (internal Linkmanager) [oh,af]
**************************************************/
// vorlader
var icons = new Array();
icons["hide"] = new Image();
icons["hide"].src = "pix/hide.gif";
icons["show"] = new Image();
icons["show"].src = "pix/show.gif";

// cross-browser image-element
function getImage(bildName,layerID) {
	return (document.all||document.getElementById?window.document.images[bildName]:layerID?document.layers[layerID].document.images[bildName]:window.document.images[bildName]);
}
// toggle-image function
function toggleImage(bildName,layerID) {
	if (getImage(bildName,layerID).src == icons["show"].src) {
		getImage(bildName,layerID).src = icons["hide"].src;
	} else {
		getImage(bildName,layerID).src = icons["show"].src;
	}
}


// zahlenfelder bearbeiten (komma durch punkt ersetzen...) //
function conv(str) {
	if(isNaN(str)) {
		stext = str.split(',');
		if (stext.length > 1) {
			snum = stext[0].split('.');
			if (snum.length > 1 && snum[1].length == 3) {
				snum = eval((snum[0] + snum[1]) - 0);
				snum = eval((snum + "." + stext[1]) - 0)
			} else {
				snum = eval((stext[0] + "." + stext[1]) - 0)
			}
			return snum;
		} else {
			alert("Bitte geben Sie hier nur Zahlen ein!");
			return '';
		}
	} else {
		snum = str.split('.');
		if (snum.length > 1 && snum[1].length == 3) {
			snum = eval((snum[0] + snum[1]) - 0);
		} else {
			snum = str - 0;
		}
		return (snum == 0) ? '' : snum;
	}
}

/**************************************************
Funktionen fuer Popup-Windows 
(benoetigt die JS-Variablen "modulPath" + "mediaDbPath" aus der "inc.sys_header.php"!)
**************************************************/

// opens new window for 'copyrights'
function copyrightWin() {
	var uri = sysPath+'popup_copyright.php'
	cWin = window.open(uri,'copyright','toolbar=0,status=0,scrollbars=1,width=500,height=400,resizable=1,top=30,left=30');
	cWin.focus();
}
// opens new window for 'help'
function helpWin(referrer) {
	var uri = sysPath+'popup_help.php?referrer='+referrer
	hWin = window.open(uri,'help','toolbar=0,status=0,scrollbars=1,width=500,height=400,resizable=1,top=30,left=30');
	hWin.focus();
}
// opens new window for show 'SYS-User-info'
function userinfoWin(userid, type) {
	if (type == undefined) { type = 'sysuser'; }
	var left = (screen.width - 382);
	var uri = sysPath+'popup_userinfo.php?id='+userid+'&type='+type;
	uWin = window.open(uri,'userinfo','toolbar=0,status=0,scrollbars=1,width=400,height=300,resizable=1,top=30,left='+left);
	uWin.focus();
}

/* MediaDB */
// opens new window for 'slideshow erstellen' from "MediaDB"
var slideshowWin = null;
function selectNewSlideshowWin(fkid,sid,col_name,formname,w,h,mustfit) {
	var left = (screen.width - 920);
	var uri = mediaDbPath+'popup_slideshow_db.php?fkid='+fkid+'&sid='+sid+'&col_name='+col_name+'&formname='+formname+'&width='+w+'&height='+h+'&mustfit='+mustfit;
	slideshowWin = window.open(uri,'slideshow','toolbar=0,status=0,scrollbars=1,width=400,height=500,resizable=1,top=30,left='+left);
	slideshowWin.focus();
}
// opens new window for 'slideshow erstellen' from "MediaDB"
function selectSlideshowWin(id,col_name,tbl_name,formname,w,h,mustfit) {
	var left = (screen.width - 968);
	var uri = mediaDbPath+'popup_slideshow.php?id='+id+'&col_name='+col_name+'&tbl_name='+tbl_name+'&formname='+formname+'&width='+w+'&height='+h+'&mustfit='+mustfit;
	slideshowWin = window.open(uri,'slideshow','toolbar=0,status=0,scrollbars=1,width=440,height=500,resizable=1,top=30,left='+left);
	slideshowWin.focus();
}
// opens new window for SELECT from "MediaDB"
var mediaWin = null;
function selectMediaWin(mediatype,formfield,formname,w,h,mustfit,opener) {
	var left = (screen.width - 520);
	if (mustfit == undefined) { mustfit = 0; }
	if (opener == undefined) { opener = 'mainWin'; }
	var uri = mediaDbPath+'popup_selectmedia.php?mediatype='+mediatype+'&formfield='+formfield+'&formname='+formname+'&width='+w+'&height='+h+'&mustfit='+mustfit+'&opener='+opener;
	mediaWin = window.open(uri,'mesdiadb','toolbar=0,status=0,scrollbars=1,width=500,height=500,resizable=1,top=30,left='+left);
	if (mediaWin != undefined) { mediaWin.focus(); }
}
// opens new window for VIEW from "MediaDB"
function viewMediaWin(id,w,h) {
	var width = w + 40;
	var height = h + 35;
	var uri = mediaDbPath+'popup_viewmedia.php?id='+id+'&w='+w+'&h='+h;
	win = window.open(uri,'popup','toolbar=0,status=0,scrollbars=1,width='+width+',height='+height+',resizable=1,top=30,left=10');
	win.focus();
}
// opens new window for SELECT FTP-path
var FTPWin = null;
function selectFtpWin(formfield,formname) {
	var uri = sysPath+'popup_selectpath.php?formfield='+formfield+'&formname='+formname;
	FTPWin = window.open(uri,'mediadb','toolbar=0,status=0,scrollbars=1,width=500,height=500,resizable=1,top=30,left=10');
	if (FTPWin != undefined) {FTPWin.focus();}
}

/* CAL */
// opens Calendar window
function calendarWin(formfield, formname) {
	var left = (screen.width - 320);
	// get current date
	var obj = document.forms[formname].elements[formfield];
	var aDatePart = obj.value.split('.');
	var calMonth = (aDatePart.length == 3 && aDatePart[1] > 0 && aDatePart[1] < 13) ? (aDatePart[1]-0) : '';
	var calYear = (aDatePart.length == 3 && aDatePart[2].length == 4) ? aDatePart[2] : '';
	
	var uri = calPath+'popup_calendar.php?formfield='+formfield+'&formname='+formname+'&calMonth='+calMonth+'&calYear='+calYear;
	win = window.open(uri,'calendar','toolbar=0,status=0,scrollbars=0,width=300,height=200,resizable=1,top=200,left='+left);
	win.focus();
}

/**************************************************
Funktionen fuer Flasheditor
**************************************************/
/****** verhindert, dass Flash-Editor den Focus behaelt (Firefox etc.) ******/
function setNewFocus(formName, fieldName) {
	window.focus();
	if (formName == "") { formName = "editForm"; }
	if (fieldName && fieldName != "") {
		field = document.forms[formName].elements[fieldName];
		field.focus();
	}
}
/************************************/

/****** Flash to JavaScript *********/
function set(flashTxt,hiddenField) {	// Aufruf durch getURL in Flash -> IE auf Windows verwendet stattdessen FSCommand
	with(document.forms["editForm"]) {
		field =	elements[hiddenField];
		field.value = flashTxt;
	}
}

function texteditor_DoFSCommand(command,args) {	// funktioniert zwar bei Mozilla, aber Umlaut-Probleme; nicht bei Opera und Mac
	//  erfordert zus???tzliche VBScript-Block im Header f???r IE auf Win => class.form_admin.php
	with(document.forms["editForm"]) {
		field =	elements[command];
		field.value = args;
	}
}
/************************************/

/*
Grundproblem:
Opera und IE auf Win k???nnen nur begrenzt Text ???ber GET beim SWF-Aufruf entgegen nehmen.
Der IE auf Win hat dar???ber hinaus das Problem, dass er nur minimal Daten ???ber getURL() wieder ausgeben kann.

Als Konsequenz ergibt sich folgende Situation:
Die Browser erhalten den Text ???ber den Aufruf einer XML-Datei. Die Probleme werden so umhgangen.

F???r die Ausgabe verwendet allein IE auf Win die Funktion fscommand(), die Opera wiederum nicht versteht.
Opera verwendet wie Mozilla und Mac f???r die Ausgabe getURL(), hat aber ein Umlaut-Problem in der Ausgabe.
Hierf???r ist in Flash eine gesonderte Weiche enthalten, da Opera kein zus???tzliches "escape()" des Texts vertr???gt. 
Innerhalb von Flash ist Weiche f???r IE (fscommand()) und andere Browser (getURL()) bereits vorgenommen.
*/

// Flash-Embed f??r IE
function embedFlash(oTag) { document.write(oTag); }
