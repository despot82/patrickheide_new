<?php
/**
* disallow-page for system ONLY [en|de] !!!
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_disallow.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// init
	require_once ("php/_include_all.php");

// vars ********************************************************************************
	$redirectOK			= $aENV['page']['sys_welcome'];	// go to startpage of this system
	$redirectOnceMore	= $aENV['path']['sys']['http']."index.php";	// back to browsercheck
	$syslang			= $aENV['system_language']['default'];
	$nameOfClient		= $aENV['client_name'];
	$nameOfSystem		= $aMSG['std']['system_name'][$syslang];
// END vars ****************************************************************************

	// download-pages
	$winIE_downloadpage['de']	= "http://www.microsoft.com/windows/ie_intl/de/download/";
	$winIE_downloadpage['en']	= "http://www.microsoft.com/windows/ie/default.asp";
	$macIE_downloadpage['de']	= "http://www.microsoft.com/mac/download/ie/ie51.asp?navindex=s7c";
	$macIE_downloadpage['en']	= "http://www.microsoft.com/mac/download/ie/ie51.asp?navindex=s7c";
	$NN_downloadpage['de']		= "http://www.netscape.de/netscapeprodukte/netscape71/download.html";
	$NN_downloadpage['en']		= "http://wp.netscape.com/computing/download/index.html?cp=hop05ft6";
	$Moz_downloadpage['de']		= "http://www.mozilla-europe.org/de/";
	$Moz_downloadpage['en']		= "http://www.mozilla.com/";
	
	// possible messages
	$aMsgs = array();
	$aMsgs[] = "no_js";			// no JavaScript
	$aMsgs[] = "oldbrowser";	// Old Browser
	$aMsgs[] = "userrights";	// incorrect userrights
	$aMsgs[] = "no_ur";			// no valid LogIn
	
	// translate get/post-vars (if in $aMsgs!)
	$msg = '';
	if (!empty($_GET['msg']))	{$msg = $_GET['msg'];}
	if (!empty($_POST['msg']))	{$msg = $_POST['msg'];}
	if (in_array($msg,$aMsgs)) {$msg = $msg;} else {$msg = '';}
	
	// browser-detection
	$browser = strtolower(getenv("HTTP_USER_AGENT"));

// HTML:
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
	
/* DEBUG:
	echo "getUserAgent: ".$oBrowser->getUserAgent()."<br>";
	echo "getName: ".$oBrowser->getName()."<br>";
	echo "getVersion: ".$oBrowser->getVersion()."<br>";
*/
?>


<table cellspacing="10" cellpadding="10" border="0" width="600"><tr><td>
<br>
<div class="title"><?php // headline
	$headline['en'] = $nameOfClient.' '.$nameOfSystem;
	$headline['de'] = $nameOfClient.' '.$nameOfSystem;
	if ($msg != "userrights") {
		$headline['en'] = 'Welcome to '.$headline['en'];
		$headline['de'] = 'Willkommen im '.$headline['de'];
	}
	echo $headline[$syslang]; ?></div>
<br>
<div class="text">


<?php // engl. version
if ($syslang == "en") { ?>
	
<?php if ($msg == "no_js") { // no JavaScript ?>

	To use <?php echo $nameOfSystem; ?>, JavaScript is required. <br>
	You have disabled JavaScript in your browser or use a browser that does not support JavaScript.<br>
	<br><br>
	[ <a href="<?php echo $redirectOnceMore; ?>">back</a> ]

<?php } elseif ($msg == "oldbrowser") { // Old Browser ?>

These pages are optimized for current browsers.  
These are i.e. on Windows: Mozilla Firefox, Microsoft Internet Explorer (Version 6+), Netscape (Version 7+) and on MAC OSX: Safari (Version 1.3+) or Firefox, on MAC OS9: Microsoft Internet Explorer (Version 5.2).<br>
The browser you currently use does not support features of this <?php echo $aMSG['std']['system_name'][$syslang]; ?>.
<br><br>
We strongly recommend to download the newest stable version of one of the following browsers:
<ul>
	<li><a href="<?php echo $Moz_downloadpage['en']; ?>" target="_blank">Mozilla Firefox</a></li>
	<li><a href="<?php echo $NN_downloadpage['en']; ?>" target="_blank">Netscape Communicator</a></li>
	<li><a href="<?php echo $winIE_downloadpage[$syslang]; ?>" target="_blank">Microsoft Internet Explorer</a></li>
</ul><br>
For help, please contact your system-/ network- admistrator.
<br><br>
[ <a href="<?php echo $redirectOnceMore; ?>">back</a> ]


<?php } elseif ($msg == "userrights") { // incorrect userrights ?>

Sorry, but you are not allowed to view this page.<br>
<br><br>
[ <a href="javascript:history.back()">back</a> ]


<?php } else { // ("no_ur") no valid LogIn ?>

Sorry, you are not allowed to enter <?php echo $nameOfSystem; ?>!
<br><br>
Please check your username / password and <br>
[ <a href="<?php echo $redirectOnceMore; ?>">try again</a> ]


<?php
	}
}

// deutsche version

if ($syslang == "de") { ?>

<?php if ($msg == "no_js") { // no JavaScript ?>

Sie haben in Ihrem Browser JavaScript deaktiviert oder verwenden einen Browser, der kein JavaScript beherrscht. Zum fehlerfreien Betrieb der Seiten ist jedoch JavaScript Voraussetzung!
<br><br>
<b>JavaScript aktivieren:</b><br>
<ul>
	<?php if (strstr($browser, 'mozilla') && !strstr($browser, 'msie')) { // Wenn Netscape ?>
	<li><b>Netscape Navigator</b><br>
	W&auml;hlen Sie bitte unter &quot;Bearbeiten&quot; den Men&uuml;punkt &quot;Einstellungen&quot; aus, klicken Sie im Men&uuml;baum auf &quot;Erweitert&quot;, stellen Sie sicher, dass die Funktion &quot;JavaScript&quot; aktiviert ist. Klicken Sie dann <a href="<?php echo $redirectOnceMore; ?>">hier</a>.</li>
	<?php } else { ?>
	<li><b>Microsoft Internet Explorer</b><br>
	W&auml;hlen Sie bitte im Men&uuml; &quot;Extras/Internetoptionen/Ansicht&quot; die Karteikarte &quot;Sicherheit&quot;, klicken Sie auf den Button &quot;Angepasst&quot;, stellen Sie sicher, dass die Funktion &quot;Scripting/Active Scripting&quot; aktiviert ist. Klicken Sie dann <a href="<?php echo $redirectOnceMore; ?>">hier</a>.</li>
	<?php } ?>
</ul><br>
<b>Aktuellen Browser installieren:</b><br><br>
Falls Ihr Browser kein JavaScript beherrscht, laden Sie bitte eine aktuelle Version z.B. des Mozilla Firefox, Netscape oder des Microsoft Internet Explorers herunter:
<br><br>
[ <a href="<?php echo $redirectOnceMore; ?>">zur&uuml;ck</a> ]


<?php } elseif ($msg == "oldbrowser") { // Alter Browser ?>

Die Seiten sind für Browser der aktuellen Generation optimiert.
wie z.B. auf dem PC: Mozilla Firefox, Microsoft Internet Explorer (ab Version 6), Netscape (ab Version 7) und auf dem MAC: (OSX) Safari (ab Version 1.3) oder Firefox, bzw. auf OS9 den Microsoft Internet Explorer (Version 5.2).<br>
Sie verwenden einen Browser, der wichtige Features noch nicht unterstützt.<br>
Sie können daher das <?php echo $nameOfSystem; ?> damit nicht fehlerfrei bedienen. 
Wir empfehlen Ihnen dringend, eine aktuelle Browser-Version z.B. eines der folgenden Browser zu installieren:
<br><ul>
	<li><a href="<?php echo $Moz_downloadpage['de']; ?>" target="_blank">Mozilla Firefox</a></li>
	<li><a href="<?php echo $NN_downloadpage['de']; ?>" target="_blank">Netscape Browser herunterladen</a></li>
	<li><a href="<?php echo $winIE_downloadpage['de']; ?>" target="_blank">Microsoft Internet Explorer herunterladen</a></li>
</ul><br>
Bei Fragen wenden Sie sich bitte an Ihren System-Administrator.<br>
<br>
[ <a href="<?php echo $redirectOnceMore; ?>">zur&uuml;ck</a> ]


<?php } elseif ($msg == "userrights") { // incorrect userrights ?>

Sie haben auf Grund Ihrer Benutzerrechte keinen Zugriff auf diese Seite.<br>
<br><br>
[ <a href="javascript:history.back()">back</a> ]


<?php } else { // ("no_ur") kein gueltiges LogIn ?>

Sie haben entweder einen falschen Usernamen oder Passwort eigegeben oder Sie haben keine Zutrittsrechte für das <?php echo $nameOfClient; ?> <?php echo $nameOfSystem; ?>!
<br><br>
[ <a href="<?php echo $redirectOnceMore; ?>">Neuer Versuch</a> ]

<?php
	}
} // END $syslang ?>

</td></tr></table><br>


<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>