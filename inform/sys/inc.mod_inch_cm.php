<?php 
// is there a custom version of this page for the customer
	$custompage = './inc.mod_inch_cm.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}
?>
<script language="JavaScript" type="text/javascript">
var aDezimal = new Array(0.0,0.0625,0.125,0.1875,0.25,0.3125,0.375,0.4375,0.5,0.5625,0.625,0.6875,0.75,0.8125,0.875,0.9375);

	function inch2cm() {
		frm2 = document.forms["laengenmasse"];
		selIndex = frm2.fraction.options[frm2.fraction.selectedIndex].value;
		inputVal = frm2.inputInch.value;
		
		if (inputVal == "") {	// wenn Eingabefeld leer ist, wird Wert 0 angenommen
			inputVal = "0";
			frm2.inputInch.value = "0";
		}
		
		if (isNaN(inputVal) || inputVal.indexOf(".") != -1 || inputVal.indexOf(",") != -1) {	// Kommazahlen sind nicht moeglich, da die Eingabe eine Bruchzahl repraesentieren soll
			alert('<?php echo $aMSG['std']['integer'][$syslang]; ?>');
			frm2.inputInch.value = parseInt(inputVal);
		}	
		else {														// Nachkommastellen in Array splitten
			fraction = selIndex.split("."); 
			newInch = parseInt(inputVal) + "." + fraction[1] + 0;	// Zusammenfuegen Inch als Dezimalzahl; durch Addition von 0 Wert von String in Number wandeln
		}	
		
		output = newInch * 2.54;	
		frm2.inputCm.value = output;
	}

	function cm2inch() {
		minVal = 0.0625;
		maxVal = 1;
		frm2 = document.forms["laengenmasse"];
		inputVal = frm2.inputCm.value;
		
		if (isNaN(inputVal) || inputVal == "") {
			alert('<?php echo $aMSG['std']['integer'][$syslang]; ?>');
			frm2.inputCm.value = "";
			frm2.inputCm.focus();
			return;
		}
		
		tmpInch = Math.round((parseFloat(inputVal) / 2.54) * 10000) / 10000;	// Umwandlung in Zoll, gerundet auf 4 Stellen
		tmpInch = tmpInch + "";													// tmpInch fuer weiteres Vorgehen in String verwandeln
		inchFraction = tmpInch.split(".");										// Nachkommastellen in Array splitten
		
		validFraction = false;
		for (i=0; i<aDezimal.length; i++) {										// Pruefen, ob Nachkommastelle einer bestehenden Fraction entspricht
			if (aDezimal[i] == ("0." + inchFraction[1])) { validFraction = true; }
		}
		
		if (validFraction == true) {											// Idealfall: wenn Nachkommastelle gueltiger Fraction entspricht...
			output = inchFraction[0] + "";
			selValue = "0." + inchFraction[1];
		}
		else {																	// else duerfte die Regel sein
			if (inchFraction[1] == undefined) {									// Sonderfall: es handelt sich um eine Ganzzahl (keine Nachkommastelle)
				output = parseInt(tmpInch) + " ";								// nicht Fraction kommt hier zum Tragen, sondern der errechnete Tmp-Wert
				selValue = "0.0";
			}
			else {
				for (i=0; i<aDezimal.length; i++) {								// naechstkleinere und naechstgroessere Dezimalstellen aus Array ermitteln
					if (parseFloat("0." + inchFraction[1]) > aDezimal[i]) { minVal = aDezimal[i]; }
					if (parseFloat("0." + inchFraction[1]) < aDezimal[i]) {
						maxVal = aDezimal[i];
						i = aDezimal.length + 1;	// warum?? :-/
					}
				}
				
				if ((parseFloat("0." + inchFraction[1]) - minVal) > (maxVal - parseFloat("0." + inchFraction[1]))) {	// es ist der naechstgroessere...
					maxVal = maxVal + "";
					if (maxVal == "1") { maxVal = "1.0"; }
					newFraction = maxVal.split(".");
				}
				else {															// es ist der naechstkleinere...
					minVal = minVal + "";
					newFraction = minVal.split(".");
				}	
				inchFraction[1] = newFraction[1];
							
				if (inchFraction[1] == "0") {									// Nachkommastelle = 0, also maxVal = 1 => nchstgrere Ganzzahl ausgeben
					output = (parseFloat(inchFraction[0]) + 1) + "";
					selValue = "0.0";
				}
				else {															// wenn Nachkommastelle in Array vorhanden, Bruchzahl ausgeben
					output = inchFraction[0] + "";
					selValue = "0." + inchFraction[1] + "";
				}
			}
		}
		
		for (i=0; i<aDezimal.length; i++) {										// neuer selectedIndex des Select-Feldes ueber value ermitteln
			if (selValue == aDezimal[i]) { selIndex = i; }
		}
		frm2.inputInch.value = output;
		frm2.fraction.selectedIndex = selIndex;
	}

	function dummy() {	// Anker auf Schalter laesst bei gescrollter Seite zum Seitenanfang springen; blur wiederum zwingt manche browser in den Hintergrund => Dummy-Funktion
		return;
	}
</script>

<form name="laengenmasse">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabelle">
<tr><td class="th" colspan="3"><b><?php echo $aMSG['portal']['cminchcalculator'][$syslang]; ?></b></td></tr>
<tr>
	<td width="20%" class="sub3"><span class="text">CM</span></td>
	<td class="sub3"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td>
	<td width="80%" class="sub3"><span class="text">Inch</span></td>
</tr>
<tr>
	<td class="sub2"><input type="text" name="inputCm" size="8" style="width:70px" value="" onBlur="cm2inch()"></td>
	<td class="sub2"><a href="javascript:dummy()" onMouseOver="window.status=''; return true;" title="<?php echo $aMSG['form']['calculate'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_equal.gif" alt="<?php echo $aMSG['form']['calculate'][$syslang]; ?>" class="btn"></a></td>
	<td class="sub2"><input type="text" name="inputInch" size="8" style="width:70px" value="" onBlur="inch2cm()">
	<select name="fraction" size="1" onChange="inch2cm()">
		<option value="0.0">&nbsp;</option>
		<option value="0.0625">1/16</option>
		<option value="0.125">1/8</option>
		<option value="0.1875">3/16</option>
		<option value="0.25">1/4</option>
		<option value="0.3125">5/16</option>
		<option value="0.375">6/16</option>
		<option value="0.4375">7/16</option>
		<option value="0.5">1/2</option>
		<option value="0.5625">9/16</option>
		<option value="0.625">10/16</option>
		<option value="0.6875">11/16</option>
		<option value="0.75">3/4</option>
		<option value="0.8125">13/16</option>
		<option value="0.875">14/16</option>
		<option value="0.9375">15/16</option>
	</select>
	</td>
</tr>
</table>
</form>		