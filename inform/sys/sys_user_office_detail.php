<?php
/**
* sys_user_office_detail.php
*
* Detailpage: office
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz (optional)
* @param	array	Formular:	$aUser (Checkboxen, die zu kommasep. string zusammengefuehrt werden muessen)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-01-24
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_user_office_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// diese seite ist nur was fuer admins ;-)
	if ($oPerm->hasPriv('admin') == false) { echo js_history_back(); }

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars:
	$sTable			= $aENV['table']['sys_office'];
	$sViewerPage	= $aENV['SELF_PATH']."sys_user_office.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// 3. DB
// DB-action: delete
	if (isset($btn['delete']) && $aData['id'] && !$alert) {
		//make delete
		$oDb->make_delete($aData['id'], $sTable);
		// weiterleitung overviewpage
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) || isset($btn['save_close'])) {
		// erst checken ob title = unique
		$sql	= "SELECT `id` FROM `".$sTable."` 
					WHERE `name` = '".$aData['name']."' 
						AND `id` != '".$aData['id']."'";
		$oDb->query($sql);
		$entries = $oDb->num_rows();
		if ($entries > 0) { echo js_alert($aMSG['err']['title_notunique'][$syslang]); $alert=true; }
	}
	if ((isset($btn['save']) || isset($btn['save_close'])) && !isset($alert)) {
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$oDb->make_update($aData, $sTable, $aData['id']);
		} else { // Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id();
		}
		// back to overviewpage
		if (isset($btn['save_close'])) {
			header("Location: ".$sViewerPage); exit;
		}
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("name", $aMSG['form']['name'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	$oForm->check_field("client_nr_startvalue", $aMSG['form']['client_nr_values'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['offices'][$syslang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td align="right"><?php // Buttons 
			echo $oForm->button("BACK"); // params: $sType[,$sClass='']
			echo $oForm->button("NEW"); // params: $sType[,$sClass='']
		?></td>
	</tr>
</table>

<?php echo HR; ?>

<?php include_once("inc.sys_detnavi.php"); 
	echo getSysUserSubnavi(); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="25%"><col width="75%"></colgroup>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['name'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->textfield("name",250,78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<?php 
	if (isset($aENV['module']['adm'])) { 
	?>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['client_nr_values'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->textfield("client_nr_startvalue", 5, 5, '000'); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
			<span class="text"> - </span>
			<?php echo $oForm->textfield("client_nr_endvalue", 5, 5, '999'); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<?php 
	}
	
	// if adr modul exists
	if(isset($aENV['module']['adr'])) {   
	?>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['sender'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textarea("adr_sender", 3, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
<?php
	} // end if adr modul exists 
	
	if ($oPerm->isDaService()
	&& isset($aENV['module']['adm'])) {
		
?>	
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['pdftemplate']['invoice'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("pdf_template_invoice"); ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['pdftemplate']['proposal'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("pdf_template_proposal"); ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['pdftemplate']['cancellation'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("pdf_template_cancellation"); ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['pdftemplate']['dunning'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("pdf_template_dunning"); ?></td>
	</tr>	
<?php } ?>	
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
