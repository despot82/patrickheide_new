<?php
/**
* inc.sys_memos_detail.php
*
*  Detailpage: zum anlegen/editieren einer Memo
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-07-14
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_memo_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("./php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$delete_id		= (isset($_GET['delete_id'])) ? $oFc->make_secure_int($_GET['delete_id']) : '';
	if (isset($_GET['remoteSave']) && $_GET['remoteSave'] == 1)	{ $remoteSave = 1; } else { $remoteSave = 0; }
	$sGEToptions	= '';
	
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['remoteSave']) && $_POST['remoteSave'] == 1)	{ $remoteSave = 1; }
	
// 2c. Vars
	$sTable			= $aENV['table']['sys_memo'];
	// diese seite hat keine eigene viewer seite!!! -> sys_portal.php
	$sViewerPage	= $aENV['SELF_PATH']."sys_portal.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// TREE ACCESS CONTROLL
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	if(!isset($aENV['php5']) || !$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php');
	}
	else {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php5');
	}
	$oTACEA	=& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();

// init MEMO
	$oMemoTools	=& new Memo($oDb);

// 3. DB
// DB-action: delete (remote)
	if (!empty($delete_id) && $delete_id > 0) {
		// zuerst nur sich aus der Liste der autorisierten rausloeschen
		$oMemoTools->doDelete($delete_id,$oTACEA,$aENV,$Userdata);
		header("Location: ".$sViewerPage); exit;
	}

// DB-action: delete
	if (isset($btn['delete']) && !empty($aData['id'])) {
		$oDb->make_delete($aData['id'], $sTable);
		$oMemoTools->clearMemo($aData['id']);
		$oTACEA->onDeleteAction($aData['id'],'memo');
		header("Location: ".$sViewerPage); exit;
	}

	unset($aData['flag_open']);

// DB-action: save or update
	if ((isset($btn['save']) || isset($btn['save_close']) || $remoteSave == 1) && !isset($alert)) {
		// view zwischenspeichern
		$viewug = $aData['viewug'];
		unset($aData['viewug']);
		
		$aData = $oMemoTools->save($aData,$aENV,$Userdata,$oTACEA);
		
		if (isset($btn['save_close'])) {
			// back to overviewpage
			header("Location: ".$sViewerPage); exit;
		}
		
	}

// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}
	// view wiederherstellen
	$aData['viewug'] = $viewug;

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm	=& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("memo", $aMSG['form']['shorttext'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}

?>
<script language="JavaScript" type="text/javascript">
function confirmRemoveMemoFromDesk() {
	var chk = window.confirm('<?php echo $aMSG['err']['delete_recfromdesk'][$syslang]; ?>');
	if (chk) {
		location.href = '<?php echo $aENV['PHP_SELF']; ?>?delete_id=<?php echo $aData['id']; ?>';
	} else {
		return(chk);
	}
}
</script>
<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="remoteSave" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['memos'][$syslang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td align="right"><?php echo get_button("NEW", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"] ?></td>
	</tr>
</table>
<br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['text'][$syslang]; ?></b></span></td>
		<td width="80%">
		<?php
			echo $oForm->textarea_flash('memo', $sTable, $aData['id'],'standalone'); 
		?>
		</td>
	</tr>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['prio'][$syslang]; ?></span></td>
		<td class="sub2">
		<?php echo $oForm->radio('prio',array('2' => $aMSG['form']['important'][$syslang], '0' => $aMSG['form']['normal'][$syslang], '1' => $aMSG['form']['useless'][$syslang])); // params: $sFieldname,$aValues ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['foruser'][$syslang]; ?></span></td>
		<td colspan="2">
<?php // build checkboxes
		$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
		$aUsername = $oUser->getAllUserNames(); // params: [$bValidOnly=true]
		$param = array('first' => '20','second' => '80','border'=>'0','cellspacing'=>'1','cellpadding'=>'2','class' =>"tabelle");
		echo $oTACEA->showTeamAccess($aData,$syslang,$aData['id'],$flagopen,$aMSG,$param,'memo',false,false); ?>
		</td>
	</tr>
</table><br>

<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo '<input type="button" value="'.$aMSG['btn']['deletefromdesk'][$syslang].'" name="btn[deletefromdesk]" id="deletefromdesk" class="but" onClick="return confirmRemoveMemoFromDesk();">'; } ?>
<?php if ($mode_key == "edit") { echo '<input type="button" value="'.$aMSG['btn']['cancel'][$syslang].'" name="btn[cancel]" id="cancel" class="but" onClick="location.href=\''.$sViewerPage.'\';">'; } ?><br>
<?php echo $oForm->end_tag(); ?>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
