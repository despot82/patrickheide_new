<?php
// is there a custom version of this page for the customer
	$custompage = './inc.mod_memo.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// INIT memo
	if(!isset($aENV['php5']) || !$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php');
	}
	else {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php5');
	}
	$oMemo =& new Memo($oDb);

// initialise TEAMACCESS Object, if it isn't allready initialised 
	if (!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		
		$oTACEA	=& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->initialize();
	}

// save USER (key: ID, value: Name)
	$oUser	=& new User($oDb);
	$oTeam	=& new Team($oDb);
	$aUsergroup	= $oTeam->getAllTeamNames(); 
	$aUsername	= $oUser->getAllUserNames(false);
?>
<script language="JavaScript" type="text/javascript">
	function confirmDeleteMemo() {
		var chk = window.confirm('<?php echo $aMSG['err']['delete_recfromdesk'][$syslang]; ?>');
		return(chk);
	}

	function confirmRemoveMemoFromDesk() {
		var chk = window.confirm('<?php echo $aMSG['err']['delete_recfromdesk'][$syslang]; ?>');
		return(chk);
	}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabelle">
<tr>
	<td class="th" width="90%"><b><?php echo $aMSG['topnavi']['memos'][$syslang]; ?></b></td>
	<td class="th" width="10%" align="right" nowrap>
		<a href="<?php echo $aENV['path']['sys']['http'].'sys_memo_detail.php';?>" title="<?php echo $aMSG['std']['new'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_add.gif" alt="<?php echo $aMSG['btn']['new'][$syslang]; ?>" class="btn"></a>
	</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php 
	$aData	= $oMemo->getMemos();
	
	for ($i=0;!empty($aData[$i]['id']);$i++) {
	// only show memos that are for the user that's logged in
		if(!$oMemo->isUserInAuthedTeam($Userdata['id'],$aData[$i]['id'],$oTACEA,$oTeam)) continue;
		if($aData[$i]['prio'] == 0) $class = 'sub2'; // normal
		if($aData[$i]['prio'] == 1) $class = 'sub3'; // unimportant
		if($aData[$i]['prio'] == 2) $class = 'highlight'; // important
	// output
		echo '<tr><td class="'.$class.'" width="90%"><span class="text">';
		// date - time
		echo '<b>'.$oDate->date_from_mysql_timestamp($aData[$i]['last_modified'])."</b>\n";
		echo ' - '.$oDate->time_from_mysql_timestamp($aData[$i]['last_modified']).', ';
		// created_by
		echo $aUsername[$aData[$i]['created_by']];
		// last_mod_by
		if (isset($aData[$i]['last_mod_by']) && ($aData[$i]['last_mod_by'] != $aData[$i]['created_by']) ) {
			echo " / ".$aMSG['std']['last_update'][$syslang].": ".$aUsername[$aData[$i]['last_mod_by']];
		}
		// show involved users
		echo '<br><small>'.$aMSG['form']['foruser'][$syslang].' '."\n";
		$sParticipants = '<span> '.str_replace(',','</span>,<span>',$oMemo->getFormatedParticipants($aData[$i]['id'],$oTeam,$oUser,$oTACEA)).'</span>';
		$sParticipants = str_replace('<span> (-)',' <span style="text-decoration: line-through;">',$sParticipants);
		echo $sParticipants;
		echo '</small></td>';
		// buttons
		echo '<td class="'.$class.'" width="10%" align="right" nowrap><p><a href="'.$aENV['path']['sys']['http'].'sys_memo_detail.php?id='.$aData[$i]['id'].'" title="'.$aMSG['std']['edit'][$syslang].'">';
		echo '<img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['std']['edit'][$syslang].'" class="btn"></a>';
		echo '<a href="'.$aENV['path']['sys']['http'].'sys_memo_detail.php?delete_id='.$aData[$i]['id'].'" title="'.$aMSG['btn']['delete'][$syslang].'">';
		echo '<img src="'.$aENV['path']['pix']['http'].'btn_delete.gif" alt="'.$aMSG['btn']['delete'][$syslang].'" class="btn"></a>';
		echo '</p></td></tr>'."\n";
		// memo
		echo '<tr><td colspan="2"><span class="text">';
		echo nl2br_cms($aData[$i]['memo']);
		echo '</span></td></tr>'."\n";
		// spacer
		echo '<tr><td colspan="2" class="off"><img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"></td></tr>';
	 } // END while ?> 
</table>