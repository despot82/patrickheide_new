<?php

	require_once ('../sys/php/_include_all.php');

// is there a custom version of this page for the customer
	$custompage = $aENV['path']['sys']['unix'].'inc.reloader.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
		
	// User
	$oUser =& new User($oDb); // params: &$oDb[,$aConfig=NULL)
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Login Information</title>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $aENV['charset']; ?>">
	<meta http-equiv="content-language" content="<?php echo $syslang; ?>">
	<meta http-equiv="REFRESH" content="60">

<link href="<?php echo $aENV['path']['css']['http']; ?>styles.css" type="text/css" rel="stylesheet">
<style type="text/css">
<?php require_once($aENV['path']['config']['unix']."styles.php"); ?>
</style>
<script language="JavaScript" type="text/javascript">
	var sysPath = '<?php echo $aENV['path']['sys']['http']; // VOR der "sys_functions.js"! ?>';
	var calPath = '<?php echo $aENV['path']['cal']['http']; // VOR der "sys_functions.js"! ?>';
	var mediaDbPath = '<?php echo $aENV['path']['mdb']['http']; // VOR der "sys_functions.js"! ?>';
	var modulPath = '<?php echo $aENV['SELF_PATH']; // VOR der "*functions.js"! ?>';
</script>
<script src="<?php echo $aENV['path']['js']['http']; ?>sys_functions.js" language="JavaScript" type="text/javascript"></script>
<?php if (file_exists("./scripts/functions.js")) { ?><script src="<?php echo $aENV['SELF_PATH']; ?>scripts/functions.js" language="JavaScript" type="text/javascript"></script><?php } ?>

</head>

<body class="bodyiframe">
<?php
// FLASHTEST ---------------------------------------------------------------------------------
	// GET-params
	if (is_object($oSess) && isset($_GET['hasFlash'])) {
		$oSess->set_var('bHasFlash', $_GET['hasFlash']); // params: $sName,$value // FLASHTEST
	}
	
	if (is_object($oSess) && isset($Userdata['id']) 
		&& $oSess->get_var('bHasFlash') != "true" && $oSess->get_var('bHasFlash') != "upgrade"
		) { // only if logged in + im cms/pms/frm
		$bgColor			= '#000000';
		$flashContentURL	= $aENV['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'].'&hasFlash=true';
		$altContentURL		= $aENV['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'].'&hasFlash=upgrade';
		$href				= $aENV['path']['sys']['http'].'flash_detection.swf?flashContentURL='.urlencode($flashContentURL).'&altContentURL='.urlencode($altContentURL).'&contentVersion=7&contentMajorRevision=0&contentMinorRevision=0&allowFlashAutoInstall=true';

	 // FLASHTEST only if not MSIE on https
		if (!($oBrowser->isIE() && $aENV['https']['inform'])) { ?>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" 
		codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=4,0,0,0" 
		width="18" 
		height="18">
	<param name="movie" value="<?php echo $href; ?>" />
	<param name="quality" value="low" />
	<param name="wmode" value="Transparent" />
	<embed src="<?php echo $href; ?>" 
		quality="low"
		wmode="Transparent"
		pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" 
		type="application/x-shockwave-flash" 
		width="18" 
		height="18">
	</embed>
</object>
<?php	
		}
	} // END FLASHTEST ------------------------------------------------------------------------
	
/**
 * If Currencyconverter is used, get the latest Currencys
 * and save them in an array.
 * Call a different class if PHP5 is used
 */
	// die aktuellen Kurse fuer den Waehrungsrechner ermitteln.
	if(in_array('calc_currency', $aENV['portal_module']['R'])) {
		require_once($aENV['path']['global_service']['unix'].'class.CurrencyConverter.php');
		$oCurConv	= new CurrencyConverter($aENV);
		$oCurConv->check($aENV);
	}
	 
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right" nowrap><span class="footer">
	<noscript><span class="marker"><b><?php echo $aMSG['err']['js_is_disabled'][$syslang]; ?></b></span></noscript>
	<?php
	// ggf. DEV-Hinweis anzeigen
	if ($_SERVER['HTTP_HOST'] == 'rhsrv2' || $_SERVER['HTTP_HOST'] == '10.1.1.3') {
		echo '<span class="marker">Development-Server / </span>';
	}
	// Server-Zeit
	echo $aMSG['std']['currenttime'][$syslang].' <b>'.date("H:i").'</b>&nbsp;|&nbsp;';
	// LOGIN-Status
	$aUsername	= $oUser->getUserNames($Userdata['id'], false, false);
	echo 'Login: <b>'.$aUsername[$Userdata['id']].'</b>&nbsp;|&nbsp;';
?>
<a href="<?php echo $aENV['page']['logout']; ?>" class="footer" target="_parent" title="<?php echo $aMSG['topnavi']['logout'][$syslang]; ?>"><?php echo $aMSG['topnavi']['logout'][$syslang]; ?></a></span></td>
</tr>
</table>



</body>
</html>