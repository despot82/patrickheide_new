<?php
/**
* sys_usergroup_detail.php
*
* Detailpage: usergroups
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz (optional)
* @param	int		$user_id	welcher Nutzer direkt mit dieser (neuen) UG verknuepft werden soll (optional)
* @param	array	Formular:	$aPriv
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2005-11-14 (NEUES RECHTEMANAGEMENT - NICHT MEHR ABWAERTSKOMPATIBEL!)
* #history	1.0 / 2005-01-12
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_usergroup_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// diese seite ist nur was fuer admins ;-)
	if ($oPerm->hasPriv('admin') == false) { echo js_history_back(); }

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$user_id		= (isset($_GET['user_id'])) ? $oFc->make_secure_int($_GET['user_id']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
	if (isset($_POST['user_id']) && !empty($_POST['user_id'])) { $user_id = $_POST['user_id']; } // ggf. POST ueberschreibt GET
	$aPriv			= (isset($_POST['aPriv'])) ? $_POST['aPriv'] : array();
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars:
	$sTable			= $aENV['table']['sys_usergroup'];
	$sViewerPage	= $aENV['SELF_PATH']."sys_usergroup.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// 3. DB
// DB-action: check delete
	if (isset($btn['delete']) && $aData['id']) {
		// check (nur!) ob user zu dieser usergroup gelinkt sind
		$alert = false;
		$oDb->query("SELECT `user_id` 
					FROM `".$aENV['table']['sys_user_usergroup']."` 
					WHERE `usergroup_id` = '".$aData['id']."'");
		if ($oDb->num_rows() > 0) { $alert = true; }
	}
// DB-action: delete
	if ( (isset($btn['delete']) && $aData['id'] && !$alert)
		|| (isset($btn['continue_delete']) && $aData['id'])	) {
		//make delete (in usergroup-table)
		$oDb->make_delete($aData['id'], $sTable);
		
		// in rel-table ggf. zu loeschende usergroup entfernen
		$aData2delete['usergroup_id'] = $aData['id'];
		$oDb->make_delete($aData2delete, $aENV['table']['sys_user_usergroup']);
		
		// weiterleitung overviewpage
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) || isset($btn['save_close'])) {
		// erst checken ob title = unique
		$oDb->query("SELECT `id` FROM `".$sTable."` 
					WHERE `title` = '".$aData['title']."' 
						AND `id` != '".$aData['id']."'");
		$entries = $oDb->num_rows();
		if ($entries > 0) { echo js_alert($aMSG['err']['title_notunique'][$syslang]); $alert=true; }
	}
	if ((isset($btn['save']) || isset($btn['save_close'])) && !isset($alert)) {
		// privileges zu kommasepariertem string zusammenfuehren
		$aData['permission'] =  implode(',', $aPriv);
		
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$oDb->make_update($aData, $sTable, $aData['id']);
		} else { // Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id();
			
			// ggf. neuen Nutzer mit dieser neuen UG verknuepfen
			if (!empty($user_id) && $aData['id'] > 0) {
				$aInsert = array('usergroup_id'	=> $aData['id'], 
								 'user_id'		=> $user_id, 
								 'created_by'	=> $Userdata['id']);
				$oDb->make_insert($aInsert, $aENV['table']['sys_user_usergroup']);
			}
		}
		// back to overviewpage
		if (isset($btn['save_close'])) {
			header("Location: ".$sViewerPage); exit;
		}
	}
// DB-action: save_copy
	if (isset($btn['save_copy'])) {
		// erst checken ob title = unique
		$oDb->query("SELECT `id` 
					FROM `".$sTable."` 
					WHERE `title` = '".$aData['title']."'");
		$entries = $oDb->num_rows();
		if ($entries > 0) { echo js_alert($aMSG['err']['title_notunique'][$syslang]); $alert=true; }
	}
	if (isset($btn['save_copy']) && !isset($alert)) {
		// privileges zu kommasepariertem string zusammenfuehren
		$aData['permission'] =  implode(',', $aPriv);
		
		// Insert eines neuen Eintrags
		$copy_id = $aData['id'];
		unset($aData['id']);
		$aData['created'] = $oDate->get_mysql_timestamp();
		$aData['created_by'] = $Userdata['id'];
		$oDb->make_insert($aData, $sTable);
		// Kopiere alle Verknuepfungen die die Vorlage besitzt auf die neue kopierte Table
		$aNewData['usergroup_id'] = $aData['id'] = $oDb->insert_id();
		// alle Verknuepfungen der Vorlage ermitteln...
		$oDb2 =& new dbconnect($aENV['db']);
		$oDb2->query("SELECT `user_id` 
					FROM `".$aENV['table']['sys_user_usergroup']."` 
					WHERE `usergroup_id` = '".$copy_id."'");
		while ($tmp = $oDb2->fetch_array()) {
		// ....diese auch mit der neuen usergroup verknuepfen
			$aNewData['user_id'] = $tmp['user_id'];
			$oDb->make_insert($aNewData, $aENV['table']['sys_user_usergroup']);
		}
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		// wenn "copy" geklickt wurde
		if (isset($btn['copy'])) {
			// title auffaellig veraendern!
			$aData['title'] = $aMSG['user']['copy_of'][$syslang].$aData['title'];
			$mode_key = "copy"; // do not change!
		}
	} else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");


// ANSICHT: wenn zu loeschende gruppe noch user beinhaltet -------------------------------------------------
if (isset($btn['delete']) && $alert) {
	
	// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">

<span class="title"><?php echo $aMSG['topnavi']['userrights'][$syslang]; ?></span>
<span class="text"><?php echo $aMSG['mode']['delete'][$syslang]; ?></span>
<br><br>

<?php include_once("inc.sys_detnavi.php"); 
	echo getSysUserSubnavi(); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['err']['delete_usergroup'][$syslang]; ?></span></td>
	</tr>
</table>

<?php echo HR; ?>

<?php echo '<input type="submit" value="'.$aMSG['btn']['continue_delete'][$syslang].'" name="btn[continue_delete]" id="continue_delete" class="but">' ?>
<?php echo $oForm->button("CANCEL"); ?>


<?php // ANSICHT: Form -----------------------------------------------------------------------------------
} else {
	// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("title", $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">
<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['userrights'][$syslang]; ?></span>
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></span></p>
		</td>
		<td align="right"><?php // Buttons 
			echo $oForm->button("BACK"); // params: $sType[,$sClass='']
			echo $oForm->button("NEW"); // params: $sType[,$sClass='']
		?></td>
	</tr>
</table>

<?php echo HR; ?>

<?php include_once("inc.sys_detnavi.php"); 
	echo getSysUserSubnavi(); ?>

<?php if (isset($btn['copy'])) { echo '<span class="text">'.$aMSG['std']['copy_reminder'][$syslang].'<br><br></span>'; } ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="25%"><col width="75%"></colgroup>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['usergroup'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->textfield("title", 250, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['description'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textarea("description", 3, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr><td colspan="2"><span class="text"><b><?php echo $aMSG['form']['privileges'][$syslang]; ?>*</b></span></td></tr>
	<tr valign="top">
		<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
		<tr valign="top">
			<td width="25%" class="sub3"><span class="text"><?php 
			// System/Userverwaltung
			echo '<b>'.$aMSG['topnavi']['users'][$syslang].'</b><br>';
			echo _getModulePrivilegeCheckboxes('sys', $aData['permission']);
			?></span></td>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
		<?php 
		// Alle anderen Module
		$max_in_one_row = 5;
		$count = count($aENV['module']);
		$anz_tds_per_row = ($count > $max_in_one_row) ? round($count / 2) : $count;
		$width = round(75 / $count);
		$i = 1;
		foreach ($aENV['module'] as $mod_key => $mod) {
			echo '<td width="'.$width.'%" class="sub2"><span class="text"><b>'.$aENV['module'][$mod_key][$syslang].'</b><br>';
			echo _getModulePrivilegeCheckboxes($mod_key, $aData['permission']);
			echo '<br></span></td>';
			if ($count > $max_in_one_row && $i == $anz_tds_per_row) {
				echo '</tr><tr valign="top">';
			}
			$i++;
		}
		// ggf. leere zellen schreiben
		if ($anz_tds_per_row < $count) {
			while ($i < ($anz_tds_per_row * 2 + 1)) {
				echo '<td width="'.$width.'%" class="sub2">&nbsp;</td>';
				$i++;
			}
		} ?></tr>
			</table>
		</td></tr>
		</table>
		</td>
	</tr>
</table>
<br>
<?php if (!$btn['copy']) { ?>
<?php	echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php	if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php	if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>&nbsp;
<?php	if ($mode_key == "edit") { echo $oForm->button("COPY"); } ?>
<?php } else { ?>
<?php	echo $oForm->button("SAVE_COPY"); // params: $sType[,$sClass=''] ?>
<?php	echo $oForm->button("CANCEL"); ?>
<?php } ?>

<?php
} // END if ANSICHT ----------------------------------------------------------------------------------- ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");

// HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////
	function _getModulePrivilegeCheckboxes($module, $sPrivileges='') {
		global $oDb, $syslang, $aENV;
		$sStr = '';
		$aPrivilege = explode(',', $sPrivileges);
		$oDb->query("SELECT `id`,`title_".$syslang."` as `title`, `description_".$syslang."` as `description` 
					FROM `".$aENV['table']['sys_privilege']."` 
					WHERE `module_key` = '".$module."' 
						AND `flag_online` = '1' 
					ORDER BY `prio` ASC");
		while ($tmp = $oDb->fetch_array()) {
			$checked = (in_array($tmp['id'], $aPrivilege)) ? ' checked' : '';
			$sStr .= '<nobr><input type="checkbox" name="aPriv[]" id="'.$tmp['id'].'" value="'.$tmp['id'].'" class="radiocheckbox"'.$checked.'>';
			$sStr .= '<label for="'.$tmp['id'].'">&nbsp;'.$tmp['title'].'</label></nobr>';
			/*if (!empty($tmp['description'])) {
				$sStr .= ' <br><small>('.$tmp['description'].')</small>';
			}*/
			$sStr .= "<br>\n";
		}
		return $sStr;
	}
?>
