<?php
// TopNavi Array
	#$sDefaultGetParams = '?slot=0';
	$sDefaultGetParams = '';
	$aTOPNAVI = array();

// Portal
	$aTOPNAVI['inform'] = array();
	$aTOPNAVI['inform'][] = array(
		'title'	=> $aMSG['topnavi']['portal'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
	$aTOPNAVI['inform'][] = array(
		'title'	=> $aMSG['topnavi']['home'][$syslang],
		'href'	=> $aENV['path']['sys']['http'].'index.php'.$sDefaultGetParams,
		'id'	=> 'sys_portal'
	);

	if (isset($aENV['module']['frm']) && $oPerm->hasPriv('view', 'frm')) {
		$aTOPNAVI['inform'][] = array(
			'title'	=> $aMSG['topnavi']['forum'][$syslang],
			'href'	=> $aENV['path']['frm']['http'].'frm.php'.$sDefaultGetParams,
			'id'	=> 'frm'
		);
	}
	if (isset($aENV['module']['arc']) && $oPerm->hasPriv('view', 'arc')) {
		$aTOPNAVI['inform'][] = array(
			'title'	=> $aMSG['topnavi']['archiv'][$syslang],
			'href'	=> $aENV['path']['arc']['http'].'arc.php'.$sDefaultGetParams,
			'id'	=> 'arc'
		);
	}
	if ($oPerm->hasPriv('view', 'sys')) {
		$aTOPNAVI['inform'][] = array(
			'title'	=> $aMSG['topnavi']['users'][$syslang],
			'href'	=> $aENV['path']['sys']['http'].'sys_user.php'.$sDefaultGetParams,
			'id'	=> 'sys_user'
		);
	}

	if ($oPerm->isDaService()) { // DA SERVICE
		$aTOPNAVI['inform'][] = array(
			'title'	=> 'Klassen-Doku',
			'href'	=> 'http://rhsrv2/aspekt_tools/inform_classdoku/index.html" target="_blank',
			'id'	=> 'doku'
		);
		if($_SERVER['HTTP_HOST'] == 'www.design-aspekt.com') {
			//DA Live-Server
			$aTOPNAVI['inform'][] = array(
				'title'	=> 'phpMyAdmin',
				'href'	=> 'http://www.design-aspekt.com/spc-bin/myadmin/index.php" target="_blank',
				'id'	=> 'dbgui'
			);
		} elseif($_SERVER['HTTP_HOST'] == 'rhsrv2' || $_SERVER['HTTP_CLIENT_IP'] == '10.1.1.3') {
			//DA DEV-Server
			$aTOPNAVI['inform'][] = array(
				'title'	=> 'phpMyAdmin',
				'href'	=> 'http://rhsrv2/aspekt_tools/inform_dbgui/dbgui/index.php" target="_blank',
				'id'	=> 'dbgui'
			);
		} else {
			//normal
			$aTOPNAVI['inform'][] = array(
				'title'	=> 'phpMyAdmin',
				'href'	=> $aENV['path']['dbgui']['http'].'" target="_blank',
				'id'	=> 'dbgui'
			);	
		}
		$aTOPNAVI['inform'][] = array(
			'title'	=> $aMSG['topnavi']['tools'][$syslang],
			'href'	=> $aENV['path']['sys']['http'].'sys_tools.php"',
			'id'	=> 'sys_tools'
		); 
	}

// Kalender
if (isset($aENV['module']['cal']) && $oPerm->hasPriv('view', 'cal')) {
	$aTOPNAVI['cal'] = array();
	$aTOPNAVI['cal'][] = array(
		'title'	=> $aMSG['topnavi']['cal'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
	$aTOPNAVI['cal'][] = array(
		'title'	=> $aMSG['topnavi']['termine'][$syslang],
		'href'	=> $aENV['path']['cal']['http'].'index.php'.$sDefaultGetParams,
		'id'	=> 'cal/cal_calendar'
	);
	//if ($oPerm->hasPriv('admin', 'cal') || $oPerm->hasPriv('admin', 'adm')) {
		$aTOPNAVI['cal'][] = array(
			'title'	=> $aMSG['topnavi']['yearview'][$syslang],
			'href'	=> $aENV['path']['cal']['http'].'cal_calendar_yearview.php'.$sDefaultGetParams,
			'id'	=> 'cal/cal_calendar_yearview'
		);
	//}
	
}

// Adressen
if (isset($aENV['module']['adr']) && $oPerm->hasPriv('view', 'adr')) {
	$aTOPNAVI['adr'] = array();
	$aTOPNAVI['adr'][] = array(
		'title'	=> $aMSG['topnavi']['adr'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
	if ( isset($aENV['navigation_open']) && $aENV['navigation_open'] == true) { 
		$aTOPNAVI['adr'][] = array(
			'title'	=> $aMSG['topnavi']['adr'][$syslang],
			'href'	=> $aENV['path']['adr']['http'].'index.php'.$sDefaultGetParams,
			'id'	=> 'adr/adr'
		);
	} else {
		$aTOPNAVI['adr'][] = array(
			'title'	=> $aMSG['topnavi']['addresses'][$syslang],
			'href'	=> $aENV['path']['adr']['http'].'index.php'.$sDefaultGetParams,
			'id'	=> 'adr/adr'
		);
	}
	$aTOPNAVI['adr'][] = array(
		'title'	=> $aMSG['topnavi']['mailings'][$syslang],
		'href'	=> $aENV['path']['adr']['http'].'adr_mailing.php'.$sDefaultGetParams,
		'id'	=> 'adr/adr_mailing'
	);
}

// Media-DB
if (isset($aENV['module']['mdb']) && $oPerm->hasPriv('view', 'mdb')) {
	$aTOPNAVI['mdb'] = array();
	$aTOPNAVI['mdb'][] = array(
		'title'	=> $aMSG['topnavi']['mdb'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
		$aTOPNAVI['mdb'][] = array(
			'title'	=> $aMSG['topnavi']['overview'][$syslang],
			'href'	=> $aENV['path']['mdb']['http'].'index.php'.$sDefaultGetParams,
			'id'	=> 'mdb'
		);
	
}
// Office
if (isset($aENV['module']['adm']) && $oPerm->hasPriv('view', 'adm')) {
	$aTOPNAVI['adm'] = array();
	$aTOPNAVI['adm'][] = array(
		'title'	=> $aMSG['topnavi']['adm'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
	if ($oPerm->hasPriv('admin', 'adm') || $oPerm->hasPriv('management', 'adm')) {
		$aTOPNAVI['adm'][] = array(
		'title'	=> $aMSG['topnavi']['projects'][$syslang],
		'href'	=> $aENV['path']['adm']['http'].'index.php'.$sDefaultGetParams,
		'id'	=> 'adm'
		);
	}
	if ($oPerm->hasPriv('admin', 'adm')) {
		$aTOPNAVI['adm'][] = array(
			'title'	=> $aMSG['topnavi']['overviewbill'][$syslang],
			'href'	=> $aENV['path']['adm']['http'].'adm_billing.php'.$sDefaultGetParams,
			'id'	=> 'billing'
		);
	}
	if ($oPerm->hasPriv('create', 'adm') && $oPerm->hasPriv('edit', 'adm')) {
		$aTOPNAVI['adm'][] = array(
			'title'	=> $aMSG['topnavi']['timesheets'][$syslang],
			'href'	=> $aENV['path']['adm']['http'].'adm_timesheet_month.php'.$sDefaultGetParams,
			'id'	=> 'timesheet'
		);
	}
	if ($oPerm->hasPriv('create', 'adm') && $oPerm->hasPriv('edit', 'adm')) {
		$aTOPNAVI['adm'][] = array(
			'title'	=> $aMSG['topnavi']['expenses'][$syslang],
			'href'	=> $aENV['path']['adm']['http'].'adm_expense.php'.$sDefaultGetParams,
			'id'	=> 'expense'
		);
	}
	if ($oPerm->hasPriv('admin', 'adm')) {
		$aTOPNAVI['adm'][] = array(
			'title'	=> $aMSG['topnavi']['invoices'][$syslang],
			'href'	=> $aENV['path']['adm']['http'].'adm_invoice.php'.$sDefaultGetParams,
			'id'	=> 'adm_invoice'
		);
	}
	if ($oPerm->hasPriv('admin', 'adm')) {
		$aTOPNAVI['adm'][] = array(
			'title'	=> $aMSG['topnavi']['employees'][$syslang],
			'href'	=> $aENV['path']['adm']['http'].'adm_employee.php'.$sDefaultGetParams,
			'id'	=> 'employee'
		);
	}
}

// Presentations
if (isset($aENV['module']['pms']) && $oPerm->hasPriv('view', 'pms')) {
	$aTOPNAVI['pms'] = array();
	$aTOPNAVI['pms'][] = array(
		'title'	=> $aMSG['topnavi']['pms'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
	if ( isset($aENV['navigation_open']) && $aENV['navigation_open'] == true) { 
		$aTOPNAVI['pms'][] = array(
			'title'	=> $aMSG['topnavi']['pms'][$syslang],
			'href'	=> $aENV['path']['pms']['http'].'index.php'.$sDefaultGetParams,
			'id'	=> 'pms'
		); } else {
		$aTOPNAVI['pms'][] = array(
			'title'	=> $aMSG['std']['overview'][$syslang],
			'href'	=> $aENV['path']['pms']['http'].'index.php'.$sDefaultGetParams,
			'id'	=> 'pms'
		);
	}
	$aTOPNAVI['pms'][] = array(
		'title'	=> $aMSG['topnavi']['archives'][$syslang],
		'href'	=> $aENV['path']['pms']['http'].'pms_archive.php'.$sDefaultGetParams,
		'id'	=> 'pms_archive'
	);
}

// Web CMS
if (isset($aENV['module']['cms']) && $oPerm->hasPriv('view', 'cms')) {
	$aTOPNAVI['cms'] = array();
	$aTOPNAVI['cms'][] = array(
		'title'	=> $aMSG['topnavi']['website'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
	if ( isset($aENV['navigation_open']) && $aENV['navigation_open'] == true) { 
		$aTOPNAVI['cms'][] = array(
			'title'	=> $aMSG['topnavi']['website'][$syslang],
			'href'	=> $aENV['path']['cms']['http'].'index.php'.$sDefaultGetParams,
			'id'	=> 'cms'
		); } else {
		$aTOPNAVI['cms'][] = array(
			'title'	=> $aMSG['topnavi']['cms'][$syslang],
			'href'	=> $aENV['path']['cms']['http'].'index.php'.$sDefaultGetParams,
			'id'	=> 'cms'
		);
	}
	if (isset($aENV['module']['clientsites']) && $oPerm->hasPriv('view', 'clientsites')) {
		$aTOPNAVI['cms'][] = array(
			'title'	=> $aMSG['topnavi']['clientsites'][$syslang],
			'href'	=> $aENV['path']['clientsites']['http'].'cms_projectsite.php'.$sDefaultGetParams,
			'id'	=> 'cms_projectsite'
		);
	}
	if (isset($aENV['allow_newsletter']) && $aENV['allow_newsletter'] == true && $oPerm->hasPriv('create', 'cms')) {
		$aTOPNAVI['cms'][] = array(
			'title'	=> $aMSG['topnavi']['newsletter'][$syslang],
			'href'	=> $aENV['path']['cms']['http'].'cms_newsletter.php'.$sDefaultGetParams,
			'id'	=> 'cms_newsletter'
		);
	}
	if (isset($aENV['allow_cug']) && $aENV['allow_cug'] == true && $oPerm->hasPriv('view', 'adr')) {
		$aTOPNAVI['cms'][] = array(
			'title'	=> $aMSG['topnavi']['cug'][$syslang],
			'href'	=> $aENV['path']['adr']['http'].'web_cug.php'.$sDefaultGetParams,
			'id'	=> 'web_cug'
		);
	}
	$aTOPNAVI['cms'][] = array(
		'title'	=> $aMSG['std']['online'][$syslang],
		'href'	=> '../../index.php" target="_blank'.$sDefaultGetParams,
		'id'	=> 'website'
	);
}


// NUR Modul clientsites (wenn jemand KEINE CMS-view-Rechte hat...)
if ((!isset($aENV['module']['cms']) || !$oPerm->hasPriv('view', 'cms')) 
	&& isset($aENV['module']['clientsites']) && $oPerm->hasPriv('view', 'clientsites')
	) {
	$aTOPNAVI['cms'] = array();
	$aTOPNAVI['cms'][] = array(
		'title'	=> $aMSG['topnavi']['website'][$syslang],
		'href'	=> '',
		'id'	=> ''
	);
	$aTOPNAVI['cms'][] = array(
		'title'	=> $aMSG['topnavi']['clientsites'][$syslang],
		'href'	=> $aENV['path']['clientsites']['http'].'cms_projectsite.php'.$sDefaultGetParams,
		'id'	=> 'cms_projectsite'
	);
	if (isset($aENV['allow_cug']) && $aENV['allow_cug'] == true && $oPerm->hasPriv('view', 'adr')) {
		$aTOPNAVI['cms'][] = array(
			'title'	=> $aMSG['topnavi']['cug'][$syslang],
			'href'	=> $aENV['path']['adr']['http'].'web_cug.php'.$sDefaultGetParams,
			'id'	=> 'web_cug'
		);
	}
}
?>