<?php

#-------------------------------------------------------------------------------
# // HELP-functions ... customized for this project
#-------------------------------------------------------------------------------

// fetch STANDARD DATA
	function get_standarddata($sNaviId, $sFilename="slideshow.swf", $sCaptionDiv='', $sHref='') {
		global $oSess, $aENV, $aMSG, $oDb, $aPageInfo, $weblang, $oMediadb, $sFirstDescription, $PREVIEW;
		$aBuffer = array();
		$sql = "SELECT navi_id,
					title_".$weblang." as title,
					title2_".$weblang." as title2,
					title3_".$weblang." as title3,
					copytext_".$weblang." as copytext,
					copytext2_".$weblang." as copytext2,
					copytext3_".$weblang." as copytext3,
					int_link, slideshow
				FROM ".$aENV['table']['cms_standard']."
				WHERE navi_id = '".$sNaviId."'";

		// Live-Version
		$oDb->query($sql.' AND preview_ref_id = 0');
		$aData = $oDb->fetch_array();
		$bIsPreview = false;

		// if preview, get the preview version
		if ($PREVIEW == true) {
			$oDb->query($sql.' AND preview_ref_id > 0');
			if ($oDb->num_rows() > 0) {
				// overwrite the live version with the preview version
				$aData = $oDb->fetch_array();
				$bIsPreview = true;
			}
		}

		$aBuffer['headline']	= (isset($aData['title']) && !empty($aData['title'])) ? $aData['title'] : '';
		if ($bIsPreview == true) {
			$aBuffer['headline'] = ICN_LOT_CONTENT.$aBuffer['headline'];
		}
		$aBuffer['headline2']	= (isset($aData['title2']) && !empty($aData['title2'])) ? $aData['title2'] : '';
		if ($bIsPreview == true) {
			$aBuffer['headline2'] = ICN_LOT_CONTENT.$aBuffer['headline2'];
		}
		$aBuffer['headline3']	= (isset($aData['title3']) && !empty($aData['title3'])) ? $aData['title3'] : '';
		if ($bIsPreview == true) {
			$aBuffer['headline3'] = ICN_LOT_CONTENT.$aBuffer['headline3'];
		}
		$aBuffer['copytext']	= (isset($aData['copytext']) && !empty($aData['copytext'])) ? nl2br_cms($aData['copytext']) : '';
		$aBuffer['copytext2']	= (isset($aData['copytext2']) && !empty($aData['copytext2'])) ? nl2br_cms($aData['copytext2']) : '';
		$aBuffer['copytext3']	= (isset($aData['copytext3']) && !empty($aData['copytext3'])) ? nl2br_cms($aData['copytext3']) : '';

		if ($aPageInfo ['template'] == 'home') {
			$aBuffer['int_link']	= ($aData['int_link'] != '') ? $aData['int_link'] : '';
			$aBuffer['object_tag']	= ($aData['slideshow'] != "") ? _write_flashSlideshow($aData['slideshow'], $sFilename, 'captionDiv', $aData['int_link']) : "";
			$aBuffer['js_content']	= ($aData['slideshow'] != "") ? _write_jsSlideshow($aData['slideshow'], 'captionDiv', $aData['int_link']) : "aSlides = new Array(); aSlideTxt = new Array();";
			$aBuffer['alt_content'] = ($aData['slideshow'] != "") ? _write_imgTag($aData['slideshow'], $aData['int_link']) : "";
			$aBuffer['first_description']  = $sFirstDescription;
		}

		else {
			$sImages = '';
			if ($aData['slideshow'] != '') {
				$aImgId = explode(",", $aData['slideshow']);
				foreach ($aImgId as $key => $val) {
					$aMedia = $oMediadb->getMedia($val);
					$sImages .= '<div class="previewImgOn"><a href="'.$aENV['path']['mdb_upload']['http'].$aMedia['filename'].'" class="lightwindow page-options" rel="Std[Images]" title="'.$aData['title'].'" caption="'.nl2br_cms($aMedia['description']).'">'._getPreviewImg($val).'</a></div>';
				}
			}
			$aBuffer['images'] = $sImages;
		}

		return $aBuffer;
	}


// SLIDESHOW: schreibt alternativen Content als einfachen IMG-Tag
	function _write_imgTag($sImgId, $sHref='') {
		global $aENV, $oMediadb, $sFirstDescription;
		$sOutput = "";
		$path = $aENV['path']['mdb_upload']['http'];

		// Unterscheidung ein oder mehr Bilder
		if (strstr($sImgId, ",")) {	// wenn mehrere Bilder
			$aTmp = explode(",", $sImgId);
			$sImgId = $aTmp[0];
		}
		$aImg = $oMediadb->getMedia($sImgId); // params: $nMediaId
		$sImgFilename = $aImg['filename'];
		$sFirstDescription = nl2br_cms($aImg['description']);

		// write image tag
		$sOutput = '<img src="'.$path.$sImgFilename.'" id="slide_bak" title="" alt="" />';
		if ($sHref != '') {
			$sOutput = '<a href="'.$sHref.'">'.$sOutput.'</a>';
		}
		return $sOutput;
	}

// SLIDESHOW schreibt JS-Slideshow
	function _write_jsSlideshow($sImgId, $sCaptionDiv='', $sHref='') {
		global $aENV, $oMediadb;
		$sOutput = "";
		$path = $aENV['path']['mdb_upload']['http'];

		$aImgSrc = explode(",", $sImgId);
		$anzahl = count($aImgSrc);

		$oMediadb->_makeDbConnect();
		$oMediadb->oDb->query("SELECT id, filename, width, height, description FROM ".$aENV['table']['mdb_media']." WHERE id IN (".$sImgId.")");

		// get image-data into temp-array
		$aImgData = array();
		while ($aImg = $oMediadb->oDb->fetch_array()) {
			$aImgData[$aImg['id']]['filename']		= $aImg['filename'];
			$aImgData[$aImg['id']]['description']	= nl2br_cms($aImg['description']);
		}
		$alt = $aImgData[$aImgSrc[0]]['description'];

		// write javascript - define array
		$sOutput .= "aSlides = new Array();\n";
		for ($i=0; $i<$anzahl; $i++) { // put all picture-filenames in this JS-array
			$sImgFilename = $aImgData[$aImgSrc[$i]]['filename'];
			if ($i == 0) { $sFirstImg = $sImgFilename; } // erstes bild fuer image-tag merken
			$sOutput .= "aSlides[". $i."] = '".$path.$sImgFilename."';\n";
		}

		$sParams = '';
		if ($sCaptionDiv != '') {
			$sOutput .= "aSlideTxt = new Array();\n";
			for ($i=0; $i<$anzahl; $i++) { // put all picture-filenames in this JS-array
				$sImgDescription = str_replace("'", "\'", $aImgData[$aImgSrc[$i]]['description']);
				$sOutput .= "aSlideTxt[". $i."] = '".$sImgDescription."';\n";
			}
			$sParams .= ', aSlideTxt, "'.$sCaptionDiv.'"';
		}

		// image-tag ggf. verlinken
		$sImageTag = '<img src="'.$path.$sFirstImg.'" id="slide" title="" alt="" />';
		if ($sHref != '') {
			$sImageTag = '<a href="'.$sHref.'">'.$sImageTag.'</a>';
		}

		$sOutput .= 'document.writeln(\''.$sImageTag.'\');'."\n";
		$sOutput .= 'Slideshow = new slideshow(aSlides, "slide", "", 1, "true", "false"'.$sParams.');	// params: aSlides,imgName[,layerID][,direction=1][,autostart="true"][,imgButtons="true"][,aSlideTxt][,captionDivID]]'."\n";

		return $sOutput;
	}

// SLIDESHOW: schreibt Object-Tag für Flash-Slideshow
	function _write_flashSlideshow($sImgId, $sFilename="slideshow.swf", $sCaptionDiv='', $sHref='') {
		global $aENV, $oMediadb, $oBrowser, $sJSArrays;
		$sOutput = "";
		$sJSArrays = "";

		$path = $aENV['path']['mdb_upload']['http'];

		$aImgId = explode(",", $sImgId);
		$anzahl = count($aImgId);

		$oMediadb->_makeDbConnect();
		$oMediadb->oDb->query("SELECT id, filename, width, height, description FROM ".$aENV['table']['mdb_media']." WHERE id IN (".$sImgId.")");

		// get image-data into temp-array
		$aImgData = array();
		while ($aImg = $oMediadb->oDb->fetch_array()) {
			$aImgData[$aImg['id']]['filename']		= $path.$aImg['filename'];
			$aImgData[$aImg['id']]['description']	= nl2br_cms($aImg['description']);
		}
		$aImgFilename = array();
		$aImgDescription = array();
		foreach ($aImgId as $id) {
			$aImgFilename[] = $aImgData[$id]['filename'];
			$aImgDescription[] = str_replace("'", "\'", $aImgData[$id]['description']);
		}
		$sImgFilename = implode(",", $aImgFilename);
		$sImgDescription = implode("|", $aImgDescription);

		// GET-Parameter f.d. Flash-Film zusammenbauen
		$sGetParams = '?sPix='.urlencode($sImgFilename);
		if ($sCaptionDiv != '') {
			$sJSArrays .= "aSlideTxt = new Array();\n";
			for ($i=0; $i<count($aImgDescription); $i++) { // put all picture-filenames in this JS-array
				$sJSArrays .= "aSlideTxt[".$i."] = '".$aImgDescription[$i]."';\n";
			}
//			$sGetParams .= '&amp;sTxt='.stripForFlash($sImgDescription);
			$sGetParams .= '&amp;txt=true';
		}
		if ($sHref != '') {
			$sGetParams .= '&amp;sHref='.$sHref;
		}

		// write HTML-Output
		$sOutput .= '<object data="./web/pix/'.$sFilename.$sGetParams.'" type="application/x-shockwave-flash" width="100%" height="100%" id="slideshow_flash">';
		$sOutput .= '<param name="allowscriptaccess" value="samedomain" />';
		$sOutput .= '<param name="movie" value="./web/pix/'.$sFilename.$sGetParams.'" />';
		$sOutput .= '<param name="quality" value="high" />';
		$sOutput .= '<param name="bgcolor" value="#ffffff" />';
		$sOutput .= '<param name="scale" value="noscale" />';
		$sOutput .= '<param name="salign" value="tl" />';
		$sOutput .= '<param name="menu" value="false" />';
		$sOutput .= '<param name="wmode" value="window" />';
		$sOutput .= '</object>';

		return $sOutput;
	}

// schreibt Flash-Tag für Animationen
	function _write_flashTag($sImgId) {
		global $aENV, $oMediadb;
		$sOutput = "";
		$path = $aENV['path']['mdb_upload']['http'];

		$oMediadb->_makeDbConnect();
		$oMediadb->oDb->query("SELECT id, filename FROM ".$aENV['table']['mdb_media']." WHERE id = ".$sImgId);
		$aImg = $oMediadb->oDb->fetch_array();

		// write HTML-Output
		$sOutput = '<object data="./web/pix/loader.swf?clip='.urlencode($path.$aImg['filename']).'" type="application/x-shockwave-flash" width="100%" height="100%" id="flash_movie">';
		#$sOutput = '<object data="'.$path.$aImg['filename'].'" type="application/x-shockwave-flash" width="100%" height="100%" id="flash_movie">';
		$sOutput .= '<param name="allowscriptaccess" value="samedomain" />';
		$sOutput .= '<param name="movie" value="./web/pix/loader.swf?clip='.urlencode($path.$aImg['filename']).'" />';
		#$sOutput .= '<param name="movie" value="'.$path.$aImg['filename'].'" />';
		$sOutput .= '<param name="quality" value="high" />';
		$sOutput .= '<param name="bgcolor" value="#191946" />';
		$sOutput .= '<param name="scale" value="showall" />';
		$sOutput .= '<param name="salign" value="tl" />';
		$sOutput .= '<param name="menu" value="false" />';
		$sOutput .= '<param name="wmode" value="window" />';
		$sOutput .= '</object>';

		return $sOutput;
	}

// Link zum PDF-File
	function _getPdf($sPdfId, $sLang) {
		// vars
		if (empty($sPdfId) || $sPdfId == 0) return;
		$sPdfId = str_replace(';', '', strip_tags($sPdfId)); // sichern
		global $aENV;

		if (empty($sLang)) { $sLang = $aENV['content_language']['default']; }
		$path		= $aENV['path']['mdb_upload'];
		$buffer		= array();
		$aPdfId		= array();
		$aPdfData	= array();
		$oDb = new dbconnect($aENV['db']);

		// explode string -> array
		if (strstr($sPdfId, ",")) { $aPdfId = explode(",", $sPdfId); }
		else { $aPdfId[] = $sPdfId; }

		// alle PDFs in tmp-array zwischenspeichern...
		$oDb->query("SELECT id, filename, description as title, filesize
					FROM ".$aENV['table']['mdb_media']."
					WHERE id IN (".$sPdfId.")");
		while ($tmp = $oDb->fetch_array()) {
			if (!file_exists($aENV['path']['mdb_upload']['unix'].$tmp['filename'])) { continue; } // wenn nicht im filesystem -> ueberspringen!
			if (empty($tmp['filesize'])) $tmp['filesize'] = filesize($path['unix'].$tmp['filename']);
			$aPdfData[$tmp['id']]['title']		= ($tmp['title'] != "") ? $tmp['title'].' - ' : '';
			$aPdfData[$tmp['id']]['filename']	= $tmp['filename'];
			$aPdfData[$tmp['id']]['filesize']	= round($tmp['filesize'] / 1024);
		}

		// ...und in der richtigen reihenfolge ausgeben
		$anzahl = count($aPdfData);
		for ($i=0; $i<$anzahl; $i++) {
			$title		= $aPdfData[$aPdfId[$i]]['title'].'PDF Download [ '.$aPdfData[$aPdfId[$i]]['filesize'].' KB ]';
			$link		= '<a href="'.$aENV['path']['mdb_upload']['http'].$aPdfData[$aPdfId[$i]]['filename'].'" title="'.$title.'">';
			$buffer[]	= $link.$title.'</a>';
		}

		// output
		return implode("<br />", $buffer);
	}

// Blätterfunktion für Übersichtsseiten
	function _getDbNavi($nEntries, $nStart=0, $nLimit=20, $sGEToptions='') {
		if (!$nEntries) return;

		// vars
		global $weblang, $aENV;
		$sOutput = '';
		$sStr = array('status','pages','links');
		$MSG = array();
		$sGEToptions = preg_replace("/^\?/","&",$sGEToptions);
		$nStart = $nStart + 0; // sichern -->> Konvertierung auf Integer durch Addition von 0

	// pages -----------------------------------------------------------------------------
		$nPages = ceil($nEntries/$nLimit);
		$nPage  = floor($nStart/$nLimit) + 1;

	// links -----------------------------------------------------------------------------
		$sStr['links'] = '';

		$MSG['next_page']['de'] = "nächste Seite";
		$MSG['next_page']['en'] = "next Page";
		$MSG['prev_page']['de'] = "vorige Seite";
		$MSG['prev_page']['en'] = "previous Page";

		if ($nEntries > $nLimit) {
			if (!$nPage) { $nPage = 1; }

			// Pfeil nach links
			if ($nStart > 0) {
				$sStr['links'] .= '<a href="'.$aENV['PHP_SELF'].'?start='.($nStart-$nLimit).$sGEToptions.'" title="'.$MSG['prev_page'][$weblang].'">';
				$sStr['links'] .= "&lt;";
				$sStr['links'] .= "</a>\n";
			}
			else {
				$sStr['links'] .= "&lt;\n";
			}

			// Seitenzahlen
			$sStr['links'] .= '&nbsp;&nbsp;<strong>'.$nPage.'</strong>';
			$sStr['links'] .= '&nbsp;|&nbsp;';
			$sStr['links'] .= $nPages.'&nbsp;&nbsp;';

			// Pfeil nach rechts
			if ($nStart < (($nPages-1)*$nLimit)) {
				$sStr['links'] .=  "\n".'<a href="'.$aENV['PHP_SELF'].'?start='.($nStart+$nLimit).$sGEToptions.'" title="'.$MSG['next_page'][$weblang].'">';
				$sStr['links'] .= "&gt;";
				$sStr['links'] .= "</a>";
			}
			else {
				$sStr['links'] .= "\n&gt;";
			}

			$sOutput = $sStr['links'];
		}

		return $sOutput;
	}

// Blätterfunktion für Thumbs
	function _getThumbNavi($nEntries, $nStart=0, $nLimit=20, $sGEToptions='') {
		if (!$nEntries) return;

		// vars
		global $weblang, $aENV;
		$sOutput = '';
		$sStr = array('status','pages','links');
		$MSG = array();
		$sGEToptions = preg_replace("/^\?/","&",$sGEToptions);
		$nStart = $nStart + 0; // sichern -->> Konvertierung auf Integer durch Addition von 0

	// pages -----------------------------------------------------------------------------
		$nPages = ceil($nEntries/$nLimit);
		$nPage  = floor($nStart/$nLimit) + 1;

	// links -----------------------------------------------------------------------------
		$sStr['links'] = '';

		$MSG['next_page']['de'] = "nächste Seite";
		$MSG['next_page']['en'] = "next Page";
		$MSG['prev_page']['de'] = "vorige Seite";
		$MSG['prev_page']['en'] = "previous Page";

		$aIcon = array(); // pfeile
		$aIcon['prev'] = '<img src="'.$aENV['path']['root']['http'].'web/pix/skip_back_on.gif" width="6" height="11" alt="'.$MSG['prev_page'][$weblang].'" title="'.$MSG['prev_page'][$weblang].'" />';
		$aIcon['next'] = '<img src="'.$aENV['path']['root']['http'].'web/pix/skip_frw_on.gif" width="6" height="11" alt="'.$MSG['next_page'][$weblang].'" title="'.$MSG['next_page'][$weblang].'" />';
		$aIcon['prev_off'] = '<img src="'.$aENV['path']['root']['http'].'web/pix/skip_back_off.gif" width="6" height="11" alt="" title="" />';
		$aIcon['next_off'] = '<img src="'.$aENV['path']['root']['http'].'web/pix/skip_frw_off.gif" width="6" height="11" alt="" title="" />';

		if ($nEntries > $nLimit) {
			if (!$nPage) { $nPage = 1; }

			// Pfeil nach links
			if ($nStart > 0) {
				$sStr['links'] .= '<a href="'.$aENV['PHP_SELF'].'?start='.($nStart-$nLimit).$sGEToptions.'" title="'.$MSG['prev_page'][$weblang].'">';
				$sStr['links'] .= $aIcon['prev'];
				$sStr['links'] .= "</a>";
			}
			else {
				$sStr['links'] .= $aIcon['prev_off'];
			}

			// Seitenzahlen
			$sStr['links'] .= '&nbsp;&nbsp;&nbsp;<span style="color:#000000">'.$nPage.'</span>';
			$sStr['links'] .= '&nbsp;&nbsp;/&nbsp;&nbsp;';
			$sStr['links'] .= $nPages.'&nbsp;&nbsp;&nbsp;';

			// Pfeil nach rechts
			if ($nStart < (($nPages-1)*$nLimit)) {
				$sStr['links'] .=  '<a href="'.$aENV['PHP_SELF'].'?start='.($nStart+$nLimit).$sGEToptions.'" title="'.$MSG['next_page'][$weblang].'">';
				$sStr['links'] .= $aIcon['next'];
				$sStr['links'] .= "</a>";
			}
			else {
				$sStr['links'] .= $aIcon['next_off'];
			}

			$sOutput = $sStr['links'];
		}

		return $sOutput;
	}

	// get Artists
	function getAllArtists($sNaviId,$sLink) {
		global $aENV, $aMSG, $oDb, $weblang, $aSlideshowAll, $aArtistId;

		$sBufferArtists = "";
		$oDb->query("SELECT id, title_".$weblang." as headline, slideshow
				FROM `web_artists`
				WHERE navi_id = '".$sNaviId."'
					AND flag_online_".$weblang." != '0'
				ORDER BY prio DESC");
		$aSlideshowAll = array();
		$aArtistId = array();
		while ($aData = $oDb->fetch_array()) {
			if ($aData['slideshow'] != '') {
				$aTmp = explode(",", $aData['slideshow']);
				$aSlideshowAll[] = $aTmp[0];
				$aArtistId[] = $aData['id'];
			}
			$sBufferArtists .= '<li><a href="'.$sLink.'?id='.$aData['id'].'" id="artist'.$aData['id'].'" title="'.$aMSG['web']['more'][$weblang].'">'.$aData['headline'].'</a><br /></li>'."\n";
		}

		// output
		return $sBufferArtists;
	}

	// get Events
	function getAllEvents($sNaviId, $sConstraint) {
		global $aENV, $aMSG, $oDb, $oDate, $weblang;

		$aBufferEvents = "";
		$oDb->query("SELECT id, title_".$weblang." as headline, subtitle_".$weblang." as subtitle, date, date2, copytext_".$weblang." as copytext
				FROM `web_events`
				WHERE navi_id = '".$sNaviId."' $sConstraint
				AND flag_online_".$weblang." != '0'
				ORDER BY date DESC");
		// $nEntries = $oDb->num_rows();
		while ($aData = $oDb->fetch_array()) {
			$aBufferEvents .= ($aData['date'] != "0000-00-00") ? $oDate->date_mysql2trad($aData['date']) : "";
			$aBufferEvents .= "<br />";
			$aBufferEvents .= "<b>".$aData['headline']."</b><br />";
			$aBufferEvents .= ($aData['subtitle'] != "") ? $aData['subtitle']."<br />" : "";
			$aBufferEvents .= ($aData['copytext'] != "") ? '<a href="'.$_SERVER['PHP_SELF'].'?id='.$aData['id'].'" title="'.$aMSG['web']['more'][$weblang].'" onfocus="this.blur()">'.$aMSG['web']['more'][$weblang].'</a>' : '';
			$aBufferEvents .= "<br /><br />";
		}
		// output
		return $aBufferEvents;
	}

	// get years
	function getYearsFromDB($sNaviId,$sTable) {
		global $oDb, $weblang;

		$aBufferYears = array();
		$oDb->query("SELECT date, id
					FROM $sTable
					WHERE navi_id = '".$sNaviId."'
						AND flag_online_".$weblang." != 0
					GROUP BY YEAR(date) ORDER BY date DESC");
		//$nEntries = $oDb->num_rows();
		while ($aData = $oDb->fetch_array()) {
			$dates = explode("-", $aData['date']);
			$dates = $dates[0];
			$aBufferYears[] = $dates;
		}
		// output
		return $aBufferYears;
	}

	function _getPreviewImg($nImgId) {
		global $oMediadb;

		$aMedia = $oMediadb->getMedia($nImgId);
		// calculate correct width of thumb to fit into box -> oMediadb->getMediaThumb() needs width parameter
		if ($aMedia['height'] < $aMedia['width']) {	// landscape format
			$nScale = floor(205 * 100 / $aMedia['width']);	// scale factor if fitted to box width
			$nNewHeight = floor(($aMedia['height'] * $nScale) / 100);	// actual height if fitted to box
			if ($nNewHeight > 140) {
				$nScale = floor(140 * 100 / $aMedia['height']);	// scale factor if fitted to box height
				$nNewHeight = floor(($aMedia['height'] * $nScale) / 100);	// actual height if fitted to box
				$nNewWidth = floor(($aMedia['width'] * $nScale) / 100);	// actual width if fitted to box height
			}
			else {
				$nNewWidth = 205;
			}
		}
		else {	// portrait format
			$nScale = floor(140 * 100 / $aMedia['height']);	// scale factor if fitted to box height
			$nNewHeight = floor(($aMedia['height'] * $nScale) / 100);	// actual height if fitted to box
			$nNewWidth = floor(($aMedia['width'] * $nScale) / 100);	// actual width if fitted to box height
		}
		$nYPos = floor(((140 - $nNewHeight) / 2)) + 8;
		$sExtra = ' style="margin-top:'.$nYPos.'px"';

		return $oMediadb->getMediaThumb($aMedia['filename'], $sExtra, 'mdb_upload', $nNewWidth, 100, true);
	}

?>