<?php

 	$aENV['address']['format']	= "{COMPANY}\n{TITLE}{FIRSTNAME}{SURNAME}\n{STREET}\n{TOWN}\n{COUNTRY}";

 	// if false email string in format "Firstname Surname <email@server.tld>" or "<email@server.tld>"
 	$bEmailStringShort			= false;

   // Postsendungen - PDF Druck
	$aFormat['font']			= "metanormal";
	$aFormat['fontabwidth']		= 6;
	$aFormat['fontemwidth']		= 9;

	$aFormat[0]['name']			= 'Zweckform 4781';
	$aFormat[0]['intname']		= '4781';
	$aFormat[0]['width']		= '97';
	$aFormat[0]['height']		= '42.3';
	$aFormat[0]['img']			= '4781.gif';
	$aFormat[0]['cols']			= 2;
	$aFormat[0]['rows']			= 6;
	$aFormat[0]['margintop']	= 21.5;
	$aFormat[0]['marginleft']	= 8;
	$aFormat[0]['marginright']	= 8;
	$aFormat[0]['marginmiddle']	= 0;
	$aFormat[0]['druckrandleft']= 7;
	$aFormat[0]['druckrandtop']	= 7;
	$aFormat[0]['maxwidth']		= 42;

	$aFormat[1]['name']			= 'Zweckform 4743';
	$aFormat[1]['intname']		= '4743';
	$aFormat[1]['width']		= '99.1';
	$aFormat[1]['height']		= '42.3';
	$aFormat[1]['img']			= '4743.gif';
	$aFormat[1]['cols']			= 2;
	$aFormat[1]['rows']			= 6;
	$aFormat[1]['margintop']	= 21.4;
	$aFormat[1]['marginleft']	= 4.7;
	$aFormat[1]['marginright']	= 4.7;
	$aFormat[1]['marginmiddle']	= 2.5;
	$aFormat[1]['druckrandleft']= 7;
	$aFormat[1]['druckrandtop']	= 7;
	$aFormat[1]['maxwidth']		= 42;

	$aFormat[2]['name']			= 'Zweckform 3490';
	$aFormat[2]['intname']		= '3490';
	$aFormat[2]['width']		= 70;
	$aFormat[2]['height']		= 36;
	$aFormat[2]['img']			= '3490.gif';
	$aFormat[2]['cols']			= 3;
	$aFormat[2]['rows']			= 8;
	$aFormat[2]['margintop']	= 4.43;
	$aFormat[2]['marginleft']	= 0;
	$aFormat[2]['marginright']	= 0;
	$aFormat[2]['marginmiddle']	= 0;
	$aFormat[2]['druckrandleft']= 5;
	$aFormat[2]['druckrandtop']	= 3;
	$aFormat[2]['maxwidth']		= 42;

	$aFormat[3]['name']			= 'Avery L7173';
	$aFormat[3]['intname']		= '7173';
	$aFormat[3]['width']		= '99.1';
	$aFormat[3]['height']		= '57';
	$aFormat[3]['img']			= '7173.gif';
	$aFormat[3]['cols']			= 2;
	$aFormat[3]['rows']			= 5;
	$aFormat[3]['margintop']	= 6;
	$aFormat[3]['marginleft']	= 4.7;
	$aFormat[3]['marginright']	= 4.7;
	$aFormat[3]['marginmiddle']	= 2.5;
	$aFormat[3]['druckrandleft']= 7;
	$aFormat[3]['druckrandtop']	= 7;
	$aFormat[3]['maxwidth']		= 42;

	$aFormat[4]['name']			= 'Avery 4782';
	$aFormat[4]['intname']		= '4782';
	$aFormat[4]['width']		= 99;
	$aFormat[4]['height']		= 67.5;
	$aFormat[4]['img']			= '4782.gif';
	$aFormat[4]['cols']			= 2;
	$aFormat[4]['rows']			= 4;
	$aFormat[4]['margintop']	= 12;
	$aFormat[4]['marginleft']	= 5;
	$aFormat[4]['marginright']	= 5;
	$aFormat[4]['marginmiddle']	= 2.5;
	$aFormat[4]['druckrandleft']= 7;
	$aFormat[4]['druckrandtop']	= 7;
	$aFormat[4]['maxwidth']		= 42;

// E-Mail Benachrichtigung

$aAdr['sendMailText']['de'] = 'Vielen Dank für ihre Anmeldung {NAME_EMPF}.

Sie können sich jetzt mit Ihrem Login "{LOGIN_EMPF}" und dem Passwort "{PASS_EMPF}" auf unserer Webseite einloggen.

Mit freundlichen Grüßen,
Das Design Aspekt Team
_______________________________
Design Aspekt
Schwanthalerstraße 76
D 80336 München

T  +49 (0)89 53 07 29.0
F  +49 (0)89 53 07 29.29
E  info@design-aspekt.com

www.design-aspekt.com
_______________________________

';
	$aAdr['cugonlinesubject']['de'] = 'Ihre Zugangsdaten / www.design-aspekt.com';


$aAdr['sendMailText']['en'] = 'Thanks for your registration {NAME_EMPF}.

You can login now on our website with the username "{LOGIN_EMPF}" and password "{PASS_EMPF}".

Kind regards
The Design Aspekt team
_______________________________
Design Aspekt
Schwanthalerstrasse 76
D 80336 Munich

T  +49 (0)89 53 07 29.0
F  +49 (0)89 53 07 29.29
E  info@design-aspekt.com

www.design-aspekt.com
_______________________________
';
	$aAdr['cugonlinesubject']['en'] = 'Your login data / www.design-aspekt.com';


?>