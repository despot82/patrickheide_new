<?php
/**
* messages.php
*
* stellt projektspezifische woerter/saetze und button-beschriftungen in den unterstuetzten sprachen [de|en] zur verfuegung.
*
* die woerter und saetze sind in gruppen (array-keys) unterteilt [err|form|std|navi|btn|mode]. 
* hier kann man projektspezifisch den bestehenden arrays etwas hinzufuegen oder bestehende ueberschreiben. 
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.0 / 2006-02-06 (in UTF-8 umgewandelt)
* @version	1.0 / 2003-08-11 (von array.messages.php kopiert)
* @see		php/sys_messages.php
*/

	$aMSG['topnavi']['overview']['de'] = "Datenbank";
	$aMSG['topnavi']['overview']['en'] = "Overview";
	
// Web allgemein
	$aMSG['web']['breadcrumbs']['de'] = "Sie sind hier";
	$aMSG['web']['breadcrumbs']['en'] = "You are here";
	$aMSG['web']['back']['de'] = "Zur&uuml;ck";
	$aMSG['web']['back']['en'] = "Back";
	$aMSG['web']['more']['de'] = "...mehr";
	$aMSG['web']['more']['en'] = "...more";
	$aMSG['web']['more_info']['de'] = "Weitere Infos unter:";
	$aMSG['web']['more_info']['en'] = "More info at";
	$aMSG['web']['overview']['de'] = "Übersicht";
	$aMSG['web']['overview']['en'] = "Overview";
	
	
/// Web Formulare
	$aMSG['form']['warntext']['de'] = "Bitte füllen Sie alle Pflichtfelder aus!";
	$aMSG['form']['warntext']['en'] = "Please fill out all mandatory fields!";
	$aMSG['form']['wrong_email']['de'] = "Bitte geben Sie eine korrekte E-Mail-Adresse an!";
	$aMSG['form']['wrong_email']['en'] = "Please enter a correct email address!";
	$aMSG['form']['wrong_country']['de'] = "Bitte geben Sie ein Land an!";
	$aMSG['form']['wrong_country']['en'] = "Please select a country!";	
	$aMSG['form']['email_web']['de'] = "E-mail";
	$aMSG['form']['email_web']['en'] = "Email";
	$aMSG['form']['message']['de'] = "Nachricht";
	$aMSG['form']['message']['en'] = "Message";
	$aMSG['form']['mandatory']['de'] = "Pflichtfelder";
	$aMSG['form']['mandatory']['en'] = "Mandatory fields";
	$aMSG['form']['form_ok']['de'] = "Ihre Anfrage wurde versandt!";
	$aMSG['form']['form_ok']['en'] = "Your request has been sent!";
	$aMSG['form']['form_ko']['de'] = "Beim Versenden Ihrer Anfrage ist ein Fehler aufgetreten!";
	$aMSG['form']['form_ko']['en'] = "While sending your request an error occured!"; 
	$aMSG['form']['form_ok_text']['de'] = "Wir werden Ihre Nachricht so schnell wie möglich bearbeiten.<br /> Vielen Dank für Ihr Interesse";
	$aMSG['form']['form_ok_text']['en'] = "We will answer your request as soon.<br />Thank you very much for your interest.";
	$aMSG['form']['form_ko_text']['de'] = "Bitte versuchen Sie es sp&auml;ter noch einmal.";
	$aMSG['form']['form_ko_text']['en'] = "Please try again later.";
	$aMSG['form']['location']['de'] = "Veranstaltungsort";
	$aMSG['form']['location']['en'] = "Location";
	$aMSG['form']['works']['de'] = "Arbeiten";
	$aMSG['form']['works']['en'] = "Works";
	$aMSG['form']['artistpicture']['de'] = "Künstler Portrait";
	$aMSG['form']['artistpicture']['en'] = "Artist portrait";
	
	$aMSG['form']['webinformation']['de'] = "Website Information";
	$aMSG['form']['webinformation']['en'] = "Website information";
	
	$aMSG['form']['medium']['de'] = "Medium";
	$aMSG['form']['medium']['en'] = "Medium";
	$aMSG['form']['year']['de'] = "Jahr";
	$aMSG['form']['year']['en'] = "Year";
	$aMSG['form']['size']['de'] = "Größe";
	$aMSG['form']['size']['en'] = "Size";
	$aMSG['form']['conditions']['de'] = "Konditionen";
	$aMSG['form']['conditions']['en'] = "Conditions";
	$aMSG['form']['owner']['de'] = "Besitzer";
	$aMSG['form']['owner']['en'] = "Owner";
	$aMSG['form']['price']['de'] = "Preis";
	$aMSG['form']['price']['en'] = "Price";
	
	
// Web Search
	$aMSG['search']['search_term']['de'] = "Suchbegriff";
	$aMSG['search']['search_term']['en'] = "Searchterm";
	$aMSG['search']['warning_searchterm']['de'] = "Bitte geben Sie einen Suchbegriff mit mindestens 3 Zeichen an!";
	$aMSG['search']['warning_searchterm']['en'] = "Please enter a search term with at least 3 characters!";
	$aMSG['search']['search_result']['de'] = "Ihr Suchergebnis f&uuml;r";
	$aMSG['search']['search_result']['en'] = "Your search result for";	
	$aMSG['search']['search_to_short']['de'] = "Ihr Suchwort muss länger als drei Zeichen sein!";
	$aMSG['search']['search_to_short']['en'] = "Your searchterm has to be longer then 3 characters!";
	$aMSG['search']['no_searchresult']['de'] = "Ihre Suche lieferte kein Ergebnis!";
	$aMSG['search']['no_searchresult']['en'] = "Your search returned no results!";
	
// Admin

// Button-Beschriftungen
	$aMSG['btn']['send']['de'] = "Senden";
	$aMSG['btn']['send']['en'] = "Send";
	$aMSG['btn']['search']['de'] = "Suchen";
	$aMSG['btn']['search']['en'] = "Search";

?>