/* custom.php - custom CSS definitions | version 1.0 | date: 2004-01-13 */

/* header */
body					{ background-color:#ffffff; }
.bodyPopup				{ background-image:url(<?php echo $aENV['path']['pix']['http']; ?>onepix.gif); }
body.bodyiframe			{background:0;}

#logo					{ margin-bottom:10px; }

.btn, .textbtn, .smallbut, .calButton		{ background-image:url('<?php echo $aENV['path']['pix']['http']; ?>btn.gif'); }
.btnon, .smallbutDisabled					{ background-image:url('<?php echo $aENV['path']['pix']['http']; ?>btnon.gif'); }
.btnsmall									{ background-image:url('<?php echo $aENV['path']['pix']['http']; ?>btnsmall.gif'); }

#header .smallbut	{ background-image:url('<?php echo $aENV['path']['pix']['http']; ?>onepix.gif'); }

<?php define('HR', '<hr size="1" color="#333336" noshade style="margin-top:4px; margin-bottom:2px">'); ?>

/* tabellen */
<?php if (count($aENV['content_language']) > 2) { // more than one language (+default) ?>
.tabelle .langde		{ background-color:#e0ffb0; }
.tabelle .langen		{ background-color:#ffe4a2; }
<?php } else { ?>
.tabelle .lang<?php echo $syslang; ?>	{ background-color:#ffffff; }
<?php } ?>