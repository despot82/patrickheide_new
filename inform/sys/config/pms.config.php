<?php
/* 
 * @version 1.0 17.01.2007
 * @author Nils Hitze <nh@design-aspekt.com>
 */
// PMS ***********************************************************************

	// Name des/der Presentation-Templates (Flash-Film)
	$aENV['pms']['template'] = array(); // mindestes eines (mit key: 1 - nicht 0!) muss angegeben sein!
	
	$aENV['pms']['template']['1']['filename']		= "template.swf";
	$aENV['pms']['template']['1']['title']			= "Global";
	$aENV['pms']['template']['1']['fullscreenWidth']	= "1024";	// max-width	(fullscreen)
	$aENV['pms']['template']['1']['fullscreenHeight']	= "768";	// max-height	(fullscreen)
	
// available layouts
	// zusaetzliche moeglichkeit JEDES template in max.6(!) versionen (A-F) anzubieten. 
	// diese eintraege werden in "pms_content_detail.php" angezeigt.
	// (auswirkungen muesssen in den jew. flash-templates programmiert werden!)
	// 'title' kann einsprachig (dann ohne sprachkeys ['de'],['en'],...) oder mehrsprachig (dann mit sprachkeys fuer jede content-language) verarbeitet werden!
	// $aENV['pms']['layout']['Intro']['contains'] -> kann sein: "text", "slide", "image", "flash", "fullscreen"
	$aENV['pms']['layout'] = array();
	/* fuer Textausrichtung ist zu beachten, dass dynamische Textfelder in Flash einen Versatz nach unten und rechts haben
	=> vom gewuenschten x-Wert ca. 3px abziehen, vom gewuenschten y-Wert je nach SCHRIFTGROESSE bis zu 12px abziehen 
	*/
	// Layouts fuer Template 1
	$aENV['pms']['layout']['1']['A']['title']['de']	= 'Bild und/oder Titel zentriert';
	$aENV['pms']['layout']['1']['A']['title']['en']	= 'Image and/or title centered';
	$aENV['pms']['layout']['1']['A']['contains']		= array("headline", "slideshow", "image", "fullscreen", "flag_media");
	$aENV['pms']['layout']['1']['A']['headlineConfig']	= "39,335,938,200,center";		// params: left, top, width, height, alignment, 0, 0, textSize[large, default]
	$aENV['pms']['layout']['1']['A']['slideshowConfig']= "0,0";						// params: left, top
	$aENV['pms']['layout']['1']['A']['slideshowWidth']	= "1024";						// max-width	(slideshow)
	$aENV['pms']['layout']['1']['A']['slideshowHeight']= "744";						// max-height	(slideshow)
	$aENV['pms']['layout']['1']['A']['imageConfig']	= "0,0";						// params: left, top
	$aENV['pms']['layout']['1']['A']['imageWidth']		= "1024";						// max-width	(image)
	$aENV['pms']['layout']['1']['A']['imageHeight']	= "744";						// max-height	(image)
	$aENV['pms']['layout']['1']['A']['layoutWidth']	= "1024";						// max-width	(flv)
	$aENV['pms']['layout']['1']['A']['layoutHeight']	= "744";						// max-height	(flv)
	
	$aENV['pms']['layout']['1']['B']['title']['de']	= 'Bild und/oder Fließtext';
	$aENV['pms']['layout']['1']['B']['title']['en']	= 'Image and/or text';
	$aENV['pms']['layout']['1']['B']['contains']		= array("headline", "text", "slideshow", "image", "flag_media");
	$aENV['pms']['layout']['1']['B']['headlineConfig']	= "30,60,750,680,left";			// params: left, top, width, height, alignment, 0, 0, textSize[large, default]
	$aENV['pms']['layout']['1']['B']['textConfig']		= "30,60,750,680,left,slave";	// params: left, top, width, height, alignment, relativeToHeadline[slave,standalone], textSize[large, default]
	$aENV['pms']['layout']['1']['B']['slideshowConfig']= "0,0";						// params: left, top
	$aENV['pms']['layout']['1']['B']['slideshowWidth']	= "1024";						// max-width	(slideshow)
	$aENV['pms']['layout']['1']['B']['slideshowHeight']= "744";						// max-height	(slideshow)
	$aENV['pms']['layout']['1']['B']['imageConfig']	= "0,0";						// params: left, top
	$aENV['pms']['layout']['1']['B']['imageWidth']		= "1024";						// max-width	(image)
	$aENV['pms']['layout']['1']['B']['imageHeight']	= "744";						// max-height	(image)
	$aENV['pms']['layout']['1']['B']['layoutWidth']	= "1024";						// max-width	(flv)
	$aENV['pms']['layout']['1']['B']['layoutHeight']	= "744";						// max-height	(flv)
	
	$aENV['pms']['layout']['1']['C']['title']['de']	= 'Text und kleines Bild links - Bild/Slide rechts';
	$aENV['pms']['layout']['1']['C']['title']['en']	= 'Text and small-image left - image/slide right';
	$aENV['pms']['layout']['1']['C']['contains']		= array("headline", "text", "slideshow", "image");
	$aENV['pms']['layout']['1']['C']['headlineConfig']	= "30,60,392,680,left";			// params: left, top, width, height, alignment, 0, 0, textSize[large, default]
	$aENV['pms']['layout']['1']['C']['textConfig']		= "30,60,392,680,left,slave";	// params: left, top, width, height, alignment, relativeToHeadline[slave,standalone], textSize[large, default]
	$aENV['pms']['layout']['1']['C']['slideshowConfig']= "512,0";						// params: left, top
	$aENV['pms']['layout']['1']['C']['slideshowWidth']	= "512";						// max-width	(slideshow)
	$aENV['pms']['layout']['1']['C']['slideshowHeight']= "744";						// max-height	(slideshow)
	$aENV['pms']['layout']['1']['C']['imageConfig']	= "60,450";						// params: left, top
	$aENV['pms']['layout']['1']['C']['imageWidth']		= "480";						// max-width	(image)
	$aENV['pms']['layout']['1']['C']['imageHeight']	= "240";						// max-height	(image)
	
	$aENV['pms']['layout']['1']['D']['title']['de']	= 'Slide links - Bild rechts';
	$aENV['pms']['layout']['1']['D']['title']['en']	= 'Slide left - still image right';
	$aENV['pms']['layout']['1']['D']['contains']		= array("slideshow", "image");
	$aENV['pms']['layout']['1']['D']['slideshowConfig']= "0,0";						// params: left, top
	$aENV['pms']['layout']['1']['D']['slideshowWidth']	= "512";						// max-width	(slideshow)
	$aENV['pms']['layout']['1']['D']['slideshowHeight']= "744";						// max-height	(slideshow)
	$aENV['pms']['layout']['1']['D']['imageConfig']	= "512,0";						// params: left, top
	$aENV['pms']['layout']['1']['D']['imageWidth']		= "512";						// max-width	(image)
	$aENV['pms']['layout']['1']['D']['imageHeight']	= "744";						// max-height	(image)
	
	$aENV['pms']['layout']['1']['E']['title']['de']	= 'Bild links - Slide rechts';
	$aENV['pms']['layout']['1']['E']['title']['en']	= 'Still image left - slide right';
	$aENV['pms']['layout']['1']['E']['contains']		= array("slideshow", "image");
	$aENV['pms']['layout']['1']['E']['slideshowConfig']= "512,0";						// params: left, top
	$aENV['pms']['layout']['1']['E']['slideshowWidth']	= "512";						// max-width	(slideshow)
	$aENV['pms']['layout']['1']['E']['slideshowHeight']= "744";						// max-height	(slideshow)
	$aENV['pms']['layout']['1']['E']['imageConfig']	= "0,0";						// params: left, top
	$aENV['pms']['layout']['1']['E']['imageWidth']		= "512";						// max-width	(image)
	$aENV['pms']['layout']['1']['E']['imageHeight']	= "744";						// max-height	(image)
	
	$aENV['pms']['layout']['1']['F']['title']['de']	= 'Bild links - Slide rechts';
	$aENV['pms']['layout']['1']['F']['title']['en']	= 'Still image left - slide right';
	$aENV['pms']['layout']['1']['F']['contains']		= array("headline", "multipart");
	$aENV['pms']['layout']['1']['F']['headlineConfig']	= "30,60,392,680,left";			// params: left, top, width, height, alignment, 0, 0, textSize[large, default]
	$aENV['pms']['layout']['1']['F']['textConfig']		= "30,60,392,680,left,slave";	// params: left, top, width, height, alignment, relativeToHeadline[slave,standalone], textSize[large, default]
	$aENV['pms']['layout']['1']['F']['imageConfig']	= "512,0";						// params: left, top
	$aENV['pms']['layout']['1']['F']['imageWidth']		= "512";						// max-width	(image)
	$aENV['pms']['layout']['1']['F']['imageHeight']	= "744";						// max-height	(image)
	
	// Layouts fuer Template 2
	// $aENV['pms']['layout']['2'][...]
	
	// Komprimierungs-Verfahren, das zur Archivierung benutzt wird (moeglich ist "pclzip" "zip" oder "tar")
	$aENV['pms']['archive_type']	= "pclzip";	// [pclzip|zip|tar]	(default: pclzip)
?>
