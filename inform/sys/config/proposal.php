<?php
/**
 * The configuration file for the PROPOSAL pdf in adm 
 * 
 * @version 1.0 17.01.2007
 * @author Nils Hitze <nh@design-aspekt.com>
 */
 
	// initialise array
	$aAdm = array();
	
	/**
	 * global values for the pdf
	 */

	// boolean flag for the getPDFData method of class.adm.php
	// writes iterating numbers for the price block entries
	$aAdm['linenumbers']		= true;
	
	// show the author in the footer line	
	$aAdm['displayfooter']		= false;
	
	// make a seperate firstpage (for proposal)	
	$aAdm['seperatefirstpage']	= true;
	
	// write lines for the sum block	
	$aAdm['bWithLine']			= true;
	
	$aAdm['days']	= $aMSG['adm']['days'];
	$aAdm['hours']	= $aMSG['adm']['hours'];
	
	// initialise master values array	
	$aAdm['master'] = array();
	$aAdm['master']['fontface']	= array();
	
	$aAdm['master']['fontface']['normal']	= 'metanormal';
	$aAdm['master']['fontface']['bold']		= 'metabold';
	$aAdm['master']['fontsize']['title']	= 14;
	$aAdm['master']['fontsize']['text']		= 10;
	$aAdm['master']['lineheight']['title']	= 7;
	$aAdm['master']['lineheight']['text']	= 4.5;
		
	// default text for input fields in adm
	$aAdm['defaulttxt'] = array();
	$aAdm['defaulttxt']['introduction']	= '';
	$aAdm['defaulttxt']['resume']		= '';

	/**
	 * Startpage
	 * 
	 * Format Arrays for the firs page
	 */ 
	
	// initialise array
	$aAdm['startpage'] = array();

	// format template for the tax line	
	$aAdm['startpage']['text']['tax']	= '+ {tax}% {taxMsg}';	

	// startpage header design
	$aAdm['startpage']['header']	= array('paging',
											'date',
											'address',	
											'projectName',
											'projectNumber');

	// startpage content design											
	$aAdm['startpage']['content']	= array('introduction');	

	// startpage margin values
	$aAdm['startpage']['margin'] = array();
	$aAdm['startpage']['margin']['top']		= 100;
	$aAdm['startpage']['margin']['right']	= 65;
	$aAdm['startpage']['margin']['bottom']	= 45;
	$aAdm['startpage']['margin']['left']	= 25;
	
	$aAdm['startpage']['fontsize'] = array();
	$aAdm['startpage']['fontsize']['title']	= '';
	$aAdm['startpage']['fontsize']['text']	= '';
	
	$aAdm['startpage']['lineheight'] = array();
	$aAdm['startpage']['lineheight']['title']	= '';
	$aAdm['startpage']['lineheight']['text']	= '';
		
	/**
	 * Format Arrays for the textblocks in header  + content
	 */		
	 
	// project Number block
	$aAdm['startpage']['projectNumber'] = array();
	$aAdm['startpage']['projectNumber']['display']		= true;
	$aAdm['startpage']['projectNumber']['align']		= '';
	$aAdm['startpage']['projectNumber']['lineheight']	= $aAdm['master']['lineheight']['title'];
	$aAdm['startpage']['projectNumber']['fontsize']		= $aAdm['master']['fontsize']['title'];
	$aAdm['startpage']['projectNumber']['fontweight']	= '';
	$aAdm['startpage']['projectNumber']['prefix']		= $aMSG['adm']['proposal'][$syslang].' ';
	$aAdm['startpage']['projectNumber']['suffix']		= '';
	$aAdm['startpage']['projectNumber']['x']			= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['projectNumber']['y']			= $aAdm['startpage']['margin']['top'];
	// format for project number
	$aAdm['startpage']['projectNumber']['format']		= '{clientNumber}.{projectNumber}'; 

	// project name block
	$aAdm['startpage']['projectName'] = array();
	$aAdm['startpage']['projectName']['display']	= true;
	$aAdm['startpage']['projectName']['align']		= '';
	$aAdm['startpage']['projectName']['lineheight']	= $aAdm['master']['lineheight']['title'];
	$aAdm['startpage']['projectName']['fontsize']	= $aAdm['master']['fontsize']['title'];
	$aAdm['startpage']['projectName']['fontweight']	= 'bold';
	$aAdm['startpage']['projectName']['prefix']		= '';
	$aAdm['startpage']['projectName']['suffix']		= '';
	$aAdm['startpage']['projectName']['x']			= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['projectName']['y']			= $aAdm['startpage']['margin']['top']+$aAdm['master']['lineheight']['title']; 
	
	// address block
	$aAdm['startpage']['address'] = array();
	$aAdm['startpage']['address']['display']	= true;
	$aAdm['startpage']['address']['align']		= '';
	$aAdm['startpage']['address']['lineheight']	= $aAdm['master']['lineheight']['text'];
	$aAdm['startpage']['address']['fontsize']	= $aAdm['master']['fontsize']['text'];
	$aAdm['startpage']['address']['fontweight']	= 'normal';
	$aAdm['startpage']['address']['prefix']		= '';
	$aAdm['startpage']['address']['suffix']		= ''; 
	$aAdm['startpage']['address']['x']			= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['address']['y']			= 48;
	
	// date block
	$aAdm['startpage']['date'] = array();
	$aAdm['startpage']['date']['display']		= true;
	$aAdm['startpage']['date']['align']			= 'L';
	$aAdm['startpage']['date']['lineheight']	= $aAdm['master']['lineheight']['text'];
	$aAdm['startpage']['date']['fontsize']		= $aAdm['master']['fontsize']['text'];
	$aAdm['startpage']['date']['fontweight']	= '';
	$aAdm['startpage']['date']['prefix']		= $aMSG['adm']['proposaldate'][$syslang];
	$aAdm['startpage']['date']['suffix']		= ''; 
	$aAdm['startpage']['date']['x']				= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['date']['y']				= 260; 
	// format method for date
	$aAdm['startpage']['date']['format']		= 'short';
	
	// paging block
	$aAdm['startpage']['paging'] = array();
	$aAdm['startpage']['paging']['display']		= false;
	$aAdm['startpage']['paging']['align']		= 'R';
	$aAdm['startpage']['paging']['lineheight']	= $aAdm['master']['lineheight']['title'];
	$aAdm['startpage']['paging']['fontsize']	= '';
	$aAdm['startpage']['paging']['fontweight']	= 'bold';
	$aAdm['startpage']['paging']['prefix']		= '';
	$aAdm['startpage']['paging']['suffix']		= ''; 
	$aAdm['startpage']['paging']['x']			= 100;
	$aAdm['startpage']['paging']['y']			= 200; 

	// introduction block
	$aAdm['startpage']['introduction'] = array();
	$aAdm['startpage']['introduction']['display']		= true;
	$aAdm['startpage']['introduction']['method']		= 'getIntroduction';
	$aAdm['startpage']['introduction']['align']			= '';
	$aAdm['startpage']['introduction']['lineheight']	= $aAdm['master']['lineheight']['text'];
	$aAdm['startpage']['introduction']['fontsize']		= $aAdm['startpage']['fontsize']['text'];
	$aAdm['startpage']['introduction']['fontweight']	= 'normal';
	$aAdm['startpage']['introduction']['prefix']		= '';
	$aAdm['startpage']['introduction']['suffix']		= '';
	$aAdm['startpage']['introduction']['x']				= $aAdm['startpage']['margin']['left']; //recommend!
	$aAdm['startpage']['introduction']['y']				= 120; 

	// price block
	$aAdm['startpage']['priceBlock']	= array();
	$aAdm['startpage']['priceBlock']['display']		= true;
	$aAdm['startpage']['priceBlock']['align']		= '';
	$aAdm['startpage']['priceBlock']['lineheight']	= $aAdm['master']['lineheight']['text'];
	$aAdm['startpage']['priceBlock']['fontsize']	= $aAdm['master']['fontsize']['text'];
	$aAdm['startpage']['priceBlock']['fontweight']	= 'normal';
	$aAdm['startpage']['priceBlock']['prefix']		= '';
	$aAdm['startpage']['priceBlock']['suffix']		= '';
	$aAdm['startpage']['priceBlock']['x']			= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['priceBlock']['y']			= '';
	// method for the price block entries
	$aAdm['startpage']['priceBlock']['method'] 		= 'getPriceBlock';
		
	// sum block	
	$aAdm['startpage']['total']	= array();
	$aAdm['startpage']['total']['display']		= true;
	$aAdm['startpage']['total']['method']		= 'getSum';
	$aAdm['startpage']['total']['align']		= '';
	$aAdm['startpage']['total']['lineheight']	= '';
	$aAdm['startpage']['total']['fontsize']		= '';
	$aAdm['startpage']['total']['fontweight']	= '';
	$aAdm['startpage']['total']['prefix']		= '';
	$aAdm['startpage']['total']['suffix']		= '';
	$aAdm['startpage']['total']['x']			= $aAdm['startpage']['margin']['left']; //recommend!
	$aAdm['startpage']['total']['y'] 			= '';	 	

	// header image
	$aAdm['startpage']['headerImg'] = array();
	$aAdm['startpage']['headerImg']['x']		= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['headerImg']['y']		= 0;
	$aAdm['startpage']['headerImg']['height']	= 27.1;
	$aAdm['startpage']['headerImg']['width']	= 173.7; 
	$aAdm['startpage']['headerImg']['url']		= $aENV['path']['config']['unix'].'hd.png';
	
	// footer image
	$aAdm['startpage']['footerImg'] = array();
	$aAdm['startpage']['footerImg']['x']		= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['footerImg']['y']		= 260;
	$aAdm['startpage']['footerImg']['height']	= 27.1;
	$aAdm['startpage']['footerImg']['width']	= 173.7; 
	$aAdm['startpage']['footerImg']['url']		= '';
	
	// footer block
	$aAdm['startpage']['footer'] = array();
	$aAdm['startpage']['footer']['x']	= $aAdm['startpage']['margin']['left'];
	$aAdm['startpage']['footer']['y']	= 255;	
	
	/**
	 * Followpage
	 * 
	 * Format array for all following pages
	 */
	
	// initialise array
	$aAdm['followpage']	= array();
	
	$aAdm['followpage']['header']	= array('paging',
											'date',
											'projectName',
											'projectNumber');
												
	$aAdm['followpage']['content']	= array('headline',
											'priceBlock',
											'total',
											'resume');	
	
	// margin for the followpage
	$aAdm['followpage']['margin']	= array();
	$aAdm['followpage']['margin']['top']	= 48;
	$aAdm['followpage']['margin']['right']	= 65;
	$aAdm['followpage']['margin']['bottom']	= 20;
	$aAdm['followpage']['margin']['left']	= 25;
		
	$aAdm['followpage']['fontsize']	= array();
	$aAdm['followpage']['fontsize']['title']	= '';
	$aAdm['followpage']['fontsize']['text']		= '';
	
	$aAdm['followpage']['lineheight'] = array();
	$aAdm['followpage']['lineheight']['title']	= '';
	$aAdm['followpage']['lineheight']['text']	= '';
	
	// date block
	$aAdm['followpage']['date'] = array();
	$aAdm['followpage']['date']['display']		= true;
	$aAdm['followpage']['date']['align']		= 'L';
	$aAdm['followpage']['date']['lineheight']	= '';
	$aAdm['followpage']['date']['fontsize']		= $aAdm['master']['fontsize']['text'];
	$aAdm['followpage']['date']['fontweight']	= '';
	$aAdm['followpage']['date']['prefix']		= $aMSG['adm']['proposaldate'][$syslang];
	$aAdm['followpage']['date']['suffix']		= '';
	$aAdm['followpage']['date']['x']			= $aAdm['followpage']['margin']['left'];	
	$aAdm['followpage']['date']['y']			= 260;
	// format for date
	$aAdm['followpage']['date']['format'] = 'short'; 
	
	// project number block	
	$aAdm['followpage']['projectNumber'] = array();
	$aAdm['followpage']['projectNumber']['display']		= false;
	$aAdm['followpage']['projectNumber']['align']		= '';
	$aAdm['followpage']['projectNumber']['lineheight']	= '';
	$aAdm['followpage']['projectNumber']['fontsize']	= 7;
	$aAdm['followpage']['projectNumber']['fontweight']	= 'bold';
	$aAdm['followpage']['projectNumber']['prefix']		= 'Nr. ';
	$aAdm['followpage']['projectNumber']['suffix']		= "\n";
	$aAdm['followpage']['projectNumber']['x']			= $aAdm['followpage']['margin']['left'];
	$aAdm['followpage']['projectNumber']['y']			= 200;
	// format project number 
	$aAdm['followpage']['projectNumber']['format']		= '{clientNumber}.{projectNumber}';
	
	// paging block
	$aAdm['followpage']['paging'] = array();
	$aAdm['followpage']['paging']['display'] = true;
	$aAdm['followpage']['paging']['align'] = ''; // hier war ein doppelter Wert. TODO: Testen ob das Align stimmt!
	$aAdm['followpage']['paging']['lineheight'] = '';
	$aAdm['followpage']['paging']['fontsize'] = '';
	$aAdm['followpage']['paging']['fontweight'] = '';
	$aAdm['followpage']['paging']['prefix'] = $aMSG['adm']['site'][$syslang].' ';
	$aAdm['followpage']['paging']['suffix'] = '';
	$aAdm['followpage']['paging']['x'] = $aAdm['followpage']['margin']['left'];
	$aAdm['followpage']['paging']['y'] = 265; 
	
	// headline block
	$aAdm['followpage']['headline'] = array();
	$aAdm['followpage']['headline']['display']		= true;
	$aAdm['followpage']['headline']['align']		= ''; 
	$aAdm['followpage']['headline']['lineheight']	= '';
	$aAdm['followpage']['headline']['fontsize']		= $aAdm['master']['fontsize']['title'];
	$aAdm['followpage']['headline']['fontweight']	= 'bold';
	$aAdm['followpage']['headline']['prefix']		=  $aMSG['adm']['proposaldet'][$syslang];
	$aAdm['followpage']['headline']['suffix']		= '';
	$aAdm['followpage']['headline']['x']			= $aAdm['followpage']['margin']['left'];
	$aAdm['followpage']['headline']['y']			= ''; 
	
	// price block
	$aAdm['followpage']['priceBlock']	= array();
	$aAdm['followpage']['priceBlock']['display']	= true;
	$aAdm['followpage']['priceBlock']['method']		= 'getPriceBlock';
	$aAdm['followpage']['priceBlock']['align']		= '';
	$aAdm['followpage']['priceBlock']['lineheight']	= $aAdm['master']['lineheight']['text'];
	$aAdm['followpage']['priceBlock']['fontsize']	= $aAdm['master']['fontsize']['text'];
	$aAdm['followpage']['priceBlock']['fontweight']	= 'normal';
	$aAdm['followpage']['priceBlock']['prefix']		= '';
	$aAdm['followpage']['priceBlock']['suffix']		= '';
	$aAdm['followpage']['priceBlock']['x']			= $aAdm['followpage']['margin']['left'];
	$aAdm['followpage']['priceBlock']['y']			= '';
	
	$aAdm['followpage']['priceBlock']['y_spacing']	= $aAdm['master']['lineheight']['text'];	
	
	// sum block
	$aAdm['followpage']['total']	= array();
	$aAdm['followpage']['total']['display']		= true;
	$aAdm['followpage']['total']['method']		= 'getSum';
	$aAdm['followpage']['total']['align']		= '';
	$aAdm['followpage']['total']['lineheight']	= '';
	$aAdm['followpage']['total']['fontsize']	= '';
	$aAdm['followpage']['total']['fontweight']	= '';
	$aAdm['followpage']['total']['prefix']		= '';
	$aAdm['followpage']['total']['suffix']		= '';
	$aAdm['followpage']['total']['x']			= $aAdm['followpage']['margin']['left']; //recommend!
	$aAdm['followpage']['total']['y'] 			= '';
		 	
	$aAdm['followpage']['total']['y_spacing']	= 2*$aAdm['master']['lineheight']['text'];
		
	// resume block	
	$aAdm['followpage']['resume'] = array();
	$aAdm['followpage']['resume']['display']	= true;
	$aAdm['followpage']['resume']['align']		= '';
	$aAdm['followpage']['resume']['lineheight']	= '';
	$aAdm['followpage']['resume']['fontsize']	= $aAdm['master']['fontsize']['text'];
	$aAdm['followpage']['resume']['fontweight'] = 'normal';
	$aAdm['followpage']['resume']['prefix']		= '';
	$aAdm['followpage']['resume']['suffix']		= ''; 
	$aAdm['followpage']['resume']['x']			= $aAdm['followpage']['margin']['left']; //recommend!
	$aAdm['followpage']['resume']['y']			= '';
	
	// header image
	$aAdm['followpage']['headerImg'] = array();
	$aAdm['followpage']['headerImg']['x']		= $aAdm['followpage']['margin']['left'];
	$aAdm['followpage']['headerImg']['y']		= 0;
	$aAdm['followpage']['headerImg']['height']	= 27.1;
	$aAdm['followpage']['headerImg']['width']	= 173.7;
	$aAdm['followpage']['headerImg']['url']		= $aENV['path']['config']['unix'].'hd.png';
	
	// footer image
	$aAdm['followpage']['footerImg'] = array();
	$aAdm['followpage']['footerImg']['x']		= $aAdm['followpage']['margin']['left'];
	$aAdm['followpage']['footerImg']['y']		= 260;
	$aAdm['followpage']['footerImg']['height']	= 27.1;
	$aAdm['followpage']['footerImg']['width']	= 173.7;
	$aAdm['followpage']['footerImg']['url']		= ''; 
	
	$aAdm['followpage']['footer']	= array();
	$aAdm['followpage']['footer']['x']	= $aAdm['followpage']['margin']['left'];
	$aAdm['followpage']['footer']['y']	= 255;		
?>
