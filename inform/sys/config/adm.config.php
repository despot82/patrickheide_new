<?php
/* 
 * @version 1.0 17.01.2007
 * @author Nils Hitze <nh@design-aspekt.com>
 */
// ADM ***********************************************************************

	// Stundensaetze fuer JobCat Im Angebot anzeigen
	$aENV['admhourrates']	= true;
	
	$aPrjStatus = array();
	// wird in folgenden Seiten verwendet:
	// 'adm_portal.php', 'adm_prj.php', 'adm_projects_detail.php'
	
	$aPrjStatus['de'][0]	= "Angebot";
	$aPrjStatus['en'][0]	= "Proposal";
	$aPrjStatus['de'][1]	= "Laufendes Projekt";
	$aPrjStatus['en'][1]	= "Current project";
	$aPrjStatus['de'][2]	= "Abgeschlossen";
	$aPrjStatus['en'][2]	= "Finished";
	$aPrjStatus['de'][3]	= "Absage";
	$aPrjStatus['en'][3]	= "Declined";
	
	$aJobCat = array();
	$aJobPrice = array();
	// wird in folgenden Seiten verwendet:
	// 'adm_timesheet.php', 'adm_expense.php'
	$aJobCat['de'][0]	= "Konzept";
	$aJobCat['en'][0]	= "Concept";
	$aJobPrice['euro'][0]	= "100";
	$aJobPrice['pound'][0]	= "70";
	
	$aJobCat['de'][1]	= "Design";
	$aJobCat['en'][1]	= "Design";
	$aJobPrice['euro'][1]	= "100";
	$aJobPrice['pound'][1]	= "70";
	
	$aJobCat['de'][2]	= "Programmierung";
	$aJobCat['en'][2]	= "Programming";
	$aJobPrice['euro'][2]	= "100";
	$aJobPrice['pound'][2]	= "70";
	
	$aJobCat['de'][3]	= "Projektmanagement";
	$aJobCat['en'][3]	= "Project management";
	$aJobPrice['euro'][3]	= "100";
	$aJobPrice['pound'][3]	= "70";
	
	$aJobCat['de'][9]	= "Besprechung";
	$aJobCat['en'][9]	= "Project meeting";
	$aJobPrice['euro'][9]	= "100";
	$aJobPrice['pound'][9]	= "70";
	
	$aJobCat['de'][4]	= "Recherche";
	$aJobCat['en'][4]	= "Research";
	$aJobPrice['euro'][4]	= "100";
	$aJobPrice['pound'][4]	= "70";
	
	$aJobCat['de'][5]	= "Umsetzung";
	$aJobCat['en'][5]	= "Realisation";
	$aJobPrice['euro'][5]	= "100";
	$aJobPrice['pound'][5]	= "70";
	
	$aJobCat['de'][6]	= "Support";
	$aJobCat['en'][6]	= "Support";
	$aJobPrice['euro'][6]	= "80";
	$aJobPrice['pound'][6]	= "55";
	
	$aJobCat['de'][7]	= "Sonstige";
	$aJobCat['en'][7]	= "Others";
	$aJobPrice['euro'][7]	= "100";
	$aJobPrice['pound'][7]	= "70";
	
	$aJobCat['de'][8]	= "System-Installation";
	$aJobCat['en'][8]	= "Installation";
	$aJobPrice['euro'][8]	= "100";
	$aJobPrice['pound'][8]	= "70";
	
	$aExpenseCat = array();
	// wird in folgenden Seiten verwendet:
	// 'adm_expense.php'
	$aExpenseCat['de'][0]	= "Projekt";
	$aExpenseCat['en'][0]	= "Project";
	$aExpenseCat['de'][1]	= "Druck, Produktion";
	$aExpenseCat['en'][1]	= "Print, Production";
	$aExpenseCat['de'][2]	= "Fotos, Illustrationen, Film";
	$aExpenseCat['en'][2]	= "Pictures, Illustrations, Film";
	$aExpenseCat['de'][3]	= "Design";
	$aExpenseCat['en'][3]	= "Design";
	$aExpenseCat['de'][4]	= "Programmierung";
	$aExpenseCat['en'][4]	= "Programming";
	$aExpenseCat['de'][5]	= "Text, Übersetzung";
	$aExpenseCat['en'][5]	= "Text, Translation";
	$aExpenseCat['de'][6]	= "Post, Kurier, Transport";
	$aExpenseCat['en'][6]	= "Post, Courier, Transport";
	$aExpenseCat['de'][7]	= "Sonstige";
	$aExpenseCat['en'][7]	= "Other";
		
	
	$sExpenseExtraCharge	= 10; // Aufschlag in Prozent fuer externe Kosten
	
	$aJobDescription = array();
	// Standardtexte fuer angebotserstellung
	$aJobDescription['de'][0] = "";
	$aJobDescription['en'][0] = "";
	$aJobDescription['de'][1] = "Entwicklung des Screendesigns zur Umsetzung von ... im Rahmen des Corporate Designs und anhand des Konzeptes vom ... ";
	$aJobDescription['en'][1] = "";
	$aJobDescription['de'][2] = "Umsetzung und Programmierung ... in HTML/PHP/Javascript; Anpassung des Seitendesigns an browserspezifische Besonderheiten, Optimierung aller grafischen Elemente";
	$aJobDescription['en'][2] = "";
	$aJobDescription['de'][3] = "Projektmanagement/Zeitplanung; Kommunikation/Koordination und Qualitätssicherung; Testing und Debugging";
	$aJobDescription['en'][3] = "";
	$aJobDescription['de'][4] = "";
	$aJobDescription['en'][4] = "";
	$aJobDescription['de'][5] = "Umsetzung, Bildbearbeitung, Pflege, Texterfassung";
	$aJobDescription['en'][5] = "";
	$aJobDescription['de'][6] = "Projektunterstützung und Support";
	$aJobDescription['en'][6] = "";
	$aJobDescription['de'][7] = "";
	$aJobDescription['en'][7] = "";
	$aJobDescription['de'][8] = "";
	$aJobDescription['en'][8] = "";
	
	$aCurrency = array();
	// wird in folgenden Seiten verwendet:
	// 'adm_expense.php'
	$aCurrency['de']['euro']	= "&euro;";
	$aCurrency['en']['euro']	= "&euro;";
	$aCurrency['de']['pound']	= "&pound;";
	$aCurrency['en']['pound']	= "&pound;";

	$aTax = array();
	// wird in folgenden Seiten verwendet:
	// 'adm_expense.php'
	$aTax['default']	= '19'; // key!
	$aTax['de']['19']	= "19%";
	$aTax['en']['19']	= "19%";
	$aTax['de']['16']	= "16%";
	$aTax['en']['16']	= "16%";
	$aTax['de']['7']	= "7%";
	$aTax['en']['7']	= "7%";
	$aTax['de']['17.5']	= "17.5%";
	$aTax['en']['17.5']	= "17.5%";
	$aTax['de']['-']	= "MwSt. frei";
	$aTax['en']['-']	= "Tax free";
	
	$aQuantity = array();
	// wird in folgenden Seiten verwendet:
	// 'adm_expense.php'
	$aQuantity['de']['day']		= "MT";
	$aQuantity['en']['day']		= "Days";
	$aQuantity['de']['hour']	= "Std.";
	$aQuantity['en']['hour']	= "Hrs.";
?>
