<?php
/* 
 * @version 1.0 17.01.2007
 * @author Nils Hitze <nh@design-aspekt.com>
 */
	//aktiviert die Kalendarwochen!
	$aENV['cal']['cwweeks'] = true;
	$aENV['cal']['startview'] = "week";

	$aScheduleLabel = array();
	// wird in folgenden Seiten verwendet:
	// 'cal_calendar_detail.php'
	$aScheduleLabel['de']['business']	= "Business";
	$aScheduleLabel['en']['business']	= "Business";
	$aScheduleLabel['de']['private']	= "Privat";
	$aScheduleLabel['en']['private']	= "Private";
	$aScheduleLabel['de']['call']		= "Anruf";
	$aScheduleLabel['en']['call']		= "Call";
	$aScheduleLabel['de']['leave']		= "Urlaub";
	$aScheduleLabel['en']['leave']		= "Holiday";
	$aScheduleLabel['de']['sick']		= "Krank";
	$aScheduleLabel['en']['sick']		= "Sick";
	$aScheduleLabel['de']['holiday']	= "Feiertag";
	$aScheduleLabel['en']['holiday']	= "Bank holiday";
	$aScheduleLabel['de']['lieu']		= "Springertag";
	$aScheduleLabel['en']['lieu']		= "Lieu day";
	
	$aCalConfig = array();
	$aCalConfig['daytime_start']	= '09'; // Sprungmarke im time-grid (view=day)
	$aCalConfig['grid_td_height']	= '20'; // Hoehe einer halben Stunde im time-grid (view=day)
	$aCalConfig['grid_box_width']	= '22'; // Maximale Breite einer Termine-Box in Prozent im time-grid (view=day)
?>
