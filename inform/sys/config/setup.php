<?php
/**
* setup.php
*
* definiert alle fuer dieses System relevanten Vars (ueberschreibt alle defaults in "_include_all.php")!
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.1 / 2004-04-22
* @see		php/_include_all.php
*/

#-----------------------------------------------------------------------------
# 1a. global environment VARS (customer-specific)
#-----------------------------------------------------------------------------

// standart title
	$aENV['client_name']		= "Patrick Heide"; // used for title/etc.
	$aENV['client_shortname']	= "heide"; // used for db-name/session-name/etc. (NO spaces etc!)
	// -> sollte ggf. der selbe wie beim CMS/PMS/... sein (=> fuer nur EINmal einloggen muessen...)
	$aENV['web_url']			= "www.patrickheide.com"; // URL der mit diesem CMS verwalteten Website (ohne "http://")

// DB-zugangsdaten
	$aENV['db'] = array(); // (wird von "class.db_mysql.php" erwartet!)
	switch (true) {

		case ($_SERVER['SERVER_NAME'] == "rhsrv2" 
		|| $_SERVER['SERVER_NAME'] == "10.1.1.3"):	// Dev-Server
			$aENV['db']['host'] 	= "localhost";
			$aENV['db']['db']   	= "210_PatrickHeide";
			$aENV['db']['user'] 	= "pma_admin";
			$aENV['db']['pass'] 	= "pma";
			/* SECURE SERVER ggf. hier einstellen! ****************************************************************/
			$aENV['https']['inform']	= false; // [true|false] (bei true wird unterhalb von "/inform/" allen http-pfaden "https://" vorangestellt)
			$aENV['https']['web']		= false; // [true|false] (bei true wird i.d. Webausgabe allen http-pfaden "https://" vorangestellt)
			/******************************************************************************************************/
			break;
		case $_SERVER['SERVER_NAME'] == "www.design-aspekt.com" : 
			$aENV['db']['host'] 	= "dbhost:3306";
			$aENV['db']['db']   	= "vs2-e3"; // <--
			$aENV['db']['user'] 	= "vs2-e";
			$aENV['db']['pass'] 	= "vi0Choj";
			/* SECURE SERVER ggf. hier einstellen! ****************************************************************/
			$aENV['https']['inform']	= false; // [true|false] (bei true wird unterhalb von "/inform/" allen http-pfaden "https://" vorangestellt)
			$aENV['https']['web']		= false; // [true|false] (bei true wird i.d. Webausgabe allen http-pfaden "https://" vorangestellt)
			/******************************************************************************************************/
			break;
		default:
			$aENV['db']['host'] 	= "192.168.33.33";
			$aENV['db']['db']   	= "patrickheide"; // <--
			$aENV['db']['user'] 	= "root";
			$aENV['db']['pass'] 	= "root";
			/* SECURE SERVER ggf. hier einstellen! ****************************************************************/
			$aENV['https']['inform']	= true; // [true|false] (bei true wird unterhalb von "/inform/" allen http-pfaden "https://" vorangestellt)
			$aENV['https']['web']		= false; // [true|false] (bei true wird i.d. Webausgabe allen http-pfaden "https://" vorangestellt)
			/******************************************************************************************************/
			error_reporting(0); // no error-messages on live-systems
			#error_reporting(E_ALL ^ E_NOTICE);
			ini_set('error_reporting', 0);
			ini_set('allow_call_time_pass_reference', true);
			ini_set('display_error', false);
			
			break;
	}
	
	if($_GET['debug']=='all'
	|| $_GET['debug']=='php') {
		error_reporting(E_ALL);	
	}

	ini_set('session.bug_compat_42','0');
	
// NOTE: die jeweilige reihenfolge ist auch ausgabe-reihenfolge (bspw. in einem select-field)

// System Name
	$aENV['system_name']['de']	= "Inform";
	$aENV['system_name']['en']	= "Inform";

// System Version
	$aENV['system_version'] 	= "1.9";

// Footer Navipunkt "Copyright" (+ Inform-MiniLogo + Version) einblenden
	#$aENV['system_copyright'] 		= false;	// default true

// Navigation
	$aENV['navigation_open']	= false;

// default system-language
	$aENV['system_language'] = array();
	$aENV['system_language']['default'] = "en"; // set default [ de | en ]
	$aENV['system_language']['de'] = "deutsch";
	$aENV['system_language']['en'] = "english";

// Anzeige-Formatierung der Nutzernamen
	$aENV['username_format'] = "firstname"; // oder z.B. "surname, firstname"... ('firstname' und 'surname' werden durch die entsprechenden values ersetzt)

// available modules
	// diese Eintraege werden in der Rechte Verwaltung als Module (und entsprechend auch in der Topnavi) angezeigt.
	// NOTE: kann einsprachig (dann ohne sprachkeys ['de'],['en'],...) 
	// oder mehrsprachig (dann mit sprachkeys fuer jede system-language) verarbeitet werden!
	$aENV['module']['adr']['de']	= "Adressen";
	$aENV['module']['adr']['en']	= "Addresses";
	$aENV['module']['cal']['de']	= "Kalender";
	$aENV['module']['cal']['en']	= "Diary";
	$aENV['module']['frm']['de']	= "Forum";
	$aENV['module']['frm']['en']	= "Forum";
	$aENV['module']['arc']['de']	= "Archiv";
	$aENV['module']['arc']['en']	= "Archive";
//	$aENV['module']['adm']['de']	= "Office";
//	$aENV['module']['adm']['en']	= "Office";
//	$aENV['module']['pms']['de']	= "Präsentationen";
//	$aENV['module']['pms']['en']	= "Presentations";
	$aENV['module']['cms']['de']	= "Website";
	$aENV['module']['cms']['en']	= "Website";
//	$aENV['module']['clientsites']['de']	= "Clientsites";
//	$aENV['module']['clientsites']['en']	= "Clientsites";
	$aENV['module']['mdb']['de']	= "Media";
	$aENV['module']['mdb']['en']	= "Media";
	
	
// Portal-MODULE, die angezeigt werden sollen (keine Beachtung der Reihenfolge!)
	// initialise array
	$aENV['portal_module']	= array('L' => array(), 'R' => array());

//	$aENV['portal_module']['L'][] = 'adm_prj_jumper';
//	$aENV['portal_module']['L'][] = 'adm_timesheet';
//	$aENV['portal_module']['L'][] = 'cal_monthcalendar';
	$aENV['portal_module']['L'][] = 'frm_recent';
	$aENV['portal_module']['L'][] = 'memo';
	$aENV['portal_module']['R'][] = 'cal_minicalendar';
	$aENV['portal_module']['R'][] = 'cal_events_today';
	$aENV['portal_module']['R'][] = 'cal_birthdays';
//	$aENV['portal_module']['R'][] = 'frm_recent';
	$aENV['portal_module']['R'][] = 'calc_currency';
	$aENV['portal_module']['R'][] = 'calc_measure';

// Forum
	$aENV['forumhomelarge']			= false;
	$aENV['frm']['bWithEditAll']	= true;
	$aENV['frm']['bWithComments']	= true;
	
// Archiv	
	// Auto DB-Dump jeden Tag
	$aENV['bAutoDbDump']		= true;
	
	// Max. Anzahl DB-Dump
	$aENV['MaxDbDump']		= 5;

	// Soll im Archiv der Fileupload für das Editorfeld angezeigt werden?
	$aENV['arc']['bWithFileEditCopy'] = true;

// Mobile
	// Systemnutzer unter MBL einblenden
	$aENV['mbl']['showSystemUser'] = true; 
	

// Konfiguration Flash-Texteditor
	$aTexteditLayout = array();
	$aTexteditLayout["de"] = 
			"0x000000,".	// InterfaceTextColor
			"0xDBDBDF,".	// ToolbarBackground
			"0xFFFFFF,".	// EditorBackground
			"0xF5F5F8,".	// ScrollBackground
			"0x000000,".	// StandardEditorTextColor
			"0xFFFFFF,".	// StandardTemplateTextColor -> ueberschreiben: setzt Standard-Textfarbe im Template
			"0x000000,".	// InvertedTemplateTextColor -> ueberschreiben: setzt invertierte Textfarbe im Template
			"0xC0C0C0";		// Link-Farbe -> muss anders sein als Dark und BrightColor!
	$aTexteditLayout["en"] = 
			"0x000000,".	// InterfaceTextColor
			"0xDBDBDF,".	// ToolbarBackground
			"0xFFFFFF,".	// EditorBackground
			"0xF5F5F8,".	// ScrollBackground
			"0x000000,".	// StandardEditorTextColor
			"0xFFFFFF,".	// StandardTemplateTextColor -> ueberschreiben: setzt Standard-Textfarbe im Template
			"0x000000,".	// InvertedTemplateTextColor -> ueberschreiben: setzt invertierte Textfarbe im Template
			"0xC0C0C0";		// Link-Farbe -> muss anders sein als Dark und BrightColor!
	
	$aTexteditBg = array();
	$aTexteditBg["de"] = "#FFFFFF";
	$aTexteditBg["en"] = "#FFFFFF";
// MDB
	$aSizeFormat = array();
	$aSizeFormat['de'][0]	= "inches";
	$aSizeFormat['en'][0]	= "inches";
	$aSizeFormat['de'][1]	= "cm";
	$aSizeFormat['en'][1]	= "cm";
?>
