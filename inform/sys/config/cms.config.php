<?php
 // CMS ***********************************************************************

// default WEBSITE content-language (website)
	$aENV['content_language'] = array();
	$aENV['content_language']['default'] = "en"; // set default [ de | en ]
	$aENV['content_language']['en'] = "english";
	#$aENV['content_language']['de'] = "deutsch";
	
// web homepage (default: "home.php")
	// auskommentieren, wenn von der "index2.php" direkt zur "home.php" weitergeleitet werden soll.
	// kommentar entfernen, wenn von der "index2.php" zu einer hier spezifizierten speziellen
	// (z.B. frameset-)seite weitergeleitet werden soll.
	$aENV['web_home']['de']	= "home.php"; // name of start-web-page -> needed in "index.php" and for language selection
	$aENV['web_home']['en']	= "home_en.php"; // name of start-web-page -> needed in "index.php" and for language selection
	
// Anzahl + Bezeichnungen fuer die Content-Navigations-Baeume (default)
	$aENV['content_navi'][0]['de'] = "Content-Navigation";
	$aENV['content_navi'][0]['en'] = "Content navigation";
//	$aENV['content_navi'][1]['de'] = "Service-Navigation";
//	$aENV['content_navi'][1]['en'] = "Service navigation";
//	$aENV['content_navi'][2]['de'] = "Service-Navigation 2";
//	$aENV['content_navi'][2]['en'] = "Service navigation 2";
	
// Anzahl moeglicher Navigations-Level bzw. Navigationstiefe (default: 2)
	$aENV['cms_navi_sublevel'] = 2; // [0 = nur HauptNavi | 1 = HauptNavi+SubNavi | 2 = HauptNavi+SubNavi+SubSubNavi ]
	
// Zeitgesteuert Navigationspunkte ein-/auszublenden (default: false)
	$aENV['cms_navi_onlinefromto'] = false; // [ true | false ]
	// default-start-value bei eingeblendeter Zeitsteuerung (default: null [=> '0000-00-00'])
	$aENV['cms_navi_onlinefromto_default_start'] = 'null'; // [ 'null' | 'today' ]
	// Art das Datum auszuwaehlen bei eingeblendeter Zeitsteuerung (default: dropdown [erzeugt '$oForm->date_dropdown()'])
	$aENV['cms_navi_onlinefromto_dateselect'] = 'popup'; // [ 'dropdown' | 'popup' ]

// Datenausgabe
	// true, wenn PHP-Container-Files generiert werden sollen (default: true)
	$aENV['create_php_container']	= true;
	// true, wenn eine XML Struktur generiert werden soll (default: false)
	$aENV['create_xml_structure']	= false;
	
// ein bild einem navigationspunkt zuweisen koennen?
	// (taucht auf "navi_detail.php" auf - oder nicht)
	$aENV['pic_on_navi'] = 'no'; // [ 'all'=alle duerfen (default) | 'da'=nur da_service darf | 'no'=keiner darf ]
//	$aENV['pic_on_navi_type'] = 'mdb'; // [ 'mdb'=bild aus der mediadb zuweisen (default) | 'txt'=textfeld ]
	
// in den meta-angaben eine standard email-adresse eintragen koennen?
//	$aENV['email_im_meta'] = false; // [ true (default) | false ]
	
// Preview-Modus aktivieren
	$aENV['preview'] = false; // [ true (default) | false ]

// CUG (default: false)
	// true, wenn CUG(s) angelegt + Seiten fuer diese geschuetzt werden koennen soll
	$aENV['allow_cug']			= false;

// write CUG protected Pages in Sitemap (default is true)
	$aENV['cug']['writeNotInSitemap']	= false;

// available templates
	// (zusaetzlich zu den basis-templates 'standard','home','static','no_page')
	// bitte mehrsprachig angeben (mit sprachkey(s) ['de'],['en'],...) - fuer jede system-language!
	// ACHTUNG: key ist der Tabellenname ohne das Prefix "web_" (also: Tabellenname="web_article" bedeutet: key="article")!!!
	$aENV['template'] = array();	$aENV['template']['form']['de']			= "Formular";
	$aENV['template']['form']['en']			= "Form";
	$aENV['template']['search']['de']		= "Suche";
	$aENV['template']['search']['en']		= "Search";
	$aENV['template']['artists']['de']		= "Künstler";
	$aENV['template']['artists']['en']		= "Artists";
	$aENV['template']['events']['de']		= "Events";
	$aENV['template']['events']['en']		= "Events";
	$aENV['template']['press']['de']		= "News & Presse";
	$aENV['template']['press']['en']		= "News & Press";

//	$aENV['template']['projectsite']['de']	= "Kundenbereich";
//	$aENV['template']['projectsite']['en']	= "Client area";

// template searchpattern
	// for the global search table you have the option to define, what
	// is saved to text and title. if you want to insert return value
	// of a function please insert function like this [function({param1},{param2_{weblang}})]
	// function has to exist in "inc.cms_functions.php" in cms module directory
	$aENV['searchpattern']['home']['title']		= '{title_{weblang}}';
	$aENV['searchpattern']['home']['text']		= '{keywords}';
	$aENV['searchpattern']['standard']['title']	= '{title_{weblang}}';
	$aENV['searchpattern']['standard']['text']	= '{copytext_{weblang}} {keywords}';
	$aENV['searchpattern']['search']['title']	= '{title_{weblang}}';
	$aENV['searchpattern']['search']['text']	= '{copytext_{weblang}} {keywords}';
	$aENV['searchpattern']['form']['title']		= '{title_{weblang}}';
	$aENV['searchpattern']['form']['text']		= '{copytext_{weblang}} {keywords}';
	$aENV['searchpattern']['events']['title']	= '{title_{weblang}}';
	$aENV['searchpattern']['events']['text']	= '|| {subtitle_{weblang}} || {copytext_{weblang}} {external_link} {keywords}';
	$aENV['searchpattern']['artists']['title']	= '{title_{weblang}}';
	$aENV['searchpattern']['artists']['text']	= '|| {subtitle_{weblang}} || {copytext_{weblang}} {external_link} {keywords}';
	$aENV['searchpattern']['press']['title']	= '{title_{weblang}}';
	$aENV['searchpattern']['press']['text']		= '|| {subtitle_{weblang}} || {copytext_{weblang}} {external_link} {keywords}';
	
// don't allow (visible or invisible) subpages within these templates:
	$aENV['no_sub_templates'] = array('home');

// available layouts
	// zusaetzliche moeglichkeit ein template in max.4(!) versionen (A|B|C|D) anzubieten. 
	// z.b. mit 1 oder 2 spaltigem text, oder die overviewseite nach datum oder prio sortiert,...
	// diese eintraege werden in "navi_detail.php" unterhalb von "template" angezeigt.
	// (auswirkungen muesssen in den jew. templates programmiert werden! z.b. fallunterscheidung bei "ORDER BY",...)
	// kann einsprachig (dann ohne sprachkeys ['de'],['en'],...) oder mehrsprachig (dann mit sprachkeys fuer jede content-language) verarbeitet werden!

	$aENV['layout'] = array();
//	$aENV['layout']['standard']['A']['de']	= "Standard Seite";
//	$aENV['layout']['standard']['A']['en']	= "Standard page";
//	$aENV['layout']['standard']['B']['de']	= "Text rechts";
//	$aENV['layout']['standard']['B']['en']	= "Text right";
	
// templates, die im internal-linkmanager ausgegeben werden sollen
	// (ACHTUNG: mit einer variable zusammengestzte feldbezeichner (z.B.'title_'.$weblang) gehen hier nicht!!!)
	
	$aENV['linkman']['multilang'] = true;
	// der 2. Key ist das TEMPLATE!
	$aENV['int_linkman'] = array();
	$aENV['int_linkman']['events']['fieldname']		= "";
	$aENV['int_linkman']['events']['orderby']		= "date DESC";
	$aENV['int_linkman']['events']['table']			= "web_events";
	$aENV['int_linkman']['artists']['fieldname']	= "";
	$aENV['int_linkman']['artists']['orderby']		= "prio DESC";
	$aENV['int_linkman']['artists']['table']		= "web_artists";
	$aENV['int_linkman']['press']['fieldname']		= "";
	$aENV['int_linkman']['press']['orderby']		= "date DESC";
	$aENV['int_linkman']['press']['table']			= "web_press";
	
// tabellen + felder, die durchsucht werden sollen, wenn ein bild aus der mediendb geloescht wird
	// (ACHTUNG: mit einer variable zusammengestzte feldbezeichner (z.B.'title_'.$weblang) gehen hier nicht!!!)
	// key: tablename, val: fieldname (mehrere kommasepariert)
	
	// Felder in denen nur eine EINZELNE id vorkommt:
	$aENV['check_single_media'][$aENV['table']['cms_standard']]	= "img_id";
	
	// felder in denen MEHRERE kommagetrennte ids vorkommen:
	$aENV['check_multiple_media'][$aENV['table']['cms_standard']]	= "slideshow";

//Sitemap Konfiguration
	$aENV['sitemap']['index']['name']	= 'sitemap.xml';
	$aENV['sitemap']['index']['url']	= 'http://'.$_SERVER['HTTP_HOST'].$aENV['path']['root']['http'].$aENV['sitemap']['index']['name'];	

// Newsletter (default: false)
	// true, wenn Newsletter verwaltet werden koennen sollen
	$aENV['allow_newsletter']	= false;

// FTP (default: false)
	// wenn ein FTP Verzeichnis auf dem Server dem Kunden bekannt ist und er per PopUp auf diese Dateien zugreifen koennen soll
	$aENV['allow_ftp']			= true;
	$aENV['ftp_folder']			= "ftp/"; // Name des FTP-Verzeichnisses (von der HTTP-Server-Root aus gesehen)
	// FTP Popup
	$aENV['ftp']['spacer']		= 34; // Pixelangabe fuer die Spacer bei dem FTP-Datei Browser
	
#-----------------------------------------------------------------------------
# 1c. global special CMS-VARS: (customer-specific)
#-----------------------------------------------------------------------------
	// Newsletter Sende-Adresse
	$sNlSender = "design aspekt <design-aspekt@design-aspekt.com>";

	$aENV['clientsites']['default']['text']	= 'Wilkommen auf Ihrer Kundenwebseite.<BR />Wenn Sie zu unserer Seite zurückgelangen wollen, klicken Sie bitte <A HREF="">hier</A>.';
?>
