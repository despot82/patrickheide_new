<?php
/**
* popup_backup.php
*
* Dieses Popup fuehrt eine DB-Sicherung durch und speichert den Dump auf der Festplatte des Servers ab.
* -> 2sprachig und kopierbar.
*
* Diese Seite wird aus der sys_portal.php aufgerufen
*
* @param	string	$userid		ID des Users
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 / 2006-01-19
*/

// 1. USER	// Diese Seite sollen ALLE sehen duerfen
// 1c. init
	require_once("../sys/php/_include_all.php");
	// login!

// 2a. GET-params abholen
// 2b. POST-params abholen
// 2c. Vars

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
?>

<div id="contentPopup">

<table width="100%" border="0" cellspacing="1" cellpadding="5" class="tabelle">
<tr>
	<td height="180" align="center"><p><?php echo $aMSG['std']['backup'][$syslang]; ?></p><br>
	<img src="<?php echo $aENV['path']['pix_global']['http']; ?>save_db_dialog.gif" width="67" height="6" alt="" border="0"></td>
</tr>
</table>
	
</body>
</html>
<?php
	// von der Komprimierungsfunktion (_include_all) gecachete Inhalte an den Browser senden
	ob_flush();
	flush();

	// vars
	$bakfolder	= 'backup';
	$idelfile	= $aENV['MaxDbDump'];
	
	// ggf. erstelle backup-folder
	if(!is_dir($aENV['path']['sys_data']['unix'].$bakfolder)) {
		mkdir($aENV['path']['sys_data']['unix'].$bakfolder, 0777);
	}

	// get all files in bakup folder
	$oFolder	=& new FSFolder($aENV['path']['sys_data']['unix'].$bakfolder, $aENV);
	$aFiles		= $oFolder->getFiles();
	$iAnz		= count($aFiles[$bakfolder]);

	if($iAnz >= $idelfile) {
		natsort($aFiles[$bakfolder]);
		$file	= current($aFiles[$bakfolder]);
		// Das x File loeschen | Speicherplatz sparen!
		unlink($aENV['path']['sys_data']['unix'].$bakfolder.'/'.$file);
		$oFolder->initialize();
		$aFiles	= $oFolder->getFiles();
	}
	if(is_array($aFiles[$bakfolder])) {
		natsort($aFiles[$bakfolder]);

		end($aFiles[$bakfolder]);
		$lfilename	= current($aFiles[$bakfolder]);
		// Aufsplitten zum Ermitteln der ID
		$aFile		= explode('_',$lfilename);
	}

	// mache db-dump
	if(!is_file($aENV['path']['sys_data']['unix'].$bakfolder.'/'.($aFile[0]).'_'.date('Ymd').'.sql') || $iAnz == 0) {
		// Die ID erhoehen und dann unter der neuen ID+aktuelles Datum speichern
		$newfilename	= (++$aFile[0]).'_'.date('Ymd').'.sql';

		if(Tools::isExecAvail()) {
			$chk	= $oDb->db_backup_in_file($aENV['path']['sys_data']['unix'].$bakfolder.'/'.$newfilename);
		}
		else {
			$chk	= $oDb->doPhpDump($aENV['path']['sys_data']['unix'].$bakfolder.'/'.$newfilename);
		}
	}

	// Fenster schliessen
	ob_start();
	echo '<script language="javascript">self.close();</script>';
	ob_flush();
	flush();
?>
