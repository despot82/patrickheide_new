<?php
/**
* inc.sys_detnavi.php
* Dieses Include baut die Userverwaltung-Subnavi, gibt sie jedoch nicht aus, sondern als string zurueck!
*
* subnavi-include (nur INTRANET/sys) //-> 2sprachig und voll kopierbar!
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	object	$oPerm		-> kommt aus der 'inc.sys_login.php'
* @param	array	$aBnParts	-> kommt aus der 'inc.sys_header.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-03-04
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.sys_detnavi.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// Subnavi
function getSysUserSubnavi() {
	// VARS
	global $aENV, $oPerm, $aBnParts, $aMSG, $syslang;
	$SUBNAVI		= '';
	$buffer			= array();
	$sLinkTemplate	= '<a href="%s" class="cnavi%s" title="%s">%s</a>';
	$sTrenner		= '&nbsp;&nbsp;|&nbsp;&nbsp;';

// LINKS
	// sys_user
	$hi = ($aBnParts[1] == "user" && $aBnParts[2] != "office" && $aBnParts[2] != "team") ? 'hi' : '';
	$buffer[] = sprintf($sLinkTemplate, $aENV['path']['sys']['http'].'sys_user.php', $hi, $aMSG['topnavi']['active_users'][$syslang], $aMSG['topnavi']['active_users'][$syslang]);
	
	// sys_user_office (nur fuer DA-Service-Zugang!)
	$hi = ($aBnParts[2] == "office") ? 'hi' : '';
	$buffer[] = sprintf($sLinkTemplate, $aENV['path']['sys']['http'].'sys_user_office.php', $hi, $aMSG['topnavi']['offices'][$syslang], $aMSG['topnavi']['offices'][$syslang]);
	
	// sys_user_team
	$hi = ($aBnParts[2] == "team") ? 'hi' : '';
	$buffer[] = sprintf($sLinkTemplate, $aENV['path']['sys']['http'].'sys_user_team.php', $hi, $aMSG['topnavi']['teams'][$syslang], $aMSG['topnavi']['teams'][$syslang]);
	
	if ($oPerm->hasPriv('admin')) {
		// sys_usergroup
		$hi = ($aBnParts[1] == "usergroup") ? 'hi' : '';
		$buffer[] = sprintf($sLinkTemplate, $aENV['path']['sys']['http'].'sys_usergroup.php', $hi, $aMSG['topnavi']['userrights'][$syslang], $aMSG['topnavi']['userrights'][$syslang]);
		
		// sys_userdeleted
		$hi = ($aBnParts[1] == "userdeleted") ? 'hi' : '';
		$buffer[] = sprintf($sLinkTemplate, $aENV['path']['sys']['http'].'sys_userdeleted.php', $hi, $aMSG['topnavi']['deactivated_users'][$syslang], $aMSG['topnavi']['deactivated_users'][$syslang]);
	}
	
	// BUILD SUBNAVI
	if (count($buffer) > 0) {
		$SUBNAVI .= '<span class="cnavi">';
		$SUBNAVI .= implode($sTrenner, $buffer);
		$SUBNAVI .= '<br><img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"><br></span><br>';
	}
	// OUTPUT
	return $SUBNAVI;
}

?>
