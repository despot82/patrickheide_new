<?php
/**
* sys_portal.php
*
* system-welcome-page: 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.4 / 2006-01-10 (NEU: Module konfigurierbar, neue Module 'adm_prj_jumper' + 'cal_monthcalendar')
* #history	2.3 / 2005-11-14 (NEUES RECHTEMANAGEMENT - NICHT MEHR ABWAERTSKOMPATIBEL!)
* #history	2.2 / 2004-08-31
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_portal.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("./php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	
// 2a. GET-params abholen
// 2b. POST-params abholen
// 2c. Vars
	$sGEToptions = '';
	$spacer = '<img src='.$aENV['path']['pix']['http'].'onepix.gif" style="clear:both" width="1" height="10" alt="" border="0"><br>';

// init TEAMACCESS (einmalig hier fuer alle Module, die das Objekt brauchen)
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();

// 3. DB

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// messages
 	if (file_exists($aENV['path']['php']['unix']."adm_messages.php")) { // wenn ADM-modul installiert ist
		require_once ($aENV['path']['php']['unix']."adm_messages.php");
	}
	if (file_exists($aENV['path']['php']['unix']."adr_messages.php")) { // wenn ADR-modul installiert ist
		require_once ($aENV['path']['php']['unix']."adr_messages.php");
	}
	if (file_exists($aENV['path']['php']['unix']."frm_messages.php")) { // wenn FORUM-modul installiert ist
		require_once ($aENV['path']['php']['unix']."frm_messages.php");
	} ?>
		
<!-- <p><span class="title"><?php echo $aMSG['portal']['hl_welcome'][$syslang]." ".$aENV['client_name']; ?></span><br>
Login:  <?php echo $Userdata['firstname']." ".$Userdata['surname']; ?><br></p>
<?php echo HR; ?> -->
<?php 	
	// Datenbank - Auto-Backup
	//print_r(Tools::getImportantInformation());
	// PHP 5! zum Testen... 
	//try {
		if ($aENV['bAutoDbDump']) {
			// vars
			$bakfolder = 'backup';
			$idelfile = $aENV['MaxDbDump'];
			// ggf. verzeichnis erstellen, in dem die SQL-Files gespeichert werden
			if (!is_dir($aENV['path']['sys_data']['unix'].$bakfolder)) {
				mkdir($aENV['path']['sys_data']['unix'].$bakfolder, 0777);
			}
			// nachsehen, ob heute schon ein backup gemacht wurde, dazu folder auslesen
			$oFolder =& new FSFolder($aENV['path']['sys_data']['unix'].$bakfolder, $aENV);
			$aFiles = $oFolder->getFiles();
			// neueste datei ermitteln
			$iAnz = count($aFiles[$bakfolder]);
			if(is_array($aFiles[$bakfolder])) {
				natsort($aFiles[$bakfolder]);
				end($aFiles[$bakfolder]);
				$file = current($aFiles[$bakfolder]);
				// Aufsplitten zum Ermitteln der ID (= erster filename-part, zweiter ist datum [YYYYMMDD])
				$aFile = explode('_',$file);
			}
			// wenn dieses nicht das heutige datum hat, -> mache backup! 
			if (!file_exists($aENV['path']['sys_data']['unix'].$bakfolder.'/'.($aFile[0]).'_'.date('Ymd').'.sql') || $iAnz == 0) {
				echo '<script language="JavaScript" type="text/javascript">';
				echo "exportWin = window.open('popup_backup.php', 'ExportWindow', 'width=300,height=200,left=100,top=200');";
				echo "exportWin.focus();";
				echo "</script>";
			}	
		}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
	<td width="66%" colspan="3">
		<!-- adm/prj_jumper -->
<?php	if (isset($aENV['module']['adm'])
			&& in_array('adm_prj_jumper', $aENV['portal_module']['L']) 
			&& file_exists("../adm/inc.adm_prj_jumper.php")
			&& $oPerm->hasPriv('admin', 'adm')
			) {
			require_once ("../adm/inc.adm_prj_jumper.php");
			echo $spacer;
		} ?>
		<!-- END adm/prj_jumper -->
		
		<!-- cal/monthcalendar -->
<?php	if (isset($aENV['module']['cal']) 
			&& in_array('cal_monthcalendar', $aENV['portal_module']['L']) 
			&& file_exists("../cal/inc.mod_monthcalendar.php")
			&& $oPerm->hasPriv('view', 'cal')
			) {
			require_once ("../cal/inc.mod_monthcalendar.php");
			echo $spacer;
		} ?>
		<!-- END cal/monthcalendar -->
		
		<!-- adm/time sheet -->
<?php	if (isset($aENV['module']['adm']) 
			&& file_exists("../adm/inc.adm_timesheet_day.php") 
			&& in_array('adm_timesheet', $aENV['portal_module']['L']) 
			&& $oPerm->hasPriv('edit', 'adm')
			) {
			require_once ("../adm/inc.adm_timesheet_day.php");
			echo $spacer;
		} ?>
		<!-- END adm/time sheet -->
		
		<!-- forum -->
<?php	if (file_exists("../frm/inc.mod_forum.php") 
			&& in_array('frm_recent', $aENV['portal_module']['L']) 
			&& $oPerm->hasPriv('view', 'frm')
			) {
			require_once ("../frm/inc.mod_forum.php");
			echo $spacer;
		} ?>
		<!-- END forum -->
		
		<!-- memos -->
<?php	if (file_exists("./inc.mod_memo.php") 
			&& in_array('memo', $aENV['portal_module']['L']) 
			) {
			require_once ("./inc.mod_memo.php");
			echo $spacer;
		} ?>
		<!-- END memos -->
		
<?php // DEBUG Module mit TeamAccessRights-Zuordnungen
if ($oPerm->isDaService() && is_object($oTACEA)) {
	
	// MEMOS-check
	$oDebugMemo =& new DebugTeamAccess('memo');
	$oDebugMemo->printResultTable();
	
	// CAL-check
	if (isset($aENV['module']['cal'])) {
		$oDebugMemo =& new DebugTeamAccess('cal');
		$oDebugMemo->printResultTable();
	}
	
	// TeamAccess-Table-check
	$oDebugMemo =& new DebugTeamAccess();
	$oDebugMemo->printResultTable();

}
// DEBUG Hilfsfunktionen
class DebugTeamAccess {
	// class vars
	var $aVar = array();
	var $aTmp = array();
	// constructor
	function DebugTeamAccess($tree='') {
		global $oDb, $aENV;
		$tree = ($tree != '') ? $tree : 'star'; // star = sys_team_access_right
		$this->aVar = array();
		$oDb2 =& new dbconnect($aENV['db']);
		$this->aTmp = array();
		switch ($tree) {
			case 'memo':
				$this->aVar['headline']   = 'Memos';
				$this->aVar['href']       = '../sys/sys_memo_detail.php';
				// db
				$oDb->query("SELECT `id`,`memo` AS `title` FROM `sys_memo`");
				while ($tmp = $oDb->fetch_array()) {
					$oDb2->query("SELECT `id` FROM `sys_team_access_right` WHERE `fk_id` = '".$tmp['id']."' AND `flag_tree` = 'memo'");
					if ($oDb2->num_rows() == (int)0) { // wenn zu einem eintrag kein gegenstueck in der team_right existiert...
						$this->aTmp[] = $tmp; // ...diesen datensatz merken!
					}
				}
				break;
			case 'cal':
				$this->aVar['headline']   = 'Calendar';
				$this->aVar['href']       = '../cal/cal_calendar_detail.php';
				// db
				$oDb->query("SELECT `id`,`subject` AS `title` FROM `cal_event` WHERE `flag_private_event` = '1'");
				while ($tmp = $oDb->fetch_array()) {
					$oDb2->query("SELECT `id` FROM `sys_team_access_right` WHERE `fk_id` = '".$tmp['id']."' AND `flag_tree` = 'cal'");
					if ($oDb2->num_rows() == (int)0) {
						$this->aTmp[] = $tmp;
					}
				}
				break;
			case 'star':
				$this->aVar['headline']   = 'TeamAccess Datens&auml;tze';
				$this->aVar['href']       = '';
				$oDb->query("SELECT `id`,`fk_id`,`flag_tree`, CONCAT('Modul: ', `flag_tree`, ', ID:', `fk_id`) AS `title` FROM `sys_team_access_right`");
				// db
				while ($tmp = $oDb->fetch_array()) {
					// check memo table
					if ($tmp['flag_tree'] == 'memo') {
						$oDb2->query("SELECT `id` FROM `sys_memo` WHERE `id` = '".$tmp['fk_id']."'");
						if ($oDb2->num_rows() == (int)0) {
							// wenn zu einem team_right-eintrag kein gegenstueck in der entsprechenden table existiert, diesen datesatz merken!
							$this->aTmp[] = $tmp;
						}
					}
					// check cal table
					if ($tmp['flag_tree'] == 'cal') {
						$oDb2->query("SELECT `id` FROM `cal_event` WHERE `id` = '".$tmp['fk_id']."'");
						if ($oDb2->num_rows() == (int)0) {
							// wenn zu einem team_right-eintrag kein gegenstueck in der entsprechenden table existiert, diesen datesatz merken!
							$this->aTmp[] = $tmp;
						}
					}
					// check frm table
					if ($tmp['flag_tree'] == 'frm') {
						$oDb2->query("SELECT `id` FROM `sys_global_tree` WHERE `id` = '".$tmp['fk_id']."'");
						if ($oDb2->num_rows() == (int)0) {
							// wenn zu einem team_right-eintrag kein gegenstueck in der entsprechenden table existiert, diesen datesatz merken!
							$this->aTmp[] = $tmp;
						}
					}
				}
				break;
			#print_r($this->aTmp);//DEBUG
		}
	}
	// print data table // ggf. gefundene datensatz-leichen ausgeben
	function printResultTable() {
		$count = count($this->aTmp);
		if ($count > 0) {
			echo $this->_makeTableStart();
			for ($i = 0; $i < $count; $i++) {
				echo $this->_makeTR($this->aTmp[$i]);
			}
			echo $this->_makeTableEnd();
		}
	}
	// hilfsfunktionen HTML
	function _makeTableStart() {
		$buffer = '<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">';
		$buffer .= '<colgroup><col width="10%"><col width="90%"></colgroup>';
		$buffer .= '<tr><td class="warning" colspan="2">';
		$buffer .= '<b>'.$this->aVar['headline'].' ohne Zuordnung:</b>';
		$buffer .= '</th></tr>';
		echo $buffer;
	}
	function _makeTR($array) {
		$buffer = '<tr><td><p>'.$array['id'].'</p></td><td><p>';
		// title
		$title = $array['title'];
		if ($this->aVar['href']) { // title ggf. verlinken
			$title = '<a href="'.$this->aVar['href'].'?id='.$array['id'].'">'.$title.'</a>';
		}
		$buffer .= $title;
		$buffer .= '</p></td></tr>';
		echo $buffer;
	}
	function _makeTableEnd() {
		echo '</table><br>';
	}
} // END class
?>

	</td>
	<td width="1%"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="10" height="1" alt="" border="0"></td>
	<td width="32%">
		
		<!-- cal/minicalendar -->
<?php	if (isset($aENV['module']['cal'])
			&& file_exists("../cal/inc.mod_minicalendar.php")
			&& $oPerm->hasPriv('view', 'cal')
			&& in_array('cal_minicalendar', $aENV['portal_module']['R']) 
			) {
			require_once ("../cal/inc.mod_minicalendar.php");
			echo $spacer;
		} ?>
		<!-- END cal/minicalendar -->
		
		<!-- cal/events_today -->
<?php	if (isset($aENV['module']['cal']) 
			&& file_exists("../cal/inc.mod_cal_events_today.php") 
			&& $oPerm->hasPriv('view', 'cal')
			&& in_array('cal_events_today', $aENV['portal_module']['R']) ) {
			require_once ("../cal/inc.mod_cal_events_today.php");
			echo $spacer;
		} ?>
		<!-- END cal/events_today -->
		
		<!-- adr/birthdays -->
<?php	if (isset($aENV['module']['adr']) 
			&& file_exists("../adr/inc.mod_birthdays.php") 
			&& $oPerm->hasPriv('view', 'adr')
			&& in_array('cal_birthdays', $aENV['portal_module']['R']) ) {
			require_once ("../adr/inc.mod_birthdays.php");
			echo $spacer;
		} ?>
		<!-- END adr/birthdays -->
		
		<!-- forum -->
<?php	if (file_exists("../frm/inc.mod_forum.php") 
			&& file_exists("../frm/inc.mod_forum.php") 
			&& $oPerm->hasPriv('view', 'frm')
			&& in_array('frm_recent', $aENV['portal_module']['R']) 
			) {
			require_once ("../frm/inc.mod_forum.php");
			echo $spacer;
		} ?>
		<!-- END forum -->
		
		<!-- waehrungsrechner -->
<?php	if (file_exists("./inc.mod_eurocalc.php") 
			&& in_array('calc_currency', $aENV['portal_module']['R']) 
			) {
			require_once ("./inc.mod_eurocalc.php");
			echo $spacer;
		} ?>
		<!-- END waehrungsrechner -->
		
		<!-- masseinheiten -->
<?php	if (file_exists("./inc.mod_inch_cm.php") 
			&& in_array('calc_measure', $aENV['portal_module']['R']) 
			) {
			require_once ("./inc.mod_inch_cm.php");
			echo $spacer;
		} ?>
		<!-- END masseinheiten -->
		
<?php #echo "<pre>"; print_r($Userdata); echo "</pre>"; // DEBUG ?>
	</td>
</tr>
</table>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>