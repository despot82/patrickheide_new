<?php
/**
* sys_user_team_detail.php
*
* Detailpage: teams
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz (optional)
* @param	array	Formular:	$aUser (Checkboxen, die zu kommasep. string zusammengefuehrt werden muessen)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-01-24
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_user_team_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// diese seite ist nur was fuer admins ;-)
	if ($oPerm->hasPriv('admin') == false) { echo js_history_back(); }

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
	$aUser			= (isset($_POST['aUser'])) ? $_POST['aUser'] : array();
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars:
	$sTable			= $aENV['table']['sys_team'];
	$sViewerPage	= $aENV['SELF_PATH']."sys_user_team.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// 3. DB
// DB-action: delete
	if (isset($btn['delete']) && $aData['id'] && !$alert) {
		//make delete
		$aData['flag_deleted'] = 1;
		$oDb->make_update($aData, $sTable,$aData['id']);
		// weiterleitung overviewpage
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) || isset($btn['save_close'])) {
		// erst checken ob title = unique
		$oDb->query("SELECT `id` FROM `".$sTable."` 
					WHERE `title` = '".$aData['title']."' 
						AND `id` != '".$aData['id']."'");
		$entries = $oDb->num_rows();
		if ($entries > 0) { echo js_alert($aMSG['err']['title_notunique'][$syslang]); $alert=true; }
	}
	if ((isset($btn['save']) || isset($btn['save_close'])) && !isset($alert)) {
		// user zu kommasepariertem string zusammenfuehren
		$aData['user'] =  implode(',', $aUser);
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$oDb->make_update($aData, $sTable, $aData['id']);
		} else { // Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id();
		}
		// back to overviewpage
		if (isset($btn['save_close'])) {
			header("Location: ".$sViewerPage); exit;
		}
	}
// DB-action: save_copy
	if (isset($btn['save_copy'])) {
		// erst checken ob title = unique
		$oDb->query("SELECT `id` FROM `".$sTable."` 
					WHERE `title` = '".$aData['title']."'");
		$entries = $oDb->num_rows();
		if ($entries > 0) { echo js_alert($aMSG['err']['title_notunique'][$syslang]); $alert=true; }
	}
	if (isset($btn['save_copy']) && !isset($alert)) {
		// user zu kommasepariertem string zusammenfuehren
		$aData['user'] =  implode(',', $aUser);
		// Insert eines neuen Eintrags
		$copy_id = $aData['id'];
		unset($aData['id']);
		$aData['created'] = $oDate->get_mysql_timestamp();
		$aData['created_by'] = $Userdata['id'];
		$oDb->make_insert($aData, $sTable);
		$aData['id'] = $oDb->insert_id();
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		// wenn "copy" geklickt wurde
		if (isset($btn['copy'])) {
			// title auffaellig veraendern!
			$aData['title'] = $aMSG['user']['copy_of'][$syslang].$aData['title'];
			$mode_key = "copy"; // do not change!
		}
	} else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("title", $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">
<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['teams'][$syslang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td align="right"><?php // Buttons 
			echo $oForm->button("BACK"); // params: $sType[,$sClass='']
			echo $oForm->button("NEW"); // params: $sType[,$sClass='']
		?></td>
	</tr>
</table>

<?php echo HR; ?>

<?php include_once("inc.sys_detnavi.php"); 
	echo getSysUserSubnavi(); ?>

<?php if (isset($btn['copy'])) { echo '<span class="text">'.$aMSG['std']['copy_reminder'][$syslang].'<br><br></span>'; } ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="25%"><col width="75%"></colgroup>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['name'][$syslang]; ?> *</b></span></td>
		<td><?php echo $oForm->textfield("title",250,78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['description'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textarea("description",3,78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['topnavi']['users'][$syslang]; ?> *</b></span></td>
		<td>
		<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr valign="top">
			<td><span class="text"><?php echo _getTeamCheckboxes($oDb, $aData['user']); ?></span></td>
			<?php if ($mode_key == "edit") { ?>
			<td align="right">
			<script language="JavaScript" type="text/javascript">
			function confirmJumpToUser(towhere) {
				var chk = window.confirm("<?php echo $aMSG['err']['is_data_saved'][$syslang]; ?>");
				var loc = (towhere == 'new') ? 'sys_user_detail.php?id=<?php echo $aData['id']; ?>' : 'sys_user.php';
				if(chk==true){
					location.replace('<?php echo $aENV['path']['sys']['http']; ?>'+loc)
				}
			}
			</script>
			<a href="#" onclick="confirmJumpToUser('edit')" title="<?php echo $aMSG['user']['edit_user'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['user']['edit_user'][$syslang]; ?>">| <?php echo $aMSG['user']['edit_user'][$syslang]; ?>&nbsp;</a>
			<a href="#" onclick="confirmJumpToUser('new')" title="<?php echo $aMSG['user']['new_user'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_contact_new.gif" alt="<?php echo $aMSG['user']['new_user'][$syslang]; ?>">| <?php echo $aMSG['user']['new_user'][$syslang]; ?>&nbsp;</a>
			</td>
			<?php } // END if ?>
		</tr></table>
		</td>
	</tr>
</table>
<br>
<?php if (!$btn['copy']) { ?>
<?php	echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php 	echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php	if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php	if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>&nbsp;
<?php	if ($mode_key == "edit") { echo $oForm->button("COPY"); } ?>
<?php } else { ?>
<?php	echo $oForm->button("SAVE_COPY"); // params: $sType[,$sClass=''] ?>
<?php	echo $oForm->button("CANCEL"); ?>
<?php } ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");

// HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////
	// nur die User ermitteln, die UGs zugeordnet sind, die zumindest 'view'-Privilegien fuer das PMS haben!
	function _getTeamCheckboxes(&$oDb, $sUserChecked='') {
		// vars
		$sStr = '';
		$aUserChecked = explode(',', $sUserChecked);
		$oUser =& new user($oDb);
		// ermittle User
		$aUsername = $oUser->getAllUserNames(); // params: [$sModule='sys'][,$sPriv='view'][,$userid=null] 
		if (!is_array($aUsername)) return;
		foreach ($aUsername as $uid => $uname) {
			// build html
			$checked = (in_array($uid, $aUserChecked)) ? ' checked' : '';
			$sStr .= '<input type="checkbox" name="aUser[]" id="'.$uid.'" value="'.$uid.'" class="radiocheckbox"'.$checked.'>';
			$sStr .= '<label for="'.$uid.'"> '.$uname."</label><br>\n";
		}
		// output
		return $sStr;
	}
?>
