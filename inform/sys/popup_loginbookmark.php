<?php
/**
* popup_loginbookmark.php
*
* Popup, um die Startseite des Intranets inklusive Login-Parameter bookmarken zu koennen. 
* -> 2sprachig und kopierbar.
*
* Diese Seite wird aus der sys_user_detail.php aufgerufen
*
* @param	string	$userid		ID des Users
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2006-01-19
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.popup_loginbookmark.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. USER	// Diese Seite sollen ALLE sehen duerfen
// 1c. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$userid = (isset($_GET['userid'])) ? $_GET['userid']: 0;
	
// 2b. POST-params abholen
// 2c. Vars
	// Userdata holen
	$aData = $oDb->fetch_by_id($userid, $aENV['table']['sys_user']);
	// Link fuer Login-Bookmark
	$bookmarkname = $aENV['client_name'].' '.$aENV['system_name'][$syslang].' Login';
	$bookmarklink = $aENV['page']['sys_welcome'].'?l='.base64_encode($aData['password'].';'.$aData['username']);
	$mobile_bookmarklink = $aENV['page']['mobile_welcome'].'?l='.base64_encode($aData['password'].';'.$aData['username']);

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	
?>
<style type="text/css">
#bmlink {display: none;}
#bmtxt {display: none;}
</style>
 
<div id="contentPopup">
<span class="title"><?php echo $aMSG['lb']['headline'][$syslang]; ?><br><br></span>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<tr>
	<td height="100">
		<p><?php echo $aMSG['lb']['description'][$syslang]; ?></p>
		<?php echo HR;?>
		<div id="bmlink">
			<p><?php echo $aMSG['lb']['oktxt1'][$syslang]; ?><br>
			<a href="javascript:bookmark();"><b><?php echo $aMSG['lb']['openwin'][$syslang]; ?></b></a><br>
			<small><?php echo $aMSG['lb']['sidebar_help'][$syslang]; ?></small><br></p>

			<?php echo HR;?>

			<p><?php echo $aMSG['lb']['oktxt2'][$syslang]; ?><br>
			<b><?php echo $bookmarklink; ?></b><br>
			<br>
			<?php echo $aMSG['lb']['mobile_login'][$syslang]; ?> <b><?php echo $mobile_bookmarklink; ?></b><br>
			</p>
		</div>
		<div id="bmtxt">
			<p><?php echo $aMSG['lb']['kotxt'][$syslang]; ?><br>
			<b><?php echo $bookmarklink; ?></b><br>
			<br>
			<?php echo $aMSG['lb']['mobile_login'][$syslang]; ?> <b><?php echo $mobile_bookmarklink; ?></b><br>
			</p>
		</div>
	</td>
</tr>
</table>
</div>

<script language="javascript" type="text/javascript">
<!--
function checkBmAvailibility() {
	var okdiv = document.getElementById('bmlink');
	var kodiv = document.getElementById('bmtxt');
	if (document.all || (window.sidebar && window.sidebar.addPanel)) {
		getLayer('bmlink').display = "block";
	} else {
		getLayer('bmtxt').display = "block";
	}
}
checkBmAvailibility();
function bookmark() {
	if (document.all) {
		var wait = window.external.AddFavorite('<?=$bookmarklink?>','<?=$bookmarkname?>');
		if(wait) this.window.close();
	} 
	else if (window.sidebar && window.sidebar.addPanel) {
		var wait = window.sidebar.addPanel('<?=$bookmarkname?>','<?=$bookmarklink?>','');
		if(wait) this.window.close();
	}
}
//-->
</script>

</body>
</html>