<?php
/**
* popup_copyright.php
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Martina Grabovszky mg@design-aspekt.com
* @version	1.0 / 2006-01-25
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.popup_copyright.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// init
	require_once("../sys/php/_include_all.php");
	###// KEIN login!
	###require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
?>

<div id="contentPopup">

<?php if ($syslang=="de") { ?>
<span class="title">Lizenzvereinbarung - Inform<br><br></span>

<table width="100%" border="0" cellspacing="1" cellpadding="5" class="tabelle">
<tr>
	<td><p>
	Stand: Januar 2006<br><br>

	<b>Präambel</b><br>
	<br>Diese Lizenzvereinbarung bezieht sich auf das von Design Aspekt M. Grabovszky und Ch. Riss GbR veröffentlichte Informations-Management System "Inform" inklusive aller Module, die in der Anlage näher beschrieben sind. <br>
	<br>Lesen Sie nachfolgende Lizenzbedingungen aufmerksam und sorgfältig durch, bevor Sie das System einsetzen. Durch Verwendung der Software erklären Sie Ihr ausdrückliches Einverständnis mit den nachstehenden Lizenzbestimmungen. Für den Fall, dass Sie mit diesen Lizenzbedingungen nicht einverstanden sind, dürfen Sie die Software nicht verwenden.

	<br><br>
	<ol>
	<li>
		<b>Gegenstand des Vertrages</b><br><br>
		<ol>
		<li>Die Software mit der Bezeichnung "Inform" ist ein urheberrechtlich geschütztes Werk. Das Urheberrecht liegt bei Design Aspekt M. Grabovszky und Ch. Riss GbR, Schwanthalerstrasse 76, 80336 München, Deutschland. Dem Lizenznehmer ist "Inform" und seine Leistungsfähigkeit bekannt. "Inform" wurde unter Beachtung wissenschaftlicher Sorgfalt und anerkannter Regeln der Technik entwickelt und wird bereits gegenüber Dritten erfolgreich zur Nutzung angeboten. "Inform" wird sowohl durch Urheberrechtsgesetze und internationale  Urheberrechtsabkommen als auch durch andere Abkommen über geistiges Eigentum geschützt. Die Parteien gehen von der Urheberrechtsfähigkeit aus. Alle Rechte an "Inform", die mit dieser Lizenzvereinbarung nicht ausdrücklich an den Lizenznehmer übertragen werden, bleiben ausschließlich dem Lizenzgeber vorbehalten.
		<br>Sollten Sie Fragen zu dieser Lizenzvereinbarung haben, wenden Sie sich bitte an <a href="mailto:inform@design-aspekt.com?Subject=Lizenzvereinbarung"><b>inform@design-aspekt.com</b></a>.</li>
		<li>Soweit der Lizenzgeber nicht selbst die Schutzrechte an der Software oder Teilen davon besitzt, so besitzt er die Rechte, die die Weitergabe und Nutzung durch Dritte erlauben.</li>
		<li>Die Software wird nicht verkauft, sondern lizenziert. Der Lizenznehmer erhält mit dem Erwerb der Software nur ein Nutzungsrecht an der Software und dem zugehörigen Material. Das Nutzungsrecht wird ausdrücklich nur bei Zahlung der Lizenzgebühr in voller Höhe übertragen. Soweit der Lizenznehmer die Lizenzgebühr nicht oder nicht vollständig an den Lizenzgeber zahlt, erhält er keine Nutzungsrechte und darf die Software nicht nutzen.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Eingeräumtes Nutzungsrecht</b><br>
		<br>Design Aspekt räumt dem Lizenznehmer ein zeitlich unbegrenztes, nicht ausschließliches Recht ein, das Informations-Management-System "Inform" auf einem Webserver zu betreiben und dort über angeschlossene PCs zur Pflege der Inhalte zu verwenden, welche unter dieser Domain gespeichert sind. In demselben Umfang werden Nutzungsrechte bezüglich der vom Lizenznehmer erworbenen Zusatzmodule eingeräumt. Eine Verletzung dieses Vertrages führt zum Erlöschen des Nutzungsrechtes. Die Software darf dann nicht weiterverwendet werden.
	</li>
	
	<br><br>
	
	<li>
		<b>Vollübertragung der Lizenz an Dritte </b><br><br>
		<ol>
		<li>Der Lizenznehmer ist berechtigt, die Lizenz für eine installierte Internetdomain vollständig an einen Dritten zu übertragen. Rechtliche Voraussetzungen hierfür sind:
			<ol>
			<li>die vorherige Liberierung der Lizenz durch vollständige Bezahlung der Lizenzgebühr sowie </li>
			<li>die vorherige Anzeige des Namens und der vollständigen Adresse des Dritten beim Lizenzgeber, unter Angabe von Postleitzahl, Ort, Straße, Hausnummer und E-Mail Adresse des Dritten und </li>
			<li>die Zustimmung des Dritten zur Übernahme und zu diesem Lizenzvertrag, die von dem Dritten gegenüber dem Lizenzgeber erklärt werden muss. </li>
			</ol></li>
		<li>Nach Erfüllung der in Ziff. 3.1 aufgeführten Voraussetzungen ändert der Lizenzgeber seine Kontrolldatenbank und tauscht dort bei der betroffenen Internetdomain den Namen und die übrigen Daten des bisherigen Lizenznehmers gegen diejenigen des benannten Dritten aus. Von da an steht das an "Inform" eingeräumte Nutzungsrecht bezüglich der betroffenen Internetdomain nicht mehr dem bisherigen Lizenznehmer, sondern allein dem von diesem benannten Dritten zu, welcher hierdurch Lizenznehmer wird. Allein der neue Lizenznehmer ist von da an auch tatsächlich in der Lage, die Pflege der betroffenen Internetdomain mit "Inform" vorzunehmen. </li>
		<li>Der bisherige Lizenznehmer ist nach erfolgter Übertragung der Lizenz berechtigt und verpflichtet, die ihm in Erfüllung des Lizenzvertrages für die übertragene Lizenz übergebenen Quellen (durch Server-Upload erfolgte Installation sowie eventuell daraus angefertigte Backup-Datenträger) unverzüglich an den neuen Lizenznehmer zu übergeben beziehungsweise die angefertigten Datenträger zu löschen oder zu vernichten.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Vermietung, Verleih, Sicherungs- / Archivkopie </b><br><br>
		<ol>
		<li>Der Lizenznehmer ist ohne vorherige Zustimmung des Lizenzgebers nicht berechtigt, "Inform" insgesamt oder Teile davon oder Kopien davon zu vermieten oder zu verleihen. </li>
		<li>Der Lizenznehmer ist berechtigt, eine Kopie von "Inform" zu Sicherungs- oder Archivierungszwecken herzustellen. </li>
		<li>Vom Lizenzgeber geliefertes schriftliches Material zu "Inform" darf ohne ausdrückliche schriftliche und vorherige Zustimmung durch der Lizenzgeber weder ganz noch in Teilen kopiert oder in sonstiger Weise vervielfältigt werden, es sei denn, das geltende Urheberrecht ließe hiervon (eine) Ausnahme(n) zu. </li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Änderungen und Aktualisierungen</b><br><br>
		<ol>
		<li>Der Lizenzgeber ist berechtigt, aber nicht verpflichtet, Aktualisierungen der Software (Updates) zu erstellen und das System des Lizenznehmers nach Ankündigung zu aktualisieren.</li>
		<li>Der Lizenzgeber kann für derartige Aktualisierungen eine Aktualisierungsgebühr verlangen.</li>
		<li>Der Lizenzgeber ist nicht verpflichtet, Aktualisierungen der Software an solche Lizenznehmer auszuliefern, die eine oder mehrere vorhergehende Aktualisierungen abgelehnt oder die Aktualisierungsgebühr nicht bezahlt haben.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Recht zur Änderung und Ergänzung gegen (kostenlose) Rücklizenz </b><br><br>
		<ol>
		<li>Der Lizenznehmer hat das Recht, die Sourcen von "Inform" zu erweitern, zu ergänzen, zu kürzen oder in sonstiger Weise zu verändern (zusammenfassend "Sourcenergänzung" genannt). </li>
		<li>Nimmt der Lizenznehmer eine Sourcenergänzung vor, so erstreckt sich der mit ihm bestehende Lizenzvertrag auf die Sourcenergänzung. Es fallen keine zusätzlichen Lizenzgebühren an. </li>
		<li>Der Lizenznehmer ist verpflichtet, die Sourcenergänzung dem Lizenzgeber vollumfänglich zur Verfügung zu stellen und ihm eine kostenlose Rücklizenz hierfür anzubieten. Die Rücklizenz ist ausschließlich und zeitlich unbefristet und umfasst sämtliche zum Zeitpunkt der pflichtgemäßen Anbietung bekannten Nutzungsarten. Hiervon unbeschadet ist das eigene Nutzungsrecht des Lizenznehmers (Rücklizenzgebers) aufgrund des erweiterten Lizenzvertrages gemäß Ziff. 6.2. </li>
		<li>Werden Anpassungen oder Erweiterungen durch den Lizenznehmer oder von ihm beauftragte Dritte vorgenommen, hat das ein Erlöschen der Gewährleistung zur Folge.</li>
		<li>Aktualisierungen an einem System, das durch eine Sourcenergänzung verändert wurde, können nur nach einer Prüfung durch den Lizenzgeber vorgenommen werden. Der Lizenzgeber kann für derartige Prüfungen eine angemessene Gebühr verlangen.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Gewährleistung </b><br><br>
		<ol>
		<li>Die Gewährleistung für "Inform" ist auf die Dauer von einem Jahr ab Auslieferung beschränkt. 
		<li>Bei Mängeln von Inform ist der Lizenzgeber entgegen § 439 Abs. 1 BGB nach seiner Wahl zur Mangelbeseitigung oder Ersatzlieferung berechtigt. </li>
		<li>Der Lizenzgeber macht erhebliche Anstrengungen, durch Qualitätssicherungsmaßnahmen eine weitestgehende Mängelfreiheit seiner Produkte zu erreichen. Nach den heutigen Stand der Technik ist es jedoch nicht möglich, Software so zu erstellen, dass sie in allen Anwendungsfällen fehlerfrei arbeitet. Gegenstand dieses Vertrages ist deswegen nur eine Software, die im Sinne der Programmbeschreibung grundsätzlich brauchbar ist. Die Garantie der Beschaffenheit bedarf einer zusätzlichen ausdrücklichen schriftlichen Vereinbarung. Die technischen Daten, Spezifikationen und Leistungsbeschreibungen in der Produktbeschreibung stellen keine Garantie der Beschaffenheit dar, es sein denn, sie sind ausdrücklich als solche vom Lizenzgeber bestätigt worden.</li>
		<li>Der Gewährleistungsanspruch entfällt hinsichtlich solcher Programmteile, die vom Lizenznehmer selbst geändert und/oder erweitert wurden oder bei Mängeln, Störungen oder Schäden, die auf Grund unsachgemäßer Bedienung bzw. Fehler der Hardware im Verantwortungsbereich des Lizenznehmers liegen.</li>
		<li>Der Gewährleistungsanspruch entfällt ebenfalls, wenn dem Lizenzgeber die Möglichkeit verwehrt wird, die Ursache des gemeldeten Mangels zu untersuchen.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Haftungsbeschränkung </b><br><br>
		<ol>
		<li>Der Lizenzgeber haftet aus gesetzlichen oder vertraglichen Haftungstatbeständen nur, wenn ihr Vorsatz oder grobe Fahrlässigkeit zur Last gelegt werden kann. </li>
		<li>Keinesfalls übernimmt der Lizenzgeber und/oder dessen Lieferanten Haftungen für indirekte oder Folgeschäden oder sonstige Schäden, die auf Nutzungsausfall, Verlust von Daten oder entgangenem Gewinn gestützt werden und die durch die oder im Zusammenhang mit der Verwendung von auf diesem Server verfügbaren Informationen entstanden sind. Der Lizenzgeber und dessen Lieferanten behalten sich die Möglichkeit vor, jederzeit Verbesserungen und/oder Änderungen an den hier beschriebenen Produkten und/oder Programmen vorzunehmen. Insbesondere ist die Haftung für Veränderungen von dritter Seite ausgeschlossen.
			<br>Ferner soweit der Lizenznehmer wegen eines Sach- oder Rechtsmangels nach gescheiterter Vertragserfüllung den Rücktritt vom Vertrag verlangt. Die Höhe des Schadensersatzes ist begrenzt auf den vorhersehbaren, vertragstypischen und unmittelbaren Durchschnittsschadens, der nach Art des Produktes entstehen kann.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Schutzrechte</b><br><br>
		<ol>
		<li>Der Lizenznehmer erkennt die Rechte des Lizenzgebers (Patente, Urheberrechte, Geschäftskennzeichen Geschäftsgeheimnisse) uneingeschränkt an. Er verpflichtet sich, diese Rechte zu wahren und alle Schritte zu unternehmen, um Beeinträchtigungen oder Verletzungen dieser Rechte durch Dritte zu unterbinden und zu verfolgen.</li>
		<li>Für den Fall der Rücklizenzierung bestätigt der Lizenznehmer, dass er sämtliche Rechte an allen dem Lizenzgeber übergebenen Programmcodes, Daten, Texten, Grafiken, und sonstigen Daten, die innerhalb der Sourcenergänzung Anwendung finden, besitzt.</li>
		<li>Für den Fall, dass der Lizenznehmer innerhalb der Sourcenergänzung bestehende Rechte Dritter verletzt und der Lizenzgeber daher von Dritten in Anspruch genommen wird, stellt der Lizenznehmer den Lizenzgeber von dem hieraus entstehenden Schaden in voller Höhe frei. Nur für den Fall eines Mitverschuldens haftet der Lizenzgeber entsprechend seinem Mitverschulden.</li>
		<li>Der Lizenznehmer verpflichtet sich, das ihm vom Lizenzgeber zugänglich gemachte Know-how und die ihm zuteil werdenden Kenntnisse geheim zu halten, insbesondere nicht an Dritte weiterzugeben und nur im Rahmen dieses Vertrages zu nutzen.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Verschiedenes </b><br><br>
		<ol>
		<li>Erfüllungsort für die vertraglichen Pflichten des Lizenzgebers und Gerichtsstand für sämtliche Streitigkeiten aus dem Vertrag ist - soweit gesetzlich zulässig - München. Der Lizenzgeber darf den Lizenznehmer auch an seinem Sitz oder gewöhnlichen Aufenthalt verklagen. </li>
		<li>Es gilt das Recht Deutschlands unter Ausschluss des einheitlichen Kaufgesetzes und des UN-Kaufrechts sowie des Internationalen Privatrechts. </li>
		<li>Sollten Teile dieses Vertrages ganz oder teilweise unwirksam sein oder werden, so berührt dies die Wirksamkeit der übrigen Regelungen nicht. Die Parteien verpflichten sich vielmehr, die unwirksame Regelung durch eine solche zu ersetzen, die dem wirtschaftlich Gewollten am nächsten kommt.
			<br>Änderungen dieses Vertrages bedürfen der Schriftform. Gleiches gilt für die Aufhebung dieser Schriftformklausel.</li>
		<li>Die ladungsfähige Anschrift des Lizenzgebers ist: Design Aspekt, Schwanthalerstrasse 76, 80336 München, Deutschland.</li>
		<li>Design Aspekt hat das Recht, auf alle mit Inform erstellten Domains einen Verweis in das Impressum der Seite zu setzen, der auf design-aspekt.com verlinkt.</li>
		</ol>
	</li>
	</ol>
	</p></td>
</tr>
</table><br><br>

<?php } else { ?>
<span class="title">License Agreement - Inform<br><br></span>

<table width="100%" border="0" cellspacing="1" cellpadding="5" class="tabelle">
<tr>
	<td><p>
	January 2006<br><br>
	<b>Preamble</b><br><br>
The present license agreement covers the Information Management System "Inform", published by Design Aspekt M. Grabovszky und Ch. Riss GbR, including all modules which are described in detail in the Annex. <br><br>
Please read the following licensing terms carefully before you start using the system. By using this software you agree to be bound by the below-mentioned licensing terms. If you do not agree with these licensing terms, you are not allowed to use this software.<br><br>

<ol>
	<li>
		<b>Object of the agreement</b><br><br>
		<ol>
			<li>The software under the name "Inform" is a work protected by copyright. The copyright is the property of Design Aspekt M. Grabovszky und Ch. Riss GbR, Schwanthalerstrasse 76, 80336 Munich, Germany. The licensee is familiar with the software "Inform" and its performance. "Inform" was developed in compliance with scientific care and state of the art technology and has already been offered successfully to third parties for use. "Inform" is both protected by copyrights and international copyright treaties as well as other treaties relating to intellectual property. Both parties assume the eligibility of the software for copyright protection. All rights in "Inform" which have not been transferred explicitly to the licensee under this license agreement shall remain the exclusive property of the licensor.<br>
			Should you have any queries on the present license agreement, please do not hesitate to contact <a href="mailto:inform@design-aspekt.com?Subject=License Agreement"><b>inform@design-aspekt.com</b></a>.</li>
			<li>To the extent that the licensor is not the owner of any protection rights in the software or parts thereof, the licensor is the owner of the rights that allow the transfer or use thereof by third parties.</li>
			<li>The software is not sold but licensed. By purchasing the software, the licensee only acquires a right of use in the software and related materials. It is herewith expressly agreed that the right of use is only transferred upon full payment of the license fee. If the licensee fails to pay the license fee as a whole or in part to the licensor, the licensee is not granted any rights of use and may not use the software.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Grant of license</b><br><br>
Design Aspekt grants the licensee a perpetual, non-exclusive right to operate the Information Management System "Inform" on a Web server and to use it on connected PCs for maintaining contents that are stored under this domain. Identical rights of use are granted for any additional modules purchased by the licensee. A breach of this agreement results in the forfeiture of the right of use. In this case, the licensee is no longer entitled to use the software.
	</li>
	
	<br><br>
	
	<li>
		<b>Full transfer of the license to third parties</b><br><br>
		<ol>
			<li>The licensee shall be entitled to transfer the license in its entirety for an installed Internet domain to a third party. Such a transfer is subject to the following legal requirements:
			<ol>
				<li>the license has been released previously through full payment of the license fee; </li>
				<li>the licensor has been notified previously of the name and complete address of the third party, including post code, city, street, house number and email address, and</li>
				<li>the third party agrees to the acceptance of the license and to the present license agreement, which has to be declared by the third party vis-à-vis the licensor. </li>
			</ol>
			</li>
			<li>When the requirements outlined in para. 3.1 are met, the licensor shall change its control database and shall replace the name and any other details of the previous licensee by the details of the designated third party in respect of the relevant Internet domain. From that date, the right of use granted in "Inform" with respect to the relevant Internet domain is no longer in the possession of the previous licensee but solely of the designated third party, which becomes the new licensee as a result. Only the new licensee is then able to maintain the relevant Internet domain using "Inform". </li>
			<li>Following the transfer of the license, the previous licensee shall be entitled and obliged to immediately furnish to the new licensee all resources handed over to the licensee under the present license agreement for the granted license (installation carried out through a server upload and any backup data carriers created), or to delete or destroy any data carriers created.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Lease, lending, backup/archiving copy</b><br><br>
		<ol>
			<li>Without the prior consent of the licensor, the licensee shall not have the right to lease or to lend "Inform" as a whole or in part or any copies thereof. </li>
			<li>The licensee shall be entitled to make a copy of "Inform" for backup or archiving purposes. </li>
			<li>The licensee is not entitled to copy or reproduce in any other manner written materials relating to "Inform" supplied by the licensor, either as a whole or in part, without the prior express written consent of the licensor, unless applicable copyright laws provide for any exception(s). </li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Changes and updates</b><br><br>
		<ol>
			<li>The licensor shall be entitled but not obliged to develop software updates and to update the licensee's system after prior announcement.</li>
			<li>The licensor may charge an update fee for such updates.</li>
			<li>The licensor is not obliged to supply any software updates to licensees that have refused to accept one or several previous updates or have failed to pay the update fee.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Right to make changes or supplements against a (free) reciprocal license</b><br><br>
		<ol>
			<li>The licensee shall be entitled to expand, to supplement, to shorten or otherwise modify the source codes of "Inform" (in short referred to as "source code expansion"). </li>
			<li>When the licensee makes source code expansions, the license agreement concluded with the licensee shall also cover the source code expansion. No additional license fees shall be charged as a result. </li>
			<li>The licensee shall be obliged to provide the source code expansion to the licensor without any restrictions and to offer the licensor a free reciprocal license. The reciprocal license is exclusive and perpetual, and covers all types of use known at the time the due offer is made. The right of use of the licensee (grantor of the reciprocal license) shall not be affected due to the expanded license agreement as defined under para. 6.2. </li>
			<li>If any adaptations or expansions are made by the licensee or third parties engaged by it, the warranty shall lapse.</li>
			<li>A system that has been modified through a source code expansion can only be updated after a review by the licensor. The licensor may charge an adequate fee for such reviews.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Warranty</b><br><br>
		<ol>
			<li>The warranty relating to "Inform" is limited to a term of one year from delivery. </li>
			<li>In the event of defects, the licensor shall be entitled to provide, in deviation from Section 439 (1) of the German Civil Code (BGB) at its option, rectification of defects or substitute delivery. </li>
			<li>The licensor shall undertake considerable efforts to deliver completely faultless products through quality assurance measures. However, pursuant to state of the art technology, it is not possible to develop software in such a manner that it is free from defects in all types of applications. Therefore, the object of this agreement is software, which can be used on principle within the meaning of the programme description. The warranty of quality requires an additional express written agreement. The technical data, specifications and service descriptions in the product description shall not constitute a warranty of quality, unless they have been acknowledged as such explicitly by the licensor.</li>
			<li>No warranty is granted for programme components that have been modified and/or expanded by the licensee or for defects, faults or disorders that are, due to improper operation or faults in hardware, under the control of the licensee.</li>
			<li>In addition, no warranty is granted where the licensor is not given an opportunity of examining the cause of the defect reported.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Limitation of liability</b><br><br>
		<ol>
			<li>The licensor is only liable for statutory or contractual liability claims in the event of intent or gross negligence on the licensor's part. </li>
			<li>The liability of the licensor and/or its suppliers for indirect or consequential damage or any other damage claims that are based on loss of use, loss of data or lost profits in connection with or arising out of the use of information available on this server shall be excluded. The licensor and its suppliers reserve the right to make improvements and/or modifications at any time to the products and/or programmes described in the present agreement. In particular, liability for changes made by third parties shall be excluded.<br>
Liability is also excluded, when the licensee wants to cancel the agreement due to a defect in title or a defect of quality subsequent to non-performance of the agreement. The amount of damages shall be limited to the foreseeable, direct average damage typical for such agreements, which may occur depending on the type of product.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Intellectual property rights</b><br><br>
		<ol>
			<li>The licensee acknowledges the rights of the licensor (patents, copyrights, business marks, and business secrets) without restrictions. The licensee undertakes to safeguard these rights and to undertake all efforts to prevent and pursue breaches or violations of these rights by any third parties.</li>
			<li>Where a reciprocal license has been granted, the licensee acknowledges that it possesses all rights in programme codes, data, texts, graphics and other data used within source code expansions and handed over to the licensor.</li>
			<li>In the event that the licensee violates any rights of third parties existing within the context of the source code expansion and claims are brought forward by third parties against the licensor in this connection, the licensee shall indemnify the licensor fully for any resulting damages. Only in the event of contributory default, the licensor shall be liable in proportion to its contributory default.</li>
			<li>The licensee undertakes to maintain secrecy regarding all know-how and any knowledge disclosed to it by the licensor, not to disclose it to third parties and to use it only in the context of the present agreement.</li>
		</ol>
	</li>
	
	<br><br>
	
	<li>
		<b>Miscellaneous provisions</b><br><br>
		<ol>
			<li>The place of performance for all contractual duties of the licensor and the place of jurisdiction for all disputes arising out of this agreement shall be, to the extent permitted by law, Munich. However, the licensor shall be entitled to sue the licensee at its registered office or usual place of residence. </li>
			<li>The law of the Federal Republic of Germany shall be applicable, the provisions of the UN Convention on Contracts for the International Sale of Goods and of the International Private Law being excluded. </li>
			<li>If individual provisions of the present agreement are or become totally or partially invalid, the validity of the remaining provisions shall not be affected. The parties undertake to replace the invalid provision by a provision which comes closest to the economic purpose intended by them.<br>
Any changes in the contract must be made in writing. This shall also apply to a waiver of this written form requirement.</li>
			<li>The licensor's address for service is: Design Aspekt, Schwanthalerstrasse 76, 80336 Munich, Germany.</li>
			<li>Design Aspekt shall have the right to place a note linking to design-aspekt.com in the imprint page of any domains usings Inform.</li>
		</ol>
	</li>
</ol>
	</p></td>
</tr>
</table><br><br>

<?php } ?>

</div>

</body>
</html>