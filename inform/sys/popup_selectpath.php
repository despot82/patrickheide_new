<?php
/** 
* popup_selectpath.php
*
* Popup, um eine Verzeichnisstruktur eines FPT-Verzeichnisses anzuzeigen und einen Link davon auszuwaehlen. 
* -> 2sprachig und kopierbar.
*
* Diese Seite wird aus der JS-function selectFtpWin(formfield,formname) aufgerufen)
*
* @param	string	$id			welcher Datensatz
* @param	string	$formfield		[um zu wissen in welches Feld uebertragen werden soll] (required)
* @param	string	$formname		[um zu wissen in welches Formular uebertragen werden soll] (required)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2006-01-04
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.popup_selectpath.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. USER	// Diese Seite sollen ALLE sehen duerfen
// 1c. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$sSort			= (isset($_GET['sortdir'])) ? $oFc->make_secure_string($_GET['sortdir']) : "ASC";
	$id				= (isset($_GET['id'])) ? $_GET['id'] : '';
	$formfield		= (isset($_GET['formfield'])) ? $oFc->make_secure_string($_GET['formfield']) : '';
	$formname		= (isset($_GET['formname'])) ? $oFc->make_secure_string($_GET['formname']) : 'editForm';
	$sGETOptions	= "?id=".$id."&formfield=".$formfield."&formname=".$formname."&sortdir=".($sSort=='ASC'?"DESC":"ASC");
// 2c. Vars

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	
// Folder-Klasse init
	$oFS =& new FSFolder($aENV['path']['ftp']['unix'], $aENV, $sSort, $aENV['PHP_SELF'].$sGETOptions);
	$oFS->setJsHandler('onclick="put_value(this.href); return false;"');
	$oFS->setSpace('22');
	$oFS->setOpenDepth('0');
?>

<script language="JavaScript" type="text/javascript">
// puts value into input field of opener page
function put_value(val) {
	<?php if ($formfield) { ?>
	val = val.replace('https:', 'http:');
	window.opener.document.forms['<?php echo $formname; ?>']['aData[<?php echo $formfield; ?>]'].value = val;
	window.opener.document.forms['<?php echo $formname; ?>']['<?php echo $formfield; ?>_title'].value = val;
	window.opener.document.forms['<?php echo $formname; ?>']['remoteSave'].value = 1;
	window.opener.document.forms['<?php echo $formname; ?>'].submit();
	window.opener.focus();
	self.close();
	<?php } else { ?>
	alert('ERROR: der JS-Funktion fehlt der Parameter $formfield!')
	<?php } ?>
}
</script>

<div id="contentPopup">
<span class="title"><?php echo $aMSG['btn']['select'][$syslang]; ?><br></span>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="6" alt="" border="0"><br>
<div class="text">
	<form>

<?php
	echo $oFS->showFileBrowser(); ?>
	
	</form>
</div>

</div>

</body>
</html>