<?php
/**
* inc.sys_header.php
*
* HTML-header-include (system) //-> 2sprachig und voll kopierbar!
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.2 / 2005-05-06 (NEU: Topnavi Klassenbasiert)
* #history	1.1 / 2004-11-03 (NEU: Printfenster eingebaut)
* #history	1.0 / 2004-04-27
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = $aENV['path']['sys']['unix'].'inc.sys_header.custom.php';
	if (file_exists($custompage)) {
		include($custompage);
	}
	else {

// basename(-teile) auslesen fuer status-highlight(n)ing
	$sBsname	= basename($_SERVER['PHP_SELF']);				// Name der Datei auslesen
	$sBasename	= substr($sBsname, 0, strrpos($sBsname, "."));	// .php abschneiden
	$aBnParts	= explode("_",$sBasename);						// $aBnParts Array erzeugen

// Link zum Printfenster (kann von der inkludierenden Seite ueberschrieben werden!)
	if (!isset($_GET['sPrintPage'])) { $sPrintPage = $aENV['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'].'&printview=true'; }
	$sStylesheet = (isset($_GET['printview']) && $_GET['printview'] == "true") ? 'print' : ''; // Pfad zum (Print-)Stylesheet

// TOPNAVI DATA
if (isset($Userdata) && !empty($Userdata)) {
	// lade topnavi-array
	if (file_exists($aENV['path']['config']['unix']."array.topnavi.php")) {
		require_once($aENV['path']['config']['unix']."array.topnavi.php");
	}
	// init class
	require_once($aENV['path']['global_module']['unix']."class.InformTopnavi.php");
	$oTopnavi =& new InformTopnavi($aTOPNAVI); // params: $aTOPNAVI 
	// Titel der aktiven Seite
	$sTopnaviTitle = $oTopnavi->getCurentTitle();
} else { 
	// Titel, wenn NICHT eingeloggt
	$sTopnaviTitle = 'Login';
	$syslang = (!empty($syslang)) ? $syslang :$aENV['system_language']['default'];
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title><?php echo $aENV['client_name'].' | '.$aENV['system_name'][$syslang].' / '.$sTopnaviTitle; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $aENV['charset']; ?>">
	<meta http-equiv="content-language" content="<?php echo $syslang; ?>">
	<meta name="robots" content="noindex">
	<meta name="copyright" content="design aspekt; http://www.design-aspekt.com">
	<meta name="author" content="design aspekt; http://www.design-aspekt.com">
	<meta name="generator" content="e-mail: kontakt@design-aspekt.com">
	<meta name="date" content="2004-07-20">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	
<link href="<?php echo $aENV['path']['css']['http'].$sStylesheet; ?>styles.css" type="text/css" rel="stylesheet">
<style type="text/css">
<?php require_once($aENV['path']['config']['unix'].$sStylesheet."styles.php"); ?>
</style>

<link href="<?php echo $aENV['path']['css']['http'].$sStylesheet; ?>topnavi.css" type="text/css" rel="stylesheet">
<style type="text/css">
<?php if ($_GET['printview'] != "true") { require_once($aENV['path']['config']['unix']."styles_topnavi.php"); } ?>
</style>

<script language="JavaScript" type="text/javascript">
	var sysPath = '<?php echo $aENV['path']['sys']['http']; // VOR der "sys_functions.js"! ?>';
	var calPath = '<?php echo $aENV['path']['cal']['http']; // VOR der "sys_functions.js"! ?>';
	var mediaDbPath = '<?php echo $aENV['path']['mdb']['http']; // VOR der "sys_functions.js"! ?>';
	var modulPath = '<?php echo $aENV['SELF_PATH']; // VOR der "*functions.js"! ?>';
</script>
<script src="<?php echo $aENV['path']['js']['http']; ?>sys_functions.js" language="JavaScript" type="text/javascript"></script>
<?php if (file_exists("./scripts/functions.js")) { ?><script src="<?php echo $aENV['SELF_PATH']; ?>scripts/functions.js" language="JavaScript" type="text/javascript"></script><?php } ?>

</head>

<?php if ($aBnParts[0] == "popup") { // bei popup NUR (popup-)BODY ?>
<body class="bodyPopup">

<?php } elseif (isset($_GET['printview']) && $_GET['printview'] == "true") { // bei Druckansicht NUR BODY ?>
<body>

<?php } else { // sonst body + logo + navi ?>
<body>

<?php if (isset($Userdata) || !empty($Userdata)) { // nicht bei login-screen ?>

<div id="toplinks">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="right">
<iframe src="../sys/inc.reloader.php" name="reloader_in_a_iframe" scrolling="no" marginheight="0" marginwidth="0" frameborder="0" allowtransparency="true" class="iframe"></iframe></td></tr></table>
</div>

<?php } // END nicht bei login-screen ?>
		
<!-- Header-Box -->
<div id="header">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
	<td class="cLogo"><a href="<?php echo $aENV['path']['sys']['http'].'index.php'; ?>" title="<?php echo $aENV['client_name']; ?>"><img src="<?php echo $aENV['path']['config']['http']; ?>logo.gif" alt="<?php echo $aENV['client_name']; ?>" border="0" class="logo"></a></td>
</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="10" alt="" border="0"><br>

<?php 
// TOPNAVI (nur anzeigen, wenn man eingeloggt ist)
if (isset($Userdata) && !empty($Userdata)) {
	// array-keys des aktuellen navigationspunktes fuers highlight ermitteln
	$aHighlight = $oTopnavi->getCurrentKeys();
	// MS-IE + Opera immer offene Topnavi geben!
	if ($oBrowser->isIE() || $oBrowser->isOpera()) {
		$aENV['navigation_open'] = true;
	}
	// TOPNAVI ANSICHT "immer offen" //////////////////////////////////////////////////////////////////////////
	if (isset($aENV['navigation_open']) && $aENV['navigation_open'] == true) { ?>
<table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr valign="top">
	<?php
		$sW = round(100 / (count($aTOPNAVI)+1));
		foreach ($aTOPNAVI as $key1 => $main) {
			$count_sub = count($main);
			echo '<td width="'.$sW.'%" class="headerbk"><table width="100%" border="0" cellspacing="0" cellpadding="0">';
			if ($count_sub > 1) {
				for ($key2=1; $key2<$count_sub; $key2++) {
					$hi = ($aHighlight['key2'] == $key2 && $aHighlight['key1'] == $key1) ? 'hi' : '';
					echo '<tr><td class="naviBgopen'.$hi.'"><a href="'.$main[$key2]['href'].'" onFocus="this.blur()" class="navi'.$hi.'">'.$main[$key2]['title']."</a></td></tr>";
				}
			}
			echo "</table></td>";
		}
	?>
	
<?php if (isset($aENV['module']['adr']) && file_exists("../adr/inc.mod_adrsearch.php") && $oPerm->hasPriv('view', 'adr')) { ?>
	<td width="<?php echo $sW ?>%" class="naviBgopen" nowrap>
<?php require_once ("../adr/inc.mod_adrsearch.php"); ?>
	</td>
<?php } ?>
</tr>
</table><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"><br>
<?php 
	} else {
	// TOPNAVI ANSICHT "aufklappmenus" //////////////////////////////////////////////////////////////////////////
		if (!defined('SYS_TOPNAVI_WIDTH')) define('SYS_TOPNAVI_WIDTH', 130); // set default
?>
<script language="javascript" type="text/javascript"><!--//--><![CDATA[//><!--
	startList = function() {	// IE-workaround
		if (document.all && document.getElementById) {	// IE
			navRoot = document.getElementById("nav");
			for (i=0; i<navRoot.childNodes.length; i++) {
				node = navRoot.childNodes[i];
				if (node.nodeName == "LI") {	// aenderungen fuer alle <LI>
					node.onmouseover = function() {	this.className += " over"; }
					node.onmouseout = function() { this.className = this.className.replace(" over", ""); }
				}
			}
		}
	}
	window.onload = startList;
//--><!]]></script>
<br><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="<?php echo (count($aTOPNAVI) * SYS_TOPNAVI_WIDTH) + count($aTOPNAVI); ?>">
		<ul id="nav" style="width:<?php echo (count($aTOPNAVI) * SYS_TOPNAVI_WIDTH) + count($aTOPNAVI); ?>;">
			<?php
			foreach ($aTOPNAVI as $key1 => $main) {
				$count_sub = count($main);
				echo "<li>";
				echo "\n\t\t\t\t".'<a href="#" onFocus="this.blur()" class="topNavi">'.$main[0]['title']."</a>";
				
				if ($count_sub > 1) {
					for ($key2=1; $key2<$count_sub; $key2++) {
						if ($aHighlight['key2'] == $key2 && $aHighlight['key1'] == $key1) {
							echo "\n\n\t\t\t\t".'<ul class="hi">';
							echo "\n\t\t\t\t\t<li><b>".$main[0]['title']."</b></li>";
							echo "\n\t\t\t\t\t".'<li><a href="'.$main[$key2]['href'].'" onFocus="this.blur()">'.$main[$key2]['title']."</a></li>";
							echo "\n\t\t\t\t</ul>";
						}
					}
					echo "\n\n\t\t\t\t<ul>";
					echo "\n\t\t\t\t\t<li><b>".$main[0]['title']."</b></li>";
					for ($i=1; $i<$count_sub; $i++) {
						echo "\n\t\t\t\t\t".'<li><a href="'.$main[$i]['href'].'" onFocus="this.blur()">'.$main[$i]['title']."</a></li>";
					}
					echo "\n\t\t\t\t</ul>";
				}
				echo "\n\n\t\t\t</li>\n\n\t\t\t";
			}
			?>
		</ul>
	</td>
	<td width="*" align="right" class="naviBg" nowrap>
<?php // ADR-Search
	if (isset($aENV['module']['adr']) && file_exists("../adr/inc.mod_adrsearch.php") && $oPerm->hasPriv('view', 'adr')) {
		require_once ("../adr/inc.mod_adrsearch.php");
	} ?></td>
</tr>
</table>
<?php
	} // END TOPNAVI ANSICHT //////////////////////////////////////////////////////////////////////////
} // END if Userdata ?>
</div>

<!-- Content-Box -->
<div id="DIVcontent">
<!-- Rahmentabelle -->
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr valign="top">
<?php } // END if popup 
	}
?>