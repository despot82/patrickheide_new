<?php
	// require system root
	require_once("./php/_include_all.php");
	// require login
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// execute sql statements
	if(isset($_POST['sql']) 
	&& !empty($_POST['sql'])) {
		$oDb->debug	= true;
		
		foreach($_POST['sql'] as $sql) {
			$oDb->query(stripslashes($sql));
		}
	}

// HTML
	require_once ($aENV['path']['config']['unix']."cms.config.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>
eine neue Sprache der Datenbank hinzufügen<br />
bitte nicht vergessen bei den Collectiontemplates die<br />
setField-Anweisung um evtl. "title_" und "flag_online_" zu erweitern
<form action="" method="POST">
<?php
// foreach aENV content language 
foreach($aENV['content_language'] as $sKey => $sValue) {
	// if sKey == key default continue
	if($sKey=='default') continue;
	// write the array compare 
	$aCompare[]	= $sKey;	
}

// temporary key
$sKey	= 'Tables_in_'.$aENV['db']['db'];
// request all tables from db
$sql	= "SHOW TABLES FROM `".$aENV['db']['db']."`";
$oDb->query($sql);

// read all except these system tables
$aExcept	= array('cms_help',
					'adr_group',
					'cug_usergroup',
					'sys_privilege');
					
// read all to array temp fetch array					
while($aTempFA[] = $oDb->fetch_array()) {
}

// foreach aTempFA
foreach($aTempFA as $aData) {
	// if table is in except array continue
	if(in_array($aData[$sKey], $aExcept)) continue;
	// if array aData is empty continue
	if(empty($aData)) continue;
	
	// request all columns from table aData sKey
	$sql	= "SHOW COLUMNS FROM `".$aData[$sKey]."`";
	$oDb->query($sql);
	
	// initialise array temp compare
	$aTempC	= array();
	// read all columns to array temp fetch array 2
	while($aTempFA2 = $oDb->fetch_array()) {
		// test for a language dependent fields
		preg_match('/\_(\w{2})$/', $aTempFA2['Field'], $aMatch);
				
		// if it's no language dependent field continue
		if(empty($aMatch)) continue;
		// if match is not in array compare continue
		if(!in_array($aMatch[1], $aCompare)) continue;
		
		// foreach array compare to v
		foreach($aCompare as $v)
			// if field for language exists save to array Tables
			if(substr($aTempFA2['Field'], -2) == $v) {
				$aTables[$aData[$sKey]][substr_replace($aTempFA2['Field'], '', -3)][$v]	= $aTempFA2['Field'];
				$aInfo[$aData[$sKey]][substr_replace($aTempFA2['Field'], '', -3)][$v]	= $aTempFA2;
			}
	}
}

// foreach array Tables as key string sTable / value array aData
foreach($aTables as $sTable => $aData) {
	// foreach array aData as key string sField / value array temp data
	foreach($aData as $sField =>$aTempD) {
		// if field exists for language continue
		if(count($aTempD) == count($aCompare)) continue;
		
		// array temp field information	
		$aTempTI	= $aInfo[$sTable][$sField];
		
		// foreach array compare as key k / value v			
		foreach($aCompare as $k => $v) {
			// string sLastField out of fieldname and language
			$sLastField	= "{$sField}_".$aCompare[$k-1];

			// the type of the field
			$sType		= $aTempTI[$aCompare[0]]['Type'];
			// is the field a NULL or not
			$sNull		= ($aTempTI[$aCompare[0]]['Null']=='NO') ? 'NOT NULL':'NULL';
			// the default value of the field
			$sDefault	= $aTempTI[$aCompare[0]]['Default'];
			
			// if the field not exists write the sql to a string, and echo the html 
			if(!in_array("{$sField}_{$v}", $aTempD))  {
				$sSql	= sprintf("ALTER TABLE `%1\$s` ADD %2\$s %3\$s %4\$s AFTER %5\$s;", 
								  $sTable, 
								  "{$sField}_{$v}", 
								  $sType, 
								  $sNull,
								  $sLastField);
				$aSql[]	= $sSql;								  
				echo '<input type="checkbox" checked="checked" name="sql[]" value="'.$sSql.'" /> '.$sSql.'<br />';
			}								  
		}
	}
}
?>
<br />
<?php
// if there is no sql, don't output the submitbutton
if(!empty($aSql)) {
?>
<input type="submit" class="btn_small" value="eintragen" /><br />
<?php
}
?>
</form>
