<?php
/**
* 404-page for system ONLY [en|de] !!!
*
* provides common 404-message.
*
* @param	string	$msg	Kuerzel fuer welche Message ausgegeben werden soll
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.1 / 2003-04-17
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_404.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// init
	require_once ("php/_include_all.php");

// vars ********************************************************************************
	$redirectOK			= $aENV['page']['sys_welcome'];	// go to startpage of this system
	$syslang			= $aENV['system_language']['default'];
	$nameOfClient		= $aENV['client_name'];
	$nameOfSystem		= $aMSG['std']['system_name'][$syslang];
// END vars ****************************************************************************

// HTML:
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>

<table cellspacing="10" cellpadding="10" border="0" width="600"><tr><td><br>

<?php // engl. version
if ($syslang == "en") { ?>

<div class="title">Welcome to <?php echo $nameOfClient.' '.$nameOfSystem; ?><br><br><br></div>
<div class="text">
The page you required does not exist any more. Please update your bookmarks.<br><br>
<b>Thanks</b>.<br><br><br>
If you are not forwarded automatically, please follow this link:<br>
<a href="<?php echo $redirectOK; ?>"><?php echo $nameOfClient.' '.$nameOfSystem; ?></a>
</div>

<?php
}
// deutsche version

if ($syslang == "de") { ?>

<div class="title">Willkommen im <?php echo $nameOfClient.' '.$nameOfSystem; ?><br><br><br></div>
<div class="text">
Die von Ihnen angeforderte Seite existiert nicht mehr. Bitte aktualisieren Sie Ihre Bookmarks.<br><br>
<b>Vielen Dank</b>.<br><br><br>
Sollten Sie nicht automatisch weitergeleitet werden, klicken Sie bitte hier:<br>
<a href="<?php echo $redirectOK; ?>"><?php echo $nameOfSite.' '.$nameOfSystem; ?></a>
</div>

<?php } ?>

</td></tr></table><br>

<script language="JavaScript" type="text/javascript">
function redirect() {
	document.location.href = '<?php echo $aENV['page']['sys_welcome']; ?>';
}
setInterval('redirect()', 3000);
</script>
  

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
</html>
