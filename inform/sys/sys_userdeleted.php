<?php
/**
* sys_user_deleted.php
*
* Overviewpage: user 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2005-11-14 (NEUES RECHTEMANAGEMENT - NICHT MEHR ABWAERTSKOMPATIBEL!)
* #history	1.0 / 2004-04-27
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_userdeleted.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("./php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");

// diese seite ist nur was fuer admins ;-)
	if ($oPerm->hasPriv('admin') == false) { echo js_history_back(); }

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	$restore_id		= (isset($_GET['restore_id'])) ? $oFc->make_secure_int($_GET['restore_id']) : '';
	$sGEToptions	= "?start=".$start;
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= $aENV['table']['sys_user'];
	$limit			= 20;	// Datensaetze pro Ausgabeseite einstellen // noetig fuer DB-Navi!
	$sEditPage		= $aENV['SELF_PATH']."sys_user_detail.php";	// fuer EDIT-link

// OBJECTS
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)

// 3. (DB) restore deleted user
	if ($restore_id > 0) {
		$aData['flag_deleted'] = '0';
		$aData['last_mod_by'] = $Userdata['id'];
		$oDb->make_update($aData, $sTable, $restore_id);
		// weiterleitung
		header("Location: ".$sEditPage."?id=".$restore_id); exit;
	}
// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

/**********************************************************************
Das ist ein ERSTER SCHRITT das intranet auf HTML-TEMPLATES umzubauen...
**********************************************************************/

// PHPLIB Templatesystem einbinden ############################################
	// Instanz der Templateklasse erzeugen, Pfad zu den Templates angeben
	$tpl =& new Template_PHPLIB("templates/", "comment"); // [keep|remove|comment]
	
	// Dokumentvorlage laden
	$tpl->setFile(array("main" => "sys_user.html"));
	$tpl->setBlock("main", "user", "u");
	
	// BLOCK Ersetzungen vorbereiten
	$aUser = $oUser->getAllUserData(false, $start, $limit, "WHERE `flag_deleted` = '1'"); // params: [$bValidOnly=true][,$start=0][,$limit=0][,$sOverwriteConstraint='']
	$entries = $oUser->getEntries();	// noetig fuer DB-Navi
	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
	foreach ($aUser as $uid => $aData) {
		// NUR geloeschte user anzeigen
		if ($aData['flag_deleted'] != '1') continue;
		// vars
		$sGEToptions = "?restore_id=".$aData['id']."&start=".$start;
		// td style
		$sTdStyle = ' class="sub2"';#off
		// name
		$sTdName = $aData['firstname'].' '.$aData['surname'];
		// contact
		$sEmail	= !empty($aData['surname'])	? $aData['surname']:'';
		$sEmail	.= !empty($aData['firstname'])	? ' '.$aData['firstname']:'';
		$sEmail	.= !empty($sEmail)	? ' <'.$aData['email'].'>':$aData['email'];		
		$sTdContact = '<a href="mailto:'.$sEmail.'">'.$aData['email'].'</a>';
		if ($aData['phone']) { $sTdContact .= "<br>".$aData['phone']; }
		// last-login
		$sTdLastlogin = ''.$oDate->datetime_mysql2trad($aData['last_login']).'<br>'; 
		// link/name
		$sTdRestore = '<a href="'.$aENV['PHP_SELF'].$sGEToptions.'" title="'.$aMSG['btn']['restore'][$syslang].'">';
		$sTdRestore .= '<img src="'.$aENV['path']['pix']['http'].'btn_restore.gif" border="0" alt="'.$aMSG['btn']['restore'][$syslang].'" class="btn"></a>';
	// BLOCK Ersetzungen vornehmen
		$tpl->setVar(array(
			"TD_STYLE"		=> $sTdStyle,
			"TD_NAME"		=> $sTdName,
			"TD_CONTACT"	=> $sTdContact,
			"TD_LASTLOGIN"	=> $sTdLastlogin,
			"TD_EDIT"		=> $sTdRestore
		));
		$tpl->parse("u", "user", true);
	}
	
	// Subnavi zusammenbauen
	include_once("inc.sys_detnavi.php"); // stellt $SUBNAVI zur Verfuegung!
	$SUBNAVI = getSysUserSubnavi();
	
	// STD Ersetzungen vornehmen
	$tpl->setVar(array(
		"HR"			=> HR,
		"SUBNAVI"		=> $SUBNAVI,
		"STD_HEADLINE"	=> $aMSG['topnavi']['users'][$syslang],
		"TH_NAME"		=> $aMSG['form']['name'][$syslang],
		"TH_CONTACT"	=> $aMSG['user']['contact'][$syslang],
		"TH_LASTLOGIN"	=> $aMSG['std']['last_login'][$syslang],
		"TH_RESTORE"	=> $aMSG['btn']['restore'][$syslang],
		"DBNAVI_STATUS"	=> $oDBNavi->getDbnaviStatus(), // params: $nEntries[,$nStart=0][,$nLimit=20]
		"DBNAVI_LINKS"	=> $oDBNavi->getDbnaviLinks() // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
	));
	
	// Seite ausgeben  
	$tpl->pParse("out",array("main")); #######################################

	require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
