<?php
/**
* popup_textmodule.php
*
* Popup Textbausteine
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$delid		welcher Datensatz soll geloescht werden (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @author 	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 / 2006-05-04	-> 
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_textmodule.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.Label.php");
	
// 2a. GET-params abholen
	$delid = (isset($_GET['delid'])) ? $_GET['delid'] : 0;
	$remotesave = (isset($_GET['remotesave'])) ? $_GET['remotesave'] : '';
	$flagtype = (isset($_GET['flagtype'])) ? $_GET['flagtype'] : '';
	$parent = (isset($_GET['parent'])) ? $_GET['parent'] : 0;
	$bWithColorPicker = (isset($_GET['bWithColorPicker'])) ? $_GET['bWithColorPicker'] : 'true';
	$parentField = (isset($_GET['parentField']) && $_GET['parentField'] != "") ? $_GET['parentField'] : 'label_id';
	$parentForm = (isset($_GET['parentFormName']) && $_GET['parentFormName'] != "") ? $_GET['parentFormName'] : 'editForm';
	
// 2b. POST-params abholen
	$btn = (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$aData = (isset($_POST['aData'])) ? $_POST['aData'] : array();
	
// 2c. Vars
	$sGEToptions = '?flagtype='.$flagtype.'&parent='.$parent.'&parentField='.$parentField.'&parentFormName='.$parentForm;
	$sEditPage = 'popup_textmodule.php'.$sGEToptions;
	
	$oLabel =& new Label($oDb,$aENV,$flagtype,$parent);

// DB
	if($delid > 0 || isset($btn['delete_all'])) {
		$oLabel->delete($delid,$btn);
		header("Location: ".$sEditPage);
	}
 
 	if(isset($btn['save']) || isset($btn['save_close']) || $remoteSave == 1) {

		$oLabel->save($aData,$Userdata);

 		if(isset($btn['save_close'])) {
 			header('Location: '.$sViewerPage);
 		}
 		if(isset($btn['save'])) {
 			header('Location: '.$sEditPage);
 		}
 	}

	$aData = $oLabel->getLabels("id ASC");

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	
// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
?>

<script language="JavaScript" type="text/javascript">
	var aTexte = new Array();
	<?php
	for($i=0;list($id, $aData2) = each($aData);$i++) {
		echo "aTexte[$id] = '".nl2br_cms($aData2['name'])."';\r\n";
	}
	reset($aData);
	?>
	function copyToParent(id) {
		sText = opener.document.forms['<?=$parentForm?>'].elements['aData[<?=$parentField?>]'].value;
		opener.document.forms['<?=$parentForm?>'].elements['aData[<?=$parentField?>]'].value = sText + str_replace(aTexte[id],"<br />","\n");
	}
</script>

<div id="contentPopup">

<span class="title"><?=$aMSG['textmodule']['title'][$syslang]?><br></span>
<br>
<?php echo $oForm->start_tag($_SERVER['PHP_SELF'].$sGEToptions,'','post','textmoduleEditForm',''); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php 
 	 	
	for($i=0;list($id, $aData2) = each($aData);$i++) {
		$oForm->aData['label_'.$id] = $aData2['name'];
?>
<tr valign="top">
	<td width="5%"><span class="small"> </span></td>
	<td width="80%">
		<?=$oForm->textarea('label_'.$id, 5, 75);?>
	</td>
	<td width="10%"><a href="JavaScript:copyToParent(<?=$id?>)" title="<?php echo $aMSG['btn']['takeacross'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_save.gif" alt="<?php echo $aMSG['btn']['takeacross'][$syslang]; ?>" class="btn"></a><a href="<?php echo $_SERVER['PHP_SELF'].$sGEToptions; ?>&delid=<?php echo $id; ?>" title="<?php echo $aMSG['btn']['delete'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_delete.gif" alt="<?php echo $aMSG['btn']['delete'][$syslang]; ?>" class="btn"></a></td>
</tr>

<?php
	} // END for
?>
<tr><td colspan="3" class="off"></td></tr>
<tr valign="top">
	<td width="5%"><span class="small">+</span></td>
	<td width="90%" colspan="2">
		<?=$oForm->textarea('newlabel', 5, 75);?>
	</td>
</tr>
<tr><td colspan="3" class="off"></td></tr>
</table>

<?php
 	echo $oForm->button("SAVE"); // params: $sType[,$sClass='']
	//echo $oForm->button("DELETE_ALL"); // params: $sType[,$sClass='']
	echo $oForm->button("CLOSE_WIN"); 

echo $oForm->end_tag();
?>

<br>
</div>

</body>
</html>