<?php
/**
* sys_user_team.php
*
* Overview-page: teams
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-01-24
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './sys_user_team.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= $aENV['table']['sys_team'];
	$sEditPage		= $aENV['SELF_PATH']."sys_user_team_detail.php";	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;					// fuer NEW-button
	
// OBJECTS
	$oTeam =& new team($oDb);
	$oUser =& new user($oDb);

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>

<form>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['teams'][$syslang]; ?></span></p></td>
		<td align="right"><?php echo get_button("NEW", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"] ?></td>
	</tr>
</table>

<?php echo HR; ?>

<?php include_once("inc.sys_detnavi.php"); 
	echo getSysUserSubnavi(); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<th width="30%"><?php echo $aMSG['form']['name'][$syslang]; ?></th>
		<th width="60%"><?php echo $aMSG['form']['username'][$syslang]; ?></th>
		<th width="10%">&nbsp;</th>
	</tr>
	
<?php // DB 
	$aData = $oTeam->getAllTeamNames();
	foreach ($aData as $id => $teamname) {
		// GET-options
		$sGEToptions = "?id=".$id;
		// user dieses teams ermitteln
		$aUser = $oUser->getUserdatas($oTeam->getTeamUserIds($id));
		$aTamUserName = array(); // container
		foreach($aUser as $uid => $vals) {  
			// DA-Service-Login nur für DA-Service-Login anzeigen
			if ($vals['flag_da_service'] == '1' && !$oPerm->isDaService()) continue;
			// NAME
			$username = $vals['firstname'].' '.$vals['surname'];
			if ($vals['flag_deleted'] == '1') {
				// geloeschte user markieren und nicht verlinken
				$username = '<span style="text-decoration:line-through">'.$username.'</span>';
			} elseif ($oPerm->hasPriv('admin')) {
				// sonst - NUR wenn admin! - verlinken
				$username = '<a href="'.$aENV['SELF_PATH'].'sys_user_detail.php?id='.$uid.'">'.$username.'</a>';
			}
			// sammeln
			$aTamUserName[] = $username;
		}
		// team-description ermitteln
		$aTeamDescription = $oTeam->getTeamDescription($id);
		// teamname ggf. verlinken
		if ($oPerm->hasPriv('admin')) {
			$teamname = view_link($teamname, $sEditPage.$sGEToptions); // params: $sText[,$sHref=''][,$nStatus=0][,$nLength=80][,$bFallback=true]
		}
?>
	<tr><td colspan="3" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td height="20"><p><?php // title(-link)
			echo $teamname;
		?></p></td>
		<td><p><?php // usernames ausgeben
			echo implode(', ',$aTamUserName);
		?></p></td>
		<td class="sub2" align="right" nowrap><p><?php // edit-button (NUR admin!)
			if ($oPerm->hasPriv('admin')) {
				?><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['std']['edit'][$syslang]; ?>" class="btn"></a><?php
			} 
		?></p></td>
	</tr>
	<tr>
		<td colspan="3" class="sub2"><small><?php // team-description
			echo $aTeamDescription[$id];
		?></small></td>
	</tr>
<?php
	}  // END while and 'no-data'-string
	if ($oTeam->countTeams() == 0) { echo '	<tr><td colspan="3" align="center"><span class="text">'.$aMSG['std']['no_entries'][$syslang].'</span></td></tr>'; }
?>
</table>
<form>
<br>


<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
