<?php
/**
* popup_editlabels.php
*
* Popup Label/Colourpickerpage 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$delid		welcher Datensatz soll geloescht werden (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @author 	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.1 / 2006-04-04	-> bWithColorPicker=false: Fehler CSS, JavaScript sowie Folgefehler nach Speichern behoben [oh]
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_editlabel.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.Label.php");
	
	// 2a. GET-params abholen
	$delid = (isset($_GET['delid'])) ? $_GET['delid'] : 0;
	$remotesave = (isset($_GET['remotesave'])) ? $_GET['remotesave'] : '';
	$flagtype = (isset($_GET['flagtype'])) ? $_GET['flagtype'] : '';
	$parent = (isset($_GET['parent'])) ? $_GET['parent'] : 0;
	$bWithColorPicker = (isset($_GET['bWithColorPicker'])) ? $_GET['bWithColorPicker'] : 'true';
	$parentField = (isset($_GET['parentField']) && $_GET['parentField'] != "") ? $_GET['parentField'] : 'label_id';
	
	// 2b. POST-params abholen
	$btn = (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$aData = (isset($_POST['aData'])) ? $_POST['aData'] : array();
	
	// 2c. Vars
	$sGEToptions = '?flagtype='.$flagtype.'&parent='.$parent.'&parentField='.$parentField.'&bWithColorPicker='.$bWithColorPicker;
	$sEditPage = 'popup_editlabels.php'.$sGEToptions;
	$oLabel =& new Label($oDb,$aENV,$flagtype,$parent);
	
	
	/* Test am 14.02.06! - funktioniert nicht zuverlässig.
	if($parent != 'business' && $flagtype != 'cal') {
		// wenn wir uns nicht im kalendar befinden dann raeumen wir bisschen auf.
		$sql = "SELECT * FROM `sys_labels`";
		$oDb->query($sql);
		$oDb2 =& new db_connect($aENV);
		for($i=0;$row=$oDb->fetch_object();$i++) {
			$sql = 'SELECT * FROM `'.$aENV['global_tree'].'` WHERE `id`='.$row->id;
			$oDb2->query($sql);
			if($oDb2->num_rows() == 0) {
				$oDb->make_delete(array('id' => $row->id),'sys_labels');	
			}
		}
	}*/
	
	if($delid > 0 || isset($btn['delete_all'])) {
		$oLabel->delete($delid,$btn);
		header("Location: ".$sEditPage);
	}
 
 	if(isset($btn['save']) || isset($btn['save_close']) || $remoteSave == 1) {

		$oLabel->save($aData,$Userdata);

 		if(isset($btn['save_close'])) {
 			header('Location: '.$sViewerPage);
 		}
 		if(isset($btn['save'])) {
 			header('Location: '.$sEditPage);
 		}
 	}


// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
?>

<script language="JavaScript1.2" src="scripts/colorpicker.js"></script>
<script language="JavaScript" type="text/javascript">
// bgColors der TRs (kann NICHT durch das stylesheet definiert werden!)
var bgClassHighlight = 'sub3';
var bgClassNormal = 'sub1';
var memoryId;
var aLabels = new Array();
var aId = new Array();

function updateParentColors(parentField) {
	var selectedLabel = opener.document.forms['editForm'].elements['selectedLabelId'];
	var oParentlabels = opener.document.forms['editForm'].elements['aData[' + parentField + ']'];
	
	oParentlabels.length = 1;
	oParentlabels.options[0].value = '';
	oParentlabels.options[0].text = '--';
	oParentlabels.options[0].style.backgroundColor = '#fff';
	
	for (e=1; e<=aLabels.length; e++) {
		var obj = document.getElementById(aLabels[e-1]);
		var objHidden = document.getElementById(aLabels[e-1]+'field');
		if (obj.value != "") {
			oParentlabels.length += 1;
			oParentlabels.options[e].value = aId[e-1];
			oParentlabels.options[e].text = obj.value;
			if(aId[e-1] == selectedLabel.value) {
				oParentlabels.options[e].selected = true;
			}
			
			<?php if ($bWithColorPicker == "true") { echo 'oParentlabels.options[e].style.backgroundColor = objHidden.value;'; } ?>
			//alert(oParentlabels.options[e].text + " / " + oParentlabels.options[e].value);
		}
	}
}		
</script>

<div id="contentPopup">

<span class="title"><?=$aMSG['label']['title'][$syslang]?><br></span>
<br>
<?php echo $oForm->start_tag($_SERVER['PHP_SELF'].$sGEToptions,'','post','coloreditForm',''); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php 
 	
 	$sql = "SELECT * FROM `sys_labels` WHERE `flag_type`='$flagtype' AND `parent`='$parent' ORDER BY `id` ASC";
 	$oDb->query($sql);
 	
	for($i=0;$row = $oDb->fetch_object();$i++) {
		$oForm->aData['label_'.$row->id.'field'] = $row->color;
		$sExtra = ($bWithColorPicker == "true") ? 'onClick="pickColor(\'label_'.$row->id.'\');"' : '';
		$sExtra2 = ($bWithColorPicker == "true") ? ' onChange="relateColor(\'label_'.$row->id.'\', this.value);"' : '';
?>
<tr valign="top">
	<td width="10%"><span class="small"><?php echo ($i+1); ?></span></td>
	<td width="80%">
		<input type="text" name="aData[label_<?php echo $row->id; ?>]" id="label_<?php echo $row->id; ?>" style="width:100%;<? if($bWithColorPicker == "true" && $row->color != "") { ?> background-color:<?php echo $row->color; } ?>;" <?=$sExtra?> value="<?php echo $row->name; ?>">
		<?php echo $oForm->hidden('label_'.$row->id.'field','',$sExtra2); ?><script>aLabels.push('label_<?php echo $row->id; ?>');aId.push('<?php echo $row->id; ?>');</script>
	</td>
	<td width="10%"><a href="<?php echo $_SERVER['PHP_SELF'].$sGEToptions; ?>&delid=<?php echo $row->id; ?>" title="<?php echo $aMSG['btn']['delete'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_delete.gif" alt="<?php echo $aMSG['btn']['delete'][$syslang]; ?>" class="btn"></a></td>
</tr>
<?php
	} // END for
	$sExtra = ($bWithColorPicker == "true") ? ' onClick="pickColor(\'newlabel\');"' : '';
?>
<tr valign="top">
	<td width="10%"><span class="small">+</span></td>
	<td width="80%">
		<input type="text" name="aData[newlabel]" id="newlabel" style="width:100%;"<?=$sExtra?>>
		<?php
			$sExtra2 = ($bWithColorPicker == "true") ? ' onChange="relateColor(\'newlabel\', this.value);"' : ''; 
			echo $oForm->hidden('newlabelfield','',$sExtra2); ?>
		<script>aLabels.push('newlabel');</script>
	</td>
	<td width="10%"></td>
</tr>

</table>
<br>

<?php
 	echo $oForm->button("SAVE"); // params: $sType[,$sClass='']
	//echo $oForm->button("DELETE_ALL"); // params: $sType[,$sClass='']
	echo $oForm->button("CLOSE_WIN"); 

echo $oForm->end_tag();
?>
<script>updateParentColors('<?php echo $parentField; ?>');</script>
<br>
</div>

</body>
</html>