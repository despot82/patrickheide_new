<?php
/**	
*	Script zur automatischen Thumbnail-Generierung (und speichert sie in einem Unterordner)
*
* @param:	string	$image			kompletter (relativer!) Pfad zum Originalbild
* @param:	int		$nWidth				gewuenschte Breite des Thumbnails
* @param:	int		$nHeight				gewuenschte Hoehe des Thumbnails
* @param:	int		$resize			Vergroesserungen zuzulassen? [0|1] (default: 0)
* @param:	int		$aspectratio	Bildproportionen erhalten? [0|1] (default x|y: 0, x+y: 1)
*
* @version	1.0 / 2007-07-10 
*/

	$types = array (1 => "gif", "jpeg", "png", "swf", "psd", "wbmp");

	// GET-vars abholen
	$sFileName	= (isset($_GET['image'])) ? strip_tags($_GET['image']) : '';
	$nWidth		= (isset($_GET['w'])) ? $_GET['w'] + 0 : '';
	$nHeight	= (isset($_GET['h'])) ? $_GET['h'] + 0 : '';
	$bRatio		= (isset($_GET['ratio']) && is_int($_GET['ratio'])) ? $_GET['ratio'] : NULL;
	$sCacheDir	= (isset($_GET['cachedir'])) ? strip_tags($_GET['cachedir']) : '';
	
	if (empty($sFileName) 
	|| !file_exists($sFileName)) { DIE('Es wurde kein Bild angegeben oder konnte nicht gefunden werden!'); }
	
	(empty($sCacheDir)) ? define ('CACHE', 'thumbs/'):define('CACHE', $sCacheDir.'/');	
	
	if (empty($nWidth))		unset($nWidth);
	if (empty($nHeight))	unset($nHeight);
	if (empty($bRatio))		unset($bRatio);

	if (!isset($nHeight) && !isset($nWidth)) { DIE('Fehlende(r) Groessenparameter!'); }
	
	if (function_exists("gd_info")) { 

		$gd_info	= gd_info();
		$aImgData	= getimagesize($sFileName);
		
		if (	(strToLower($types[$aImgData[2]]) == "gif"	&& $gd_info['GIF Create Support'] == false)
			 ||	(strToLower($types[$aImgData[2]]) == "jpeg"	&& $gd_info['JPG Support'] == false)
			 ||	(strToLower($types[$aImgData[2]]) == "png"	&& $gd_info['PNG Support'] == false)
			 ||	(strToLower($types[$aImgData[2]]) == "wbmp"	&& $gd_info['WBMP Support'] == false)
			 ||	(strToLower($types[$aImgData[2]]) == "psd")) {
			$icon = "icn_image.gif";
		}
		
	} else {
		
		$icon = "icn_image.gif";
		
	} // end else
	
	if (strToLower($types[$aImgData[2]]) == "swf") {
		$icon = "icn_flash.gif";
	}
	
	if (!empty($icon)) {
		require_once ("../sys/php/_include_all.php");
		
		$icon	= $aENV['path']['pix_global']['unix'].$icon;
		
		header("Content-Type: image/gif");
		
		$i	= fopen($icon, "rb");
		fpassthru($i);
		fclose($i);
		
		exit;
	} 

	$sCacheDir	= substr($sFileName, 0, strrpos($sFileName, '/') + 1).CACHE;
	
	(!is_dir($sCacheDir))
		? mkdir($sCacheDir, 0777)
		: system("chmod 0777 ".$sCacheDir);
	
	(!$aImgData[2] || $aImgData[2] == 4 || $aImgData[2] == 5)
		? DIE ('Bei der angegebenen Datei handelt es sich nicht um ein Bild!')
		: false;
	
	eval ('
		if (!(imagetypes() & IMG_'.strtoupper($types[$aImgData[2]]).')) {
			DIE ("Das ".strtoupper($types[$aImgData[2]])."-Format wird nicht unterstuetzt!");
		}
	');
	
	(!isset($bRatio))
		? (isset($nWidth) && isset($nHeight)
			? $bRatio = true
			: $bRatio = false)
		: $bRatio;
	
	(!isset($nWidth))
		? $nWidth = floor($nHeight * $aImgData[0] / $aImgData[1])
		: $nWidth;
	
	(!isset($nHeight))
		? $nHeight = floor($nWidth * $aImgData[1] / $aImgData[0])
		: $nHeight;
	
	if(!isset($bRatio)
	&& !empty($nHeight)
	&& !empty($nWidth)) {
		
		if ($aImgData[0] > $aImgData[1]) {
			
			$nHeight = floor ($nWidth * $aImgData[1] / $aImgData[0]);
			
		} else if ($aImgData[1] > $aImgData[0]) {
			
			$nWidth = floor ($nHeight * $aImgData[0] / $aImgData[1]);
			
		} // end else
		
	} // end if

	$sThumbFile = substr($sFileName, strrpos($sFileName, '/') + 1);
	
	if (file_exists($sCacheDir.$sThumbFile)) {
		$aThumbdata = getimagesize($sCacheDir.$sThumbFile);
		$aThumbdata[0] == $nWidth && $aThumbdata[1] == $nHeight
			? $bIsCached	= true
			: $bIsCached	= false;
	} else {
		$bIsCached = false;
	}

	if ($bIsCached == false) {
		($aImgData[0] > $nWidth || $aImgData[1] > $nHeight) ||
		(($aImgData[0] < $nWidth || $aImgData[1] < $nHeight) && $resize)
			? $bMakeThumb = true
			: $bMakeThumb = false;
	} else {
		$bMakeThumb = false;
	}

	// if thumbnail has to be created, do so
	if ($bMakeThumb == true) {
	
		$rImage	= call_user_func("imagecreatefrom".$types[$aImgData[2]], $sFileName);
			
		if (function_exists('imagecreatetruecolor') && ($aImgData[2] == 2 || $aImgData[2] == 3)) {
			$rThumb = imagecreatetruecolor($nWidth, $nHeight); // use imagecreatetruecolor (bei GD 2.01 und JPG/PNG)
		} else {
			$rThumb = imagecreate($nWidth, $nHeight); // use imagecreate
		}
		imagecopyresized($rThumb, $rImage, 0, 0, 0, 0, $nWidth, $nHeight, $aImgData[0], $aImgData[1]);
		call_user_func("image".$types[$aImgData[2]], $rThumb, $sCacheDir.$sThumbFile);
		imagedestroy($rImage);
		imagedestroy($rThumb);
		$sImgFile = $sCacheDir.$sThumbFile;
		
	} else {
		
		// is the file cached, then return the thumbfile
		($bIsCached == true)
			? $sImgFile = $sCacheDir.$sThumbFile
			: $sImgFile = $sImgFile;
	}
	
	// open the thumbfile and output it too the browser
	header ("Content-Type: image/".$types[$aImgData[2]]);
	
	$rImage = fopen($sImgFile, "rb"); 
	fpassthru($rImage);
	fclose($rImage);
?>
