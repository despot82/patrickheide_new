<?php
/**
* mdb_detail.php
*
* Detailpage: media-db //-> 2sprachig und voll kopierbar!
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	Formular:	$compare_filetype
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.4 / 2005-04-06 (auf MediaDB-Klasse umgestellt)
* #history	2.31 / 2004-10-27 (BUGFIX bei "update"-file-uload bei sonderzeichen im upload-filename)
* #history	2.3 / 2004-10-19 (Reihenfolge upload/delete-old-file bei "update" umgestellt)
* #history	2.2 / 2004-09-06 (auf Form-Methode "get_media_type()" umgestellt)
* #history	2.1 / 2004-08-31 (icon/thumbs auf Form-Methode "get_media_icon()" umgestellt)
* #history	2.0 / 2004-05-04 (new_intranet)
* #history	1.2 / 2003-06-03
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mdb_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_module']['unix']."class.Mdb.php");
	require_once($aENV['path']['global_service']['unix']."class.download.php");
	require_once($aENV['path']['global_service']['unix']."pclzip.lib.php");
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$sGEToptions	= "?start=".$start;
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$compare_filetype = (isset($_POST['compare_filetype']))	? $_POST['compare_filetype'] : '';
// 2c. Vars:
	$sTable			= $aENV['table']['mdb_media'];
	$sViewerPage	= $aENV['SELF_PATH']."mdb.php".$sGEToptions; 	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;				// fuer NEW-button
   
// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();
	// Modulklasse
	$oMdb =& new Mdb($aENV,$oDb);
	$oMdb->setSess($oSess);
	$oMdb->setUserdata($Userdata);
	$oMdb->setSearchMDir($searchMDir);
	$oMdb->setDate($oDate);
	$oMdb->setFile($oFile);
    
// 3. DB
// DB-action: delete
	if (isset($btn['delete']) && $aData['id']) {
		// erst checken ob dieses file noch irgendwo verwendet wird
		$check = $oMdb->checkMedia($aData['id']);
		if (!empty($check)) {
			echo js_alert($check);
		} else {
			// delete
			$oMdb->delete($aData);
			// back to overviewpage
			header("Location: ".$sViewerPage); exit;
		}
	}

// DB-action: save or update
	if (isset($btn['save']) || isset($btn['save_close'])) {
		// upload new file + save
		$aData = $oMdb->save($aData,$_FILES,$syslang,$compare_filetype);
		// ggf. back to overviewpage
		if (is_array($aData)) {
			if (isset($btn['save_close']) || $aData['bCloseAfterSave']) {
				header("Location: ".$sViewerPage); exit;
			}
		}
	}

// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

// OBJECTS
	// TREE
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], MDB_STRUCTURE_FILE, 'mdb', array('sOrderBy' => 'prio DESC')); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions='']
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();
	
	// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}


// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['mdb']['unix']."inc.mdb_folder.php");

?>

<script language="JavaScript" type="text/javascript">
function checkForZip(ulValue) {
	if(ulValue.indexOf('.zip') != -1) {	
		document.getElementById('zipDiv').style.display = 'block';
		document.getElementById('extract0').checked = false;
		document.getElementById('extract1').checked = true;
	} else {
		document.getElementById('zipDiv').style.display = 'none';
		document.getElementById('extract0').checked = false;
		document.getElementById('extract1').checked = false;
	}
}
</script>
  
<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="aData[filename]" value="<?php echo $aData['filename']; ?>">
<input type="hidden" name="compare_filetype" value="<?php echo $aData['filetype']; // wird bei "replace" benutzt ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td width="25%"><p><span class="title"><?php echo $aMSG['form']['file'][$syslang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td width="75%" align="right"><span class="text"><?php 
			echo '<a href="'.$sViewerPage.'" title="'.$aMSG['btn']['list'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['btn']['list'][$syslang].'" class="btn"></a>';
			if ($oPerm->hasPriv('create')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"]
		?></span></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="25%"><p><b><?php echo $aMSG['form']['file'][$syslang]; ?> *</b></p></td>
		<td width="75%">
		<?php // zip-entpack-option nur bei NEW!
		$sExtra = ($mode_key == "new") ? 'onChange="checkForZip(this.value)"' : '';
		echo $oForm->media_upload($sExtra); // params: - 
		?>
		</td>
	</tr>
</table>

<div id="zipDiv" style="display:none;">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="25%"><p><?php echo $aMSG['form']['zipentpacken'][$syslang]; ?></p></td>
		<td width="75%"><p>
			<input type="radio" value="0" id="extract0" name="aData[extract]"> <label for="extract0"><?=$aMSG['form']['zipentpacken2'][$syslang]?></label><br>
			<input type="radio" value="1" id="extract1" name="aData[extract]"> <label for="extract1"><?=$aMSG['form']['zipentpacken1'][$syslang]?></label><br></p>
		</td>
	</tr>
</table>
</div>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php if (isset($aData['id'])) { ?>
	<tr valign="top">
		<td width="25%" class="sub2"><p><?php echo $aMSG['form']['details'][$syslang]; ?></p></td>
		<td width="75%" class="sub2"><p><?php
		// details::filesize
		if (isset($aData['filesize'])) {
			$aData['filesize'] = $oFile->format_filesize($aData['filesize'], 'AUTO', 0);
			echo "<b>".$aMSG['form']['filesize'][$syslang].":</b> ".$aData['filesize']."<br>";
		}
		// details::filetype/dimensions
		$mtype = mediadb::getMediaType($aData['filename']); // params: $sFilename
		if (!empty($mtype)) {
			echo "<b>".$aMSG['form']['filetype'][$syslang].":</b> ".$mtype;
			if($oPerm->isDaService()) {
				echo " <small>[".$aData['filetype']."]</small>";
			}
			echo "<br>";
		}
		if(!empty($aData['width'])) {
			echo "<b>".$aMSG['form']['dimensions'][$syslang].":</b> ".$aData['width'];
		}
		if(!empty($aData['height'])) {
			echo " x ".$aData['height']." px<br>";
		}
		?></p></td>
	</tr>
<?php } ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
		<? if(!empty($id)) { ?>
	<tr valign="top">
		<td width="25%"><p><b><?php echo $aMSG['media']['mdbfilename'][$syslang]; ?> *</b><br><small>[max. 50 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td width="75%">
		<?php echo $oForm->textfield('name',50,78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<? } ?>
	<tr valign="top">
		<td width="25%"><p><b><?php echo $aMSG['form']['alttag'][$syslang]; ?> *</b><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td width="75%">
		<?php echo $oForm->textarea("description",4,78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['keywords'][$syslang]; ?><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td>
		<?php echo $oForm->textarea("keywords",2,78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
<?php if ($oTree->getCount()) { // only if more than "root" ?>
	<tr>
		<td><p><?php echo $aMSG['form']['folder'][$syslang]; ?></p></td>
		<td><p><?php 
		if ($oForm->bEditMode) {
			$oTree->setRootOff();
			if ($searchMDir == -1) {
				$oTree->addOption(array('-1' => $aMSG['media']['upload_folder'][$syslang]));
				$oTree->addOption(array('0' => '---------------------')); // Trenner
			}
			$oTree->addOption(array('0' => $aMSG['media']['root_folder'][$syslang]));
			if ($mode_key == "new") { $aData['tree_id'] = (!empty($searchMDir)) ? $searchMDir : ''; } // bei "neu" zuletzt eingestellten ordner anzeigen
			echo $oTree->treeFormSelect("aData[tree_id]", $aData['tree_id']); // params: $name[,$currentId='']
		} else {
			// nur view
			if ($aData['tree_id'] == '-1') {
				echo $aMSG['media']['upload_folder'][$syslang];	
			} elseif ($aData['tree_id'] == '0') {
				echo $aMSG['media']['root_folder'][$syslang];
			} else {
				echo $oTree->getValueById($aData['tree_id'], 'title');
			}
		} ?></p></td>
	</tr>
<?php } ?>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr><td><p><small><?php	
	// HINTS:
		// Ersetzungen vorbereiten
		$UPLOAD_MAX_FILESIZE =  ini_get("upload_max_filesize")."B";
		// Ersetzungen vornehmen + ausgeben
		echo str_replace("{UPLOAD_MAX_FILESIZE}", $UPLOAD_MAX_FILESIZE, $aMSG['media']['hint_max_filesize'][$syslang])."<br>\n";
		if (isset($aENV['module']['mdb']) && isset($aENV['module']['pms'])) {
		 	echo "<br>".$aMSG['media']['hint_jpeg'][$syslang]."<br>\n";
		} ?>
	</small></p></td></tr>
</table>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>