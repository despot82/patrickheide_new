<?
/**
* mdb_folder.php
*
* Overviewpage: navi //-> 2sprachig und voll kopierbar!
*
* @param	int		$nToggleId	[welche id zum aufklappen] (optional)	// HIGHLIGHT-var
* @param	string	$priomove	[fuer Prio (wird gesondert abgeholt): 'first'|'last'|'up'|'down'] (optional)
* @param	int		$naviid		[fuer Prio (wird gesondert abgeholt): id des zu priorisierenden datensatzes] (optional)
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 / 2005-11-02 
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mdb_folder.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. Includes
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

	require_once($aENV['path']['global_bean']['unix'].'class.TeamAccessControllBean.php');
	include_once($aENV['path']['global_bean']['unix'].'class.FolderBean.php');
	require_once($aENV['path']['global_service']['unix'].'class.TeamAccessControllEA.php');
	require_once($aENV['path']['global_service']['unix'].'class.NaviToggle.php');
	if(!$aENV['php5']) {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php');
	}
	else {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php5');
	}
	require_once($aENV['path']['global_service']['unix']."class.tree.php");

// 2a. GET-params abholen
	$nToggleId		= (isset($_GET['nToggleId'])) ? $oFc->make_secure_int($_GET['nToggleId']) : '';
	$priomove		= (isset($_GET['priomove'])) ? $oFc->make_secure_string($_GET['priomove']) : '';
	$naviid			= (isset($_GET['naviid'])) ? $oFc->make_secure_int($_GET['naviid']) : '';
	$sortway		= (isset($_GET['sortway'])) ? $oFc->make_secure_string($_GET['sortway']) : '';
	$sorttoggle		= (isset($_GET['sorttoggle'])) ? $oFc->make_secure_int($_GET['sorttoggle']) : '';
	
// 2b. POST-params abholen

// 2c. Vars:
	$sEditPage = $aENV['SELF_PATH'].'mdb_folder_detail.php';
	//$oDb->debug = true;
	$sGEToptions	= '?nToggleId='.$nToggleId;
	
// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();
	
	// Folder EasyAccess
	$mfea =& new FolderEA($oDb);
	$mfea->setTable($aENV['table']['global_tree']);
	$mfea->setFlagtree('mdb');
	$mfea->setAENV($aENV);
	$mfea->setUid($Userdata['id']);
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$mfea->setOnlyauthed(true);
		}
	}
	if(!empty($sorttoggle)) {
		$mfea->sortFolder($sorttoggle,$sortway);	
	}
	$mfea->setOrderBy('prio');
	$mfea->setOrder('desc');
	$mfea->initialize($start,$priomove,$naviid);
	
	// TREE (VOR HTML!)
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], MDB_STRUCTURE_FILE, 'mdb',array('sOrderBy' => 'prio DESC')); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions='']
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();
	
	if(!empty($priomove) || !empty($sorttoggle)) {
		// nach prio-aenderung: 1. cache leeren...
		$oTree->clearCache();
		// ... und 2. reloaden.
		header("Location: ".$aENV['PHP_SELF'].$sGEToptions);
	}
// HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['mdb']['unix']."inc.mdb_folder.php");

?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['media']['folders'][$syslang]; ?></span> <?php echo $aMSG['mode']['edit'][$syslang]; ?></p></td>
		<td align="right"><span class="text">
		<?php // Buttons
			echo '<a href="'.$aENV['SELF_PATH'].'mdb.php" title="'.$aMSG['btn']['back'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['btn']['back'][$syslang].'" class="btn"></a>';
			echo '<a href="'.$sEditPage.$sGEToptions.'" title="'.$aMSG['navi']['add_section'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_add.gif" alt="'.$aMSG['navi']['add_section'][$syslang].'" class="btn"></a>';
		?></span></td>
	</tr>
</table>

<?php echo HR; ?><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup>
		<col width="10%">
		<col width="70%">
		<col width="10%">
	</colgroup>
	<tr valign="top">
		<th width="10%"><p><b><?php echo $aMSG['form']['prio'][$syslang]; ?></b></p></th>
		<th width="70%"><p><b><?php echo $aMSG['form']['title'][$syslang]; ?></b></p></th>
		<th width="10%"><p><b>&nbsp;</b></p></th>
	</tr>
<?php
if (is_object($mfea) && $mfea->getEntries() != 0) {
	$oToggle =& new NaviToggle();
	$oToggle->initialize($aENV,$aMSG,$syslang,$mfea,$nToggleId);
	
	for ($i=0;$oToggle->nextDataset($i);$i++) {
		
		$ebene = $oToggle->getValueByName('ebene');
		$style = ($ebene == 0) ? 'sub1' : 'sub2';
		
		if($ebene == 0 && $i != 0) { ?>
	 <tr><td colspan="4" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

<?php	} ?>
	<tr>
		<td class="<?=$style?>"><p><?=$oToggle->getTogglePrio();?></p></td>
		<td class="<?=$style?>"><p><?=$oToggle->getToggleLink();?>
			<a href="<?php echo $sEditPage.'?tree_id='.$oToggle->getValueByName('id').'&nToggleId='.$nToggleId; ?>" title="<?php echo$aMSG['std']['edit'][$syslang]; ?>"><?=$oToggle->getToggleTitle();?></a></p>
		</td>
		<td class="<?=$style?>" align="right" nowrap><p>
		<?php // button fuer alphabetische sortierung der unterpunkte
			if($oToggle->hasSubMenue($oToggle->getValueByName('id'))) { echo $oToggle->getNewSortLink(); }
		?>
		<a href="<?php echo $sEditPage.'?tree_id='.$oToggle->getValueByName('id').'&nToggleId='.$nToggleId; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['std']['edit'][$syslang]; ?>" class="btn"></a>
		</p></td>
	</tr>
<?php
	} // END for
}
// 'no-data'-string
else  { echo '	<tr><td colspan="4" align="center"><p>'.$aMSG['std']['no_entries'][$syslang].'</p></td></tr>'; }
?>

</table>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>