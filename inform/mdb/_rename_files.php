<?php
// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");


class mdb_rename_old_files {

/**
* @access   private
* @var	 	int		Zaehler fuer importierte Dateien
*/
	var $nCount;
/**
* @access   private
* @var	 	array	error
*/
	var $aError;

	### globals ###
/**
* @access   private
* @var	 	array	importiertes globales Setup-Array
*/
	var $aENV;
/**
* @access   private
* @var	 	object	importiertes globales DB-Object
*/
	var $oDb;

#-----------------------------------------------------------------------------

/**
* Konstruktor
*
* @access   public
* @return   void
*/
	function mdb_rename_old_files() {
		// set vars
		$this->aError	= array();
		$this->nCount	= 0; // zaehler
		
		// import global vars
		global $aENV, $Userdata, $oDb, $oDate;
		$this->aENV		=& $aENV;
		if (!is_array($this->aENV)) die("ERROR: NO aENV!");
		$this->oDb		=& $oDb;
		if (!is_object($this->oDb)) {
			$this->oDb	=&  new dbconnect($this->aENV['db']);
		}
		if (!is_object($this->oDb)) die("ERROR: NO oDb!");
	}

/**
* Diese Funktion geht durch des angegebene Verzeichnis und macht den eigentlichen job...
*
* @access   public
* @param 	array	aData
* @return	int		Anzahl importierter Dateien
*/
	function _rename() {
		$sDirectory = "../../media/import/";
		// Check path auf "/" am ende
		$sDirectory = (!preg_match("/(\/$)/", $sDirectory)) ? $sDirectory."/" : $sDirectory;
		
		// oeffne angegebenes dir
		if (!$handle = @opendir($sDirectory)) {
			$this->aError[] = "Cannot open directory!"; return false;
		}
		// lese angegebenes dir aus
		while ($sFile = @readdir($handle)) {
			// keine dirs
			if ($sFile == "." || $sFile == ".." || is_dir($sDirectory.$sFile)) { continue; }
			// reset Datensatz-Container
			$aFileInDir = array();
			
		// get new filename + name
			$sNewFile = $this->_format_filename($sFile);
			/*
		// check if unique
			$this->oDb->query("SELECT id FROM ".$this->aENV['table']['mdb_media']." WHERE filename='".$sNewFile['filename']."'");
			if ($this->oDb->num_rows() > 0) {
				$this->aError[] = "Filename '".$sNewFile."' ist schon vergeben!"; return false;
			}
		// Update Eintrag
			$aUpdate['filename'] = $sFile;
			$this->oDb->make_update($sNewFile, $this->aENV['table']['mdb_media'], $aUpdate);
			*/
		// zaehle hoch
			$this->nCount++;
		} // END while
		@closedir($handle);
		// return zaehler
		return $this->nCount;
	}

/**
* Diese private Methode formatiert den Dateinamen.
* D.h. sie schneidet zuerst ggf. eine schon vorhandene uniqueID raus. 
* ggf. bastelt sie eine (nur) 8-stellige Unique-ID in den Dateinamen
* und/oder kuerzt ihn os9 konform.
*
* @access   private
* @param 	string	sFilename
* @return	string	Behandelter Dateiname
*/
	function _format_filename($sFilename) {
		if (empty($sFilename)) return;
		$suchmuster = "/(_[a-z0-9]{8})(\.\d{3})/i";
		$ersetzung = "\$2";
		echo preg_replace($suchmuster, $ersetzung, $sFilename);
	}

/**
* Diese Funktion gibt die ggf. angefallenen Error-Meldungen als String formatiert zurueck.
*
* @access   public
* @return	string	Error-Meldungen
*/
	function getErrorString() {
		return implode("\n", $this->aError);
	}

#-----------------------------------------------------------------------------

} // END of class
?>