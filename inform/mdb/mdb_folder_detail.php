<?php
/**
* mdb_folder_detail.php
*
* Specialpage: Importiere vorhandene mediafiles in die media-db 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$folderid	[welcher Datensatz] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-01-07
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mdb_folder_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_module']['unix']."class.Mdb.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	if(!$aENV['php5']) {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php');
	}
	else {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php5');
	}
	require_once($aENV['path']['global_service']['unix']."class.user.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['tree_id'])) ? $oFc->make_secure_int($_GET['tree_id']) : '';
	$viewug			= (isset($_GET['viewug'])) ? $oFc->make_secure_string($_GET['viewug']) : '';
	// backup zur sicherheit!!!
	$treeid			= (isset($_GET['tree_id'])) ? $oFc->make_secure_int($_GET['tree_id']) : 0;
	$nToggleId		= (isset($_GET['nToggleId'])) ? $oFc->make_secure_int($_GET['nToggleId']) : '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$group			= (isset($_POST['group'])) ? $_POST['group'] : '';
	$user			= (isset($_POST['user'])) ? $_POST['user'] : '';
// 2c. Vars:
	$sGEToptions	= '?tree_id='.$id.'&nToggleId='.$nToggleId."&flag_open=".$aData['flag_open'];
	$sTable			= $aENV['table']['global_tree'];
	$sNewPage		= $aENV['PHP_SELF'].'?nToggleId='.$nToggleId;			// fuer NEW-button
	$sViewerPage	= $aENV['SELF_PATH']."mdb_folder.php".$sGEToptions;		// fuer BACK-button

// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();
	
	$oFolderEA =& new FolderEA($oDb);
	$oFolderEA->setFlagtree('mdb');
	$oFolderEA->setTable($sTable);
	if(!isset($aData['flag_open'])) { 
		if(is_array($oTACEA->getRightForID($treeid,'mdb'))) { 
			$flopen = 0; 
		} else { 
			$flopen = 1; 
		}
	}
	else {
		$flopen = $aData['flag_open'];	
	}
	unset($aData['flag_open']);
	
	// TREE
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], MDB_STRUCTURE_FILE, 'mdb',array('sOrderBy' => 'prio DESC')); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions='']

	// build TREE (VOR HTML!)
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	// USER
	$oUser =& new User($oDb);

	// MDB
	$oMdb =& new Mdb($aENV,$oDb);

// 3. DB
// DB-action: delete_all
	if (isset($btn['delete']) && $aData['id']) {
		// get all sub-items of this navi-item
		$aIDs2delete = $oMdb->returnAllSubIds($aData['id'], $sTable);
		// add this id
		$aIDs2delete[] = $aData['id'];
		// make delete(s) in Media-Tree-Table
		$oDb->query("DELETE FROM ".$sTable." WHERE id IN (".implode(",", $aIDs2delete).") AND flag_tree='mdb'");
		// Accessrights löschen
		$oTACEA->onDeleteAction($aData['id'],'mdb');
		// Update Media-Content-Table -> change "tree_id" of all affected records into "parent_id"
		if (!isset($aData['parent_id'])) {$aData['parent_id'] = '0';}
		$oDb->query("UPDATE ".$aENV['table']['mdb_media']." SET tree_id=".$aData['parent_id']." WHERE tree_id IN (".implode(",", $aIDs2delete).")");
		$oTree->clearCache();
		// back to overview page
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) || isset($btn['save_close'])) {

		// Die Prio editieren, sofern der folder in einen anderen Ast gehaengt wurde
		$rs = $oDb->fetch_by_id($aData['id'],$sTable);

		if($aData['parent_id'] != $rs['parent_id'] || !$aData['id']) {
			$sql = "SELECT prio FROM ".$sTable." WHERE parent_id='".$aData['parent_id']."' AND flag_tree='mdb' ORDER BY prio DESC LIMIT 1";
			$oDb->query($sql);
			$row = $oDb->fetch_object();
			$aData['prio'] = ($row->prio+1);
		}
		$group = $aData['group'];
		$user = $aData['user'];
		$viewug = $aData['viewug'];
		unset($aData['group']);
		unset($aData['user']);
		unset($aData['viewug']);
		
		if (!empty($aData['id'])) {
		// Update-actions of an existing entry
			// set userid
			$aData['last_mod_by'] = $Userdata['id'];
			// make update
			$aUpdate['id'] = $aData['id'];
			$oFolderEA->doUpdate($aData);
		} else {
		// Insert-actions of a new entry
			// set timestamp + userid
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			// make insert
			$oFolderEA->doInsert($aData);
			$aData['id'] = $oDb->insert_id(); // get last inserted id
			$treeid = $aData['id'];
			$sGEToptions .= '&tree_id='.$treeid;
		}
		
		if($flopen == 1) {
			$oTACEA->delRightForTree($treeid,'mdb');
		}
		else {
			$oTACEA->addRights($group, $user,$treeid,'mdb');
		}

		// delete tree-cache (file + session)
		$oTree->clearCache();
		
		if(isset($btn['save_close'])) {
			// back to overview page
			header("Location: ".$sViewerPage); exit;
		}
		if(isset($btn['save'])) {
			// reload
			#header("Location: ".$aENV['PHP_SELF'].$sGEToptions.'&viewug='.$viewug); exit;
		}
	}

// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

	if(!empty($viewug)) $aData['viewug'] = $viewug;

	$oTree->buildTree();

	$aStructure = $oTree->getAllChildren();

	
	// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("title", $aMSG['form']['title'][$syslang].'!'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}


// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['mdb']['unix']."inc.mdb_folder.php");

?>


<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="remoteSave" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['media']['folder'][$syslang]; ?></span>
		<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p></td>
		<td align="right"><span class="text"><?php
			echo '<a href="'.$sViewerPage.'" title="'.$aMSG['btn']['back'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['btn']['back'][$syslang].'" class="btn"></a>';
			if ($oPerm->hasPriv('create')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"]
		?></span></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['name'][$syslang]; ?> *</b></p></td>
		<td>
		<?php echo $oForm->textfield("title",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr>
		<td><p><b><?php echo $aMSG['form']['assignment'][$syslang]; ?></b></p></td>
		<td><?php #echo $oTree->treeFormSelect("aData[parent_id]", $aData['parent_id']); // params: $name[,$currentId=''] ?>

			<select name="aData[parent_id]" size="1">
<?php // build select-field -> NUR die ersten 2 Ebenen!
			
			$sel = ($id == '0') ? " selected" : ""; // highlight
			echo '<option value="0"'.$sel.'>'.$aMSG['media']['noassignment'][$syslang]."</option>\n";
			foreach($aStructure as $kMain => $aData2) {
				if ($id == $aData2['id']) continue; // nicht noch mal sich selbst anzeigen!
				//
				$sel = ($aData2['id'] == $aData['parent_id']) ? " selected" : ""; // highlight
				echo '<option value="'.$aData2['id'].'"'.$sel.'>'.$aData2['title']."</option>\n";
				foreach($aData2['children'] as $kSub => $aData3) {
					if ($id > 0 && $id == $aData3['id']) continue; // nicht noch mal sich selbst anzeigen!
					$sel = ($aData3['id'] == $aData['parent_id']) ? " selected" : ""; // highlight
					
					echo '<option value="'.$aData3['id'].'"'.$sel.'>- '.$aData3['title']."</option>\n";
				}
			} ?>
			</select>
		</td>
	</tr>
	
<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<tr valign="top">
	<td><p><b><?php echo $aMSG['form']['forumstatus'][$syslang]; ?></b></p></td>
	<td>
<? 
$param = array('first' => '20','second' => '70','border'=>'0','cellspacing'=>'1','cellpadding'=>'2','class' =>"tabelle");?>
<?=$oTACEA->showTeamAccess($aData,$syslang,$treeid,$flopen,$aMSG,$param,'mdb',true); ?>
	</td>
</tr>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>