<?php
/**
* popup_slideshow.php
*
* Popup, um Bilder aus der MediaDB zu einer Slideshow zusammenzustellen.
* -> 2sprachig und kopierbar. 
*
* Diese Seite wird aus der JS-selectSlideshowWin(id,col_name,tbl_name,$formname) aufgerufen
*
* @param	int		$id			[um zu wissen um welchen Datensatz (Projekt...) es geht] (required)
* @param	string	$col_name	[um zu wissen um welche Spalte i.d. Tabelle es geht] (required)
* @param	string	$tbl_name	[um zu wissen um welche Tabelle i.d. DB es geht] (required)
* @param	string	$formname	[um zu wissen um welches Form i.d. openerpage es geht] (required)
* @param	int		$width		[zum filtern der auszuwaehlenden Bilder um eine maximale/fixe Breite] (optional)
* @param	int		$height		[zum filtern der auszuwaehlenden Bilder um eine maximale/fixe Hoehe] (optional)
* @param	int		$mustfit	[bestimmt ob $width/$height maximal/fix bedeutet] (optional)
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.7 / 2005-04-26 [neuer Filter als GET-Parameter "mustfit" + umbenannt: "maxwidth"/"maxheight" in "width"/"height"]
* #history	1.6 / 2005-04-01 [neue Filter als GET-Parameter "maxwidth" + "maxheight"]
* #history	1.52 / 2004-06-28 (BUGFIX: rel.-path fuer thumbs)
* #history	1.51 / 2004-06-02 (NEU: $formname)
* #history	1.5 / 2004-01-28 (tree/thumbs)
* #history	1.41 / 2004-01-22 (logo rausgenommen / header verkuerzt)
* #history	1.4 / 2004-01-13 (umbau... weniger klicks...)
* #history	1.32 / 2003-09-05 ($aENV['slideshow'][$syslang] eingebaut)
* #history	1.31 / 2003-08-29 (BUGFIX img -> image im select_media.php aufruf)
* #history	1.3 / 2003-08-22 ("maximale anzahl der bilder" via "setup"/"include_all" konfigurierbar gemacht)
* #history	1.21 / 2003-07-16 (kommentar verbessert)
* #history	1.2 / 2003-07-11 (errorhandling und neue buttonfunktionen eingebaut)
* #history	1.1 / 2003-06-18 (2sprachigkeit hinzugefuegt)
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_slideshow.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.ArrayTools.php");
	require_once($aENV['path']['global_service']['unix']."class.Prio.php");
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");

// 2a. GET-params abholen
	$id				= ($_GET['id']) ? $oFc->make_secure_int($_GET['id']) : '';
	$col_name		= ($_GET['col_name']) ? $oFc->make_secure_string($_GET['col_name']) : '';
	$tbl_name		= ($_GET['tbl_name']) ? $oFc->make_secure_string($_GET['tbl_name']) : '';
	$formname		= ($_GET['formname']) ? $oFc->make_secure_string($_GET['formname']) : '';
	$width			= (isset($_GET['width'])) ? $oFc->make_secure_int($_GET['width']) : '';
	$height			= (isset($_GET['height'])) ? $oFc->make_secure_int($_GET['height']) : '';
	$mustfit		= (isset($_GET['mustfit']) && $_GET['mustfit'] == 1) ? 1 : 0;
	$remoteSave		= (isset($_GET['remoteSave']) && $_GET['remoteSave']==1) ? 1 : 0;
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	// POST ueberschreibt ggf. GET
	if (isset($_POST['id']))		{$id		= $oFc->make_secure_int($_POST['id']);}
	if (isset($_POST['col_name']))	{$col_name	= $oFc->make_secure_string($_POST['col_name']);}
	if (isset($_POST['tbl_name']))	{$tbl_name	= $oFc->make_secure_string($_POST['tbl_name']);}
	if (isset($_POST['formname']))	{$formname	= $oFc->make_secure_string($_POST['formname']);}
	if (isset($_POST['width']))		{$width		= $oFc->make_secure_int($_POST['width']);}
	if (isset($_POST['height']))	{$height	= $oFc->make_secure_int($_POST['height']);}
	if (isset($_POST['mustfit']))	{$mustfit	= $oFc->make_secure_int($_POST['mustfit']);}
	if (isset($_POST['remoteSave']) && $_POST['remoteSave']==1)	{ $remoteSave = 1; }
// 2c. Vars:
	$sAddSizeJsVars = ",'".$width."','".$height."','".$mustfit."','slideshowWin'";
	$oPrio =& new Prio($_GET);
	$sGEToptions = '&col_name='.$col_name.'&tbl_name='.$tbl_name.'&formname='.$formname.'&width='.$width.'&height='.$height.'&mustfit='.$mustfit.'&id='.$id;
	$updated = false;

// 2d. Texte
	$MSG = array();
	$MSG['edit_slideshow']['de']	= $aENV['slideshow']['de']." bearbeiten";
	$MSG['edit_slideshow']['en']	= "Edit ".$aENV['slideshow']['en'];
	$MSG['create_slideshow']['de']	= $aENV['slideshow']['de']." erstellen";
	$MSG['create_slideshow']['en']	= "Create ".$aENV['slideshow']['en'];
	$MSG['add_picture']['de']		= "+ ".$aMSG['form']['picture']['de'];
	$MSG['add_picture']['en']		= "Add ".$aMSG['form']['picture']['en'];

	if(isset($_GET['moveDir'])) {
		if ($id && $col_name && $tbl_name) { 
			$aData = $oDb->fetch_by_id($id,$tbl_name); 
			$aUpdateData[$col_name] = $oPrio->doMove($aData[$col_name]);
			$aUpdate['id'] = $id;
			$oDb->make_update($aUpdateData, $tbl_name, $aUpdate);
			$updated = true;
		}
	}
// 3. DB
	if (isset($btn['save']) || $remoteSave == 1) {
		// DB-action: update data
		if ($aData && $id && $col_name && $tbl_name) {
			foreach ($aData as $key => $val) { if ($val == "") { unset($aData[$key]); } } // geloeschte aus dem array entfernen
			$aUpdateData[$col_name] = implode(",",$aData);
			$aUpdate['id'] = $id;
			$oDb->make_update($aUpdateData, $tbl_name, $aUpdate);
			$updated = true;
		}
	}
	
	if (isset($btn['clear_all'])) {
		// DB-action: clear all fields
		if ($id && $col_name && $tbl_name) {
			$aUpdateData[$col_name] = '';
			$aUpdate['id'] = $id;
			$oDb->make_update($aUpdateData, $tbl_name, $aUpdate);
			$closeThisWin = true;
		}
	}

// MEDIA-DB
if (!is_object($oMediadb)) {
	$oMediadb =& new mediadb(); // params: - 
}
// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
?>

<script language="JavaScript" type="text/javascript">
// bgColors der TRs (kann NICHT durch das stylesheet definiert werden!)
var bgClassHighlight = 'sub3';
var bgClassNormal = 'sub1';
var memoryId;

// funktionen zu den buttons (remove, replace, select)
function removeOne(n,m) {
	document.forms['slideshowForm'][n].value='';
	document.forms['slideshowForm'][m].value='';
	document.forms['slideshowForm']['remoteSave'].value = 1;
	document.forms['slideshowForm'].submit();
}
function replaceOne(p) {
	// pruefen, ob 'popup_selectmedia.php' nicht geoeffnet (oder zwischenzeitlich geschlossen) wurde...
	if (window.opener.mediaWin == null || window.opener.mediaWin.document == undefined) { // ... und ggf. oeffnen
		opener.selectMediaWin('image','img_neu','slideshowForm'<?php echo $sAddSizeJsVars; ?>);
		return setTimeout("replaceOne('"+p+"')", 100); // aussteigen und nochmal probieren (popup braucht zeit zum laden!)
	}
	window.opener.mediaWin.document.forms['selectmediaForm']['formfield'].value='img_' + p;
	// highlight
	pId = 'tr' + p;
	document.getElementById(pId).className = bgClassHighlight;
	document.getElementById('trneu').className = bgClassNormal;
	if (memoryId && memoryId != pId) {
		document.getElementById(memoryId).className = bgClassNormal;
	}
	memoryId = pId; // zuletzt geklickten merken
	window.opener.mediaWin.focus();
}
function selectOne() {
	// pruefen, ob 'popup_selectmedia.php' nicht geoeffnet (oder zwischenzeitlich geschlossen) wurde...
	if (window.opener.mediaWin == null || window.opener.mediaWin.document == undefined) {  // ... und ggf. oeffnen
		opener.selectMediaWin('image','img_neu','slideshowForm'<?php echo $sAddSizeJsVars; ?>);
		return setTimeout('selectOne', 100); // aussteigen und nochmal probieren (popup braucht zeit zum laden!)
	}
	window.opener.mediaWin.document.forms['selectmediaForm']['formfield'].value='img_neu';
	// highlight
	document.getElementById('trneu').className = bgClassHighlight;
	if (memoryId) {
		document.getElementById(memoryId).className = bgClassNormal;
	}
	window.opener.mediaWin.focus();
}
// funktionen zu den buttons (clear_all, close_win)
function confirmClearAll() {
	var chk = window.confirm('<?php echo $aMSG['err']['clear_all_images'][$syslang]; ?>');
	return(chk);
}
function closeThisWin() {
	// das hauptfenster-formular abschicken (-> aktualisieren)
	window.opener.document.forms[<?php echo (!empty($formname)) ? "'".$formname."'" : '0'; ?>]['remoteSave'].value = 1;
	window.opener.document.forms[<?php echo (!empty($formname)) ? "'".$formname."'" : '0'; ?>].submit();
	// und beim schliessen dieses fensters auch das selectmedia-fenster schliessen
	window.opener.mediaWin.close();
	self.close();
}
// funktion, die beim laden der seite ausgefuehrt wird
function init() {
<?php // beim ERSTMALIGEN laden der seite das selectmedia-fenster oeffnen
	if (!$updated) {
		#echo "if (window.opener.mediaWin == null || window.opener.mediaWin.document == undefined) { opener.selectMediaWin('image','img_neu','slideshowForm'".$sAddSizeJsVars."); }";
		echo "opener.selectMediaWin('image','img_neu','slideshowForm'".$sAddSizeJsVars.");";
	} else { // sonst nur das hiddenfield 'formfield' neu beschreiben
		echo "window.opener.mediaWin.document.forms['selectmediaForm']['formfield'].value = 'img_neu';";
	} ?>
	// 'trneu' highlighten
	document.getElementById('trneu').className = bgClassHighlight;
}
<?php // nach dem leeren aller images fenster schliessen
	#echo ($closeThisWin) ? "closeThisWin();" : "onload = init;"; // DEACTIVATED 2005-05-04af
?>
onload = init;
</script>

<div id="contentPopup">

<span class="title"><?php echo (!empty($aData[$col_name])) ? $MSG['edit_slideshow'][$syslang] : $MSG['create_slideshow'][$syslang]; ?><br></span>
<br>
<form action="<?php echo $aENV['PHP_SELF']; ?>" name="slideshowForm" method="post" target="_self">
<input type="hidden" name="id" value="<?php echo $id; ?>">
<input type="hidden" name="col_name" value="<?php echo $col_name; ?>">
<input type="hidden" name="tbl_name" value="<?php echo $tbl_name; ?>">
<input type="hidden" name="formname" value="<?php echo $formname; ?>">
<input type="hidden" name="width" value="<?php echo $width; ?>">
<input type="hidden" name="height" value="<?php echo $height; ?>">
<input type="hidden" name="mustfit" value="<?php echo $mustfit; ?>">
<input type="hidden" name="remoteSave" value="0">

<?php // warnmeldung DEBUG
	if (!$id || !$col_name || !$tbl_name) { echo '<p><font color="red"><b>ERROR: (!$id || !$col_name || !$tbl_name) !!!</b></font></p>'; } ?>

<?php // hole slideshow-ids
	if ($id && $col_name && $tbl_name) { $aData = $oDb->fetch_by_id($id,$tbl_name); } ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php // Vars
	if ($aData[$col_name]) { $aSlideshow_ids = explode(",", $aData[$col_name]); }
	$anzahl = count($aSlideshow_ids);
	
	for ($i=0; $i<$anzahl; $i++) {
		$I = $i+1;
?>
<tr valign="top">
	<td width="15%" id="tr<?php echo $I; ?>">
		<?=$oPrio->getPrioButton($aSlideshow_ids,$i,$aENV,$aMSG,$syslang,$sGEToptions)?>
	</td>
	<td width="10%" id="tr<?php echo $I; ?>"><span class="small"><?php #echo $aMSG['form']['image'][$syslang]; ?>&nbsp;<?php echo $I; ?></span></td>
	<td width="10%" align="center">
<?php // if picture is in DB -> show thumbnail and replace/delete-buttons
		$sValue = ($aSlideshow_ids[$i]) ? $aSlideshow_ids[$i] : '';
		$aImg = $oMediadb->getMedia($sValue); // params: $nMediaId

		$width = (!empty($aImg['width'])) ? $aImg['width'] : "''";
		$height = (!empty($aImg['height'])) ? $aImg['height'] : "''";
		
		if(!empty($aImg['filesize']) && !empty($aImg['filename'])) { // sonst wirft die filesystem class nen fatal error, dass das file nicht existiert.
			$fs = $oFile->format_filesize($aImg['filesize'],'AUTO', 0);	
		}	
		else {
			$fs = 0;
		}
		$sStr = '<input type="hidden" name="aData[img_'.$I.']" value="'.$sValue.'">'."\n";
		$sStr .= '<a href="javascript:viewMediaWin(\''.$aImg['id'].'\','.$width.','.$height.');" title="View Uploadfile ['.$aImg['filename'].' | '.$fs.']">';
		$sStr .= $oMediadb->getMediaThumb($aImg['filename'], ' alt="Image '.$I.'"'); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
		$sStr .= '</a></td><td width="80%">';
		$sStr .= '<nobr><input type="text" name="img_'.$I.'_title" value="'.$aImg['name'].'" size="20" readonly>&nbsp;';
		$sStr .= '<input type="button" value="'.$aMSG['btn']['replace'][$syslang].'" name="btn[select_img_'.$I.']" class="smallbut" onClick="replaceOne('.$I.')"> ';
		$sStr .= '<input type="button" value="'.$aMSG['btn']['remove'][$syslang].'" name="btn[delete_img_'.$I.']" class="smallbut" onClick="removeOne(\'aData[img_'.$I.']\',\'img_'.$I.'_title\');"></nobr>';
		
		echo $sStr;
?>

	</td>
</tr>
<?php
	} // END for

	// eingabemoeglichkeit fuer weiteres bild
?>
 
<tr valign="top">
	<td colspan="3" id="trneu"><span class="small"><?php echo $MSG['add_picture'][$syslang]; ?></span></td>
	<td>
<?php // select-button for new picture
		$sStr = '<input type="hidden" name="aData[img_neu]" value="">'."\n";
		$sStr .= '<input type="text" name="img_neu_title" value="" size="20" readonly> ';
		$sStr .= '<input type="button" value="'.$aMSG['btn']['select'][$syslang].'" name="btn[select_img_neu]" class="smallbut" onClick="selectOne()">';
		
		echo $sStr;
?>
	</td>
</tr>

</table>
<br>
<input type="button" value="<?php echo $aMSG['btn']['save_close_win'][$syslang]; ?>" name="btn[close_win]" class="but" onClick="closeThisWin()">
<input type="submit" value="<?php echo $aMSG['btn']['clear_all'][$syslang]; ?>" name="btn[clear_all]" class="but" onClick="return confirmClearAll()">

</form>
<br>
</div>

</body>
</html>