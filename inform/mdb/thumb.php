<?php
/**	Script zur automatischen Thumbnail-Generierung (und speichert sie in einem Unterordner)
*	-> generiert nur wenn es noch nicht existiert!
*
* @param:	string	$image			kompletter (relativer!) Pfad zum Originalbild
* @param:	string	$subdir			Ein Unterordner relativ zum media-directory
* @param:	int		$x				gewuenschte Breite des Thumbnails
* @param:	int		$y				gewuenschte Hoehe des Thumbnails
* @param:	int		$resize			Vergroesserungen zuzulassen? [0|1] (default: 0)
* @param:	int		$aspectratio	Bildproportionen erhalten? [0|1] (default x|y: 0, x+y: 1)
*
* @version	2.5 / 2007-11-07 SECURITY FIX: Akzeptiert nur noch Dateinamen ohne Pfad und optional einen Unterordner (relativ zum "media"-Directory)
* #history  2.4 / 2005-01-19 (NEU: GET-vars richtig abholen + error-handling)
* #history	2.3 / 2005-01-18 (BUGFIX "GIF Create Support" + NEU: GET-vars abholen)
* #history	2.2 / 2004-12-21 (GDlib-check verbessert -> "if function_exists else" eingebaut)
* #history	2.1 / 2004-08-30 (genauer GDlib-check -> Fallback fuer fehlende GIF-Unterstuetzung!)
* #history	2.0 / 2004-05-04 (new_intranet)
*/

	require_once '../sys/_include_all.php';

//	error_reporting(0);
	$types = array (1 => "gif", "jpeg", "png", "swf", "psd", "wbmp");

	// GET-vars abholen
	$image			= (isset($_GET['image'])) ? basename($_GET['image']) : '';
	$subdir			= (isset($_GET['subdir'])) ? $_GET['subdir'] : '';
	$x				= (isset($_GET['x'])) ? $_GET['x'] + 0 : '';
	$y				= (isset($_GET['y'])) ? $_GET['y'] + 0 : '';
	$resize			= (isset($_GET['resize']) && $_GET['resize'] == 1) ? $GET['resize'] + 0 : 0;
	$bWeb			= (isset($_GET['bWeb']) && $_GET['bWeb'] == 0) ? true : false;
	$aspectratio	= (isset($_GET['aspectratio']) && $_GET['aspectratio'] == 1) ? $_GET['aspectratio'] + 0 : '';
	
	// subdir anpassen
	$subdir = preg_replace('/\\\\/', '/', $subdir);
	$subdir = preg_replace('/^[a-zA-Z0-9]+:/', '', $subdir);	
	if ($subdir[0] != '/') {
		$subdir = '/'. $subdir;
	}
	if (!preg_match('/\/$/', $subdir)) {
		$subdir .= '/';
	}
	$subdir = preg_replace('/\/(\.{1,2}\/)+/', '/', $subdir);
	$subdir = preg_replace('/\/+/', '/', $subdir);
	
	// Image
	$image = basename(preg_replace('/^[a-zA-Z0-9]+:/', '', $image));
	
	$image = preg_replace('/^.*\\\\/', '', $image);
	$image = $aENV['path']['root']['unix'] ."media" . $subdir . $image;	
	
	if($bWeb) {
		define ('CACHE', 'thumbs/');	
	}
	else {
		define ('CACHE', 'cache/');
	}
	if (empty($x))				unset($x);
	if (empty($y))				unset($y);
	if (empty($aspectratio))	unset($aspectratio);
	// check vars
	if (!isset($x) && !isset($y))	DIE('Fehlende(r) Groessenparameter!');
	if (empty($image))				DIE('Es wurde kein Bild angegeben!');
	if (!file_exists($image))		DIE('Die angegebene Datei konnte nicht auf dem Server gefunden werden!');

	// genauer GDlib-check ----------------------
	$icon = '';
	if (function_exists("gd_info")) { // "if function_exists else" added 20041221af
		$gd_info = gd_info();
		$imagedata = getimagesize($image);
		if (	(strToLower($types[$imagedata[2]]) == "gif" && $gd_info['GIF Create Support'] == false)
			 ||	(strToLower($types[$imagedata[2]]) == "jpeg" && $gd_info['JPG Support'] == false)
			 ||	(strToLower($types[$imagedata[2]]) == "png" && $gd_info['PNG Support'] == false)
			 ||	(strToLower($types[$imagedata[2]]) == "wbmp" && $gd_info['WBMP Support'] == false)
			 ||	(strToLower($types[$imagedata[2]]) == "psd")	) {
			$icon = "icn_image.gif";
		}
	} else {
		$icon = "icn_image.gif";
	}
	if (strToLower($types[$imagedata[2]]) == "swf") {
		$icon = "icn_flash.gif";
	}
	if (!empty($icon)) {
		$icon = $aENV['path']['pix_global']['unix'].$icon;
		header("Content-Type: image/gif");
		$i = fopen($icon, "rb");
		fpassthru($i);
		fclose($i);
		exit;
	} // icon anzeigen und fertig!

	// weiter mit GDlib ----------------------------------------

	$cachedir = substr($image, 0, strrpos($image, '/') + 1).CACHE;
	
	!is_dir($cachedir)
		? mkdir($cachedir, 0777)
		: system("chmod 0777 ".$cachedir);
	
	if (!isset($imagedata)) $imagedata = getimagesize($image);
	
	!$imagedata[2] || $imagedata[2] == 4 || $imagedata[2] == 5
		? DIE ('Bei der angegebenen Datei handelt es sich nicht um ein Bild!')
		: false;
	
	eval ('
	if (!(imagetypes() & IMG_'.strtoupper($types[$imagedata[2]]).')) {
		DIE ("Das ".strtoupper($types[$imagedata[2]])."-Format wird nicht unterstuetzt!");
	}
	');

	!isset($aspectratio)
		? isset($x) && isset($y)
			? $aspectratio = 1
			: $aspectratio = 0
		: $aspectratio;
	
	!isset($x)
		? $x = floor($y * $imagedata[0] / $imagedata[1])
		: $x;
	
	!isset($y)
		? $y = floor($x * $imagedata[1] / $imagedata[0])
		: $y;
	
	if ($aspectratio && isset($x) && isset($y)) {
		if ($imagedata[0] > $imagedata[1]) {
			$y = floor ($x * $imagedata[1] / $imagedata[0]);
		} else if ($imagedata[1] > $imagedata[0]) {
			$x = floor ($y * $imagedata[0] / $imagedata[1]);
		}
	}

	$thumbfile = substr($image, strrpos($image, '/') + 1);
	if (file_exists($cachedir.$thumbfile)) {
		$thumbdata = getimagesize($cachedir.$thumbfile);
		$thumbdata[0] == $x && $thumbdata[1] == $y
			? $iscached = true
			: $iscached = false;
	} else {
		$iscached = false;
	}

	if (!$iscached) {
		($imagedata[0] > $x || $imagedata[1] > $y) ||
		(($imagedata[0] < $x || $imagedata[1] < $y) && $resize)
			? $makethumb = true
			: $makethumb = false;
	} else {
		$makethumb = false;
	}

	header ("Content-Type: image/".$types[$imagedata[2]]);
	
	if ($makethumb) {
		$image = call_user_func("imagecreatefrom".$types[$imagedata[2]], $image);
		if (function_exists('imagecreatetruecolor') && ($imagedata[2] == 2 || $imagedata[2] == 3)) {
			$thumb = imagecreatetruecolor($x, $y); // use imagecreatetruecolor (bei GD 2.01 und JPG/PNG)
		} else {
			$thumb = imagecreate($x, $y); // use imagecreate
		}
		imagecopyresampled($thumb, $image, 0, 0, 0, 0, $x, $y, $imagedata[0], $imagedata[1]);
		call_user_func("image".$types[$imagedata[2]], $thumb, $cachedir.$thumbfile);
		imagedestroy($image);
		imagedestroy($thumb);
		$image = $cachedir.$thumbfile;
	} else {
		$iscached
			? $image = $cachedir.$thumbfile
			: $image = $image;
	}
	
	$image = fopen($image, "rb"); # NOTE: sorgt "rb" hin und wieder fuer verwirrung?
	fpassthru($image);
	fclose($image);
?>
