<?php
/**
* popup_viewprotected.php
*
* Popup, um aus dem CMS heraus eine Datei anzuzeigen, die mit der Form-Methode "file_upload()" 
* in das "protected"-Verzeichnis hochgeladen wurde. 
* -> kopierbar!
*
* @param	string	$filename		[welches File] (required)
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2005-05-04 [umgestellt auf: ALLE Dateitypen mit PHP-Klasse ausgeben]
* #history	1.0 / 2005-01-18 (neu)
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_viewprotected.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. USER	// Diese Seite sollen ALLE sehen duerfen
// 1c. init
	require_once ("../sys/php/_include_all.php");

// 2a. GET-params abholen
	$filename		= (isset($_GET['filename'])) ? $oFc->make_secure_string($_GET['filename']) : '';
	// fallback
	if (empty($filename)) { exit; }
	
// Filetype bestimmen
	$aAsAttachment = array('zip', 'pdf', 'doc');
	$ext = strToLower(substr(strrchr($filename, "."), 1)); // file-extension
	$mode = (in_array($ext, $aAsAttachment)) ? 'attachment' : 'inline';

// Datei ausgeben.
	require_once($aENV['path']['global_service']['unix']."class.download.php");
	$oDl =& new download(); // params: -
	$oDl->setFile($aENV['path']['data']['unix'].'cms/protected/'.$filename);
	$oDl->setName($filename);
	$oDl->setContentDisposition($mode);
	#$oDl->debug = true;
	$oDl->send();
?>