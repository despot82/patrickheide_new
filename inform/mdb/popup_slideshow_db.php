<?php
/**
* popup_slideshow.php
*
* Popup, um Bilder aus der MediaDB zu einer Slideshow zusammenzustellen.
* -> 2sprachig und kopierbar. 
*
* Diese Seite wird aus der JS-selectNewSlideshowWin(id,col_name,$formname) aufgerufen
*
* @param	int		$id			[um zu wissen um welchen Datensatz (Projekt...) es geht] (required)
* @param	string	$col_name	[um zu wissen um welche Spalte i.d. Tabelle es geht] (required)
* @param	string	$formname	[um zu wissen um welches Form i.d. openerpage es geht] (required)
* @param	int		$width		[zum filtern der auszuwaehlenden Bilder um eine maximale/fixe Breite] (optional)
* @param	int		$height		[zum filtern der auszuwaehlenden Bilder um eine maximale/fixe Hoehe] (optional)
* @param	int		$mustfit	[bestimmt ob $width/$height maximal/fix bedeutet] (optional)
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Frederic Hoppenstock <af@design-aspekt.com>
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_slideshow_db.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.Slideshow.php");
	require_once($aENV['path']['global_service']['unix']."class.form_admin.php");
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");

// 2a. GET-params abholen
	$fkid			= ($_GET['fkid']) ? $oFc->make_secure_int($_GET['fkid']) : '';
	$sid			= ($_GET['sid']) ? $oFc->make_secure_string($_GET['sid']) : '';
	$col_name		= ($_GET['col_name']) ? $oFc->make_secure_string($_GET['col_name']) : '';
	$formname		= ($_GET['formname']) ? $oFc->make_secure_string($_GET['formname']) : '';
	$width			= (isset($_GET['width'])) ? $oFc->make_secure_int($_GET['width']) : '';
	$height			= (isset($_GET['height'])) ? $oFc->make_secure_int($_GET['height']) : '';
	$delid			= ($_GET['delid']) ? $oFc->make_secure_int($_GET['delid']) : '';
	$mustfit		= (isset($_GET['mustfit']) && $_GET['mustfit'] == 1) ? 1 : 0;
	$remoteSave		= (isset($_GET['remoteSave']) && $_GET['remoteSave']==1) ? 1 : 0;
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	// POST ueberschreibt ggf. GET
	if (isset($_POST['col_name']))	{$col_name	= $oFc->make_secure_string($_POST['col_name']);}
	if (isset($_POST['formname']))	{$formname	= $oFc->make_secure_string($_POST['formname']);}
	if (isset($_POST['width']))		{$width		= $oFc->make_secure_int($_POST['width']);}
	if (isset($_POST['height']))	{$height	= $oFc->make_secure_int($_POST['height']);}
	if (isset($_POST['mustfit']))	{$mustfit	= $oFc->make_secure_int($_POST['mustfit']);}
	if (isset($_POST['remoteSave']) && $_POST['remoteSave']==1)	{ $remoteSave = 1; }

// 2c. Vars:
	$sAddSizeJsVars = ",'".$width."','".$height."','".$mustfit."','slideshowWin'";
	$oSlide =& new Slideshow($oDb,$Userdata);
	// MEDIA-DB
	if (!is_object($oMediadb)) {
		$oMediadb =& new mediadb(); // params: - 
	}
	$oSlide->setMediadb($oMediadb);
	$sTable = 'sys_slideshow';
// 2d. Texte
	$MSG = array();
	$MSG['edit_slideshow']['de']	= $aENV['slideshow']['de']." bearbeiten";
	$MSG['edit_slideshow']['en']	= "Edit ".$aENV['slideshow']['en'];
	$MSG['create_slideshow']['de']	= $aENV['slideshow']['de']." erstellen";
	$MSG['create_slideshow']['en']	= "Create ".$aENV['slideshow']['en'];
	$MSG['add_picture']['de']		= "+ ".$aMSG['form']['picture']['de'];
	$MSG['add_picture']['en']		= "Add ".$aMSG['form']['picture']['en'];

// 3. DB
	if (isset($btn['save']) || $remoteSave == 1) {
		// DB-action: update data
		if ($aData && $col_name) {
			//foreach ($aData as $key => $val) { if ($val == "") { unset($aData[$key]); } } // geloeschte aus dem array entfernen
			$oSlide->save($aData);	
		}
	}
	
	if(isset($delid) && !empty($delid)) {
		$oDb->make_delete(array('id' => $delid),$sTable);
		header('Location: '.$aENV['PHP_SELF'].'?sid='.$sid.'&fkid='.$fkid.'&formname='.$formname.'&col_name='.$col_name.'&width='.$width.'&height='.$height.'&mustfit='.$mustfit);
	}
	
	if (isset($btn['clear_all'])) {
		// DB-action: clear all fields
		$oSlide->clearAll($sid,$fkid);
		header('Location: '.$aENV['PHP_SELF'].'?sid='.$sid.'&fkid='.$fkid.'&formname='.$formname.'&col_name='.$col_name.'&width='.$width.'&height='.$height.'&mustfit='.$mustfit);

	}
	if (!isset($aData['fkid'])) $aData['fkid'] = $fkid;
	if (!isset($aData['sid'])) $aData['sid'] = $sid;	
	if ($aData['sid'] || $sid) { $aData = $oSlide->getSlideshow($aData['sid'],$aData['fkid']); } 

	$sGETvars = '?sid='.$aData['sid'].'&fkid='.$aData['fkid'].'&formname='.$formname.'&col_name='.$col_name.'&width='.$width.'&height='.$height.'&mustfit='.$mustfit;
	$oForm =& new form_admin($aData);
// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
?>

<script language="JavaScript" type="text/javascript">
// bgColors der TRs (kann NICHT durch das stylesheet definiert werden!)
var bgClassHighlight = 'sub3';
var bgClassNormal = 'sub1';
var memoryId;

function replaceOne(p) {
	// pruefen, ob 'popup_selectmedia.php' nicht geoeffnet (oder zwischenzeitlich geschlossen) wurde...
	if (window.opener.mediaWin == null || window.opener.mediaWin.document == undefined) { // ... und ggf. oeffnen
		opener.selectMediaWin('image','img_neu','slideshowForm'<?php echo $sAddSizeJsVars; ?>);
		return setTimeout("replaceOne('"+p+"')", 100); // aussteigen und nochmal probieren (popup braucht zeit zum laden!)
	}
	window.opener.mediaWin.document.forms['selectmediaForm']['formfield'].value='img_' + p;
	document.forms['slideshowForm'].elements["aData[replaceSave]"].value = p;
	// highlight
	pId = 'tr' + p;
	document.getElementById(pId).className = bgClassHighlight;
	document.getElementById('trneu').className = bgClassNormal;
	if (memoryId && memoryId != pId) {
		document.getElementById(memoryId).className = bgClassNormal;
	}
	memoryId = pId; // zuletzt geklickten merken
	window.opener.mediaWin.focus();
}
function selectOne() {
	// pruefen, ob 'popup_selectmedia.php' nicht geoeffnet (oder zwischenzeitlich geschlossen) wurde...
	if (window.opener.mediaWin == null || window.opener.mediaWin.document == undefined) {  // ... und ggf. oeffnen
		opener.selectMediaWin('image','img_neu','slideshowForm'<?php echo $sAddSizeJsVars; ?>);
		return setTimeout('selectOne', 100); // aussteigen und nochmal probieren (popup braucht zeit zum laden!)
	}
	window.opener.mediaWin.document.forms['selectmediaForm']['formfield'].value='img_neu';
	document.forms['slideshowForm'].elements["aData[replaceSave]"].value = 0;
	// highlight
	document.getElementById('trneu').className = bgClassHighlight;
	if (memoryId) {
		document.getElementById(memoryId).className = bgClassNormal;
	}
	window.opener.mediaWin.focus();
}
// funktionen zu den buttons (clear_all, close_win)
function confirmClearAll() {
	var chk = window.confirm('<?php echo $aMSG['err']['clear_all_images'][$syslang]; ?>');
	return(chk);
}
function closeThisWin() {
	window.opener.mediaWin.close();
	self.close();
}
// funktion, die beim laden der seite ausgefuehrt wird
function init() {
<?php // beim ERSTMALIGEN laden der seite das selectmedia-fenster oeffnen
	if (!$updated) {
		#echo "if (window.opener.mediaWin == null || window.opener.mediaWin.document == undefined) { opener.selectMediaWin('image','img_neu','slideshowForm'".$sAddSizeJsVars."); }";
		echo "opener.selectMediaWin('image','img_neu','slideshowForm'".$sAddSizeJsVars.");";
	} else { // sonst nur das hiddenfield 'formfield' neu beschreiben
		echo "window.opener.mediaWin.document.forms['selectmediaForm']['formfield'].value = 'img_neu';";
	} ?>
	// 'trneu' highlighten
	document.getElementById('trneu').className = bgClassHighlight;
}
<?php // nach dem leeren aller images fenster schliessen
	#echo ($closeThisWin) ? "closeThisWin();" : "onload = init;"; // DEACTIVATED 2005-05-04af
?>
onload = init;
</script>

<div id="contentPopup">

<span class="title"><?php echo (!empty($aData[$col_name])) ? $MSG['edit_slideshow'][$syslang] : $MSG['create_slideshow'][$syslang]; ?><br></span>
<br>
<?=$oForm->start_tag($aENV['PHP_SELF'],$sGETvars,'POST','slideshowForm',' target="_self"')?>
<? 
	echo $oForm->hidden('sid');
	echo $oForm->hidden('fkid');
	echo $oForm->hidden('replaceSave',0);
?>
<input type="hidden" name="col_name" value="<?php echo $col_name; ?>">
<input type="hidden" name="formname" value="<?php echo $formname; ?>">
<input type="hidden" name="width" value="<?php echo $width; ?>">
<input type="hidden" name="height" value="<?php echo $height; ?>">
<input type="hidden" name="mustfit" value="<?php echo $mustfit; ?>">
<input type="hidden" name="remoteSave" value="0">


<?php // warnmeldung DEBUG
	if (!$aData['sid'] || !$col_name || !$aData['fkid']) { echo '<p><font color="red"><b>ERROR: (!$sid || !$col_name || $fkid) !!!</b></font></p>'; } ?>


<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php // Vars
	movePrio($sTable, "fk_id");
	$entries = $oSlide->getEntries();

	for ($i=0; is_array($aData[$i]); $i++) {
		$I = $i+1;
?>
<tr valign="top">
	<td align="center"><?php // prio-buttons
		echo get_prio_buttons($I, $entries, $aData[$i]['id'], $aData[$i]['fk_id'], $sGETvars); // params: $nCounter,$nEntries,$sID[,$sCat=''][,$sGEToptions='']
	?></td>
	<td width="10%" id="tr<?php echo $I; ?>"><span class="small"><?php #echo $aMSG['form']['image'][$syslang]; ?>&nbsp;<?php echo $I; ?></span></td>
	<td width="10%" align="center">
<?php // if picture is in DB -> show thumbnail and replace/delete-buttons
		
		$width = (!empty($aData[$i]['aImg']['width'])) ? $aData[$i]['aImg']['width'] : "''";
		$height = (!empty($aData[$i]['aImg']['height'])) ? $aData[$i]['aImg']['height'] : "''";
		
		$sStr = $oForm->hidden('img_'.$I)."\n";
		$sStr .= '<a href="javascript:viewMediaWin(\''.$aData[$i]['mdb_id'].'\','.$width.','.$height.');" title="View Uploadfile ['.$aData[$i]['aImg']['filename'].' | '.$oFile->format_filesize($aData[$i]['aImg']['filesize'], 'AUTO', 0).']">';
		$sStr .= $oMediadb->getMediaThumb($aData[$i]['aImg']['filename'], ' alt="Image '.$I.'"'); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
		$sStr .= '</a></td><td width="80%">';
		$sStr .= '<nobr>'.$oForm->textfield('img_'.$I.'_title',120,20,'','readonly="readonly"').'&nbsp;';
		$sStr .= '<input type="button" value="'.$aMSG['btn']['replace'][$syslang].'" name="btn[select_img_'.$I.']" class="smallbut" onClick="replaceOne('.$I.')"> ';
		$sStr .= '<a href="'.$aENV['PHP_SELF'].$sGETvars.'&delid='.$aData[$i]['id'].'" title="'.$aMSG['btn']['delete'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_delete.gif" alt="'.$aMSG['btn']['delete'][$syslang].'" class="btn"></a>';
		echo $sStr;
?>

	</td>
</tr>
<?php
	} // END for

	// eingabemoeglichkeit fuer weiteres bild
?>
 
<tr valign="top">
	<td colspan="3" id="trneu"><span class="small"><?php echo $MSG['add_picture'][$syslang]; ?></span></td>
	<td>
<?php // select-button for new picture
		$sStr = '<input type="hidden" name="aData[img_neu]" value="">'."\n";
		$sStr .= '<input type="text" name="img_neu_title" value="" size="20" readonly> ';
		$sStr .= '<input type="button" value="'.$aMSG['btn']['select'][$syslang].'" name="btn[select_img_neu]" class="smallbut" onClick="selectOne()">';
		
		echo $sStr;
?>
	</td>
</tr>

</table>
<br>
<input type="button" value="<?php echo $aMSG['btn']['save_close_win'][$syslang]; ?>" name="btn[close_win]" class="but" onClick="closeThisWin()">
<input type="submit" value="<?php echo $aMSG['btn']['clear_all'][$syslang]; ?>" name="btn[clear_all]" class="but" onClick="return confirmClearAll()">

</form>
<br>
</div>

</body>
</html>