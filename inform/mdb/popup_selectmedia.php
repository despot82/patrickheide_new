<?php
/**
* popup_selectmedia.php
*
* Popup, um ein Mediafile aus der MediaDB auszuwaehlen. 
* -> 2sprachig und kopierbar!
*
* @param	string	$formfield		[um zu wissen in welches Feld uebertragen werden soll] (required)
* @param	string	$formname		[um zu wissen in welches Formular ubertragen werden soll] (required)
* @param	string	$mediatype		[um nur eine (oder mehrere kommagetrennte) Media-Art(en) anzuzeigen -> beeinflusst type-dropdown!] (optional)
* @param	string	$searchMTerm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$searchMType	[zum filtern der Anzeige auf nur einen Mediatyp (diese sind i.d. _include_all definiert)] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$searchMDir		[zum filtern der Anzeige um ein Directory (im Tree)] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	int		$width			[zum filtern der Anzeige um eine maximale/fixe Breite] (optional)
* @param	int		$height			[zum filtern der Anzeige um eine maximale/fixe Hoehe] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	3.2 / 2005-04-26 [neuer Filter als GET-Parameter "mustfit" + umbenannt: "maxwidth"/"maxheight" in "width"/"height"]
* #history	3.1 / 2005-03-31 [neue Filter als GET-Parameter "maxwidth" + "maxheight"]
* #history	3.0 / 2005-01-10 [upload-path auf neue MediaDB umgestellt - NICHT MEHR ABWAERTSKOMPATIBEL!]
* #history	2.04 / 2004-09-20 ["icn_" hinzugefuegt]
* #history	2.03 / 2004-07-21 [BUGFIX: $mediatype default]
* #history	2.02 / 2004-06-28 [BUGFIX: rel.-path fuer thumbs]
* #history	2.01 / 2004-06-02 [$mediatype einschraenkungen eingebaut]
* #history	2.0 / 2004-05-04 (new_intranet)
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_selectmedia.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_module']['unix']."class.Mdb.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
	require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	if (isset($_GET['searchMTerm'])) { $searchMTerm = $oFc->make_secure_string($_GET['searchMTerm']); } // suchbegriff (kein default wegen session!)
	if (isset($_GET['searchMType'])) { $searchMType = $oFc->make_secure_string($_GET['searchMType']); } // mediatype (kein default wegen session!)
	if (isset($_GET['searchMDir']))  { $searchMDir = $oFc->make_secure_string($_GET['searchMDir']); } // tree-dir (kein default wegen session!)
	$mediatype		= (isset($_GET['mediatype']) && !empty($_GET['mediatype'])) ? strToLower($oFc->make_secure_string($_GET['mediatype'])) : 'all';
	$formfield		= (isset($_GET['formfield'])) ? $oFc->make_secure_string($_GET['formfield']) : '';
	$formname		= (isset($_GET['formname'])) ? $oFc->make_secure_string($_GET['formname']) : '';
	$width			= (isset($_GET['width'])) ? $oFc->make_secure_int($_GET['width']) : '';
	$height			= (isset($_GET['height'])) ? $oFc->make_secure_int($_GET['height']) : '';
	$mustfit		= (isset($_GET['mustfit']) && $_GET['mustfit'] == 1) ? 1 : 0;
	$opener			= (isset($_GET['opener'])) ? $oFc->make_secure_string($_GET['opener']) : '';
// 2b. POST-params abholen
// 2c. Vars
	$mwidth = (!empty($aData['width'])) ? $aData['width'] : '';
	$hwidth = (!empty($aData['height'])) ? $aData['height'] : '';
	$sTable = $aENV['table']['mdb_media'];
	$limit = 50;	// Datensaetze pro Ausgabeseite einstellen
	if (isset($_GET['mediatype']) && !empty($_GET['mediatype']) && !isset($_GET['searchMType'])) { // Get[mediatype] ueberschreibt Session!
		// bei mehreren kommagetrennten mediatypen nehme nur die erste!
		$searchMType = (strstr($mediatype, ",")) ? substr($mediatype, 0, strpos($mediatype, ",")) : $mediatype;
	}
	if (!isset($searchMType))	{ $searchMType = 'all'; } 	// set default
	if (!isset($searchMDir))	{ $searchMDir = '0'; } 		// set default
	// GET-Parameter fuer DB-Navi
	$sDbnaviGetParams = '&formfield='.$formfield.'&formname='.$formname.'&mediatype='.$mediatype.'&width='.$width.'&height='.$height.'&mustfit='.$mustfit.'&opener='.$opener;

// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();
	// TREE
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], MDB_STRUCTURE_FILE, 'mdb',array('sOrderBy' => 'prio DESC')); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions='']
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();
	
	// MEDIA-DB
	$oMediadb =& new mediadb(); // params: - 
	// MDB
    $oMdb =& new Mdb($aENV, $oDb);
	$oMdb->setSess($oSess);
	$oMdb->setDate($oDate);
	$oMdb->setFile($oFile);
	$oMdb->setTree($oTree);
	$oMdb->setUserdata($Userdata);
	$oMdb->setSearchMDir($searchMDir);
	$oMdb->setSearchMType($searchMType);
	$oMdb->setSearchMTerm($searchMTerm);

// 3. DB: select
	$oMdb->authThreads($oTree->getAllAuthedIds()); // params: $aTreeId
	$aMediafile = $oMdb->select($start, $limit, false, $width, $height, $mustfit);
	$entries = $oMdb->getEntries();	// noetig fuer DB-Navi

	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");

?>

<script language="JavaScript" type="text/javascript">
// puts value into input field of opener page and save (to reload)
function put_value(valID,valTitel) {
	if (document.forms['selectmediaForm']['formfield'].value == '') { // fallback (sollte nie vorkommen...)
		alert('<?php echo $aMSG['err']['no_formfield'][$syslang]; ?>')
	} else {
<?php // slideshowWin kommt ggf. als GET-parameter "opener" ?>
		targetwin = window.opener<?php if ($opener=='slideshowWin') echo '.slideshowWin'; ?>;
		// dieses fenster
		fn = document.forms['selectmediaForm']['formname'].value;
		if (fn == '') {fn = '0';}
		n = 'aData[' + document.forms['selectmediaForm']['formfield'].value + ']';
		m = document.forms['selectmediaForm']['formfield'].value + '_title';
		// opener fenster
		targetwin.document.forms[fn][n].value = valID;
		targetwin.document.forms[fn][m].value = valTitel;
		targetwin.document.forms[fn]['remoteSave'].value = 1;
		targetwin.document.forms[fn].submit();
		targetwin.focus();
		// nach speichern schliessen (ausser im slideshow-win-modus)
		if (targetwin != window.opener.slideshowWin) {
			self.close();
		}
	}
}
</script>

<div id="contentPopup">
<span class="title"><?php echo $aMSG['btn']['select'][$syslang]; ?><br></span>
<br>
<form action="<?php echo $aENV['PHP_SELF']; ?>" name="selectmediaForm" method="get">
<input type="hidden" name="formfield" value="<?php echo $formfield; ?>">
<input type="hidden" name="formname" value="<?php echo $formname; ?>">
<input type="hidden" name="mediatype" value="<?php echo $mediatype; ?>">
<input type="hidden" name="width" value="<?php echo $width; ?>">
<input type="hidden" name="height" value="<?php echo $height; ?>">
<input type="hidden" name="mustfit" value="<?php echo $mustfit; ?>">
<input type="hidden" name="opener" value="<?php echo $opener; ?>">

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<td><!-- suche-modul -->
			<input type="text" name="searchMTerm" value="<?php echo $searchMTerm; ?>">
<?php		// mediatypes dropdown (NUR bei "all" oder bei kommaseparierten typen!)
			echo $oMdb->getMediaTypesDropdown($aMSG['mdb']['all_filetypes'][$syslang],$mediatype,false,true);
			// mediadirs (tree)
			echo $oMdb->getMediaDirsDropdown();
			// search button
			echo get_button("SEARCH",$syslang); // params: $sType,$sLang[,$sClass="but"] ?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
			<td><p><?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></p></td>
			<td align="right"><p><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks($sDbnaviGetParams, 5); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></p></td>
			</tr></table>
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="25%">&nbsp;</th>
		<th width="75%"><?php echo $aMSG['form']['filename'][$syslang]; ?></th>
	</tr>
<?php
// print files
	foreach ($aMediafile as $mediaid => $aData) {
		$sGEToptions = "?id=".$aData['id']."&start=".$start;
		$type = $oMediadb->getMediaType($aData['filename']); // params: $sFilename[,$sAlt='']
		$icon = $oMediadb->getMediaThumb($aData['filename']); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
		$sFilesize = $oFile->format_filesize($aData['filesize'], 'AUTO', 0); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		// abmessungen bei img/swf
		$dim = '';
		$w = (isset($aData['width'])) ? $aData['width'] : "''";
		$dim .= (isset($aData['width'])) ? $aData['width'] : "-";
		$h = (!empty($aData['height'])) ? $aData['height'] : "''";
		$dim .= (isset($aData['height'])) ? "x".$aData['height'] : "-";
		$dim = (!empty($dim)) ? $dim : '?';
		// output HTML: ?>
	<tr valign="top">
		<td align="center"><?php // thumb/icon
			echo '<a href="javascript:viewMediaWin(\''.$aData['id'].'\','.$w.','.$h.')" title="View Uploadfile ['.$aData['name'].' | '.$sFilesize.']">'.$icon.'</a>';
			?></td>
		<td><span class="small"><a href="javascript:put_value('<?php echo $aData['id'] ?>','<?php echo $aData['name']; ?>')" title="<?php echo $aMSG['media']['clicktocopy'][$syslang]; ?>"><?php echo substr($aData['name'], 0, 70); ?></a><br>
			<?php if ($aData['description']) { echo $aData['description']."<br>\n"; } ?>
			<?php if ($aData['width']) { echo "[ ".$w." x ".$h." ]"; } ?></span>
		</td>
	</tr>
<?php } // END while ?>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
			<td><p><?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></p></td>
			<td align="right"><p><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks($sDbnaviGetParams, 5); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></p></td>
			</tr></table>
		</td>
	</tr>
</table>
</form>
<br>
</div>

</body>
</html>