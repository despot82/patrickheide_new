<?php
/**
* mdb.php
*
* Overviewpage: media-db //-> 2sprachig und voll kopierbar!
*
* @param	int		$start			[zum richtigen zurueckspringen] (optional)
* @param	string	$searchMTerm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$searchMType	[zum filtern der Anzeige auf nur einen Mediatyp (diese sind i.d. _include_all definiert)] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$searchMDir		[zum filtern der Anzeige um ein Directory (im Tree)] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$folder			[zum ueberschreiben des Tree-Directorys $searchMDir] (optional)
* @param	array	Formular:		$btn
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.4 / 2006-03-14 (BUGFIX bei session-params abholen und moveto)
* #history	2.3 / 2005-04-06 (auf MediaDB-Klasse umgestellt)
* #history	2.2 / 2004-09-06 (auf Form-Methode "get_media_type()" umgestellt)
* #history	2.1 / 2004-08-31 (icon/thumbs auf Form-Methode "get_media_icon()" umgestellt)
* #history	2.0 / 2004-05-04 (new_intranet)
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mdb.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_module']['unix']."class.Mdb.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
	require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	$btn			= (isset($_GET['btn'])) ? $_GET['btn'] : array();
	if (isset($_GET['folder'])) { $searchMDir = $oFc->make_secure_string($_GET['folder']); } // tree-dir (kein default wegen session!)
	$sGEToptions	= '';
	
// 2b. POST-params abholen
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$movetoMDir		= (isset($_POST['movetoMDir'])) ? $_POST['movetoMDir'] : '';
	
// 2c. Vars:
	$sEditPage		= $aENV['SELF_PATH']."mdb_detail.php";	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;				// fuer NEW-button
	if (!isset($searchMType))	{ $searchMType = 'all'; } 	// set default
	if (!isset($searchMDir))	{ $searchMDir = '0'; } 		// set default
	$sTable	= $aENV['table']['mdb_media'];
	$limit	= 20;	// Datensaetze pro Ausgabeseite einstellen
	// orderby-message
	$sOrderbyMsg = (isset($searchMTerm) && $searchMTerm != '') 
		? $aMSG['form']['filename']
		: $aMSG['std']['last_mod'];

// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();
	
	// TREE
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], MDB_STRUCTURE_FILE, 'mdb',array('sOrderBy' => 'prio DESC')); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions='']
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();
	
	// MEDIA-DB
	$oMediadb =& new mediadb(); // params: - 

	// MDB
    $oMdb =& new Mdb($aENV, $oDb);
	$oMdb->setSess($oSess);
	$oMdb->setDate($oDate);
	$oMdb->setFile($oFile);
	$oMdb->setTree($oTree);
	$oMdb->setUserdata($Userdata);
	$oMdb->setSearchMDir($searchMDir);
	$oMdb->setSearchMType($searchMType);
	$oMdb->setSearchMTerm($searchMTerm);

	// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']

// DB-action: delete
	if($_SERVER["REQUEST_METHOD"] == "POST" && !isset($btn['save'])) {
		$oMdb->doBatchDelete($aData);
	}
// DB-action: update
	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($btn['save']) && $movetoMDir != '') {
		$oMdb->doBatchUpdate($aData, $movetoMDir);
		// Formdata manuell leeren!
		unset($oForm->aData);
	}
// DB-action: select
	$oMdb->authThreads($oTree->getAllAuthedIds()); // params: $aTreeId
	$aMediafile = $oMdb->select($start, $limit, true);
	$entries = $oMdb->getEntries();	// noetig fuer DB-Navi
	
	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['mdb']['unix']."inc.mdb_folder.php");

// GD-CHECK (DEBUG)
	echo "<!-- gd-check: ---------var_dump(gd_info):";
	if (function_exists("gd_info")) { $gd_info = gd_info();	var_dump($gd_info); }
	echo "\n -->";
?>

<script language="JavaScript" type="text/javascript">
  function submitForm() {  
  	if(confirmDelete()) {
		document.forms['mvdelForm'].submit();  
  	}
  }
</script>
  
<form action="<?php echo $aENV['PHP_SELF']; ?>" method="get">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['mdb'][$syslang]; ?></span> 
			<?php echo $aMSG['std']['orderby'][$syslang]; ?> "<?php echo $sOrderbyMsg[$syslang]; ?>"</p></td>
		<td align="right" nowrap><?php if ($oPerm->hasPriv('create')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"] ?></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<td class="th"><!-- suche-modul -->
			<input type="text" name="searchMTerm" value="<?php echo $searchMTerm; ?>"> <?php
			// mediatypes
			echo $oMdb->getMediaTypesDropdown($aMSG['mdb']['all_filetypes'][$syslang],'all',true,true);
			// mediadirs (tree)
			echo $oMdb->getMediaDirsDropdown(true);
			// search button
			echo get_button("SEARCH", $syslang); // params: $sType,$sLang[,$sClass="but"] ?>
		</td>
	</tr>
</table>
</form>

<table width="100%" border="0" cellspacing="1" cellpadding="2">
	<tr>
		<td><p><small><?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></small></p>
		</td>
		<td align="right"><p><small><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks(); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></small></p>
		</td>
	</tr>
</table>
<br>
<?=$oForm->start_tag($_SERVER["PHP_SELF"],$getvars,'POST','mvdelForm')?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="55%" colspan="3"><?php echo $aMSG['form']['file'][$syslang]; ?>/<?php echo $aMSG['form']['description'][$syslang]; ?></th>
		<th width="20%" colspan="2"><?php echo $aMSG['form']['dimensions'][$syslang]; ?></th>
		<th width="15%"><?php echo $aMSG['form']['filetype'][$syslang]; ?></th>
	</tr>
<?php 
	// set vars
	$aCheckboxId = array();	// Array zum Speichern aller Checkbox-Namen, damit diese ueber JS ueber "alle auswaehlen" umgeschaltet werden koennen
	
	// datensaetze ausgeben
	foreach ($aMediafile as $mediaid => $aData) {
		$sGEToptions = "?id=".$aData['id']."&start=".$start;

		// abmessungen bei img/swf
		$dim	= '';
		$w		= (isset($aData['width'])) ? $aData['width'] : "''";
		$dim	.= (isset($aData['width'])) ? $aData['width'] : "-";
		$h		= (!empty($aData['height'])) ? $aData['height'] : "''";
		$dim	.= (isset($aData['height'])) ? "x".$aData['height'] : "-";
		$dim	= (!empty($dim)) ? $dim : '?';

		// filesize
		$filesize = $oFile->format_filesize($aData['filesize'], 'AUTO', 0); // params: $sFile[,$sExt='AUTO'][,$nPrecision=2]
		
		// filetype
		$filetype = $oMediadb->getMediaType($aData['filename']); // params: $sFilename[,$sAlt='']

		// thumb/icon
		$thumb = $oMediadb->getMediaThumb($aData['filename']); // params: $sFilename[,$sExtra=''][,$sPathKey='mdb_upload']
		$thumb = '<a href="javascript:viewMediaWin(\''.$aData['id'].'\','.$w.','.$h.')" title="View Uploadfile ['.$aData['name'].' | '.$filesize.']">'.$thumb.'</a>';

		// del/moveto checkbox
		$actioncheckbox = '';
		if ($oPerm->hasPriv('edit') || $oPerm->hasPriv('delete')) {
			$actioncheckbox = $oForm->checkbox("mvdel".$aData['id']);
			$aCheckboxId[] = "mvdel".$aData['id'];
		}
		//description
		if (isset($aData['description']) && !empty($aData['description'])) {
			$aData['description'] .= "<br>\n";
		}
		// output HTML:
?>
	<tr>
		<td width="5%" align="center" class="sub2"><?php echo $actioncheckbox; ?></td>
		<td width="5%" align="center"><p><?php echo $thumb; ?></p></td>
		<td><p><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><?php echo $aData['name']; ?></a><br>
			<?php echo $aData['description']; ?>
			<small><?php echo $aData['path']; ?></small></p>
		</td>
		<td nowrap><p><small><?php echo $filesize; ?></small></p></td>
		<td nowrap><p><small><?php echo $dim; ?></small></p></td>
		<td><p><?php echo $filetype; ?></p></td>
	</tr>

<?php } // END while
	if ($entries == 0) { echo '<tr><td colspan="6" align="center"><p>'.$aMSG['std']['no_entries'][$syslang].'</p></td></tr>'; }
?>
	
<?php
    if($entries > 0 && ($oPerm->hasPriv('edit') || $oPerm->hasPriv('delete'))) {
?>
	<tr>
		<td align="center" class="sub3">
			<script language="JavaScript1.2" type="text/javascript">
			<!--
			var aCheckboxId = new Array('<?php echo implode("','", $aCheckboxId); ?>');
			
			function checkAll(switchState) {
				for (var i=0; i<aCheckboxId.length; i++) {
					checkbox = document.getElementById(aCheckboxId[i]);
					if (switchState == true) { checkbox.checked = true; }
					else { checkbox.checked = false; }
				}
			}
			//--></script>
			<input type="checkbox" name="selectAll" id="selectAll" value="" class="radiocheckbox" onClick="checkAll(this.checked)" />
		</td>
		<td colspan="5" class="sub3"><p>&nbsp;&nbsp;&nbsp;<label for="selectAll"><?php echo $aMSG['mdb']['chooseall'][$syslang]; ?></label></p></td>
	</tr>
	<tr><td colspan="6" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	<tr>
		<td colspan="6" class="sub3"><p><?php
			// delete
			if ($oPerm->hasPriv('delete')) {
				echo '<a href="#" onClick="submitForm()">'.$aMSG['btn']['deleteselected'][$syslang].'</a>&nbsp;&nbsp;|&nbsp;&nbsp;';
			}
			// moveto
			echo $aMSG['form']['move_to'][$syslang].'&nbsp;';
			// mediadirs (tree)
			echo $oMdb->getMediaDirsDropdown(true, false, "movetoMDir");
			// search button
			echo get_button("SAVE", $syslang); // params: $sType,$sLang[,$sClass="but"]
			?></p>
		</td>
	</tr>
<?php	} ?>
</table>
<?php echo $oForm->end_tag(); ?>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2">
	<tr>
		<td><p><small><?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></small></p>
		</td>
		<td align="right"><p><small><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks(); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></small></p>
		</td>
	</tr>
</table>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>