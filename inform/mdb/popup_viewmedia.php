<?php
/**
* popup_viewmedia.php
*
* Popup, um ein Mediafile aus der MediaDB anzuzeigen. 
* -> kopierbar!
*
* @param	int		$id		[welches File aus der Media-DB - aus der JS-function viewMediaWin(id,w,h)] (required)
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.3 / 2006-03-27 [Umbau wegen MS-IE-BUG]
* #history	2.2 / 2005-05-04 [umgestellt auf: ALLE Dateitypen mit PHP-Klasse ausgeben]
* #history	2.1 / 2005-04-06 [auf MediaDB-Klasse umgestellt]
* #history	2.0 / 2005-01-10 [upload-path auf neue MediaDB umgestellt - NICHT MEHR ABWAERTSKOMPATIBEL!]
* #history	1.1 / 2003-07-21 [kommentiert]
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_viewmedia.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

	if (isset($_GET['id'])) {
		// include_all einbinden
		define('DONT_SEND_HEADERS', true);
		require_once ('../sys/php/_include_all.php');
		
		// ID abholen
		$id	= $oFc->make_secure_int($_GET['id']);
		
		// media-daten ermitteln
		require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
		$oMediadb =& new mediadb(); // params: - 
		$aData = $oMediadb->getMedia($id); // params: $nMediaId
		
		// fallback
		if (empty($aData['filename'])) { exit; }
		
		// set vars
		$file	= $aENV['path']['mdb_upload']['unix'].$aData['filename'];
		$name	= str_replace(' ', '_', $aData['name']);
		$type	= $aData['filetype'];
		// alle Bilder inline, alles andere als Attachment!
		$mode = ($oMediadb->getMediaType($aData['name']) == 'image') ? 'inline' : 'attachment';
		
		// wenn MS-IE ->seite mit neuen parametern nochmal abschicken (weil IE und _include_all nicht geht!)
		if (isset($_SERVER['HTTP_USER_AGENT']) && strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) { // name als letztes!
			header("Location: ".$aENV['PHP_SELF']."?file=".$file."&mode=".$mode."&type=".$type."&name=".$name);
		}
	}
	else {
		// Wenn IE, dann wird dieses script zum zweiten mal mit ALLEN file-infos aufgerufen
		// WICHTIG: include_all NICHT einbinden(!)
		
		// GET-Vars abholen
		$file	= (strip_tags($_GET['file']));
		$mode	= (isset($_GET['mode']) && $_GET['mode'] == 'attachment') ? 'attachment' : 'inline';
		$type	= (strip_tags(str_replace('..', '.', $_GET['type'])));
		$name	= (strip_tags(str_replace('..', '.', $_GET['name'])));
	}

	// fallback
	if (empty($file)) { exit; }
	
	// Datei ausgeben.
	require_once("../sys/php/global/service/class.download.php");
	$oDl =& new download();
	$oDl->setFile($file);
	$oDl->setName($name);
	$oDl->setContentType($type);
	$oDl->setContentDisposition($mode);
//	$oDl->debug = true;
	$oDl->send();		
?>