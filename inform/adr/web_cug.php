<?php
/**
* web_cug.php
*
* Overviewpage: CUG-usergroups (also alle, die ein Web-System (CMS, Clientsites,...) nutzen wollen)
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2006-03-02 (layout wie sys_usergroup + rechte auf ADR umgestellt)
* #history	1.0 / 2004-08-24
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './web_cug.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	#$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "cug_user";
	$sEditPage		= $aENV['SELF_PATH']."web_cug_detail.php";	// fuer EDIT-link
	$sNewPage		= $aENV['SELF_PATH']."web_cug_detail.php";	// fuer NEW-link
	$sUserEditPage	= $aENV['SELF_PATH']."adr_edit_w.php";		// fuer EDIT-link
	$sUserNewPage	= $aENV['SELF_PATH']."adr_edit_d.php";		// fuer NEW-button

// CUG
	require_once($aENV['path']['global_service']['unix']."class.cug.php");
	$oCug =& new cug($aENV['db'], $syslang);

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>

<script language="JavaScript" type="text/javascript">
function openContactWin(url) { // opens new window
	var left = (screen.width - 470);
	owin = open(url,'ContactWin','toolbar=0,status=1,scrollbars=1,resizable=1,width=450,height=520,top=10,left='+left);
	owin.focus();
}
</script>

<form action="<?php echo $aENV['PHP_SELF'].$sGEToptions; ?>" method="get">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['cug'][$syslang]; ?></span></p></td>
		<td align="right"><?php if ($oPerm->hasPriv('create')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"] ?></td>
	</tr>
</table>
<br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<th width="30%"><?php echo $aMSG['form']['usergroup'][$syslang]; ?></th>
		<th width="60%"><?php echo $aMSG['form']['username'][$syslang]; ?></th>
		<th width="10%">&nbsp;</th>
	</tr>
	
<?php
// DB-action: select
	$aUG = $oCug->getAllUsergroups(false); // params: [$bTitleOnly=true]
	$entries = count($aUG);	// noetig fuer DB-Navi und PRIO!
	foreach ($aUG as $ug_id => $aData) {
		// vars
		$sGEToptions = "?id=".$ug_id;
		
		// Subquery: User dieser UG ermitteln
		$aCugUser = $oCug->getUserByCug($ug_id);
		$aUser = array(); // init
		foreach ($aCugUser as $u_id => $tmp) {
			// Userdaten aufbereiten + in Array sammeln
			$sUserGEToptions = "?type=".$tmp['type']."&id=".$tmp['contact_id'];
			$aUser[] = '<a href="'.$sUserEditPage.$sUserGEToptions.'">'.$tmp['firstname'].' '.$tmp['surname'].'</a>'; #
		}
?>
	<tr><td colspan="3" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td height="20"><p><?php // title(-link)
			echo view_link($aData['title'], $sEditPage.$sGEToptions); // params: $sText[,$sHref=''][,$nStatus=0][,$nLength=80][,$bFallback=true]
		?><br></p></td>
		<td><p><?php echo implode(', ', $aUser); ?>&nbsp;</p></td>
		<td class="sub2" align="right" nowrap><p>
			<a href="JavaScript:openContactWin('<?php echo $aENV['SELF_PATH']; ?>popup_selectcontact.php?cug=<?php echo $ug_id; ?>')" title="<?php echo $aMSG['btn']['add_contact'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_contact_new.gif" alt="<?php echo $aMSG['btn']['add_contact'][$syslang]; ?>" class="btn"></a>
			<a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['std']['edit'][$syslang]; ?>" class="btn"></a>
		</p></td>
	</tr>
	<tr>
		<td colspan="3" class="sub2"><small>
		<?php // subtitle
			echo nl2br_cms($aData['description']);
		?></small></td>
	</tr>
	
<?php
	}  // END while and 'no-data'-string
	if ($entries == 0) { echo '	<tr><td colspan="3" align="center"><span class="text">'.$aMSG['std']['no_entries'][$syslang].'</span></td></tr>'; }
?>
</table>
<form>
<br>

<?php	require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>