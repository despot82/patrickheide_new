<?php
/**
* adr_detail.php
*
* Detailpage: addresses 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	int		$type		[company|contact] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author 	Nils hitze <nh@design-aspekt.com>
* @version	1.0 / 2004-02-18
* @version  1.1 / 2006-12-12
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : '';
	$sGEToptions	= "?start=".$start;
	
// 2b. POST-params abholen

// 2c. Vars:
	$sTable			= "adr_contact";
	$sViewerPage	= $aENV['SELF_PATH']."adr.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;				// fuer NEW-button

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// 3. DB
// DB-action: 1. get data for country names
	$aCountry = _get_all_countries($oDb); // von "inc.adr_functions.php"
	
// DB-action: 2. get data CONTENT
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		// set type (fallback)
		$type = (!isset($aData['company']) || empty($aData['company'])) ? "contact" : "company";
		// ggf. edit-/delete-rechte reduzieren (wenn der akt. nutzer nicht der eigentuemer ist)
		if (($oPerm->hasPriv('delete') || $oPerm->hasPriv('edit')) && !$oPerm->hasPriv('admin') && $Userdata['id'] != $aData['created_by']) {
			if ($oPerm->hasPriv('edit')) $oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
			if ($oPerm->hasPriv('delete')) $oPerm->removePriv('delete'); // params: $sPrivilege[,$sModule='']
		}
	} else {
		header("Location:$sViewerPage");
		exit();
//		$mode_key = "new"; // do not change!
	}
	
// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>

<form action="<?php echo $aENV['PHP_SELF']; ?>" method="get" name="jumpForm">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="middle">
		<td><span class="title"><?php
		if ($type == "contact") {
		// name / title
			echo '<b>'.$aData['title'].' '.$aData['firstname'].' '.$aData['surname'].'</b>'; #echo $aData['gender'];
		} else {
		// companyname
			echo '<b>'.nl2br_cms($aData['company']).'</b>';
		} ?></span>
		</td>
		
		<td align="right"><span class="text">
		<?php if ($oPerm->hasPriv('edit')) {	// EDIT-BUTTON
			$sEditTxt = $aMSG['address']['edit'][$syslang].' '.$aMSG['address'][$type][$syslang]; ?>
			<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>" title="<?php echo $sEditTxt; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $sEditTxt; ?>" class="btn"></a>
		<?php } // END if ?>&nbsp;
		<?php if ($oPerm->hasPriv('create')) {	// NEW-BUTTONS ?>
			<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?type=contact" title="<?php echo $aMSG['btn']['new_contact'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_contact_new.gif" alt="<?php echo $aMSG['btn']['new_contact'][$syslang]; ?>" class="btn"></a><a
			href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?type=company" title="<?php echo $aMSG['btn']['new_company'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_company_new.gif" alt="<?php echo $aMSG['btn']['new_company'][$syslang]; ?>" class="btn"></a>
		<?php } // END if ?>
		</span></td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="tabelle">
<?php if (	(isset($aData['grp']) && !empty($aData['grp']))
		||	(isset($aData['birthday']) && !empty($aData['birthday']) && $aData['birthday'] != '0000-00-00')
		||	(isset($aData['client_nr']) && !empty($aData['client_nr']))	) { ?>
	<tr valign="top">
		<th colspan="2">
		<?php if (isset($aData['grp']) && !empty($aData['grp'])) {
				echo '<b>'.$aMSG['form']['group'][$syslang].':</b> '; // group headline
				$oDb->query("SELECT title_".$syslang." FROM adr_group WHERE id='".$aData['grp']."'");
				if ($oDb->num_rows()) echo ''.$oDb->fetch_field("title_".$syslang);
			} ?>
		<?php if (isset($aData['birthday']) && !empty($aData['birthday']) && $aData['birthday'] != '0000-00-00') {
			echo '&nbsp;/&nbsp;&nbsp;<b>'.$aMSG['form']['birthday'][$syslang].':</b> '; // birthday headline
			echo $oDate->date_mysql2trad($aData['birthday']);
			} ?>
		<?php  if (isset($aData['client_nr']) && !empty($aData['client_nr']) && $aData['client_nr'] != '000') {
			echo '&nbsp;/&nbsp;&nbsp;<b>'.$aMSG['form']['client_nr'][$syslang].':</b> '; // client_nr headline
			echo ''.$aData['client_nr'].'';
			} ?>
		</th>
	</tr>
<?php } // END if ?>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="4" class="tabelle">
	<tr><td colspan="3" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
<?php // ANSICHT "CONTACT" #############################################################################
if ($type == "contact") { ?>
	<tr valign="middle">
		<th width="34%"><?php echo $aMSG['address']['p'][$syslang]; ?></th>
		<th width="33%"><?php echo $aMSG['address']['o1'][$syslang]; ?></th>
		<th width="33%"><?php echo $aMSG['address']['o2'][$syslang]; ?></th>
	</tr>
	<tr valign="top">
		<td><span class="text">
<?php // PRIVAT
		$sql	= "SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='p'";
		$oDb->query($sql);
		$aData2	= $oDb->fetch_array();
		
		// name
		if (isset($aData2['recipient']) && !empty($aData2['recipient'])) {
			echo '<b>'.$aData2['recipient']."</b><br>\n";
		} elseif (!empty($aData2['id'])) {
			echo '<b>'.$aData['title'].' '.$aData['firstname'].' '.$aData['surname']."</b><br>\n"; #echo $aData['gender'];
		}
		
		// address
		if (!empty($aData2['street'])) echo $aData2['street']."<br>";
		if (!empty($aData2['building'])) echo $aData2['building']."<br>";
		if (!empty($aData2['region'])) echo $aData2['region']."<br>";
		if (!empty($aData2['town'])) echo $aData2['town']."<br>";
		if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>";
		echo "<br>\n";
		
		// po-box
		if (!empty($aData2['po_box'])) {
			echo $aData2['po_box']."<br>";
			echo $aData2['po_box_town']."<br>";
			if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>";
			echo "<br>\n";
		}
		
		// phone
		if (!empty($aData2['phone_tel'])) {
			echo "<b>T</b> ".format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."<br>\n";
		}
		if (!empty($aData2['phone_mob'])) {
			echo "<b>M</b> ".format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."<br>\n";
		}
		if (!empty($aData2['phone_fax'])) {
			echo "<b>F</b> ".format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."<br>\n";
		}
		
		// email
		formatEmailString($aData, $aData2, $bEmailStringShort);		

		
		// www
		if (!empty($aData2['web'])) echo '<a href="'.link_uri($aData2['web']).'" target="_blank">'.link_uri($aData2['web'])."</a><br>\n";
		// END PRIVAT ?>
		&nbsp;</span></td>
		<td><span class="text">
		
<?php // 1. OFFICE
		// personal company-data
		$sql	= "SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='o1'";
		$oDb->query($sql);
		if ($oDb->num_rows()) {
			$aData3 = $oDb->fetch_array();
			
			// name
			if (!empty($aData3['id'])) {
				echo '<b>'.$aData['title'].' '.$aData['firstname'].' '.$aData['surname']."</b><br>\n"; #echo $aData['gender'];
			}
			
			// firmenname (+client-id) mit link zu seiner detailseite
			$sql	= "SELECT company,client_nr FROM ".$sTable." WHERE id=".$aData3['company_id'];
			$oDb->query($sql);
			$tmp = $oDb->fetch_array();
			echo '<b><a href="'.$aENV['PHP_SELF']."?id=".$aData3['company_id']."&start=".$start.'&type=company">'.nl2br_cms($tmp['company'])."</a></b><br>\n";
			
			$sql	= "SELECT * FROM adr_address WHERE reference_id=".$aData3['company_id']." AND cat='c'";
			// global comany-data
			$oDb->query($sql);
			$aData2 = $oDb->fetch_array();
			
			// address
			if (!empty($aData2['street'])) echo $aData2['street']."<br>";
			if (!empty($aData2['building'])) echo $aData2['building']."<br>";
			if (!empty($aData2['region'])) echo $aData2['region']."<br>";
			if (!empty($aData2['town'])) echo $aData2['town']."<br>";
			if (!empty($aData2['street']) && !empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>";
			
			if (!empty($aData2['town']) && !empty($aData2['po_box'])){ echo "<br>";}
			
			// po-box
			if (!empty($aData2['po_box'])) {
				echo $aData2['po_box']."<br>";
				echo $aData2['po_box_town']."<br>";
				if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>";
			}
			echo "<br>\n";
			
			// phone (tel)
			if (!empty($aData3['phone_tel'])) { // personal
				echo "<b>T</b> ".format_phone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			} elseif (!empty($aData2['phone_tel'])) { // global
				echo "<b>T</b> ".format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			// phone2 (tel (mobile))
			if (!empty($aData2['phone_mob'])) { // global (company T2)
				echo "<b>T</b> ".format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			// phone (mobile)
			if (!empty($aData3['phone_mob'])) { // personal (office Mob)
				echo "<b>M</b> ".format_phone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			// phone (fax)
			if (!empty($aData3['phone_fax'])) { // personal
				echo "<b>F</b> ".format_phone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			} elseif (!empty($aData2['phone_fax'])) { // global
				echo "<b>F</b> ".format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			// email
			if (!empty($aData3['email'])) { // personal
				formatEmailString($aData, $aData3, $bEmailStringShort);
			} elseif (!empty($aData2['email'])) { // global
				formatEmailString($aData, $aData2, $bEmailStringShort);
			}
			// www
			if (!empty($aData3['web'])) { // personal
				echo '<br><a href="'.link_uri($aData3['web']).'" target="_blank">'.link_uri($aData3['web'])."</a><br>\n";
			} elseif (!empty($aData2['web'])) { // global
				echo '<br><a href="'.link_uri($aData2['web']).'" target="_blank">'.link_uri($aData2['web'])."</a><br>\n";
			}
			echo "<br>";
			// department
			if (!empty($aData3['company_dep'])) echo $aMSG['form']['department'][$syslang].': '.$aData3['company_dep']."<br>\n";
			// position
			if (!empty($aData3['company_pos'])) echo $aMSG['form']['position'][$syslang].': '.$aData3['company_pos']."<br>\n";
			// client_nr
			echo (!empty($tmp['client_nr']) && $tmp['client_nr'] != '000') ? $aMSG['form']['client_nr'][$syslang].': '.$tmp['client_nr']."<br>\n" : "<br>\n";
			
		} // END 1. OFFICE ?>
		&nbsp;</span></td>
		<td><span class="text">
<?php // 2. OFFICE
		// personal comany-data
		$sql	= "SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='o2'";
		$oDb->query($sql);
		if ($oDb->num_rows()) {
			$aData3 = $oDb->fetch_array();
			// name
			if (!empty($aData3['id'])) {
				echo '<b>'.$aData['title'].' '.$aData['firstname'].' '.$aData['surname']."</b><br>\n"; #echo $aData['gender'];
			}
			// firmenname (+client-id) mit link zu seiner detailseite
			$sql	= "SELECT company,client_nr FROM ".$sTable." WHERE id=".$aData3['company_id'];
			$oDb->query($sql);
			$tmp = $oDb->fetch_array();
			echo '<b><a href="'.$aENV['PHP_SELF']."?id=".$aData3['company_id']."&start=".$start.'&type=company">'.nl2br_cms($tmp['company'])."</a></b><br>\n";
			
			// global comany-data
			$sql	= "SELECT * FROM adr_address WHERE reference_id=".$aData3['company_id']." AND cat='c'";
			$oDb->query($sql);
			$aData2 = $oDb->fetch_array();
			// address
			if (!empty($aData2['street']))		echo $aData2['street']."<br>";
			if (!empty($aData2['building']))	echo $aData2['building']."<br>";
			if (!empty($aData2['region']))		echo $aData2['region']."<br>";
			if (!empty($aData2['town']))		echo $aData2['town']."<br>";
			if (!empty($aData2['country']))		echo $aCountry[$aData2['country']]['name']."<br>";
			if (!empty($aData2['town']) 
			&& !empty($aData2['po_box'])) 		echo "<br>";
			
			// po-box
			if (!empty($aData2['po_box'])) {
				echo $aData2['po_box']."<br>";
				echo $aData2['po_box_town']."<br>";
				if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>";
			}
			echo "<br>\n";
			
			// phone (tel)
			if (!empty($aData3['phone_tel'])) { // personal
				echo "<b>T</b> ".format_phone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			} elseif (!empty($aData2['phone_tel'])) { // global
				echo "<b>T</b> ".format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			// phone2 (tel (mobile))
			if (!empty($aData2['phone_mob'])) { // global (company T2)
				echo "<b>T</b> ".format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			// phone (mobile)
			if (!empty($aData3['phone_mob'])) { // personal (office Mob)
				echo "<b>M</b> ".format_phone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			// phone (fax)
			if (!empty($aData3['phone_fax'])) { // personal
				echo "<b>F</b> ".format_phone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			} elseif (!empty($aData2['phone_fax'])) { // global
				echo "<b>F</b> ".format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			}
			
			// email
			if (!empty($aData3['email'])) { // personal
				formatEmailString($aData, $aData3, $bEmailStringShort);
			} elseif (!empty($aData2['email'])) { // global
				formatEmailString($aData, $aData2, $bEmailStringShort);			
			}
			
			// www
			if (!empty($aData3['web'])) { // personal
				echo '<br><a href="'.link_uri($aData3['web']).'" target="_blank">'.link_uri($aData3['web'])."</a><br>\n";
			} elseif (!empty($aData2['web'])) { // global
				echo '<br><a href="'.link_uri($aData3['web']).'" target="_blank">'.link_uri($aData3['web'])."</a><br>\n";
			}
			echo "<br>";
			// department
			if (!empty($aData3['company_dep'])) echo $aMSG['form']['department'][$syslang].': '.$aData3['company_dep']."<br>\n";
			// position
			if (!empty($aData3['company_pos'])) echo $aMSG['form']['position'][$syslang].': '.$aData3['company_pos']."<br>\n";
			// client_nr
			echo (!empty($tmp['client_nr']) && $tmp['client_nr'] != '000') ? $aMSG['form']['client_nr'][$syslang].': '.$tmp['client_nr']."<br>\n" : "<br>\n";
			
		} // END 2. OFFICE ?>
		&nbsp;</span></td>
	</tr>
<?php // ANSICHT "COMPANY" #############################################################################
} else { ?>
	<tr valign="middle">
		<th width="34%"><?php echo $aMSG['address']['c'][$syslang]; ?></th>
		<th width="66%" colspan="2"><?php echo $aMSG['address']['s'][$syslang]; ?></th>
	</tr>
	<tr valign="top">
		<td><span class="text">
<?php // OFFICE
		$sql	= "SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='c'";
		$oDb->query($sql);
		$aData2 = $oDb->fetch_array();
		
		// company-name
		echo '<b>'.nl2br_cms($aData['company'])."</b><br>\n";
		
		// address
		if (!empty($aData2['street'])) echo $aData2['street']."<br>";
		if (!empty($aData2['building'])) echo $aData2['building']."<br>";
		if (!empty($aData2['region'])) echo $aData2['region']."<br>";
		if (!empty($aData2['town'])) echo $aData2['town']."<br>";
		if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>";
		
		if (!empty($aData2['town']) 
		&& !empty($aData2['po_box']))	echo "<br>";
			
		// po-box
		if (!empty($aData2['po_box'])) {
			echo $aData2['po_box']."<br>";
			echo $aData2['po_box_town']."<br>";
			if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>";
		}
		
		echo "<br>\n";
		
		// phone
		if (!empty($aData2['phone_tel'])) {
			echo "<b>T</b> ".format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."<br>\n";
		}
		if (!empty($aData2['phone_mob'])) {
			echo "<b>T</b> ".format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."<br>\n";
		}
		if (!empty($aData2['phone_fax'])) {
			echo "<b>F</b> ".format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."<br>\n";
		}
		// email
		if (!empty($aData2['email'])) {
			formatEmailString($aData, $aData2, $bEmailStringShort);
		}
		echo "<br>\n";
		// www
		if (!empty($aData2['web'])) echo '<a href="'.link_uri($aData2['web']).'" target="_blank">'.link_uri($aData2['web'])."</a><br>\n";
		// END OFFICE ?>
		&nbsp;</span></td>
		<td colspan="2"><span class="text">
<?php // STAFF
		// groups zwischenspeichern
		$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
		
		// stamm-daten + personal office-daten ausgeben
		$aData3 = array();
		$sql	= "	SELECT	a.reference_id,
						 		a.cat,
								a.country,
								a.phone_tel,
								a.phone_mob,
								a.phone_fax,
								a.email,
								a.web,
								a.company_dep,
								a.company_pos,
								c.id,
								c.firstname,
								c.surname,
								c.grp
						FROM	adr_address a, 
								adr_contact c 
						WHERE	a.company_id='".$id."' 
						AND		a.reference_id=c.id	
						ORDER BY c.grp, c.surname, c.firstname";
		$oDb->query($sql);
		while ($aData3 = $oDb->fetch_array()) {
			echo '<table width="100%" border="0" cellspacing="0" cellpadding="2"><tr valign="top"><td width="40%"><span class="text">';
			// name (verlinkt) + group/department/position
			echo '<b><a href="'.$aENV['PHP_SELF']."?id=".$aData3['id']."&start=".$start.'&type=contact">';
			if (empty($aData3['surname'])) {
				$name = $aData3['firstname'];
			} else if (empty($aData3['firstname'])) {
				$name = $aData3['surname'];
			} else {
				$name = $aData3['surname'].', '.$aData3['firstname'];
			}
			echo $name;
			echo "</a></b><br>\n";
			if (!empty($aData3['company_dep'])) echo $aData3['company_dep']."<br>\n";
			if (!empty($aData3['company_pos'])) echo $aData3['company_pos']."<br>\n";
			echo '<b>'.$aMSG['form']['group'][$syslang].':</b> '.$aGroup[$aData3['grp']].'<br>';
			echo '</span</td><td width="60%"><span class="text">';
			// phone/email/web
			if (!empty($aData3['phone_tel'])) echo "<b>T </b>".format_phone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone'])."&nbsp;\n";
			if (!empty($aData3['phone_mob'])) echo "<b>M </b>".format_phone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone'])."\n";
			if (!empty($aData3['phone_fax'])) echo "<br><b>F </b>".format_phone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone'])."\n";
			
			if (!empty($aData3['email'])) {
				formatEmailString($aData, $aData3, $bEmailStringShort);
//				$sEmail	= '';
//				$sEmail	.= !empty($aData3['firstname'])	? $aData3['firstname']:'';
//				$sEmail	.= !empty($aData3['surname'])	? " ".$aData3['surname']:'';
//				$sEmail	= !empty($sEmail)	? $sEmail.' <'.$aData3['email'].'>':$aData3['email'];				
//				echo '<br><b>E </b> <a href="'.link_uri($sEmail).'">'.show_uri($aData3['email'])."</a>\n";
			}
			if (!empty($aData3['web'])) echo ' <br><a href="'.link_uri($aData3['web']).'" target="_blank">'.link_uri($aData3['web'])."</a><br>\n";
			echo '</span></td></tr></table>'.HR."\n";
		}
		// END STAFF ?>
		&nbsp;</span></td>
	</tr>
<?php
} // END ANSICHT ############################################################################# ?>

<?php // CUG-Data? -------------------------------------------------------
	$oDb->query("SELECT id, contact_id, login FROM cug_user WHERE contact_id='".$id."'");
	if ($oDb->num_rows() > 0) {
		$aData2 = $oDb->fetch_array(); ?>
	<tr><td colspan="3" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="middle">
		<th><?php echo $aMSG['address']['description'][$syslang]; ?></th>
		<th><?php echo $aMSG['address']['comment'][$syslang]; ?></th>
		<th><?php echo $aMSG['form']['cugusergroup'][$syslang]; ?></th>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php // description
		echo nl2br_cms($aData['description']);
		?>&nbsp;</span></td>
		<td><span class="text"><?php // comment
		echo nl2br_cms($aData['comment']);
		?>&nbsp;</span></td>
		<td><span class="text"><?php // CUGs
		require_once($aENV['path']['global_service']['unix']."class.cug.php");
		$oCug =& new cug($aENV['db'], $syslang);
		$aUg = $oCug->getUsergroupByUid($aData2['id']); // params: $sUserId
		foreach ($aUg as $ug_id => $ug_title) {
			echo ''.$ug_title.'<br>';
		}
		?>&nbsp;</span></td>
	</tr>	
<?php } else { ?>
	<tr><td colspan="3" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="middle">
		<th><?php echo $aMSG['address']['description'][$syslang]; ?></th>
		<th colspan="2"><?php echo $aMSG['address']['comment'][$syslang]; ?></th>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php // description
		echo nl2br_cms($aData['description']);
		?>&nbsp;</span></td>
		<td colspan="2"><span class="text"><?php // comment
		echo nl2br_cms($aData['comment']);
		?>&nbsp;</span></td>
	</tr>
<?php } // END CUG ------------------------------------------------------- ?>

</table>
</form>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>