<?php
/**
* adr_edit_s.php
*
* Detailpage: zum anzeigen der Details der Kontakte einer Company
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	int		$clearid	[id zum loeschen eines o1/o2-datensatzes] (optional)
* @param	string	$type		[company|contact] (optional -> default: company)
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-02-19
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_edit_s.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : 'company'; // company|contact
	$btn			= (isset($_GET['btn'])) ? $_GET['btn'] : array();
	$clearid		= (isset($_GET['clearid'])) ? $oFc->make_secure_int($_GET['clearid']) : '';
	$sGEToptions	= "?start=".$start."&type=".$type."&id=".$id; // id nur fuer subnavi
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_address";
	$sViewerPage	= $aENV['SELF_PATH']."adr_detail.php".$sGEToptions;	// fuer BACK-button
	$sReloadPage	= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// 3. DB
// DB-action: delete contact (remote)
	if (isset($btn['remoteDelete']) && $btn['remoteDelete'] > 0) {
		_delete_contact(&$oDb, $btn['remoteDelete']);
	}
// DB-action: mitarbeiter: office-zuordnung (o1-/o2-daten) loeschen
	if (!empty($clearid)) {
		// abhaengige daten loeschen:
		$aSub2delete['id'] = $clearid;
		$oDb->make_delete($aSub2delete, "adr_address"); // address-data
		// weiterleitung
		header("Location: ".$sReloadPage); exit; // fehler durch reload der seite vermeiden
	}
// DB-action: save (remote)
	if (isset($btn['remoteSave']) && $btn['remoteSave'] == 1) {
		// ... nichts zu saven hier, daher nur edit-mode beenden...
		// weiterleitung
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: 1. get data for country names
	$aCountry = _get_all_countries($oDb); // von "inc.adr_functions.php"

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>
<script language="JavaScript" type="text/javascript">
function confirmDelete() {
	var chk = window.confirm('<?php echo $aMSG['err']['delete_rec'][$syslang]; ?>');
	return(chk);
}
</script>
<form action="<?php echo $aENV['PHP_SELF']; ?>" method="get" name="editForm">
<input type="hidden" name="btn[remoteSave]" value="0">
<input type="hidden" name="btn[remoteDelete]" value="0">
<!-- GET-options -->
<input type="hidden" name="start" value="<?php echo $start; ?>">
<input type="hidden" name="type" value="<?php echo $type; ?>">
<input type="hidden" name="id" value="<?php echo $id; ?>">

<?php require_once ("inc.adr_detnavi.php"); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php // STAFF
		$oDb2 =& new dbconnect($aENV['db']);
		// groups zwischenspeichern
		$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
		
		// office-country zwischenspeichern
		$oDb->query("SELECT country FROM adr_address WHERE reference_id='".$id."'");
		if ($oDb->num_rows()) $country = $oDb->fetch_field("country");
		
		// stamm-daten + personal office-daten ausgeben
		$aData3 = array();
		$oDb->query("	SELECT	a.id,
								a.reference_id,
						 		a.cat,
								a.country,
								a.phone_tel,
								a.phone_mob,
								a.phone_fax,
								a.email,
								a.web,
								a.company_dep,
								a.company_pos,
								c.firstname,
								c.surname,
								c.grp
						FROM	adr_address a, 
								adr_contact c 
						WHERE	a.company_id='".$id."' 
						AND		a.reference_id=c.id	
						ORDER BY c.grp, c.surname, c.firstname");
		while ($aData3 = $oDb->fetch_array()) {
			// default-data ermitteln
			$oDb2->query("SELECT * FROM adr_contact WHERE id='".$aData3['reference_id']."'");
			$aData2 = $oDb2->fetch_array();
		// alles ausgeben
			echo '<tr valign="top">'."\n";
			// L: name (verlinkt) + group/department/position
			echo '<td width="40%"><span class="text">';
			echo '<b><a href="'.$aENV['SELF_PATH'].'adr_edit_o.php?id='.$aData3['reference_id'].'&type=contact&cat='.$aData3['cat'].'">';
			$name = (empty($aData2['surname'])) ? $aData2['firstname'] : $aData2['surname'].', '.$aData2['firstname'];
			$name = (empty($aData3['firstname'])) ? $aData3['surname'] : $aData3['surname'].', '.$aData3['firstname'];
			echo $name."</a></b><br>\n";
			
			if (!empty($aData3['company_dep'])) echo $aData3['company_dep']."<br>\n";
			if (!empty($aData3['company_pos'])) echo $aData3['company_pos']."<br>\n";
			echo '<b>Group:</b> '.$aGroup[$aData2['grp']]."\n";
			echo '</span</td>'."\n";
			// M: phone/email/web
			echo '<td width="45%"><span class="text">';
			if (!empty($aData3['phone_tel'])) echo "<b>T </b>".format_phone($aData3['phone_tel'], $aCountry[$country]['phone'])."&nbsp;\n";
			if (!empty($aData3['phone_mob'])) echo "<b>M </b>".format_phone($aData3['phone_mob'], $aCountry[$country]['phone'])."\n";
			if (!empty($aData3['email'])) {
				formatEmailString($aData, $aData3, $bEmailStringShort);
//				$sEmail	= !empty($aData2['firstname'])	? $aData2['firstname']:'';
//				$sEmail	.= !empty($aData2['surname'])	? ' '.$aData2['surname']:'';				
//				$sEmail	.= !empty($sEmail)	? ' <'.$aData3['email'].'>':$aData3['email'];
//				echo '<br><b>E</b> <a href="'.link_uri($sEmail).'">'.show_uri($aData3['email'])."</a><br>\n";
			}
			echo '</span</td>'."\n";
			// R: edit/clear button
			echo '<td width="15%" align="right"><p>';
			echo '<a href="'.$sReloadPage.'&clearid='.$aData3['id'].'" class="textbtn">'; // delete this office-data
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_contact_new.gif" alt="'.$aMSG['btn']['delete_connection'][$syslang].'">| ';
			echo $aMSG['btn']['delete_connection'][$syslang].'&nbsp;';
			echo '</a>';
			
			echo '</p></td>';
			echo '</tr><tr><td colspan="3" class="off"><img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"></td></tr>'."\n";
		}
		// END STAFF ?>
	<tr>
		<td colspan="3" align="right" class="off">
			<?php if ($oPerm->hasPriv('create')) { // NEW button ?>
			<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?type=contact&company1=<?php echo $id; ?>" title="<?php echo $aMSG['btn']['new_contact'][$syslang]; ?>" class="textbtn">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_contact_new.gif" alt="<?php echo $aMSG['btn']['new_contact'][$syslang]; ?>">| <?php echo $aMSG['btn']['new'][$syslang]; ?>&nbsp;</a>
			<?php } ?>
		</td>
	</tr>
</table>
</form>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>