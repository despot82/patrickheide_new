<?php
/**
* inc.adr_mailing_detnavi.php
*
* subnavi-include (nur INTRANET/address) //-> 2sprachig und voll kopierbar!
*
* @param	int		$id			welcher Datensatz
* @param	string	$type		welcher Typ [contact|company]
* @param	string	$cat		welche Kategorie [o1|o2]
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-02-19
*/
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.adr_mailing_detnavi.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
	<tr valign="top">
		<td><span class="cnavi">
			<?php if ($oPerm->hasPriv('edit')) { ?>
				<a href="<?php echo $aENV['SELF_PATH']; ?>adr_mailing_edit.php?mailinglist=<?php echo $mailinglist; ?>" class="cnavi<?php if($sBasename == "adr_mailing_edit") echo "hi"; ?>"><?php echo $aMSG['address']['settings'][$syslang] ?></a>
				<?php	if (isset($mailinglist)) { ?>&nbsp;&nbsp;|&nbsp;
				<a href="<?php echo $aENV['SELF_PATH']; ?>adr_mailing_list.php?mailinglist=<?php echo $mailinglist; ?>&listview=not_in&searchATerm=" class="cnavi<?php if($sBasename == "adr_mailing_list" && $listview != "in") echo "hi"; ?>"><?php echo $aMSG['topnavi']['addresses'][$syslang] ?></a>&nbsp;&nbsp;|&nbsp;
				<a href="<?php echo $aENV['SELF_PATH']; ?>adr_mailing_list.php?mailinglist=<?php echo $mailinglist; ?>&listview=in&searchATerm=" class="cnavi<?php if($listview == "in") echo "hi"; ?>"><?php echo $aMSG['address']['in_list'][$syslang] ?></a>
				<?php	} ?>
			<?php } // END if ?>
		</td>
		<td align="right">
			<a href="<?php echo $aENV['SELF_PATH']; ?>adr_print_labels.php?mailinglist=<?php echo $mailinglist; ?>" class="cnavi<?php if($sBasename == "adr_mailing_list_printview") echo "hi"; ?>" target="_blank" title="<?php echo $aMSG['btn']['printview'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_print.gif" alt="<?php echo $aMSG['btn']['printview'][$syslang]; ?>" class="btn"></a>
			<a href="<?php echo $aENV['SELF_PATH']; ?>adr_excel_list.php?mailinglist=<?php echo $mailinglist; ?>" class="cnavi<?php if($sBasename == "adr_excel_list") echo "hi"; ?>" title="<?php echo $aMSG['btn']['exceldwn'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_dwn.gif" alt="<?php echo $aMSG['btn']['exceldwn'][$syslang]; ?>" class="btn"></a>
		</td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="15" alt="" border="0"><br>