<?php
/**
* adr_mailing_printview.php
*
* Overviewpage: mailinglists 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start			[zum richtigen zurueckspringen] (optional)
* @param	string	$searchATerm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der session
* @param	string	$searchAType	[zum filtern der Anzeige um den Contact-type (contact|company)] (optional) -> kommt ggf. aus der session
* @param	string	$orderbyA		[zum ordnen der Anzeige ] (optional) -> kommt ggf. aus der session
* @param	int		$mailinglist	welche liste
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-07-05
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_mailing_printview.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");
	require_once ($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$mailinglist	= (isset($_GET['mailinglist'])) ? $oFc->make_secure_int($_GET['mailinglist']) : '';
	$listview		= (isset($_GET['listview']) && $_GET['listview'] == "in") ? "in" : "not_in";
	$sGEToptions	= "?mailinglist=".$mailinglist."&listview=".$listview;
	// ohne $mailinglist geht nix!
	#if ($mailinglist == '') { header("Location: ".$aENV['SELF_PATH']."adr.php"); }
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_mailing_contact";
	$sPrintPage		= $aENV['SELF_PATH']."adr_print_labels.php";	// fuer PRINT-link

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
	require_once ("./inc.print_functions.php");
// END SHARED *****************************************************************************

	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
?>

<form action="<?php echo $aENV['PHP_SELF']; ?>" method="get" name="jumpForm">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><span class="title"><?php echo $aMSG['address']['mailinglist'][$syslang] ?>: 
		<?php echo $sListName; ?> / <?php echo $aMSG['address']['printshortlist'][$syslang] ?><br></span></td>
		<td align="right"><input type="button" name="print" class="but" value="<?php echo $aMSG['btn']['print_page'][$syslang]; ?>" onclick="window.open('<?php echo $sPrintPage; ?>?mailinglist=<?php echo $mailinglist; ?>&template=<?php echo $sListTemplate; ?>&start=<?php echo $start; ?>')"></td>
	</tr>
</table>
<?php echo HR ?>
<?php require_once ("inc.adr_mailing_detnavi.php"); ?>

</form>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
		<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
		echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
		?></span></td>
		<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
		echo $oDBNavi->getDbnaviLinks($sGEToptions); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
		?></span></td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="10" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="6" class="tabelle">
<?php
	$oDb2 =& new dbconnect($aENV['db']); // fuer name / adresse / firmenname
	$i = 1; // counter (um die zellen pro reihe zu ermitteln)
	$empty_TDs	= $anz_zellen;
	while ($aData = $oDb->fetch_array()) {
		// init vars
		$sAdress	= '';
		$empty_rows	= 0;
		
		// TR oeffnen
		if (($i % $anz_zellen) == 1) { $sAdress .= '<tr valign="top">'."\n"; }
		// TD oeffnen
		$sAdress .= '<td width="'.$td_width.'"><span class="text">'."\n";
		
		// ermittle name dieser kontakt-person
		$name = _get_contact_name($oDb2, $aData['contact_id']);
		// ermittle adresse und ggf. firmenname
		$aData2 = _get_contact_address($oDb2, $aData['contact_id'], $aData['address']);
		// name/address layout
		
		if (!empty($aData2['recipient']))		{ $sAdress .= $aData2['recipient']."<br>"; 
		} else {
			if (!empty($name))					{ $sAdress .= $name."<br>"; }
		}
		if (!empty($aData2['company']))	{ $sAdress .= nl2br($aData2['company'])."<br>"; } else { $empty_rows++; }
		
		// po-box hat prioritaet!
		if (!empty($aData2['po_box']))	{
			$sAdress .= $aData2['po_box']."<br>";
			$sAdress .= $aData2['po_box_town']."<br>";
			$empty_rows += 2;
		} else {
			if (!empty($aData2['street']))	{ $sAdress .= $aData2['street']."<br>"; } else { $empty_rows++; }
			if (!empty($aData2['building'])){ $sAdress .= $aData2['building']."<br>"; } else { $empty_rows++; }
			if (!empty($aData2['region']))	{ $sAdress .= $aData2['region']."<br>"; } else { $empty_rows++; }
			if (!empty($aData2['town']))	{ $sAdress .= $aData2['town']."<br>"; } else { $empty_rows++; }
		}
		if (!empty($aData2['country']))	{ $sAdress .= $aCountry[$aData2['country']]['name']."<br>"; } else { $empty_rows++; } // "$aCountry" aus der "inc.print_functions.php"!
		#$sAdress .= str_repeat("<br>", $empty_rows); // nicht genutzte felder auffuellen
		
		// TD schliessen
		$sAdress .= "</span></td>\n";
		// counter fuer leere TDs hochzaehlen
		$empty_TDs = ($empty_TDs > 1) ? $empty_TDs - 1 : $anz_zellen;
		// ggf. fehlende TDs auffuellen
		if ($i == ($entries - $start) && ($i % $anz_zellen) != 0) {
			$sAdress .= str_repeat('<td width="'.$td_width.'"></td>'."\n", $empty_TDs);
			$i += $empty_TDs; // counter anpassen
		}
		// TR schliessen
		if (($i % $anz_zellen) == 0) { $sAdress .= "</tr>\n"; }
		
		// output
		echo $sAdress;
		
		// counter hochzaehlen
		$i++;
		
	} // END while
?>
</table>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
		<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
		echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
		?></span></td>
		<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
		echo $oDBNavi->getDbnaviLinks($sGEToptions); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
		?></span></td>
	</tr>
</table>

<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");

?>
