<?php
/**
* adr_indexsearch.php
*
* Specialpage: indiziert ALLE Adress-Datensaetze fuer die ADR-Suche
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-02-22
*/

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// insert/update ALL search-data (!)
	function adr_save_ALL_searchdata(&$oDb) {
		global $aENV;
		$oDb2 =& new dbconnect($aENV['db']); // fuer subqueries
		$error = array();
		$i = 0;
		$oDb->query("SELECT id, company, firstname, surname FROM adr_contact ORDER BY id");
		while ($aData = $oDb->fetch_array()) {
			// datensatz indizieren
			$done = adr_save_searchdata($oDb2, $aData['id']);
			
			// name
			$name = (!isset($aData['company']) || empty($aData['company'])) 
				? $aData['firstname'].' '.$aData['surname']
				: $aData['company'];
			// error speichern
			if (!$done) $error[] = $aData['id'].': '.$name;
			// ausgabe
			echo ($done) 
				? $aData['id'].' '.$name 
				: '--';
			echo "<br>\n";
			$i++;
		}
		// ergebnis
		echo "<br>\n";
		echo "<b>".$i." Datensaetze indiziert!</b>";
		echo "<br>\n";
		// error?
		if (count($error) > 0) {
			echo '<span class="red">ERROR:'."<br>\n";
			print_r($error);
			echo '</span>'."<br>\n";
		}
	}
?>

<span class="title">ADR-Search komplett neuindizieren</span>
<br><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top" height="20">
		<td><span class="text"><?php 
		// nur fuer design aspekt service zugang
		if ($oPerm->isDaService()) {
			#$oDb->debug = true;
			adr_save_ALL_searchdata($oDb);
		} else {
			echo "Bitte zum kompletten Neu-Indizieren der ADR-Daten den DA-Service-Zugang verwenden!";
		} ?></span></td>
	</tr>
</table>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
