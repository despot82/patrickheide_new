<?php
/**
* adr_mailing_list.php
*
* Overviewpage: mailinglists 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start			[zum richtigen zurueckspringen] (optional)
* @param	string	$searchATerm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der session
* @param	string	$searchAType	[zum filtern der Anzeige um den Contact-type (contact|company)] (optional) -> kommt ggf. aus der session
* @param	int		$mailinglist	welche liste
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-06-24
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_mailing_list.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");
	require_once ($aENV['path']['global_service']['unix']."class.Search.php");
	require_once ($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	if (isset($_GET['searchATerm']))	{ $searchATerm = $oFc->make_secure_string($_GET['searchATerm']); }  // (kein default wegen session!)
	if (isset($_GET['searchAType']))	{ $searchAType = $oFc->make_secure_string($_GET['searchAType']); } // (kein default wegen session!)
	$mailinglist	= (isset($_GET['mailinglist'])) ? $oFc->make_secure_int($_GET['mailinglist']) : '';
	$listview		= (isset($_GET['listview']) && $_GET['listview'] == "in") ? "in" : "";
	$scrolly		= (isset($_GET['scrolly'])) ? $_GET['scrolly'] : 0;
	$sGEToptions	= "?mailinglist=".$mailinglist."&listview=".$listview;

	// ohne $mailinglist geht nix!
	if ($mailinglist == '') { header("Location: ".$aENV['SELF_PATH']."adr.php"); }

// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_contact";
	$sViewerPage	= $aENV['SELF_PATH']."adr_detail.php";	// fuer DETAIL-link
	$sEditPage		= $aENV['SELF_PATH']."adr_edit_d.php";	// fuer EDIT-link
// 2d. DB-action: select
	// NAME dieser Mailinglist (string $sListName)
	$sql	= "SELECT listname FROM adr_mailing_name WHERE id='".$mailinglist."'";
	$oDb->query($sql);
	if ($oDb->num_rows()) { $sListName = $oDb->fetch_field('listname'); }

// 3. DB-action: store //////////////////////////////////////////////////////////////////////////
	if (isset($_GET['db_action'])) {
		// benoetigte GET-params abholen
		$contact_id	= $oFc->make_secure_int($_GET['contact_id']);
		$address	= $oFc->make_secure_string($_GET['address']);

		if (empty($contact_id) || empty($address)) { // fallback
			echo js_alert("ERROR: NO contact_id OR address!!! _GET['db_action']: ".$_GET['db_action']);
		}
		
		switch ($_GET['db_action']) {
			// db-actions
			case "insert":
				$aData['list_id']		= $mailinglist;
				$aData['contact_id']	= $contact_id;
				$aData['address']		= $address;
				// set timestamp + userid
				$aData['created']		= $oDate->get_mysql_timestamp();
				$aData['created_by']	= $Userdata['id'];
				// make insert
				$oDb->make_insert($aData, "adr_mailing_contact");
				break;
	
			case "delete":
				$aData2delete['contact_id']	= $contact_id;
				$aData2delete['address']	= $address;
				// make delete
				$oDb->make_delete($aData2delete, "adr_mailing_contact");
				break;
		}
		
		// db-errors vermeiden durch reload ohne db-params
		header("Location: ".$aENV['PHP_SELF'].$sGEToptions."&start=".$start."&scrolly=".$scrolly);
		exit;
		
	} //////////////////////////////////////////////////////////////////////////

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
	require_once ("./inc.print_functions.php");
// END SHARED *****************************************************************************

// 5. DB-action: select
/*
	// GROUPS (array $aGroup)
	$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
	
	// EINTRAEGE in dieser mailingliste
	$aInMListID = array();		// (flaches array nur mit IDs)
	$aInMailingList = array();	// (assioziatives array key:ID, val:address)
	$oDb->query("SELECT contact_id, address FROM adr_mailing_contact WHERE list_id='".$mailinglist."' ORDER BY id ASC");
	$MailEntries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
	while ($tmp = $oDb->fetch_array()) {
		$aInMListID[] = $tmp['contact_id']; // fuer SQL
		$aInMailinglist[$tmp['contact_id']][] = $tmp['address']; // fuer Checkboxen
	}
	if ($listview == "in") {
		$dbConstraint = (count($aInMListID)) ? " id IN (".implode(',', $aInMListID).") ": ' id=0'; // falback fuer leeres array
	} else {
		$dbConstraint = '';
	}
	
	// CONTENT
	
	// set options
	$orderbyA = "searchtext ASC";
	
	$limit = 10;	// Datensaetze pro Ausgabeseite einstellen
	$fulltextsearch = false;
	// db-search-vars
	$searchfields				= "searchtext";
	$oDb->soptions['select']	= "id,company,client_nr,client_shortname,firstname,surname,grp,comment,description";
	$oDb->soptions['extra']		= $dbConstraint; // ggf. auf listview=in einschraenken!
	// extra constraint -> nur companies / personen
	#if ($searchAType == 'company') { $oDb->soptions['extra'] = " WHERE company<>'' "; } // wenn type auf company eingeschraenkt wurde
	#if ($searchAType == 'contact') { $oDb->soptions['extra'] = " WHERE (company='' OR company IS NULL) "; } // wenn type auf contact eingeschraenkt wurde
	
	###if ($oPerm->isDaService()) {$oDb->debug = true;} // DEBUG
	
	// ggf. SEARCH
	if (!empty($searchATerm)) {
		// search-options
		$oDb->soptions['search_umlauts']	= true;
		$oDb->blacklist = array("nazi", "scheiße", "ein", "eine", "der", "die", "das");
		#$oDb->soptions['ignore_case']		= true; #(std)
		#$oDb->soptions['keep_wordgroups']	= true; #(std)
		#$oDb->soptions['goperator']		= "AND"; #(std)
		// DEBUG
		#$oDb->soptions['passive_mode']		= true;
		#$oDb->debug = true;
		
		// 1: fulltext-search
		$oDb->fulltext_search($searchfields, $searchATerm, $sTable, $orderbyA);
		$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
		if ($entries > 0) {
			if (empty($_GET['orderbyA'])) { $orderbyA = ""; } // bei fulltextsearch-treffer + KEINEM GET -> sortierung nach relevanz!
			$oDb->fulltext_search($searchfields, $searchATerm, $sTable, $orderbyA, $limit, $start);
			$fulltextsearch = true;
		}
		// 2: normal-search, if NO search-result with fulltext-search
		if ($entries < 1) {
			$orderby = (empty($orderbyA)) ? $aENV['table']['sys_global_search'].".searchtext_mod ASC" : $orderbyA;
			$oDb->make_search($searchfields, $searchATerm, $sTable, $orderby);
			$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
			if ($entries > $limit) { $oDb->make_search($searchfields, $searchATerm, $sTable, $orderby, $limit, $start); }
		}
	}

	*/
	// 4. DB-action: select
	// GROUPS (store in array)
	$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
	$aData	= array();
	
// INIT Search Class
	$oSearch	=& new Search($oDb, $aENV, 'adr', $Userdata['id']);
	
	// EINTRAEGE in dieser mailingliste
	$aInMListID		= array();		// (flaches array nur mit IDs)
	$aInMailingList	= array();	// (assioziatives array key:ID, val:address)

	$sql	= "SELECT contact_id, address FROM adr_mailing_contact WHERE list_id='".$mailinglist."' ORDER BY id ASC";
	$oDb->query($sql);

	$MailEntries	= floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
	while ($tmp = $oDb->fetch_array()) {
		$aInMListID[] = $tmp['contact_id']; // fuer SQL
		$aInMailinglist[$tmp['contact_id']][] = $tmp['address']; // fuer Checkboxen
	}
	if ($listview == "in") {
		$dbConstraint = (count($aInMListID)) ? " adr_contact.id IN (".implode(',', $aInMListID).") ": ' adr_contact.id=0'; // fallback fuer leeres array
	} else {
		$dbConstraint = '';
	}
	
	// CONTENT
	$limit = 20;	// Datensaetze pro Ausgabeseite einstellen
	if (empty($start)) {$start=0;}
	$fulltextsearch	= false;

	// db-search-vars
	$searchfields				= $aENV['table']['sys_global_search'].".searchtext_mod";
	$oDb->soptions['select']	= "adr_contact.id,adr_contact.company,adr_contact.client_nr,adr_contact.client_shortname,adr_contact.title, adr_contact.firstname,adr_contact.surname,adr_contact.grp,adr_contact.comment,adr_contact.description";
	$oDb->soptions['extra']		= $dbConstraint;

	// ggf. SEARCH
	if (!empty($searchATerm)) {		
		// 2006-04-13af: FIX fuer einfache Anfuehrungszeichen im suchbegriff:
		$searchATerm	= str_replace("'", "\'", $searchATerm);
		$sSearch		= $oSearch->modifySearchString($searchATerm);
		$oSearch->oDb->soptions['extra']	= "ref_table='adr_contact'";
		$oSearch->select($sSearch, $start, $limit, $sOrder, true, false);
		
		// 1: fulltext-search
		if ($oSearch->getNumfulltext() > 0) {
			$aData	= $oSearch->getSearchResult();
			$fulltextsearch = true;
		} else { // else
			$oSearch->select($sSearch, $start, $limit, 'searchtext_mod ASC', false, true);
			$aData	= $oSearch->getSearchResult();
		} // end else

		for($i=0; is_array($aData[$i]); $i++) {
			$sql	= "SELECT id,company,client_nr,client_shortname,title,firstname,surname,grp,comment,description FROM adr_contact WHERE id='".$aData[$i]['id']."'";

			$oDb->query($sql);
			$row	= $oDb->fetch_array();
			
			foreach($row as $field => $value) {
				$aData[$i][$field]	= $value;
			} // end foreach
			
		} // end for
		$entries	= $oSearch->getEntries();

	} else { // else if searchATerm is empty
	 
		$orderby	= (empty($orderbyA)) ? $aENV['table']['sys_global_search'].".searchtext_mod ASC" : $orderbyA;
		// '.$oDb->soptions['extra'].'
		$sql	= 'SELECT '.$oDb->soptions['select'].' FROM adr_contact INNER JOIN '.$aENV['table']['sys_global_search'].' ON adr_contact.id='.$aENV['table']['sys_global_search'].'.ref_id WHERE '.$aENV['table']['sys_global_search'].'.module=\'adr\'';
		
		if(!empty($oDb->soptions['extra'])) $sql .= ' AND '.$oDb->soptions['extra'];
		
		$sql	.= ' ORDER BY '.$orderby;
		$oDb->query($sql);
		$entries	= floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
		
		if ($entries > $limit) { $oDb->limit($sql, $start, $limit); }
		
		for($i=0;$aData[$i] = $oDb->fetch_array();$i++);
		$last	= count($aData);
		unset($aData[($last-1)]);
	}
	
	if($listview == 'in') {
		$entries	= $MailEntries;
	}
	
	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
?>

<script language="JavaScript" type="text/javascript">
function changeList(contact_id, address, db_action, start, searchATerm) {
	// scroll-position beim speichern merken
	document.jumpForm.scrolly.value = (document.all) ? document.body.scrollTop : window.pageYOffset;
	// restliche vars beim speichern merken
	document.jumpForm.contact_id.value = contact_id;
	document.jumpForm.address.value = address;
	document.jumpForm.db_action.value = db_action;
	document.jumpForm.start.value = start;
	document.jumpForm.searchATerm.value = searchATerm;
	// form abschicken
	document.jumpForm.submit();
}
// letzte scroll-position wiederherstellen
function scrollToCoordinates() {
	window.scrollTo(0, <?php echo ($_GET['scrolly']) ? $_GET['scrolly'] : 0; ?>);
}
onload = scrollToCoordinates; // onload = wichtig!
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<span class="title"><?php echo $aMSG['address']['mailing'][$syslang] ?>: <?php echo $sListName; ?> / <?php echo $MailEntries.' '.$aMSG['address']['labels'][$syslang] ?></span>
		</td>
		<td align="right">
		<a href="<?php echo $aENV['SELF_PATH']; ?>adr_mailing.php" title="<?php echo $aMSG['btn']['list'][$syslang]; ?>">
		<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_list.gif" alt="<?php echo $aMSG['btn']['list'][$syslang]; ?>" class="btn"></a>
	</tr>
</table>
<?php echo HR ?>

<?php require_once ("inc.adr_mailing_detnavi.php"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
		<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
		echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
		?></span></td>
		<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
		echo $oDBNavi->getDbnaviLinks($sGEToptions); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
		?></span></td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="10" alt="" border="0"><br>

<form name="jumpForm" method="GET" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="mailinglist" value="<?php echo $mailinglist; ?>">
<input type="hidden" name="listview" value="<?php echo $listview; ?>">
<input type="hidden" name="db_start" value="<?php echo $start; ?>">
<input type="hidden" name="scrolly" value="">
<input type="hidden" name="contact_id" value="">
<input type="hidden" name="address" value="">
<input type="hidden" name="db_action" value="">
<input type="hidden" name="start" value="<?=$start?>">
<input type="hidden" name="searchATerm" value="<?=$searchATerm?>">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr height="20">
		<th width="32%"><?php echo $aMSG['address']['p'][$syslang]; ?></th>
		<th width="32%"><?php echo $aMSG['address']['o1'][$syslang]; ?></th>
		<th width="32%"><?php echo $aMSG['address']['o2'][$syslang]; ?></th>
		<th width="4%">&nbsp;</span></th>
	</tr>
<?php
	$oDb2 =& new dbconnect($aENV['db']); // fuer subqueries
	for($i=0;is_array($aData[$i]);$i++) {
		if ($aData[$i]['company'] == '') {
			// ermittle adressen dieser kontakt(person) -> und damit welche checkboxen angezeigt werden
			$aAdrCat = array();
			$sql	= "SELECT cat FROM adr_address WHERE reference_id='".$aData[$i]['id']."'";
			$oDb2->query($sql);
			while ($aData2 = $oDb2->fetch_array()) { $aAdrCat[] = $aData2['cat']; }
			
			// get-vars in der while-schleife
			$sGETvars = "?id=".$aData[$i]['id']."&start=".$start."&type=contact";
?>
	<tr valign="top">
		<td colspan="3"><span class="text"><?php // name
			$name = (empty($aData[$i]['surname'])) ? $aData[$i]['firstname'] : $aData[$i]['surname'].', '.$aData[$i]['firstname'];
			echo '<b>'.$name.'</b> ( '.$aMSG['form']['group'][$syslang].': '.$aGroup[$aData[$i]['grp']].' )'; // name ?></span>
		</td>
		<td align="right" nowrap><p>
		<?php if ($oPerm->hasPriv('edit')) { // edit button?>
			<a href="<?php echo $sEditPage.$sGETvars; ?>" title="<?php echo $aMSG['std']['edit'][$syslang].' '.$aMSG['address']['contact'][$syslang]; ?>" target="_blank">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['std']['edit'][$syslang].' '.$aMSG['address']['contact'][$syslang]; ?>" class="btn"></a>
		<?php } ?>
		</p></td>
	</tr>
	<tr valign="top">
		<td class="sub2"><?php //checkbox "private address"
		if (in_array('p', $aAdrCat)) { // nur wenn dieser adress-datensatz exisitert:
			// ermittle adresse und ggf. firmenname
			$aData2 = _get_contact_address($oDb2, $aData[$i]['id'], 'p');
			// zusaetzlicher check, ob hier adress-angaben oder nur tel-nummern gespechert sind
			if (!empty($aData2['po_box']) || !empty($aData2['town'])) {
				$checked	= (isset($aInMailinglist[$aData[$i]['id']]) && in_array('p', $aInMailinglist[$aData[$i]['id']])) ? ' checked' : '';
				$db_action	= (!empty($checked)) ? 'delete' : 'insert';
				?>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr valign="top">
					<?php if ($oPerm->hasPriv('edit')) { ?><td width="5%" class="sub2"><input type="checkbox" onClick="changeList('<?php echo $aData[$i]['id']; ?>', 'p', '<?php echo $db_action; ?>','<?=$start?>','<?=$searchATerm?>')" name="p_<?php echo $aData[$i]['id']; ?>" value="p"<?php echo $checked; ?> title="private address" class="radiocheckbox"></td><?php } ?>
					<td width="95%" class="sub2"><span class="text"><?php // name/address layout
						echo _format_address($aData2, (!empty($aData[$i]['title'])?$aData[$i]['title'].' ':'').$aData[$i]['firstname'].' '.$aData[$i]['surname']);?></span></td>
				</tr>
				</table>
			<?php
			}
		} ?></td>
		<td class="sub2"><?php //checkbox "office 1 address"
		if (in_array('o1', $aAdrCat)) { // nur wenn dieser adress-datensatz exisitert:
			// ermittle adresse und ggf. firmenname
			$aData2 = _get_contact_address($oDb2, $aData[$i]['id'], 'o1');
			$sAdress = '';
			// zusaetzlicher check, ob hier adress-angaben oder nur tel-nummern gespechert sind
			if (!empty($aData2['po_box']) || !empty($aData2['town'])) {
				$checked	= (isset($aInMailinglist[$aData[$i]['id']]) && in_array('o1', $aInMailinglist[$aData[$i]['id']])) ? ' checked' : '';
				$db_action	= (!empty($checked)) ? 'delete' : 'insert';
				?>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr valign="top">
					<?php if ($oPerm->hasPriv('edit')) { ?><td width="5%" class="sub2"><input type="checkbox" onClick="changeList('<?php echo $aData[$i]['id']; ?>', 'o1', '<?php echo $db_action; ?>','<?=$start?>','<?=$searchATerm?>')" name="o1_<?php echo $aData['id']; ?>" value="o1"<?php echo $checked; ?> title="office 1 address" class="radiocheckbox"></td><?php } ?>
					<td width="95%" class="sub2"><span class="text"><?php // name/address layout
						echo _format_address($aData2, (!empty($aData[$i]['title'])?$aData[$i]['title'].' ':'').$aData[$i]['firstname'].' '.$aData[$i]['surname']);?></span></td>
				</tr>
				</table>
			<?php
			}
		} ?></td>
		<td class="sub2"><span class="text"><?php //checkbox "office 2 address"
		if (in_array('o2', $aAdrCat)) { // nur wenn dieser adress-datensatz exisitert:
			// ermittle adresse und ggf. firmenname
			$aData2 = _get_contact_address($oDb2, $aData[$i]['id'], 'o2');
			// zusaetzlicher check, ob hier adress-angaben oder nur tel-nummern gespechert sind
			if (!empty($aData2['po_box']) || !empty($aData2['town'])) {
				$checked	= (isset($aInMailinglist[$aData[$i]['id']]) && in_array('o2', $aInMailinglist[$aData[$i]['id']])) ? ' checked' : '';
				$db_action	= (!empty($checked)) ? 'delete' : 'insert';
				?>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr valign="top">
					<?php if ($oPerm->hasPriv('edit')) { ?><td width="5%" class="sub2"><input type="checkbox" onClick="changeList('<?php echo $aData[$i]['id']; ?>', 'o2', '<?php echo $db_action; ?>','<?=$start?>','<?=$searchATerm?>')" name="o2_<?php echo $aData['id']; ?>" value="o2"<?php echo $checked; ?> title="office 2 address" class="radiocheckbox"></td><?php } ?>
					<td width="95%" class="sub2"><span class="text"><?php // name/address layout

						echo _format_address($aData2, (!empty($aData[$i]['title'])?$aData[$i]['title'].' ':'').$aData[$i]['firstname'].' '.$aData[$i]['surname']);?></span></td>
				</tr>
				</table>
			<?php
			}
		} ?></td>
		<td class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td>
	</tr>
	
	<tr><td colspan="4" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

<?php	} else {
			// get-vars in der while-schleife
			$sGETvars = "?id=".$aData[$i]['id']."&start=".$start."&type=company"; ?>

	<tr valign="top">
		<td colspan="3"><span class="text"><?php // name
			echo '<b>'.$aData[$i]['company'].'</b> ( '.$aMSG['form']['group'][$syslang].': '.$aGroup[$aData[$i]['grp']].' )'; ?></span>
		</td>
		<td align="right" nowrap><p>
		<?php if ($oPerm->hasPriv('edit')) { // edit button?>
			<a href="<?php echo $sEditPage.$sGETvars; ?>" title="<?php echo $aMSG['std']['edit'][$syslang].' '.$aMSG['address']['contact'][$syslang]; ?>" target="_blank">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['std']['edit'][$syslang].' '.$aMSG['address']['contact'][$syslang]; ?>" class="btn"></a>
		<?php } ?>
		</p></td>
	</tr>
	<tr valign="top">
		<td class="sub2"><span class="text"></span></td>
		<td class="sub2"><?php //checkbox "office 1 address"
			// ermittle adresse und ggf. firmenname
			$aData2 = _get_contact_address($oDb2, $aData[$i]['id'], 'c');
			$sAdress = '';
			// zusaetzlicher check, ob hier adress-angaben oder nur tel-nummern gespechert sind
			if (!empty($aData2['po_box']) || !empty($aData2['town'])) {
				$checked	= (isset($aInMailinglist[$aData[$i]['id']]) && in_array('c', $aInMailinglist[$aData[$i]['id']])) ? ' checked' : '';
				$db_action	= (!empty($checked)) ? 'delete' : 'insert';
				?>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr valign="top">
					<?php if ($oPerm->hasPriv('edit')) { ?><td width="5%" class="sub2"><input type="checkbox" onClick="changeList('<?php echo $aData[$i]['id']; ?>', 'c', '<?php echo $db_action; ?>','<?=$start?>','<?=$searchATerm?>')" name="c_<?php echo $aData[$i]['id']; ?>" value="c"<?php echo $checked; ?> title="company address" class="radiocheckbox"></td><?php } ?>
					<td width="95%" class="sub2"><span class="text"><?php // name/address layout
						echo _format_address($aData2, $aData[$i]['company']);?></span></td>
				</tr>
				</table>
			<?php
			}
		?></td>
		<td class="sub2"><span class="text"></span></td>
		<td class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td>
	</tr>
	
	<tr><td colspan="4" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
<?php } // END if
	} // END while
	if ($entries == 0) { echo '<tr><td colspan="3" align="center"><span class="text">'.$aMSG['std']['no_entries'][$syslang].'</span></td></tr>'; }
?>
</table>
</form>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
		<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
		echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
		?></span></td>
		<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
		echo $oDBNavi->getDbnaviLinks($sGEToptions); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
		?></span></td>
	</tr>
</table>

<br>

<?php // HILFSFUNKTIONEN //////////////////////////////////////////////////////////////////////////////////
// formatiere Adresse als string aus dem array, das aus der funktion "_get_contact_address()" generiert wird
	function _format_address(&$aAddress, $sName='') {
		if (!is_array($aAddress)) return;
		global $aCountry; // aus der "inc.print_functions.php"!
		
		if (!empty($aAddress['recipient']))		{ $sAdress .= $aAddress['recipient']."<br>"; 
		} else {
			if (!empty($sName))					{ $sAdress .= $sName."<br>"; }
		}
		if (!empty($aAddress['company']))		{ $sAdress .= $aAddress['company']."<br>"; }
		
		// po-box hat prioritaet!
		if (!empty($aAddress['po_box']))	{
			$sAdress .= $aAddress['po_box']."<br>";
			$sAdress .= $aAddress['po_box_town']."<br>";
		} else {
			if (!empty($aAddress['street']))	{ $sAdress .= $aAddress['street']."<br>"; }
			if (!empty($aAddress['building']))	{ $sAdress .= $aAddress['building']."<br>"; }
			if (!empty($aAddress['region']))	{ $sAdress .= $aAddress['region']."<br>"; }
			if (!empty($aAddress['town']))		{ $sAdress .= $aAddress['town']."<br>"; }
		}
		if (!empty($aAddress['country']))		{ $sAdress .= $aCountry[$aAddress['country']]['name']."<br>"; }
		if(!empty($aAddress['email'])) 			{ $sAdress .= $aAddress['email']."<br>"; }
		
		// output
		return $sAdress;
	}
 ?>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
