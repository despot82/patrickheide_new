<?php
/**
* adr_edit_o.php
*
* Detailpage: zum anlegen/editieren der Details eines Firmen-Adress-Adressdatensatzes einer Person (contact)
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	string	$cat		[o1|o2] (required!)
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$type		[company|contact] (optional -> default: contact)
* @param	array	Formular:	$aData
* @param	array	Formular:	$aDefault
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-02-19
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_edit_o.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : 'company'; // company|contact
	$cat			= (isset($_GET['cat'])) ? $oFc->make_secure_string($_GET['cat']) : '';
	$sGEToptions	= "?start=".$start."&type=".$type."&cat=".$cat."&id=".$id; // id nur fuer subnavi
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$aDefault		= (isset($_POST['aDefault'])) ? $_POST['aDefault'] : array();
// 2c. Vars:
	$sTable			= "adr_address";
	$sViewerPage	= $aENV['SELF_PATH']."adr_detail.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button
	$sTelDefault	= "(0)";
	
	if (empty($cat)) header("Location: ".$sViewerPage); // fallback: $cat = PFLICHT!

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// 3. DB
// DB-action: delete contact (remote)
	if (isset($btn['remoteDelete']) && $btn['remoteDelete'] > 0) {
		_delete_contact(&$oDb, $btn['remoteDelete']);
	}
	
// DB-action: clear
	if (isset($btn['clear']) && $aData['id']) {
		// abhaengige daten loeschen:
		$aSub2delete['id'] = $aData['id'];
		
		$oDb->make_delete($aSub2delete, $sTable); // address-data
		// update SEARCH-field
		adr_save_searchdata($oDb, $aData['reference_id']);
		
		// weiterleitung
//		header("Location: ".$sNewPage."&address_id=".$aData['id']); exit;
		header("Location: ".$sNewPage); exit;
	}
	
// DB-action: save or update
	if (isset($btn['save']) 
	|| (isset($btn['remoteSave']) && $btn['remoteSave'] == 1)) {
		// speichere nur die, die nicht default-wert haben
		if ($aData['phone_mob'] == $sTelDefault) { $aData['phone_mob'] =''; }
		foreach ($aDefault as $key => $val) {
			if ($aData[$key] == $aDefault[$key]) { unset($aData[$key]); }
		}
		
		if ($aData['id']) {
			// alten datensatz leeren wenn company geaendert wurde!
			if (isset($btn['clearData']) && $btn['clearData']==1) {
				$aData['company_dep'] = '';
				$aData['company_pos'] = '';
				$aData['phone_tel'] = '';
				$aData['phone_mob'] = '';
				$aData['phone_fax'] = '';
				$aData['email'] = '';
				$aData['web'] = '';
				unset($btn['remoteSave']); // damit NICHT weitergeleitet wird wenn nur die firma getauscht wird
			}
			// Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$aUpdate['id'] = $aData['id'];
			
			$oDb->make_update($aData, $sTable, $aUpdate);
			// update SEARCH-field
			adr_save_searchdata($oDb, $aData['reference_id']);
			// weiterleitung
			if (isset($btn['remoteSave']) && $btn['remoteSave'] == 1) {
				header("Location: ".$sViewerPage); exit; // weiterleitung NUR bei remote-save
			}
		} else { // nur wenn IRGENDWAS eingegeben wurde!
			if (!empty($aData['company_id']) || !empty($aData['company_dep']) || !empty($aData['company_pos']) || !empty($aData['phone_tel']) || !empty($aData['phone_mob']) || !empty($aData['phone_fax']) || !empty($aData['email']) || !empty($aData['web'])) {
				// Insert eines neuen Eintrags
				$aData['created'] = $oDate->get_mysql_timestamp();
				$aData['created_by'] = $Userdata['id'];
				$oDb->make_insert($aData, $sTable);
				$aData['id'] = $oDb->insert_id();
				// update SEARCH-field
				adr_save_searchdata($oDb, $aData['reference_id']);
				// weiterleitung
				header("Location: ".$sNewPage."&address_id=".$aData['id']); exit; // doppelt-speichern-probleme durch neuladen der seite vermeiden
			}
		}
	}
// DB-action: 1. get data for country names
	$aCountry = _get_all_countries($oDb); // von "inc.adr_functions.php"
	
// DB-action: 2. get data CONTENT
	if (isset($aData['id'])) { $address_id = $aData['id']; } else { $aData = ""; }
	if (!isset($address_id) && isset($id) && $id) { // ggf. ID des abhaengigen datensatzes holen
		$oDb->query("SELECT id FROM adr_address WHERE reference_id='".$id."' AND cat='".$cat."'");
		if ($oDb->num_rows()) $address_id = $oDb->fetch_field("id");
	}
	if (isset($address_id) && $address_id) {
		$aData = $oDb->fetch_by_id($address_id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>

<script language="JavaScript" type="text/javascript">
function openCompanyWin(url) { // opens new window
	var left = (screen.width - 470);
	owin = open(url,'CompanyWin','toolbar=0,status=1,scrollbars=1,resizable=1,width=450,height=520,top=10,left='+left);
	owin.focus();
}
</script>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
?><input type="hidden" name="aData[id]" value="<?php echo $address_id; ?>">
<input type="hidden" name="aData[reference_id]" value="<?php echo $id; ?>">
<input type="hidden" name="aData[cat]" value="<?php echo $cat; ?>">
<input type="hidden" name="btn[remoteSave]" value="0">
<input type="hidden" name="btn[remoteDelete]" value="0">
<input type="hidden" name="btn[clearData]" value="0">

<?php require_once ("inc.adr_detnavi.php"); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="20%"><span class="text"><?php echo $aMSG['form']['company'][$syslang]; ?></span></td>
		<td width="80%">
<?php if (!empty($aData['company_id'])) { ?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr valign="top">
			<td width="30%"><nobr><span class="text"><?php
			// firmenname (+client-id) mit link zu seiner detailseite
			$oDb->query("SELECT company, client_nr FROM adr_contact WHERE id=".$aData['company_id']);
			$tmp = $oDb->fetch_array();
			echo '<b><a href="'.$aENV['SELF_PATH'].'adr_edit_d.php?id='.$aData['company_id']."&start=".$start.'&type=company" title="'.$aMSG['std']['edit'][$syslang].' '.$aMSG['address']['company'][$syslang].'">'.$tmp['company']."</a></b>"; // detail-seite (edit-mode)
			echo ($tmp['client_nr'] != '000') ? " (".$tmp['client_nr'].")<br>\n" : "<br>\n";
			// global comany-data // address
			$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$aData['company_id']." AND cat='c'");
			$aData2 = $oDb->fetch_array();
			if (!empty($aData2['street'])) echo $aData2['street']."<br>\n";
			if (!empty($aData2['building'])) echo $aData2['building']."<br>\n";
			if (!empty($aData2['region'])) echo $aData2['region']."<br>\n";
			if (!empty($aData2['town'])) echo $aData2['town']."<br>\n";
			if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br>\n"; ?>
			</span></nobr></td>
			<td width="5%">&nbsp;</td>
			<td width="30%"><nobr><span class="text"><?php
			if (!empty($aData2['phone_tel'])) echo "<b>T</b> ".format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			if (!empty($aData2['phone_mob'])) echo "<b>T</b> ".format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			if (!empty($aData2['phone_fax'])) echo "<b>F</b> ".format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."<br>\n";
			if (!empty($aData2['email'])) {
				formatEmailString($aData, $aData2, $bEmailStringShort);
//				$sEmail	= !empty($aData['firstname'])	? $aData['firstname']:'';
//				$sEmail	.= !empty($aData['surname'])	? ' '.$aData['surname']:'';
//				$sEmail	.= !empty($sEmail)	? ' <'.$aData2['email'].'>':$aData2['email'];
//								
//				echo '<b>E</b> <a href="'.link_uri($sEmail).'">'.show_uri($aData2['email'])."</a><br>\n";
			}
			if (!empty($aData2['web'])) echo '<a href="'.link_uri($aData2['web']).'" target="_blank">'.link_uri($aData2['web'])."</a><br>\n"; ?>
			</span></nobr></td>
			<td width="5%">&nbsp;</td>
			<td width="30%" align="right"><nobr><span class="text">
			<input type="hidden" name="aData[company_id]" value="<?php echo $aData['company_id']; ?>">
			<a href="JavaScript:openCompanyWin('<?php echo $aENV['SELF_PATH']; ?>popup_selectcompany.php?formfield=company_id')" title="<?php echo $aMSG['btn']['replace_company'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_company_new.gif" alt="<?php echo $aMSG['btn']['replace_company'][$syslang]; ?>">| <?php echo $aMSG['btn']['replace'][$syslang]; ?>&nbsp;</a>
			<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?type=company&contact=<?php echo $id; ?>&office=<?php echo $cat; ?>" title="<?php echo $aMSG['btn']['new_company'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_company_new.gif" alt="<?php echo $aMSG['btn']['new_company'][$syslang]; ?>">| <?php echo $aMSG['btn']['new'][$syslang]; ?>&nbsp;</a>
			
<?php } else { // wenn noch kein o1/2 datensatz angelegt wurde ?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr valign="top">
			<td align="right"><input type="hidden" name="aData[company_id]" value="">
				<a href="JavaScript:openCompanyWin('<?php echo $aENV['SELF_PATH']; ?>popup_selectcompany.php?formfield=company_id')" title="<?php echo $aMSG['btn']['select_company'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_company_new.gif" alt="<?php echo $aMSG['btn']['select_company'][$syslang]; ?>">| <?php echo $aMSG['btn']['select'][$syslang]; ?>&nbsp;</a>
				<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?type=company&contact=<?php echo $id; ?>&office=<?php echo $cat; ?>" title="<?php echo $aMSG['btn']['new_company'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_company_new.gif" alt="<?php echo $aMSG['btn']['new_company'][$syslang]; ?>">| <?php echo $aMSG['btn']['new'][$syslang]; ?>&nbsp;</a>
<?php } ?>
			</td>
		</tr></table>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['department'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("company_dep",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['position'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("company_pos",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['phone'][$syslang]; ?></span></td>
		<td><input type="text" name="countryPrefix[tel]" value="<?php echo $aCountry[$aData2['country']]['phone']; ?>" size="4" disabled class="country">
			<?php if (!isset($aData['phone_tel']) || empty($aData['phone_tel'])) { $sClass = ' class="default"'; } else { $sClass = ''; } ?>
			<?php echo $oForm->textfield("phone_tel", 50, 40, $aData2['phone_tel'], $sClass); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
			<?php if (isset($aData2['phone_tel']) && !empty($aData2['phone_tel'])) {
				 ?><input type="hidden" name="aDefault[phone_tel]" value="<?php echo $aData2['phone_tel']; ?>"><?php
				  } // END if ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['mobile'][$syslang]; ?></span></td>
		<td><input type="text" name="countryPrefix[mob]" value="<?php echo $aCountry[$aData2['country']]['phone']; ?>" size="4" disabled class="country">
			<?php echo $oForm->textfield("phone_mob", 50, 40, $sTelDefault); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['fax'][$syslang]; ?></span></td>
		<td><input type="text" name="countryPrefix[fax]" value="<?php echo $aCountry[$aData2['country']]['phone']; ?>" size="4" disabled class="country">
			<?php if (!isset($aData['phone_fax']) || empty($aData['phone_fax'])) { $sClass = ' class="default"'; } else { $sClass = ''; } ?>
			<?php echo $oForm->textfield("phone_fax", 50, 40, $aData2['phone_fax'], $sClass); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
			<?php if (isset($aData2['phone_fax']) && !empty($aData2['phone_fax'])) {
				 ?><input type="hidden" name="aDefault[phone_fax]" value="<?php echo $aData2['phone_fax']; ?>"><?php
				  } // END if ?>
		</td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['email'][$syslang]; ?></span></td>
		<td><?php if (!isset($aData['email']) || empty($aData['email'])) { $sClass = ' class="default"'; } else { $sClass = ''; } ?>
			<?php echo $oForm->textfield("email",250,75, $aData2['email'], $sClass); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
			<?php if (isset($aData2['email']) && !empty($aData2['email'])) {
				 ?><input type="hidden" name="aDefault[email]" value="<?php echo $aData2['email']; ?>"><?php
				  } // END if ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['web'][$syslang]; ?></span></td>
		<td><?php if (!isset($aData['web']) || empty($aData['web'])) { $sClass = ' class="default"'; } else { $sClass = ''; } ?>
			<?php echo $oForm->textfield("web",250,75, $aData2['web'], $sClass); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
			<?php if (isset($aData2['web']) && !empty($aData2['web'])) {
				 ?><input type="hidden" name="aDefault[web]" value="<?php echo $aData2['web']; ?>"><?php
				  } // END if ?>
		</td>
	</tr>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php if ($oPerm->hasPriv('delete') && $mode_key == "edit") { #echo $oForm->button("DELETE"); #onClick="return confirmClear()" ?>
<input type="submit" value="<?php echo $aMSG['btn']['clear_save'][$syslang]; ?>" onClick="return confirmClear()" name="btn[clear]" id="clear" class="but">
<?php } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>