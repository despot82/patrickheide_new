<?php

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_print_labels.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2. GET-params abholen
	$mailinglist	= (isset($_GET['mailinglist'])) ? $oFc->make_secure_int($_GET['mailinglist']) : '';
//	POST-params abholen
	$mailinglist	= (isset($_POST['mailinglist'])) ? $oFc->make_secure_int($_POST['mailinglist']) : '';
	$aFields		= (isset($_POST['fields'])) ? $_POST['fields'] : '';

	if(!is_array($aFields) 
	|| empty($aFields)) {
		header("Location:adr_excel_list.php?mailinglist=".$mailinglist);
	}
	
	set_time_limit(0);
		
	include($aENV['path']['pear']['unix']."Writer.php");
	include($aENV['path']['global_module']['unix']."class.AdrLabelPdfPrinter.php");
	$oExcel =& new AdrLabelPdfPrinter($aENV);

	$sql	= "SELECT listname FROM adr_mailing_name WHERE id='".$mailinglist."'";
	$oDb->query($sql);
	if ($oDb->num_rows()) { $sListName = $oDb->fetch_field('listname'); }

	$oExcel->setODb($oDb);
	
	$oExcel->setMailinglist($mailinglist);
	$oExcel->initialize();
	$oExcel->setIsExcel(true);
	$aInfo	= $oExcel->getDBValues();
	$workbook = new Spreadsheet_Excel_Writer();
	$sFilename	= $oFile->return_good_filename($sListName);
	$workbook->send($sFilename);

	$worksheet =& $workbook->addWorksheet();
	$worksheet->setInputEncoding('utf-8');
	
	if(in_array('strasse', $aFields)) {
		$aFields[]	= 'building';
		$aFields[]	= 'region';
		$aFields[]	= 'ort';
	}

	if(in_array('pobox', $aFields)) {
		$aFields[]	= 'poboxtown';
	}

	$i	= 1;
	foreach($aInfo['emstrasse'] as $nKey => $sValue) {
		$c	= 0;
		foreach($aFields as $sField) {
			$sDesc	= (isset($aMSG['form'][$sField])) ? $aMSG['form'][$sField][$syslang]:$aMSG['address'][$sField][$syslang];
			switch($sField) {
				case 'ort':
					$sDesc	= $aMSG['form']['town'][$syslang];
				break;
				case 'pobox':
					$sDesc	= $aMSG['form']['po_box'][$syslang];
				break;
				case 'poboxtown':
					$sDesc	= $aMSG['form']['po_box'][$syslang]." ".$aMSG['form']['town'][$syslang];;
				break;

				case 'strasse':
					$sDesc	= $aMSG['form']['address'][$syslang];
				break;				
			}
			$sDesc	= utf8_decode($sDesc);			
			$worksheet->write(0, $c, $sDesc);
			$worksheet->write($i, $c, $aInfo["em$sField"][$nKey]);
			$c++;		
		}
		$i++;
	}

	$workbook->close();
?>