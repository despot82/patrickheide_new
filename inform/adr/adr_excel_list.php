<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_excel_list.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");
	require_once ($aENV['path']['global_service']['unix']."class.Search.php");
	require_once ($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$mailinglist	= (isset($_GET['mailinglist'])) ? $oFc->make_secure_int($_GET['mailinglist']) : '';
	$sGEToptions	= "?mailinglist=".$mailinglist;

	// ohne $mailinglist geht nix!
	if ($mailinglist == '') { header("Location: ".$aENV['SELF_PATH']."adr.php"); }

// 2d. DB-action: select
	$sql	= "SELECT listname FROM adr_mailing_name WHERE id='".$mailinglist."'";
	$oDb->query($sql);
	if ($oDb->num_rows()) { $sListName = $oDb->fetch_field('listname'); }

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
	require_once ("./inc.print_functions.php");
// END SHARED *****************************************************************************

	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	echo $oForm->start_tag('adr_excel_labels.php', $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
?>
<input type="hidden" name="mailinglist" value="<?php echo $mailinglist; ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<span class="title"><?php echo $aMSG['address']['mailing'][$syslang] ?>: <?php echo $sListName; ?> / <?php echo $MailEntries.' '.$aMSG['address']['labels'][$syslang] ?></span>
		</td>
		<td align="right">
		<a href="<?php echo $aENV['SELF_PATH']; ?>adr_mailing.php" title="<?php echo $aMSG['btn']['list'][$syslang]; ?>">
		<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_list.gif" alt="<?php echo $aMSG['btn']['list'][$syslang]; ?>" class="btn"></a>
	</tr>
</table>

<?php echo HR ?>

<?php require_once ("inc.adr_mailing_detnavi.php"); ?>

<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="10" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['address']['excelsettings'][$syslang] ?></b></span></td>
		<td width="80%"><span class="text">
			<input type="checkbox" name="fields[]" checked value="name"/> <?php echo $aMSG['form']['name'][$syslang]; ?><br />
			<input type="checkbox" name="fields[]" checked value="company"/> <?php echo $aMSG['form']['company'][$syslang]; ?><br />
			<input type="checkbox" name="fields[]" checked value="recipient"/> <?php echo $aMSG['form']['recipient'][$syslang]; ?><br />
			<input type="checkbox" name="fields[]" checked value="strasse"/> <?php echo $aMSG['form']['address'][$syslang]; ?><br />
			<input type="checkbox" name="fields[]" checked value="pobox"/> <?php echo $aMSG['form']['po_box'][$syslang]; ?><br />
			<input type="checkbox" name="fields[]" checked value="country"/> <?php echo $aMSG['form']['country'][$syslang]; ?><br />
			<input type="checkbox" name="fields[]" checked value="email"/> <?php echo $aMSG['form']['email'][$syslang]; ?><br />
		</span></td>
	</tr>
</table>
<input type="submit" value="<?php echo $aMSG['btn']['export'][$syslang]; ?>" class="but" />
