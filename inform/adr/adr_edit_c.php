<?php
/**
* adr_edit_c.php
*
* Detailpage: zum anlegen/editieren der Details eines Adress-Adressdatensatzes einer Company
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$type		[company|contact] (optional -> default: company)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-02-19
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_edit_c.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : 'company'; // company|contact
	$sGEToptions	= "?start=".$start."&type=".$type."&id=".$id; // id nur fuer subnavi
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars:
	$sTable			= "adr_address";
	$sViewerPage	= $aENV['SELF_PATH']."adr_detail.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button
	$cat			= "c";
	$sTelDefault	= "(0)";

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// 3. DB
// DB-action: delete contact (remote)
	if (isset($btn['remoteDelete']) && $btn['remoteDelete'] > 0) {
		_delete_contact(&$oDb, $btn['remoteDelete']);
	}
// DB-action: clear
	if (isset($btn['clear']) && $aData['id']) {
		// abhaengige daten loeschen:
		$aSub2delete['id'] = $aData['id'];
		$oDb->make_delete($aSub2delete, $sTable); // address-data
		// weiterleitung
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) || (isset($btn['remoteSave']) && $btn['remoteSave'] == 1)) {
		if ($aData['phone_tel'] == $sTelDefault) { $aData['phone_tel'] =''; }
		if ($aData['phone_mob'] == $sTelDefault) { $aData['phone_mob'] =''; }
		if ($aData['phone_fax'] == $sTelDefault) { $aData['phone_fax'] =''; }
		if ($aData['id']) {
			// Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$aUpdate['id'] = $aData['id'];
			$oDb->make_update($aData, $sTable, $aUpdate);
			// update SEARCH-field
			adr_save_searchdata($oDb, $aData['reference_id']);
			// weiterleitung
			if (isset($btn['remoteSave']) && $btn['remoteSave'] == 1) {
				header("Location: ".$sViewerPage); exit; // weiterleitung NUR bei remote-save
			}
		} else {
			// Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id();
			// update SEARCH-field
			adr_save_searchdata($oDb, $aData['reference_id']);
			// weiterleitung
			header("Location: ".$sNewPage."&address_id=".$aData['id']); exit; // doppelt-speichern-probleme durch neuladen der seite vermeiden
		}
	}
// DB-action: 1. get data for country names
	$aCountry = _get_all_countries($oDb); // von "inc.adr_functions.php"
	
// DB-action: 2. get data CONTENT
	if (isset($aData['id'])) { $address_id = $aData['id']; } else { $aData = ""; }
	if (!isset($address_id) && isset($id) && $id) { // ggf. ID des abhaengigen datensatzes holen
		$oDb->query("SELECT id FROM adr_address WHERE reference_id='".$id."' AND cat='".$cat."'");
		if ($oDb->num_rows()) $address_id = $oDb->fetch_field("id");
	}
	if (isset($address_id) && $address_id) {
		$aData = $oDb->fetch_by_id($address_id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	//$oForm->check_field("street",$aMSG['form']['street'][$syslang].'!'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	//$oForm->check_field("town",$aMSG['form']['town'][$syslang].'!'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>

<script language="JavaScript" type="text/javascript">
country = new Object();
<?php // countries as javascript-objects
	foreach ($aCountry as $key => $val) {
		echo "country['".$key."'] = '".$val['phone']."';\n";
	} ?>
// Funktion zum automatischen Befuellen der Laendervorwahl nach Auswahl eines Landes aus einer option-list
function fillPhonePrefixe(selObj) {
	var sel = selObj.options[selObj.selectedIndex].value;
	document.forms['editForm'].elements['countryPrefix[tel]'].value = country[sel];
	document.forms['editForm'].elements['countryPrefix[mob]'].value = country[sel];
	document.forms['editForm'].elements['countryPrefix[fax]'].value = country[sel];
}
</script>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
?><input type="hidden" name="aData[id]" value="<?php echo $address_id; ?>">
<input type="hidden" name="aData[reference_id]" value="<?php echo $id; ?>">
<input type="hidden" name="aData[cat]" value="c">
<input type="hidden" name="btn[remoteSave]" value="0">
<input type="hidden" name="btn[remoteDelete]" value="0">

<?php require_once ("inc.adr_detnavi.php"); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="20%"><span class="text"><?php echo $aMSG['form']['street'][$syslang]; ?></span></td>
		<td width="80%"><?php echo $oForm->textfield("street",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text">&nbsp;</span></td>
		<td><?php echo $oForm->textfield("building",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text">&nbsp;</span></td>
		<td><?php echo $oForm->textfield("region",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['town'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("town",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['po_box'][$syslang]; ?></span></td>
		<td class="sub2">
			<?php echo $oForm->textfield("po_box",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['town'][$syslang]; ?></span></td>
		<td class="sub2">
			<?php echo $oForm->textfield("po_box_town",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['country'][$syslang]; ?></span></td>
		<td><?php // Country dropdown
		#echo $oForm->select("country",$aCountry); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false]
		// wegen den "most wanted" countries -> select von hand aufbauen: ?>
		<select name="aData[country]" size="1" onchange="fillPhonePrefixe(this)">
		<option value=""<?php if ($mode_key=="new") echo " selected"; ?>><?php echo $aMSG['form']['please_select'][$syslang]; ?></option>
		<option value="UK">United Kingdom</option>
		<option value="DE">Germany</option>
		<option value="FR">France</option>
		<option value="US">United States</option>
		<option value="ES">Spain</option>
		<option value="">----------------</option>
		<?php // country dropdown
		foreach ($aCountry as $key => $val) {
		if ($mode_key == "edit" && $aData['country']==$key) {$sel=" selected"; $country_phone=$val['phone'];} else {$sel="";} // highlight
		echo '<option value="'.$key.'"'.$sel.'>'.$val['name']."</option>\n";
		} ?></select></td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['phone'][$syslang]; ?> 1</span></td>
		<td><input type="text" name="countryPrefix[tel]" value="<?php echo $country_phone; ?>" size="4" disabled class="country">
			<?php echo $oForm->textfield("phone_tel", 50, 40, $sTelDefault); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['phone'][$syslang]; ?> 2</span></td>
		<td><input type="text" name="countryPrefix[mob]" value="<?php echo $country_phone; ?>" size="4" disabled class="country">
			<?php echo $oForm->textfield("phone_mob", 50, 40, $sTelDefault); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['fax'][$syslang]; ?></span></td>
		<td><input type="text" name="countryPrefix[fax]" value="<?php echo $country_phone; ?>" size="4" disabled class="country">
			<?php echo $oForm->textfield("phone_fax", 50, 40, $sTelDefault); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['email'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("email",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['web'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("web",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php if ($oPerm->hasPriv('delete') && $mode_key == "edit") { #onClick="return confirmClear()" ?>
<input type="submit" value="<?php echo $aMSG['btn']['clear_save'][$syslang]; ?>" name="btn[clear]" id="clear" class="but">
<?php } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>