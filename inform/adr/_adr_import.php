<?php

echo $_SERVER['DOCUMENT_ROOT'];

// set global modi *****************************************
	#$root_path				= '/var/www/html/samba/';
	#$import_file			= 'kunden/manss/data_in/Adressexport/Adressexport.txt';
	$root_path				= '/home/www/html-data/';
	$import_file			= 'home/Adressexport.txt';
	$fields_trennzeichen	= ';';
	$lines_trennzeichen		= '\n';
	$ignoreFirstline		= true; // [true|false]
	// specials
	$formatPhoneNumbers		= true; // [true|false]
	$emptyCountryIsUK		= true; // [true|false]
	$adr_group_default		= '40'; // [10|20|30|40|50]
	// DEBUG/TEST
	$debugMode				= true; // [true|false]
//***********************************************************

// init
	require_once ("php/_include_all.php");
	$oDb = new dbconnect($aENV['db']);
	$oDb2 = new dbconnect($aENV['db']);
	if ($debugMode) {
		#$oDb->set_debug_true();
		#$oDb2->set_debug_true();
	}


/* OUTLOOK felder

"Anrede","Vorname","Weitere Vornamen","Nachname","Suffix",
"Firma","Abteilung","Position",
"Stra�e gesch�ftlich","Stra�e gesch�ftlich 2","Stra�e gesch�ftlich 3","Ort gesch�ftlich","Region gesch�ftlich","Postleitzahl gesch�ftlich","Land gesch�ftlich",
"Stra�e privat","Stra�e privat 2","Stra�e privat 3","Ort privat","Region privat","Postleitzahl privat","Land privat",
"Weitere Stra�e","Weitere Stra�e 2","Weitere Stra�e 3","Weiterer Ort","Weitere Region","Weitere Postleitzahl","Weiteres Land",
"Telefon Assistent","Fax gesch�ftlich","Telefon gesch�ftlich","Telefon gesch�ftlich 2","R�ckmeldung","Autotelefon","Telefon Firma","Fax privat","Telefon privat","Telefon privat 2","ISDN","Mobiltelefon","Weiteres Fax","Weiteres Telefon","Pager","Haupttelefon","Mobiltelefon 2","Telefon f�r H�rbehinderte","Telex","Abrechnungsinformation",
"Benutzer 1","Benutzer 2","Benutzer 3","Benutzer 4",
"Beruf","B�ro",
"E-Mail-Adresse","E-Mail-Typ","E-Mail: Angezeigter Name",
"E-Mail 2: Adresse","E-Mail 2: Typ","E-Mail 2: Angezeigter Name",
"E-Mail 3: Adresse","E-Mail 3: Typ","E-Mail 3: Angezeigter Name",
"Empfohlen von","Geburtstag","Geschlecht","Hobby","Initialen","Internet-Frei/Gebucht","Jahrestag","Kategorien","Kinder","Konto","Name Assistent","Name des/der Vorgesetzten","Notizen","Organisations-Nr.","Ort","Partner","Postfach","Priorit�t","Privat","Regierungs-Nr.","Reisekilometer","Sprache","Stichw�rter","Vertraulichkeit","Verzeichnisserver",
"Webseite"

~90 felder! */

// alle relevanten felder:
$aValidOutlookFields = array(
"Anrede","Vorname","Nachname","Firma",
"Abteilung","Funktion","Position","Stra�e gesch�ftlich","Stra�e gesch�ftlich 2","Ort gesch�ftlich","Region gesch�ftlich","Postleitzahl gesch�ftlich","Land gesch�ftlich",
"Stra�e privat","Stra�e privat 2","Ort privat","Region privat","Postleitzahl privat","Land privat",
"Weitere Stra�e","Weitere Stra�e 2","Weiterer Ort","Weitere Region","Weitere Postleitzahl","Weiteres Land",
"Fax gesch�ftlich","Telefon gesch�ftlich",
"Fax privat","Telefon privat","Mobiltelefon",
"Weiteres Fax","Weiteres Telefon","Mobiltelefon 2",
"E-Mail-Adresse","E-Mail 2: Adresse","E-Mail 3: Adresse",
"Geburtstag","Geschlecht","Kategorien","Notizen","Organisations-Nr.","Stichw�rter","Webseite"
);

/*
1. TODO: 1. zeile der csv-datei auslesen und ggf. reihenfolge ueberpruefen
2. TODO: restliche zeilen auslesen und alle relevanten zellen in die import-table schaufeln
3. TODO: datensaetze (entweder gleich beim import oder sp�ter mittels UPDATE) reformatieren ( laender->k�rzel, format_phone(), format_email(), ...)
4. TODO: datensaetze in die "echten" tables schaufeln
*/


// ----------------------------------------------------------------------------------
// HILFSFUNKTIONEN

// erstelle Import DB-Table (EINE table mit allen importmoeglichkeiten)
	function _create_import_table() {
		global $oDb;
		$sql = "CREATE TABLE IF NOT EXISTS `adr_import` (
		  `id` int(11) NOT NULL auto_increment,
		  `cat` enum('p','o1','o2','c') NOT NULL default 'p',
		  `company` varchar(150) default NULL,
		  `client_nr` varchar(5) default NULL,
		  `gender` enum('m','f') default NULL,
		  `_anrede` varchar(10) default NULL,
		  `title` varchar(10) default NULL,
		  `firstname` varchar(50) default NULL,
		  `surname` varchar(50) default NULL,
		  `birthday` date default NULL,
		  `_birthday` varchar(30) default NULL,
		  `description` text,
		  `comment` text,
		  
		  `p_street` varchar(100) default NULL,
		  `p_building` varchar(100) default NULL,
		  `p_region` varchar(100) default NULL,
		  `p_town` varchar(100) default NULL,
		  `_p_zip` varchar(10) default NULL,
		  `p_country` char(2) default NULL,
		  `_p_country` varchar(50) default NULL,
		  `p_phone_mob` varchar(30) default NULL,
		  `p_phone_tel` varchar(30) default NULL,
		  `p_phone_tel2` varchar(30) default NULL,
		  `p_phone_fax` varchar(30) default NULL,
		  `p_email` varchar(50) default NULL,
		  `p_web` varchar(50) default NULL,
		  `p_recipient` varchar(150) default NULL,
		  
		  `o1_street` varchar(100) default NULL,
		  `o1_building` varchar(100) default NULL,
		  `o1_region` varchar(100) default NULL,
		  `o1_town` varchar(100) default NULL,
		  `_o1_zip` varchar(10) default NULL,
		  `o1_country` char(2) default NULL,
		  `_o1_country` varchar(50) default NULL,
		  `o1_phone_mob` varchar(30) default NULL,
		  `o1_phone_tel` varchar(30) default NULL,
		  `o1_phone_tel2` varchar(30) default NULL,
		  `o1_phone_fax` varchar(30) default NULL,
		  `o1_email` varchar(50) default NULL,
		  `o1_web` varchar(50) default NULL,
		  `o1_company_id` int(11) default NULL,
		  `o1_company_dep` varchar(150) default NULL,
		  `o1_company_pos` varchar(150) default NULL,
		  
		  `o2_street` varchar(100) default NULL,
		  `o2_building` varchar(100) default NULL,
		  `o2_region` varchar(100) default NULL,
		  `o2_town` varchar(100) default NULL,
		  `_o2_zip` varchar(10) default NULL,
		  `o2_country` char(2) default NULL,
		  `_o2_country` varchar(50) default NULL,
		  `o2_phone_mob` varchar(30) default NULL,
		  `o2_phone_tel` varchar(30) default NULL,
		  `o2_phone_tel2` varchar(30) default NULL,
		  `o2_phone_fax` varchar(30) default NULL,
		  `o2_email` varchar(50) default NULL,
		  `o2_web` varchar(50) default NULL,
		  `o2_company_id` int(11) default NULL,
		  `o2_company_dep` varchar(150) default NULL,
		  `o2_company_pos` varchar(150) default NULL,
		  
		  `c_street` varchar(100) default NULL,
		  `c_building` varchar(100) default NULL,
		  `c_region` varchar(100) default NULL,
		  `c_town` varchar(100) default NULL,
		  `_c_zip` varchar(10) default NULL,
		  `c_country` char(2) default NULL,
		  `_c_country` varchar(50) default NULL,
		  `c_phone_mob` varchar(30) default NULL,
		  `c_phone_tel` varchar(30) default NULL,
		  `c_phone_tel2` varchar(30) default NULL,
		  `c_phone_fax` varchar(30) default NULL,
		  `c_email` varchar(50) default NULL,
		  `c_web` varchar(50) default NULL,
		  
		  PRIMARY KEY  (`id`)
		) TYPE=MyISAM;";
		return $oDb->query($sql);
	}

// Import DB-Table LOESCHEN
	function _delete_import_table() {
		global $oDb;
		$sql = "DROP TABLE IF EXISTS `adr_import`;";
		return $oDb->query($sql);
	}

// Import DB-Table leeren
	function _clear_import_table() {
		global $oDb;
		$sql = "DELETE FROM `adr_import`;";
		return $oDb->query($sql);
	}



/*manss::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// Daten in Temp-Table laden
function _load_file_in_table() {
	global $oDb;
	global $root_path, $import_file, $fields_trennzeichen, $lines_trennzeichen, $ignoreFirstline; // ????
	$sql = "LOAD DATA LOCAL INFILE '".$root_path.$import_file."' 
			REPLACE INTO TABLE adr_import 
			FIELDS TERMINATED BY '".$fields_trennzeichen."' 
			LINES TERMINATED BY '".$lines_trennzeichen."' ";
	if ($ignoreFirstline) { $sql .= "IGNORE 1 LINES "; }
	$sql .= "(`description`,`title`,`firstname`,`surname`,`company`,`company_pos`,`building`,`street`,`region`,`town`,`country`,`phone_tel`,`phone_tel2`,`phone_fax`,`phone_mob`,`email`,`web`,`comment`);";
	//Reihenfolge im txt-file: Beschreibung;Titel;Vorname;Nachname;Firma;Position;Building;Street;Region;Zip City;Land;Telefon 1;Telefon 2;Fax;Mobiltelefon;Email;www;Comment
	return $oDb->query($sql);
}

// 3: "cat" updaten
	$sql = "UPDATE `tmp_adr_import` SET cat='c' WHERE company<>NULL OR company<>'';";
	if ($debugMode) {
		echo $sql."<br><br>";
	} else {
		$oDb->query($sql);
		echo "<br><br>3: UPDATE `tmp_adr_import`: fertig!";
	}


// ----------------------------------------------------------------------------------
// Daten in die echten Tables schaufeln...

// 4:
	// alle FIRMEN (ohne Personen) auslesen
	$sql = "SELECT DISTINCT company,description,comment, 
			street,building,region,town,country,phone_mob,phone_tel,phone_tel2,phone_fax,email,web 
			FROM `tmp_adr_import` WHERE company<>'' AND firstname='' AND surname='' ORDER BY company;";
	if ($debugMode) {
		echo $sql."<br><br>";
	} else {
		$oDb->query($sql);
		$anz = $oDb->num_rows();
		echo "<br><br>4: alle FIRMEN (ohne Personen) auslesen: (".$anz.") fertig!";
	}
	$i = 0;
	// alle Firmen (ohne Personen) importieren
	while ($aData = $oDb->fetch_array()) {
		// mob/tel2 handling
		if (empty($aData['phone_mob'])) { // wenn 'mob' leer und 'tel2' gefuellt -> verschiebe 'tel2' in 'mob'
			$aData['phone_mob'] = $aData['phone_tel2'];
		}
		if (!empty($aData['phone_mob']) && !empty($aData['phone_tel2'])) {
			$aData['comment'] .= ' '.$aData['phone_tel2'];
		} // wenn 'mob' gefuellt und 'tel2' gefuellt -> verschiebe 'tel2' ans ende von 'comment'
		
	// a. stamm-datensatz insert
		$sql2 = "INSERT INTO `adr_contact` (company,description,comment) 
				VALUES ('".addslashes(trim($aData['company']))."','".addslashes(trim($aData['description']))."','".addslashes(trim($aData['comment']))."');";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			echo "<br>".$aData['company'];
			echo "<br>a. stamm-datensatz insert: fertig!";
		}
	// b. vars aufbereiten
		// reference_id
		$reference_id = $oDb2->insert_id();
		// country
		$country = convert_country($aData['country']);
		// phone/email/web
		$aData['phone_tel'] = convert_phone($aData['phone_tel']);
		$aData['phone_tel2'] = convert_phone($aData['phone_tel2']);
		$aData['phone_mob'] = convert_phone($aData['phone_mob']);
		$aData['phone_fax'] = convert_phone($aData['phone_fax']);
		$aData['email'] = convert_email($aData['email']);
		$aData['web'] = convert_web($aData['web']);
	// c. adress-datensatz insert
		$sql2 = "INSERT INTO `adr_address` (reference_id,cat,street,building,region,town,country,phone_mob,phone_tel,phone_fax,email,web) 
				VALUES ('".$reference_id."','c','".addslashes(trim($aData['street']))."','".addslashes(trim($aData['building']))."','".addslashes(trim($aData['region']))."','".addslashes(trim($aData['town']))."','".addslashes($country)."','".addslashes(trim($aData['phone_mob']))."','".addslashes(trim($aData['phone_tel']))."','".addslashes(trim($aData['phone_fax']))."','".addslashes($aData['email'])."','".addslashes($aData['web'])."');";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			echo "<br>c. adress-datensatz insert: fertig!";
		}
		$i++; // hochzaehlen
	}
	echo "<br> ".$i." von ".$anz." FIRMEN (ohne Personen) eingetragen. fertig!";

// 5:
	// alle FIRMEN (mit Personen) auslesen
	$sql = "SELECT DISTINCT company,street,building,region,town,country,web 
			FROM `tmp_adr_import` WHERE company<>'' AND (firstname<>'' OR surname<>'') ORDER BY company;";
	if ($debugMode) {
		echo $sql."<br><br>";
	} else {
		$oDb->query($sql);
		$anz = $oDb->num_rows();
		echo "<br><br>5: alle FIRMEN (mit Personen) auslesen: (".$anz.") fertig!";
	}
	$i = 0;
	// alle Firmen (mit Personen) importieren
	while ($aData = $oDb->fetch_array()) {
	// a. ueberpruefen, ob es diese firma schon gibt
		$sql2 = "SELECT c.id 
				FROM `adr_contact` as c, `adr_address` as a 
				WHERE c.company='".addslashes(trim($aData['company']))."'
					AND a.street='".addslashes(trim($aData['street']))."'
					AND a.building='".addslashes(trim($aData['building']))."'
					AND a.region='".addslashes(trim($aData['region']))."'
					AND a.town='".addslashes(trim($aData['town']))."';";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			if ($oDb2->num_rows()) { $mode = "KEIN insert"; } else { $mode = "insert"; }
			echo "<br>a. ueberpruefen, ob es diese firma (".$aData['company'].") schon gibt: ";
			echo ($mode == "insert") ? "Nein -> Insert." : "<font color=red>Ja -> KEIN Insert!</font>";
		}
		if ($mode == "insert") {
	// b. ggf. stamm-datensatz insert
			$sql2 = "INSERT INTO `adr_contact` (company) 
					VALUES ('".addslashes(trim($aData['company']))."');";
			if ($debugMode) {
				echo $sql2."<br><br>";
			} else {
				$oDb2->query($sql2);
				// reference_id (fuer adress-datensatz insert) ermitteln
				$reference_id = $oDb2->insert_id();
				echo "<br>b. stamm-datensatz insert: fertig!";
			}
	// c. ggf. vars aufbereiten
			// country
			$country = convert_country($aData['country']);
			// phone/email/web
			$aData['web'] = convert_web($aData['web']);
	// d. ggf. adress-datensatz insert
			$sql2 = "INSERT INTO `adr_address` (reference_id,cat,street,building,region,town,country,web) 
					VALUES ('".$reference_id."','c','".addslashes(trim($aData['street']))."','".addslashes(trim($aData['building']))."','".addslashes(trim($aData['region']))."','".addslashes(trim($aData['town']))."','".addslashes($country)."','".addslashes($aData['web'])."');";
			if ($debugMode) {
				echo $sql2."<br><br>";
			} else {
				$oDb2->query($sql2);
				echo "<br>d. adress-datensatz insert: fertig!";
			}
		}
		if ($mode == "insert") $i++; // hochzaehlen
	}
	echo "<br> ".$i." von ".$anz." FIRMEN (mit Personen) eingetragen. fertig!";

// 6:
	// alle (Privat-)Personen auslesen
	$sql = "SELECT title,firstname,surname,description,comment, 
			street,building,region,town,country,phone_mob,phone_tel,phone_fax,email,web 
			FROM `tmp_adr_import` WHERE cat='p';";
	if ($debugMode) {
		echo $sql."<br><br>";
	} else {
		$oDb->query($sql);
		$anz = $oDb->num_rows();
		echo "<br><br>6. alle (Privat-)Personen auslesen: (".$anz.") fertig!";
	}
	$i = 0;
	// a. alle (Privat-)Personen importieren (stamm-datensatz)
	while ($aData = $oDb->fetch_array()) {
	// a. stamm-datensatz insert
		$sql2 = "INSERT INTO `adr_contact` (grp,title,firstname,surname,description,comment) 
				VALUES ('".$adr_group_default."','".addslashes(trim($aData['title']))."','".addslashes(trim($aData['firstname']))."','".addslashes(trim($aData['surname']))."','".addslashes(trim($aData['description']))."','".addslashes(trim($aData['comment']))."');";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			echo "<br>".$aData['firstname']." ".$aData['surname'];
			echo "<br>a. stamm-datensatz insert: fertig!";
		}
	// b. vars aufbereiten
		// reference_id
		$reference_id = $oDb2->insert_id();
		// country
		$country = convert_country($aData['country']);
		// phone/email/web
		$aData['phone_tel'] = convert_phone($aData['phone_tel']);
		$aData['phone_mob'] = convert_phone($aData['phone_mob']);
		$aData['phone_fax'] = convert_phone($aData['phone_fax']);
		$aData['email'] = convert_email($aData['email']);
		$aData['web'] = convert_web($aData['web']);
	// c. adress-datensatz insert
		$sql2 = "INSERT INTO `adr_address` (reference_id,cat,street,building,region,town,country,phone_mob,phone_tel,phone_fax,email,web) 
				VALUES ('".$reference_id."','p','".addslashes(trim($aData['street']))."','".addslashes(trim($aData['building']))."','".addslashes(trim($aData['region']))."','".addslashes(trim($aData['town']))."','".addslashes($country)."','".addslashes(trim($aData['phone_mob']))."','".addslashes(trim($aData['phone_tel']))."','".addslashes(trim($aData['phone_fax']))."','".addslashes($aData['email'])."','".addslashes($aData['web'])."');";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			echo "<br>c. adress-datensatz insert: fertig!";
		}
		$i++; // hochzaehlen
	}
	echo "<br> ".$i." von ".$anz." (Privat-)Personen eingetragen. fertig!";

// 7:
	// alle (Firmen-)Personen auslesen
	$sql = "SELECT title,firstname,surname,description,comment, 
			company,company_pos,street,building,region,town,country,phone_mob,phone_tel,phone_fax,email,web 
			FROM `tmp_adr_import` WHERE cat='c' AND (firstname<>'' OR surname<>'');";
	if ($debugMode) {
		echo $sql."<br><br>";
	} else {
		$oDb->query($sql);
		$anz = $oDb->num_rows();
		echo "<br><br>7: alle (Firmen-)Personen auslesen: (".$anz.") fertig!";
	}
	$i = 0;
	// a. alle (Firmen-)Personen importieren
	while ($aData = $oDb->fetch_array()) {
	// a. stamm-datensatz insert
		$sql2 = "INSERT INTO `adr_contact` (grp,title,firstname,surname,description,comment) 
				VALUES ('".$adr_group_default."','".addslashes(trim($aData['title']))."','".addslashes(trim($aData['firstname']))."','".addslashes(trim($aData['surname']))."','".addslashes(trim($aData['description']))."','".addslashes(trim($aData['comment']))."');";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			echo "<br>".$aData['firstname']." ".$aData['surname'];
			echo "<br>a. stamm-datensatz insert: fertig!";
		}
	// b. vars aufbereiten
		// reference_id
		$reference_id = $oDb2->insert_id();
		// country
		$country = convert_country($aData['country']);
		// phone/email/web
		$aData['phone_tel'] = convert_phone($aData['phone_tel']);
		$aData['phone_mob'] = convert_phone($aData['phone_mob']);
		$aData['phone_fax'] = convert_phone($aData['phone_fax']);
		$aData['email'] = convert_email($aData['email']);
		$aData['web'] = convert_web($aData['web']);
	// c. zugehoerige Firmen-ID ermitteln
		$sql2 = "SELECT id FROM `adr_contact` WHERE company='".addslashes(trim($aData['company']))."';";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			echo "<br>c. zugehoerige Firmen-ID ermitteln: fertig!";
		}
		
		if ($oDb2->num_rows()) {
			$company_id = $oDb2->fetch_field("id");
		} else {
			die ("ERROR: keine company_id!");
		}
	// d. adress-datensatz insert
		$sql2 = "INSERT INTO `adr_address` (reference_id,cat,company_id,company_pos,street,building,region,town,country,phone_mob,phone_tel,phone_fax,email,web) 
				VALUES ('".$reference_id."','o1','".$company_id."','".addslashes(trim($aData['company_pos']))."','".addslashes(trim($aData['street']))."','".addslashes(trim($aData['building']))."','".addslashes(trim($aData['region']))."','".addslashes(trim($aData['town']))."','".addslashes($country)."','".addslashes(trim($aData['phone_mob']))."','".addslashes(trim($aData['phone_tel']))."','".addslashes(trim($aData['phone_fax']))."','".addslashes($aData['email'])."','".addslashes($aData['web'])."');";
		if ($debugMode) {
			echo $sql2."<br><br>";
		} else {
			$oDb2->query($sql2);
			echo "<br>d. adress-datensatz insert: fertig!";
		}
		$i++; // hochzaehlen
	}
	echo "<br> ".$i." von ".$anz." (Firmen-)Personen eingetragen. fertig!";
*/

// TEST...
//SELECT company FROM `tmp_adr_import`  WHERE company<>'' ORDER BY company ASC (->63)
//SELECT DISTINCT company FROM `tmp_adr_import`  WHERE company<>'' ORDER BY company ASC (->48)


///////////////////////////////////////////////////////////////////////////////////////////////
// hilfsfunktionen

/**
* aendert ausgeschrieben laender-namen in dessen (2stelligen) laender-code
* @param (string) $name
* @return (string) $country_code
*/
function convert_country($name) {
	// vars
	global $emptyCountryIsUK;
	// TODO: Funktion schreiben, die diese arrays aus der country-table generiert - diese dann hier nur includen...
	$searchCountry = array("GERMANY","SPAIN","USA","FRANCE","ITALY",
							"CANADA","DENMARK","AUSTRALIA","AUSTRIA","CUBA",
							"SWITZERLAND","SOUTH AFRICA","SWEDEN","NETHERLANDS","CZECH REPUBLIC",
							"RUSSIA","KOREA","BELGIUM","JAPAN","GREECE",
							"ISRAEL","FINLAND","CHILE","ARGENTINA","THAILAND",
							"IRELAND","HONG KONG","SINGAPORE");
	$replaceCountry = array("DE","ES","US","FR","IT",
							"CA","DK","AU","AT","CU",
							"CH","ZA","SE","NL","CZ",
							"RU","KR","BE","JP","GR",
							"IL","FI","CL","AR","TH",
							"IE","HK","SG");
	// konvertierung
	$country = strToUpper(trim($name));
	$country = str_replace($searchCountry, $replaceCountry, $country);
	if (empty($country) 
		&& strstr($aData['phone_tel'], "+49")) {$country = "DE";}
	if ($emptyCountryIsUK && empty($country) 
		&& strstr($aData['phone_tel'], "020") 
		&& strstr($aData['town'], "London")) {$country = "UK";}
	if ($emptyCountryIsUK && empty($country)) {$country = "UK";}
	// output
	return $country;
	/*	TODO: -> nachdem man alle datensaetze durch die funktion geschickt hat,
		mittels sql checken ob irgendeiner der datensaetze noch einen laendernamen hat der laenger als 2 chars ist
		... das selbe mit der "convert_phone()" machen...
	*/
}

/**
* entfernt aus der tel-nummer ggf die laendervorwahl
* @param (string) $phone
* @return (string) $phone
*/
function convert_phone($phone) {
	global $formatPhoneNumbers;
	// TODO: @see: "convert_country()"...
	$searchPhone = array("+49 ","+34 ","+1 ","+33 ","+39 ",
						"+45 ","+61 ","+43 ","+53 ","+41 ",
						"+27 ","+46 ","+31 ","+42","+7 ",
						"+82 ","+32 ","+81 ","+30 ","+972 ",
						"+358 ","+56 ","+54 ","+66","+353 ",
						"+852 ","+65 ");
	$replacePhone = array("","","","","",
						"","","","","",
						"","","","","",
						"","","","","",
						"","","","","",
						"","");
	// konvertierung
	$phone = ($formatPhoneNumbers) ? str_replace($searchPhone, $replacePhone, trim($phone)) : trim($phone);
	// output
	return $phone;
}

/**
* entfernt aus der mailadresse ggf das "mailto:"
* @param (string) $email
* @return (string) $email
*/
function convert_email($email) {
	// konvertierung
	$email =  str_replace("mailto:", '', trim($email));
	// output
	return $email;
}

/**
* entfernt aus der webadresse ggf das "http://"
* @param (string) $email
* @return (string) $email
*/
function convert_web($web) {
	// konvertierung
	$web =  str_replace("http://", '', trim($web));
	// output
	return $web;
}
?>
