<?php
// SHARED BEGIN ****************************************************************************
	
	/*	folgende GET-params werden erwartet:
		- KEINE
		
		folgende Variablen stehen nach diesem Include zur Verfuegung:
		- KEINE
		
		Funktionen:
		- function _get_all_groups(&$oDb)		// returns (array)	$aGroup (by reference)
		- function _get_all_countries(&$oDb)	// returns (array)	$aCountry (by reference)
		- function _delete_contact(&$oDb)		// returns void (redirect)
		// zum indizieren eines ADR-Datensatzes:
		- function adr_save_searchdata(&$oDb, $id)	// returns void
	*/
	
// HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////////
	// get data for country names and phone prefix
	function &_get_all_groups(&$oDb) {
		global $syslang;
		$aGroup = array();
		$oDb->query("SELECT id, title_".$syslang." as title FROM adr_group ORDER BY id ASC");
		while ($tmp = $oDb->fetch_array()) {
			$aGroup[$tmp['id']] = $tmp['title'];
		}
		return $aGroup;
	}
	
	// get data for country names and phone prefix
	function &_get_all_countries(&$oDb) {
		$aCountry = array();
		$oDb->query("SELECT code, country, phone_prefix FROM adr_country ORDER BY country");
		while ($tmp = $oDb->fetch_array()) {
			$aCountry[$tmp['code']]['name']		= $tmp['country'];
			$aCountry[$tmp['code']]['phone']	= (!empty($tmp['phone_prefix'])) ? '+'.$tmp['phone_prefix'] : '';
		}
		return $aCountry;
	}
	
	// delete complete contact (and all address-dependencies)
	function _delete_contact(&$oDb, $sContactId) {
		global $aENV, $start, $type, $Userdata, $bDelError;
		$bDelError = false;
		
		if (isset($aENV['module']['adm'])) {
			// check whether contact is assigned to a current project
			$sql	= "SELECT id FROM adm_project WHERE client_id = '".$sContactId."' OR contact_id = '".$sContactId."'";
			$oDb->query($sql);
			$nProjects = $oDb->num_rows();
			if ($nProjects > 0) {
				$bDelError = true;
			}
		}
		
		if ($bDelError == false) {
			require_once($aENV['path']['global_service']['unix'].'class.Search.php');
					
			$oSearch	= new Search($oDb,$aENV,'adr',$Userdata['id']);
			$oSearch->delete($sContactId, 'adr_contact');
			
			// delete data
			$aData2delete['id']	= $sContactId; // stammdatensatz
			$oDb->make_delete($aData2delete, "adr_contact");
			
			// abhaengige daten loeschen
			$aSub2delete['reference_id'] = $sContactId; // address-data
			$oDb->make_delete($aSub2delete, "adr_address");
			
			// weiterleitung
			header("Location: ".$aENV['SELF_PATH']."adr.php?start=".$start."&type=".$type); exit;
		}
	}
	
// ADR-SEARCH ////////////////////////////////////////////////////////////////////////
	// insert/update search-data
	function adr_save_searchdata($oDb, $sRefId) {
		// check vars
		if (empty($sRefId)) die("ERROR in adr_save_searchdata(): NO id!");
		global $Userdata,$aENV;
		require_once($aENV['path']['global_service']['unix'].'class.Search.php');
		$oSearch = new Search($oDb,$aENV,'adr',$Userdata['id']);
		// update
		return $oSearch->save($sRefId, 'adr_contact', '', trim(_get_search_data($oDb, $sRefId)));
	}
	// get data search
	function &_get_search_data(&$oDb, $id) {
		global $syslang, $oDate;
		
		// Stammdatensatz
		$aData = $oDb->fetch_by_id($id, "adr_contact");
		// set type
		$type = (!isset($aData['company']) || empty($aData['company'])) ? "contact" : "company";
		// format name
		$name = ($type == "company") 
			? $aData['company'] 
			: trim($aData['surname'].' '.$aData['firstname'].' '.$aData['client_shortname']); // ggf. 1. leerzeichen entfernen (wenn kein (nach/vor)name)
		// format basics
		$basics = ($type == "company") 
			? $aData['client_shortname'].' '.$aData['client_nr'] 
			: $oDate->date_mysql2trad($aData['birthday']);
		$basics .= ' '.$aData['comment'].' '.$aData['description'];
		if (isset($aData['grp']) && !empty($aData['grp'])) { // group
			$oDb->query("SELECT title_de, title_en FROM adr_group WHERE id='".$aData['grp']."'");
			$tmp = $oDb->fetch_array();
			$basics .= ' '.$tmp['title_de'].' '.$tmp['title_en'];
		}
		
		// Adressdatensatz
		$aDataAdr = array();
		$cat = ($type == "contact") ? "p" : "c";
		$oDb->query("SELECT adr_address.*, adr_country.country AS country FROM adr_address, adr_country WHERE adr_address.country = adr_country.code AND adr_address.reference_id=".$id." AND cat='".$cat."'");
		$aData2 = $oDb->fetch_array();
		_format_search_phone($aData2['phone_tel']);
		_format_search_phone($aData2['phone_mob']);
		_format_search_phone($aData2['phone_fax']);
		$sAdr = $aData2['street'].' '.$aData2['building'].' '.$aData2['region'].' '.$aData2['town'].' '.$aData2['country'];
		$sAdr .= $aData2['po_box'].' '.$aData2['po_box_town'].' '.$aData2['recipient'];
		$sAdr .= $aData2['phone_tel'].' '.$aData2['phone_mob'].' '.$aData2['phone_fax'];
		$sAdr .= show_uri($aData2['email']).' '.show_uri($aData2['web']);
		
		// weitere Daten
		$sAdd = ' ';
		if ($type == "contact") {
			// Office 1
			$oDb->query("SELECT c.company, c.client_shortname, a.email, a.phone_mob, a.company_dep 
						FROM adr_address a, adr_contact c 
						WHERE a.company_id=c.id 
							AND a.reference_id=".$id." 
							AND a.cat='o1'");
			$tmp = $oDb->fetch_array();
			$sAdd .= $tmp['company'].' '.$tmp['email'].' '.$tmp['phone_mob'].' '.$tmp['client_shortname'].' '.$tmp['company_dep'].' ';
			// Office 2
			$oDb->query("SELECT company, c.client_shortname, a.email, a.company_dep 
						FROM adr_address a, adr_contact c 
						WHERE a.company_id=c.id 
							AND a.reference_id=".$id." 
							AND a.cat='o2'");
			$tmp = $oDb->fetch_array();
			$sAdd .= $tmp['company'].' '.$tmp['email'].' '.$tmp['phone_mob'].' '.$tmp['client_shortname'].' '.$tmp['company_dep'].' ';
		} else {
			// Mitarbeiter
			$oDb->query("SELECT c.firstname, c.surname, a.email, a.company_dep 
						FROM adr_address a, adr_contact c 
						WHERE a.company_id='".$id."' 
							AND a.reference_id=c.id	
						ORDER BY c.grp, c.surname, c.firstname"); #SELECT a.company_dep, a.company_pos, c.grp, 
			while ($tmp = $oDb->fetch_array()) {
				$sAdd .= $tmp['firstname'].' '.$tmp['surname'].' '.$tmp['email'].' '.$tmp['company_dep'].' ';
			}
		}
		
		return addslashes($name.' '.$name.' '.$sAdd.' '.$basics.' '.$sAdr);
	}
	// format phone for search
	function _format_search_phone(&$phone) {
		$aSearch = array(' ', '.', '/', '-', '(', ')', '*', '#', '+');
		$phone = str_replace($aSearch, '', $phone);
	}
	
	/*function getEmailLink($aData,$aENV,$aMSG,$aAdr,$syslang,$sSender) {

		$buffer	= '<a title="'.$aMSG['form']['opentemplate'][$syslang].'" class="textbtn" href="mailto:'.$aData['email'];
		$buffer .= '?subject='.$aAdr['cugonlinesubject'][$syslang].'&body=';
		// Vorlage laden
		// Platzhalter ersetzen
		$aTmplSearch	= array("{NAME_EMPF}", "{LOGIN_EMPF}", "{PASS_EMPF}", "{EMAIL_SENDER}");
        $aTmplReplace	= array($aData['firstname'].' '.$aData['surname'], $aData['login'], $aData['password'], $sSender);
		$sbuffer = urlencode(@str_replace($aTmplSearch, $aTmplReplace, Tools::convertFromUTF8($aAdr['sendMailText'][$syslang])));
		$sbuffer = str_replace('+', '%20', $sbuffer);
		$buffer .= $sbuffer;
		$buffer .= '"><img src="'.$aENV['path']['pix']['http'].'btn_sendmail.gif" alt="'.$aMSG['btn']['notify_users'][$syslang].'">| '.$aMSG['form']['opentemplate'][$syslang].'&nbsp;</a>';
		return $buffer; // '+' wird im E-Mail Programm nicht dekodiert!
	}*/
	
	function getEmailLink($aData,$aENV,$aMSG,$aAdr,$syslang,$sSender,$sUserLang,$aWeblang) {
		$aTmplSearch	= array("{NAME_EMPF}", "{LOGIN_EMPF}", "{PASS_EMPF}", "{EMAIL_SENDER}");
        $aTmplReplace	= array($aData['firstname'].' '.$aData['surname'], $aData['login'], $aData['password'], $sSender);
		
		$buffer = '<script type="text/javascript">
			var mailLang = "'.$sUserLang.'"	// default;			

			var aMailMsg = new Array();
		';
		foreach ($aWeblang as $key => $val) {
			$buffer .= 'aMailMsg["'.$key.'"] = "'.str_replace('+', '%20', urlencode(@str_replace($aTmplSearch, $aTmplReplace, Tools::convertFromUTF8($aAdr['sendMailText'][$key])))).'";'."\n";
		}
		$buffer .= '			
			var emailLink1 = \'<a title="'.$aMSG['form']['opentemplate'][$syslang].'" class="textbtn" href="mailto:'.$aData['email'].'?subject='.$aAdr['cugonlinesubject'][$syslang].'&body=\';
			var emailLink2 = \'"><img src="'.$aENV['path']['pix']['http'].'btn_sendmail.gif" alt="'.$aMSG['btn']['notify_users'][$syslang].'">| '.$aMSG['form']['opentemplate'][$syslang].'&nbsp;</a>\';
		
			function writeEmailLink() {
				if (document.getElementById("emailLink")) {
					document.getElementById("emailLink").innerHTML = emailLink1 + aMailMsg[mailLang] + emailLink2;
				}
			}
		</script>';
		$buffer .= '
			<span id="emailLink"></span>
		';
		$buffer .= '
			<script type="text/javascript">
				writeEmailLink();
			</script>
		';
		
		return $buffer; // '+' wird im E-Mail Programm nicht dekodiert!
	}
/**
 * echos a formatet email, depending on the config flag
 * 
 * Example:
<code>
	formatEmailString($aData, $aData2, $bEmailStringShort);
</code>
 *
 * @access   public
 * @param array $aData contains firstname & surname
 * @param array $aData2 contains the emailadress
 * @param boolean $bEmailStringShort is the config flag for short or long email
 */
	function formatEmailString($aData, $aData2, $bEmailStringShort) {
		if (empty($aData2['email'])) { return; }
		
		if ($bEmailStringShort == false) {
			$sEmail	= '';
			$sEmail	.= !empty($aData['firstname'])	? $aData['firstname']:'';
			$sEmail	.= !empty($aData['surname'])	? " ".$aData['surname']:'';
			$sEmail	= !empty($sEmail)	? $sEmail.' <'.$aData2['email'].'>':$aData2['email'];
	
			echo '<b>E</b> <a href="'.link_uri($sEmail).'">'.show_uri($aData2['email'])."</a><br>\n";
		} else {
			echo '<b>E</b> <a href="'.link_uri($aData2['email']).'">'.show_uri($aData2['email'])."</a><br>\n";
		}
	}	
// END HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////
// END SHARED *****************************************************************************
?>