<?php
/**
* adr_mailing_edit.php
*
* Detailpage: zum anlegen/editieren einer Mailingliste
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
*  @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-06-24
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_mailing_edit.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	if (isset($_GET['mailinglist'])){ $id = $oFc->make_secure_int($_GET['mailinglist']); }
	$mailinglist	= (isset($_GET['mailinglist'])) ? $oFc->make_secure_int($_GET['mailinglist']) : '';
	$sGEToptions	= ''; // id nur fuer subnavi
	
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	
// 2c. Vars:
	$sTable			= "adr_mailing_name";
	$sViewerPage	= $aENV['SELF_PATH']."adr_mailing.php";	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'];					// fuer NEW-button

// 3. DB
// DB-action: delete (remote)
	if (isset($btn['delete']) && $aData['id']) {
		
		// delete data
		$aData2delete['id']	= $aData['id']; // stammdatensatz
		$oDb->make_delete($aData2delete, $sTable);
		
		// abhaengige daten loeschen
		$aSub2delete['list_id']	= $aData['id'];
		$oDb->make_delete($aSub2delete, "adr_mailing_contact"); // address-data
		
		// weiterleitung
		header("Location: ".$sViewerPage); exit;
	}
	
// DB-action: save or update
	if (isset($btn['save']) 
	|| (isset($btn['remoteSave']) && $btn['remoteSave'] == 1)) {
		
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by']	= $Userdata['id'];
			$aUpdate['id']	= $aData['id'];
			$oDb->make_update($aData, $sTable, $aUpdate);
			
			if (isset($btn['remoteSave']) && $btn['remoteSave'] == 1) {
				header("Location: ".$sViewerPage); exit; // weiterleitung NUR bei remote-save
			}
			
		} else { // Insert eines neuen Eintrags
		
			$aData['created']		= $oDate->get_mysql_timestamp();
			$aData['created_by']	= $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			$aData['id']	= $oDb->insert_id();
			
		}
		
		header("Location: ".$aENV['PHP_SELF']."?mailinglist=".$aData['id']); exit; // doppelt-speichern-probleme durch neuladen der seite vermeiden
	}
	
// DB-action: 2. get data CONTENT
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		
		$aData		= $oDb->fetch_by_id($id, $sTable);
		$mode_key	= "edit"; // do not change!
		
	} else {
		
		$aSender	= $oDb->fetch_by_id($Userdata['office_id'], 'sys_office');
		$aData['sender']	= $aSender['adr_sender'];
		
		$mode_key	= "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("listname", $aMSG['form']['listname'][$syslang].'!'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}

	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
?>

<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="btn[remoteSave]" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<?php	if (isset($mailinglist)) { ?>
		<span class="title"><?php echo $aMSG['address']['mailing'][$syslang] ?>: <?php echo $aData['listname']; ?></span>
		<?php	} else { ?>
		<span class="title"><?php echo $aMSG['address']['settings'][$syslang] ?></span>
		<?php	} ?>
		</td>
		<td align="right">
		<a href="<?php echo $sViewerPage; ?>" title="<?php echo $aMSG['btn']['list'][$syslang]; ?>">
		<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_list.gif" alt="<?php echo $aMSG['btn']['list'][$syslang]; ?>" class="btn"></a>
	</tr>
</table>
<?php
	echo HR; 
	
	require_once ("inc.adr_mailing_detnavi.php"); 
?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['name'][$syslang]; ?> *</b></span></td>
		<td width="80%"><?php echo $oForm->textfield("listname",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['sender_line'][$syslang]; ?> *</b></span></td>
		<td width="80%"><?php echo $oForm->textfield("sender", 250, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>	
	<tr valign="top">
		<td width="20%"><span class="text"><b>Template *</b></span></td>
		<td>
			<table border="0" cellspacing="0" cellpadding="4">
				<tr>
			<?php
			
				for($i=0; !empty($aFormat[$i]['name']); $i++) {
					echo '<td><img src="'.$aENV['path']['config']['http'].$aFormat[$i]['img'].'" width="88" alt="'.$aFormat[$i]['name'].'" border="0"></td>';
					echo "<td>&nbsp;</td>\n";
				}
				
				echo '</tr>	<tr valign="top">'."\n";
				
				for($i=0; !empty($aFormat[$i]['name']); $i++) {
					
					if($aFormat[$i]['intname'] == $aData['template']) {
						$op	= 'checked';
					} else {
						$op	= '';
					}
					
					if(empty($aData['template']) && $i==0) {
						$op	= 'checked';
					}

					echo '  <td><input type="radio" name="aData[template]" id="'.$aFormat[$i]['intname'].'" value="'.$aFormat[$i]['intname'].'" class="radiocheckbox" '.$op.'>';
					echo '	<span class="text"><label for="'.$aFormat[$i]['intname'].'">'.$aFormat[$i]['name'];
					
					if(!empty($aFormat[$i]['width'])) {
						echo '<br>'.$aFormat[$i]['width'].' x '.$aFormat[$i]['height'].' mm';
					}
					
					echo '</label></span></td>'."\n";
				    echo '<td>&nbsp;</td>'."\n";
				
				} // end for 
			?>
			</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<?php 
	echo $oForm->button("SAVE"); // params: $sType[,$sClass='']
	 
	if ($mode_key == "edit") { echo $oForm->button("DELETE"); } 
	if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } 
?>
<br />

</form>
<br />

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>