<?php
/**
* browser-detection-page for ADR!
*
* checkt os/browser/version und filtert die fuer das ADR kritischen (NN4 und MacIE4.0) aus (bzw. schickt sie zur disallow-page).
* -> voll kopierbar!
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.0 / 2004-04-27
* #history	1.0
*/
// init
	require_once ("../sys/php/_include_all.php");

// browser-detection + login ok => weiterleitung
	header("Location: ".$aENV['page']['adr_welcome']); exit;

?>