<?php
/**
* adr_edit_w.php
*
* Detailpage: zum anlegen/editieren der Web-CUG Details eines Adress-Datensatzes
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Stamm-Datensatz
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$type		[company|contact] (optional)
* @param	string	$cug		[default, wenn von einer CUG-Grupper hierher verlinkt wird] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-03-03
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_edit_w.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : 'company'; // company|contact
	$cug			= (isset($_GET['cug'])) ? $oFc->make_secure_string($_GET['cug']) : ''; // cug_usergroup_id
	$sGEToptions = "?start=".$start."&type=".$type."&id=".$id; // id nur fuer subnavi
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['cug']))	{ $company1 = $_POST['cug']; } // POST ueberschreibt GET
// 2c. Vars:
	$sTable			= "cug_user";
	$sViewerPage	= $aENV['SELF_PATH']."adr_detail.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// CUG
	require_once($aENV['path']['global_service']['unix']."class.cug.php");
	$oCug =& new cug($aENV['db'], $syslang);

// Web-Lang
	require_once($aENV['path']['global_module']['unix']."class.cms_weblang.php");
	$oWeblang =& new cms_weblang(); // params: -
	
// 3. DB
// DB-action: delete contact (remote)
	if (isset($btn['remoteDelete']) && $btn['remoteDelete'] > 0) {
		_delete_contact(&$oDb, $btn['remoteDelete']);
	}
// DB-action: clear
	if (isset($btn['clear']) && $aData['id']) {
		// abhaengige daten (cug-usergroup-rel) loeschen:
		$aData2delete['rel_table'] = $sTable;
		$aData2delete['rel_id'] = $aData['id'];
		$oDb->make_delete($aData2delete, "cug_usergroup_rel");
		// stammdatensatz (cug-user) loeschen
		$oDb->make_delete($aData['id'], $sTable); // address-data
		// weiterleitung
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if ((isset($btn['save']) || (isset($btn['remoteSave']) && $btn['remoteSave'] == 1)) && !empty($aData['cug_login'])) {
		// erst checken ob login = unique (check: login + email)
		$oDb->query("SELECT id FROM ".$sTable." 
					WHERE login='".$aData['cug_login']."' 
						AND id!='".$aData['id']."'");
		$entries = $oDb->num_rows();
		if ($entries>0) { echo js_alert($aMSG['err']['login_notunique'][$syslang]); $alert=true; }
	}
	if ((isset($btn['save']) || (isset($btn['remoteSave']) && $btn['remoteSave'] == 1)) && !$alert) {
		if (!empty($aData['cug_login'])) { // mindestbedingung fuer einen datensatz!
			$aData['login'] = $aData['cug_login'];
			unset($aData['cug_login']);
			$aData['password'] = $aData['cug_password'];
			unset($aData['cug_password']);
			// usergroup zwischenspeichern, da es das feld i.d. DB nicht gibt!
			$usergroup = $aData['usergroup'];
			unset($aData['usergroup']);
			if ($aData['id']) {
				// Update eines bestehenden Eintrags
				$aData['last_mod_by'] = $Userdata['id'];
				$oDb->make_update($aData, $sTable, $aData['id']);
			} else { // Insert eines neuen Eintrags
				$aData['created'] = $oDate->get_mysql_timestamp();
				$aData['created_by'] = $Userdata['id'];
				$oDb->make_insert($aData, $sTable);
				$aData['id'] = $oDb->insert_id();
			}
			// CUG: usergroups in verknuepfungstabelle speichern (NACH insert!)
			$oCug->assignUsergroup($aData['id'], $sTable, $usergroup); // params: $sRelId, $sRelTable, $usergroup
		} // END if
		
		// ggf. weiterleitung
		if (isset($btn['remoteSave']) && $btn['remoteSave'] == 1) {
			header("Location: ".$sViewerPage); exit; // weiterleitung NUR bei remote-save
		}
		// weiterleitung overviewpage
		#header("Location: ".$sViewerPage); exit;
	}
// DB-action: get data
	if (isset($aData['contact_id'])) { $id = $aData['contact_id']; }
	if (isset($id) && $id) {
		$oDb->query("SELECT * FROM ".$sTable." WHERE contact_id='".$id."'");
		$aData = $oDb->fetch_array();
		$aData['cug_exists'] = ($oDb->num_rows() > 0) ? true : false;
		$aData['cug_password'] = $aData['password'];
		$aData['cug_login'] = $aData['login'];
		$sql = "SELECT email,firstname,surname FROM adr_address a INNER JOIN adr_contact c ON a.reference_id=c.id WHERE reference_id='$id' AND (cat='o1' OR cat='o2' OR cat='p') AND email IS NOT NULL AND email != ''";
		$oDb->query($sql);
		if($oDb->num_rows() > 0) {
			$aData['email'] = $oDb->fetch_field('email');
			$aData['firstname'] = $oDb->fetch_field('firstname');
			$aData['surname'] = $oDb->fetch_field('surname');
		}
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

	// set defaults
	if ($cug != '') { // wenn cug uebergeben wurde, entsprechende checkbox highlighten
		if (empty($aData['cug'])) {
			$aData['cug'] = $cug;
		} else {
			$aData['cug'] = $aData['cug'].','.$cug;
		}
	}
	if (empty($aData['content_lang_default'])) {
		$aData['content_lang_default'] = $oWeblang->getDefaultKey(); // params: -
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("cug_login", $aMSG['form']['login'][$syslang].'!'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>

<script language="JavaScript" type="text/javascript">
function fillPw() {
	with (document.forms['editForm']) {
		obj = elements['cug_password'];
		obj.value = '<?php echo $oCug->generatePassword();?>';
	}
}
</script>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
?><input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">
<input type="hidden" name="aData[contact_id]" value="<?php echo $id; ?>">
<input type="hidden" name="btn[remoteSave]" value="0">
<input type="hidden" name="btn[remoteDelete]" value="0">
<script>
var txt = "eins zwei";
</script>
<?php require_once ("inc.adr_detnavi.php"); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr><!-- ACHTUNG: login muss pflichtfeld bleiben (ODER unique-check aendern)! -->
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['login'][$syslang]; ?> *</b></span></td>
		<td width="80%"><?php echo $oForm->textfield("cug_login", 20,40); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr>
		<td><span class="text"><b><?php echo $aMSG['form']['password'][$syslang]; ?> *</b></span></td>
		<td><?php // Passwort (Textfeld solange noch nicht eingeloggt, danach Passwortfeld)
			if (!isset($aData['last_login']) || empty($aData['last_login'])) { 
				echo $oForm->textfield("cug_password", 20, 20); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra='']
				// Passwort-Generator Button (nur solange noch nicht eingeloggt)
				if ($oPerm->hasPriv('create')) {
					echo '<input type="button" value="'.$aMSG['btn']['random_password'][$syslang].'" onclick="fillPw()" class="smallbut">';
				}
			} else {
				echo $oForm->password("cug_password", 20,40); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra='']
			} ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['user']['content_lang_default'][$syslang]; ?></span></td>
		<td><?php	// Default-Sprachwahl
			$aWeblang = $oWeblang->getAllLanguages(); // params: -
			echo $oForm->select("content_lang_default", $aWeblang, ' onchange=mailLang=this.options[this.selectedIndex].value;writeEmailLink()'); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false] ?>
			<?php // Passwort (Textfeld solange noch nicht eingeloggt, danach Passwortfeld)
			if (!isset($aData['last_login']) || empty($aData['last_login'])) { 
				if ($mode_key == "edit" && $aData['cug_exists']) {
					if($aData['email'] != '') {
						echo getEmailLink($aData,$aENV,$aMSG,$aAdr,$syslang,$aAdr['mailsender'][$syslang],$aData['content_lang_default'],$aWeblang);
					}
					else {
						echo '<span class="text">'.$aMSG['form']['emailnotexists'][$syslang].'</span>';
					}
				}
			} ?>
		</td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['cugusergroup'][$syslang]; ?> *</b></span></td>
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr valign="top">
			<td><?php	// CUG: alle Verknuepften Usergroups ermitteln...
			$aUgRelated = $oCug->getUsergroupByUid($aData['id']); // params: $sUserId
			$aUgRelatedId = array_keys($aUgRelated);
			// alle usergroups ausgeben
			$aUG = $oCug->getAllUsergroups(); // params: -
			foreach ($aUG as $ug_id => $ug_title) {
				$checked = ((is_array($aUgRelatedId) && in_array($ug_id, $aUgRelatedId)) || $ug_id == $cug) ? ' checked' : '';
				echo '<input type="checkbox" name="aData[usergroup][]" value="'.$ug_id.'" id="chbx'.$ug_id.'" class="radiocheckbox" '.$checked.'>';
				echo '<label for="chbx'.$ug_id.'" class="text">'.$ug_title.'<br></label>';
			}
			// hinweis wenn noch keine usergroups existieren
			if (count($aUG) == 0) {
				echo '<span class="text"><a href="'.$aENV['SELF_PATH'].'web_cug.php">'.$aMSG['form']['no_cugusergroup'][$syslang].'</a><br></span>';
			} ?>
			</td>
			<?php if ($oPerm->hasPriv('admin')) { ?>
			<td align="right">
			<script language="JavaScript" type="text/javascript">
			function confirmJumpToCUG(towhere) {
					if (towhere == 'edit') {
						jumpto = 'web_cug.php';
					} else {
						jumpto = 'web_cug_detail.php';
					}
					document.forms['editForm'].elements['btn[remoteSave]'].value = 1;
					document.forms['editForm'].submit();
					// folgende zeile wird trotz submit (-> reload!) ausgefuehrt:
					location.replace('<?php echo $aENV['SELF_PATH']; ?>' + jumpto);
			}
			</script>
			<a href="#" onclick="confirmJumpToCUG('edit')" title="<?php echo $aMSG['form']['edit_cugusergroup'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['form']['edit_cugusergroup'][$syslang]; ?>">| <?php echo $aMSG['form']['edit_cugusergroup'][$syslang]; ?>&nbsp;</a>
			<a href="#" onclick="confirmJumpToCUG('new')" title="<?php echo $aMSG['form']['new_cugusergroup'][$syslang]; ?>" class="textbtn"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_add.gif" alt="<?php echo $aMSG['form']['new_cugusergroup'][$syslang]; ?>">| <?php echo $aMSG['form']['new_cugusergroup'][$syslang]; ?>&nbsp;</a>
			</td><?php } // END if ?>
			</tr></table>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['std']['last_login'][$syslang]; ?></span></td>
		<td class="sub2"><span class="text"><?php echo $oDate->datetime_mysql2trad($aData['last_login']); ?></span></td>
	</tr>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php if ($oPerm->hasPriv('delete') && $mode_key == "edit") { #echo $oForm->button("DELETE"); #onClick="return confirmClear()" ?>
<input type="submit" value="<?php echo $aMSG['btn']['clear_save'][$syslang]; ?>" name="btn[clear]" id="clear" class="but">
<?php } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>


<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>