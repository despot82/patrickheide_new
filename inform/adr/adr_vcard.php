<?php
/**
* adr_detail.php
*
* Detailpage: addresses 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	int		$type		[company|contact] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-02-18
*/
// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : '';
	$sGEToptions	= "?start=".$start;	
	
	$sTable			= "adr_contact";
// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// 3. DB
// DB-action: 1. get data for country names
	$aCountry = _get_all_countries($oDb); // von "inc.adr_functions.php"
	
// DB-action: 2. get data CONTENT
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		// set type (fallback)
		$type = (!isset($aData['company']) || empty($aData['company'])) ? "contact" : "company";
		// ggf. edit-/delete-rechte reduzieren (wenn der akt. nutzer nicht der eigentuemer ist)
		if (($oPerm->hasPriv('delete') || $oPerm->hasPriv('edit')) && !$oPerm->hasPriv('admin') && $Userdata['id'] != $aData['created_by']) {
			if ($oPerm->hasPriv('edit')) $oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
			if ($oPerm->hasPriv('delete')) $oPerm->removePriv('delete'); // params: $sPrivilege[,$sModule='']
		}
	} else {
		$mode_key = "new"; // do not change!
	}

    // include the class file
    require_once $aENV['path']['sys']['unix'].'php/PEAR/VCARD/Builder.php';

    $vcard = new Contact_Vcard_Build();
    $sFilename	= ($type=="contact") ? $aData['firstname'].''.$aData['surname']:$aData['company'];
    
    $vcard->setFormattedName($type=="contact" ? $aData['firstname'].' '.$aData['surname']:$aData['company']);
    
	if($type=='contact')
	    $vcard->setName($aData['firstname'], $aData['surname'], '', $aData['title'], '');
	 else
	 	$vcard->setName('','','','','');
	
	$vcard->addOrganization($aData['company']);

    if(!empty($aData['title']))
    	$vcard->setTitle($aData['title']);
    if(!empty($aData['birthday']))
    	$vcard->setBirthday($aData['birthday']);
 
if ($type == "contact") { 
		$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='p'");
		$aData2 = $oDb->fetch_array();
		#print_r($aData2);
		
		$vcard->addTelephone($aData2['phone_mob']);
		$vcard->addParam('TYPE', 'CELL');
		$vcard->addParam('TYPE', 'HOME');
		
		$vcard->addTelephone($aData2['phone_tel']);
		$vcard->addParam('TYPE', 'HOME');

		$vcard->addAddress($aData2['po_box'], $aData2['building'], $aData2['street'], $aData2['town'], $aData2['region'], $aData2[''], $aData2['country']);
		$vcard->addParam('TYPE', 'HOME');
		
		$vcard->addEmail($aData2['email']);
		$vcard->addParam('TYPE', 'HOME');
		$vcard->addParam('TYPE', 'PREF');
		
		// 1. OFFICE
		// personal comany-data
		$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='o1'");
		if ($oDb->num_rows()) {
			$aData3 = $oDb->fetch_array();
			
			$vcard->addTelephone($aData3['phone_mob']);
			$vcard->addParam('TYPE', 'CELL');
			
			$vcard->addTelephone($aData3['phone_tel']);
			$vcard->addParam('TYPE', 'WORK');

    		$vcard->addAddress($aData3['po_box'], $aData3['building'], $aData3['street'], $aData3['town'], $aData3['region'], $aData3[''], $aData3['country']);
    		$vcard->addParam('TYPE', 'WORK');
    		
    		$vcard->addEmail($aData3['email']);
    		$vcard->addParam('TYPE', 'WORK');
    					
			// firmenname (+client-id) mit link zu seiner detailseite
			$oDb->query("SELECT company,client_nr FROM ".$sTable." WHERE id=".$aData3['company_id']);
			$tmp = $oDb->fetch_array();
			
			// global comany-data
			$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$aData3['company_id']." AND cat='c'");
			$aData2 = $oDb->fetch_array();
			
		}
		// END 1. OFFICE 
		
		// 2. OFFICE
		// personal comany-data
		$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='o2'");
		if ($oDb->num_rows()) {
			$aData3 = $oDb->fetch_array();
			print_r($aData3);
		} // END 2. OFFICE 
		
} 
else 
{
		$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='c'");
		$aData2 = $oDb->fetch_array();

		$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
		// stamm-daten + personal office-daten ausgeben
		$aData3 = array();
		$oDb->query("	SELECT	a.reference_id,
						 		a.cat,
								a.country,
								a.phone_tel,
								a.phone_mob,
								a.phone_fax,
								a.email,
								a.web,
								a.company_dep,
								a.company_pos,
								c.id,
								c.firstname,
								c.surname,
								c.grp
						FROM	adr_address a, 
								adr_contact c 
						WHERE	a.company_id='".$id."' 
						AND		a.reference_id=c.id	
						ORDER BY c.grp, c.surname, c.firstname");
		/*while ($aData3 = $oDb->fetch_array()) {
			echo '<table width="100%" border="0" cellspacing="0" cellpadding="2"><tr valign="top"><td width="40%"><span class="text">';
			// name (verlinkt) + group/department/position
			echo '<b><a href="'.$aENV['PHP_SELF']."?id=".$aData3['id']."&start=".$start.'&type=contact">';
			if (empty($aData3['surname'])) {
				$name = $aData3['firstname'];
			} else if (empty($aData3['firstname'])) {
				$name = $aData3['surname'];
			} else {
				$name = $aData3['surname'].', '.$aData3['firstname'];
			}
			echo $name;
			echo "</a></b><br>\n";
			if (!empty($aData3['company_dep'])) echo $aData3['company_dep']."<br>\n";
			if (!empty($aData3['company_pos'])) echo $aData3['company_pos']."<br>\n";
			echo '<b>'.$aMSG['form']['group'][$syslang].':</b> '.$aGroup[$aData3['grp']].'<br>';
			echo '</span</td><td width="60%"><span class="text">';
			// phone/email/web
			if (!empty($aData3['phone_tel'])) echo "<b>T </b>".format_phone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone'])."&nbsp;\n";
			if (!empty($aData3['phone_mob'])) echo "<b>M </b>".format_phone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone'])."\n";
			if (!empty($aData3['phone_fax'])) echo "<br><b>F </b>".format_phone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone'])."\n";
			if (!empty($aData3['email'])) echo '<br><b>E </b> <a href="'.link_uri($aData3['email']).'">'.show_uri($aData3['email'])."</a>\n";
			if (!empty($aData3['web'])) echo ' <br><a href="'.link_uri($aData3['web']).'" target="_blank">'.link_uri($aData3['web'])."</a><br>\n";
			echo '</span></td></tr></table>'.HR."\n";
		}*/
		// END STAFF ?>
<?php
}
header('Content-Type: text/plain');
#header('Content-Disposition: attachment; filename="'.$sFilename.'.vcf"');
echo $vcard->fetch();
?>