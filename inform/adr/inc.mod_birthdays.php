<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mod_birthdays.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}
?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php // get birthdays of next 60 days
	$oDb->query("SELECT id, company, title, firstname, surname, birthday, DAYOFYEAR(birthday) as dayofyear 
				FROM adr_contact 
				WHERE birthday <> '0000-00-00' 
					AND birthday IS NOT NULL 
					AND DAYOFYEAR(birthday) >= (DAYOFYEAR(CURDATE()) -3) 
					AND DAYOFYEAR(birthday) < (DAYOFYEAR(CURDATE()) + 14) 
				ORDER BY MONTH(birthday), DAYOFMONTH(birthday)");
?>
	<tr><td class="th"><b><?php echo $aMSG['portal']['birthdays'][$syslang]; ?></b> <?php echo "[ 2 ".$aMSG['portal']['weeks'][$syslang]." ]";?></td></tr>
	<tr><td class="sub2"><span class="text">
<?php
	while ($aData = $oDb->fetch_array()) {
		// name / type
		if ($aData['company'] != '') { // ?
			$name = $aData['company'];
			$type = "company";
		} else {
			$name = $aData['title'].' '.$aData['firstname'].' '.$aData['surname'];
			$type = "contact";
		}
		// date
		$date = "0000-".substr($aData['birthday'], 5);
		$date = $oDate->date_mysql2trad($date);
		// geburtsjahr an namen haengen
		$year = substr($aData['birthday'], 0, 4);
		if ($year != "0000") $name .= " (".$year.")";
		// link
		$href = $aENV['path']['adr']['http'].'adr_detail.php?id='.$aData['id'].'&start=0&type='.$type;
		
		if (date("m-d") > substr($aData['birthday'], 5)) {
			// vergangenheit
			$date_name = '<small>'.$date.'</small> ';
			$date_name .= '<a href="'.$href.'">'.$name."</a><br>\n";
		} elseif (date("m-d") == substr($aData['birthday'], 5)) {
			// heute
			$date_name = '<b>'.$date.'</b>';
			$date_name .= ' <a href="'.$href.'" class="text"><b>'.$name."</b></a><br>\n";
		} else {
			// zukunft
			$date_name = ''.$date.'';
			$date_name .= ' <a href="'.$href.'" class="text">'.$name."</a><br>\n";
		}
		// output
		echo $date_name;
	}
?>
	</span></td></tr>
</table>