<?php
/**
* adr.php
*
* Overviewpage: addresses 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start			[zum richtigen zurueckspringen] (optional)
* @param	string	$searchATerm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der session
* @param	string	$searchAType	[zum filtern der Anzeige um den Contact-type (contact|company)] (optional) -> kommt ggf. aus der session
* @param	string	$orderbyA		[zum ordnen der Anzeige ] (optional) -> kommt ggf. aus der session
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	2.0 / 2006-05-03 (komplettumbau auf class.Search.php)
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");
	require_once ($aENV['path']['global_service']['unix']."class.Search.php");
	require_once ($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_contact";
	$sViewerPage	= $aENV['SELF_PATH']."adr_detail.php";	// fuer EDIT-link
	$sEditPage		= $aENV['SELF_PATH']."adr_edit_d.php";	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;				// fuer NEW-button

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// 4. DB-action: select
	// GROUPS (store in array)
	$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
	
// INIT Search Class
	$oSearch =& new Search($oDb,$aENV,'adr',$Userdata['id']);
	
	// CONTENT
	$limit = 20;	// Datensaetze pro Ausgabeseite einstellen
	if (empty($start)) {$start=0;}
	$fulltextsearch = false;
	// db-search-vars
	$searchfields				= $aENV['table']['sys_global_search'].".searchtext_mod";
	$oDb->soptions['select']	= "adr_contact.id,adr_contact.company,adr_contact.client_nr,adr_contact.client_shortname,adr_contact.firstname,adr_contact.surname,adr_contact.grp,adr_contact.comment,adr_contact.description";

	// ggf. SEARCH
	if (!empty($searchATerm)) {		
		// 2006-04-13af: FIX fuer einfache Anfuehrungszeichen im suchbegriff:
		$searchATerm = str_replace("'", "\'", $searchATerm);
		$sSearch = $oSearch->modifySearchString($searchATerm);
		
		$oSearch->select($sSearch, $start, $limit, $sOrder,true,false);
		// 1: fulltext-search
		if ($oSearch->getNumfulltext() > 0) {
			$aData = $oSearch->getSearchResult();
			$fulltextsearch = true;
		}
		else {
			$oSearch->select($sSearch, $start, $limit, 'searchtext_mod ASC',false,true);
			$aData = $oSearch->getSearchResult();
		}
		for($i=0;is_array($aData[$i]);$i++) {
			$sql = "SELECT id,company,client_nr,client_shortname,firstname,surname,grp,comment,description FROM adr_contact WHERE id='".$aData[$i]['id']."'";
			$oDb->query($sql);
			$row	= array();
			$row = $oDb->fetch_array();
			if(empty($row)) { continue; }
			foreach($row as $field => $value) {
				$aData[$i][$field] = $value;
			}
		}
		$entries = $oSearch->getEntries();
	} else {
	// wenn KEINE SEARCH
		$orderby = (empty($orderbyA)) ? $aENV['table']['sys_global_search'].".searchtext_mod ASC" : $orderbyA;
		// '.$oDb->soptions['extra'].'
		$sql = 'SELECT '.$oDb->soptions['select'].' FROM adr_contact INNER JOIN '.$aENV['table']['sys_global_search'].' ON adr_contact.id='.$aENV['table']['sys_global_search'].'.ref_id WHERE '.$aENV['table']['sys_global_search'].'.module=\'adr\' ORDER BY '.$orderby;

		$oDb->query($sql);
		$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
		if ($entries > $limit) { $oDb->limit($sql, $start, $limit); }
		for($i=0;$aData[$i] = $oDb->fetch_array();$i++);
		$last = count($aData);
		unset($aData[($last-1)]);
	}
	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="middle">
		<td><span class="title"><?php echo $aMSG['address']['headline'][$syslang] ?></span></td>
		
		<td align="right"><span class="text">
		<?php	// NEW-BUTTONS
		if ($oPerm->hasPriv('create')) {
			echo '<a href="'.$aENV['SELF_PATH'].'adr_edit_d.php?type=contact" title="'.$aMSG['btn']['new_contact'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_contact_new.gif" alt="'.$aMSG['btn']['new_contact'][$syslang].'" class="btn"></a>';
			echo '<a href="'.$aENV['SELF_PATH'].'adr_edit_d.php?type=company" title="'.$aMSG['btn']['new_company'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_company_new.gif" alt="'.$aMSG['btn']['new_company'][$syslang].'" class="btn"></a>';
		} // END if ?>
		</span></td>
	</tr>
</table>

<?php echo HR ?>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
			<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></span></td>
			<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks(); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></span>
		</td>
	</tr>
</table>

<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="10" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr height="20">
<?php // bei fulltextsearch -> neue Spalte fuer Sortierung nach RELEVANZ!
		if ($fulltextsearch == true) {  ?>
		<th width="5%"><?php echo _sortTHfulltext($aMSG['form']['relevance'][$syslang]); ?></th>
<?php	} // END if ($fulltextsearch) ?>
		<th width="40%"><?php echo _sortTH($aMSG['form']['name'][$syslang], $aENV['table']['sys_global_search'].'.searchtext ASC'); ?></th>
		<?php if ($searchAType!="company") {  ?><th width="15%"><?php echo _sortTH($aMSG['form']['group'][$syslang], 'grp ASC,'.$aENV['table']['sys_global_search'].'.searchtext ASC'); ?></th><?php }  ?>
		<th width="38%"><?php echo _sortTH($aMSG['form']['description'][$syslang], 'description ASC,'.$aENV['table']['sys_global_search'].'.searchtext ASC'); ?></th>
		<?php if ($oPerm->hasPriv('edit')) { ?><th width="7%">&nbsp;</th><?php } ?>
	</tr>
<?php

	for($i=0;is_array($aData[$i]);$i++) {
		if ($aData[$i]['company'] == '') {
			if (empty($aData[$i]['surname']) && !empty($aData[$i]['firstname'])) {
				$name = $aData[$i]['firstname'];
			} elseif (empty($aData[$i]['firstname']) && !empty($aData[$i]['surname'])) {
				$name = $aData[$i]['surname'];
			} else {
				$name = $aData[$i]['surname'].', '.$aData[$i]['firstname'];
			}
			$type = 'contact';
			
			// ermittle o1 / o2 dieser kontakt(person)
			$sQuerySub = "SELECT a.company_id, c.company FROM adr_address a, adr_contact c WHERE a.company_id=c.id AND a.reference_id='".$aData[$i]['id']."'";
			$aCompany = array();
			$oDb->query($sQuerySub);
			while ($aData2 = $oDb->fetch_array()) {
				$aCompany[$aData2['company_id']] = $aData2['company'];#
			}
		} else {
			$name = nl2br_cms($aData[$i]['company']);
			$type = 'company';
		}
		$sGEToptions = "?id=".$aData[$i]['id']."&start=".$start."&type=".$type;
		
		// bei genau einem suchtreffer sofort auf die detailseite weiterleiten
		if ($entries == 1) {
			echo '<script language="JavaScript" type="text/javascript">document.location.href=\''.$sViewerPage.$sGEToptions.'&clearsearch=true'.'\'</script>';
		}
?>
	<tr valign="top" height="20">
		<?php if ($fulltextsearch == true) { // bei fulltextsearch -> neue Spalte fuer Sortierung nach RELEVANZ!  ?>
		<td align="center"><span class="text"><?php echo ($start + ($i+1));#$aData['relevance'] ?></span></th>
		<?php } // END if ($fulltextsearch) ?>

		<td><span class="text"><a href="<?php echo $sViewerPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><?php // name
			echo '<b>'.shorten_text($name, 45).'</b>'; ?></a></span>
		</td>
		
		<?php if ($searchAType != "company") { ?>
		<td class="sub2"><span class="text"><?php echo ($aData[$i]['grp']) ? $aGroup[$aData[$i]['grp']] : '-'; ?></span></td>
		<?php }  ?>
		
		<td class="sub2"><span class="text"><?php echo shorten_text($aData[$i]['description'], 40); ?>&nbsp;</span></td>
		
		<?php if ($oPerm->hasPriv('edit')) { ?>
		<td align="right"><p><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php 
			$sEdit = $aMSG['btn']['edit_mode'][$syslang].' ';
			$sEdit .= ($type == 'company') ? $aMSG['address']['company'][$syslang] : $aMSG['address']['contact'][$syslang];
			echo $sEdit; ?>" class="btn"></a>
		</p></td>
		<?php } ?>
		
	</tr>
	
	<?php if ($type == "contact" && (empty($aData[$i]['company']) && count($aCompany))) { ?>
	<tr>
		<?php if ($fulltextsearch == true) { ?><td class="off"><span class="text"></span></th><?php } // END if ($fulltextsearch) ?>
		<td colspan="4" class="sub2">
			<?php // bei contact -> zeige zugehoerige companies
			if (empty($aData[$i]['company']) && count($aCompany)) {
				foreach ($aCompany as $k => $v) {
					echo '<span class="navi"><a href="'.$sViewerPage.'?id='.$k.'&start='.$start.'&type=company">'.$v."</a></span><br>";
				}
			} ?>
		</td>
	</tr>
	<?php }  ?>

	<tr><td colspan="4" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
<?php } // END while
	if ($entries == 0) { echo '<tr><td colspan="4" align="center"><span class="text">'.$aMSG['std']['no_entries'][$syslang].'</span></td></tr>'; }
?>
</table>

<br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
			<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></span></td>
			<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks(); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></span>
		</td>
	</tr>
</table>
<br>

<?php // hilfsfunktion
function _sortTH($sTitle, $sOrderby='') {
	if (!$sTitle) return;
	global $orderbyA; // _GET / _SESSION
	global $aENV;
	global $fulltextsearch;
	if (empty($orderbyA) && $fulltextsearch == false) $orderbyA = $aENV['table']['sys_global_search'].".searchtext_mod ASC";
	$img = '';
	// make vars
	$olddir = (strstr($orderbyA, "DESC")) ? "DESC" : "ASC";
	$newdir = (strstr($orderbyA, "DESC")) ? "ASC" : "DESC";
	$sCompareOrderby = str_replace($newdir, $olddir, $sOrderby);
	if (str_replace($olddir, '', $sCompareOrderby) == str_replace($olddir, '', urldecode($orderbyA))) {
		$sOrderby = str_replace($olddir, $newdir, $sOrderby);
	}
	$imgFile = ($newdir == "DESC") ? "pf_down" : "pf_up";
	if (isset($orderbyA) && urldecode($orderbyA) == $sCompareOrderby) {
		$img = '<img src="'.$aENV['path']['pix']['http'].$imgFile.'.gif" width="9" height="9" border="0" alt="'.$newdir.'">';
	}
	// output
	return '<nobr><a href="'.$aENV['PHP_SELF'].'?orderbyA='.$sOrderby.'">'.$sTitle.'</a> '.$img.'</nobr>';
}
function _sortTHfulltext($sTitle) {
	if (!$sTitle) return;
	global $orderbyA; // _GET / _SESSION
	global $aENV;
	$img = '';
	// make vars
	if (!isset($orderbyA) || empty($orderbyA)) {
		$img = '<img src="'.$aENV['path']['pix']['http'].'pf_down.gif" width="9" height="9" border="0" alt="'.$sTitle.'">';
	}
	// output
	return '<nobr><a href="'.$aENV['PHP_SELF'].'?orderbyA=">'.$sTitle.'</a>&nbsp;'.$img.'</nobr>';
} ?>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
