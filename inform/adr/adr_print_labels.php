<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_print_labels.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2. GET-params abholen
	$mailinglist	= (isset($_GET['mailinglist'])) ? $oFc->make_secure_int($_GET['mailinglist']) : '';

	include($aENV['path']['global_module']['unix']."class.AdrLabelPdfPrinter.php");
	$pdf =& new AdrLabelPdfPrinter($aENV);

	$pdf->setODb($oDb);
	$pdf->setFontsize($aFormat['fontabwidth'],$aFormat['fontemwidth']);
	$pdf->setFontfamily($aFormat['font']);
	$pdf->setFormate($aFormat);
	$pdf->setMailinglist($mailinglist);
	$pdf->setSender();
	$pdf->initialize();
	$pdf->printPdf();

	header('Content-type: application/pdf');
	header('Content-Disposition: inline; filename='.$pdf->getSavename().'');
	header("Location: ".$aENV['path']['adr_data']['http'].$pdf->getSavename());
?>