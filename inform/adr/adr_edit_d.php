<?php
/**
* adr_edit_d.php
*
* Detailpage: zum anlegen/editieren der Details eines Adress-Stammdatensatzes
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$type		[company|contact] (optional)
* @param	string	$company1	[wenn ein neuer Mitarbeiter erstellt wird] (optional)
* @param	string	$contact	[wenn ein neues Office erstellt wird] (optional)
* @param	string	$office		[wenn ein neues Office erstellt wird] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2004-11-30
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_edit_d.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : 'company'; // company|contact
	$company1		= (isset($_GET['company1'])) ? $oFc->make_secure_string($_GET['company1']) : ''; // company_id bei neuem Mitarbeiter
	$contact		= (isset($_GET['contact'])) ? $oFc->make_secure_string($_GET['contact']) : ''; // contact_id bei neuem Office
	$office			= (isset($_GET['office'])) ? $oFc->make_secure_string($_GET['office']) : ''; // office_type [o1|o2] bei neuem Office
	$sGEToptions = "?start=".$start."&type=".$type."&id=".$id; // id nur fuer subnavi
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['company1']))	{ $company1 = $_POST['company1']; } // POST ueberschreibt GET
	if (isset($_POST['contact']))	{ $contact = $_POST['contact']; } // POST ueberschreibt GET
	if (isset($_POST['office']))	{ $office = $_POST['office']; } // POST ueberschreibt GET
// 2c. Vars:
	$sTable			= "adr_contact";
	$sViewerPage	= $aENV['SELF_PATH']."adr_detail.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button

// SHARED BEGIN ****************************************************************************
	require_once ("./inc.adr_functions.php");
// END SHARED *****************************************************************************

// 3. DB
// DB-action: delete contact (remote)
	if (isset($btn['remoteDelete']) && $btn['remoteDelete'] > 0) {
		_delete_contact(&$oDb, $btn['remoteDelete']);
	}
// DB-action: delete contact (form-button)
	if (isset($btn['delete']) && isset($aData['id'])) {
		_delete_contact(&$oDb, $aData['id']); // stammdatensatz-ID
	}
// DB-action: save or update
	if (isset($btn['save']) || (isset($btn['remoteSave']) && $btn['remoteSave'] == 1)) {
		if ($type == 'contact') { // wenn stammdaten einer person editiert werden
			$_POST['bday_y'] = (!isset($_POST['bday_y']) || empty($_POST['bday_y'])) ? "0000" : $_POST['bday_y']; // wenn leer -> mit 0000 auffuellen!
			$_POST['bday_y'] = (strlen($_POST['bday_y']) < 4) ? "19".$_POST['bday_y'] : $_POST['bday_y']; // 4-stelliges jahr!
			$aData['birthday'] = $_POST['bday_y']."-".$_POST['bday_m']."-".$_POST['bday_d']; // merge bday-dropdown into a valid mysql-date 
		}
		if (!empty($aData['client_nr'])) { // wenn eine client_nr eingegeben wurde
			// wenn kein shortname eingegeben wurde, den companynamen bzw. nachnamen nehmen (da bei PRJ NUR NOCH nach shortname sortiert wird!)
			if (empty($aData['client_shortname'])) {
				if ($type == 'company') $aData['client_shortname'] = shorten_text($aData['company'], 23);
				if ($type == 'contact') $aData['client_shortname'] = shorten_text($aData['surname'], 23);
			}
		}
		if ($aData['id']) {
			// Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$aUpdate['id'] = $aData['id'];
			$oDb->make_update($aData, $sTable, $aUpdate);
			// update SEARCH-field
			adr_save_searchdata($oDb, $aData['id']);
			// ggf. weiterleitung
			if (isset($btn['remoteSave']) && $btn['remoteSave'] == 1) {
				header("Location: ".$sViewerPage); exit; // weiterleitung NUR bei remote-save
			}
		} else { // Insert eines neuen Eintrags
			// check, ob dieser Kontakt (exakt genauso geschrieben) schon existiert
			$sql	= "SELECT 
					   *
					   FROM adr_contact 
					   WHERE 
					   (company = '".$aData['company']."' AND company != NULL )
					   OR (firstname='".$aData['firstname']."'
					   AND surname='".$aData['surname']."')"; //company empty fix @Jul/2008 ck
					   
			$oDb->query($sql);
			$sAddInsertGetParam = ($oDb->num_rows()) ? '&contact_exists=true' : '';
			
			// insert
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id();
			if (isset($company1) && !empty($company1) && !empty($aData['id'])) {
			// bei neuem Mitarbeiter
				$aInsert = array();
				$aInsert['reference_id'] = $aData['id'];
				$aInsert['company_id'] = $company1;
				$aInsert['cat'] = "o1";
				$aInsert['created'] = $oDate->get_mysql_timestamp();
				$aInsert['created_by'] = $Userdata['id'];
				$oDb->make_insert($aInsert, "adr_address");
			}
			if (isset($contact) && !empty($contact) && isset($office) && !empty($office) && !empty($aData['id'])) {
			// bei neuem Office
				// erst entscheiden od update oder insert?
				$oDb->query("SELECT id FROM adr_address WHERE reference_id='".$contact."' AND cat='".$office."'");
				if ($oDb->num_rows()) {
					// neue company_id im vorhandenen datensatz updaten
					$aInsert = array();
					$aInsert['company_id'] = $aData['id'];
					$aUpdate = array();
					$aUpdate['reference_id'] = $contact;
					$aUpdate['cat'] = $office;
					$oDb->make_update($aInsert, "adr_address", $aUpdate);
				} else {
					// neuen datensatz inserten
					$aInsert = array();
					$aInsert['reference_id'] = $contact;
					$aInsert['cat'] = $office;
					$aInsert['company_id'] = $aData['id'];
					$aInsert['created'] = $oDate->get_mysql_timestamp();
					$aInsert['created_by'] = $Userdata['id'];
					$oDb->make_insert($aInsert, "adr_address");
				}
			}
			// update SEARCH-field
			adr_save_searchdata($oDb, $aData['id']);
			// weiterleitung
			header("Location: ".$sNewPage."&id=".$aData['id'].$sAddInsertGetParam); exit; // doppelt-speichern-probleme durch neuladen der seite vermeiden
		}
	}
// DB-action: get data
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		#if ($aData['company']=='') {$type = "contact";} else {$type = "company";}
		if ($aData['client_nr'] == '000') $aData['client_nr'] = '';
	} else {
		$mode_key = "new"; // do not change!
		// set defaults
		if ($company1 != '') { $aData['company1'] = $company1; } // wenn company_id uebergeben wurde, erstes dropdown korrekt highlighten
		$aData['grp'] = '40'; // default fuer group-dropdown
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	if ($type == "company") {
		#echo "<!-- company -->"; // DEBUG
		$oForm->check_field("company", $aMSG['form']['company'][$syslang].'!'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	} else {
		#echo "<!-- contact -->"; // DEBUG
		$sSonderJScheck = "
		gender = elements['aData[gender]']; // es wird erwartet, dass gender ein ARRAY ist!
		checked = 0;
		for (anz=0; anz < gender.length; anz++){
			if (gender[anz].checked) { checked = 1; }
		}
		if (checked != 1) {ok=false;m=' ".$aMSG['err']['part_insert'][$syslang].$aMSG['form']['gender'][$syslang]."! ';mfocus=gender[0];}
		if (elements['aData[firstname]'].value=='' && elements['aData[surname]'].value=='') {ok=false;m=' ".$aMSG['err']['names_missing'][$syslang]."';mfocus=elements['aData[firstname]'];}
		";
		$oForm->add_js($sSonderJScheck); // params: $sJScode
	}
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}

	// wenn beim Insert ein gleichnamiger Kontakt gefunden wurde, dann beim neuladen der seite:
	if ($_GET['contact_exists'] == 'true') {
		echo js_alert($aMSG['err']['contact_exists'][$syslang]); // Meldung ausgeben
	}
?>


<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
?><input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<?php // wenn ein neuer mitarbeiter angelegt wird
if ($type == "contact" && $mode_key == "new" && !empty($company1)) {
	echo '<input type="hidden" name="company1" value="'.$company1.'">';
} ?>
<?php // wenn ein neues office angelegt wird
if ($type == "company" && $mode_key == "new" && !empty($contact) && !empty($office)) {
	echo '<input type="hidden" name="contact" value="'.$contact.'">';
	echo '<input type="hidden" name="office" value="'.$office.'">';
} ?>
<input type="hidden" name="btn[remoteSave]" value="0">
<input type="hidden" name="btn[remoteDelete]" value="0">

<?php require_once ("inc.adr_detnavi.php"); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php // DETAILS COMPANY ################################################################################
if ($type == "company") { ?>
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['company'][$syslang]; ?> *</b></span></td>
		<td width="80%"><?php echo $oForm->textarea("company", 2, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['description'][$syslang]; ?><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></span></td>
		<td><?php echo $oForm->textarea("description", 2, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['comment'][$syslang]; ?><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></span></td>
		<td><?php echo $oForm->textarea("comment", 5, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
		<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['group'][$syslang]; ?> *</b></span></td>
		<td><?php // Group dropdown -> default: Contact!
		$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
		echo $oForm->select("grp", $aGroup); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false] ?>
		</td>
	</tr>
	
<?php // DETAILS CONTACT ################################################################################
} else { ?>
	<tr valign="top">
		<td width="20%"><span class="text"><b>*</b></span></td>
		<td width="80%"><span class="text">
			<input type="radio" name="aData[gender]" value="m" <?php if ($aData['gender']=="m") {echo "checked";} ?> title="<?php echo $aMSG['form']['gender_m'][$syslang]; ?>" class="radiocheckbox"><img src="<?php echo $aENV['path']['pix']['http']; ?>m.gif" width="15" height="16" alt="<?php echo $aMSG['form']['gender_m'][$syslang]; ?>" border="0">&nbsp;&nbsp;
			<input type="radio" name="aData[gender]" value="f" <?php if ($aData['gender']=="f") {echo "checked";} ?> title="<?php echo $aMSG['form']['gender_f'][$syslang]; ?>" class="radiocheckbox"><img src="<?php echo $aENV['path']['pix']['http']; ?>f.gif" width="15" height="16" alt="<?php echo $aMSG['form']['gender_f'][$syslang]; ?>" border="0">
		</span></td>
	</tr>
	
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['firstname'][$syslang]; ?></b></span></td>
		<td><?php echo $oForm->textfield("firstname", 250, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['surname'][$syslang]; ?></b></span></td>
		<td><?php echo $oForm->textfield("surname", 250, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['academic_title'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("title", 250, 40); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['birthday'][$syslang]; ?></span></td>
		<td>
		<?php if (!$oPerm->hasPriv('edit')) { ?>
		<?php	echo $oDate->date_mysql2trad($aData['birthday']); ?>
		<?php } else { ?>
		<select name="bday_d" size="1" class="smallForm">
		<option value="">--</option>
		<?php	for ($i=1; $i<32; $i++) {
					if ($i == substr($aData['birthday'], 8)) { $sel=' selected'; } else { $sel=''; }
					if ($i < 10) { $i = '0'.$i; }
					echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
				} ?>
		</select>
		<select name="bday_m" size="1" class="smallForm">
		<option value="">--</option>
		<?php	for ($i=1; $i<13; $i++) {
					if ($i == substr($aData['birthday'],5,2)) { $sel=' selected'; } else { $sel=''; }
					if ($i < 10) { $i = '0'.$i; }
					echo '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
				} ?>
		</select>
		<input type="text" name="bday_y" value="<?php
		if (!empty($aData['birthday'])) { $val = substr($aData['birthday'], 0, 4); } else { $val=''; }
		if ($val == '0000') { $val=''; }
		echo $val; ?>" size="5" maxlength="4">
		<?php } // END if ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['group'][$syslang]; ?> *</b></span></td>
		<td><?php // Group dropdown -> default: Contact!
		$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
		echo $oForm->select("grp", $aGroup); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['description'][$syslang]; ?><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></span></td>
		<td><?php echo $oForm->textarea("description", 2, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['comment'][$syslang]; ?><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></span></td>
		<td><?php echo $oForm->textarea("comment", 5, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
<?php
} // END DETAILS ################################################################################ ?>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
<?php if (isset($aENV['module']['adm']) && $oPerm->hasPriv('view', 'adm')) { ?>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['client_nr'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("client_nr", 5, 7); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		<?php // client_nr Vorschlag machen
		if (empty($aData['client_nr'])) { // nur wenn noch keine vergeben wurde
			$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL) 
			$aOffice = $oUser->getAllOffices(); // params: [$bWithUsers=false] 
			$nCountOffices = count($aOffice);
			foreach ($aOffice as $key => $val) {
				if ($nCountOffices == 1) $val['name'] = ''; // bei nur einem Office -> keinen Namen ausgeben
				echo clientNrSuggestion($oDb, $val['name'], $val['client_nr_startvalue'], $val['client_nr_endvalue']);
			}
		} // END client_nr Vorschlag ?>
		</td>
	</tr>
<?php } ?>
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['client_shortname'][$syslang]; ?></span></td>
		<td><?php echo $oForm->textfield("client_shortname", 25, 40); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php
// Contact could not be deleted -> alert at end of page to assure proper page display
	if ($bDelError == true) {
		echo '<script type="text/javascript">alert("'.$aMSG['err']['project_assigned'][$syslang].'");</script>';
	}
?>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); 

// HILFSFUNKTIONEN /////////////////////////////////////////////////////////////////////
	function clientNrSuggestion(&$oDb, $o_name, $o_startvalue, $o_endvalue) {
		global $aMSG, $syslang;
		$oDb->query('SELECT MAX(client_nr) as max_client_nr 
					FROM adr_contact 
					WHERE client_nr >= '.$o_startvalue.' 
						AND client_nr < '.$o_endvalue); // bisher hoechste client_nr ermitteln...
		$new_client_nr = ($oDb->fetch_field('max_client_nr') + 1); // ... und eins dazu zaehlen
		// ggf. fuehrende nullen voranstellen
		if ($new_client_nr >= 10 && $new_client_nr < 100) $new_client_nr = '0'.$new_client_nr;
		if ($new_client_nr < 10) $new_client_nr = '00'.$new_client_nr;
		// check limit
		if ($new_client_nr >= $o_endvalue) echo js_alert('client_nr range limit is exceeded for '.$o_name.'! No new client number available.');
		// html button
		$office = (!empty($o_name)) ? ' ('.$o_name.')' : '';
		return '<input type="button" value="'.$aMSG['btn']['next_client_nr'][$syslang].$office.'" class="smallbut" onclick="'."document.forms['editForm'].elements['aData[client_nr]'].value='".$new_client_nr."'\">\n";
	}

?>