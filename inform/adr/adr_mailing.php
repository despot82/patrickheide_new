<?php
/**
* adr_mailing.php
*
* Overviewpage: Mailinglisten 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-06-24
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './adr_mailing.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_mailing_name";
	$sEditPage		= $aENV['SELF_PATH']."adr_mailing_edit.php";	// fuer EDIT-link
	$sListPage		= $aENV['SELF_PATH']."adr_mailing_list.php";	// fuer LISTVIEW-link
	
	if ($oPerm->hasPriv('edit')) {
		$sFilter		= "";	// fuer LISTVIEW-link
	} else {
		$sFilter		= "&listview=in&searchATerm=";	// fuer LISTVIEW-link
	} 
	
	$sNewPage		= $sEditPage.$sGEToptions;						// fuer NEW-button
	$sDeletePage	= $aENV['SELF_PATH']."adr_mailing_delete.php";	// fuer "delete"-link
	$sPrintLink		= $aENV['SELF_PATH']."adr_print_labels.php";	// fuer PRINT-link
	$sExcelLink		= $aENV['SELF_PATH']."adr_excel_list.php";		// fuer EXCEL-link
// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// CONTENT
	$sQuery = "SELECT id, listname FROM ".$sTable." ORDER BY listname ASC";
	$oDb->query($sQuery);
	$entries = floor($oDb->num_rows());	// noetig fuer DB-Navi und PRIO!
	if (empty($start)) { $start=0; } // noetig fuer DB-Navi und PRIO!
	$counter = 0; // zaehler

?>

<script language="JavaScript" type="text/javascript">
function confirmDelete() {
	var chk = window.confirm('<?php echo $aMSG['err']['delete_rec'][$syslang]; ?>');
	return(chk);
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<span class="title"><?php echo $aMSG['topnavi']['mailings'][$syslang]; ?></span>
		</td>
		<td align="right">
		<?php if ($oPerm->hasPriv('create')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"] ?>
		</td>
	</tr>
</table><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr height="20">
		<th width="80%"><?php echo $aMSG['form']['name'][$syslang]; ?></th>
		<th width="20%">&nbsp;</th>
	</tr>
<?php	while ($aData = $oDb->fetch_array()) {
			#$sGETvars = "?id=".$aData['id']."&mailinglist=".$aData['id'];
			$sGETvars = "?mailinglist=".$aData['id'];
			$counter++; // zaehle hoch
?>
	<tr valign="top" height="20">
		<td><span class="text"><a href="<?php echo $sListPage.$sGETvars.$sFilter; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>">
			<?php echo $aData['listname']; ?></a></span>
		</td>
		<td align="right" nowrap><p>
			<?php if ($oPerm->hasPriv('edit')) { ?>
			<a href="<?php echo $sEditPage.$sGETvars; ?>" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php $aMSG['std']['edit'][$syslang]; ?>" class="btn"></a>
			<?php } ?>
			<a href="<?php echo $sPrintLink; ?>?mailinglist=<?php echo $aData['id']; ?>" class="cnavi<?php if($sBasename == "adr_mailing_list_printview") echo "hi"; ?>" target="_blank" title="<?php echo $aMSG['btn']['printview'][$syslang]; ?>">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_print.gif" alt="<?php echo $aMSG['btn']['printview'][$syslang]; ?>" class="btn"></a>
			<a href="<?php echo $sExcelLink; ?>?mailinglist=<?php echo $aData['id']; ?>" class="cnavi<?php if($sBasename == "adr_mailing_list_printview") echo "hi"; ?>" title="<?php echo $aMSG['btn']['exceldwn'][$syslang]; ?>">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_dwn.gif" alt="<?php echo $aMSG['btn']['exceldwn'][$syslang]; ?>" class="btn"></a>
			<?php if ($oPerm->hasPriv('delete')) { ?>
			<a href="<?php echo $sDeletePage.'?deleteid='.$aData['id']; ?>" onClick="return confirmDelete()" title="<?php echo $aMSG['btn']['delete'][$syslang]; ?>">
			<img src="<?php echo $aENV['path']['pix']['http']; ?>btn_delete.gif" alt="<?php $aMSG['btn']['delete'][$syslang]; ?>" class="btn"></a>
			<?php } ?>
		</p></td>
	</tr>
	
<?php	} // END while
	if ($entries == 0) { echo '<tr><td colspan="2" align="center"><span class="text">'.$aMSG['std']['no_entries'][$syslang].'</span></td></tr>'; }
?>
</table>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
