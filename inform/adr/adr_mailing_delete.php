<?php
/**
* adr_mailing_delete.php
*
* Specialpage: loeschen einer Mailingliste (NUR SCRIPT - KEINE AUSGABE!)
*
* @param	int		$deleteid	welches archiv v.d. viewpage aus geloescht werden soll
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$referrer	[zum richtigen zurueckspringen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	int		$slot		-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.0 / 2004-05-04 (new_intranet)
*/

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$deleteid		= (isset($_GET['deleteid'])) ? $oFc->make_secure_int($_GET['deleteid']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$referrer		= (isset($_GET['referrer'])) ? $oFc->make_secure_string($_GET['referrer']) : 'adr_mailing.php';
	$sGEToptions	= "?start=".$start;
// 2b. POST-params abholen
// 2c. Vars:
	$sViewerPage	= $aENV['SELF_PATH'].$referrer.$sGEToptions;	// fuer BACK

// 3. DB
// DB-action: delete

	if (!empty($deleteid)) { // from listview
		// delete data
		$aData2delete['id'] = $deleteid; // stammdatensatz
		$oFile =& new filesystem();

		for($i=0;!empty($aFormat[$i]['intname']);$i++) {
			$oFile->delete_files($aENV['path']['adr_data']['unix']."Labels".$aFormat[$i]['intname'].$deleteid.".pdf");
		}

		$oDb->make_delete($aData2delete, "adr_mailing_name");
		// abhaengige daten loeschen
		$aSub2delete['list_id'] = $deleteid;
		$oDb->make_delete($aSub2delete, "adr_mailing_contact"); // address-data
		// weiterleitung
		header("Location: ".$sViewerPage); exit;
	}
?>