<?php
/**
* web_cug_detail.php
*
* Detailpage: CUG-usergroups (also alle, die ein Web-System (CMS, Clientsites,...) nutzen wollen)
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz (optional)
* @param	int		$user_id	welcher Nutzer direkt mit dieser (neuen) UG verknuepft werden soll (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$aENV		-> kommt aus der 'inc.sys_login.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2006-03-02 (layout wie sys_usergroup + rechte auf ADR umgestellt)
* #history	1.0 / 2005-03-03
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './web_cug_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$user_id		= (isset($_GET['user_id'])) ? $oFc->make_secure_int($_GET['user_id']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars:
	$sTable			= "cug_usergroup";
	$sViewerPage	= $aENV['SELF_PATH'].'web_cug.php'; 	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;		// fuer NEW-button

// OBJECTS
	// CUG
	require_once($aENV['path']['global_service']['unix']."class.cug.php");
	$oCug =& new cug($aENV['db'], $syslang);
	// LANG
	require_once($aENV['path']['global_module']['unix']."class.cms_weblang.php");
	$oWeblang =& new cms_weblang($weblang);

// 3. DB
// DB-action: check delete
	if (isset($btn['delete']) && $aData['id'] && !isset($aData['newCugId'])) {
		// check (nur!) ob user zu dieser usergroup gelinkt sind
		$alert = $oCug->hasUsergroupUser($aData['id']);
	}
// DB-action: delete
	if ( (isset($btn['delete']) && $aData['id'] && !$alert)
		|| (isset($btn['continue_delete']) && $aData['id'])	) {
		// make delete
		$oCug->deleteUsergroup($aData['id']); // params: $sUsergroupId
		// weiterleitung overviewpage
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save
	if (isset($btn['save']) || isset($btn['save_copy'])) {
		// erst checken ob title = unique
		if (!$oCug->isTitelUnique($aData, $oWeblang->getAllKeys())) {
			echo js_alert($aMSG['err']['title_notunique'][$syslang]);
			$alert=true;
		}
	}
// DB-action: insert or update
	if (isset($btn['save']) && !isset($alert)) {
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$aUpdate['id'] = $aData['id'];
			$oDb->make_update($aData, $sTable, $aUpdate);
		} else { // Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			$oDb->make_insert($aData, $sTable);
			#$aData['id'] = $oDb->insert_id();
		}
		// weiterleitung overviewpage
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save_copy
	if (isset($btn['save_copy']) && !isset($alert)) {
		// Insert eines neuen Eintrags
		$copy_id = $aData['id'];
		unset($aData['id']);
		$aData['created'] = $oDate->get_mysql_timestamp();
		$aData['created_by'] = $Userdata['id'];
		$oDb->make_insert($aData, $sTable);
		// Kopiere alle Verknuepfungen die die Vorlage besitzt auf die neue kopierte Table
		$aNewData['usergroup_id'] = $oDb->insert_id();
		$oDb2 =& new dbconnect($aENV['db']);
		// alle Verknuepfungen der Vorlage ermitteln...
		$oDb2->query("SELECT rel_id, rel_table FROM cug_usergroup_rel WHERE usergroup_id='".$copy_id."'");
		while ($tmp = $oDb2->fetch_array()) {
		// ....diese auch mit der neuen usergroup verknuepfen
			$aNewData['rel_table'] = $tmp['rel_table'];
			$aNewData['rel_id'] = $tmp['rel_id'];
			$oDb->make_insert($aNewData, "cug_usergroup_rel");
		}
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		// wenn "copy" geklickt wurde
		if (isset($btn['copy'])) {
			// title (fuer jede ausgabesprache) auffaellig veraendern!
			foreach ($oWeblang->getAllKeys() as $k) {
				$aData['title_'.$k] = $aMSG['form']['copy_of'][$k].$aData['title_'.$k];
			}
			$mode_key = "copy"; // do not change!
		}
	} else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");


// ANSICHT: wenn zu loeschende gruppe noch user beinhaltet -------------------------------------------------
if (isset($btn['delete']) && $alert) {
	
	// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">

<span class="title"><?php echo $aMSG['topnavi']['cug'][$syslang]; ?></span>
<span class="text"><?php echo $aMSG['mode']['delete'][$syslang]; ?></span>
<br><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['err']['delete_usergroup'][$syslang]; ?></span></td>
	</tr>
</table>
<br>
<?php echo '<input type="submit" value="'.$aMSG['btn']['continue_delete'][$syslang].'" name="btn[continue_delete]" id="continue_delete" class="but">' ?>
<?php echo $oForm->button("CANCEL"); ?>

<?php // ANSICHT: Form -----------------------------------------------------------------------------------
} else {
	// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	foreach ($oWeblang->getAllKeys() as $k) {
		$oForm->check_field("title_".$k, $aMSG['form']['title'][$syslang].' ('.$k.')'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	}
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
	// JavaScripts + oeffnendes Form-Tag
	echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">
<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['topnavi']['cug'][$syslang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td align="right"><?php // Buttons 
			echo $oForm->button("BACK"); // params: $sType[,$sClass='']
			echo $oForm->button("NEW"); // params: $sType[,$sClass='']
		?></td>
	</tr>
</table>
<br>

<?php if (isset($btn['copy'])) { echo '<span class="text">'.$aMSG['std']['copy_reminder'][$syslang].'<br><br></span>'; } ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="25%"><col width="75%"></colgroup>
<?php foreach ($oWeblang->getAllKeys() as $k) { ?>
	<tr valign="top">
		<td class="lang<?php echo $k; ?>"><span class="text"><b><?php echo $aMSG['form']['title'][$syslang].' ('.$k.')'; ?> *</b><br><small>[max. 50 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></span></td>
		<td class="lang<?php echo $k; ?>"><?php echo $oForm->textfield("title_".$k, 150, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
<?php } // END foreach ?>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['description'][$syslang]; ?></span></td>
		<td class="sub2"><?php echo $oForm->textarea("description", 3, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
</table>
<br>
<?php if (!$btn['copy']) { ?>
<?php	echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php	if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php	if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>&nbsp;&nbsp;
<?php	if ($mode_key == "edit") { echo $oForm->button("COPY"); } ?>
<?php } else { ?>
<?php	echo $oForm->button("SAVE_COPY"); // params: $sType[,$sClass=''] ?>
<?php	echo $oForm->button("CANCEL"); ?>
<?php } ?>

<?php
} // END if ANSICHT ----------------------------------------------------------------------------------- ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
