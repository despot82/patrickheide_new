<?php
// SHARED BEGIN ****************************************************************************
	
	/*	folgende GET-params werden erwartet:
		- mailinglist	(required)
		- template		(optional)
		- start			(optional / required fuer "adr_print_labels.php")
		
		folgende Variablen stehen nach diesem Include zur Verfuegung:
		- (array)	$aCountry
		- (string)	$sListName
		- (string)	$sListTemplate
		- (array)	$aListProps
		- (int)		$limit
		- (int)		$anz_zellen (pro TR)
		- (string)	$td_width
		Funktionen:
		- QUERY wurde abgeschickt [EINTRAEGE in dieser mailingliste (ordered by "last_modified" ASC)]
		- function _get_contact_name(&$oDb2, $contact_id)				// returns (string)	$name
		- function _get_contact_address(&$oDb2, $contact_id, $cat='p')	// returns (array)	$aData2
		- function _get_absender($client_shortname, $country, $name)	// returns (string)	$absender
	*/
	
	// get data for COUNTRY NAMES
	$aCountry = _get_all_countries($oDb); // von "inc.adr_functions.php"
	
	// NAME + TEMPLATE dieser Mailinglist
	$oDb->query("SELECT listname, template FROM adr_mailing_name WHERE id='".$mailinglist."'");
	$tmp = $oDb->fetch_array();
	$sListName		= $tmp['listname'];
	$sListTemplate	= $tmp['template'];
	
	// Label Eigenschaften
	$aListProps		= array();
	$aListProps['3490']['cols'] = 3;
	$aListProps['3490']['rows'] = 8;
	$aListProps['4781']['cols'] = 2;
	$aListProps['4781']['rows'] = 6;
	$aListProps['4743']['cols'] = 2;
	$aListProps['4743']['rows'] = 6;
	$aListProps['7173']['cols'] = 2;
	$aListProps['7173']['rows'] = 5;
	
	// Datensaetze pro Ausgabeseite einstellen
	$limit = $aListProps[$sListTemplate]['cols'] * $aListProps[$sListTemplate]['rows'];
	// (Anzahl Zellen (TDs) pro Reihe (TR)
	$anz_zellen = $aListProps[$sListTemplate]['cols'];
	// TD Breite (abhaengig von Anzahl TDs)
	$td_width = ($aListProps[$sListTemplate]['cols'] == 3) ? "33%" : "50%";
	
	// EINTRAEGE in dieser mailingliste (ordered by "last_modified" ASC)
	$sQuery = "SELECT contact_id, address 
				FROM adr_mailing_contact 
				WHERE list_id='".$mailinglist."' 
				ORDER BY last_modified ASC";
	$oDb->query($sQuery);
	$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
	if (empty($start)) {$start=0;}
	$oDb->limit($sQuery, $start, $limit);
	
// HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////////
	function _get_contact_name(&$oDb2, $contact_id) {
		// get name
		$oDb2->query("SELECT title, firstname, surname, company 
					FROM adr_contact 
					WHERE id='".$contact_id."'");
		$tmp = $oDb2->fetch_array();
		if (!empty($tmp['company'])) {
			$name = $tmp['company'];
		} else {
			$name = $tmp['firstname'].' '.$tmp['surname'];
			$name = (empty($tmp['title'])) ? $name : $tmp['title'].' '.$name; // ggf. title davor
		}
		// output
		return $name;
	}
	function _get_contact_address(&$oDb2, $contact_id, $cat='p') {
		$buffer = array();
		if ($cat == 'p' || $cat == 'c') {
			// get private address
			$sSql	= "SELECT po_box, po_box_town, street, building, region, town, country, recipient 
						FROM adr_address 
						WHERE reference_id=".$contact_id." 
							AND cat='".$cat."'";
			$oDb2->query($sSql);
			$tmp = $oDb2->fetch_array();
		} else {
			// get company-id
			$oDb2->query("SELECT company_id 
						FROM adr_address 
						WHERE reference_id='".$contact_id."' 
							AND cat='".$cat."'");
			$tmp = $oDb2->fetch_array();
			$buffer['company_id'] = $tmp['company_id'];
			if (empty($buffer['company_id'])) { return false; }
			// get company-name & -address
			$oDb2->query("SELECT c.company, a.po_box, a.po_box_town, a.street, a.building, a.region, a.town, a.country, a.recipient 
						FROM adr_contact c, adr_address a 
						WHERE c.id=".$buffer['company_id']." 
							AND a.reference_id=c.id 
							AND a.cat='c'");
			$tmp = $oDb2->fetch_array();
			$buffer['company']	= $tmp['company'];
		}

		$buffer['po_box']		= $tmp['po_box'];
		$buffer['po_box_town']	= $tmp['po_box_town'];
		$buffer['street']		= $tmp['street'];
		$buffer['building']		= $tmp['building'];
		$buffer['region']		= $tmp['region'];
		$buffer['town']			= $tmp['town'];
		$buffer['country']		= $tmp['country'];
		$buffer['recipient']	= $tmp['recipient'];
		// output
		return $buffer;
	}
// END HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////
// END SHARED *****************************************************************************
?>