<?php
/**
* inc.adr_detnavi.php
*
* subnavi-include (nur INTRANET/address) //-> 2sprachig und voll kopierbar!
*
* @param	int		$id			welcher Datensatz
* @param	string	$type		welcher Typ [contact|company]
* @param	string	$cat		welche Kategorie [o1|o2]
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-02-19
*/
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.adr_detnavi.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

?>
<script language="JavaScript" type="text/javascript">
// modifizierte Delete Funktion die es ermoeglicht mit einem html-link (ohne form) zu loeschen
function confirmRemoteDelete(deleteid) {
	var chk = confirmDelete();
	if (chk == true) {
		document.forms['editForm'].elements['btn[remoteDelete]'].value = deleteid;
		document.forms['editForm'].submit();
	}
}
function doRemoteSave() {
	document.forms['editForm'].elements['btn[remoteSave]'].value = 1;
	document.forms['editForm'].submit();
}
</script>


<?php // ggf. title
if (empty($id)) {
	echo '<span class="title">'.$aMSG['address']['new_'.$type.''][$syslang].'<br></span>';
} else { ?>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><span class="title"><?php	// company-/contact-name
		if ($type == "contact"){
			$oDb->query("SELECT concat(title,' ',firstname,' ',surname) as name FROM adr_contact WHERE id='".$id."'");
			if ($oDb->num_rows()) echo ''.$oDb->fetch_field("name").'';
		} else {
			$oDb->query("SELECT company FROM adr_contact WHERE id='".$id."'");
			if ($oDb->num_rows()) echo ''.nl2br_cms($oDb->fetch_field("company")).'';
		}
		?><br></span></td>
		<td align="right">
		<?php if ($oPerm->hasPriv('edit')) { ?>
		<a href="#" onclick="doRemoteSave()" title="<?php echo $aMSG['btn']['view_mode'][$syslang]; ?>" class="cnavi"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_save.gif" alt="<?php echo $aMSG['btn']['view_mode'][$syslang]; ?>" class="btn"></a><?php 
		} // END if ?><?php if ($oPerm->hasPriv('delete')) { ?><a 
		href="#" onclick="confirmRemoteDelete(<?php echo $id; ?>)" title="<?php echo $aMSG['btn']['delete'][$syslang]; ?>" class="cnavi"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_delete.gif" alt="<?php echo $aMSG['btn']['delete'][$syslang]; ?>" class="btn"></a><br>
		<?php } // END if ?>
		</td>
	</tr>
	</table>
	<?php echo HR ?>
	
	<span class="cnavi">
	<?php if ($oPerm->hasPriv('edit')) { ?>
	<?php	if (isset($type) && $type == 'contact') { ?>
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>" class="cnavi<?php if ($sBsname == "adr_edit_d.php") echo 'hi'; ?>"><?php echo $aMSG['address']['d'][$syslang]; ?></a>&nbsp;&nbsp;|&nbsp;
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_p.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>" class="cnavi<?php if ($sBsname == "adr_edit_p.php") echo 'hi'; ?>"><?php echo $aMSG['address']['p'][$syslang]; ?></a>&nbsp;&nbsp;|&nbsp;
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_o.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>&cat=o1" class="cnavi<?php if ($cat=='o1') echo 'hi'; ?>"><?php echo $aMSG['address']['o1'][$syslang]; ?></a>&nbsp;&nbsp;|&nbsp;
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_o.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>&cat=o2" class="cnavi<?php if ($cat=='o2') echo 'hi'; ?>"><?php echo $aMSG['address']['o2'][$syslang]; ?></a>&nbsp;&nbsp;|&nbsp;
	
	<?php if (isset($aENV['allow_cug']) && $aENV['allow_cug']=="true") { ?>
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_w.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>" class="cnavi<?php if ($sBsname == "adr_edit_w.php") echo 'hi'; ?>"><?php echo $aMSG['address']['w'][$syslang]; ?></a>
	<?php } ?>
	
	<?php	} else { ?>
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_d.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>" class="cnavi<?php if ($sBsname == "adr_edit_d.php") echo 'hi'; ?>"><?php echo $aMSG['address']['d'][$syslang]; ?></a>&nbsp;&nbsp;|&nbsp;
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_c.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>" class="cnavi<?php if ($sBsname == "adr_edit_c.php") echo 'hi'; ?>"><?php echo $aMSG['address']['c'][$syslang]; ?></a>&nbsp;&nbsp;|&nbsp;
	<a href="<?php echo $aENV['SELF_PATH']; ?>adr_edit_s.php?id=<?php echo $id; ?>&type=<?php echo $type; ?>&start=<?php echo $start; ?>" class="cnavi<?php if ($sBsname == "adr_edit_s.php") echo 'hi'; ?>"><?php echo $aMSG['address']['s'][$syslang]; ?></a>
	<?php	} // END if ?>
	<?php } // END if ?>
	</span>

<?php } // END if ?>
<br>

<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="10" alt="" border="0"><br>