<?php
/**
* popup_selectcontact.php
*
* Detailpage (popup): select contact for cug
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	string	$cug			[um zu wissen welche CUG uebertragen werden soll] (optional)
* @param	int		$start			[zum filtern der Anzeige] (optional)
* @param	string	$searchATerm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der 'inc.login.php'
* #@param	string	$searchAType	[zum filtern der Anzeige um den Contact-type (contact|company)] (optional) -> kommt ggf. aus der 'inc.login.php'
* @param	string	$searchAGroup	[zum filtern der Anzeige auf nur eine "adr_group"] (optional) -> kommt ggf. aus der 'inc.login.php'
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-02-19
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_selectcontact.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/inc.sys_login.php");
	require_once ($aENV['path']['global_service']['unix']."class.Search.php");
	require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$cug			= (isset($_GET['cug'])) ? $oFc->make_secure_string($_GET['cug']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	#if (isset($_GET['searchATerm'])){ $searchATerm = $oFc->make_secure_string($_GET['searchATerm']); } // (kein default wegen session!)
	$searchATermPopup = (isset($_GET['searchATermPopup'])) ? $oFc->make_secure_string($_GET['searchATermPopup']) : '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_contact";

//	$oDb->debug = true;
// INIT Search Class
	$oSearch =& new Search($oDb,$aENV,'adr',$Userdata['id']);
	
	// CONTENT
	$limit = 20;	// Datensaetze pro Ausgabeseite einstellen
	if (empty($start)) {$start=0;}
	// db-search-vars
	$searchfields				= $aENV['table']['sys_global_search'].".searchtext_mod";
	$oDb->soptions['select']	= "adr_contact.id,adr_contact.company,adr_contact.client_nr,adr_contact.client_shortname,adr_contact.firstname,adr_contact.surname,adr_contact.grp,adr_contact.comment,adr_contact.description";

	if (!empty($searchATermPopup)) {	
	// SEARCH		
		// 2006-04-13af: FIX fuer einfache Anfuehrungszeichen im suchbegriff:
		$searchATermPopup = str_replace("'", "\'", $searchATermPopup);
		$sSearch = $oSearch->modifySearchString($searchATermPopup);
		
		$oSearch->select($sSearch, 0, 99999, 'searchtext_mod ASC',true,true);
		$aData = $oSearch->getSearchResult();
		
		$aData2 = array();
		for($i=0,$j=0;is_array($aData[$i]);$i++) {
			$sql = "SELECT id,company,client_nr,client_shortname,firstname,surname,grp,comment,description FROM adr_contact WHERE id='".$aData[$i]['id']."' AND (company='' OR company IS NULL)";
			$oDb->query($sql);
			$row = $oDb->fetch_array();
			if(is_array($row)) {
				if($j>=$start && $j < ($start+$limit)) {
					$aData2[] = $row;
				}
				$j++;
			}
		}
		$aData = $aData2;
		$entries = $j;
	} else {
	// wenn KEINE SEARCH
		$orderby = (empty($orderbyA)) ? $aENV['table']['sys_global_search'].".searchtext_mod ASC" : $orderbyA;
		// '.$oDb->soptions['extra'].'
		$sql = 'SELECT '.$oDb->soptions['select'].' FROM adr_contact INNER JOIN '.$aENV['table']['sys_global_search'].' ON adr_contact.id='.$aENV['table']['sys_global_search'].'.ref_id WHERE '.$aENV['table']['sys_global_search'].'.module=\'adr\' AND (company=\'\' OR company IS NULL) ORDER BY '.$orderby;

		$oDb->query($sql);
		$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
		if ($entries > $limit) { $oDb->limit($sql, $start, $limit); }
		for($i=0;$aData[$i] = $oDb->fetch_array();$i++);
		$last = count($aData);
		unset($aData[($last-1)]);
	}
	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
	
	/*
// 3. DB-action: select
	// build query
	$sQuery = "SELECT id, firstname, surname FROM ".$sTable." ";
	// type auf company einschraenken
	$sQuery .= " WHERE (company='' OR company IS NULL) ";
	// wenn suchbegriff eingegeben wurde
	if (isset($searchATermPopup) && !empty($searchATermPopup)) {
		$searchATermPopup = strToLower($searchATermPopup);
		$sQuery .= " AND ( (LOWER(firstname) LIKE '%".$searchATermPopup."%') OR (LOWER(surname) LIKE '%".$searchATermPopup."%') OR (LOWER(comment) LIKE '%".$searchATermPopup."%') OR (LOWER(description) LIKE '%".$searchATermPopup."%') ) ";
	}
	// orderby
	$sQuery .= " ORDER BY surname ASC, firstname ASC";
	
	$oDb->query($sQuery);
	$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
	if (empty($start)) { $start=0; }
	$oDb->limit($sQuery, $start, $limit);
	*/

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");
?>
<script language="JavaScript" type="text/javascript">
// puts value into input field of opener page and save (to reload)
function loadPage(contact_id, cug_id) {
	window.opener.location.href='adr_edit_w.php?type=contact&id='+contact_id+'&cug='+cug_id;
	window.opener.focus();
	// nach speichern schliessen
	self.close();
}
</script>

<div id="contentPopup">

<span class="title"><?php echo $aMSG['btn']['select_contact'][$syslang]; ?><br></span>
<br>
<form action="<?php echo $aENV['PHP_SELF']; ?>" method="get" name="selectForm">
<input type="hidden" name="cug" value="<?php echo $cug; ?>">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<td class="sub2"><!-- suche-modul -->
			<input type="text" name="searchATermPopup" size="25" value="<?php echo $searchATermPopup; ?>" class="input">
			<input type="submit" name="search" value="<?php echo $aMSG['btn']['search'][$syslang]; ?>" class="but">
		</td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
			<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></span></td>
			<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks('&cug='.$cug.'&searchATermPopup='.$searchATermPopup, 5); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></span>
		</td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="75%"><?php echo $aMSG['form']['name'][$syslang]; ?></th>
	</tr>
<?php for ($i=0;is_array($aData[$i]);$i++) { ?>
	<tr>
		<td class="sub1"><span class="text"><a href="javascript:loadPage('<?php echo $aData[$i]['id']; ?>','<?php echo $cug; ?>')" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><?php echo (!empty($aData[$i]['surname'])) ? $aData[$i]['surname'].', ' : ''; echo $aData[$i]['firstname']; ?></a></span></td>
	</tr>
<?php } // END while ?>
</table>

<br>
</form>
</div>

</body>
</html>