<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mod_adrsearch.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// vars
	$searchform_action = (isset($mailinglist) && $mailinglist > 0)  
		? $aENV['path']['adr']['http'].'adr_mailing_list.php' 
		: $aENV['path']['adr']['http'].'adr.php';
	$searchtext_value =  ($searchATerm) ? stripslashes($searchATerm) : $aMSG['topnavi']['searchadr'][$syslang];

// HTML
?>
<form action="<?php echo $searchform_action; ?>" method="get" name="searchForm"><?php	
if (isset($mailinglist) && $mailinglist > 0) { 
	?><input type="hidden" name="mailinglist" value="<?php echo $mailinglist; ?>"><?php 
} ?><input type="text" name="searchATerm" size="14" class="input" value="<?php echo $searchtext_value; ?>" onFocus="this.value=''"><?php 
echo get_button('SEARCH', $syslang, 'smallbut'); 
?></form>