<?php
/**
* mbl_adr.php
*
* Mobile Services Adressen - Einstieg 
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.0 
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mbl_adr.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/inc.sys_login.php");
	require_once ($aENV['path']['global_service']['unix']."class.Search.php");
	require_once ($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$btn			= (isset($_GET['btn'])) ? $oFc->make_secure_string($_GET['btn']) : '';
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_contact";
	$sViewerPage	= "mbl_adr_detail.php";	// fuer EDIT-link
	$sEditPage		= "mbl_adr_edit.php";	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;				// fuer NEW-button

// 3. HTML
	require_once ("./inc.mbl_header.php");

// SHARED BEGIN ****************************************************************************
	require_once ($aENV['path']['adr']['unix']."inc.adr_functions.php");
// END SHARED *****************************************************************************

// 4. DB-action: select
	// GROUPS (store in array)
	$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
	
// INIT Search Class
	$oSearch =& new Search($oDb,$aENV,'adr',$Userdata['id']);

	// CONTENT
	$limit = 20;	// Datensaetze pro Ausgabeseite einstellen
	if (empty($start)) {$start=0;}
	$fulltextsearch = false;
	// db-search-vars
	$searchfields				= $aENV['table']['sys_global_search'].".searchtext_mod";
	$oDb->soptions['select']	= "adr_contact.id,adr_contact.company,adr_contact.client_nr,adr_contact.client_shortname,adr_contact.firstname,adr_contact.surname,adr_contact.grp,adr_contact.comment,adr_contact.description";

	// ggf. SEARCH
	if (!empty($searchATerm)) {		
		// 2006-04-13af: FIX fuer einfache Anfuehrungszeichen im suchbegriff:
		$searchATerm = str_replace("'", "\'", $searchATerm);
		$sSearch = $oSearch->modifySearchString($searchATerm);
		
		$oSearch->select($sSearch, $start, $limit, $sOrder,true,false);
		// 1: fulltext-search
		if ($oSearch->getNumfulltext() > 0) {
			$aData = $oSearch->getSearchResult();
			$fulltextsearch = true;
		}
		else {
			$oSearch->select($sSearch, $start, $limit, 'searchtext_mod ASC',false,true);
			$aData = $oSearch->getSearchResult();
		}
		for($i=0;is_array($aData[$i]);$i++) {
			$sql = "SELECT id,company,client_nr,client_shortname,firstname,surname,grp,comment,description FROM adr_contact WHERE id='".$aData[$i]['id']."'";
			$oDb->query($sql);
			$row = $oDb->fetch_array();
			foreach($row as $field => $value) {
				$aData[$i][$field] = $value;
			}
		}
		$entries = $oSearch->getEntries();


	} else {
	// wenn KEINE SEARCH
		$orderby = (empty($orderbyA)) ? $aENV['table']['sys_global_search'].".searchtext_mod ASC" : $orderbyA;
		// '.$oDb->soptions['extra'].'
		$sql = 'SELECT '.$oDb->soptions['select'].' FROM adr_contact INNER JOIN '.$aENV['table']['sys_global_search'].' ON adr_contact.id='.$aENV['table']['sys_global_search'].'.ref_id WHERE '.$aENV['table']['sys_global_search'].'.module=\'adr\' ORDER BY '.$orderby;

		$oDb->query($sql);
		$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
		if ($entries > $limit) { $oDb->limit($sql, $start, $limit); }
		for($i=0;$aData[$i] = $oDb->fetch_array();$i++);
		$last = count($aData);
		unset($aData[($last-1)]);
	}
	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
?>

	<br />
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" name="searchForm">
	<input type="text" name="searchATerm" size="13" class="input" value="<?php echo ($searchATerm)? $searchATerm : ""; ?>" /><input type="submit" name="btn[search]" class="button" value="&raquo;" />
	</form>
	<br />
	
	<div class="titleRow" style="margin-bottom: 1%;">
	<b><?php echo $aMSG['address']['headline'][$syslang] ?></b>
	 &nbsp;|&nbsp; 
	<?php
	if ($start > 0) { echo '<a href="'.$_SERVER['PHP_SELF'].'?start='.($start - $limit).'" title="'.$aMSG['btn']['back'][$syslang].'">'.$aMSG['btn']['back'][$syslang].'</a> &nbsp;|&nbsp; '; }
	if ($entries > $limit) { echo '<a href="'.$_SERVER['PHP_SELF'].'?start='.($start + $limit).'" title="'.$aMSG['btn']['continue'][$syslang].'">'.$aMSG['btn']['continue'][$syslang].'</a>'; }
	?>
	</div>
	
	<?php
	$oDb2 =& new dbconnect($aENV['db']); // fuer o1 / o2
	$i = 1;
	for($i=0;is_array($aData[$i]);$i++) {
		if ($aData[$i]['company'] == '') {
			if (empty($aData[$i]['surname']) && !empty($aData[$i]['firstname'])) {
				$name = $aData[$i]['firstname'];
			} else if (empty($aData[$i]['firstname']) && !empty($aData[$i]['surname'])) {
				$name = $aData[$i]['surname'];
			} else {
				$name = $aData[$i]['surname'].', '.$aData[$i]['firstname'];
			}
			$type = 'contact';
			
			// ermittle o1 / o2 dieser kontakt(person)
			$sQuerySub = "SELECT a.company_id, c.company FROM adr_address a, adr_contact c WHERE a.company_id=c.id AND a.reference_id='".$aData[$i]['id']."'";
			$aCompany = array();
			$oDb->query($sQuerySub);
			while ($aData2 = $oDb->fetch_array()) {
				$aCompany[$aData2['company_id']] = $aData2['company'];
			}
		} else {
			$name = nl2br_cms($aData[$i]['company']);
			$type = 'company';
		}
		$sGEToptions = "?id=".$aData[$i]['id']."&start=".$start."&type=".$type;
	?>
	<div class="textRow">
	<a href="<?php echo $sViewerPage.$sGEToptions; ?>" class="textLink"><?php echo '<b>'.shorten_text($name, 45).'</b>'; ?></a>
	
	<?php
		if ($type == "contact" && (empty($aData[$i]['company']) && count($aCompany))) {
			echo "<br />\n";
			// bei contact -> zeige zugehoerige companies
			if (empty($aData[$i]['company']) && count($aCompany)) {
				foreach ($aCompany as $k => $v) {
					echo '<a href="'.$sViewerPage.'?id='.$k.'&start='.$start.'&type=company" class="subLink">'.$v."</a><br />\n";
				}
			}
		} 
	?>
	</div>
	
<?php
	} // END while
	if ($entries == 0) { echo '<div class="textRow">'.$aMSG['std']['no_entries'][$syslang].'</div>'; }

	// "seite x von y" + Anzahl gefundene Datensaetze anzeigen
	echo '<p>'.$oDBNavi->getDbnaviStatus()."</p>\n"; // params: $nEntries[,$nStart=0][,$nLimit=20]
	?>

	<?php if ($oPerm->hasPriv('create')) {	// NEW-BUTTONS ?>
	<!--<p>
	<a href="mbl_adr_edit.php?type=contact" class="naviLink"><?php echo $aMSG['btn']['new_contact'][$syslang]; ?> &raquo;</a> &nbsp;|&nbsp; <a href="mbl_adr_edit.php?type=company" class="naviLink"><?php echo $aMSG['btn']['new_company'][$syslang]; ?> &raquo;</a>
	</p>-->
	<?php } // END if ?>
	
<?php require_once ("./inc.mbl_footer.php"); ?>