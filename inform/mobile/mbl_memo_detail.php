<?php
/**
* mbl_memo_detail.php
*
* Mobile Services Memo-Detail: 
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mbl_memo_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : 0;
	$edit			= (isset($_GET['edit'])) ? $oFc->make_secure_string($_GET['edit']) : "false";
	$sGEToptions	= '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars
	$sTable			= $aENV['table']['sys_memo'];
	$flagopen		= (isset($aData['flag_open'])) ? $aData['flag_open'] : 0;
	$sViewerPage	= "index.php".$sGEToptions;	// fuer BACK-button
// TREE ACCESS CONTROLL
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_service']['unix']."class.team.php");
	if(!isset($aENV['php5']) || !$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php');
	}
	else {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php5');
	}
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->initialize();
	
	$oTeam =& new Team($oDb);
	// USER zwischenspeichern (key: ID, value: Name)
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUsername = $oUser->getAllUserNames(true); // params: [$bValidOnly=true]
	$oMemoTools =& new Memo($oDb);
// 3. DB
// DB-action: delete
	if (isset($btn['delete']) && !empty($aData['id'])) {
		$oDb->make_delete($aData['id'], $sTable);
		$oMemoTools->clearMemo($aData['id']);
		$oTACEA->onDeleteAction($aData['id'],'memo');
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save'])) {
		// auth in die richtige formatierung umwandeln.
		$user = implode(',',$aData['authorisation']);
		unset($aData['authorisation']);
		if(empty($user)) $user = $Userdata['id'];
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$oDb->make_update($aData, $sTable, $aData['id']);
			if ($flagopen == 1) {			
				$oTACEA->delRightForTree($aData['id']);
			}
			else {			
				$oTACEA->addRights('', $user,$aData['id'],'memo',true,$Userdata['id']);
			}
		} else { // Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			// make insert
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id();
			if ($flagopen == 1) {			
				$oTACEA->delRightForTree($aData['id']);
			}
			else {			
				$oTACEA->addRights('', $user,$aData['id'],'memo',true,$Userdata['id']);
			}
	
			header("Location: ".$sViewerPage); exit;
		}
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
	}
// 4. HTML
	require_once ("./inc.mbl_header.php");

// anzeigen
if (isset($id) && $id > 0 && $edit == "false") {
	echo "<p>\n";
	echo '<a href="'.$sViewerPage.'" title="'.$aMSG['mbl']['overview'][$syslang].'">'.$aMSG['mbl']['overview'][$syslang].'</a>';
	echo ' &nbsp;|&nbsp; <a href="'.$_SERVER['PHP_SELF'].'?id='.$id.'&edit=true" title="'.$aMSG['std']['edit'][$syslang].'">'.$aMSG['std']['edit'][$syslang].'</a>';
	echo '</p>';
	
	// output
	echo '<div class="titleRow">'."\n";
	// date - time
	echo '<b>'.$oDate->date_from_mysql_timestamp($aData['last_modified'])."</b>";
	echo ' - '.$oDate->time_from_mysql_timestamp($aData['last_modified']).', ';
	// created_by
	echo $aUsername[$aData['created_by']];
	// last_mod_by
	if (isset($aData['last_mod_by']) && ($aData['last_mod_by'] != $aData['created_by']) ) {
		echo " / ".$aMSG['std']['last_update'][$syslang].": ".$aUsername[$aData['last_mod_by']];
	}
	// beteiligte User auflisten
	echo '<br />'.$aMSG['form']['foruser'][$syslang].' ';
	$tmp = array();
	// Die Teilnehmer aus der Memo-Klasse ermitteln	
	echo implode(', ',$oTACEA->getParticipants($oTeam,$oUser,'memo',$aData['id']))."</div>\n";
	// memo
	echo '<div class="textRow">'."\n";
	echo nl2br_cms($aData['memo']);
	echo '</div>'."\n";	

} else { // neuanlegen / editieren
	$sHeaderText = ($edit == "true") ? $aMSG['mbl']['edit_memo'][$syslang] : $aMSG['mbl']['new_memo'][$syslang];
?>
	<p><b><?php echo $sHeaderText; ?></b> &nbsp;|&nbsp; <a href="<?php echo $sViewerPage; ?>" title="<?php echo $aMSG['mbl']['overview'][$syslang]; ?>"><?php echo $aMSG['mbl']['overview'][$syslang]; ?></a></p>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="editForm" id="editForm">
	<input type="hidden" name="aData[id]" id="memo_id" value="<?php echo $id; ?>" />
	<textarea rows="2" cols="18" name="aData[memo]" id="memo" class="input"><?php echo (isset($aData['memo'])) ? $aData['memo'] : ""; ?></textarea><br />
	<?php echo $aMSG['form']['foruser'][$syslang]; ?>:<br />
	<?php // build checkboxes
	if ($oPerm->isDaService()) { // eintraege fuer da-service nur fuer da anzeigen
		echo '<input type="checkbox" name="aData[authorisation][]" id="'.$Userdata['id'].'" value="'.$Userdata['id'].'" checked="checked" class="radio" />&nbsp;&nbsp;';
		echo '<label for="'.$Userdata['id'].'">'.$Userdata['firstname']." ".$Userdata['surname']."</label><br />\n";
	}
	foreach ($aUsername as $u_id => $u_name) {
		// checked?
		$sel = ($oMemoTools->isUserInAuthedTeam($u_id,$aData['id'],$oTACEA,$oTeam)) ? ' checked="checked"' : '';
		// output
		echo '<input type="checkbox" name="aData[authorisation][]" id="'.$u_id.'" value="'.$u_id.'"'.$sel.' class="radio" />&nbsp;&nbsp;';
		echo '<label for="'.$u_id.'">'.$u_name."</label><br />\n";
	}
	?>	
	<input type="submit" value=" <?php echo $aMSG['btn']['save'][$syslang]; ?> " class="button" name="btn[save]" id="save" />
	<?php if ($edit == "true") { ?>
	<input type="submit" value=" <?php echo $aMSG['btn']['delete'][$syslang]; ?> " class="button" name="btn[delete]" id="delete" />	
	<?php } ?>
	</form><br />
<?php
}

require_once ("./inc.mbl_footer.php");
?>