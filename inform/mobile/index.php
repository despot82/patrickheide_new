<?php
/**
* index.php
*
* Mobile Services-Index
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 
*/

// 1. init
	require_once("../sys/inc.sys_login.php");

// 2. Vars
	$sGEToptions 	= '';
	$delete_id		= (isset($_GET['delete_id'])) ? $oFc->make_secure_int($_GET['delete_id']) : 0;
	$sTable			= $aENV['table']['sys_memo'];

// TREE ACCESS CONTROLL
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_service']['unix']."class.team.php");
	if(!isset($aENV['php5']) || !$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php');
	}
	else {
		require_once($aENV['path']['global_module']['unix'].'class.Memo.php5');
	}
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->initialize();
	$oTeam =& new Team($oDb);
	// USER zwischenspeichern (key: ID, value: Name)
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUsername = $oUser->getAllUserNames(false); // params: [$bValidOnly=true]
	$oMemoTools =& new Memo($oDb);
	
// 3. DB
// DB-action: delete
	if (isset($delete_id) && $delete_id > 0) {
		$oDb->make_delete($delete_id, $sTable);
		$oMemoTools->clearMemo($delete_id);
		$oTACEA->onDeleteAction($delete_id,'memo');
	}
	
// 4. HTML
	require_once ("./inc.mbl_header.php");
?>

<!-- memos -->
<p><b><?php echo $aMSG['mbl']['memo'][$syslang]; ?></b> &nbsp;|&nbsp; <a href="mbl_memo_detail.php" title="<?php echo $aMSG['std']['new'][$syslang]; ?>"><?php echo $aMSG['std']['new'][$syslang]; ?></a></p>

<?php
// echo $oBrowser->getUserAgent().'<br/><br/>';

// Netscape 7: mozilla/5.0 (windows; u; windows nt 5.1; de-de; rv:1.4) gecko/20030619 netscape/7.1 (ax)
// Nokia 6230: nokia6230/2.0 (03.15) profile/midp-2.0 configuration/cldc-1.1
// Sony: sonyericssonk700i/r2a semc-browser/4.0 profile/midp-1.0 midp-2.0 configuration/cldc-1.1

// DB: get memos
$aData = $oMemoTools->getMemos();
for ($i=0;!empty($aData[$i]['id']);$i++) {
	// zeige nur die, die fuer mich bestimmt sind
	if(!$oMemoTools->isUserInAuthedTeam($Userdata['id'],$aData[$i]['id'],$oTACEA,$oTeam)) continue;
	// output
	echo '<div class="titleRow">'."\n";
	// date - time
	echo '<b>'.$oDate->date_from_mysql_timestamp($aData[$i]['last_modified'])."</b>";
	echo ' - '.$oDate->time_from_mysql_timestamp($aData[$i]['last_modified']).', ';
	// created_by
	echo $aUsername[$aData[$i]['created_by']]."</div>\n";
	// memo
	echo '<div class="textRow">'."\n";
	echo shorten_text($aData[$i]['memo'], 60).' <a href="mbl_memo_detail.php?id='.$aData[$i]['id'].'" title="'.$aMSG['mbl']['more'][$syslang].'" class="textLink">'.$aMSG['mbl']['more'][$syslang].'</a>';
	echo ' | <a href="'.$_SERVER['PHP_SELF'].'?delete_id='.$aData[$i]['id'].'" title="'.$aMSG['btn']['delete'][$syslang].'" class="textLink">'.$aMSG['mbl']['delete'][$syslang].'</a>';
	echo '</div>'."\n";
} // END for

require_once ("./inc.mbl_footer.php");
?>