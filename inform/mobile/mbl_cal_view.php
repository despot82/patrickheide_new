<?php
/**
* mbl_cal_view.php
*
* Mobile Services Kalender-Tagesansicht
*
* @param	string	$view		[zur Unterscheidung der Ansicht] (optional)
* @param	string	$filter		[zur Unterscheidung der Ansicht] (optional)
* @param	string	$office_id	[zur Unterscheidung der Ansicht] (optional)
* @param	int		$date_ts	[timestamp zum anzeigen von bestimmten tagen/wochen/..] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mbl_cal_view.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$today_ts		= time();
	$date_ts		= (isset($_GET['date_ts'])) ? $oFc->make_secure_int($_GET['date_ts']) : $today_ts; // default: heute
	$calMonth		= (isset($_GET['calMonth'])) ? $oFc->make_secure_string($_GET['calMonth']) : '';
	$calYear		= (isset($_GET['calYear'])) ? $oFc->make_secure_string($_GET['calYear']) : '';
	$filter			= (isset($_GET['filter'])) ? $oFc->make_secure_string($_GET['filter']) : '';
	$view			= (isset($_GET['view'])) ? $oFc->make_secure_string($_GET['view']) : 'day';
	$office_id		= (isset($_GET['office_id'])) ? $_GET['office_id'] + 0 : '';
	$sGEToptions	= '?view=day&date_ts='.$date_ts.'&calMonth='.$calMonth.'&calYear='.$calYear;
	if (!empty($filter))	{ $sGEToptions .= '&filter='.$filter; } // filter-vars nur bei bedarf!
	if (!empty($office_id))	{ $sGEToptions .= '&office_id='.$office_id; }

// 3. vars
	$sEditPage	= 'mbl_cal_detail.php'.$sGEToptions;
	$sNewPage	= $sEditPage;
	$date_ts_pregsearch = '/(\?|\&)date_ts=([0-9])*/';

	if (!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->initialize();
	}
// 4. date
	$oDate->set_timestamp($date_ts); // init timestamp in date-object

// 5. HTML
	require_once ("./inc.mbl_header.php");

// create new calendar object.
   	if(!$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	}
	else {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php5");
	}
	$oCal =& new calendar($syslang,$oTACEA);
	// apply filter
	if ($filter == 'my') {
		$aFilter = array("show_birthdays" => false, "participant" => $Userdata['id']);
	}
	if ($filter == 'office') {
		$aFilter = array("show_birthdays" => true, "office" => $office_id);
	}
	if ($filter == 'all') {
		$aFilter = array("show_birthdays" => true);
	}
	if (empty($filter) && $Userdata['office_id'] != 0) { // default = my office
		$aFilter = array("show_birthdays" => true, "office" => $Userdata['office_id']);
	}
	if(is_array($aFilter)) {
		$oCal->setFilter($aFilter); // params: $aFilter
	}
	
// get all usernames
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUsername = $oUser->getAllUserNames(false); // params: [$bValidOnly=true]
?>
<p><a href="mbl_cal.php<?php echo $sGEToptions; ?>" title="<?php echo $aMSG['mbl']['month_overview'][$syslang]; ?>"><?php echo $aMSG['mbl']['month_overview'][$syslang]; ?></a> &nbsp;|&nbsp; <a href="<?php echo $sNewPage; ?>" title="<?php echo $aMSG['std']['new'][$syslang]; ?>"><?php echo $aMSG['std']['new'][$syslang]; ?></a></p>
<p><b><?php echo $oDate->get_dayname(NULL,false).', '.$oDate->get_today(false); ?></b></p>

<?php
	$addDayVars .= '#'.$aCalConfig['daytime_start']; // @see setup.php

	if (isset($aENV['module']['adm'])) {
		// PROJECTS zwischenspeichern (key: ID, value: Name)
		$aProject		= _getProjectArray($oDb);
	}

	// get EVENTS
	$aEvents		= $oCal->getDayEvents($oDb, $date_ts, "wap"); // params: &$oDb[,$day=NULL][,$media='']

	// array fuer datums-formatierung
	$iso_today		= date("Y-m-d", $date_ts);
	
	foreach ($aEvents as $k => $event) {
		$eventBirthday = (strstr($event['icon'], "icn_birthday")) ? true : false;
?>
	<div class="titleRow">
		<?php
		echo $event['icon'].' ';
		if ($eventBirthday == false) { echo '<a href="'.$sEditPage.'&id='.$event['id'].'" class="textLink">'; }
		echo $event['subject'];
		if ($eventBirthday == false) { echo '</a>'; }
		echo $event['sign_multidays'];
		?>
	</div>
	<div class="textRow">
		<?php echo $event['show_time']; ?>
		<?php echo $event['show_total_time']; ?>
		<?php if (!empty($event['location'])) echo $aMSG['form']['location'][$syslang].': '.$event['location'].'<br />'; ?>
		<?php if (!empty($event['participant'])) echo $aMSG['form']['participant'][$syslang].': '.$event['participant'].'<br />'; ?>
		<?php if (!empty($event['project_id']) && is_array($aProject)) $aProject[$event['project_id']].'<br />'; ?>
		<?php if (!empty($event['comment'])) echo '<br>'.$event['comment'].'<br />'; ?>
	</div>
<?php
	} // END foreach $aEvents

	// Buttons zum blaettern
	$self = $_SERVER['PHP_SELF'].preg_replace($date_ts_pregsearch, '', $sGEToptions).'&date_ts=';
	$prev_day_ts = $oDate->add_days(-1);
	$next_day_ts = $oDate->add_days(1);
?>
<div><a href="<?php echo $self.$prev_day_ts; ?>" title="<?php echo $aMSG['mbl']['yesterday'][$syslang]; ?>">&lt;</a> &nbsp;|&nbsp; <a href="<?php echo $self.$today_ts; ?>" title="<?php echo $aMSG['btn']['today'][$syslang]; ?>"><?php echo $aMSG['btn']['today'][$syslang]; ?></a> &nbsp;|&nbsp; <a href="<?php echo $self.$next_day_ts; ?>" title="<?php echo $aMSG['mbl']['tomorrow'][$syslang]; ?>">&gt;</a></div>

<p>
	<b>Filter:</b><br>
<?php
	$sLinkTemplate	=  '<a href="%s">%s%s%s</a>';
	
	// get offices
	$aOffice = $oUser->getAllOfficeNames(); // params: [$bWithUsers=false]
	
	// FILTER
	$aFilterLink = array();
	$self = $_SERVER['PHP_SELF'].'?'.preg_replace('/(\?|\&)(filter=)([my|all|office])+/', '', $_SERVER['QUERY_STRING']).'&filter=';
	$self = preg_replace('/(\&)(office_id=)([0-9])*/', '', $self); // zusaetzlich office_id filtern
	
	// all
	if (count($aOffice) > 1) {
		$boldstart = (isset($_GET['filter']) && $_GET['filter'] == 'all') ? '<b>' : '';
		$boldend = (isset($_GET['filter']) && $_GET['filter'] == 'all') ? '</b>' : '';
	} 
	else { // wenn nur ein office, wird dieses nicht ausgegeben, daher hier default angeben wenn noch kein filter uebergeben wurde!
		$boldstart = (!isset($_GET['filter']) || (isset($_GET['filter']) && $_GET['filter'] == 'all')) ? '<b>' : '';
		$boldend = (!isset($_GET['filter']) || (isset($_GET['filter']) && $_GET['filter'] == 'all')) ? '</b>' : '';
	}
	$aFilterLink[] = sprintf($sLinkTemplate, $self.'all', $boldstart, $aMSG['cal']['filter_all'][$syslang], $boldend);
	
	// offices
	if (count($aOffice) > 1) {
		foreach ($aOffice as $o_id => $o_name) {
			$boldstart = (isset($_GET['filter']) && $_GET['filter'] == 'office' && $_GET['office_id'] == $o_id) ? '<b>' : '';
			$boldend = (isset($_GET['filter']) && $_GET['filter'] == 'office' && $_GET['office_id'] == $o_id) ? '</b>' : '';
			if (empty($_GET['filter']) && $Userdata['office_id'] == $o_id) {
				$boldstart = '<b>';
				$boldend = '</b>';
			}
			$aFilterLink[] = sprintf($sLinkTemplate, $self.'office&office_id='.$o_id, $boldstart, $o_name, $boldend); // zusaetzlich office_id anheangen
		}
	}
	
	// my
	$boldstart = (isset($_GET['filter']) && $_GET['filter'] == 'my') ? '<b>' : '';
	$boldend = (isset($_GET['filter']) && $_GET['filter'] == 'my') ? '</b>' : '';
	$aFilterLink[] = sprintf($sLinkTemplate, $self.'my', $boldstart, $aMSG['cal']['filter_my'][$syslang], $boldend);
	
	echo implode("<br>", $aFilterLink);
?>
</p>


<?php require_once ("./inc.mbl_footer.php"); ?>

<?php // HILFSFUNKTIONEN /////////////////////////////////////////////////////////////
	
	// Projects zwischenspeichern (key: ID, value: Name)
	function &_getProjectArray(&$oDb) {
		$buffer = array('' => '--');
		$oDb->query("SELECT p.id, p.projectname, p.prj_nr, p.prj_status, c.client_shortname, c.client_nr
					FROM adm_project p, adr_contact c 
					WHERE c.id = p.client_id 
					ORDER BY c.client_shortname ASC, p.projectname ASC");
		while ($tmp = $oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['client_shortname'].' - '.$tmp['projectname'].' [ '.$tmp['client_nr'].'.'.$tmp['prj_nr'].' ]';
		}
		return $buffer;
	}
?>

