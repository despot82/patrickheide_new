<?php
/**
* mbl_cal.php
*
* Mobile Services Kalender - Einstieg 
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.0 
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mbl_cal.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/inc.sys_login.php");

// 2. GET-params abholen
	$calMonth	= (isset($_GET['calMonth'])) ? $oFc->make_secure_string($_GET['calMonth']) : '';
	$calYear	= (isset($_GET['calYear'])) ? $oFc->make_secure_string($_GET['calYear']) : '';

	if(!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->initialize();
	}

// 3. HTML
	require_once ("./inc.mbl_header.php");

	// create new calendar object.
    require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	$oCal =& new calendar($syslang,$oTACEA);
	
	// nur auf Detailseite linken, wenn modul "CAL" auch installiert ist! -------------------------------------
	$sHref = ''; // set default
	if (isset($aENV['module']['adr'])) {
		// eigene termine ermitteln
		$oCal->addMyDates($oDb); // params: &$oDb
		// default ueberschreiben
		$sHref = 'mbl_cal_view.php?&date_ts={TS}&calMonth='.$calMonth.'&calYear='.$calYear; // ({TS} = Platzhalter -> wird erstzt!)
		if ($aENV['cal']['cwweeks'] == true) {
			// ggf. KW verlinken
			###$sKwHref = $aENV['path']['cal']['http'].'cal_calendar.php?&view=week&date_ts={TS}'; // ({TS} = Platzhalter -> wird erstzt!)
		}
	} // END CAL ----------------------------------------------------------------------------------------------
	
    // output calendar
	echo "<br />\n".$oCal->getMinicalendarWap($sHref, '', $aENV['cal']['cwweeks'], $sKwHref); // params: [$sHref=''][,$sButAddGetVars=''][,$bWithCw=false][,$sCwHref=''] 
?>

<?php require_once ("./inc.mbl_footer.php"); ?>