<?php
/**
* mbl_cal_detail.php
*
* Mobile Services Kalender-Detailseite 
*
* @param	int		$id			welcher Datensatz
* @param	string	$view		[zum richtigen zurueckspringen] (optional)
* @param	string	$filter		[zum richtigen zurueckspringen] (optional)
* @param	string	$office_id	[zur Unterscheidung der Ansicht] (optional)
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	int		$date_ts	[timestamp zum anzeigen von bestimmten tagen/wochen/.. (bei NEW)] (optional)
* @param	string	$time_start	[zum anzeigen einer bestimmten start-zeit (bei NEW)] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mbl_cal_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");

// date
	$today_ts		= time();

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : 0;
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : '';
	$filter			= (isset($_GET['filter'])) ? $oFc->make_secure_string($_GET['filter']) : '';
	$office_id		= (isset($_GET['office_id'])) ? $oFc->make_secure_int($_GET['office_id']) : 0;
	$time_start		= (isset($_GET['time_start'])) ? $oFc->make_secure_string($_GET['time_start']) : '';
	$date_ts		= (isset($_GET['date_ts'])) ? $oFc->make_secure_int($_GET['date_ts']) : $today_ts; // default: heute
	$calMonth		= (isset($_GET['calMonth'])) ? $oFc->make_secure_string($_GET['calMonth']) : '';
	$calYear		= (isset($_GET['calYear'])) ? $oFc->make_secure_string($_GET['calYear']) : '';
	$view			= (isset($_GET['view'])) ? $oFc->make_secure_string($_GET['view']) : 'day';
	$edit			= (isset($_GET['edit'])) ? $oFc->make_secure_string($_GET['edit']) : "false";
	
	$sGEToptions	= '?view=day&date_ts='.$date_ts.'&calMonth='.$calMonth.'&calYear='.$calYear;
	if (!empty($filter))	{ $sGEToptions .= '&filter='.$filter; } // filter-vars nur bei bedarf!
	if (!empty($office_id))	{ $sGEToptions .= '&office_id='.$office_id; }
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars
	$sTable			= $aENV['table']['cal_event'];
	$sViewerPage	= "mbl_cal_view.php".$sGEToptions;	// fuer BACK-button
	$oDate->set_timestamp($date_ts); // init timestamp in date-object
	
// TREE ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->initialize();
	
// 3. DB
// DB-action: delete
	if ((isset($btn['delete']) && !empty($aData['id']))) {
		$oTACEA->onDeleteAction($aData['id'],'cal');
		$oDb->make_delete($aData['id'], $sTable);
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) && isset($aData['subject'])) { //subject=mindestbedingung!
		// checkboxen (kommen als array) zu string implodieren
		$aData['participant']	= (is_array($aData['participant'])) ? implode(',', $aData['participant']) : $aData['participant'];
		$user = $aData['participant'];
		unset($aData['participant']);
		$aData['office']		= (is_array($aData['office'])) ? implode(',', $aData['office']) : $aData['office'];
		// checkbox defaults
		if (!isset($aData['office']) || empty($aData['office'])) {
				$aData['flag_private_event'] = '1';
		}
		if (!isset($aData['flag_private_event'])) { $aData['flag_private_event'] = '0'; }
		
		// Operation Zeitänderung!
		$aTimeStart = explode(':',$aData['time_start']);
		$aTimeEnd = explode(':',$aData['time_end']);

		if($aTimeStart[0] < 0 || $aTimeStart[0] > 23) $aTimeStart[0] = date('H');
		if($aTimeEnd[0] < 0 || $aTimeEnd[0] > 23) $aTimeEnd[0] = date('H');
		if($aTimeStart[1] < 0 || $aTimeStart[1] > 59) $aTimeStart[1] = 0;
		if($aTimeEnd[1] < 0 || $aTimeEnd[1] > 59) $aTimeEnd[1] = 59;
		
		if($aTimeStart[1] < 15) $aTimeStart[1] = '00';
		if($aTimeStart[1] >= 15 && $aTimeStart[1] <= 30) {
			$aTimeStart[1] = 30;
		}
		
		if($aTimeStart[1] >= 30 && $aTimeStart[1] < 45) {
			$aTimeStart[1] = 30;
		}
		if($aTimeStart[1] >= 45 && $aTimeStart[1] < 59) {
			$aTimeStart[1] = '00';
			$aTimeStart[0]++;
		}
		
		if($aTimeEnd[1] < 15) $aTimeEnd[1] = '00';
		if($aTimeEnd[1] >= 15 && $aTimeEnd[1] < 30) {
			$aTimeEnd[1] = 30;
		}
		if($aTimeEnd[1] >= 30 && $aTimeEnd[1] < 45) {
			$aTimeEnd[1] = 30;
		}
		if($aTimeEnd[1] >= 45 && $aTimeEnd[1] < 59) {
			$aTimeEnd[1] = '00';
			$aTimeEnd[0]++;
		}
		$aData['time_start'] = implode(':',$aTimeStart);
		$aData['time_end'] = implode(':',$aTimeEnd);

		// datetime start zusammenbauen
		list($d, $m, $y) = explode('.', $aData['date_start']);
		$aData['event_start']	= $y.'-'.$m.'-'.$d.' '.$aData['time_start'].':00';
		unset($aData['date_start']); unset($aData['time_start']); // nicht vorhandene felder loeschen
		// datetime end zusammenbauen
		list($d, $m, $y) = explode('.', $aData['date_end']);
		$aData['event_end']		= $y.'-'.$m.'-'.$d.' '.str_replace('24:00', '23:59', $aData['time_end']).':00'; // datetime geht nur bis 23:59!
		unset($aData['date_end']); unset($aData['time_end']); // nicht vorhandene felder loeschen
		// projekt-id auf 0 setzen
		$aData['project_id'] = 0;
		
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			$oDb->make_update($aData, $sTable, $aData['id']);
		}
		else { // Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			// make insert
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id();
		}		
		if ($flagopen == 1) {		
			$oTACEA->delRightForTree($treeid);
		}
		else {			
			$oTACEA->addRights($group, $user,$aData['id'],'cal');
		}
		
		if ($aData['id'] > 0) { header("Location: ".$sViewerPage); exit; }
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		// datetime start trennen
		list($aData['date_start'], $aData['time_start']) = explode(' ', $aData['event_start']);
		list($y, $m, $d) = explode('-', $aData['date_start']);
		$aData['date_start']	= $d.'.'.$m.'.'.$y;
		$aData['time_start']	= substr($aData['time_start'], 0, 5);
		// datetime end trennen
		list($aData['date_end'], $aData['time_end']) = explode(' ', $aData['event_end']);
		list($y, $m, $d) = explode('-', $aData['date_end']);
		$aData['date_end']	= $d.'.'.$m.'.'.$y;
		$aData['time_end']	= substr($aData['time_end'], 0, 5);
	} else {
		// set defaults
		$aData['date_start'] = $oDate->get_today();
		$aData['time_start'] = date("H").':00';
		$aData['date_end'] = $oDate->get_today();
		$aData['time_end'] = (date("H")+1).':00';
		if (!empty($time_start)) { // wenn eine startzeit per GET uebergeben wurde
			$aData['time_start'] = $time_start;
			$time_end = substr($time_start, 0, 1).(substr($time_start, 1, 1) + 1).substr($time_start, 2);
			$aData['time_end'] = $time_end;
		}
		$aData['office'] = (isset($_GET['office_id'])) ? $_GET['office_id'] : $Userdata['office_id'];
	}
	
	 // USER + OFFICE PHP ........................................................................
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUser = $oUser->getAllUserData(); // params: [$bValidOnly=true]
	$aOffice = $oUser->getAllOfficeNames(); // params: [$bWithUsers=false]

// 4. HTML
	require_once ("./inc.mbl_header.php");

if (isset($id) && $id > 0 && $edit == "false") { // Detailansicht ?>
	<p>
	<!-- <b><?php echo $aMSG['cal']['schedule'][$syslang]; ?></b> &nbsp;|&nbsp; -->
	<a href="<?php echo $sViewerPage; ?>" title="<?php echo $aMSG['mbl']['overview'][$syslang]; ?>"><?php echo $aMSG['mbl']['overview'][$syslang]; ?></a>
	 &nbsp;|&nbsp; <a href="<?php echo $_SERVER['PHP_SELF'].$sGEToptions; ?>&id=<?php echo $id; ?>&edit=true" title="<?php echo $aMSG['std']['edit'][$syslang]; ?>"><?php echo $aMSG['std']['edit'][$syslang]; ?></a>
	</p>
	
	<div class="titleRow"><b><?php echo $aData['subject']; ?></b> (<?php echo $aScheduleLabel[$syslang][$aData['label']]; ?>)</div>
	<div class="textRow">
		<?php
		if ($aData['location'] != "") {
			echo '<b>'.$aMSG['form']['location'][$syslang].':</b> '.$aData['location'].'<br /><br />';
		}
		
		echo '<b>'.$aMSG['form']['date_start'][$syslang].':</b><br />';
		echo $aData['date_start'];
		if ($aData['time_start'] != "") { echo ' '.$aData['time_start']; }
		if ($aData['event_arrival'] != "") { echo ' + '.$aData['event_arrival'].' '.$aMSG['form']['arrival'][$syslang]; }
		echo '<br /><br />';
		
		echo '<b>'.$aMSG['form']['date_end'][$syslang].':</b><br />';
		echo $aData['date_end'];
		if ($aData['time_end'] != "") { echo ' '.$aData['time_end']; }
		if ($aData['event_return'] != "") { echo ' + '.$aData['event_return'].' '.$aMSG['form']['return'][$syslang]; }
		echo '<br />';
		
		if ($aData['flag_allday'] == 1) { echo '<b>'.$aMSG['form']['flag_allday'][$syslang].'</b><br />'; }
		echo '<br />';
		
		if (isset($aENV['module']['adm']) && $aData['project_id'] > 0) {
			echo '<b>'.$aMSG['form']['project'][$syslang].':</b><br />';
			
			$oDb->query("SELECT p.client_id, p.id, p.projectname, p.prj_nr, c.client_nr, c.client_shortname
						FROM adm_project p, adr_contact c 
						WHERE c.id = p.client_id 
						AND p.id = '".$aData['project_id']."'");
			$tmp = $oDb->fetch_array();
			echo '<span>'.$tmp['projectname'].' [ '.$tmp['client_nr'].'.'.$tmp['prj_nr'].' ]</span><br /><br />';
		} // END if
		
		if ($aData['comment'] != "") {
			echo '<b>'.$aMSG['form']['comment'][$syslang].':</b><br />'.$aData['comment'].'<br /><br />';
		}
		
		echo '<b>'.$aMSG['form']['participant'][$syslang].':</b><br />';

		echo _getValidUserNames($aUser, $oTACEA->getRightForID($aData['id'],'cal')).'<br />';
		
		echo '<b>'.$aMSG['form']['visibility'][$syslang].':</b><br />';
		echo _getPrivateNames($aMSG['form']['flag_private_event'][$syslang], $aData['flag_private_event']);
		echo _getOfficeNames($aOffice, $aData['office']);
		?>	
	</div>
<?php
}
else {	// neu anlegen / editieren
	$sHeaderText = ($edit == "true") ? $aMSG['mbl']['edit_event'][$syslang] : $aMSG['mbl']['new_event'][$syslang];
?>
	<p><b><?php echo $sHeaderText; ?></b> &nbsp;|&nbsp; <a href="<?php echo $sViewerPage; ?>" title="<?php echo $aMSG['mbl']['day_overview'][$syslang]; ?>"><?php echo $aMSG['mbl']['day_overview'][$syslang]; ?></a></p>
	<form action="<?php echo $_SERVER['PHP_SELF'].$sGEToptions; ?>" method="post" name="editForm" id="editForm">
	<input type="hidden" name="aData[id]" id="event_id" value="<?php echo $id; ?>" />	
	<b><?php echo $aMSG['form']['subject'][$syslang]; ?>:</b><br />
	<input type="text" name="aData[subject]" id="subject" value="<?php echo (isset($aData['subject'])) ? $aData['subject'] : ""; ?>" size="18" maxlength="150" class="input" /><br />
	<select name="aData[label]" id="label" size="1" class="select">
	<?php
	foreach ($aScheduleLabel[$syslang] as $key => $val) {
		$sel = (isset($aData['label']) && $aData['label'] == $key) ? ' selected="selected"' : '';
		echo '<option value="'.$key.'"'.$sel.'>'.$val.'</option>'."\n";
	}
	?>
	</select><br />
	
	<b><?php echo $aMSG['form']['location'][$syslang]; ?>:</b><br />
	<input type="text" name="aData[location]" id="location" value="<?php echo (isset($aData['location'])) ? $aData['location'] : ""; ?>" size="18" maxlength="150" class="input" /><br />
	
	<b><?php echo $aMSG['form']['date_start'][$syslang]; ?>:</b><br />
	<input type="text" name="aData[date_start]" id="date_start" value="<?php echo $aData['date_start']; ?>" size="8" maxlength="10" class="smallinput" />
	<input type="text" name="aData[time_start]" id="time_start" value="<?php echo $aData['time_start']; ?>" size="5" maxlength="5" class="smallinput" />
<? /*
	<select name="aData[time_start]" id="time_start" size="1" class="select">
	<?php
	$aTime = _getTimeOptions();
	foreach ($aTime as $key) {
		$sel = ($key == $aData['time_start']) ? ' selected="selected"' : '';
		echo '<option value="'.$key.'"'.$sel.'>'.$key."</option>\n";
	}
	?>
	</select>
	*/ ?>
	<br />
	
	<b><?php echo $aMSG['form']['date_end'][$syslang]; ?>:</b><br />
	<input type="text" name="aData[date_end]" id="date_end" value="<?php echo $aData['date_end']; ?>" size="8" maxlength="10" class="smallinput" />
	<input type="text" name="aData[time_end]" id="time_end" value="<?php echo $aData['time_end']; ?>" size="5" maxlength="5" class="smallinput" />
	
	<? /*
	<select name="aData[time_end]" id="time_end" size="1" class="select">
	<?php
	$aTime = _getTimeOptions();
	foreach ($aTime as $key) {
		$sel = ($key == $aData['time_end']) ? ' selected="selected"' : '';
		echo '<option value="'.$key.'"'.$sel.'>'.$key."</option>\n";
	}
	?>
	</select>
	*/ ?>
	<br />
	
	<input type="hidden" name="aData[flag_allday]" value="0" />
	<?php echo $aMSG['form']['flag_allday'][$syslang]; ?> <input type="checkbox" name="aData[flag_allday]" id="flag_allday" value="1" class="radio"<?php echo ($aData['flag_allday'] == 1) ? ' checked="checked"' : ""; ?>/><br />
	<br />
	<b><?php echo $aMSG['form']['comment'][$syslang]; ?>:</b><br />
	<textarea rows="2" cols="18" name="aData[comment]" id="comment" class="input"><?php echo (isset($aData['comment'])) ? $aData['comment'] : ""; ?></textarea><br />
	
<?php
	// USER + OFFICE HTML ........................................................................ ?>
	<b><?php echo $aMSG['form']['participant'][$syslang]; ?>:</b><br />
	<?php echo _getValidUserCheckboxes($aUser, $oTACEA->getRightForID($aData['id'],'cal')); ?><br />
		
	<b><?php echo $aMSG['form']['visibility'][$syslang]; ?>:</b><br />
	<?php echo _getPrivateCheckbox($aMSG['form']['flag_private_event'][$syslang], $aData['flag_private_event']); ?>
	<?php echo _getOfficeCheckboxes($aOffice, $aData['office']); ?>
	<br />
<?php // END USER + OFFICE HTML ........................................................................ ?>

	<input type="submit" value=" <?php echo $aMSG['btn']['save'][$syslang]; ?> " class="button" name="btn[save]" id="save" />
	<?php if ($edit == "true") { ?>
	<input type="submit" value=" <?php echo $aMSG['btn']['delete'][$syslang]; ?> " class="button" name="btn[delete]" id="delete" />	
	<?php } ?>
	</form><br />
<?php
} // END neu anlegen

require_once ("./inc.mbl_footer.php");

// HILFSFUNKTIONEN /////////////////////////////////////////////////////////////

	function &_getTimeOptions($from=null) {
		$buffer = array();
		$from = (!is_null($from) && !empty($from)) ? substr($from, 0, 2) : 0;
		for ($i=$from; $i < 24; $i++) {
			if (strlen($i) == 1) $i = '0'.$i;
			$buffer[$i.':00'] = $i.':00';
			$buffer[$i.':30'] = $i.':30';
		}
		if (!is_null($from) && !empty($from)) {
			$buffer['24:00'] = '24:00';
		}
		return $buffer;
	}

	function &_getValidUserCheckboxes(&$aUser, $aCheckedUsers) {
		// get all valid usernames
		$num = count($aUser);
		if ($num == 0) return; // fallback
		
		$buffer = '';
		foreach ($aUser as $u_id => $user) {
			// checked?
			$sel = (isset($aCheckedUsers[$u_id]) && is_array($aCheckedUsers[$u_id])) ? ' checked="checked"' : '';
			// output
			$buffer .= '<input type="checkbox" name="aData[participant][]" id="cbxU'.$u_id.'" value="'.$u_id.'"'.$sel.' class="radio" />&nbsp;&nbsp;';
			$buffer .= '<span>'.$user['name']."</span><br />\n";
		}
		return $buffer;
	}
	
	function &_getValidUserNames(&$aUser, $aCheckedUsers) {
		// get all valid usernames
		$num = count($aUser);
		if ($num == 0) return; // fallback
		
		$buffer = '';
		foreach ($aUser as $u_id => $user) {
			// output
			if (isset($aCheckedUsers[$u_id]) && is_array($aCheckedUsers[$u_id])) { $buffer .= '<span>'.$user['name']."</span><br />\n"; }
		}
		return $buffer;
	}

	function &_getOfficeCheckboxes(&$aOffice, $sCheckedValue) {
		global $Userdata;
		$aCheckedValue = explode(',', $sCheckedValue);
		// get all valid usernames
		$num = count($aOffice);
		if ($num == 0) return; // fallback
		
		$buffer = '';
		foreach ($aOffice as $o_id => $o_name) {
			// checked?
			$sel = (in_array($o_id, $aCheckedValue)) ? ' checked="checked"' : '';
			// output
			$buffer .= '<input type="checkbox" name="aData[office][]" id="cbxO'.$o_id.'" value="'.$o_id.'"'.$sel.' class="radio" />&nbsp;&nbsp;';
			$buffer .= '<span>'.$o_name."</span><br />\n";
		}
		return $buffer;
	}
	
	function &_getOfficeNames(&$aOffice, $sCheckedValue) {
		global $Userdata;
		$aCheckedValue = explode(',', $sCheckedValue);
		// get all valid usernames
		$num = count($aOffice);
		if ($num == 0) return; // fallback
		
		$buffer = '';
		foreach ($aOffice as $o_id => $o_name) {
			// output
			if (in_array($o_id, $aCheckedValue)) { $buffer .= '<span>'.$o_name."</span><br />\n"; }
		}
		return $buffer;
	}

	function &_getPrivateCheckbox($sLabel, $sCheckedValue) {
		// output
		$sel = ($sCheckedValue == 1) ? ' checked="checked"' : '';
		$buffer = '<input type="checkbox" name="aData[flag_private_event]" id="flag_private_event" value="1"'.$sel.' class="radio" />&nbsp;&nbsp;';
		$buffer .= '<span>'.$sLabel."</span><br />\n";
		
		return $buffer;
	}
	
	function &_getPrivateNames($sLabel, $sCheckedValue) {
		// output
		$buffer = '';
		if ($sCheckedValue == 1) { $buffer = '<span>'.$sLabel."</span><br />\n"; }
		
		return $buffer;
	}
 ?>
