<?php
/**
* mbl_adr.php
*
* Mobile Services Adressen - Einstieg 
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.sys_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.0 
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mbl_sys_usr.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "sys_user";

// 3. HTML
	require_once ("./inc.mbl_header.php");

// 4. DB-action: select
	$sql = "SELECT * FROM ".$sTable.' WHERE flag_da_service=\'0\' AND flag_deleted=\'0\'';
	$aData = $oDb->fetch_in_aarray($sql);
?>

	<br />
	
<?	for($i=0;is_array($aData[$i]);$i++) {
?>
	<div class="textRow">
		<?=$aData[$i]['firstname']?> <?=$aData[$i]['surname']?>
		<br />
		<?=$aMSG['std']['last_login'][$syslang]?>: <?=$oDate->datetime_mysql2trad($aData[$i]['last_login'])?>
	</div>
<? } ?>
<?php
 require_once ("./inc.mbl_footer.php"); ?>