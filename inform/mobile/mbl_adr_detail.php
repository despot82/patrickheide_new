<?php
/**
* mbl_adr_detail.php
*
* Mobile Services Adress-Detailseite
*
* @param	int		$id			welcher Datensatz
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	int		$type		[company|contact] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.intra_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Oliver Hilscher <oh@design-aspekt.com>
* @version	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './mbl_adr_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/inc.sys_login.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : 0;
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	$type			= (isset($_GET['type'])) ? $oFc->make_secure_string($_GET['type']) : '';
	$sGEToptions	= "?start=".$start;
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= "adr_contact";
	$sViewerPage	= "mbl_adr.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $_SERVER['PHP_SELF'].$sGEToptions;				// fuer NEW-button

// SHARED BEGIN ****************************************************************************
	require_once ($aENV['path']['adr']['unix']."inc.adr_functions.php");
// END SHARED *****************************************************************************

// 3. DB
// DB-action: 1. get data for country names
	$aCountry = _get_all_countries($oDb); // von "inc.adr_functions.php"
	
// DB-action: 2. get data CONTENT
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		// set type (fallback)
		$type = (!isset($aData['company']) || empty($aData['company'])) ? "contact" : "company";
		// ggf. edit-/delete-rechte reduzieren (wenn der akt. nutzer nicht der eigentuemer ist)
		if (($oPerm->hasPriv('delete') || $oPerm->hasPriv('edit')) && !$oPerm->hasPriv('admin') && $Userdata['id'] != $aData['created_by']) {
			if ($oPerm->hasPriv('edit')) $oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
			if ($oPerm->hasPriv('delete')) $oPerm->removePriv('delete'); // params: $sPrivilege[,$sModule='']
		}
	} else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ("./inc.mbl_header.php");
?>

	<p>
	<a href="<?php echo $sViewerPage; ?>" title="<?php echo $aMSG['mbl']['overview'][$syslang]; ?>"><?php echo $aMSG['mbl']['overview'][$syslang]; ?></a>
	</p>
	
	<div class="titleRow">
		<?php
		/* Test
		echo format_mobilephone('0049-8953072917').'<br />';
		echo format_mobilephone('0049-8953072917', '+49').'<br />';
		echo format_mobilephone('08953072917', '49').'<br />';
		echo format_mobilephone('8953072917', '+49').'<br />';
		echo format_mobilephone('(0)89-530729 17', '+49').'<br />';
		echo format_mobilephone('+49(0)89-530729 17', '49').'<br /><br />';	// Fall der praktisch eigentlich nicht möglich ist
		*/
		
		if ($type == "contact") { // name / title
			echo '<b>'.$aData['title'].' '.$aData['firstname'].' '.$aData['surname']."</b><br />\n";
		}
		else { // companyname
			echo '<b>'.nl2br_cms($aData['company'])."</b><br />\n";
		}
		
		if ((isset($aData['grp']) && !empty($aData['grp']))
		||	(isset($aData['birthday']) && !empty($aData['birthday']) && $aData['birthday'] != '0000-00-00')
		||	(isset($aData['client_nr']) && !empty($aData['client_nr']))	) {
			if (isset($aData['grp']) && !empty($aData['grp'])) {
				echo '<b>'.$aMSG['form']['group'][$syslang].':</b> '; // group headline
				$oDb->query("SELECT title_".$syslang." FROM adr_group WHERE id='".$aData['grp']."'");
				if ($oDb->num_rows()) { echo ''.$oDb->fetch_field("title_".$syslang); }
				echo "<br />\n";
			}
			if (isset($aData['birthday']) && !empty($aData['birthday']) && $aData['birthday'] != '0000-00-00') {
				echo '<b>'.$aMSG['form']['birthday'][$syslang].':</b> '; // birthday headline
				echo $oDate->date_mysql2trad($aData['birthday']);
				echo "<br />\n";
			}
			if (isset($aData['client_nr']) && !empty($aData['client_nr']) && $aData['client_nr'] != '000') {
				echo '<b>'.$aMSG['form']['client_nr'][$syslang].':</b> '; // client_nr headline
				echo ''.$aData['client_nr'].'';
				echo "<br />\n";
			}
		} // END if
		?>
	</div>

	<div class="textRow">
<?php // ANSICHT "CONTACT" #############################################################################
if ($type == "contact") {
	// PRIVAT
	$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='p'");
	if ($oDb->num_rows()) {
		echo "<b>".$aMSG['address']['p'][$syslang]."</b><br />\n";
		$aData2 = $oDb->fetch_array();
		// name
		if (isset($aData2['recipient']) && !empty($aData2['recipient'])) {
			echo $aData2['recipient']."<br />\n";
		} else if (!empty($aData2['id'])) {
			echo $aData['title'].' '.$aData['firstname'].' '.$aData['surname']."<br />\n"; #echo $aData['gender'];
		}
		// address
		if (!empty($aData2['street'])) echo $aData2['street']."<br />";
		if (!empty($aData2['building'])) echo $aData2['building']."<br />";
		if (!empty($aData2['region'])) echo $aData2['region']."<br />";
		if (!empty($aData2['town'])) echo $aData2['town']."<br />";
		if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
		echo "<br />\n";
		// po-box
		if (!empty($aData2['po_box'])) {
			echo $aData2['po_box']."<br />";
			echo $aData2['po_box_town']."<br />";
			if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
			echo "<br />\n";
		}
		// phone
		if (!empty($aData2['phone_tel'])) {
			echo '<b>T</b> <a href="tel:'.format_mobilephone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		if (!empty($aData2['phone_mob'])) {
			echo '<b>M</b> <a href="tel:'.format_mobilephone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		if (!empty($aData2['phone_fax'])) {
			echo '<b>F</b> <a href="tel:'.format_mobilephone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// email
		if (!empty($aData2['email'])) echo '<b>E</b> <a href="'.link_uri($aData2['email']).'" class="subLink">'.show_uri($aData2['email'])."</a><br />\n";
		// www
		if (!empty($aData2['web'])) echo '<a href="'.link_uri($aData2['web']).'" target="_blank" class="textLink">'.link_uri($aData2['web'])."</a><br />\n";
		
		echo "<br />\n";
	}	// END PRIVAT
	
	// 1. OFFICE
	// personal comany-data
	$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='o1'");
	if ($oDb->num_rows()) {
		echo "<b>".$aMSG['address']['o1'][$syslang]."</b><br />\n";
		$aData3 = $oDb->fetch_array();
		// name
		if (!empty($aData3['id'])) {
			echo $aData['title'].' '.$aData['firstname'].' '.$aData['surname']."<br />\n";
		}
		// firmenname (+client-id) mit link zu seiner detailseite
		$oDb->query("SELECT company,client_nr FROM ".$sTable." WHERE id=".$aData3['company_id']);
		$tmp = $oDb->fetch_array();
		echo '<a href="'.$_SERVER['PHP_SELF']."?id=".$aData3['company_id']."&start=".$start.'&type=company" class="textLink">'.nl2br_cms($tmp['company'])."</a><br />\n";
		
		// global comany-data
		$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$aData3['company_id']." AND cat='c'");
		$aData2 = $oDb->fetch_array();
		// address
		if (!empty($aData2['street'])) echo $aData2['street']."<br />";
		if (!empty($aData2['building'])) echo $aData2['building']."<br />";
		if (!empty($aData2['region'])) echo $aData2['region']."<br />";
		if (!empty($aData2['town'])) echo $aData2['town']."<br />";
		if (!empty($aData2['street']) && !empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
		if (!empty($aData2['town']) && !empty($aData2['po_box'])){ echo "<br />";}
		
		// po-box
		if (!empty($aData2['po_box'])) {
			echo $aData2['po_box']."<br />";
			echo $aData2['po_box_town']."<br />";
			if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
		}
		echo "<br />\n";
		
		// phone (tel)
		if (!empty($aData3['phone_tel'])) { // personal
			echo '<b>T</b> <a href="tel:'.format_mobilephone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		} else if (!empty($aData2['phone_tel'])) { // global
			echo '<b>T</b> <a href="tel:'.format_mobilephone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// phone2 (tel (mobile))
		if (!empty($aData2['phone_mob'])) { // global (company T2)
			echo '<b>M</b> <a href="tel:'.format_mobilephone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// phone (mobile)
		if (!empty($aData3['phone_mob'])) { // personal (office Mob)
			echo '<b>M</b> <a href="tel:'.format_mobilephone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// phone (fax)
		if (!empty($aData3['phone_fax'])) { // personal
			echo '<b>F</b> <a href="tel:'.format_mobilephone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		} else if (!empty($aData2['phone_fax'])) { // global
			echo '<b>F</b> <a href="tel:'.format_mobilephone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// email
		if (!empty($aData3['email'])) { // personal
			echo '<b>E</b> <a href="'.link_uri($aData3['email']).'" class="subLink">'.show_uri($aData3['email'])."</a><br />\n";
		} else if (!empty($aData2['email'])) { // global
			echo '<b>E</b> <a href="'.link_uri($aData2['email']).'" class="subLink">'.show_uri($aData2['email'])."</a><br />\n";
		}
		// www
		if (!empty($aData3['web'])) { // personal
			echo '<br><a href="'.link_uri($aData3['web']).'" target="_blank" class="textLink">'.link_uri($aData3['web'])."</a><br />\n";
		} else if (!empty($aData2['web'])) { // global
			echo '<br><a href="'.link_uri($aData2['web']).'" target="_blank" class="textLink">'.link_uri($aData2['web'])."</a><br />\n";
		}
		echo "<br />";
		// department
		if (!empty($aData3['company_dep'])) echo $aMSG['form']['department'][$syslang].': '.$aData3['company_dep']."<br />\n";
		// position
		if (!empty($aData3['company_pos'])) echo $aMSG['form']['position'][$syslang].': '.$aData3['company_pos']."<br />\n";
		if (!empty($tmp['client_nr']) && $tmp['client_nr'] != '000') echo $aMSG['form']['client_nr'][$syslang].': '.$tmp['client_nr']."<br />\n";
		
		echo "<br />\n";
	} // END 1. OFFICE
	
	// 2. OFFICE
	// personal comany-data
	$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='o2'");
	if ($oDb->num_rows()) {
		echo "<b>".$aMSG['address']['o2'][$syslang]."</b><br />\n";
		$aData3 = $oDb->fetch_array();
		// name
		if (!empty($aData3['id'])) {
			echo $aData['title'].' '.$aData['firstname'].' '.$aData['surname']."<br />\n";
		}
		// firmenname (+client-id) mit link zu seiner detailseite
		$oDb->query("SELECT company,client_nr FROM ".$sTable." WHERE id=".$aData3['company_id']);
		$tmp = $oDb->fetch_array();
		echo '<a href="'.$_SERVER['PHP_SELF']."?id=".$aData3['company_id']."&start=".$start.'&type=company">'.nl2br_cms($tmp['company'])."</a><br />\n";
		
		// global comany-data
		$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$aData3['company_id']." AND cat='c'");
		$aData2 = $oDb->fetch_array();
		// address
		if (!empty($aData2['street'])) echo $aData2['street']."<br />";
		if (!empty($aData2['building'])) echo $aData2['building']."<br />";
		if (!empty($aData2['region'])) echo $aData2['region']."<br />";
		if (!empty($aData2['town'])) echo $aData2['town']."<br />";
		if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
		if (!empty($aData2['town']) && !empty($aData2['po_box'])){ echo "<br />";}
		
		// po-box
		if (!empty($aData2['po_box'])) {
			echo $aData2['po_box']."<br />";
			echo $aData2['po_box_town']."<br />";
			if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
		}
		echo "<br />\n";
		
		// phone (tel)
		if (!empty($aData3['phone_tel'])) { // personal
			echo '<b>T</b> <a href="tel:'.format_mobilephone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		} else if (!empty($aData2['phone_tel'])) { // global
			echo '<b>T</b> <a href="tel:'.format_mobilephone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// phone2 (tel (mobile))
		if (!empty($aData2['phone_mob'])) { // global (company T2)
			echo '<b>M</b> <a href="tel:'.format_mobilephone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// phone (mobile)
		if (!empty($aData3['phone_mob'])) { // personal (office Mob)
			echo '<b>M</b> <a href="tel:'.format_mobilephone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// phone (fax)
		if (!empty($aData3['phone_fax'])) { // personal
			echo '<b>F</b> <a href="tel:'.format_mobilephone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		} else if (!empty($aData2['phone_fax'])) { // global
			echo '<b>F</b> <a href="tel:'.format_mobilephone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		}
		// email
		if (!empty($aData3['email'])) { // personal
			echo '<b>E</b> <a href="'.link_uri($aData3['email']).'" class="subLink">'.show_uri($aData3['email'])."</a><br />\n";
		} else if (!empty($aData2['email'])) { // global
			echo '<b>E</b> <a href="'.link_uri($aData3['email']).'" class="subLink">'.show_uri($aData3['email'])."</a><br />\n";
		}
		// www
		if (!empty($aData3['web'])) { // personal
			echo '<br><a href="'.link_uri($aData3['web']).'" target="_blank" class="textLink">'.link_uri($aData3['web'])."</a><br />\n";
		} else if (!empty($aData2['web'])) { // global
			echo '<br><a href="'.link_uri($aData3['web']).'" target="_blank" class="textLink">'.link_uri($aData3['web'])."</a><br />\n";
		}
		echo "<br />";
		// department
		if (!empty($aData3['company_dep'])) echo $aMSG['form']['department'][$syslang].': '.$aData3['company_dep']."<br />\n";
		// position
		if (!empty($aData3['company_pos'])) echo $aMSG['form']['position'][$syslang].': '.$aData3['company_pos']."<br />\n";
		if (!empty($tmp['client_nr']) && $tmp['client_nr'] != '000') echo $aMSG['form']['client_nr'][$syslang].': '.$tmp['client_nr']."<br />\n";
		
		echo "<br />\n";
	} // END 2. OFFICE ?>
		
<?php // ANSICHT "COMPANY" #############################################################################
} else {
	// OFFICE
	echo "<b>".$aMSG['address']['c'][$syslang]."</b><br />\n";
	$oDb->query("SELECT * FROM adr_address WHERE reference_id=".$id." AND cat='c'");
	$aData2 = $oDb->fetch_array();
	// company-name
	echo nl2br_cms($aData['company'])."<br />\n";
	
	// address
	if (!empty($aData2['street'])) echo $aData2['street']."<br />";
	if (!empty($aData2['building'])) echo $aData2['building']."<br />";
	if (!empty($aData2['region'])) echo $aData2['region']."<br />";
	if (!empty($aData2['town'])) echo $aData2['town']."<br />";
	if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
	if (!empty($aData2['town']) && !empty($aData2['po_box'])) { echo "<b /r>"; }
		
	// po-box
	if (!empty($aData2['po_box'])) {
		echo $aData2['po_box']."<br />";
		echo $aData2['po_box_town']."<br />";
		if (!empty($aData2['country'])) echo $aCountry[$aData2['country']]['name']."<br />";
	}
	
	echo "<br>\n";
	
	// phone
	if (!empty($aData2['phone_tel'])) {
		echo '<b>T</b> <a href="tel:'.format_mobilephone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_tel'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
	}
	if (!empty($aData2['phone_mob'])) {
		echo '<b>M</b> <a href="tel:'.format_mobilephone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_mob'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
	}
	if (!empty($aData2['phone_fax'])) {
		echo '<b>F</b> <a href="tel:'.format_mobilephone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData2['phone_fax'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
	}
	// email
	if (!empty($aData2['email'])) echo '<b>E</b> <a href="'.link_uri($aData2['email']).'" class="subLink">'.show_uri($aData2['email'])."</a><br />\n";
	echo "<br>\n";
	// www
	if (!empty($aData2['web'])) echo '<a href="'.link_uri($aData2['web']).'" target="_blank" class="textLink">'.link_uri($aData2['web'])."</a><br />\n";
	
	echo "<br />\n";
	// END OFFICE

	// STAFF
	echo "<b>".$aMSG['address']['s'][$syslang]."</b><br />\n";
	// groups zwischenspeichern
	$aGroup = _get_all_groups($oDb); // von "inc.adr_functions.php"
	
	// stamm-daten + personal office-daten ausgeben
	$aData3 = array();
	$oDb->query("	SELECT	a.reference_id,
					 		a.cat,
							a.country,
							a.phone_tel,
							a.phone_mob,
							a.phone_fax,
							a.email,
							a.web,
							a.company_dep,
							a.company_pos,
							c.id,
							c.firstname,
							c.surname,
							c.grp
					FROM	adr_address a, 
							adr_contact c 
					WHERE	a.company_id='".$id."' 
					AND		a.reference_id=c.id	
					ORDER BY c.grp, c.surname, c.firstname");
	while ($aData3 = $oDb->fetch_array()) {
		// name (verlinkt) + group/department/position
		echo '<a href="'.$_SERVER['PHP_SELF']."?id=".$aData3['id']."&start=".$start.'&type=contact" class="textLink">';
		if (empty($aData3['surname'])) {
			$name = $aData3['firstname'];
		} else if (empty($aData3['firstname'])) {
			$name = $aData3['surname'];
		} else {
			$name = $aData3['surname'].', '.$aData3['firstname'];
		}
		echo $name;
		echo "</a><br />\n";
		if (!empty($aData3['company_dep'])) echo $aData3['company_dep']."<br />\n";
		if (!empty($aData3['company_pos'])) echo $aData3['company_pos']."<br />\n";
		echo $aMSG['form']['group'][$syslang].': '.$aGroup[$aData3['grp']].'<br />';
		
		// phone/email/web
		if (!empty($aData3['phone_tel'])) echo '<b>T </b><a href="tel:'.format_mobilephone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_tel'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		if (!empty($aData3['phone_mob'])) echo '<b>M </b><a href="tel:'.format_mobilephone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_mob'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		if (!empty($aData3['phone_fax'])) echo '<b>F </b><a href="tel:'.format_mobilephone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone']).'" class="subLink">'.format_phone($aData3['phone_fax'], $aCountry[$aData2['country']]['phone'])."</a><br />\n";
		if (!empty($aData3['email'])) echo '<b>E </b> <a href="'.link_uri($aData3['email']).'" class="subLink">'.show_uri($aData3['email'])."</a><br />\n";
		if (!empty($aData3['web'])) echo '<a href="'.link_uri($aData3['web']).'" target="_blank" class="textLink">'.link_uri($aData3['web'])."</a><br />\n";
		
		echo "<br />\n";
	}
	// END STAFF ?>
		
<?php
} // END ANSICHT ############################################################################# ?>

<?php // CUG-Data -------------------------------------------------------
	$oDb->query("SELECT id, contact_id, login FROM cug_user WHERE contact_id='".$id."'");
	if ($oDb->num_rows() > 0) {
		$aData2 = $oDb->fetch_array();
			if ($aData['description'] != "") { // description
			echo "<b>".$aMSG['address']['description'][$syslang]."</b><br />\n";
			echo nl2br_cms($aData['description'])."<br /><br />\n";
			}
		
			if ($aData['comment'] != "") { // comment
			echo "<b>".$aMSG['address']['comment'][$syslang]."</b><br />\n";
			echo nl2br_cms($aData['comment'])."<br /><br />\n";
			}	
		
			echo $aMSG['form']['cugusergroup'][$syslang]."<br />";
			require_once($aENV['path']['global_service']['unix']."class.cug.php");
			$oCug =& new cug($aENV['db'], $syslang);
			$aUg = $oCug->getUsergroupByUid($aData2['id']); // params: $sUserId
			foreach ($aUg as $ug_id => $ug_title) {
				echo ''.$ug_title.'<br />';
			}
	} else {
		if ($aData['description'] != "") { // description
			echo "<b>".$aMSG['address']['description'][$syslang]."</b><br />\n";
			echo nl2br_cms($aData['description'])."<br /><br />\n";
		}
		
		if ($aData['comment'] != "") { // comment
			echo "<b>".$aMSG['address']['comment'][$syslang]."</b><br />\n";
			echo nl2br_cms($aData['comment'])."<br /><br />\n";
		}
	} // END CUG ------------------------------------------------------- ?>

	</div>
	<br />

<?php require_once ("./inc.mbl_footer.php"); ?>