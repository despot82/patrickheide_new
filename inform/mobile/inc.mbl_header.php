<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mbl_header.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// basename(-teile) auslesen fuer status-highlight(n)ing
	$sBsname	= basename($_SERVER['PHP_SELF']);				// Name der Datei auslesen
	$sBasename	= substr($sBsname, 0, strrpos($sBsname, "."));	// .php abschneiden
	$aBnParts	= explode("_",$sBasename);						// $aBnParts Array erzeugen

/* 
Funktioniert leider bei den Sony-Ericssons nicht:
echo '<?xml version="1.0" encoding="'.$aENV['charset'].'"?>'."\n";
echo '<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">'."\n";
echo '<html xmlns="http://www.w3.org/1999/xhtml">'."\n";
... deshalb HTML 4.01 Transitional:
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title><?php echo $aENV['client_shortname'].' Mobile'; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $aENV['charset']; ?>" />
	
	<style type="text/css">
	body					{ font-family: sans-serif; font-weight: normal; font-size: x-small; color: #000000; background-color: #dddddd; margin: 2%; padding: 0px; }
	#content				{ float: none; width: auto; max-width: 600px; }
	#content, p, div, span	{ font-family: sans-serif; font-weight: normal; font-size: x-small; color: #000000; }	
	a, .textLink			{ color: #ff3300; }
	.naviLink				{ color: #ffffff; }
	.subLink				{ color: #666666; }
	img 					{ max-width: 100%; border-style: none; vertical-align: middle; margin: 0px; }	
	.topNavi				{ display: block; background-color: #000000; padding: 0px; }	
	.titleRow				{ background-color: #c6c6c6; margin: 0px; padding: 3px; }
	.textRow				{ background-color: #ffffff; margin: 0px 0px 1% 0px; padding: 3px; }
	.noborder				{ border-style: none; }
	
	form					{ display: inline; }
	.input, .smallinput, .button, .select { font-family: sans-serif; font-weight: normal; font-size: x-small; color: #000000; border-width: 1px; border-style: solid; padding: 1px; }
	.input, .select			{ width: 90%; max-width: 200px; background-color: #ffffff; border-color: #000000 #cccccc #cccccc #000000; margin: 0px 0px 0px 0px; }
	.smallinput				{ max-width: 200px; background-color: #ffffff; border-color: #000000 #cccccc #cccccc #000000; margin: 0px 0px 0px 0px; }
	.button					{ background-color: #ffffff; border-color: #cccccc #000000 #000000 #cccccc; }
	.radio					{ vertical-align: middle; margin: 0px 0px 0px 0px; padding: 0px; border-style: none; }
	</style>
	
	<?php if ($aBnParts[1] == "cal") { ?>
	<style type="text/css">
	.calTable			{ width: 100%; margin: 0px; padding: 0px; }
	.calTableHead		{ height: 19px; font-family: sans-serif; font-size: x-small; color: #000000; background-color: #dfdfdf; }
	.calDaynames		{ height: 19px; font-family: sans-serif; font-size: x-small; color: #000000; font-weight: bold; text-decoration: none; text-align: center; padding: 2px; background-color: #CCCCCC; vertical-align: middle; }
	.calWeek			{ height: 19px; font-family: sans-serif; font-size: x-small; color: #000000; font-weight: bold; text-decoration: none; text-align: center; padding: 2px; background-color: #CCCCCC; vertical-align: middle; }
	.calWeekdayTd, .calSaturdayTd, .calSundayTd, .calNotthismonthTd, .calHighlightTd {
		height: 19px; text-align: center; padding: 2px; vertical-align: middle;
	}
	.calWeekday, .calSaturday, .calSunday, .calNotthismonth, .calHighlight {
		font-family: sans-serif; font-size: x-small; color: #000000; text-decoration: none;
	}
	.calWeekdayTd					{ background-color: #FFFFFF; }
	.calSaturdayTd, .calSundayTd	{ background-color: #f6f6f6; }
	.calNotthismonthTd				{ background-color: #e6e6e6; }
	.calHighlightTd					{ background-color: #e0ffb0; }
	.calSaturday, .calSunday, .calNotthismonth { color: #999999; }
	
	#calTodayTd	{ border: 1px solid #000000; text-decoration: none; }
	.calButton	{ font-family: sans-serif; font-weight: normal; color: #000000; font-size: x-small; text-decoration: none; padding: 1px; background-color: #ffffff; border-color: #cccccc #000000 #000000 #cccccc; border-width: 1px; border-style: solid; }
	</style>
	<?php } ?>
</head>

<body>

<div id="content">
	<div class="topNavi">
		<a href="index.php" title="Home"><img src="pix/home_<?php if($sBasename == 'index' || $aBnParts[1] == 'memo') { echo 'on'; } else { echo 'off'; } ?>.gif" width="25" height="13" alt="Home" title="Home" /></a>
		<a href="mbl_adr.php" title="<?php echo $aMSG['mbl']['adress'][$syslang]; ?>"><img src="pix/adr_<?php if($aBnParts[1] == 'adr') { echo 'on'; } else { echo 'off'; } ?>.gif" width="25" height="13" alt="<?php echo $aMSG['mbl']['adress'][$syslang]; ?>" title="<?php echo $aMSG['mbl']['adress'][$syslang]; ?>" /></a>
		<a href="mbl_cal_view.php" title="<?php echo $aMSG['mbl']['calendar'][$syslang]; ?>"><img src="pix/cal_<?php if($aBnParts[1] == 'cal') { echo 'on'; } else { echo 'off'; } ?>.gif" width="25" height="13" alt="<?php echo $aMSG['mbl']['calendar'][$syslang]; ?>" title="<?php echo $aMSG['mbl']['calendar'][$syslang]; ?>" /></a>
		<? if($aENV['mbl']['showSystemUser'] && isset($oPerm) && $oPerm->hasPriv('view')) { ?>
		<a href="mbl_sys_usr.php" title="<?php echo $aMSG['mbl']['sys'][$syslang]; ?>"><img src="pix/user_<?php if($aBnParts[1] == 'sys') { echo 'on'; } else { echo 'off'; } ?>.gif" width="25" height="13" alt="<?php echo $aMSG['mbl']['sys'][$syslang]; ?>" title="<?php echo $aMSG['mbl']['sys'][$syslang]; ?>" /></a>
		<? } ?>
	</div>