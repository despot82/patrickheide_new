<?php
/**
* inc.forum_folder.php
*
* folder-include (nur FORUM) //-> 2sprachig und voll kopierbar!
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	object	$oTree		-> kommt aus der inkludierenden Datei
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2006-02-07
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.forum_folder.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// OBJECTS
	// PrintFolder Klasse init
	require_once($aENV['path']['global_service']['unix'].'class.PrintFolder.php');
	$oPrintFolder =& new PrintFolder($oTree);

// vars
	$href = $aENV['SELF_PATH']."frm.php?frmSearchterm=&frmThread="; // an den href wird in der Klasse noch die ID angehaengt!
	$marker		= ($aBnParts[1] == "folder") ? 'contentNaviPadoff' : 'contentNaviPad';
	$divider	= ($aBnParts[1] == "folder") ? 'hDivideroff' : 'hDivider';
	
	// variable umschichten fuers highlighting
	$current_folder	= $frmThread;
	if ($aBnParts[1] == "folder") unset($current_folder); // reset navi bei edit_folder!
?>

<!-- Rahmentabelle linke Zelle -->
<td width="25%" class="<?php echo $marker; ?>">
<div class="text">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td width="80%" height="24"><span class="title"><?php echo $aMSG['forum']['folders'][$syslang]; ?></span></td>
		<td width="20%" align="right" nowrap><span class="text"><?php // button
		if ($oPerm->hasPriv('edit') && $aBnParts[1] != "folder") {
			echo '<a href="'.$aENV['SELF_PATH'].'frm_folder.php" title="'.$aMSG['content']['edit'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['content']['edit'][$syslang].'" class="btn"></a>';
		} ?></span></td>
	</tr>
</table>

<?php
// horizontale Linie
	echo HR;

// JavaScript-Block: Icon-Preloader
	echo $oPrintFolder->getJsIconPreloader();

// new / recent_threads
	$class	= ($current_folder == 'new') ? 'cnavihi' : 'cnavi'; // highlight
	if ($aBnParts[1] == "folder") { $class = 'cnavi'; }
	echo $oPrintFolder->getIcon(0, 0); 
	echo $oPrintFolder->getLink($href.'new', $class, $aMSG['forum']['recent_entries'][$syslang]); 
	// divider Linie
	echo $oPrintFolder->getDivider($divider);

// root / unsorted
	$class	= ($current_folder == '0' && $current_folder != 'all') ? 'cnavihi' : 'cnavi'; // highlight
	if ($aBnParts[1] == "folder") { $class = 'cnavi'; }
	echo $oPrintFolder->getIcon(0, 0); 
	echo $oPrintFolder->getLink($href.'0', $class, $aMSG['forum']['root_folder'][$syslang]); 
	// divider Linie
	echo $oPrintFolder->getDivider($divider);

// write NAVI
	$oPrintFolder->writeNavi($current_folder, $href, $divider);
	
// JavaScript-Block: Default-Aufklappen
	echo $oPrintFolder->getJsOpenDefault();
?>

</div>
<!-- Rahmentabelle linke Zelle schliessen -->
</td>

<!-- Rahmentabelle Main-Content-Zelle -->
<td width="75%" class="contentPadding">

