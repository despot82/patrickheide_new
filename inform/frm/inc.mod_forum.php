<?php

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mod_forum.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_module']['unix']."class.frm_forum.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
	require_once($aENV['path']['global_service']['unix'].'class.PrintFolder.php');
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	
// OBJECTS
	if(!is_object($oTACEA)) {
		// TEAM ACCESS CONTROLL
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
	// TREE (VOR FRM!)
	$oFrmTree =& new tree($oDb, $aENV['table']['global_tree'], FORUM_STRUCTURE_FILE, 'frm', array("sOrderBy" => "prio DESC")); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin', 'frm')) {
			$oFrmTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oFrmTree->buildTree();
	if (!is_object($oFrmPrintFolder)) {	// select Recent Postings on SYS-Portal
		// PrintFolder Klasse init
		$oFrmPrintFolder =& new PrintFolder($oFrmTree);
	}
	// FRM
	if (!is_object($oForum)) {	// select Recent Postings on SYS-Portal
		$oForum =& new frm_forum($oDb); // params: &$oDb
		// get authed folder-IDs from TREE
		$oForum->authThreads($oFrmTree->getAllAuthedIds()); // params: $aTreeId
		$sThreadPage	= $aENV['path']['frm']['http'].'frm.php';	// fuer List-Threads-link
	}
	// MDB
	if (!is_object($oMediadb)) {
		$oMediadb =& new mediadb(); // params: - 
	}
	// USER
	if (!is_object($oUser)) {	// select Recent Postings on SYS-Portal
		$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	}

// einspalter ODER zweispalter?
	$layout = (in_array('frm_recent', $aENV['portal_module']['L'])) ? '2' : '1';
	// anzahl Forumseintraege, die ausgelesen werden sollen
	#$nLimit = ($layout == '1') ? 5 : 10;
	$nLimit = 6;
	// get FORUM DATA
	$aForumData 	= $oForum->getRecent($nLimit); // params: [$nLimit=10][,$nPostingOnly=false][,$bValidOnly=true]


// HTML
	if (is_array($aForumData)) {
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabelle">
<tr>
	<td class="th" width="80%"><b><?php echo $aMSG['forum']['recent_entries'][$syslang]; ?></b></td>
	<td class="th" width="20%" align="right" nowrap>
	<a href="<?php echo $aENV['path']['frm']['http'].'frm.php';?>" title="<?php echo $aMSG['forum']['recent_threads'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_goto.gif" alt="<?php echo $aMSG['forum']['recent_threads'][$syslang]; ?>" class="btn"></a><?php 
	if ($oPerm->hasPriv('create', 'frm')) { ?><a href="<?php echo $aENV['path']['frm']['http'].'frm_detail.php';?>" title="<?php echo $aMSG['std']['new'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_add.gif" alt="<?php echo $aMSG['btn']['new'][$syslang]; ?>" class="btn"></a><?php } ?>
	</td>
</tr>
</table>
<?php
	}
	// LAYOUT 1 (einspalter) -------------------------------------------------------------------------
	if (is_array($aForumData) && $layout == '1') {
 ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php	foreach ($aForumData as $k => $aData) {
			// GET Parameter zusammenbauen
			$sGEToptions = '?entry_id='.$aData['id'].'&frmThread='.$aData['tree_id'];
			// ggf. Anzahl Kommentare
			$comments = ($aData['comments'] > 0) ? $aData['comments'] : '';
			// forumhomelarge formatierungen
			if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) {
				// ggf. image
				if (isset($aData['img_id']) && $aData['img_id']!='0') {
					$img = $oMediadb->getImageTag($aData['img_id'], 'border="0" width="40" height="40" class="userPict"');
				} else { 
					$img = Tools::getUserPicture($oMediadb, $aData['created_by']);
				}
				$img = '<td>'.$img.'</td>';
				// path
				$path = $oFrmPrintFolder->getBreadcrumbs($aData['tree_id']);
				$addinfos = '';
				if ($path != '<small>') {
					$addinfos .= $path.'<br>';
				}
				$addinfos .= Tools::formatKeywords($sThreadPage, array('frmSearchterm'=>$aData['keywords']), array('frmThread'=>'all')).'</small>';
			} else { 
				$img = '';
				$addinfos = '';
			}
?>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
				<?php echo $img; ?>
				<td width="100%"><span class="text">
					<?php echo $oUser->getUserName($aData['created_by']); ?>
					<small><?php echo Tools::formatDatetime($oDate, $aData['created']); ?></small><br>
					<a href="<?php echo $sThreadPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['view'][$syslang]; ?>"><b><?php echo $aData['headline']; ?></b></a><br>
					<?php echo $addinfos; ?>
				</span></td>
			</tr>
			</table>
		</td>
		<td width="10%" class="sub2" align="center"><p><b><?php echo $comments; ?></b></p></td>
	</tr>
		
<?php		if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) { ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
<?php		} // END if ?>
	
<?php	}  // END foreach ?>
</table>
<?php 
	} // END if LAYOUT 1

	// LAYOUT 2 (zweispalter) -------------------------------------------------------------------------
	if (is_array($aForumData) && $layout == '2') {
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<table width="50%" border="0" cellspacing="1" cellpadding="2" class="tabelle" align="left">
<?php	$i = 1;
		foreach ($aForumData as $k => $aData) {
			// GET Parameter zusammenbauen
			$sGEToptions = '?entry_id='.$aData['id'].'&frmThread='.$aData['tree_id'];
			// ggf. Anzahl Kommentare
			$comments = ($aData['comments'] > 0) ? $aData['comments'] : '';
			// forumhomelarge formatierungen
			if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) {
				// ggf. image
				if (isset($aData['img_id']) && $aData['img_id']!='0') {
					$img = $oMediadb->getImageTag($aData['img_id'], 'border="0" width="40" height="40" class="userPict"');
				} else { 
					$img = Tools::getUserPicture($oMediadb, $aData['created_by']);
				}
				$img = '<td>'.$img.'</td>';
				// path
				$path = $oFrmPrintFolder->getBreadcrumbs($aData['tree_id']);
				$addinfos = '';
				if ($path != '<small>') {
					$addinfos .= $path.'<br>';
				}
				$addinfos .= Tools::formatKeywords($sThreadPage, $aData['keywords']).'</small>';
				$tdheight = 55;
			} else { 
				$img = '';
				$addinfos = '';
				$tdheight = 24;
			}
?>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top" height="<?php echo $tdheight; ?>">
				<?php echo $img; ?>
				<td width="100%"><span class="text">
					<?php echo $oUser->getUserName($aData['created_by']); ?>
					<small><?php echo Tools::formatDatetime($oDate, $aData['created']); ?></small><br>
					<a href="<?php echo $sThreadPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['view'][$syslang]; ?>"><b><?php echo $aData['headline']; ?></b></a><br>
					<?php echo $addinfos; ?>
				</span></td>
			</tr>
			</table>
		</td>
		<td width="10%" class="sub2" align="center"><p><b><?php echo $comments; ?></b></p></td>
	</tr>
		
<?php		if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) { ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
<?php		} ?>

<?php		if ($i == ceil($nLimit/2)) {
				echo '</table>';
				echo '<table width="50%" border="0" cellspacing="1" cellpadding="2" class="tabelle">';
			} // END if
			$i++;
		}  // END foreach
?>
</table></td></tr></table>
<?php
	} // END if LAYOUT 2 -------------------------------------------------------------------------
	
	$aForumData 	= $oForum->getRecentSubscriptions($Userdata['id'],$nLimit); // params: [$nLimit=10][,$nPostingOnly=false][,$bValidOnly=true]
 
// HTML
	if (is_array($aForumData) && count($aForumData) > 0) {
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabelle">
<tr>
	<td class="th" colspan="2" width="100%"><b><?php echo $aMSG['forum']['subscriptions'][$syslang]; ?></b></td>
</tr>
</table>
<?php
	}
	// LAYOUT 1 (einspalter) -------------------------------------------------------------------------
	if (is_array($aForumData) && $layout == '1') {
 ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php	foreach ($aForumData as $k => $aData) {
			// GET Parameter zusammenbauen
			$sGEToptions = '?entry_id='.$aData['id'].'&frmThread='.$aData['tree_id'];
			// ggf. Anzahl Kommentare
			$comments = ($aData['comments'] > 0) ? $aData['comments'] : '';
			// forumhomelarge formatierungen
			if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) {
				// ggf. image
				if (isset($aData['img_id']) && $aData['img_id']!='0') {
					$img = $oMediadb->getImageTag($aData['img_id'], 'border="0" width="40" height="40" class="userPict"');
				} else { 
					$img = Tools::getUserPicture($oMediadb, $aData['created_by']);
				}
				$img = '<td>'.$img.'</td>';
				// path
				$path = $oFrmPrintFolder->getBreadcrumbs($aData['tree_id']);
				$addinfos = '';
				if ($path != '<small>') {
					$addinfos .= $path.'<br>';
				}
				$addinfos .= Tools::formatKeywords($sThreadPage, $aData['keywords']).'</small>';
			} else { 
				$img = '';
				$addinfos = '';
			}
?>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
				<?php echo $img; ?>
				<td width="100%"><span class="text">
					<?php echo $oUser->getUserName($aData['created_by']); ?>
					<small><?php echo Tools::formatDatetime($oDate, $aData['created']); ?></small><br>
					<a href="<?php echo $sThreadPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['view'][$syslang]; ?>"><b><?php echo $aData['headline']; ?></b></a><br>
					<?php echo $addinfos; ?>
				</span></td>
			</tr>
			</table>
		</td>
		<td width="10%" class="sub2" align="center"><p><b><?php echo $comments; ?></b></p></td>
	</tr>
		
<?php		if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) { ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
<?php		} // END if ?>
	
<?php	}  // END foreach ?>
</table>
<?php 
	} // END if LAYOUT 1

	// LAYOUT 2 (zweispalter) -------------------------------------------------------------------------
	if (is_array($aForumData) && $layout == '2') {
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<table width="50%" border="0" cellspacing="1" cellpadding="2" class="tabelle" align="left">
<?php	$i = 1;
		foreach ($aForumData as $k => $aData) {
			// GET Parameter zusammenbauen
			$sGEToptions = '?entry_id='.$aData['id'].'&frmThread='.$aData['tree_id'];
			// ggf. Anzahl Kommentare
			$comments = ($aData['comments'] > 0) ? $aData['comments'] : '';
			// forumhomelarge formatierungen
			if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) {
				// ggf. image
				if (isset($aData['img_id']) && $aData['img_id']!='0') {
					$img = $oMediadb->getImageTag($aData['img_id'], 'border="0" width="40" height="40" class="userPict"');
				} else { 
					$img = Tools::getUserPicture($oMediadb, $aData['created_by']);
				}
				$img = '<td>'.$img.'</td>';
				// path
				$path = $oFrmPrintFolder->getBreadcrumbs($aData['tree_id']);
				$addinfos = '';
				if ($path != '<small>') {
					$addinfos .= $path.'<br>';
				}
				$addinfos .= Tools::formatKeywords($sThreadPage, $aData['keywords']).'</small>';
				$tdheight = 55;
			} else { 
				$img = '';
				$addinfos = '';
				$tdheight = 24;
			}
?>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top" height="<?php echo $tdheight; ?>">
				<?php echo $img; ?>
				<td width="100%"><span class="text">
					<?php echo $oUser->getUserName($aData['created_by']); ?>
					<small><?php echo Tools::formatDatetime($oDate, $aData['created']); ?></small><br>
					<a href="<?php echo $sThreadPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['view'][$syslang]; ?>"><b><?php echo $aData['headline']; ?></b></a><br>
					<?php echo $addinfos; ?>
				</span></td>
			</tr>
			</table>
		</td>
		<td width="10%" class="sub2" align="center"><p><b><?php echo $comments; ?></b></p></td>
	</tr>
		
<?php		if (isset($aENV['forumhomelarge']) && $aENV['forumhomelarge'] == true) { ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
<?php		} ?>

<?php		if ($i == ceil($nLimit/2)) {
				echo '</table>';
				echo '<table width="50%" border="0" cellspacing="1" cellpadding="2" class="tabelle">';
			} // END if
			$i++;
		}  // END foreach
?>
</table></td></tr></table>
<?php
	} // END if LAYOUT 2 -------------------------------------------------------------------------
?>