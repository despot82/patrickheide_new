<?php
/**
* frm_detail.php
*
* Detailpage: frm //-> 2sprachig und voll kopierbar!
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$entry_id	welcher Datensatz ist der eigentliche Beitrag (wichtig zum zurueckspringen!)
* @param	int		$parent_id	wird diese uebergeben, ist es ein Kommentar, sonst ein Beitrag
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen: (0|1)] (optional)
* @param	int		$compare_tree_id	um zu ermitteln, ob der Beitrag verschoben wurde (weil dann auch alle Kommentare verschoben werden muessen!)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-04-20
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './frm_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	require_once($aENV['path']['global_module']['unix']."class.frm_forum.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$entry_id		= (isset($_GET['entry_id'])) ? $oFc->make_secure_int($_GET['entry_id']) : '';
	$parent_id		= (isset($_GET['parent_id'])) ? $oFc->make_secure_int($_GET['parent_id']) : '';
	if (isset($_GET['frmSearchterm']))	{ $oSess->set_var('frmSearchterm', $_GET['frmSearchterm']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	if (isset($_GET['frmThread']))		{ $oSess->set_var('frmThread', $_GET['frmThread']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	$frmSearchterm	= $oSess->get_var('frmSearchterm'); // params: $sName[,$default=null]
	$frmThread		= $oSess->get_var('frmThread'); // params: $sName[,$default=null]
	$sGEToptions	= '?entry_id='.$entry_id;
	#$sGEToptions	= (!empty($parent_id)) ? '&parent_id='.$parent_id : '&parent_id='.$id;
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$compare_tree_id= (isset($_POST['compare_tree_id'])) ? $_POST['compare_tree_id'] : '';
	$remoteSave		= (isset($_POST['remoteSave']) && $_POST['remoteSave'] == 1) ? 1 : 0;
	// title-Hiddenfields der MDB / PMS Files
	$pms_id_title	= (isset($_POST['pms_id_title'])) ? $_POST['pms_id_title'] : '';
	$mdb_id_title	= (isset($_POST['mdb_id_title'])) ? $_POST['mdb_id_title'] : '';
	// DL-files
	$aDl			= (isset($_POST['aDl'])) ? $_POST['aDl'] : array();
// 2c. Vars:
	$sTable			= $aENV['table']['frm_forum'];
	$sViewerPage	= $aENV['SELF_PATH']."frm.php".$sGEToptions; 	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'];
	$sEditPage		= $aENV['SELF_PATH'].'frm_detail.php';		// fuer EDIT-link								// fuer NEW-button

// FORUM
	$oForum =& new frm_forum($oDb); // params: &$oDb

// init mediadb (VOR DB!)
	$oMediadb =& new mediadb(); // params: - 

// 3. DB
// DB-action: delete

	if(isset($btn['delete']) && $aData['id']) {
		$oMediadb->moduleDelete($_POST['existing_img_id']); // params: $mdbId
		// Subscriptions auflösen!
		$oDb->make_delete(array('fkid' => $aData['id']),'frm_subscription');
		// SPECIAL: ggf. delete Upload-FILE(s)
		Tools::deleteAllUploadfiles($oMediadb, $aDl);
		// delete data
		$oForum->deleteEntry($aData['id']); // params: $id
		header("Location: ".$sViewerPage); exit;
	}

// DB-action: save
	if ((isset($btn['save']) || isset($btn['save_close']) || $remoteSave == 1) && !isset($alert)) {
		// Media-DB: ggf. altes Bild loeschen
		if (isset($_POST['delete_img_id'])) {
			// altes Bild in MediaDB loeschen
			$oMediadb->moduleDelete($_POST['existing_img_id']); // params: $mdbId
			// SPECIAL: ggf. delete Upload-FILE(s)
			Tools::deleteAllUploadfiles($oMediadb, $aDl);
			$aData['img_id'] = '';
		}
		// Media-DB: ggf. neues Bild hochladen
		if (isset($_FILES['img_id']['name']) && !empty($_FILES['img_id']['name'])) {
			$oMediadb->moduleDelete($_POST['existing_picture']); // params: $mdbId
			$aData['img_id'] = $oMediadb->moduleUpload('img_id', $aData['title'], $aMSG['topnavi']['forum'][$syslang]); // params: $sUploadFieldname[,$sDescription=''][,$sKeywords='']
		}
		if(isset($aData['pms_id']) && empty($aData['pms_id'])) unset($aData['pms_id']);
		if(isset($aData['mdb_id']) && empty($aData['mdb_id'])) unset($aData['mdb_id']);
		// SPECIAL: manage Upload-FILE(s)
		$aData['downloads'] = Tools::prepareUploadToolsData($oMediadb, $aDl, $aData, $aMSG['topnavi']['forum'][$syslang]);
		
		// prepare Flasheditor data
		/*$aData['content'] = str_replace(
			array('&lt;', '&gt;', '&amp;', '&quot;'),
			array('<', '>', '&', '"'),
			$aData['content']
		);*/

		$aData['keywords'] = Tools::getValuesFromKeywords($aData['keywords']);
		// save data
		$aData = $oForum->saveEntry($aData, $Userdata['id'], $compare_tree_id); // params: $aData[,$user_id=0][,$compare_tree_id='']
		
		// wenn Beitrag verschoben wurde -> verschiebe auch seine Kommentare
		if (!empty($compare_tree_id) && $compare_tree_id != $aData['tree_id'] && $aData['parent_id'] == 0) {
			$oDb->query('UPDATE '.$oForum->sTable.' SET tree_id = '.$aData['tree_id'].' WHERE parent_id = '.$aData['id']);
			// wenn NICHT zurueck zur overview-seite...
			if (!isset($btn['save_close'])) {
				// ...seite mit neuer Kategorie reloaden, damit session (und damit die folder-navi) aktualisiert wird
				header("Location: ".$aENV['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'].'&id='.$aData['id'].'&frmThread='.$aData['tree_id']); exit;
			} else {
				// ansonsten zurueck aber mit geaendertem parameter
				header("Location: ".$sViewerPage.'&frmThread='.$aData['tree_id']); exit;
			}
		}
		$id = $aData['id'];
		if(isset($btn['save'])) {
			header('Location: '.$sEditPage.'?entry_id='.$id.'&id='.$id);
		}
		// back to overviewpage
		if (isset($btn['save_close'])) {
			header("Location: ".$sViewerPage); exit;
		}

	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (!empty($id)) {
		$aData = $oForum->getEntry($id); // params: $id
		$mode_key = 'edit'; // do not change!
	} else {
		if (!empty($parent_id)) { // Kommentar-Funktion
			$aPosting = $oForum->getEntry($parent_id); // params: $id
			$aData['parent_id']	= $parent_id;
			$aData['tree_id']	= $aPosting['tree_id'];
			$aData['headline']	= $oForum->quoteHeadline($aPosting['headline']); // params: $sHeadline
			$aData['content']	= $oForum->quoteContent($aPosting['content']); // params: $sContent[,$nSplitLength=40][,$bStripTags=true]
		}
		$aData['is_active'] = 1; // set default
		$mode_key = 'new'; // do not change!
	}

	if(strstr($aData['keywords'],',')) {
		$aData['keywords'] = str_replace(',',' ',$aData['keywords']);
	}

// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();

	// TREE (VOR HTML!)
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], FORUM_STRUCTURE_FILE,'frm',array("sOrderBy" => "prio DESC"));  // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();

// HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['frm']['unix']."inc.forum_folder.php");
	if($aData['flag_edit_all'] == 1) {
		$oPerm->addPriv('admin','frm');
	}
// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	
	$oForm->check_field("headline", $aMSG['form']['headline'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>

<script language="JavaScript" type="text/javascript">
// opens new window for SELECT from "PMS-Archives"
function selectPmsArchiveWin(formfield,formname) {
	var left = (screen.width - 520);
	var pmsPath = '<?php echo $aENV['path']['pms']['http']; ?>';
	mediaWin = window.open(pmsPath+'popup_selectarchive.php?formfield='+formfield+'&formname='+formname,'pmsarchive','toolbar=0,status=0,scrollbars=1,width=500,height=500,resizable=1,top=30,left='+left);
	mediaWin.focus();
}
</script>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="remoteSave" value="0">
<?php echo $oForm->hidden("parent_id"); // params: $sFieldname[,$sDefault=''][,$sExtra=''] ?>
<?php echo $oForm->hidden("tree_id"); // params: $sFieldname[,$sDefault=''][,$sExtra=''] ?>
<input type="hidden" name="compare_tree_id" value="<?php echo $aData['tree_id']; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['forum']['posting'][$syslang]; ?></span> 
			<?php echo (!empty($parent_id)) ? $aMSG['forum']['mode_comment'][$syslang] : $aMSG['mode'][$mode_key][$syslang]; ?></p></td>
		<td align="right"><?php // BUTTONS
if ($entry_id > 0) { // BACK-Button nur wenn im Thread
	echo get_button("BACK", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"]
}
if ($oPerm->hasPriv('create')) { ?>
<?php echo get_button('NEW', $syslang, 'smallbut'); // params: $sType,$sLang[,$sClass="but"] ?><?php 
} ?></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php if ($oTree->getCount() && empty($parent_id)) { ?>
	<tr>
		<td><p><?php echo $aMSG['forum']['folder'][$syslang]; ?></p></td>
		<td><?php // select ueberschreibt ggf. hidden-field
		$oTree->setRootOff();
		if ($mode_key == 'new') {$aData['tree_id'] = (!empty($frmThread)) ? $frmThread : ''; } // bei "neu" zuletzt eingestellten ordner anzeigen
		echo $oTree->treeFormSelect('aData[tree_id]', $aData['tree_id'],'','',true,$Userdata['id']); // params: $name[,$currentId=''] ?></td>
	</tr>
<?php } ?>
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['title'][$syslang]; ?> *</b></span></td>
		<td width="80%"><?php echo $oForm->textfield('headline',250,78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['image'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->file_upload('img_id'); // params: [$sFieldname='uploadfile'][,$sPathKey='mdb_upload'] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['text'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea_flash('content', $sTable, $aData['id'], 'standalone'); // params: $sFieldname, $sTable, $id, (config) ?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['keywords'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea('keywords', 2, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>

<!-- Upload-Modul START /////////////////////////////////////////////////////////////////// -->
<?php if ($oForm->bEditMode == true) { ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	<tr valign="top">
		<th colspan="2"><?php echo $aMSG['form']['downloadfile'][$syslang]; ?></th>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['title'][$syslang]; ?></p></td>
		<td><input type="text" name="aDl[0][title]" value="" <?php echo get_input_size(485); ?> maxlength="150"></td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['downloadfile'][$syslang]; ?></p></td>
		<td>
<input type="file" size="25" name="uploadfile">
<?php	if (isset($aENV['module']['pms'])) { ?>
<input type="hidden" name="aData[pms_id]" value=""><input type="hidden" name="pms_id_title" value=""><input type="button" value="<?php echo $aMSG['btn']['presentation'][$syslang]; ?>" name="btn[pms_id]" class="smallbut" onClick="javascript:selectPmsArchiveWin('pms_id','editForm')">
<?php	} // END if ?>
<?php	if (isset($aENV['module']['mdb'])) { ?>
<input type="hidden" name="aData[mdb_id]" value=""><input type="hidden" name="mdb_id_title" value=""><input type="button" value="<?php echo $aMSG['btn']['mediafile'][$syslang]; ?>" name="btn[mdb_id]" class="smallbut" onClick="javascript:selectMediaWin('all','mdb_id','editForm')">
<?php	} // END if ?>
		</td>
	</tr>
<?php } // END if ?>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
<?php 
	// zeige schon bestehende downloads
	$aLines = explode("\n", $aData['downloads']);
	$i = 1;
	foreach($aLines as $k => $line) {
		if (!strstr($line, "||")) continue; // leere zeilen ueberspringen!
		list($row_type, $row_file, $row_title) = explode("||", trim($line)); ?>
	<tr>
		<td class="sub2"><p><b><?php echo $aMSG['form']['downloadfile'][$syslang]; ?> <?php echo $i; ?></b></p></td>
		<td class="sub2"><p><?php if ($oForm->bEditMode == true) { ?><input type="checkbox" name="aDl[<?php echo $i; ?>][delete]" id="delete_dl_<?php echo $i; ?>" value="1">&nbsp;<label for="delete_dl_<?php echo $i; ?>"><?php echo $aMSG['btn']['delete'][$syslang]; ?></label><?php } // END if ?></p></td>
	</tr>
	<tr>
		<td><p><?php echo $aMSG['form']['title'][$syslang]; ?></p></td>
		<td><p><?php if ($oForm->bEditMode == true) { ?><input type="text" name="aDl[<?php echo $i; ?>][title]" value="<?php echo $row_title; ?>" size="57" maxlength="150"><?php } else { echo $row_title; } // END if ?></p></td>
	</tr>
	<tr>
		<td height="24"><p><?php echo $aMSG['form']['downloadfile'][$syslang]; ?></p></td>
		<td><p><?php // anzeige des files
			if ($row_type == "mdb") {
				$aMediaFile = $oMediadb->getMedia($row_file); // params: $nMediaId
				#echo $oMediadb->getImageTag($row_file, 'border="0" width="50" height="50"'); // params: $imgSrc[,$extra=''][,$layerID=''(f.Slideshow)] 
				echo $oMediadb->getMediaIcon($aMediaFile['filename']); // params: $sFilename
				echo $aMediaFile['filename'];
				echo " (".$aENV['module']['mdb'][$syslang].")";
			}
			elseif ($row_type == "pms") {
				$oDb->query("SELECT id,comment,filename,pTitle,pAuthor,pDate,last_modified 
							FROM ".$aENV['table']['pms_archive']." 
							WHERE id = '".$row_file."'");
				$tmp = $oDb->fetch_array();
				echo $oMediadb->getMediaIcon($tmp['filename']); // params: $sFilename
				echo $tmp['filename'];
				echo " (".$aENV['module']['pms'][$syslang].")";
			} ?>
			<input type="hidden" name="aDl[<?php echo $i; ?>][file]" value="<?php echo $row_file; ?>">
			<input type="hidden" name="aDl[<?php echo $i; ?>][type]" value="<?php echo $row_type; ?>">
		</p></td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
<?php $i++;
	} // END foreach ?>
<!-- Upload-Modul ENDE /////////////////////////////////////////////////////////////////// -->

<?php if ($aENV['frm']['bWithEditAll'] && $aData['parent_id'] == 0) { // nur bei eingeschaltenem Modus ?>
	<tr valign="top">
		<td class="sub2"><p><b><?php echo $aMSG['forum']['editAll'][$syslang]; ?></b></p></td>
		<td class="sub2"><p><?=$oForm->checkbox('flag_edit_all',1,0)?></p></td>
	</tr>
<?php } ?>	
<?php if ($aENV['frm']['bWithComments'] && $aData['parent_id'] == 0) { // nur bei eingeschaltenem Modus ?>
	<tr valign="top">
		<td class="sub2"><p><b><?php echo $aMSG['forum']['withComments'][$syslang]; ?></b></p></td>
		<td class="sub2"><p><?=$oForm->checkbox('flag_with_comments',0,1)?>
		<? if ($mode_key == 'edit') { // nur bei root-beitraegen 
		echo '('.$aData['comments'].' '.$aMSG['forum']['comments'][$syslang].')';
		 } ?>
		</p></td>
	</tr>
<?php } else { 
	if ($mode_key == 'edit' && $aData['parent_id'] == 0) { // nur bei root-beitraegen ?>
	<tr valign="top">
		<td class="sub2"><p><b><?php echo $aMSG['forum']['comments'][$syslang]; ?></b></p></td>
		<td class="sub2"><p><?php echo $aData['comments']; ?></p></td>
	</tr>
<?php } ?>	
<? }?>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>