<?php
/**
* frm_forum_folder.php
*
* Specialpage: Verwalte Foren-Ordner 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$tree_id	[welcher Datensatz] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-01-07
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './frm_folder_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_module']['unix']."class.frm_forum.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['tree_id'])) ? $oFc->make_secure_int($_GET['tree_id']) : 0;
	$viewug			= (isset($_GET['viewug'])) ? $oFc->make_secure_string($_GET['viewug']) : '';
	// backup zur sicherheit!!!
	$treeid			= (isset($_GET['tree_id'])) ? $oFc->make_secure_int($_GET['tree_id']) : 0;
	$nToggleId		= (isset($_GET['nToggleId'])) ? $oFc->make_secure_int($_GET['nToggleId']) : '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$group			= (isset($_POST['group'])) ? $_POST['group'] : '';
	$user			= (isset($_POST['user'])) ? $_POST['user'] : '';
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
// 2c. Vars:
	$sTable			= $aENV['table']['global_tree'];
	$sGEToptions	= '?tree_id='.$id.'&nToggleId='.$nToggleId."&flag_open=".$aData['flag_open'];	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].'?nToggleId='.$nToggleId;							// fuer NEW-button
	$sViewerPage	= $aENV['SELF_PATH']."frm_folder.php".$sGEToptions;
	
// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();

	if(!isset($aData['flag_open'])) { 
		if(is_array($oTACEA->getRightForID($treeid,'frm'))) { 
			$flopen = 0; 
		} else { 
			$flopen = 1; 
		}
	}
	else {
		$flopen = $aData['flag_open'];	
	}
	unset($aData['flag_open']);
	
	// TREE (VOR DB & HTML!)
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], FORUM_STRUCTURE_FILE, 'frm', array("sOrderBy" => "prio DESC")); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	
	// build TREE (VOR HTML!)
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	// USER
	$oUser =& new User($oDb);
	
	// FORUM
	$oForum =& new frm_forum($oDb);
	
// 3. DB
// DB-action: delete_all
	if (isset($btn['delete_all']) && $aData['id']) {
		$oFrm =& new frm_forum(&$oDb);
		// get all sub-items of this navi-item
		$aIDs2delete = $oFrm->returnAllSubIds($aData['id'], $sTable,$aENV);
		// add this id
		$aIDs2delete[] = $aData['id'];
		// Datensätze ermitteln.
		$oDb->query("SELECT id FROM frm_forum WHERE tree_id='".$aData['id']."'");
		// Forum Subscriptions löschen
		for($i=0;$row = $oDb->fetch_object();$i++) {
			$oDb->make_delete(array('fkid' => $row->id),'frm_subscription');
		}
		// make delete(s) in Forum-Tree-Table
		$oDb->query("DELETE FROM ".$sTable." WHERE id IN (".implode(",", $aIDs2delete).") AND flag_tree='frm'");
		// Delete Entries in Forum-Content-Table
		$oDb->query("DELETE FROM ".$aENV['table']['frm_forum']." WHERE tree_id IN (".implode(",", $aIDs2delete).")");
		// delete tree-cache (file + session)
		$oTree->clearCache();
		// teamaccess rechte loeschen
		$oTACEA->onDeleteAction($aData['id'],'frm');
		
		// back to overview page
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
	if (isset($btn['save']) || isset($btn['save_close'])) {
		
		// Die Prio editieren, sofern das Forum in einen anderen Baum gehaengt wurde
		$rs = $oDb->fetch_by_id($aData['id'],$sTable);
		if($aData['parent_id'] != $rs['parent_id'] || !$aData['id']) {
			$sql = "SELECT prio FROM ".$sTable." WHERE parent_id='".$aData['parent_id']."' AND flag_tree='frm' ORDER BY prio DESC LIMIT 1";
			$oDb->query($sql);
			$row = $oDb->fetch_object();
			$aData['prio'] = ($row->prio+1);
		}
		$group = $aData['group'];
		$user = $aData['user'];
		$viewug = $aData['viewug'];
		$aData2['flag_fw'] = $aData['flag_fw'];
		unset($aData['group']);
		unset($aData['user']);
		unset($aData['viewug']);
		unset($aData['flag_fw']);
		if ($aData['id']) {
		// Update-actions of an existing entry
			$aData2['fkid'] = $aData['id'];
			// set userid
			$aData['last_mod_by'] = $Userdata['id'];
			// make update
			$aUpdate['id'] = $aData['id'];
			$oDb->make_update($aData, $sTable, $aUpdate);
		} else {
		// Insert-actions of a new entry
			// set timestamp + userid
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];
			// make insert
			$oDb->make_insert($aData, $sTable);
			$aData['id'] = $oDb->insert_id(); // get last inserted id
			$treeid = $aData['id'];
			$sGEToptions .= '&tree_id='.$treeid;
			$aData2['fkid'] = $aData['id'];
		}
		// Die Zusatzinformationen speichern (wiki oder forumzweig)
		//$oForum->saveFolderOptions($aData2);
		if($flopen == 1) {
			$oTACEA->delRightForTree($treeid,'frm');
		}
		else {
			$oTACEA->addRights($group, $user,$treeid,'frm');
		}

		// delete tree-cache (file + session)
		$oTree->clearCache();

		if(isset($btn['save_close'])) {
			// back to overview page
			header("Location: ".$sViewerPage); exit;
		}
		if(isset($btn['save'])) {
			// reload
			header("Location: ".$aENV['PHP_SELF'].$sGEToptions.'&viewug='.$viewug); exit;
		}
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

	if(!empty($viewug)) $aData['viewug'] = $viewug;
	$oTree->buildTree();
	$aStructure = $oTree->getAllChildren();

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['frm']['unix']."inc.forum_folder.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("title", $aMSG['form']['title'][$syslang].'.'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	//$oDb->debug = true;
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>


<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="remoteSave" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
	<td><p><span class="title"><?php echo $aMSG['forum']['folder'][$syslang]; ?></span>
		<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p></td>
	<td align="right"><span class="text"><?php
		echo '<a href="'.$sViewerPage.'" title="'.$aMSG['btn']['back'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['btn']['back'][$syslang].'" class="btn"></a>';
		if ($oPerm->hasPriv('create')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"]
	?></span></td>
</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<colgroup><col width="20%"><col width="70%"></colgroup>
<tr valign="top">
	<td><p><b><?php echo $aMSG['form']['name'][$syslang]; ?> *</b></p></td>
	<td>
	<?php echo $oForm->textfield("title",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
	</td>
</tr>
<tr>
	<td><p><b><?php echo $aMSG['form']['assignment'][$syslang]; ?></b></p></td>
	<td><?php #echo $oTree->treeFormSelect("aData[parent_id]", $aData['parent_id']); // params: $name[,$currentId=''] ?>
		<select name="aData[parent_id]" size="1">
<?php // build select-field -> NUR die ersten 2 Ebenen!
		$sel = ($id == '0') ? " selected" : ""; // highlight
		echo '<option value="0"'.$sel.'>'.$aMSG['forum']['noassignment'][$syslang]."</option>\n";
		foreach($aStructure as $kMain => $aData2) {
			if ($id == $aData2['id']) continue; // nicht noch mal sich selbst anzeigen!
			$sel = ($aData2['id'] == $aData['parent_id']) ? " selected" : ""; // highlight
			echo '<option value="'.$aData2['id'].'"'.$sel.'>'.$aData2['title']."</option>\n";
			foreach($aData2['children'] as $kSub => $aData3) {
				if ($id > 0 && $id == $aData3['id']) continue; // nicht noch mal sich selbst anzeigen!
				$sel = ($aData3['id'] == $aData['parent_id']) ? " selected" : ""; // highlight
				echo '<option value="'.$aData3['id'].'"'.$sel.'>- '.$aData3['title']."</option>\n";
			}
		} ?>
		</select>
	</td>
</tr>
<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
<? /*
<tr valign="top">
	<td><p><b><?php echo $aMSG['form']['choose'][$syslang]; ?></b></p></td>
	<td>
		<?=$oForm->radio('flag_fw',array('0' => $aMSG['forum']['forum'][$syslang], '1' => $aMSG['forum']['wiki'][$syslang]),true)?>
	</td>
</tr>
<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
 */ ?>
<tr valign="top">
	<td><p><b><?php echo $aMSG['form']['forumstatus'][$syslang]; ?></b></p></td>
	<td>
<? 
$param = array('first' => '20','second' => '70','border'=>'0','cellspacing'=>'1','cellpadding'=>'2','class' =>"tabelle");?>
<?=$oTACEA->showTeamAccess($aData,$syslang,$treeid,$flopen,$aMSG,$param,'frm',true); ?>
	</td>
</tr>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE_ALL"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>