<?php
/**
* frm.php
*
* Overviewpage: sys_forum //-> 2sprachig und voll kopierbar!
*
* @param	int		$entry_id		welcher Thread
* @param	string	$frmSearchterm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$frmThread		[zum filtern der Anzeige um ein Directory (im Tree)] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	array	Formular:		$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-04-12
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './frm.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_module']['unix']."class.frm_forum.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");

// 2a. GET-params abholen
	$entry_id		= (isset($_GET['entry_id'])) ? $oFc->make_secure_int($_GET['entry_id']) : 0;
	if (isset($_GET['frmSearchterm']))	{ $oSess->set_var('frmSearchterm', $_GET['frmSearchterm']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	if (isset($_GET['frmThread']))		{ $oSess->set_var('frmThread', $_GET['frmThread']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	$frmSearchterm	= $oSess->get_var('frmSearchterm'); // params: $sName[,$default=null]
	$frmThread		= $oSess->get_var('frmThread'); // params: $sName[,$default=null]
	$subscribe		= (isset($_GET['subscribe'])) ? $_GET['subscribe'] : 0;
	$sGEToptions	= '';
// 2b. POST-params abholen
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
// 2c. Vars:
	$sTable			= $aENV['table']['frm_forum'];
	$sViewerPage	= $aENV['PHP_SELF'];						// fuer BACK-link
	$sThreadPage	= $aENV['PHP_SELF'];						// fuer List-Threads-link
	$sEditPage		= $aENV['SELF_PATH'].'frm_detail.php';		// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;					// fuer NEW-button
	// wenn kein suchbegriff aber 'recent' als thema -> thema auf 'all' setzen, da suchen auf 'new' nicht moeglich!
	if (!empty($frmSearchterm) && $frmThread == 'new')	{ $frmThread = 'all'; }
	// wenn kein suchbegriff aber alle themen -> thema auf 'recent' eingrenzen!
	if (empty($frmSearchterm) && $frmThread == 'all') { $frmThread = 'new'; }
	// set default / fallback
	if ($frmThread == '')	{ $frmThread = 'new'; }


// OBJECTS
	// FORUM
	$oForum =& new frm_forum($oDb); // params: &$oDb
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();
	
	// TREE
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], FORUM_STRUCTURE_FILE, 'frm', array("sOrderBy" => "prio DESC")); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();
	
	// USER
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	
	$oForum->authThreads($oTree->getAllAuthedIds()); // params: $aTreeId
	
	// MEDIA-DB
	$oMediadb =& new mediadb(); // params: - 

	if(isset($subscribe) && $subscribe == 1) {
		$aData['id'] = $entry_id;
		$aData['uid'] = $Userdata['id'];
		$oForum->saveSubscribtion($aData);
		header('Location: '.$sViewerPage.'?entry_id='.$entry_id.'&id='.$aData['id']);
	}
	
// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['frm']['unix']."inc.forum_folder.php");

?>

<form action="<?php echo $aENV['PHP_SELF']; ?>" method="get">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['forum']['forum'][$syslang]; ?></span></p></td>
		<td align="right"><?php // BUTTONS
		if ($entry_id > 0) { // BACK-Button nur wenn im Thread
			echo get_button("BACK", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"]
		}
		if ($oPerm->hasPriv('create')) {
			echo get_button('NEW', $syslang, 'smallbut'); // params: $sType,$sLang[,$sClass="but"]
		} ?></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<td class="th"><!-- suche-modul -->
			<input type="text" name="frmSearchterm" value="<?php echo $frmSearchterm; ?>">
			<?php if ($oTree->getCount()) {
			// select ueberschreibt ggf. hidden-field
				$oTree->setRootOff();
				if ($mode_key == 'new') {$aData['tree_id'] = $oSess->get_var('frmThread'); } // bei "neu" zuletzt eingestellten ordner anzeigen
				$oTree->addOption(array('new' => $aMSG['forum']['recent_entries'][$syslang]));
				$oTree->addOption(array('0' => $aMSG['forum']['root_folder'][$syslang])); // -> hier: "unsorted"
				$oTree->setAllName($aMSG['forum']['all_folders'][$syslang]);
				echo $oTree->treeFormSelect("frmThread", $frmThread,'',''); // params: $name[,$currentId='']
			}
			// search button
			echo get_button("SEARCH", $syslang); // params: $sType,$sLang[,$sClass="but"] ?>
		</td>
	</tr>
</table>
</form>
<br>
<?php if ($entry_id > 0) { // Thread eines Beitrags -------------------------------------
		// select Beitrag
		$aData		= $oForum->getEntry($entry_id); // params: $id
		$aData2 = array();
		$aData2['id'] = $aData['id'];
		$aData2['uid'] = $Userdata['id'];
		$oForm =& new form_admin($aData2);
		// ACHTUNG: nach einem delete landet man hier, obwohl es den entry dann gar nicht mehr gibt...
		if (!isset($aData['id']) || empty($aData['id'])) { // ... deshalb:
			header('Location: '.$sViewerPage); exit;	// weiterleitung zu viewer-page!
		}
		// edit-button (Link zur Edit-Seite NUR wenn User==Creator)
		$button_edit	= ($oPerm->hasPriv('admin') || ($aData['created_by'] == $Userdata['id'] && $oPerm->hasPriv('edit')) || $aData['flag_edit_all'] == 1)
			? '<a href="'.$sEditPage.'?entry_id='.$entry_id.'&id='.$aData['id'].'" title="'.$aMSG['std']['edit'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['std']['edit'][$syslang].'" class="btn"></a>'
			: '';
		// comment-button
		$button_comment	= ($oPerm->hasPriv('create') && $aData['flag_with_comments'] == 0) 
			? '<a href="'.$sEditPage.'?entry_id='.$entry_id.'&parent_id='.$aData['id'].'" title="'.$aMSG['btn']['comment'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_comment.gif" alt="'.$aMSG['btn']['comment'][$syslang].'" class="btn"></a>'
			: '';
		$subscribe = '<a href="'.$sViewerPage.'?entry_id='.$entry_id.'&frmThread='.$frmThread.'&subscribe=1"';
		if(!$oForum->getsubscribeStatus($aData['id'],$Userdata['id'])) {
			$subscribe .= ' title="'.$aMSG['forum']['subscribe'][$syslang].'">';
			$subscribe .= '<img src="'.$aENV['path']['pix']['http'].'icn_bookmark.gif" alt="'.$aMSG['forum']['subscribe'][$syslang].'" class="btn">';
		}
		else {
			$subscribe .= ' title="'.$aMSG['forum']['unsubscribe'][$syslang].'">';
			$subscribe .= '<img src="'.$aENV['path']['pix']['http'].'icn_removebookmark.gif" alt="'.$aMSG['forum']['unsubscribe'][$syslang].'" class="btn">';
		}
		$subscribe .= '</a>';
?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="90%"><?php echo $aMSG['forum']['posting'][$syslang]; ?></th>
		<th width="10%">&nbsp;</th>
	</tr>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
				<td><?php // userpicture nur wenn kein bild eingepflegt wurde
				if (!isset($aData['img_id']) || $aData['img_id']=='0') {
					echo Tools::getUserPicture($oMediadb, $aData['created_by']);
				} ?></td>
				<td width="100%">
					<span class="text"><?php echo Tools::formatUsername($aData['created_by']); ?>
					<small><?php echo Tools::formatDatetime($oDate, $aData['created']); ?></small><br>
					<b><?php echo $aData['headline']; ?></b><br>
					<small><?php echo Tools::formatKeywords($sThreadPage, array('frmSearchterm'=>$aData['keywords']), array('frmThread'=>'all')); ?></small></span>
				</td>
			</tr>
			</table>
		</td>
		<td align="right" nowrap><p>
		<?php echo $button_comment; ?><?=$subscribe?><?php echo $button_edit;?>
		</p></td>
	</tr>
	<tr valign="top">
		<td class="sub2" width="70%">
			<?php // bild im beitrag
			if (isset($aData['img_id']) && $aData['img_id']!='') {
				echo $oMediadb->getImageTag($aData['img_id'], 'border="0" align="left"');
			} ?>
			<span class="text"><?php echo nl2br($aData['content']); ?><br><br>
			<?php echo Tools::formatDownloads($oMediadb, $oDb, $oFile, $aData['downloads']); ?>
			</span>
		</td>
		<td class="off">&nbsp;</td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php // select Comments .........................................................
	if($aData['flag_with_comments'] == 0) {
		$aForumData = $oForum->getComments($entry_id); // params: $nPostingId
		foreach ($aForumData as $k => $aData) {
			// edit-button (Link zur Edit-Seite NUR wenn User==Creator)
			$button_edit	= ($oPerm->hasPriv('admin') || ($aData['created_by'] == $Userdata['id'] && $oPerm->hasPriv('edit')))
				? '<a href="'.$sEditPage.'?entry_id='.$entry_id.'&id='.$aData['id'].'" title="'.$aMSG['std']['edit'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['std']['edit'][$syslang].'" class="btn"></a>'
				: '';
?>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
				<td><?php echo Tools::getUserPicture($oMediadb, $aData['created_by']); ?></td>
				<td width="100%">
					<span class="text"><?php echo Tools::formatUsername($aData['created_by']); ?>
					<small><?php echo Tools::formatDatetime($oDate,$aData['created']); ?></small><br>
					<b><?php echo $aData['headline']; ?></b><br>
					<small><?php echo Tools::formatKeywords($sThreadPage, array('frmSearchterm'=>$aData['keywords']), array('frmThread'=>'all')); ?></small></span>
				</td>
			</tr>
			</table>
		</td>
		<td align="right" nowrap><p><?php echo $button_edit; ?></p></td>
	</tr>
	<tr valign="top">
		<td class="sub3">
			<?php
			if(isset($aData['img_id']) && $aData['img_id']!='') {
				echo $oMediadb->getImageTag($aData['img_id'], 'border="0" align="left"');
			} ?>
			<span class="text"><?php echo nl2br($aData['content']); ?><br>
			<?php echo Tools::formatDownloads($oMediadb, $oDb, $oFile, $aData['downloads']); ?></span>
		</td>
		<td class="off">&nbsp;</td>
	</tr>
	<tr><td class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php	} // END while 
	} // END IF ?>
</table>

<?php } else { // Search / Tree / Recent Postings ------------------------------------------------------------
		// select
		if (!empty($frmSearchterm)) {
			$aForumData = $oForum->getSearchResult($frmSearchterm, $frmThread); // params: $sSearchtext[,$nTreeId]
		} else {
			if ($frmThread == 'new') {
				$aForumData = $oForum->getRecent(); // params: [$nLimit=10][,$nPostingOnly=false]
			}
			else if ($frmThread == 'all') {
				$aForumData = $oForum->getSearchResult($frmSearchterm); // params: $sSearchtext[,$nTreeId]
			}
			else {
				$aForumData = $oForum->getPostings($frmThread); // params: $nTreeId
			}
		}
?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="85%"><?php echo $aMSG['forum']['posting'][$syslang]; ?></th>
		<th width="15%"><?php echo $aMSG['forum']['comments'][$syslang]; ?></th>
	</tr>
<?php	foreach ($aForumData as $k => $aData) {
			// GET Parameter zusammenbauen
			$sGEToptions = '?entry_id='.$aData['id'].'&frmThread='.$aData['tree_id'];
			// ggf. path (NUR bei Recent-Threads oder Suchergebnis)
			if ($frmThread == 'new' || $frmThread == 'all' || isset($btn['search'])) {
				$path = (is_object($oPrintFolder)) ? $oPrintFolder->getBreadcrumbs($aData['tree_id']) : '';
			}
			// ggf. Anzahl Kommentare
			$comments = ($aData['comments'] > 0 && $aData['flag_with_comments'] == 0) ? $aData['comments'] : '';
?>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
				<td><?php 
				if(isset($aData['img_id']) && $aData['img_id']!='0') {
					echo $oMediadb->getImageTag($aData['img_id'], 'border="0" width="40" height="40" class="userPict"');
				} else {
					echo Tools::getUserPicture($oMediadb, $aData['created_by']);
				} ?></td>
				<td width="100%">
					<span class="text"><?php echo Tools::formatUsername($aData['created_by']); ?>
					<small><?php echo Tools::formatDatetime($oDate,$aData['created']); ?><br></small>
					<a href="<?php echo $sThreadPage.$sGEToptions; ?>" title="<?php echo $aMSG['std']['view'][$syslang]; ?>"><b><?php echo $aData['headline']; ?></b></a><br>
					<small><?php echo $path.'<br>'; ?>
					<?php echo Tools::formatKeywords($sThreadPage, array('frmSearchterm'=>$aData['keywords']), array('frmThread'=>'all')); ?></small></span>
				</td>
			</tr>
			</table>
		</td>
		<td align="center"><p><b><?php echo $comments; ?></b></p></td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php	} // END while ?>
</table>

<?php }   // END if -------------------------------------------------------------------------- ?>

<br><br>
<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
