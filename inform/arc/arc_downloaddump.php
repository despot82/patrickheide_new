<?php
/**
* arc_downloaddump.php
*
* Seite zum Downloaden eines Datenbank-Backups //-> voll kopierbar!
*
* @param	int		$filename		welches Backup soll heruntergeladen werden
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
*
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 / 30.01.2006
*/
 
 // TODO prüfen warum die _include_all.php nicht funktioniert...
	//require_once ("../sys/php/_include_all.php");
 	$filename = (isset($_GET['filename'])) ? $_GET['filename'] : '';
	require_once('../sys/php/global/service/class.download.php');
	$oDownload =& new download();
	$oDownload->setFile('../../data/sys/backup/'.$filename); 
	$oDownload->setContentDisposition('attachment');
	$oDownload->setContentType('text/plain');
	$oDownload->send();

?>
