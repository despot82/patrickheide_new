<?php
/**
* inc.arc_folder.php
*
* folder-include (nur archive) //-> 2sprachig und voll kopierbar!
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	object	$oTree		-> kommt aus der inkludierenden Datei
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2006-02-07
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.arc_folder.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// OBJECTS
	// PrintFolder Klasse init
	require_once($aENV['path']['global_service']['unix'].'class.PrintFolder.php');
	$oPrintFolder =& new PrintFolder($oTree);

// vars
	$href		= $aENV['SELF_PATH']."arc.php?arcSearchterm=&arcFolder="; // an den href wird in der Klasse noch die ID angehaengt!
	$marker		= ($aBnParts[1] == "folder") ? 'contentNaviPadoff' : 'contentNaviPad';
	$divider	= ($aBnParts[1] == "folder") ? 'hDivideroff' : 'hDivider';
	
	// variable umschichten fuers highlighting
	$current_folder	= $arcFolder;
	if ($aBnParts[1] == "folder") unset($current_folder); // reset navi bei edit_folder!
?>

<!-- Rahmentabelle linke Zelle -->
<td width="25%" class="<?php echo $marker; ?>">
<div class="text">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td width="80%" height="24"><span class="title"><?php echo $aMSG['archiv']['folders'][$syslang]; ?></span></td>
		<td width="20%" align="right" nowrap><span class="text"><?php // button
		if ($oPerm->hasPriv('edit') && $aBnParts[1] != "folder") {
			echo '<a href="'.$aENV['SELF_PATH'].'arc_folder.php" title="'.$aMSG['content']['edit'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['content']['edit'][$syslang].'" class="btn"></a>';
		} ?></span></td>
	</tr>
</table>

<?php 
// horizontale Linie
	echo HR;

// JavaScript-Block: Icon-Preloader
	echo $oPrintFolder->getJsIconPreloader();

// root / recent_threads
	$class	= ($current_folder == 0 && $current_folder != 'all' && !strstr($_SERVER['PHP_SELF'],'arc_restoredump.php')) ? 'cnavihi' : 'cnavi'; // highlight
	if ($aBnParts[1] == "folder") { $class = 'cnavi'; }
	echo $oPrintFolder->getIcon(0, 0); 
	echo $oPrintFolder->getLink($href.'new', $class, $aMSG['archiv']['recent_entries'][$syslang]); 

// divider Linie
	echo $oPrintFolder->getDivider($divider);

// write NAVI
	$oPrintFolder->writeNavi($current_folder, $href, $divider);

// JavaScript-Block: Default-Aufklappen
	echo $oPrintFolder->getJsOpenDefault();

	if($oPerm->hasPriv('admin')) {
		echo '<br>'.HR;
		// highlight
		$class = (strstr($_SERVER['PHP_SELF'],'arc_restoredump.php')) ? 'cnavihi' : 'cnavi'; 
		echo $oPrintFolder->getIcon(0, 0); 
		echo $oPrintFolder->getLink('arc_restoredump.php', $class, $aMSG['archiv']['dbbackups'][$syslang]); 
		echo $oPrintFolder->getDivider($divider); 
	}
?>

</div>
<!-- Rahmentabelle linke Zelle schliessen -->
</td>

<!-- Rahmentabelle Main-Content-Zelle -->
<td width="75%" class="contentPadding">


