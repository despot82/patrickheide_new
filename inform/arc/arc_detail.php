<?php
/**
* arc_detail.php
*
* Detailpage: archive //-> 2sprachig und voll kopierbar!
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$entry_id	welcher Datensatz ist der eigentliche Beitrag (wichtig zum zurueckspringen!)
* @param	int		$parent_id	wird diese uebergeben, ist es ein Kommentar, sonst ein Beitrag
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen: (0|1)] (optional)
* @param	int		$compare_tree_id	um zu ermitteln, ob der Beitrag verschoben wurde (weil dann auch alle Kommentare verschoben werden muessen!)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.0 / 2005-01-20
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './arc_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$entry_id		= (isset($_GET['entry_id'])) ? $oFc->make_secure_int($_GET['entry_id']) : '';
	$parent_id		= (isset($_GET['parent_id'])) ? $oFc->make_secure_int($_GET['parent_id']) : '';
	if (isset($_GET['arcSearchterm']))	{ $oSess->set_var('arcSearchterm', $_GET['arcSearchterm']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	if (isset($_GET['arcFolder']))		{ $oSess->set_var('arcFolder', $_GET['arcFolder']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	$arcSearchterm	= $oSess->get_var('arcSearchterm'); // params: $sName[,$default=null]
	$arcFolder		= $oSess->get_var('arcFolder'); // params: $sName[,$default=null]
	$sGEToptions	= '?entry_id='.$entry_id;
	#$sGEToptions	= (!empty($parent_id)) ? '&parent_id='.$parent_id : '&parent_id='.$id;
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$edate			= (isset($_POST['edate'])) ? $_POST['edate'] : array();
	$compare_tree_id= (isset($_POST['compare_tree_id'])) ? $_POST['compare_tree_id'] : '';
	
// 2c. Vars:
	$sTable			= $aENV['table']['arc_data'];
	$sViewerPage	= $aENV['SELF_PATH']."arc.php".$sGEToptions; 	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'];								// fuer NEW-button

// init mediadb (VOR DB!)
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	$oMediadb =& new mediadb(); // params: - 
	
	$aData['keywords'] = Tools::getValuesFromKeywords($aData['keywords']);
	
	$oArchiv->setTable($sTable);
	$oArchiv->setUserdata($Userdata);
	
	// prepare Flasheditor data
	$aData['content'] = str_replace(
		array('&lt;', '&gt;', '&amp;', '&quot;'),
		array('<', '>', '&', '"'),
		$aData['content']
	);
	if(isset($btn['save']) || isset($btn['save_close'])) {
		// Media-DB: ggf. altes Bild loeschen
		if (isset($_POST['delete_img_id'])) {
			// altes Bild in MediaDB loeschen
			$oMediadb->moduleDelete($_POST['existing_img_id']); // params: $mdbId
			$aData['img_id'] = '';
		}
		// Media-DB: ggf. neues Bild hochladen
		if (isset($_FILES['img_id']['name']) && !empty($_FILES['img_id']['name'])) {
			$oMediadb->moduleDelete($_POST['existing_picture']); // params: $mdbId
			$aData['img_id'] = $oMediadb->moduleUpload('img_id', $aData['title'], $aMSG['topnavi']['archiv'][$syslang]); // params: $sUploadFieldname[,$sDescription=''][,$sKeywords='']
		}
		if(isset($_FILES['fileeditcopy']['name']) && !empty($_FILES['fileeditcopy']['name'])) {
			$aData['content'] .= $oArchiv->copyFromFile($_FILES['fileeditcopy']);
		}
		$aData['edate'] = $oDate->date_trad2mysql($edate['day'].'.'.$edate['month'].'.'.$edate['year']);
		$ret = $oArchiv->save($aData);

		if(isset($btn['save'])) {
			if(empty($aData['id'])) { $aData['id'] = $ret; }
			header('Location: '.$sNewPage.'?id='.$aData['id']);
		}
		if(isset($btn['save_close'])) {
			header('Location: '.$sViewerPage);
		}
	}	
	if(isset($btn['delete'])) {
		$oArchiv->delete($aData['id']);
		header('Location: '.$sViewerPage);
	}

// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (!empty($id)) {
		$aData = $oArchiv->getArchivEntry($id); // params: $id
		$mode_key = 'edit'; // do not change!
	} else {
		$mode_key = 'new'; // do not change!
	}

// OBJECTS
	// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();

	// TREE (VOR HTML)
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], ARCHIV_STRUCTURE_FILE, 'arc', array("sOrderBy" => "prio DESC"));  // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['arc']['unix']."inc.arc_folder.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	#$oForm->check_field("intnr", $aMSG['form']['intnr'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	$oForm->check_field("title", $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}

// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="remoteSave" value="0">
<?php echo $oForm->hidden("tree_id"); // params: $sFieldname[,$sDefault=''][,$sExtra=''] ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['archiv']['posting'][$syslang]; ?></span>
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p></td>
		<td align="right"><?php // BUTTONS
		echo get_button("BACK", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"]
		if ($oPerm->hasPriv('create')) {
			echo get_button('NEW', $syslang, 'smallbut'); // params: $sType,$sLang[,$sClass="but"]
		} ?></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php if ($oTree->getCount()) { ?>
	<tr>
		<td><p><?php echo $aMSG['archiv']['folder'][$syslang]; ?></p></td>
		<td><?php // select ueberschreibt ggf. hidden-field
		$oTree->setRootOff();
		if ($mode_key == 'new') {$aData['tree_id'] = (!empty($arcFolder)) ? $arcFolder : ''; } // bei "neu" zuletzt eingestellten ordner anzeigen
		echo $oTree->treeFormSelect('aData[tree_id]', $aData['tree_id']); // params: $name[,$currentId='']
		?></td>
	</tr>
<?php } ?>
	
	<tr valign="top">
		<td width="20%"><span class="text"><?php echo $aMSG['form']['nr'][$syslang]; ?></span></td>
		<td width="80%"><?php echo $oForm->textfield('intnr',7,26); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['title'][$syslang]; ?> *</b></span></td>
		<td width="80%"><?php echo $oForm->textfield('title',250,78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td width="20%"><span class="text"><?php echo $aMSG['form']['date'][$syslang]; ?></span></td>
		<td width="80%"><?php echo $oForm->date_dropdown('edate',false); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['avail'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->checkbox('avail',1,0); // params: $sFieldname=''[,$sPathKey='mdb_upload'] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td class="sub2" height="26"><span class="text">Label</span></td>
		<td class="sub2">
		 <?=$oForm->labelDropdown('label_id',$aData['tree_id'],'arc',$oDb);?>
		</td>
	</tr>
	
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['content'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea_flash('content', $sTable, $aData['id'], 'standalone'); // params: $sFieldname, $sTable, $id, (config) 
			if($aENV['arc']['bWithFileEditCopy']) {
				echo $oForm->file_upload('fileeditcopy'); // params: [$sFieldname='uploadfile'][,$sPathKey='mdb_upload'] 
		 	} 
		 	?>
		</td>
	</tr>
	<tr valign="top">
		<td width="20%"><span class="text"><?php echo $aMSG['form']['link'][$syslang]; ?></span></td>
		<td width="80%"><?php echo $oForm->textfield('link',250,78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['image'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->file_upload('img_id'); // params: [$sFieldname='uploadfile'][,$sPathKey='mdb_upload'] ?>
		</td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['keywords'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea('keywords', 2, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>

</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>