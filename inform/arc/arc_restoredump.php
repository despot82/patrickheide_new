<?php
/**
* arc_restoredump.php
*
* Seite zum Widerherstellen der Datenbank-Backups //-> 2sprachig und voll kopierbar!
*
* @param	int		$restore_id		welches Backup soll wiederhergestellt werden
* @param	string	$arcSearchterm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$arcFolder	[zum filtern der Anzeige um ein Directory (im Tree)] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	array	Formular:		$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-04-12
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './arc_restoredump.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$restore_id		= (isset($_GET['restore_id'])) ? $oFc->make_secure_int($_GET['restore_id']) : 0;
	$delete_id		= (isset($_GET['delete_id'])) ? $oFc->make_secure_int($_GET['delete_id']) : 0;
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sViewerPage	= $aENV['PHP_SELF'];					// fuer BACK-link
	$sThreadPage	= $aENV['PHP_SELF'];					// fuer List-Threads-link
	$sEditPage		= $aENV['SELF_PATH'].'arc_detail.php';	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;				// fuer NEW-button

// DB
	$aArchivData = $oArchiv->getDBBackupArchiv();
	// ein Archiv wiederherstellen
	if($restore_id > 0) {
		for($i=0;!empty($aArchivData[$i]);$i++) {
			$aName = explode('_',$aArchivData[$i]);
			if($aName[0] == $restore_id) {
				$fname = $aArchivData[$i];	
			}
		}
		if(Tools::isExecAvail()) {
			$oDb->db_restore_from_file($aENV['path']['sys_data']['unix'].'backup/'.$fname);
		}
		header('Location: '.$_SERVER['PHP_SELF']);
	}
	// Ein Backup loeschen
	if($delete_id > 0) {
		for($i=0;!empty($aArchivData[$i]);$i++) {
			$aName = explode('_',$aArchivData[$i]);
			if($aName[0] == $delete_id) {
				$fname = $aArchivData[$i];	
			}
		}
		unlink($aENV['path']['sys_data']['unix'].'backup/'.$fname);
		header('Location: '.$_SERVER['PHP_SELF']);
	}

// TREE (VOR HTML!)
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], ARCHIV_STRUCTURE_FILE, 'arc', array("sOrderBy" => "prio DESC")); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();

// HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['arc']['unix']."inc.arc_folder.php");

// HTML //////////////////////////////////////////////////////////////////////////////
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['archiv']['dbbackup'][$syslang]; ?></span></p>
		</td>
		<td align="right"><?php // BUTTONS
		if ($entry_id > 0) { // BACK-Button nur wenn im Thread
			echo get_button("BACK", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"]
		} ?></td>
	</tr>
</table>

<br>
<script language="JavaScript" type="text/javascript">
function confirmRestore() {
	return window.confirm('<?php echo $aMSG['form']['restore'][$syslang]; ?>');
}
</script>
<? if(is_array($aArchivData)) { ?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="85%"><?php echo $aMSG['archiv']['dblastbackup'][$syslang]; ?></th>
		<th width="15%">&nbsp;</th>
	</tr>
<?php
	natsort($aArchivData);

	foreach($aArchivData as $i => $sData) {
		$aNewArchivData[] = $sData;	
	}
	$ico = count($aNewArchivData)-1;

	for($i=0;!empty($aNewArchivData[$i]);$i++) {
		$aArchivData[($ico-$i)] = $aNewArchivData[$i];
	}

	for ($i=0; !empty($aArchivData[$i]); $i++) {
		$aName 			= explode('_',$aArchivData[$i]);
		$button_save	= '<a href="arc_downloaddump.php?filename='.$aArchivData[$i].'" title="'.$aMSG['btn']['download'][$syslang].'" target="_blank"><img src="'.$aENV['path']['pix']['http'].'btn_dwn.gif" alt="'.$aMSG['btn']['download'][$syslang].'" class="btn"></a>';
		$button_restore	= (Tools::isExecAvail()) ? '<a href="'.$_SERVER['SCRIPT_NAME'].'?restore_id='.$aName[0].'" onClick="return confirmRestore()" title="'.$aMSG['btn']['restore'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_restore.gif" alt="'.$aMSG['btn']['restore'][$syslang].'" class="btn"></a>' : '';
		$button_delete	= '<a href="'.$_SERVER['PHP_SELF'].'?delete_id='.$aName[0].'" title="'.$aMSG['btn']['delete'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_delete.gif" alt="'.$aMSG['btn']['delete'][$syslang].'" class="btn"></a>';
?>
	<tr valign="top">
		<td><span class="text">
			<?php // erstellungsdatum aus filemtime() auslesen
			$restorefilemtime = filemtime($aENV['path']['sys_data']['unix'].'backup/'.$aArchivData[$i]);
			echo $oDate->get_today(true, $restorefilemtime).' ';
			echo $oDate->get_now(true, $restorefilemtime);
			?>
		</span></td>
		<td align="right" nowrap><p>
			<?php echo $button_save; ?>
			<?php echo $button_restore; ?>
			<?php echo $button_delete; ?>
		</p></td>
	</tr>

<?php  
	} // END for
?>
</table>
<br><br>
<? } ?>
<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>