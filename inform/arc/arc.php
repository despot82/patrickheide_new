<?php
/**
* arc.php
*
* Overviewpage: arc //-> 2sprachig und voll kopierbar!
*
* @param	int		$entry_id		[welcher Datensatz] (optional)
* @param	int		$start			[zum blaettern] (optional)
* @param	string	$arcSearchterm	[zum filtern der Anzeige um einen Suchbegriff] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	string	$arcFolder		[zum filtern der Anzeige um ein Directory (im Tree)] (optional) -> kommt ggf. aus der 'inc.sys_login.php'
* @param	array	Formular:		$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-04-12
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './arc.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$entry_id	= (isset($_GET['entry_id'])) ? $oFc->make_secure_int($_GET['entry_id']) : 0;
	$start		= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : 0;
	if (isset($_GET['arcSearchterm']))	{ $oSess->set_var('arcSearchterm', $_GET['arcSearchterm']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	if (isset($_GET['arcFolder']))		{ $oSess->set_var('arcFolder', $_GET['arcFolder']); } // GET ueberschreibt SESSION (wenn umgeschaltet werden soll)!
	$arcSearchterm	= $oSess->get_var('arcSearchterm'); // params: $sName[,$default=null]
	$arcFolder		= $oSess->get_var('arcFolder'); // params: $sName[,$default=null]
	$nAvailable		= (isset($_GET['available']) && $_GET['available']!='') ? $_GET['available']:null;   
	
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars:
	$sTable			= $aENV['table']['arc_main'];
	$sViewerPage	= $aENV['PHP_SELF'];					// fuer BACK-link
	$sThreadPage	= $aENV['PHP_SELF'];					// fuer List-Threads-link
	$sEditPage		= $aENV['SELF_PATH'].'arc_detail.php';	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;				// fuer NEW-button
	// CONTENT
	$limit = 20;	// Datensaetze pro Ausgabeseite einstellen
	// wenn kein suchbegriff aber 'recent' als thema -> thema auf 'all' setzen, da suchen auf 'new' nicht moeglich!
	if (!empty($arcSearchterm) && $arcFolder == 'new')	{ $arcFolder = 'all'; }
	// wenn kein suchbegriff aber alle themen -> thema auf 'recent' eingrenzen!
	if (empty($arcSearchterm) && $arcFolder == 'all') { $arcFolder = 'new'; }
	// set default / fallback
	if ($arcFolder == '')	{ $arcFolder = 'new'; }

// OBJECTS
	// FolderEA
	if(!$aENV['php5']) {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php');
	}
	else {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php5');
	}
	require_once($aENV['path']['global_bean']['unix'].'class.FolderBean.php');
	require_once($aENV['path']['global_service']['unix'].'class.DbNavi.php');
	
	$oFEA = new FolderEA($oDb);
	$oFEA->setTable($aENV['table']['global_tree']);
	$oFEA->setFlagtree('arc');
	$oFEA->initialize(0,0,0);
	$oArchiv->setFEA($oFEA);
	$oArchiv->setTable($aENV['table']['arc_data']);
	
	
	// TEAM ACCESS CONTROLL
	if(!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
	
	// TREE (VOR HTML!)
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], ARCHIV_STRUCTURE_FILE, 'arc', array("sOrderBy" => "prio DESC")); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) {
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	$oTree->buildTree();
	// User
	require_once($aENV['path']['global_service']['unix']."class.user.php");
	$oUser =& new user($oDb);
	
	// MEDIA-DB
	require_once($aENV['path']['global_service']['unix']."class.mediadb.php");
	$oMediadb =& new mediadb(); // params: - 

// CONTENT
	$aArchivData = array();

	if (!empty($arcSearchterm)) {
		// suche
		$aArchivData	= $oArchiv->getSearchResult($arcSearchterm, $oTree, $arcFolder, 0, 0, $nAvailable); // params: $sSearchtext[,$nTreeId]
	} else {
		if($arcFolder == 'all' || $arcFolder == 'new') {
			if($arcFolder != 'all') {
			// neueste
				$aArchivData = $oArchiv->getRecent($limit, $nAvailable);
			}
			else {
			// alle

				$aArchivData = $oArchiv->getAllArchivData($limit,$start, $nAvailable); // params: $nPostingId
			}
		}
		else {
			// ein bestimmtes

			$sConstraint	= ($nAvailable != "" ? (' AND avail="'.$nAvailable.'"'):'');
			
			$aArchivData = $oArchiv->getArchivData($arcFolder, $limit, $start, 'intnr ASC, title ASC', $sConstraint); // params: $nPostingId
		}
	}

	$oDBNavi = new DbNavi($Userdata,$aENV,$oArchiv->getEntries(), $start, $limit);
// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['arc']['unix']."inc.arc_folder.php");

?>

<form action="<?php echo $aENV['PHP_SELF']; ?>" method="get">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['archiv']['archiv'][$syslang]; ?></span></p></td>
		<td align="right"><?php // BUTTONS
		if ($entry_id > 0) { // BACK-Button nur wenn im Thread
			echo get_button("BACK", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"]
		}
		if ($oPerm->hasPriv('create') && $oTree->getCount()) {
			echo get_button('NEW', $syslang, 'smallbut'); // params: $sType,$sLang[,$sClass="but"] 
		} ?></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<td class="th"><!-- suche-modul -->
			<input type="text" name="arcSearchterm" value="<?php echo $arcSearchterm; ?>">
			<?php if ($oTree->getCount()) {
			// select ueberschreibt ggf. hidden-field
				$oTree->setRootOff();
				if ($mode_key == 'new') {$aData['tree_id'] = $oSess->get_var('arcFolder'); } // bei "neu" zuletzt eingestellten ordner anzeigen
				$oTree->addOption(array('new' => $aMSG['archiv']['recent_entries'][$syslang]));
				$oTree->addOption(array('0' => $aMSG['archiv']['root_folder'][$syslang])); // -> hier: "unsorted"
				$oTree->setAllName($aMSG['archiv']['all_folders'][$syslang]);
				echo $oTree->treeFormSelect("arcFolder", $arcFolder,'',''); // params: $name[,$currentId='']
			}

			echo '<select name="available">';
			echo '	<option '.($nAvailable=='' ? 'selected':'').' value="">--</option>';
			echo '	<option '.($nAvailable==1 ? 'selected':'').' value="1">'.$aMSG['archiv']['avail'][$syslang].'</option>';
			echo '	<option '.($nAvailable=='0' ? 'selected':'').' value="0">'.$aMSG['archiv']['notavail'][$syslang].'</option>';
			echo '</select>';
			
			// search button
			echo get_button("SEARCH", $syslang); // params: $sType,$sLang[,$sClass="but"] ?>
		</td>
	</tr>
</table>
</form>

<?php
if($entry_id == 0) {
// Search / Tree / Recent Postings ------------------------------------------------------------	
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
			<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></span></td>
			<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks((isset($_GET['available']) ? '&available='.$nAvailable:'')); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></span>
		</td>
	</tr>
</table><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="90%"><col width="5%"><col width="5%"></colgroup>
	<tr>
		<th width="90%"><?php echo $aMSG['archiv']['posting'][$syslang]; ?></th>
		<th colspan="2" width="10%">&nbsp;</th>
	</tr>
<?php	
	for ($i=0; !empty($aArchivData[$i]); $i++) {
		$aLabels = Tools::getLabels($oDb,$aENV,'arc',$aArchivData[$i]['tree_id']);
		
		// edit-button (Link zur Edit-Seite NUR wenn User==Creator)
		$button_edit	= ($oPerm->hasPriv('admin') || ($aData['created_by'] == $Userdata['id'] && $oPerm->hasPriv('edit')))
			? '<a href="'.$sEditPage.'?entry_id='.$entry_id.'&id='.$aArchivData[$i]['id'].'" title="'.$aMSG['std']['edit'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['std']['edit'][$syslang].'" class="btn"></a>'
			: '';
		// bg-color (label)
		$td_bgcolor		= (isset($aArchivData[$i]['label_id']) && $aArchivData[$i]['label_id'] != '' && $aArchivData[$i]['label_id'] != '0' && $aLabels[$aArchivData[$i]['label_id']]['color'] != '' && $aLabels[$aArchivData[$i]['label_id']]['treeid'] == $aArchivData[$i]['tree_id']) 
			? ' style="background-color:'.$aLabels[$aArchivData[$i]['label_id']]['color'].'"' 
			: '';
		// ggf. Number
		$nr				= (isset($aArchivData[$i]['intnr']) && $aArchivData[$i]['intnr'] != '' && $aArchivData[$i]['intnr'] != 0) 
			? $aMSG['form']['nr'][$syslang].''.$aArchivData[$i]['intnr'] 
			: '';
		// ggf. Label
		$label			= (isset($aArchivData[$i]['label_id']) && $aLabels[$aArchivData[$i]['label_id']]['treeid'] == $aArchivData[$i]['tree_id'] && $aArchivData[$i]['label_id'] != '' && $aArchivData[$i]['label_id'] != '0') 
			? '[ '.$aLabels[$aArchivData[$i]['label_id']]['name'].' ]' 
			: '';
		// ggf. Folder
		$folder			= ($arcFolder == 'all' || $arcFolder == 'new' || empty($arcFolder)) 
			? '<br><b>'.$aArchives[$aArchivData[$i]['tree_id']].'</b>' 
			: '';
		// Availibility
		$icon_avail		= ($aArchivData[$i]['avail'] == 0) 
			? '<img src="'.$aENV['path']['pix']['http'].'icn_off.gif" alt="'.$aMSG['archiv']['notavail'][$syslang].'">'  
			: '<img src="'.$aENV['path']['pix']['http'].'icn_on.gif" alt="'.$aMSG['archiv']['avail'][$syslang].'">'; 
?>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
				<td>
					<?php echo $oMediadb->getImageTag($aArchivData[$i]['img_id'], 'border="0" width="40" height="40" class="userPict"'); ?>
				</td>
				<td width="100%"<?php echo $td_bgcolor; ?>><span class="text">
					<?php echo $nr; ?>
					<a href="<?php echo $sThreadPage.'?entry_id='.$aArchivData[$i]['id'].'&id='.$aArchivData[$i]['id']; ?>" title="<?php echo $aMSG['std']['view'][$syslang]; ?>"><b><?php echo $aArchivData[$i]['title']; ?></b></a><br>
					<?php echo $label; ?>
					<?php echo $folder; ?>
					<small><?php echo Tools::formatKeywords($sThreadPage, array('arcSearchterm'=>$aArchivData[$i]['keywords']), array('arcFolder'=>'all')); ?></small>
				</span></td>
			</tr>
			</table>
		</td>
		<td align="right" nowrap><p><?php echo $icon_avail; ?></p></td>
		<td align="right" nowrap><p><?php echo $button_edit; ?></p></td>
	</tr>

<?php  
	}
	// No-Data MSG
	if(!is_array($aArchivData)) { echo '	<tr><td colspan="3" align="center"><p>'.$aMSG['std']['no_entries'][$syslang].'</p></td></tr>'; }
?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="text">
			<?php // "seite x von y" + Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></span></td>
			<td align="right"><span class="text"><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks(); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions=''][,$nPageLimit=10]
			?></span>
		</td>
	</tr>
</table>
	
<?php
} 
else {
// Ansicht EIN Posting -------------------------------------------------------------------------- 
	
	// get DATA
	$aData		= $oArchiv->getArchivEntry($entry_id); // params: $id
	
	$aLabels = Tools::getLabels($oDb,$aENV,'arc',$aData['tree_id']);
	// ACHTUNG: nach einem delete landet man hier, obwohl es den entry dann gar nicht mehr gibt...
	if (!isset($aData['id']) || empty($aData['id'])) { // ... deshalb:
		header('Location: '.$sViewerPage); exit;	// weiterleitung zu viewer-page!
	}
	// edit-button (Link zur Edit-Seite NUR wenn User==Creator)
	$button_edit	= ($oPerm->hasPriv('admin') || ($aData['created_by'] == $Userdata['id'] && $oPerm->hasPriv('edit')))
		? '<a href="'.$sEditPage.'?entry_id='.$entry_id.'&id='.$aData['id'].'" title="'.$aMSG['std']['edit'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['std']['edit'][$syslang].'" class="btn"></a>'
		: '';
	// Availibility
	$icon_avail		= ($aData['avail'] == 0) 
		? '<img src="'.$aENV['path']['pix']['http'].'icn_off.gif" alt="'.$aMSG['archiv']['notavail'][$syslang].'">'  
		: '<img src="'.$aENV['path']['pix']['http'].'icn_on.gif" alt="'.$aMSG['archiv']['avail'][$syslang].'">';
	// bg-color (label)
	$td_bgcolor		= (isset($aData['label_id']) && $aData['label_id'] != '' && $aData['label_id'] != '0') 
		? ' style="background-color:'.$aLabels[$aData['label_id']]['color'].'"' 
		: '';
	// ggf. Number
	$nr				= (isset($aData['intnr']) && $aData['intnr'] != '' && $aData['intnr'] != 0) 
		? $aMSG['form']['nr'][$syslang].''.$aData['intnr'] 
		: '';
	// ggf. Label
	$label			= (isset($aData['label_id']) && $aData['label_id'] != '' && $aData['label_id'] != '0') 
		? '[ '.$aLabels[$aData['label_id']]['name'].' ]' 
		: '';
	// ggf. Date
	$edate			= (isset($aData['edate']) && $aData['edate'] != '0000-00-00') 
		? $aMSG['form']['date'][$syslang].': '.$oDate->date_mysql2trad($aData['edate']).'<br>' 
		: '';
	// ggf. Link
	$link			= (isset($aData['link']) && $aData['link'] != '') 
		? $aMSG['form']['link'][$syslang].': <a href="'.link_uri($aData['link']).'" target="_blank">'.$aData['link'].'</a><br>' 
		: '';
?>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="90%"><col width="5%"><col width="5%"></colgroup>
	<tr>
		<th width="90%"><?php echo $aMSG['archiv']['posting'][$syslang]; ?></th>
		<th colspan="2" width="10%">&nbsp;</th>
	</tr>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr valign="top">
				<td><?php 
					echo $oMediadb->getImageTag($aArchivData[$i]['img_id'], 'border="0" width="40" height="40" class="userPict"');
				?></td>
				<td width="100%" <?php echo $td_bgcolor; ?>><span class="text">
					<?php echo $nr; ?>
					<b><?php echo $aData['title']; ?></b><br>
					<?php echo $label; ?>
					<small><?php echo Tools::formatKeywords($sThreadPage, array('arcSearchterm'=>$aData['keywords']), array('arcFolder'=>'all')); ?></small>
				</span></td>
			</tr>
			</table>
		</td>
		<td align="right" nowrap><p><?php echo $icon_avail; ?></p></td>
		<td align="right" nowrap><p><?php echo $button_edit; ?></p></td>
	</tr>
	<tr valign="top">
		<td class="sub2">
			<?php echo $oMediadb->getImageTag($aData['img_id'], 'border="0" align="left"'); ?>
			<span class="text">
			<?php echo nl2br($aData['content']); ?>
			<br><br>
			<?php echo $edate; ?>
			<?php echo $link; ?>
			<?php echo $aMSG['std']['first_created'][$syslang].': '.Tools::formatUsername($aData['created_by']); ?>
			<small><?php echo Tools::formatDatetime($oDate,$aData['created']); ?></small><br>
			</span>
		</td>
		<td colspan="2" class="off">&nbsp;</td>
	</tr>
</table>
	
<?php
// END Ansicht -------------------------------------------------------------------------- 
} ?>
<br><br>
<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
