<?php
/**
* arc_folder_detail.php
*
* Specialpage: Verwalte Archive-Ordner 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$tree_id	[welcher Datensatz] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2005-01-07
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './arc_folder_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	if(!$aENV['php5']) {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php');
	}
	else {
		include_once($aENV['path']['global_service']['unix'].'class.FolderEA.php5');
	}
	require_once($aENV['path']['global_service']['unix']."class.team.php");
	require_once($aENV['path']['global_service']['unix']."class.tree.php");
// 2a. GET-params abholen
	$id				= (isset($_GET['tree_id'])) ? $oFc->make_secure_int($_GET['tree_id']) : 0;
	// backup zur sicherheit!!!
	$treeid			= (isset($_GET['tree_id'])) ? $oFc->make_secure_int($_GET['tree_id']) : 0;
	$viewug			= (isset($_GET['viewug'])) ? $oFc->make_secure_string($_GET['viewug']) : '';
	$nToggleId		= (isset($_GET['nToggleId'])) ? $oFc->make_secure_int($_GET['nToggleId']) : '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$group			= (isset($_POST['group'])) ? $_POST['group'] : '';
	$user			= (isset($_POST['user'])) ? $_POST['user'] : '';
// 2c. Vars:
	$sTable			= $aENV['table']['global_tree'];
	$sGEToptions	= '?nToggleId='.$nToggleId;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].'?nToggleId='.$nToggleId;							// fuer NEW-button
	$sViewerPage	= $aENV['SELF_PATH']."arc_folder.php".$sGEToptions;

	$oFEA =& new FolderEA($oDb);
	$oFEA->setTable($aENV['table']['global_tree']);
	$oFEA->setFlagtree('arc');
	$oArchiv->setFEA($oFEA);
	if (isset($aData['id'])) { $id = $aData['id']; }
	
	if(!is_object($oTACEA)) {
		// TEAM ACCESS CONTROLL
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
	
	
// TREE (VOR Save!)
	$oTree =& new tree($oDb, $aENV['table']['global_tree'], ARCHIV_STRUCTURE_FILE, 'arc', array("sOrderBy" => "prio DESC")); // params: $oDb,$sTable[,$sCachefile=''][,$module=''][,$aOptions=array()]
	if (!$oPerm->isDaService()) { // ggf. nur authorisierte Datensaetze ausgeben
		if (!$oPerm->hasPriv('admin')) { // ggf. nur authorisierte Datensaetze ausgeben
			$oTree->setAuth($Userdata['id'], $oTACEA, $oSess); // params: $uid,$oTACEA,$oSess
		}
	}
	
	if(!isset($aData['flag_open'])) { 
		if(is_array($oTACEA->getRightForID($treeid,'arc'))) { 
			$flopen = 0; 
		} else { 
			$flopen = 1; 
		}
	}
	else {
		$flopen = $aData['flag_open'];	
	}
	unset($aData['flag_open']);			
// 3. DB
// DB-action: delete_all
	if (isset($btn['delete']) && $aData['id']) {
		$oArchiv->delete($aData['id']);
		$oTACEA->onDeleteAction($aData['id'],'arc');

		// delete tree-cache (file + session)
		$oTree->clearCache();
		
		// back to overview page
		header("Location: ".$sViewerPage); exit;
	}
// DB-action: save or update
		
	if (isset($btn['save']) || isset($btn['save_close'])) {
		
		$group = $aData['group'];
		$user = $aData['user'];
		$viewug = $aData['viewug'];
		unset($aData['group']);
		unset($aData['user']);
		unset($aData['viewug']);
		
		$oArchiv->save($aData);
		$treeid = $aData['id'];

		if(empty($treeid)) { // es war ein insert!
			$treeid = $oDb->insert_id(); // get last inserted id
		}
		if($flopen == 1) {
			
			$oTACEA->delRightForTree($treeid,'arc');
		}
		else {
		
			$oTACEA->addRights($group, $user,$treeid,'arc');
		}

		// delete tree-cache (file + session)
		$oTree->clearCache();
		
		if(isset($btn['save_close'])) {
			// back to overview page
			header("Location: ".$sViewerPage); exit;
		}
		if(isset($btn['save'])) {
			// back to overview page
			if($treeid==0) $treeid = $oDb->insert_id();
			header("Location: ".$aENV['PHP_SELF'].$sGEToptions.'&tree_id='.$treeid.'&viewug='.$viewug); exit;
		}
	}
// DB-action: select
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
	} else {
		$mode_key = "new"; // do not change!
	}

	if(!empty($viewug)) $aData['viewug'] = $viewug;
	$oTree->buildTree();
	$aStructure = $oTree->getAllChildren();

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['arc']['unix']."inc.arc_folder.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("title", $aMSG['form']['title'][$syslang].'.'); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	//$oDb->debug = true;
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>


<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] 
echo $oForm->hidden('id'); 
?>
<input type="hidden" name="remoteSave" value="0">
<?=$oForm->hidden('flag_tree','arc');?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
	<td><p><span class="title"><?php echo $aMSG['archiv']['folder'][$syslang]; ?></span>
		<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p></td>
	<td align="right"><span class="text"><?php
		echo '<a href="'.$sViewerPage.'" title="'.$aMSG['btn']['back'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['btn']['back'][$syslang].'" class="btn"></a>';
		if ($oPerm->hasPriv('create')) { echo get_button("NEW", $syslang, "smallbut"); } // params: $sType,$sLang[,$sClass="but"]
	?></span></td>
</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<colgroup><col width="20%"><col width="70%"></colgroup>
<tr valign="top">
	<td><p><b><?php echo $aMSG['form']['name'][$syslang]; ?> *</b></p></td>
	<td>
	<?php echo $oForm->textfield("title",250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
	</td>
</tr>
<tr valign="top">
	<td><p><b><?php echo $aMSG['form']['archivstatus'][$syslang]; ?></b></p></td>
	<td>
<? 	
	$param = array('first' => '20','second' => '70','border'=>'0','cellspacing'=>'1','cellpadding'=>'2','class' =>"tabelle");?>
<?=$oTACEA->showTeamAccess($aData,$syslang,$treeid,$flopen,$aMSG,$param,'arc',true); ?>
	</td>
</tr>
<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>