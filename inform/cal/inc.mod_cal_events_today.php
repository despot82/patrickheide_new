<?php
/*
 * Portal-Modul cal_events_today 
 */

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mod_cal_events_today.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

	// ggf. USER zwischenspeichern (key: ID, value: Name)
	if (!is_array($aUsername)) {
		$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
		$aUsername = $oUser->getAllUserNames(false); // params: [$bValidOnly=true]
	}
	// vars
	$sEditPage		= $aENV['path']['cal']['http'].'cal_calendar_detail.php';
	$sNewPage		= $sEditPage;
	$sDayViewPage	= $aENV['path']['cal']['http'].'cal_calendar.php?view=day';
	
	// ggf. TEAMACCESS
	if(!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
	
	// create new calendar object.
	if(!$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	}
	else {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php5");
	}
	$oCal =& new calendar($syslang,$oTACEA);
	
	// apply filter
	$aFilter = array("show_birthdays" => true, "office" => $Userdata['office_id']);
	$oCal->setFilter($aFilter); // params: $aFilter
	
	// get EVENTS
	$aEvents	= $oCal->getDayEvents($oDb); // params: &$oDb[,$day=NULL][,$media='']
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabelle">
<tr>
	<td class="th" width="75%"><b><?php echo $aMSG['portal']['cal_events_today'][$syslang]; ?></b> <?php
		// office-name
		$aOffice = $oUser->getOfficeNames($Userdata['office_id']); // params: [$officeid]
		echo '[ '.current($aOffice).' ]'; ?></td>
	<td class="th" width="25%" align="right" nowrap>
		<a href="<?php echo $sDayViewPage;?>" title="<?php echo $aMSG['portal']['cal_events_today'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_goto.gif" alt="<?php echo $aMSG['portal']['cal_events_today'][$syslang]; ?>" class="btn"></a><?php 
		if ($oPerm->hasPriv('create', 'cal')) { ?><a href="<?php echo $sNewPage; ?>" title="<?php echo $aMSG['std']['new'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_add.gif" alt="<?php echo $aMSG['std']['new'][$syslang]; ?>" class="btn"></a><?php } ?>
	</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php // printout events
	foreach ($aEvents as $k => $event) {
?>
	<tr valign="top">
		<td width="35%" class="sub2"><div style="float:left"><span class="text"><?php
		// time
		echo strip_tags($event['show_time']); // show_time ohne <br> am ende
		if (!empty($event['show_total_time'])) echo '<br><small>'.$event['show_total_time'].'</small>'; 
		?></span></div><div style="float:right; padding:1px"><?php
		// icon
		echo " ".$event['icon']; ?>
		</div></td>
		<td width="65%" class="sub2"><span class="text">
		<?php
		$sStateConfirmed = ($aENV['flag_confirmed'] && $event['flag_confirmed'] == 0) ? '[tbc] ' : ''; 
		echo ($oPerm->hasPriv('edit', 'cal')) 
				? '<a href="'.$sEditPage.'?id='.$event['id'].'">'.$sStateConfirmed.$event['subject'].'</a>' 
				: $sStateConfirmed.$event['subject'];
		echo ''.$event['sign_multidays']; ?><br>
		<?php if (!empty($event['location'])) echo $event['location'].'<br>'; ?>
		<?php if (!empty($event['participant'])) echo $aMSG['form']['participant'][$syslang].': '.$event['participant'].'<br>'; ?>
		<?php if (!empty($event['comment'])) echo '<small>'.nl2br($event['comment']).'<br></small>'; ?>
		</span></td>
	</tr>
	
<?php } // END foreach
	// no events?
	if (count($aEvents) == 0) {
		echo '<tr><td colspan="2" class="sub2"><span class="text">'.$aMSG['form']['nodates'][$syslang].'</span></td></tr>';
	}
?>
	</tr>
</table>