<?php
/**
* cal_calender.php
*
* Overviewpage: Calendar 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	string	$view		[zur Unterscheidung der Ansicht] (optional)
* @param	string	$filter		[zur Unterscheidung der Ansicht] (optional)
* @param	string	$office_id	[zur Unterscheidung der Ansicht] (optional)
* @param	int		$date_ts	[timestamp zum anzeigen von bestimmten tagen/wochen/..] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-07-14
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cal_calendar.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
// 2b. POST-params abholen
	$years	= getYears();
	$year = (isset($_POST['year'])) ? $_POST['year'] : date('Y');
	if (is_array($years)) {
		if (!in_array($year, $years)) { $year = $years[0]; }
	}
	
// vars

	function getYears() {
		global $oDb;
		$sql = "SELECT DISTINCT(YEAR(event_start)) as year FROM cal_event WHERE label='sick' OR label='leave'";
		$oDb->query($sql);
		for($i=0;$row=$oDb->fetch_object();$i++) {
			$years[] = $row->year;	
		}
		return $years;
	}
	function getYearDropdown($cyear) {
		$years = getYears();
		if (!is_array($years)) {
			$years = array($cyear);
		}
		$drop = '<select name="year" onChange="document.editForm.submit()">'."\n";
		for($i=0;!empty($years[$i]);$i++) {
			$sel = ($cyear == $years[$i]) ? 'selected' : '';
			$drop .= '<option value="'.$years[$i].'" '.$sel.'>'.$years[$i].'</option>'."\n";
		}
		$drop .= "</select>\n";
		return $drop;
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// create new calendar object.
	if(!$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	}
	else {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php5");
	}
	$oCal =& new calendar();

// get all usernames
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUsername = $oUser->getAllUserNames(true); // params: [$bValidOnly=true]
	$aEvents = $oCal->getUserEvents($year,$oDb,$aUsername);
	$oForm =& new form_admin($aData);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><span class="title"><?php echo $aMSG['topnavi']['yearview'][$syslang];#$oDate->get_dayname().", ".$oDate->get_today_long(); ?></span> 
			<span class="text"><br></span>
		</td>
		<td align="right"><?php // Buttons 
			#echo $oForm->button("BACK"); // params: $sType[,$sClass='']
			#echo $oForm->button("NEW"); // params: $sType[,$sClass='']
		?></td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<td class="th"><?=$oForm->start_tag()?><?=getYearDropdown($year)?><?=$oForm->end_tag()?></td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">

	<tr valign="top">
		<th width="64%"><?php echo $aMSG['form']['name'][$syslang]; ?></th>
		<th width="12%"><?php echo $aMSG['cal']['holiday'][$syslang]; ?></th>
		<th width="12%"><?php echo $aMSG['cal']['sick'][$syslang]; ?></th>
		<th width="12%"><?php echo $aMSG['cal']['lieu'][$syslang]; ?></th>
	</tr>
<? foreach ($aUsername as $uid => $val) { 
	if($oPerm->hasPriv('admin','adm') || $uid == $Userdata['id']) {	
?>
	<tr>
		<td colspan="1"><p><b><?php 
			echo $val; ?></b></p>
		</td>
		<td><p>
		<?php if ($aEvents[$uid]['leave']['day']!="0") { echo $aEvents[$uid]['leave']['day'].' '.$aMSG['cal']['d'][$syslang]; } ?>
		<?php if ($aEvents[$uid]['leave']['day']!="0" && $aEvents[$uid]['leave']['hour']!="0") { echo ' / '; } ?>
		<?php if ($aEvents[$uid]['leave']['hour']!="0") { echo $aEvents[$uid]['leave']['hour'].' '.$aMSG['cal']['h'][$syslang]; } ?>
		</p></td>
		<td class="sub2"><p>
		<?php if ($aEvents[$uid]['sick']['day']!="0") { echo $aEvents[$uid]['sick']['day'].' '.$aMSG['cal']['d'][$syslang]; } ?>
		<?php if ($aEvents[$uid]['sick']['day']!="0" && $aEvents[$uid]['sick']['hour']!="0") { echo ' / '; } ?>
		<?php if ($aEvents[$uid]['sick']['hour']!="0") { echo $aEvents[$uid]['sick']['hour'].' '.$aMSG['cal']['h'][$syslang]; } ?>
		</p></td>
		<td class="sub2"><p>
		<?php if ($aEvents[$uid]['lieu']['day']!="0") { echo $aEvents[$uid]['lieu']['day'].' '.$aMSG['cal']['d'][$syslang]; } ?>
		<?php if ($aEvents[$uid]['lieu']['day']!="0" && $aEvents[$uid]['lieu']['hour']!="0") { echo ' / '; } ?>
		<?php if ($aEvents[$uid]['lieu']['hour']!="0") { echo $aEvents[$uid]['lieu']['hour'].' '.$aMSG['cal']['h'][$syslang]; } ?>
		</p></td>
	</tr>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
<? }
} ?>
</table>
<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
