<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.cal_day.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$today_ts		= time();
	$date_ts		= (isset($_GET['date_ts'])) ? $oFc->make_secure_int($_GET['date_ts']) : $today_ts; // default: heute
	$filter			= (isset($_GET['filter'])) ? $oFc->make_secure_string($_GET['filter']) : '';
	$office_id		= (isset($_GET['office_id'])) ? $oFc->make_secure_int($_GET['office_id']) : '';
	$label	= (isset($_GET['label'])) ? $_GET['label'] : '';
	$sGEToptions	= '?date_ts='.$date_ts.'&view=day';
	if (!empty($filter)) 	$sGEToptions	.= '&filter='.$filter;
	if (!empty($office_id)) $sGEToptions	.= '&office_id='.$office_id;
	if(!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
	$aLabels = Tools::getLabels($oDb,$aENV,'cal','business');
// vars
	$sEditPage	= $aENV['SELF_PATH'].'cal_calendar_detail.php'.$sGEToptions;

// create new calendar object.	
	if(!$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	}
	else {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php5");
	}
	$oCal =& new calendar($syslang,$oTACEA);
	// apply filter
	if ($filter == 'my') {
		$aFilter = array("show_birthdays" => false, "participant" => $Userdata['id']);
	}
	if ($filter == 'office') {
		$aFilter = array("show_birthdays" => true, "office" => $office_id);
	}
	if (empty($filter)) { // default = my office
		$aFilter = array("show_birthdays" => true, "office" => $Userdata['office_id']);
	}
	
	if($label != '') {
		
		$aFilter['label'] = $label;
		
	}
	$oCal->setFilter($aFilter); // params: $aFilter 

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $aENV['charset']; ?>">
	<meta http-equiv="content-language" content="<?php echo $syslang; ?>">
	<title>Calendar - DAY</title>
	<meta name="robots" content="noindex">
	<meta name="copyright" content="design aspekt; http://www.design-aspekt.com">
	<meta name="author" content="design aspekt; http://www.design-aspekt.com">
	<meta name="generator" content="AF,MG; e-mail: kontakt@design-aspekt.com">
	<meta name="date" content="2005-03-07">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="<?php echo $aENV['path']['css']['http'].$sStylesheet; ?>styles.css" type="text/css" rel="stylesheet">
	<style type="text/css">
<?php if ($_GET['printview'] != "true") { require_once($aENV['path']['config']['unix']."styles.php"); } ?>
	</style>
	<style type="text/css">
<?php // GRID styles
	$grid_height = _calculateGridHeight(); // td = 1/2h * 2 = 1h *24 = 1 Tag (@see setup.php) ?>
	#calBgTable		{position:absolute; top:0px; left:0px}
	.calBgTable		{width:100%}
	.calBgTD1		{height:<?php echo ($aCalConfig['grid_td_height'] - 1); ?>px; background-color:#FFFFFF} /* border-bottom:1px dotted black; */
	.calBgLine1		{height:1px; background-color:#FFFFFF}
	.calBgTD2		{height:<?php echo ($aCalConfig['grid_td_height'] - 1); ?>px; background-color:#F5F5F8}
	.calBgLine2		{height:1px; background-color:#E8E8EB}
	#calBgEvents	{position:absolute; background-color:transparent; top:0px; left:50px; width:90%; height:<?php echo $grid_height; ?>px; border-left:1px solid #BCBDC3}
	
<?php if ($oBrowser->isIE() && $oBrowser->isWin()) { // "overflow:hidden" nur auf Win-IE!
		// weil IE auf OSX zeigt mit "overflow:hidden" gar nichts an!
		// Moz kann ohne "overflow:hidden" lustigerweise den Text overflowen ohne die Box samt Hintergrundfarbe aufzudruecken (nettes Feature ;-) ?>
	.eventBoxDaybusiness	{overflow:hidden}
	.eventBoxDayprivate		{overflow:hidden}
	.eventBoxDayleave		{overflow:hidden}
	.eventBoxDaysick		{overflow:hidden}
	.eventBoxDayholiday		{overflow:hidden}
<?php } ?>
	</style>
</head>
<body>

<!-- GRID -->
<div id="calBgTable">
	<table border="0" cellpadding="0" cellspacing="0" class="calBgTable">
<?php // print table rows
for ($i=0; $i < 24; $i++) {
	if (strlen($i) == 1) $i = '0'.$i;?>
	<tr><td	class="calBgTD1"><p><a name="<?php echo $i; ?>"></a>&nbsp;<a href="<?php echo $sEditPage.'&time_start='.$i.':00'; ?>" title="<?php echo $aMSG['mode']['new'][$syslang]; ?>" target="_parent"><?php echo $i; ?>:00</a></p></td></tr>
	<tr><td	class="calBgLine1"></td></tr>
	<tr><td	class="calBgTD2"><p>&nbsp;<a href="<?php echo $sEditPage.'&time_start='.$i.':30'; ?>" title="<?php echo $aMSG['mode']['new'][$syslang]; ?>" target="_parent"><?php echo $i; ?>:30</a></p></td></tr>
	<tr><td	class="calBgLine2"></td></tr>
<?php
} // END for ?>
	</table>
</div>

<!-- EVENTS -->
<div id="calBgEvents">
<?php
	// USER zwischenspeichern (key: ID, value: Name)
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUsername = $oUser->getAllUserNames(false); // params: [$bValidOnly=true]

	// PROJECTS zwischenspeichern (key: ID, value: Name)
	if (isset($aENV['module']['adm'])) {
		$aProject		= _getProjectArray($oDb);
	}

	// get EVENTS
	$aEvents		= $oCal->getDayEvents($oDb, $date_ts); // params: &$oDb[,$day=NULL][,$media='']
	
	// vars
	$countEvents	= count($aEvents);
	$boxWidth		= ($countEvents > 0) ? floor(100 / $countEvents - 1) : 0; // jeweils 1% weniger wegen umbruchgefahr!
	if ($boxWidth > $aCalConfig['grid_box_width']) { $boxWidth = $aCalConfig['grid_box_width']; } // maximale Breite (@see setup.php)
	$iso_today		= date("Y-m-d", $date_ts);
	$left = 0;
	
	// build BOXES
	foreach ($aEvents as $k => $event) {
		// box class/top/height
		$aBox = _calculateBox($event, $iso_today, $aLabels);
?>
	<!-- EVENT-BOX -->
	<div style="left:<?php echo $left; ?>%; width:<?php echo $boxWidth; ?>%; top:<?php echo $aBox['outer_top']; ?>px; height:<?php echo $aBox['outer_height']; ?>px" class="eventBoxAnfahrt">
		<div class="<?php echo $aBox['inner_class']; ?>" style="<?php echo $aBox['inner_style']; ?>">
		<table width="100%" height="98%" border="0" cellpadding="0" cellspacing="0"><tr><td><div style="padding:5px" class="text">
		<?php echo $event['icon']; ?>
		<? if($aENV['flag_confirmed'] && $event['flag_confirmed'] == 0) {
			//TODO icon?
			 echo '[tbc]'; 
		} ?>
		<a href="<?php echo $sEditPage.'&id='.$event['id']; ?>" target="_parent"><?php echo $event['subject']; ?></a> <?php echo $event['sign_multidays']; ?><br>
		<?php echo $event['show_time']; ?>
		<small><?php echo $event['show_total_time']; ?></small>
		<?php if (!empty($event['location'])) echo $aMSG['form']['location'][$syslang].': '.$event['location'].'<br>'; ?>
		<?php #if (!empty($event['label'])) echo $aScheduleLabel[$syslang][$event['label']].'<br>'; ?>
		<?php if (!empty($event['participant'])) echo $aMSG['form']['participant'][$syslang].': '.$event['participant'].'<br>'; ?>
		<?php if (!empty($event['project_id']) && is_array($aProject)) $aProject[$event['project_id']].'<br>'; ?>
		<?php if (!empty($event['comment'])) echo '<br>'.$event['comment'].'<br>'; #echo $event['hours_today']; ?>
		</div></td></tr></table>
		</div>
	</div>

<?php	$left += $boxWidth;
	} // END foreach $aEvents ?>
</div>

</body>
</html>
<?php // HILFSFUNKTIONEN /////////////////////////////////////////////////////////////

	function _calculateGridHeight() {
		global $aCalConfig; // td = 1/2h * 2 = 1h *24 = 1 Tag (@see setup.php)
		return ($aCalConfig['grid_td_height'] * 48);
	}
	
	function _calculateBox(&$event, $date_today, $aLabels) {
		// vars
		global $aENV;
		global $aCalConfig; // @see setup.php
		
		$grid_height = _calculateGridHeight();
		$aBox = array();
		$pixelperhour = $aCalConfig['grid_td_height'] * 2;
		
		// box top (in px)
		if ($event['date_start'] == $date_today) {
			$aBox['outer_top'] = (substr($event['time_start'], 0, 2) - $event['arrival']) * $pixelperhour;
			if (substr($event['time_start'], 3, 5) == '30') {
				$aBox['outer_top'] += $aCalConfig['grid_td_height'];
			}
			// arrival dazurechnen (kann auch 0 sein)
			$aBox['inner_top'] = $event['arrival'] * $pixelperhour;
			// fallback: wenn anreise laenger als der zur Verfuegung stehende Platz ist
			if ($aBox['outer_top'] < 0) { // ungueltigen minuswert fuer weitere berechnungen zwischenspeichern
				$aBox['inner_top'] = $aBox['inner_top'] + $aBox['outer_top']; // + weil overhead negativ ist!
				$aBox['outer_top'] = 0;
			}
		} else { // wenn start-date i.d. Vergangenheit liegt
			$aBox['outer_top'] = 0; // @see setup.php
			$aBox['inner_top'] = 0; // @see setup.php
		}
		
		// box height (in px)
		$aBox['inner_height'] = ($event['hours_today'] * $pixelperhour) - 1; // am ende 1px abziehen -> passt beser
		$aBox['outer_height'] = $aBox['inner_height'];
		if ($event['arrival'] > 0 && $event['date_start'] == $date_today) { // wenn heute anreise laenger
			$aBox['outer_height'] = $aBox['outer_height'] + ($event['arrival'] * $pixelperhour);
		}
		if ($event['return'] > 0 && $event['date_end'] == $date_today) { // wenn heute abreise laenger
			$aBox['outer_height'] = $aBox['outer_height'] + ($event['return'] * $pixelperhour);
		}
		#if ($aBox['outer_height'] > $grid_height) { $aBox['outer_height'] = $grid_height - 1; } // fallback fallback ;-)
		
		// box styles 
		$aBox['inner_class']	= 'eventBoxDay'.$event['label'];
		$aBox['inner_style']	= 'top:'.$aBox['inner_top'].'px; height:'.$aBox['inner_height'].'px;';
		if (!empty($event['labelid']) && !empty($aLabels[$event['labelid']]['color'])) { // class-farbe ggf. mit labelfarbe ueberschreiben: 
			$aBox['inner_style'] .= ' background-color:'.$aLabels[$event['labelid']]['color'].';';
		}
		
		// output
		return $aBox;
	}
	
	// USER: (kommagetrennte) ID(s) in Name(n) umwandeln
	function _formatUserNames(&$aUsername, $sUserId) {
		$tmp = array();
		$aUserId = explode(',', $sUserId);
		foreach ($aUserId as $uid) { $tmp[] = $aUsername[$uid]; }
		return implode(', ', $tmp).'';
	}
	
	// Projects zwischenspeichern (key: ID, value: Name)
	function &_getProjectArray(&$oDb) {
		$buffer = array('' => '--');
		$oDb->query("SELECT p.id, p.projectname, p.prj_nr, p.prj_status, c.client_shortname, c.client_nr
					FROM adm_project p, adr_contact c 
					WHERE c.id = p.client_id 
					ORDER BY c.client_shortname ASC, p.projectname ASC");
		while ($tmp = $oDb->fetch_array()) {
			$buffer[$tmp['id']] = $tmp['client_shortname'].' - '.$tmp['projectname'].' [ '.$tmp['client_nr'].'.'.$tmp['prj_nr'].' ]';
		}
		return $buffer;
	}
?>
