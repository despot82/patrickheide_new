// Spezielle Funktionen fuer die Seite cal_calendar_detail.php //////////////////////////

function updateEndTime(time_start) { // enduhrzeit ggf. anpassen
	with (document.forms['editForm']) {
		if (makeIntDate(date_end.value) == makeIntDate(date_start.value)) { 
			vStart = time_start.value;
			writeTimeOptions(time_end, vStart);
		} else {
			writeTimeOptions(time_end, "00:00");
		}
	}
}

function writeTimeOptions(obj, vStart) {
	aTimeparts = vStart.split(":");
	
	// fuer den Vergleich mit gespeicherter End-Zeit
	nachkomma = (aTimeparts[1] == "30") ? ".5" : ".0";
	startSelected = parseFloat(aTimeparts[0]) + parseFloat(nachkomma);
	
	// select-feld
	vStart = ((aTimeparts[0]-0) + (aTimeparts[1]-0)/60)+.5;	
	vLength = (24*2)-(vStart*2) + 1;
	
	obj.options.length = vLength;
	for (r=0; r<vLength; r++) { 
		vTime = (r + (vStart*2))/2;
					
		vHour = Math.floor(vTime);
		if (vHour < 10) { vHour = '0' + vHour; }
		vMin = (vTime-vHour)*60;
		if (vMin == 0) { vMin = "0" + vMin; }
		
		obj.options[r].value = vHour + ':' + vMin;
		obj.options[r].text = vHour + ':' + vMin;		
	}
	
	/*
	if (newEntry == true || startSelected >= timeEndSavedFloat) { obj.options[0].selected = true; }
	else {
		for (r=0; r<vLength; r++) { 
			if (obj.options[r].value == timeEndSaved) { obj.options[r].selected = true; }
		}
	}
	*/	
	
	selEntry = (timeSpan / 0.5) - 1;
	if (selEntry < obj.options.length) { obj.options[selEntry].selected = true; }
	else { obj.options[obj.options.length-1].selected = true; }	
}


function makeIntDate(val) { // hilfsfunktion zum einfacheren vergleichen von start- und end-datum
	aPart = val.split(trenner); // -> aus 12.03.2005 wird 20050312
	aPart[0] = aPart[0].replace(/(\D)*/, ''); // alles ausser zahlen entfernen
	aPart[1] = aPart[1].replace(/(\D)*/, ''); // alles ausser zahlen entfernen
	aPart[2] = aPart[2].replace(/(\D)*/, ''); // alles ausser zahlen entfernen
	return parseInt(aPart[2] + aPart[1] + aPart[0]);
}

function floatEndTime(val) { // hilfsfunktion zum umwandeln der endzeit in fliesskomma
	timeEndSaved = val;
	aParts = timeEndSaved.split(":");
	aParts[1] = (aParts[1] == "30") ? ".5" : ".0";
	timeEndSavedFloat = parseFloat(aParts[0]) + parseFloat(aParts[1]);
	
	timeSpan = (startSelected != undefined) ? timeEndSavedFloat-startSelected : timeEndSavedFloat-timeStartSavedFloat;
}

function updateEndDate(date_start) { // enddatum ggf. anpassen
	date_start = checkDate(date_start);
	with (document.forms['editForm']) {
		if (makeIntDate(date_end.value) < makeIntDate(date_start.value)) {
			date_end.value = date_start.value;
		}
	}
}

function switchAllday() { // uhrzeiten ein-/ausblenden + auf defaults setzen
	with (document.forms['editForm']) {
		// layer unsichtbar/sichtbar schalten
		divStart = getLayer('tStart');
		divStart.visibility = (flag_allday.checked == true) ? "hidden" : "visible";
		divEnd = getLayer('tEnd');
		divEnd.visibility = (flag_allday.checked == true) ? "hidden" : "visible";
		// times auf defaultwerte setzen
		if (flag_allday.checked == true) {
			time_start.selectedIndex = 0;
			time_end.selectedIndex = time_end.options.length-1;
			event_arrival.selectedIndex = 0;
			event_return.selectedIndex = 0;
		}
	}
}

