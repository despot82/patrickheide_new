<?php
/*
 * Portal-Modul minicalendar 
 */

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mod_minicalendar.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

	// ggf. TEAMACCESS
	if(!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
	
	// create new calendar object.
	if(!$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	}
	else {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php5");
	}
	$oCal =& new calendar($syslang, $oTACEA);
	
	// nur auf Detailseite linken, wenn modul "CAL" auch installiert ist! -------------------------------------
	$sHref = ''; // set default
	$sKwHref = ''; // set default
	if (isset($aENV['module']['cal'])) {
		// eigene termine ermitteln
		$oCal->addMyDates($oDb); // params: &$oDb
		// default ueberschreiben
		$sHref = $aENV['path']['cal']['http'].'cal_calendar.php?&view=day&date_ts={TS}'; // ({TS} = Platzhalter -> wird erstzt!)
		$sMonthHref = $aENV['path']['cal']['http'].'cal_calendar.php?&view=month&date_ts={TS}';
		if ($aENV['cal']['cwweeks'] == true) {
			// ggf. KW verlinken
			$sKwHref = $aENV['path']['cal']['http'].'cal_calendar.php?&view=week&date_ts={TS}'; // ({TS} = Platzhalter -> wird erstzt!)
		}
	} // END CAL ----------------------------------------------------------------------------------------------
    // output calendar
    echo $oCal->getMinicalendarHtml($sHref, '', $aENV['cal']['cwweeks'], $sKwHref,$sMonthHref); // params: [$sHref=''][,$sButAddGetVars=''][,$bWithCw=false][,$sCwHref=''] 
?>
<?php if (isset($aENV['module']['cal']) && $oPerm->hasPriv('view', 'cal')) { ?>
<table border="0" cellspacing="2" cellpadding="2"><tr>
	<td id="calHighlightHome"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="8" height="8" alt="" border="0"></td>
	<td><span class="small"><?php echo $aMSG['form']['mydates'][$syslang]; ?></span></td>
	<td><span class="small">&nbsp;</span></td>
	<td id="calToday"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="8" height="8" alt="" border="0"></td>
	<td><span class="small"><?php echo $aMSG['form']['today'][$syslang]; ?></span></td>
</tr></table>
<?php } ?>