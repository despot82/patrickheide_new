<?php
/**
* cal_calender.php
*
* Overviewpage: Calendar 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	string	$view		[zur Unterscheidung der Ansicht] (optional)
* @param	string	$filter		[zur Unterscheidung der Ansicht] (optional)
* @param	string	$office_id	[zur Unterscheidung der Ansicht] (optional)
* @param	int		$date_ts	[timestamp zum anzeigen von bestimmten tagen/wochen/..] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2004-07-14
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cal_calendar.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$today_ts		= time();
	$date_ts		= (isset($_GET['date_ts']) && !empty($_GET['date_ts'])) ? $oFc->make_secure_int($_GET['date_ts']) : $today_ts; // default: heute
	$view			= (isset($_GET['view']) && !empty($_GET['view'])) ? $oFc->make_secure_string($_GET['view']) : 'week';
	$filter			= (isset($_GET['filter'])) ? $oFc->make_secure_string($_GET['filter']) : '';
	$office_id		= (isset($_GET['office_id'])) ? $oFc->make_secure_int($_GET['office_id']) : '';
	$label			= (isset($_GET['label'])) ? $oFc->make_secure_string($_GET['label']) : '';

// 2b. POST-params abholen
// vars
	$date_ts_pregsearch	= '/(\?|\&)date_ts=([0-9])*/';
	$view_pregsearch	= '/(\?|\&)view=(day|week|month)/';
	
	$sGEToptions		= '?view='.$view.'&date_ts='.$date_ts;
	if (!empty($filter))	{ $sGEToptions .= '&filter='.$filter; } // filter-vars nur bei bedarf!
	if (!empty($office_id))	{ $sGEToptions .= '&office_id='.$office_id; }
	if (!empty($label))		{ $sGEToptions .= '&label='.$label; }
	
	$sEditPage	= $aENV['SELF_PATH'].'cal_calendar_detail.php'.$sGEToptions;
	$sNewPage	= $sEditPage;
	
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();
	
// date
	// init timestamp in date-object
	$oDate->set_timestamp($date_ts);
	if ($view == "week" || $view == "workweek") {
		$kw = $oDate->get_kw(); // params: [$timestamp]
		$year = $oDate->get_php_date("Y");
		$ts_kw_monday = $oDate->get_mondaykw_timestamp($kw, $year); // params: [$kw, $jahr]
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// create new calendar object.
	if(!$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	}
	else {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php5");
	}
	$oCal =& new calendar($syslang, $oTACEA);
	// apply filter
	if ($filter == 'my') {
		$aFilter = array("show_birthdays" => false, "participant" => $Userdata['id']);
	}
	if ($filter == 'office') {
		$aFilter = array("show_birthdays" => true, "office" => $office_id);
	}
	if ($filter == 'all') {
		$aFilter = array("show_birthdays" => true);
	}

	if (empty($filter) && isset($Userdata['office_id'])) { // default = my office
		$aFilter = array("show_birthdays" => true, "office" => $Userdata['office_id']);
	}
	if(!empty($label)) {
		$aFilter['label'] = $label;
	}
	if(is_array($aFilter)) {
		$oCal->setFilter($aFilter); // params: $aFilter
	}
// get all usernames
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUsername = $oUser->getAllUserNames(false); // params: [$bValidOnly=true]

// der NEW-Button soll in der Wochen- und Monatsansicht nur angezeigt werden, wenn der aktuelle User das view-privileg hat
	$bWithNewButton = ($oPerm->hasPriv('create', 'cal')) ? true : false;
	
// FLYBYs Javascript + Layer
	require_once ("./inc.cal_flyby.php");
?>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<span class="title" valign="bottom"><?php 
		// Anzeige des aktuellen Datums
		if ($view == "day") {
			echo $oDate->get_dayname().', ';
			echo $oDate->get_today_long();	// volles Datum
		}
		if ($view == "week" || $view == "workweek") {
			// Kalenderwoche
			echo $aMSG['form']['kw'][$syslang].' '.$kw.', ';
			// timestamp vom ende der woche
			if ($view == "week")		$plusdays = 6;
			if ($view == "workweek")	$plusdays = 4;
			$ts_endofweek = $oDate->add_days($plusdays, $ts_kw_monday);
			// Monat
			$monthname = $oDate->get_monthname($ts_kw_monday);
			$monthname2 = $oDate->get_monthname($ts_endofweek);
			// Jahr
			$yearname = $oDate->get_php_date("Y");
			$yearname2 = $oDate->get_php_date("Y", $ts_endofweek);
			// output => logik: "januar 2006" oder "januar - februar 2006" oder "dezember 2005 - januar 2006"
			$monthname2 = ($monthname == $monthname2) ? '' : ' - '.$monthname2;
			$yearname = ($yearname == $yearname2) ? '' : ' '.$yearname;
			echo $monthname.$yearname.$monthname2.' '.$yearname2;
		}
		if ($view == "month") {
			echo $oDate->get_monthname().' '; // Monat
			echo $oDate->get_php_date("Y");	// Jahr
		} ?><br></span>
		</td>
		<td align="right"><?php // Buttons zum blaettern
		$self = $aENV['PHP_SELF'].preg_replace($date_ts_pregsearch, '', $sGEToptions).'&date_ts=';
		if ($view == "day") {
			$prev_day_ts = $oDate->add_days(-1);
			$next_day_ts = $oDate->add_days(1);
		}
		if ($view == "week" || $view == "workweek") {
			$prev_day_ts = $oDate->add_days(-7);
			$next_day_ts = $oDate->add_days(7);
		}
		if ($view == "month") {
			$prev_day_ts = $oDate->add_months(-1);
			$next_day_ts = $oDate->add_months(1);
		}
		?>
		<table border="0" cellspacing="0" cellpadding="0">
		<tr valign="top"><td><span class="text">
		<?php
		// zurueck
		echo '<input type="button" value="&lt;" class="smallbut" onclick="location.href=\''.$self.$prev_day_ts.'\'">';
		// heute
		echo '<input type="button" value="'.$aMSG['btn']['today'][$syslang].'" class="smallbut" onclick="location.href=\''.$self.$today_ts.'\'">';
		// vorwaerts
		echo '<input type="button" value="&gt;" class="smallbut" onclick="location.href=\''.$self.$next_day_ts.'\'">';
		?>
		</span></td><td><span class="text">&nbsp;
		<?php
		// day
		$btnstyle = ($view == 'day') ? 'btnon' : 'btn';
		echo '<a href="'.$aENV['PHP_SELF'].'?view=day'.preg_replace($view_pregsearch, '', $sGEToptions).'" title="'.$aMSG['cal']['viewday'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_day.gif" alt="'.$aMSG['cal']['viewday'][$syslang].'" class="'.$btnstyle.'"></a>';
		// week
		$btnstyle = ($view == 'week') ? 'btnon' : 'btn';
		echo '<a href="'.$aENV['PHP_SELF'].'?view=week'.preg_replace($view_pregsearch, '', $sGEToptions).'" title="'.$aMSG['cal']['viewweek'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_week.gif" alt="'.$aMSG['cal']['viewweek'][$syslang].'" class="'.$btnstyle.'"></a>';
		// month
		$btnstyle = ($view == 'month') ? 'btnon' : 'btn';
		echo '<a href="'.$aENV['PHP_SELF'].'?view=month'.preg_replace($view_pregsearch, '', $sGEToptions).'" title="'.$aMSG['cal']['viewmonth'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_month.gif" alt="'.$aMSG['cal']['viewmonth'][$syslang].'" class="'.$btnstyle.'"></a>';
		?>
		</span></td><td><span class="text">&nbsp;
		<?php 
		// new
		echo get_button("NEW", $syslang, "smallbut"); // params: $sType,$sLang[,$sClass="but"]
		?>
		</span></td>
		</tr></table>
		</td>
	</tr>
</table>

<?php echo HR; ?>

<?php // SUBNAVI
	include_once("inc.cal_detnavi.php"); // stellt $SUBNAVI zur Verfuegung!
	echo getCalSubnavi($view,$date_ts,$label);
?>

<style>
.calTableBody td	{text-align:left;}
.calWeekday			{height:70px;}
.calSaturday		{height:70px;}
.calSunday			{height:70px;}
.calNotthismonth	{height:70px;}
</style>
<?php // ANSICHT day ///////////////////////////////////////////////////////////////////////////////////////////////
if ($view == 'day') {
	$addDayVars .= '#'.$aCalConfig['daytime_start']; // @see setup.php ?>

<iframe frameborder="0" src="./inc.cal_day.php<?php echo $sGEToptions.$addDayVars ?>" width="100%" height="400" name="DAY_in_a_iframe" class="califrame">
	<p>Ihr Browser kann leider keine eingebetteten Frames anzeigen:
	Sie k&ouml;nnen die eingebettete Seite &uuml;ber den folgenden Verweis
	aufrufen: <a href="./inc.cal_day.php<?php echo $sGEToptions.$addDayVars ?>">DAY</a></p>
</iframe><br>

<?php // ANSICHT week ///////////////////////////////////////////////////////////////////////////////////////////////
} elseif ($view == 'week') { 
		$aLabels = Tools::getLabels($oDb,$aENV,'cal','business');
?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="calTableBody">
	<tr valign="top">
<?php // TH: daynames
	$ts_tmp = $ts_kw_monday; // in temp-var umschaufeln
	for ($i=0; $i<7; $i++) {
		$href_new	= preg_replace($date_ts_pregsearch, '', $sNewPage).'&date_ts='.$ts_tmp;
		$href_day	= $aENV['PHP_SELF'].preg_replace($date_ts_pregsearch, '', $sGEToptions).'&date_ts='.$ts_tmp;
		$href_day	= str_replace('view=week', 'view=day', $href_day);
		$date_day	= $oDate->get_dayname($ts_tmp, false).' '.$oDate->get_php_date("d.m.", $ts_tmp);
?>
		<th class="calDaynames">
			<?php if ($bWithNewButton) { ?>
			<a href="<?php echo $href_new; ?>" class="cnavi" title="<?php echo $aMSG['mode']['new'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_add_small.gif" alt="<?php echo $aMSG['std']['new'][$syslang]; ?>" class="btnsmall"></a>
			<?php } ?>
			<a href="<?php echo $href_day; ?>" title="<?php echo $date_day; ?>"><?php echo $date_day; ?></a>
		</th>
<?php	$ts_tmp += 86400;
	} // END for ?>
	</tr>
	<tr valign="top">
<?php // TD: daily events
	$ts_tmp = $ts_kw_monday; // in temp-var umschaufeln
	for ($i=0; $i<7; $i++) {
		$td_class	= ($i > 4) ? 'calSunday' : 'calWeekday';
		$iso_tmp	= date("Y-m-d", $ts_tmp);
		$iso_today	= date("Y-m-d", $today_ts); ?>
		<td height="300" class="<?php echo $td_class; ?>"<?php if ($iso_tmp == $iso_today) { echo ' id="calToday"'; } ?>>
<?php	// get events
		$aEvents = $oCal->getDayEvents($oDb, $iso_tmp); // params: &$oDb[,$day=NULL][,$media='']
		// printout events
		foreach ($aEvents as $k => $event) {
			// box class
			$class = 'eventBoxWeek'.$event['label'];
			// subject mit link zur detailseite? (-> nur wenn edit-rechte und id vorhanden!)
			if (isset($event['id']) && !empty($event['id'])) {
				$href	= preg_replace($date_ts_pregsearch, '', $sEditPage).'&date_ts='.$ts_tmp;
				// FlyBy-Text
				$event['subject'] = '<a href="'.$href.'&id='.$event['id'].'&label='.$label.'" title="'.$aMSG['std']['edit'][$syslang].'">'.$event['subject'].'</a>';
			}
?>		
	<?php if ($event['flag_allday']==1) { $classDay="allday"; } else { $classDay="notallday"; }?>
	<div class="<?php echo $classDay; ?>">
		<? if($event['label'] != 'business' || empty($event['labelid'])) { ?>
		<div class="<?php echo $class; ?>">
		<? } else { 
			if($aLabels[$event['labelid']]['color'] != '') { 
				$bgcolor = "background-color: ".$aLabels[$event['labelid']]['color'].';';
			}
			else { 
				$bgcolor ='';
			}
		?>
		<div style="<?=$bgcolor?> border-left:1px solid #f7f7f7; border-bottom:1px solid #ffffff; padding:2px;">
		<? } ?>
		<span class="text">
			<?php echo $event['icon']; ?>
			<? if($aENV['flag_confirmed'] && $event['flag_confirmed'] == 0) {
				//TODO icon?
				 echo '[tbc]'; 
			} ?>
			<?php echo $event['subject']; ?><?php echo $event['sign_multidays']; ?><br>
			<?php echo $event['show_time']; ?>
			<?php echo '<small>'.$event['show_total_time'].'</small>'; ?>
			<?php if (!empty($event['location'])) echo $event['location'].'<br>'; ?>
			<?php if (!empty($event['participant'])) echo $aMSG['form']['participant'][$syslang].': '.$event['participant'].'<br>'; ?>
			<?php if (!empty($event['comment'])) echo '<small>'.nl2br($event['comment']).'<br></small>'; ?>
		</span></div>
	</div>
	
<?php	} // END foreach ?>
		&nbsp;</td>
<?php	$ts_tmp += 86400;
	} // END for ?>
	</tr>
</table>

<?php // ANSICHT month ///////////////////////////////////////////////////////////////////////////////////////////////
} elseif ($view == 'month') { ?>

<?php
	echo $oCal->getMonthcalendarHtml($oDb, $aENV['cal']['cwweeks'], $sEditPage, '', '', $bWithNewButton); // params: $oDb[,$bWithCw=false][,$sEditPage=''][,$sDayViewPage=''][,$sWeekViewPage=''][,$bWithNewButton=true]
?>
	
<?php // END ANSICHT ///////////////////////////////////////////////////////////////////////////////////////////////
} ?>

<br></form>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
