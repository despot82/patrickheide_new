<?php
/**
* inc.cal_calendar_detail.php
*
* Detailpage: Calendar 
* -> 2sprachig und voll kopierbar! (-> alle texte sind ausgelagert)
*
* @param	int		$id			welcher Datensatz
* @param	string	$view		[zum richtigen zurueckspringen] (optional)
* @param	string	$filter		[zum richtigen zurueckspringen] (optional)
* @param	string	$office_id	[zur Unterscheidung der Ansicht] (optional)
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	int		$date_ts	[timestamp zum anzeigen von bestimmten tagen/wochen/.. (bei NEW)] (optional)
* @param	string	$time_start	[zum anzeigen einer bestimmten start-zeit (bei NEW)] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$syslang	-> kommt aus der 'inc.login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author 	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	1.1 / 2005-12-14
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cal_calendar_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
	require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
	require_once($aENV['path']['global_service']['unix']."class.team.php");

// date
	$today_ts		= time();
	$date_ts		= (isset($_GET['date_ts'])) ? $oFc->make_secure_int($_GET['date_ts']) : $today_ts; // default: heute
	// init timestamp in date-object
	$oDate->set_timestamp($date_ts);
	$date_iso		= date("Y-m-d", $date_ts);

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$view			= (isset($_GET['view'])) ? $oFc->make_secure_string($_GET['view']) : '';
	$filter			= (isset($_GET['filter'])) ? $oFc->make_secure_string($_GET['filter']) : '';
	$office_id		= (isset($_GET['office_id'])) ? $oFc->make_secure_int($_GET['office_id']) : '';
	$label			= (isset($_GET['label'])) ? $oFc->make_secure_string($_GET['label']) : '';
	$time_start		= (isset($_GET['time_start'])) ? $oFc->make_secure_string($_GET['time_start']) : '';
	$delete_id		= (isset($_GET['delete_id'])) ? $oFc->make_secure_int($_GET['delete_id']) : '';
	if (isset($_GET['remoteSave']) && $_GET['remoteSave'] == 1)	{ $remoteSave = 1; } else { $remoteSave = 0; }
	$sGEToptions	= '?view='.$view.'&date_ts='.$date_ts; // pflichtparameter
	if (!empty($filter))	{ $sGEToptions .= '&filter='.$filter; } // filter-vars nur bei bedarf!
	if (!empty($office_id))	{ $sGEToptions .= '&office_id='.$office_id; }
	if (!empty($label))		{ $sGEToptions .= '&label='.$label; }
	
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['remoteSave']) && $_POST['remoteSave'] == 1)	{ $remoteSave = 1; }

	
// 2c. Vars
	$sTable			= $aENV['table']['cal_event'];
	$sViewerPage	= $aENV['SELF_PATH']."cal_calendar.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;						// fuer NEW-button
	$flagopen		= (isset($aData['flag_open'])) ? $aData['flag_open'] : 0;
	$oTeam			= new team($oDb);

// TEAM ACCESS CONTROLL
	$oTACEA =& new TeamAccessControllEA($oDb);
	$oTACEA->setTable($aENV['table']['sys_team_access']);
	$oTACEA->setAENV($aENV);
	$oTACEA->initialize();

	if(!isset($aData['flag_open']) || $aData['flag_open'] == 2) { 
		if($oTACEA->isInAuthTable($id)) { 
			$aData['flag_open'] = 0; 
			$flopen = 0; 
		} else { 
			$aData['flag_open'] = 1; 
		}
	}

// 3. DB
// DB-action: delete (& remote)
	if ((isset($btn['delete']) && !empty($aData['id'])) || $delete_id > 0) {
		if ($delete_id > 0) { $aData['id'] = $delete_id; }
		$oTACEA->onDeleteAction($aData['id'],'cal');

		$oDb->make_delete($aData['id'], $sTable);			
		header("Location: ".$sViewerPage); exit;
	}
	$flopen	= $aData['flag_open'];
	unset($aData['flag_open']);
	
	$viewug	= $aData['viewug'];
	unset($aData['viewug']);

// DB-action: save or update
	if ((isset($btn['save']) || isset($btn['save_close']) || $remoteSave == 1) && !isset($alert) && isset($aData['subject'])) { //subject=mindestbedingung!
	
		$group	= $aData['group'];
		$user	= $aData['user'];

		if(empty($user) && empty($group) && !is_array($aData['office'])) {
			$aData['flag_private_event'] = 0;
			$aData['office'] = $Userdata['office_id'];
		}
		unset($aData['group']);
		unset($aData['user']);
		if($aData['label'] != 'business') $aData['label_id'] = 0;
		// checkboxen (kommen als array) zu string implodieren
		$aData['office']		= (is_array($aData['office'])) ? implode(',', $aData['office']) : $aData['office'];
		// checkbox defaults

		if (!isset($aData['office']) || empty($aData['office'])) {
				$aData['flag_private_event'] = '1';
		}
		if (!isset($aData['flag_private_event'])) { $aData['flag_private_event'] = '0'; }
		// fallbacks
		if (!isset($aData['date_start']) || empty($aData['date_start'])) $aData['date_start'] = date("Y-m-d");
		if (!isset($aData['time_start']) || empty($aData['time_start'])) $aData['time_start'] = date("H:i");
		if (!isset($aData['date_end']) || empty($aData['date_end'])) $aData['date_end'] = date("Y-m-d");
		if (!isset($aData['time_end']) || empty($aData['time_end'])) $aData['time_end'] = date("H:i");
		// datetime start zusammenbauen
		list($d, $m, $y) = explode('.', $aData['date_start']);
		$d = preg_replace('/\D/', '', $d); // ggf. Wochentagnamen entfernen!
		$aData['event_start']	= $y.'-'.$m.'-'.($d + 0).' '.$aData['time_start'].':00';
		unset($aData['date_start']); unset($aData['time_start']); // nicht vorhandene felder loeschen
		// datetime end zusammenbauen
		list($d, $m, $y) = explode('.', $aData['date_end']);
		$d = preg_replace('/\D/', '', $d); // ggf. Wochentagnamen entfernen!
		$aData['event_end']		= $y.'-'.$m.'-'.$d.' '.str_replace('24:00', '23:59', $aData['time_end']).':00'; // datetime geht nur bis 23:59!
		unset($aData['date_end']); unset($aData['time_end']); // nicht vorhandene felder loeschen

		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aData['last_mod_by'] = $Userdata['id'];
			
				$oDb->make_update($aData, $sTable, $aData['id']);
		} else { // Insert eines neuen Eintrags
			$aData['created'] = $oDate->get_mysql_timestamp();
			$aData['created_by'] = $Userdata['id'];

			// make insert
				$oDb->make_insert($aData, $sTable);
				$aData['id'] = $oDb->insert_id();
		}
		
		if($flagopen == 1) {
			$oTACEA->delRightForTree($treeid);
		}
		else {
			$oTACEA->addRights($group, $user,$aData['id'],'cal');
		}
		// back to overviewpage
		if (isset($btn['save_close'])) {
			header("Location: ".$sViewerPage); exit;
		}
	}

// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	if (isset($id) && !empty($id)) {
		$aData = $oDb->fetch_by_id($id, $sTable);
		$mode_key = "edit"; // do not change!
		// datetime start trennen
		list($aData['date_start'], $aData['time_start']) = explode(' ', $aData['event_start']);
		$aData['date_start'] = $oDate->date_mysql2trad($aData['date_start'], true);
		$aData['time_start']	= substr($aData['time_start'], 0, 5); // time start ohne sekunden
		// datetime end trennen
		list($aData['date_end'], $aData['time_end']) = explode(' ', $aData['event_end']);
		/*list($y, $m, $d) = explode('-', $aData['date_end']);*/
		$aData['date_end']	= $oDate->date_mysql2trad($aData['date_end'], true);
		$aData['time_end']	= substr($aData['time_end'], 0, 5); // time end ohne sekunden
	} else {
		$mode_key = "new"; // do not change!
		// set defaults
		$aData['date_start'] = $oDate->get_today(true, null, true);
		$aData['time_start'] = date("H").':00';// time start erzeugen
		$aData['date_end'] = $oDate->get_today(true, null, true);
		$aData['time_end'] = (date("H")+1).':00';// time end erzeugen
		if (!empty($time_start)) { // wenn eine startzeit per GET uebergeben wurde
			$aData['time_start'] = $time_start;
			$time_end = substr($time_start, 0, 1).(substr($time_start, 1, 1) + 1).substr($time_start, 2);
			$aData['time_end'] = $time_end;
		}
		$aData['office'] = (isset($_GET['office_id'])) ? $_GET['office_id'] : $Userdata['office_id'];
	}
	$aData['viewug'] = $viewug;

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->check_field("subject", $aMSG['form']['subject'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	
	$aTAUR = $oTACEA->getRightForID($aData['id'],'cal');
	if(!is_array($aTAUR)) $aTAUR = array();
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
		$oForm->bEditMode = false;
		$oTACEA->setBeditmode(false);
	}
?>
<style type="text/css">
#tStart	{display:inline; visibility:visible;}
#tEnd	{display:inline; visibility:visible;}
</style>
<?php if ($oForm->bEditMode == true) { ?>
<script language="JavaScript" type="text/javascript" src="scripts/calendar.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
// vars
var newEntry = <?php echo (isset($id) && $id > 0) ? 'false' : 'true'; ?>;
if (newEntry == false) { 	// bei Edit-Page muessen zuvor gespeicherte End-Zeit zwischengespeichert werden fuer Funktion writeTimeOptions()
	var timeStartSaved = '<?php echo $aData['time_start']; ?>';
	aParts = timeStartSaved.split(":");
	aParts[1] = (aParts[1] == "30") ? ".5" : ".0";
	var timeStartSavedFloat = parseFloat(aParts[0]) + parseFloat(aParts[1]);
	
	var timeEndSaved = '<?php echo $aData['time_end']; ?>';
	aParts = timeEndSaved.split(":");
	aParts[1] = (aParts[1] == "30") ? ".5" : ".0";
	var timeEndSavedFloat = parseFloat(aParts[0]) + parseFloat(aParts[1]);
	
	var timeSpan = timeEndSavedFloat - timeStartSavedFloat;
}
else { var timeSpan = 1; }
var startSelected;

var trenner = '.';
var diffTime = 2; // 2 indices ergeben eine Stunde (bei halbstuendiger Abstufung des Select-Feldes)

// funcs
function checkDate(field) { // hilfsfunktion: datumsformat ueberpruefen und ggf. ausbessern
	if (field.value == '') return;
	field.value = field.value.replace(/(-|\/|,)/, '.'); // dash + slash + comma gegen dot ersetzen
	var aDatePart = field.value.split(trenner);
	var anzDateParts = aDatePart.length;
	// ggf. wochentagsnamen am anfang rausnehmen
	while (aDatePart.length > 3) {
		aDatePart.shift();
	}
	if (anzDateParts < 3) {
		alert('<?php echo $aMSG['err']['invalid_date_format'][$syslang]; ?>');
		var d = new Date(); // ... stattdessen aktuelles Datum einfuegen
		aDatePart[0] = d.getDate(); aDatePart[1] = (d.getMonth() + 1); aDatePart[2] = d.getYear();
		if (aDatePart[0] < 10) { aDatePart[0] = '0' + aDatePart[0]; } // fuehrende null...
		if (aDatePart[1] < 10) { aDatePart[1] = '0' + aDatePart[1]; } // fuehrende null...
	}
	aDatePart[0] = aDatePart[0].replace(/(\D)*/, ''); // alles ausser zahlen entfernen
	if (aDatePart[0] > 31) { // bei tag ueber 31 -> alert!
		alert('<?php echo $aMSG['err']['invalid_day'][$syslang]; ?>');
		aDatePart[0] = 31;
	}
	aDatePart[1] = aDatePart[1].replace(/(\D)*/, ''); // alles ausser zahlen entfernen
	if (aDatePart[1] > 12) { // bei monat ueber 12 -> alert!
		alert('<?php echo $aMSG['err']['invalid_month'][$syslang]; ?>');
		aDatePart[1] = 12;
	}
	if (aDatePart[0] < 10 && aDatePart[0].length == 1) { // fuehrende null bei einstelligen zahlen hinzufuegen
		aDatePart[0] = '0' + aDatePart[0];
	}
	if (aDatePart[1] < 10 && aDatePart[1].length == 1) { // fuehrende null bei einstelligen zahlen hinzufuegen
		aDatePart[1] = '0' + aDatePart[1];
	}
	aDatePart[2] = aDatePart[2].replace(/(\D)*/, ''); // alles ausser zahlen entfernen
	if (aDatePart[2] < 100) { // bei nur zweistelligem jahr 20 voranstellen
		aDatePart[2] = '20' + aDatePart[2];
	}
	// Wochentagname
	d2 = new Date(aDatePart[2], (aDatePart[1] - 1), aDatePart[0]);
	weekday = d2.getDay()
	aWdName = new Array("<?php echo implode ('","', $oDate->aDaynameShort['de']); ?>");
	field.value = aWdName[weekday] + ', ' + aDatePart.join(trenner);
	//return field;
}

function checkEndDate(date_end) { // enddatum ueberpruefen
	date_end = checkDate(date_end);
	with (document.forms['editForm']) {
		if (makeIntDate(date_end.value) < makeIntDate(date_start.value)) {
			alert("<?php echo $aMSG['err']['invalid_enddate'][$syslang]; ?>");
			updateEndDate(date_start);
		}
		else if (makeIntDate(date_end.value) > makeIntDate(date_start.value)) { 
			writeTimeOptions(time_end, "00:00");
		} else {
			writeTimeOptions(time_end, time_start.value);
		}
	}
}


function _checkEndTime(time_end) { // Hilfsfunktion: enduhrzeit ueberpruefen
	with (document.forms['editForm']) {
		if (time_end.selectedIndex <= time_start.selectedIndex) {
			alert("<?php echo $aMSG['err']['invalid_endtime'][$syslang]; ?>");
			updateEndTime(time_start);
		}
	}
}

// -->
</script>
  
<?php } // END if ($oForm->bEditMode == true) ?>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions,'post','editForm'); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
?><input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">
<input type="hidden" name="btn[remoteSave]" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><span class="title"><?php echo $aMSG['cal']['schedule'][$syslang];#$oDate->get_dayname().", ".$oDate->get_today_long(); ?></span> 
			<span class="text"><?php echo $aMSG['mode'][$mode_key][$syslang]; ?><br></span>
		</td>
		<td align="right"><?php // Buttons 
			echo $oForm->button("BACK"); // params: $sType[,$sClass='']
			echo $oForm->button("NEW"); // params: $sType[,$sClass='']
		?>
		</td>
	</tr>
</table>
<br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td width="20%"><span class="text"><b><?php echo $aMSG['form']['subject'][$syslang]; ?> *</b></span></td>
		<td width="80%">
		<?php echo $oForm->textfield("subject", 150, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['location'][$syslang]; ?></span></td>
		<td class="sub2">
		<?php echo $oForm->textfield("location", 150, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		<?
			if($aENV['mapLink']) {
				if(!$oForm->bEditMode) {
					$mapurl = $aData['location'];
				}
				else {
					$mapurl = 'document.forms[\'editForm\'].location.value';
				}
				echo '<button type="button" class="smallbut" onClick="window.open(\''.$aENV['mapLinkURL'].'\'+'.$mapurl.',\''.$aMSG['form']['MapLink'][$syslang].'\',\'width=300,height=400,left=100,top=200\')">'.$aMSG['form']['MapLink'][$syslang].'</button>';
			}
		?>
		</td>
	</tr>
		<tr valign="top">
		<td class="sub2" height="26"><span class="text">Label</span></td>
		<td class="sub2">
		<script language="JavaScript" type="text/javascript">
		function swapLabelIdDiv(selVal) {
			if (selVal == "business") { document.getElementById("labelIdDiv").style.display = "block"; }
			else { document.getElementById("labelIdDiv").style.display = "none"; }
		}
		</script>
		<div style="float:left;margin-right:5px">
		<?php // label
		if ($oForm->bEditMode == true) {
			echo '<select name="aData[label]" id="label" size="1" onChange="swapLabelIdDiv(this.options[selectedIndex].value)">';
			if(!$oPerm->hasPriv('admin')) { 
				unset($aScheduleLabel[$syslang]['leave']);
				unset($aScheduleLabel[$syslang]['sick']);
				unset($aScheduleLabel[$syslang]['holiday']);
				unset($aScheduleLabel[$syslang]['lieu']);
			}
			foreach ($aScheduleLabel[$syslang] as $key => $val) {
				$sel = (isset($aData['label']) && $aData['label'] == $key) ? ' selected' : '';
				echo '<option value="'.$key.'" class="eventBoxDay'.$key.'"'.$sel.'>'.$val.'</option>'."\n";
			}
			echo '</select>';
		} else {
			echo '<span class="text">'.$aScheduleLabel[$syslang][$aData['label']].'</span>';
		} ?>
		</div>
		<!-- nur wenn business: -->
		<?php $sVisibility = ($aData['label'] == "business" || !isset($aData['label'])) ? "block" : "none"; ?>
		
		<div id="labelIdDiv" style="display:<?php echo $sVisibility; ?>;">
		<table border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td> <?php echo $oForm->labelDropdown('label_id','business','cal',$oDb);?></td>
		</tr>
		</table>
		
		</div>
		</td>
	</tr>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

	<tr width="40%" valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['date_start'][$syslang]; ?> *</b></span></td>
		<td valign="bottom"><?php 
			echo $oForm->date_popup("date_start", 10, 12, '', 'onblur="updateEndDate(this)"', '', true); // params: $sFieldname[,$nMaxlength=10][,$nSize=12][,$sDefault=''][,$sExtra=''][,$sImg=''] ?>
			<div id="tStart">&nbsp;<?php $aTime = _getTimeOptions();
			echo $oForm->select("time_start", $aTime, 'onchange="updateEndTime(this)"'); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false] 
			?><span class="text"> + </span>
			<?php $aTimeHours = _getTimeHours();
			echo $oForm->select("event_arrival", $aTimeHours);
			?><span class="text"><?php echo $aMSG['form']['arrival'][$syslang].''; ?></span>
			</div>
			&nbsp;&nbsp;
			<?php echo $oForm->checkbox("flag_allday", 1, 0, 'onClick="switchAllday()"'); // params: $sFieldname[,$sDefaultCheckboxValue='1'][,$sDefaultHiddenValue='0'][,$sExtra=''] ?><label for="flag_allday" class="text"><?php echo $aMSG['form']['flag_allday'][$syslang]; ?></label>
			<?php
			if ($aENV['flag_confirmed'] == true) {
				$sel = '';
				if (!isset($id) || empty($id)) {
					$sel = ($aENV['event_confirmed_default'] == true) ? ' checked' : '';
				}
				echo $oForm->checkbox("flag_confirmed", 1, 0, $sel);
					
			?>
				<label for="flag_confirmed" class="text"><?php echo $aMSG['form']['flag_confirmed'][$syslang]; ?></label>
			<?	} ?>
		</td>
	</tr>
	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['date_end'][$syslang]; ?> *</b></span></td>
		<td valign="bottom"><?php 
			echo $oForm->date_popup("date_end", 10, 12, $sDefault, 'onblur="checkEndDate(this)"', '', true); // params: $sFieldname[,$nMaxlength=10][,$nSize=12][,$sDefault=''][,$sExtra=''][,$sImg=''] ?>
			<div id="tEnd">&nbsp;<?php 
			if($oForm->bEditMode == true) {
				$aStart = explode(' ',$aData['event_start']);
				$aEnd = explode(' ',$aData['event_end']);
				if($aStart[0] == $aEnd[0]) {
					$sTime = $aData['time_start'];
				}
			}
			$aTime = _getTimeOptions($sTime);
			echo $oForm->select("time_end", $aTime, 'onchange="floatEndTime(this.value);"'); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false] 
			?><span class="text"> + </span><?php #, 'onchange="checkEndTime(this)"'
			echo $oForm->select("event_return", $aTimeHours);
			?><span class="text"><?php echo $aMSG['form']['return'][$syslang].''; ?></span>
			</div>
		</td>
	</tr>
	
	<tr valign="top">
		<td><span class="text"><?php echo $aMSG['form']['participant'][$syslang]; ?></span></td>
		<td>
<?php // USER + OFFICE PHP ........................................................................
	$oUser =& new user($oDb); // params: &$oDb[,$aConfig=NULL)
	$aUser = $oUser->getAllUserData(); // params: [$bValidOnly=true]
	$aOffice = $oUser->getAllOfficeNames(); // params: [$bWithUsers=false]
	$param = array('first' => '60','second' => '40','border'=>'0','cellspacing'=>'0','cellpadding'=>'0','class' =>"tabelle");
	echo $oTACEA->showTeamAccess($aData,$syslang,$aData['id'],$flagopen,$aMSG,$param,'cal',false,true,true);
?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

<?php if (isset($aENV['module']['adm'])) { ?>
	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['project'][$syslang]; ?></span></td>
		<td class="sub2"><?php 
		// ADM/PRJ ....................................................................
		require_once($aENV['path']['global_module']['unix'].'class.AdmProjectEA.php');
		$oAdmProjectEA =& new AdmProjectEA($oDb, $aENV);
		if ($oForm->bEditMode == true) {
			// Aktuelle Projekte als verschachteltes Client/Projects DHTML-DropDown
			echo $oAdmProjectEA->getClientprojectsDropdown($aData['project_id']);
		} else {
			$tmp = $oAdmProjectEA->getClientproject($aData['project_id']);
			if ($tmp['projectname']) {
				echo '<span class="text">'.$tmp['projectname'].' [ '.$tmp['client_nr'].'.'.$tmp['prj_nr'].' ]</span>';
			}
		} ?>
		</td>
	</tr>
<?php } // END if ?>

	<tr valign="top">
		<td class="sub2"><span class="text"><?php echo $aMSG['form']['comment'][$syslang]; ?></span></td>
		<td class="sub2">
		<?php echo $oForm->textarea("comment", 3, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

	<tr valign="top">
		<td><span class="text"><b><?php echo $aMSG['form']['visibility'][$syslang];#office ?> *</b></span></td>
		<td>
		<?php echo _getPrivateCheckbox($aMSG['form']['flag_private_event'][$syslang], $aData['flag_private_event']); ?>
		<div class="hDivider"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></div>
		<?php echo _getOfficeCheckboxes($aOffice, $aData['office']); ?>
		</td>
	</tr>
<?php // END USER + OFFICE HTML ........................................................................ ?>
</table><br>

<?php if ($oForm->bEditMode == true) {
// USER + OFFICE JS ........................................................................ ?>
<script language="JavaScript" type="text/javascript"> // --------------------------- USER + OFFICE JS
// globale Variablen
var office = <?php echo (count($aOffice) > 0) ? 'true' : 'false'; ?>;
var oCbxPrivate = document.forms['editForm'].elements['aData[flag_private_event]'];

if (office == true) { var oCbxOffice = document.forms['editForm'].elements['aData[office][]']; }
else { var oCbxOffice = null; }

// Funktionen
// Hilfsfunktion: falls nur EINE Office-Checkbox existiert, wird der String zu einem Array konvertiert
function checkCbxOfficeArray() { // mehrere checkboxen (=array), 1 hiddenfield (=string)
	if (oCbxOffice != "undefined" && oCbxOffice.length == "undefined") {
		oCbxOffice = new Array(oCbxOffice); // wenn string -> konvertiere zu array
	}
} // zuerst ueberpruefen, ob es ein oder mehrere Offices gibt...
if (office == true) { checkCbxOfficeArray(); }	// ...damit die nachfolgenden Funktionen immer mit einem Array hantieren koennen!	

// Hilfsfunktion: checkt ob mindestens eine Office-Checkbox aktiviert ist oder nicht
function isNoOfficeChecked() {
	noOfficeIsChecked = true;
	for (var i=0; i<oCbxOffice.length; i++) {
		if (oCbxOffice[i].checked == true) { noOfficeIsChecked = false; }
	}
	return noOfficeIsChecked;
}

// Hilfsfunktion: aktiviert die Office-Checkbox des aktuellen users
function activateDefaultOfficeCheckbox() {
	for (var i=0; i<oCbxOffice.length; i++) {
		if (oCbxOffice[i].value == <?php echo $Userdata['office_id']; ?>) {
			oCbxOffice[i].checked = true;
			break;
		}
	}
}
	
function checkFlagPrivate() {	// wird von Team / User aufgerufen (function formatHidden)
	if (aAllSelectedUsers.length > 0) {	// es gibt gecheckte User
		oCbxPrivate.checked = true;
	}
	else {
		oCbxPrivate.checked = false;		
		// gegebenenfalls Default-Office anwaehlen
		if (isNoOfficeChecked() == true) { activateDefaultOfficeCheckbox(); }
	}
}
// init
checkFlagPrivate();	// bei Init werden die Funktionen in Team / User schon abgearbeitet, bevor Seite vollstaendig geladen => beim Laden der Seite hier manuell aufrufen

function checkAllSelections() {	// wird von Office-Checkboxen aufgerufen
	if (oCbxPrivate.checked == false && isNoOfficeChecked() == true) { activateDefaultOfficeCheckbox(); }
}
	
// ------------------------------------------------------------------------------------ NOTIFY
// assioziatives array (key = user_id, value = email-adresse)
var aUserEmail = new Object();
<?php // JS-Array mit User->Office-Relation schreiben
foreach ($aUser as $key => $val) {
	echo "aUserEmail['".$key."'] =  '".$val['email']."';\n";
}
?>

// benachrichtige alle ausgewaehlten Teilnehmer
function notifyUsers() {
	// if (aAllSelectedUsers.length == 0) return;
	
	var mailtostring = "mailto:";
	for (var u = 0; u < aAllSelectedUsers.length; u++) {
		uid = aAllSelectedUsers[u];
		mailtostring += aUserEmail[uid] + "<?php echo ($oBrowser->isMac()) ? '%2c' : ','; // Trenner: Mac-Mail braucht Komma, Outlook(PC) ein Semikolon! ?>";
	}
	mailtostring += "?subject=<?php echo urlencode($aMSG['cal']['schedule'][$syslang]); // Subject vorbelegen ?>";
	with (document.forms["editForm"]) {
		if (elements["aData[subject]"].value != "") {
			mailtostring += ": " + elements["aData[subject]"].value;
		}
		mailbody = "";		
		if (elements["aData[location]"].value != "") {
			mailbody += "<?php echo $aMSG['form']['location'][$syslang]; ?>: ";
			mailbody += elements["aData[location]"].value + "%0D%0A";
			//mailbody += elements["aData[location]"].value + "\n";
		}
		if (elements["aData[date_start]"].value != "") {
			mailbody += "<?php echo $aMSG['form']['date_start'][$syslang]; ?>: ";
			mailbody += elements["aData[date_start]"].value;
		}
		if (elements["aData[time_start]"].value != "") {
			mailbody += ", " + elements["aData[time_start]"].value + "%0D%0A";
			//mailbody += ", " + elements["aData[time_start]"].value + "\n";
		}
		if (elements["aData[date_end]"].value != "") {
			mailbody += "<?php echo $aMSG['form']['date_end'][$syslang]; ?>: ";
			mailbody += elements["aData[date_end]"].value;
		}
		if (elements["aData[time_end]"].value != "") {
			mailbody += ", " + elements["aData[time_end]"].value + "%0D%0A";
			//mailbody += ", " + elements["aData[time_end]"].value + "\n";
		}
		<? if (isset($aENV['module']['adm'])) { ?>		
		if (elements["clients"].selectedIndex != 0) {
			mailbody += "<?php echo $aMSG['form']['project'][$syslang]; ?>: ";
			mailbody += elements['clients'].options[elements['clients'].selectedIndex].text + " - "; 
			mailbody += elements["aData[project_id]"].options[elements["aData[project_id]"].selectedIndex].text + "%0D%0A";
			//mailbody += elements["aData[project_id]"].options[elements["aData[project_id]"].selectedIndex].text + "\n";
		}
		<? } ?>
		if (elements["aData[comment]"].value != "") {
			mailbody += "<?php echo $aMSG['form']['comment'][$syslang]; ?>: ";
			mailbody += "%0D%0A" + str_replace(elements["aData[comment]"].value, "\n", "%0D%0A") + "%0D%0A";
			//mailbody += "\n" + elements["aData[comment]"].value + "\n";
		}
		mailbody = str_replace(mailbody, "ä", "ae");
		mailbody = str_replace(mailbody, "Ä", "Ae");
		mailbody = str_replace(mailbody, "ö", "oe");
		mailbody = str_replace(mailbody, "Ö", "Oe");
		mailbody = str_replace(mailbody, "ü", "ue");
		mailbody = str_replace(mailbody, "Ü", "Ue");
		mailbody = str_replace(mailbody, "ß", "ss");
		mailbody = str_replace(mailbody, "€", "EUR");
		mailbody = str_replace(mailbody, "œ", "oe");
		mailbody = str_replace(mailbody, "Œ", "Oe");
		mailbody = str_replace(mailbody, "æ", "ae");
		mailbody = str_replace(mailbody, "Æ", "Ae");
		//mailtostring = mailtostring + "&body=" + escape(mailbody.replace("&", "%26"));
		mailtostring = mailtostring + "&body=" + mailbody.replace("&", "%26");
	}
	<?php if ($mode_key == 'edit') { // nur bei "edit" (wegen ID!): Body mit Link zu dieser Seite vorbelegen
			echo "mailtostring += '%0D%0A".urlencode($aENV['PHP_SELF'].'?id='.$aData['id'])."';";
		} ?>
	//alert(mailtostring);
	location.href = mailtostring;
	return; // wichtig!
}
</script>
<?php } // END USER + OFFICE JS ........................................................................ ?>

<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php if ($mode_key == "edit") { echo $oForm->button("DELETE"); } ?>
<?php if ($mode_key == "edit") { echo $oForm->button("CANCEL"); } ?>
<?php echo $oForm->end_tag(); ?>
<br>

<?php if ($oForm->bEditMode == true) { ?>
<script type="text/javascript" language="JavaScript">
switchAllday();
</script>
<?php } // END if ($oForm->bEditMode == true) ?>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");

// HILFSFUNKTIONEN /////////////////////////////////////////////////////////////

	function &_getTimeOptions($from=null) {
		$buffer = array();
		$from = (!is_null($from) && !empty($from)) ? substr($from, 0, 2) : 0;
		for ($i=$from; $i < 24; $i++) {
			if (strlen($i) == 1) $i = '0'.$i;
			$buffer[$i.':00'] = $i.':00';
			$buffer[$i.':30'] = $i.':30';
		}
		if (!is_null($from) && !empty($from)) {
			$buffer['24:00'] = '24:00';
		}
		return $buffer;
	}
	
	function &_getTimeHours($from=null) {
		$hrs = array();
		$from = (!is_null($from) && !empty($from)) ? substr($from, 0, 2) : 0;
		for ($i=$from; $i < 9; $i++) {
			$hrs[$i.'.0'] = $i.'.0';
			$hrs[$i.'.5'] = $i.'.5';
		}
		return $hrs;
	}

	function &_getValidUserCheckboxes(&$aUser, $sCheckedValue) {
		global $oForm;
		$aCheckedValue = explode(',', $sCheckedValue);
		// get all valid usernames
		$num = count($aUser);
		if ($num == 0) return; // fallback
		$i = 0;
		$buffer = '<table border="0" cellpadding="0" cellspacing="0"><tr valign="top"><td>';
		foreach ($aUser as $u_id => $user) {
			// checked?
			$sel = (in_array($u_id, $aCheckedValue)) ? ' checked' : '';
			// output
			if ($oForm->bEditMode) { // wenn edit-rechte
				$buffer .= '<input type="checkbox" name="aData[participant][]" id="cbxU'.$u_id.'" value="'.$u_id.'"'.$sel.' class="radiocheckbox" onClick="checkOffice(this, '.$user['office_id'].')">';
				$buffer .= '<label for="cbxU'.$u_id.'" class="text">'.$user['name']."</label>&nbsp;&nbsp;<br>\n";
			} else {
				if (!empty($sel)) {
					$buffer .= '<span class="text">'.$user['name']."</span>&nbsp;&nbsp;<br>\n";
				}
			}
			// ggf. zweispaltig (bei mehr als 7 Usern)
			$i++;
			if ($num > 7 && $i == ceil($num / 2)) { $buffer .= '</td><td>'; }
		}
		$buffer .= '</nobr></td></tr></table>';
		return $buffer;
	}

	function &_getOfficeCheckboxes(&$aOffice, $sCheckedValue) {
		global $oForm, $Userdata;
		$aCheckedValue = explode(',', $sCheckedValue);
		// get all valid usernames
		$num = count($aOffice);
		if ($num == 0) return; // fallback
		$i = 0;
		$buffer = '<table border="0" cellpadding="0" cellspacing="0"><tr valign="top"><td><nobr>';
		foreach ($aOffice as $o_id => $o_name) {
			// checked?
			$sel = (in_array($o_id, $aCheckedValue)) ? ' checked' : '';
			// output
			if ($oForm->bEditMode) { // wenn edit-rechte
				$buffer .= '<input type="checkbox" name="aData[office][]" id="cbxO'.$o_id.'" value="'.$o_id.'"'.$sel.' class="radiocheckbox" onClick="checkAllSelections()">';
				$buffer .= '<label for="cbxO'.$o_id.'" class="text">'.$o_name."</label>&nbsp;&nbsp;<br>\n";
			} else {
				if (!empty($sel)) {
					$buffer .= '<span class="text">'.$o_name."</span>&nbsp;&nbsp;<br>\n";
				}
			}
			// ggf. zweispaltig (bei mehr als 7 Offices)
			$i++;
			if ($num > 7 && $i == ceil($num / 2)) { $buffer .= '</td><td>'; }
		}
		$buffer .= '</nobr></td></tr></table>';
		return $buffer;
	}

	function &_getPrivateCheckbox($sLabel, $sCheckedValue) {
		global $oForm;
		// output
		if ($oForm->bEditMode) { // wenn edit-rechte
			$sel = ($sCheckedValue == 1) ? ' checked' : '';
			$buffer = '<input type="checkbox" name="aData[flag_private_event]" id="flag_private_event" value="1"'.$sel.' class="radiocheckbox" disabled>';
			$buffer .= '<label for="flag_private_event" class="text">'.$sLabel."</label>&nbsp;&nbsp;<br>\n";
		} else {
			$buffer = ($sCheckedValue == 1) 
				? '<span class="text">'.$sLabel."</span>&nbsp;&nbsp;<br>\n"
				: '';
		}
		return $buffer;
	}

 ?>