<?php
/**
* inc.cal_detnavi.php
* Dieses Include baut die Kalenderansicht-Subnavi, gibt sie jedoch nicht aus, sondern als string zurueck!
*
* subnavi-include (nur CAL) //-> 2sprachig und voll kopierbar!
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	object	$oPerm		-> kommt aus der 'inc.sys_login.php'
* @param	array	$aBnParts	-> kommt aus der 'inc.sys_header.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.1 / 2006-01-03 [bugfixes]
* #history	1.0 / 2005-03-04
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.cal_detailnavi.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// Subnavi
function getCalSubnavi($view,$datets,$sellabel) {
	// VARS
	global $aENV, $oPerm, $aBnParts, $aMSG, $syslang, $oUser, $Userdata;
	$SUBNAVI		= '';
	$aFilterLink	= array();
	$sLinkTemplate	=  '<a href="%s" class="cnavi%s">%s</a>';
	$sTrenner		= '&nbsp;&nbsp;|&nbsp;&nbsp;';
	// get offices
	$aOffice = $oUser->getAllOfficeNames(); // params: [$bWithUsers=false] 

	// FILTER
	$self = $aENV['PHP_SELF'].'?'.preg_replace('/(\?|\&)(filter=)([my|all|office])+/', '', $_SERVER['QUERY_STRING']).'&filter=';
	$self = preg_replace('/(\&)(office_id=)([0-9])*/', '', $self); // zusaetzlich office_id filtern
	// all
	if (count($aOffice) > 1) {
		$hi = (isset($_GET['filter']) && $_GET['filter'] == 'all') ? 'hi' : '';
	} else { // wenn nur ein office, wird dieses nicht ausgegeben, daher hier default angeben wenn noch kein filter uebergeben wurde!
		$hi = (!isset($_GET['filter']) || (isset($_GET['filter']) && $_GET['filter'] == 'all')) ? 'hi' : '';
	}
	$aFilterLink[] = sprintf($sLinkTemplate, $self.'all', $hi, $aMSG['cal']['filter_all'][$syslang]);
	// offices
	if (count($aOffice) > 1) {
		foreach ($aOffice as $o_id => $o_name) {
			$hi = (isset($_GET['filter']) && $_GET['filter'] == 'office' && $_GET['office_id'] == $o_id) ? 'hi' : '';
			if (empty($_GET['filter']) && $Userdata['office_id'] == $o_id) $hi = 'hi';
			$aFilterLink[] = sprintf($sLinkTemplate, $self.'office&office_id='.$o_id, $hi, $o_name); // zusaetzlich office_id anheangen
		}
	}
	// my
	$hi = (isset($_GET['filter']) && $_GET['filter'] == 'my') ? 'hi' : '';
	$aFilterLink[] = sprintf($sLinkTemplate, $self.'my', $hi, $aMSG['cal']['filter_my'][$syslang]);

	// BUILD HTML
	if ($oPerm->hasPriv('view')) {# && count($aSubnaviLink) > 0
		$SUBNAVI .= '<form method="GET" action="'.trim($aENV['PHP_SELF']).'" name="filterForm">';
		// GET-Params als hidden-fields
		$SUBNAVI .= '<input type="hidden" name="view" value="'.$view.'">';
		$SUBNAVI .= '<input type="hidden" name="date_ts" value="'.$datets.'">';
		if (isset($_GET['filter'])) {
			$SUBNAVI .= '<input type="hidden" name="filter" value="'.$_GET['filter'].'">';
		}
		if (isset($_GET['office_id'])) {
			$SUBNAVI .= '<input type="hidden" name="office_id" value="'.$_GET['office_id'].'">';
		}
		$SUBNAVI .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
		$SUBNAVI .= '<td width="100%"><span class="cnavi">';
		$SUBNAVI .= implode($sTrenner, $aFilterLink);
		$SUBNAVI .= '&nbsp;&nbsp;&nbsp;';			
		$SUBNAVI .= _getLabelDropDown($sellabel);
		$SUBNAVI .= '</span></td>';
		$SUBNAVI .= '</tr></table><br>';
		$SUBNAVI .= '</form>';
	}
	
	// OUTPUT
	return $SUBNAVI;
}
// Hilfsfunktion fuer "getCalSubnavi()"
	function _getLabelDropDown($sellabel) {
		// VARS
		global $aENV,$syslang,$aScheduleLabel,$aMSG,$oDb;
		$aLabels = Tools::getLabels($oDb,$aENV,'cal','business');
		// html
		$label .= '<select name="label" id="label" size="1" onChange="document.forms[\'filterForm\'].submit()">';
		$label .= '<option value="">'.$aMSG['cal']['alllabel'][$syslang].'</option>';
		foreach ($aScheduleLabel[$syslang] as $key => $val) {
			$sel = ($sellabel == $key) ? ' selected' : '';
			$label .= '<option value="'.$key.'" '.$sel.'>'.$val.'</option>'."\n"; // rausgenommen, da nicht mehr wichtig:  class="eventBoxDay'.$key.'"'.$sel.'
			if($key == 'business' && isset($aLabels) && is_array($aLabels)) {
				foreach ($aLabels as $k => $v) {
					$checked = ($sellabel == $k) ? ' selected' : '';
					$label .= "<option value=\"".$k."\" ";
					if(!empty($v['color'])) {
						$label .= "style=\"background-color: ".$v['color'].";\"";	
					}
					$label .= "$checked>&nbsp;&nbsp;".$v['name']."</option>\n";
				}
			}
		}
		$label .= '</select>';
		// output
		return $label;	
	}
?>
