<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_calendar.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// GET-vars abholen
	$formfield	= (isset($_GET['formfield'])) ? $oFc->make_secure_string($_GET['formfield']) : '';
	$formname	= (isset($_GET['formname'])) ? $oFc->make_secure_string($_GET['formname']) : '';

	if(!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $aENV['charset']; ?>">
	<meta http-equiv="content-language" content="<?php echo $syslang; ?>">
	<title>Kalender Popup</title>
	
	<link href="<?php echo $aENV['path']['css']['http']; ?>styles.css" type="text/css" rel="stylesheet">
	<style type="text/css">
	<?php require_once($aENV['path']['config']['unix'].$sStylesheet."styles.php"); ?>
	</style>
	<style type="text/css">
	#contentPopup	{position:absolute; top:3px; left:0px; }
	</style>
	
	<script language="JavaScript" type="text/javascript">
	// puts value into input field of opener page
	function put_value(val) {
		<?php if (!empty($formfield) && !empty($formname)) { ?>
		opener.document.forms['<?php echo $formname; ?>']['aData[<?php echo $formfield; ?>]'].value = val;
		opener.document.forms['<?php echo $formname; ?>']['aData[<?php echo $formfield; ?>]'].focus();
		opener.focus();
		self.close();
		<?php } else { ?>
		alert('<?php echo $aMSG['err']['popup_miss_vars'][$syslang]; ?>')
		<?php } ?>
	}
	</script>
</head>

<body class="bodyPopup">

<div id="contentPopup">
<?php 
	// create new calendar object.
    require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	$oCal =& new calendar($syslang,$oTACEA);
	
	// set calendar vars
	$sHref = "javascript:put_value('{D}.{M}.{Y}')"; // Link zum opener.window ({x} = Ersetzungen)
	$sGetVars = '&amp;formname='.urlencode($formname).'&amp;formfield='.urlencode($formfield); // Buttons!
	
    // output calendar
	echo $oCal->getMinicalendarHtml($sHref, $sGetVars, $aENV['cal']['cwweeks']); // params: [$sHref=''][,$sButAddGetVars=''][,$bWithCw=false]
?>
</div>

</body>
</html>