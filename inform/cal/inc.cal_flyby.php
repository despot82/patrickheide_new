<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.cal_flyby.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}
?>
<script language="JavaScript" type="text/javascript">
// FLYBYs
function getLayer(layerID) { // getLayerObject
	return (document.getElementById?document.getElementById(layerID):document.all?document.all[layerID]:document.layers[layerID]);
}
function captureEvent() { // Cross-Browser Event-Capturing fuer MOUSEMOVE
	if(document.all) { document.onmousemove = mouseMove; }	// IE
	else if(document.getElementById) { document.addEventListener("mousemove", mouseMove, true); }	// W3C
}
function getMouseX(e) { return (document.layers ? e.pageX : document.all ? window.event.clientX : document.getElementById ? e.pageX : false); }
function getMouseY(e) { return (document.layers ? e.pageY : document.all ? window.event.clientY : document.getElementById ? e.pageY : false); }
var xpos, ypos; // init!
function mouseMove(e) { // Aktion bei Event MOUSEMOVE
	xpos = getMouseX(e);
	ypos = getMouseY(e);
}
captureEvent(); // init!
function Flyby(str) { // "info"-Layer an mouse-position einblenden
	var layerwidth = 350;
	var newxpos = (xpos > 500) ? (xpos - (layerwidth + 20)) : (xpos + 10); // ab mouseX=500 nach links, sonst nach rechts
	getLayer("info").style.width = layerwidth;
	getLayer("info").style.left = newxpos;
	getLayer("info").style.top = ypos + 5;
	getLayer("info").style.visibility = "visible";
	getLayer("info").innerHTML = "<p>" + str + "</p>";
}
function hideFlyby() { // "info"-Layer ausblenden
	getLayer("info").style.visibility = "hidden";
}
</script>
<div id="info" style="position:absolute; z-index:1000; visibility:hidden; left:200px; top:200px; width:350px; background-color:#ffffff; border-color:#999999; border-width:1px; border-style:solid; padding:10px"></div>
