<?php
/*
 * Portal-Modul monthcalendar 
 */

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.mod_monthcalendar.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

	// date
	$today_ts	= time();
	$date_ts	= (isset($_GET['date_ts'])) ? $oFc->make_secure_int($_GET['date_ts']) : $today_ts; // default: heute
	$oDate->set_timestamp($date_ts); // init timestamp in date-object
	
	// define edit-page link
	$sGEToptions = '?date_ts='.$date_ts;
	$sEditPage	= $aENV['path']['cal']['http'].'cal_calendar_detail.php';
	$sViewPage	= $aENV['path']['cal']['http'].'cal_calendar.php?view=day';
	
	// ggf. TEAMACCESS
	if(!is_object($oTACEA)) {
		require_once($aENV['path']['global_service']['unix']."class.TeamAccessControllEA.php");
		require_once($aENV['path']['global_bean']['unix']."class.TeamAccessControllBean.php");
		$oTACEA =& new TeamAccessControllEA($oDb);
		$oTACEA->setTable($aENV['table']['sys_team_access']);
		$oTACEA->setAENV($aENV);
		$oTACEA->initialize();
	}
	
	// create new calendar object.
	if(!$aENV['php5']) {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php");
	}
	else {
		require_once($aENV['path']['global_module']['unix']."class.calendar.php5");
	}
	$oCal =& new calendar($syslang, $oTACEA);
	
	// set filter default
	$aFilter = array("show_birthdays" => true);
	if (isset($Userdata['office_id'])) {
		$aFilter['office'] = $Userdata['office_id'];
	}
	$oCal->setFilter($aFilter); // params: $aFilter
	
	// Buttons zum blaettern
	#$date_ts_pregsearch = '/(\?|\&)date_ts=([0-9])*/';
	#$self = $aENV['PHP_SELF'].preg_replace($date_ts_pregsearch, '', $sGEToptions).'?date_ts=';
	$self = $aENV['PHP_SELF'].'?date_ts=';
	$prev_day_ts = $oDate->add_months(-1);
	$next_day_ts = $oDate->add_months(1);
	
	// HTML
?>
<style>
.calTableBody td	{text-align:left;}
.calWeekday,.calSaturday,.calSunday,.calNotthismonth	{height:48px;}
</style>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabelle"><tr><th>
	<table width="100%" border="0" cellspacing="1" cellpadding="2"><tr><td class="th"><b><?php
		echo $oDate->get_monthname().' '; // Monat
		echo $oDate->get_php_date("Y");	// Jahr
		?></b></td></tr></table>
	</th><th>
	<table width="100%" border="0" cellspacing="1" cellpadding="2"><tr><td class="th" align="right"><?php
	echo '<input type="button" value="&lt;" class="smallbut" onclick="location.href=\''.$self.$prev_day_ts.'\'">';
	echo '<input type="button" value="'.$aMSG['btn']['today'][$syslang].'" class="smallbut" onclick="location.href=\''.$self.$today_ts.'\'">';
	echo '<input type="button" value="&gt;" class="smallbut" onclick="location.href=\''.$self.$next_day_ts.'\'">';
	?></td></tr></table>
</th></tr></table>

<?php // print out calendar
	echo $oCal->getMonthcalendarHtml($oDb, $aENV['cal']['cwweeks'], $sEditPage, $sViewPage); // params: $oDb[,$bWithCw=false][,$sEditPage=''][,$sViewPage='']
?>