<?php
/**
* cms_navi.php
*
* Overviewpage: navi //-> 2sprachig und voll kopierbar!
*
* @param	int		$nToggleId	[welche id zum aufklappen] (optional)	// HIGHLIGHT-var
* @param	string	$moveDir	[fuer Prio (wird gesondert abgeholt): 'top'|'btm'|'up'|'dwn'] (optional)
* @param	int		$moveId		[fuer Prio (wird gesondert abgeholt): id des zu priorisierenden datensatzes] (optional)
* @param	string	$moveCat	[fuer Prio (wird gesondert abgeholt): feld der db, welches "kategorisiert"] (optional)
*
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.3 / 2005-06-21 (NEU: Anzahl Subnavi-Level mit "$aENV['cms_navi_sublevel']" parametrisiert)
* #history	2.2 / 2004-12-06 (NEU: Navi-Klasse verbessert + 3.Ebene + Service-Navi eingebaut)
* #history	2.1 / 2004-08-02 (NEU: Navi-Klasse und "no_sub_templates" eingebaut)
* #history	2.0 / 2004-05-04 (new_intranet)
* #history	1.1 / 2003-05-27
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_navi.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	
	if (!$oPerm->hasPriv('admin')) { header("Location: ".$aENV['page']['cms_welcome']); } // NO ACCESS without 'admin'-privilege!

// 2a. GET-params abholen
	$nToggleId		= (isset($_GET['nToggleId'])) ? $oFc->make_secure_int($_GET['nToggleId']) : '';
	$sGEToptions	= '';

// 2b. POST-params abholen

// 2c. Vars:
	$sTable			= $aENV['table']['cms_navi'];
	$oDb2			=& new dbconnect($aENV['db']); // fuer SUB
	$oDb3			=& new dbconnect($aENV['db']); // fuer SUBSUB
	$sEditPage		= $aENV['SELF_PATH']."cms_navi_detail.php";	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;					// fuer NEW-button
	$nEntriesMain	= 0; // init fuer zaehler
	$sContainerPath		= '../../';

	// available language-versions
	$aAvailableLanguages = $aENV['content_language'];
	unset($aAvailableLanguages['default']); // delete "default"

	// CMS-Content (+ Search) Object
	require_once($aENV['path']['global_module']['unix']."class.cms_content_edit.php");
	$oCmsContent =& new cms_content_edit($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]

// 3. PRIO
	if (!empty($_GET['moveDir']) && !empty($_GET['moveId'])) {
		// move_prio SPEZIAL mit zusaetzlichem parameter
		movePrio($aENV['table']['cms_navi'], "parent_id", "AND flag_topnavi='".($_GET['flag_topnavi'] + 0)."'"); // params: "meine_tabelle"[, "mein_kategorie_feld"]
		// aktualisiere navi-Array-FILE
		$oNav->clearCache();
		// ggf. aktualisiere XML-Struktur
		if (isset($aENV['create_xml_structure']) && $aENV['create_xml_structure'] == true) {
			write_xml_structure();
		}
		// CmsContent: aktualisiere XML-Struktur f.d. Flash-Texteditor
		if (is_object($oCmsContent)) {
			$oCmsContent->_write_flasheditor_xmlstructure(); // params: -
		}
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

	// CMS-Navi Toggle-Hilfsklasse (Seitenende)
	$oToggle =& new cmsNaviToggle();
?>

<form>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['cms']['structure'][$syslang]; ?></span></p></td>
		<td align="right">
			<p><?php echo '<a href="'.$aENV['SELF_PATH'].'cms_portal.php" title="'.$aMSG['cms']['backtocontent'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['cms']['backtocontent'][$syslang].'" class="btn"></a>'; ?>

			</p>
		</td>
	</tr>
</table>

<?php echo HR; ?>

<?php // NAVI
foreach($aENV['content_navi'] as $flag_topnavi => $naviBez) { // i.d. "setup.php" definiert!
	// GET-vars (pflicht!)
	$sGEToptions = "?flag_topnavi=".$flag_topnavi;
	// Titel dieses Navigationsbereichs
	echo '<table width="100%" border="0" cellspacing="0" cellpadding="2"><tr><td>';
	echo '<span class="text">'.strToUpper($naviBez[$syslang]).'</span><br>';
	echo '</td><td align="right"><p>';
	echo '<a href="'.$sEditPage.$sGEToptions.'" title="'.$aMSG['navi']['add_section'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_add.gif" alt="'.$aMSG['navi']['add_section'][$syslang].'" class="btn"></a>';
	echo '</p></td></tr></table>';
?>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup>
		<col width="10%">
		<col width="65%">
		<col width="5%">
		<col width="10%">
		<col width="5%">
		<col width="5%">
	</colgroup>
	<tr valign="top">
		<th rowspan="2" width="10%"><?php echo $aMSG['form']['prio'][$syslang]; ?></th>
		<th rowspan="2" width="65%"><?php echo $aMSG['form']['title'][$syslang]; ?></th>
		<th rowspan="2" width="5%">&nbsp;</th>
		<th width="10%"><?php echo $aMSG['form']['state'][$syslang]; ?></th>
		<th rowspan="2" width="5%">&nbsp;</th>
		<th rowspan="2" width="5%">&nbsp;</th>
	</tr>
	<tr>
		<td class="th"><?php echo implode("&nbsp;|&nbsp;", array_keys($aAvailableLanguages)); ?></td>
	</tr>
<?php
// MAIN #####################################################################################################
	// QUERY -> {PARENT_ID} ist ein Platzhalter und muss in jeder Schleife ersetzt werden!!!
	$sQueryTemplate = "SELECT id, parent_id, prio,";
	foreach ($aAvailableLanguages as $key => $val) {
		$sQueryTemplate .= "title_".$key.", flag_online_".$key.", online_from_".$key.", online_to_".$key.", ";
	}
	$sQueryTemplate .= "flag_navi, template FROM ".$sTable." WHERE parent_id='{PARENT_ID}' AND flag_topnavi='".$flag_topnavi."' ORDER BY prio DESC";
	
	// DB MAIN
	$sQuery = str_replace('{PARENT_ID}', '0', $sQueryTemplate);
	$oDb->query($sQuery);
	$nEntriesMain = floor($oDb->num_rows());
	$nEntriesMainTotal += $nEntriesMain; // get total main entries
	$m = 0; // zaehler main-navi entries
	while ($aData = $oDb->fetch_array()) {
		$m++; // zaehle hoch
		$sGEToptions = "?id=".$aData['id'];
		
		// DB SUB
		$sQuery = str_replace('{PARENT_ID}', $aData['id'], $sQueryTemplate);
		$oDb2->query($sQuery);
		$nEntriesSub = floor($oDb2->num_rows()); // get total entries
?>
	<tr>
		<td class="sub1"><p><?php // prio-buttons (NAVI SPECIAL: mit flag_topnavi!)
			echo get_prio_buttons($m, $nEntriesMain, $aData['id'], $aData['parent_id'], '&nToggleId='.$nToggleId.'&flag_topnavi='.$flag_topnavi); // params: $nCounter,$nEntries,$sID[,$sCat=''][,$sGEToptions='']
		?></p></td>
		<td class="sub1"><p><?php // show/hide subnavi
			echo $oToggle->getToggleIcon($aData['id'], $nEntriesSub);
		?><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo$aMSG['mode']['edit'][$syslang]; ?>"><b><?php echo (!empty($aData['title_'.$weblang])) ? $aData['title_'.$weblang]:$aMSG['std']['no_entries_lang'][$syslang]; ?></b></a></p>
		</td>
		<td class="sub2"><p><?php // EDIT
			echo '<a href="'.$sEditPage.'?id='.$aData['id'].'&flag_topnavi='.$flag_topnavi.'" title="'.$aMSG['btn']['edit'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['btn']['edit_content'][$syslang].'" class="btn"></a>';
		?></p></td>
		<td class="sub2"><p><?php // status-image
		foreach ($aAvailableLanguages as $key => $val) {
			echo get_status_img($aData['flag_online_'.$key], $aData['title_'.$key], $sEditPage.$sGEToptions."&weblang=".$key, '', $aData['flag_navi'], $aData['online_from_'.$key], $aData['online_to_'.$key])."\n";
		} ?></p></td>
		<td class="sub1" align="right"><p><?php // Jump-to-Content-Button
		if ($aData['template'] != 'no_page') {
			echo '<a href="'.$aENV['path']['cms']['http'].'template.'.$aData['template'].'.php?navi_id='.$aData['id'].'" title="'.$aMSG['btn']['edit_content'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_goto.gif" alt="'.$aMSG['btn']['edit_content'][$syslang].'" class="btn"></a>';
		} ?></p></td>
		<td class="sub1" align="right"><p><?php // ggf. add subsection
		if ($aENV['cms_navi_sublevel'] > 0 && !in_array($aData['template'], $aENV['no_sub_templates'])) {
			echo '<a href="'.$sEditPage.'?parent_id='.$aData['id'].'&flag_topnavi='.$flag_topnavi.'" title="'.$aMSG['navi']['add_subsection'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_add.gif" alt="'.$aMSG['navi']['add_subsection'][$syslang].'" class="btn"></a>';
		} ?></p></td>
	</tr>
	
<?php if ($oToggle->openToggle($aData['id'])) { // zeige subnavi, wenn gewuenscht
	// SUB *************************************************************************************************
		$s = 0; // zaehler sub-navi entries
		while ($aData2 = $oDb2->fetch_array()) {
			$s++; // zaehle hoch
			$sGEToptions = "?id=".$aData2['id'].'&parent_id='.$aData['id'];
			
			// DB SUBSUB
			$sQuery = str_replace('{PARENT_ID}', $aData2['id'], $sQueryTemplate);
			$oDb3->query($sQuery);
			$nEntriesSubSub = floor($oDb3->num_rows()); // get total entries
 ?>
	<tr>
		<td class="sub2"><p><?php // prio-buttons (NAVI SPECIAL: mit flag_topnavi!)
			echo get_prio_buttons($s,$nEntriesSub, $aData2['id'], $aData2['parent_id'], '&nToggleId='.$nToggleId.'&flag_topnavi='.$flag_topnavi); // params: $nCounter,$nEntries,$sID[,$sCat=''][,$sGEToptions='']
		?></p></td>
		<td class="sub2"><p><?php // show/hide subnavi
			echo '<img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="17" height="1" alt="" border="0">'; // spacer
			echo $oToggle->getToggleIcon($aData2['id'], $nEntriesSubSub);
		?><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo $aMSG['mode']['edit'][$syslang]; ?>"><?php echo (!empty($aData2['title_'.$weblang])) ? $aData2['title_'.$weblang]:$aMSG['std']['no_entries_lang'][$syslang]; ?></a></p>
		</td>
		<td class="sub2"><p><?php // EDIT
			echo '<a href="'.$sEditPage.'?id='.$aData2['id'].'&flag_topnavi='.$flag_topnavi.'" title="'.$aMSG['btn']['edit'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['btn']['edit_content'][$syslang].'" class="btn"></a>';
		?></p></td>
		<td class="sub2"><p><?php // status-image
		foreach ($aAvailableLanguages as $key => $val) {
			echo get_status_img($aData2['flag_online_'.$key], $aData2['title_'.$key], $sEditPage.$sGEToptions."&weblang=".$key, '', $aData2['flag_navi'], $aData2['online_from_'.$key], $aData2['online_to_'.$key])."\n";
		} ?></p></td>
		<td class="sub1" align="right"><p><?php // Jump-to-Content-Button
		if ($aData2['template'] != 'no_page') {
			echo '<a href="'.$aENV['path']['cms']['http'].'template.'.$aData2['template'].'.php?navi_id='.$aData2['id'].'" title="'.$aMSG['btn']['edit_content'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_goto.gif" alt="'.$aMSG['btn']['edit_content'][$syslang].'" class="btn"></a>';
		} ?></p></td>
		<td class="sub1" align="right"><p><?php // ggf. add subsubsection
		if ($aENV['cms_navi_sublevel'] > 1 && !in_array($aData['template'], $aENV['no_sub_templates'])) {
			echo '<a href="'.$sEditPage.'?parent_id='.$aData2['id'].'&flag_topnavi='.$flag_topnavi.'" title="'.$aMSG['navi']['add_subsection'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_add.gif" alt="'.$aMSG['navi']['add_subsection'][$syslang].'" class="btn"></a>';
		} ?></p></td>
	</tr>
<?php		if ($oToggle->openToggle($aData2['id'])) { // zeige subsubnavi, wenn gewuenscht
		// SUBSUB .....................................................................................
				$z = 0; // zaehler sub-navi entries
				while ($aData3 = $oDb3->fetch_array()) {
					$z++; // zaehle hoch
					$sGEToptions = "?id=".$aData3['id']."&flag_topnavi=".$flag_topnavi.'&parent_id='.$aData2['id'];
 ?>
	<tr>
		<td class="sub2"><p><?php // prio-buttons (NAVI SPECIAL: mit flag_topnavi!)
			echo get_prio_buttons($z,$nEntriesSubSub, $aData3['id'], $aData3['parent_id'], '&nToggleId='.$nToggleId.'&flag_topnavi='.$flag_topnavi); // params: $nCounter,$nEntries,$sID[,$sCat=''][,$sGEToptions='']
		?></p></td>
		<td class="sub2"><p><?php 
			echo '<img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="34" height="1" alt="" border="0">'; // spacer
			echo $oToggle->aToggleIcon['here']; ?><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="<?php echo $aMSG['mode']['edit'][$syslang]; ?>"><?php 
			echo (!empty($aData3['title_'.$weblang])) ? $aData3['title_'.$weblang]:$aMSG['std']['no_entries_lang'][$syslang]; ?></a></p>
		</td>
		<td class="sub2"><p><?php // EDIT
			echo '<a href="'.$sEditPage.'?id='.$aData3['id'].'&flag_topnavi='.$flag_topnavi.'" title="'.$aMSG['btn']['edit'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['btn']['edit_content'][$syslang].'" class="btn"></a>';
		?></p></td>
		<td class="sub2"><p><?php // status-image
		foreach ($aAvailableLanguages as $key => $val) {
			echo get_status_img($aData3['flag_online_'.$key], $aData3['title_'.$key], $sEditPage.$sGEToptions."&weblang=".$key, '', $aData3['flag_navi'], $aData3['online_from_'.$key], $aData3['online_to_'.$key])."\n";
		} ?></p></td>
		<td class="sub1" align="right"><p><?php // Jump-to-Content-Button
		if ($aData3['template'] != 'no_page') {
			echo '<a href="'.$aENV['path']['cms']['http'].'template.'.$aData3['template'].'.php?navi_id='.$aData3['id'].'" title="'.$aMSG['btn']['edit_content'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_goto.gif" alt="'.$aMSG['btn']['edit_content'][$syslang].'" class="btn"></a>';
		} ?></p></td>
		<td class="sub1" align="right"><p>&nbsp;</p></td>
	</tr>
<?php			} // END while (subsub) ?>
<?php		} // END SUBSUB ...........................................................................
		} // END while (sub) ?>
<?php } // END SUB ************************************************************************************ ?>

	 <tr><td colspan="5" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>

<?php }  // END while (main) 
	//  END MAIN ######################################################################################

// 'no-data'-string
if ($nEntriesMain == 0) { echo '	<tr><td colspan="5" align="center"><p>'.$aMSG['std']['no_entries'][$syslang].'</p></td></tr>'; }
?>
</table>
<?php
} // END foreach NAVI
?>

<table width="100%" border="0" cellspacing="1" cellpadding="2">
	<tr>
		<td><span class="text"><small><?php
// Anzahl gefundene Datensaetze anzeigen
	echo $nEntriesMainTotal; ?> <?php echo $aMSG['navi']['sections'][$syslang]; ?> | <?php
// Anzahl gefundene Datensaetze anzeigen
	$oDb->query("SELECT id FROM ".$sTable." WHERE parent_id<>'0'");
	$nEntriesSub = $oDb->num_rows();
	echo $nEntriesSub; ?> <?php echo $aMSG['navi']['subsections'][$syslang]; ?></small></span></td>
	</tr>
	<tr>
		<td><span class="text"><?php echo HR; ?><br>
		<img src="<?php echo $aENV['path']['pix']['http']; ?>icn_on.gif" alt="Online" class="btn"><small>&nbsp;Online</small>&nbsp;
		<img src="<?php echo $aENV['path']['pix']['http']; ?>icn_on_innavi.gif" alt="Online + Navigation" class="btn"><small>&nbsp;Online + Navigation</small>&nbsp;
		<img src="<?php echo $aENV['path']['pix']['http']; ?>icn_off.gif" alt="Offline" class="btn"><small>&nbsp;Offline</small><br>
		</span></td>
	</tr>
</table>
</form>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); 

// HILFSFUNKTIONEN /////////////////////////////////////////////////////////////////////////////////

// Class cmsNaviToggle
/**
* Diese Klasse stellt eine einfache Moeglichkeit zur Verfuegung in der CMS-struktur die Auf- und Zuklapp-Pfeile einzubinden! 
*
* Example: 
* <pre><code> 
* $oToggle =& new cmsNaviToggle(); // params: -
* // show/hide subnavi
* echo $oToggle->getToggleIcon($aData['id'], $nEntriesSub);
* if ($oToggle->openToggle($aData['id'])) { // zeige subnavi, wenn gewuenscht
* </code></pre>
*
* @access   public
* @package  Content
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.0 / 2003-10-20
*/
class cmsNaviToggle {
	// Class Vars
	var $aENV			= array();		// System-Variable
	var $aMSG			= array();		// System-Variable
	var $syslang		= 'de';			// System-Variable
	var $oNav			= NULL;			// System-Variable
	var $aToggleIcon	= array();		// Icons fuer Toggle-Icons (zum Aufklappen der Subnavi)
	var $nToggleId		= '';			// private Variable (aktuelle id)
	var $aToggleId		= array();		// private Variable (id UND parents of id -> alle ids bei denen aufgeklappt werden muss)
	
/**
* Konstruktor -> CMS-spezialisiert: Initialisiert das object
*
* Beispiel: 
* <pre><code> 
* $oCmsNavi =& new cmsNaviToggle(); // params: -
* </code></pre>
*
* @access   public
*/
	function cmsNaviToggle() {
		// system-vars importieren
		global $aENV, $aMSG, $syslang, $oNav;
		$this->aENV				=& $aENV;
		$this->aMSG				=& $aMSG;
		$this->syslang			=& $syslang;
		$this->oNav				=& $oNav;
		$this->nToggleId		= (isset($_GET['nToggleId'])) ? strip_tags($_GET['nToggleId']) : '';
		if (isset($_GET['navi_id'])) $this->nToggleId = strip_tags($_GET['navi_id']);
		$this->aToggleId		= array();
		// ahnen bestimmen
		if (!empty($this->nToggleId)) {
			$aParentId = $this->oNav->getAllParentId($this->nToggleId);
			foreach ($aParentId as $pid) {
				if ($pid > 0) { $this->aToggleId[] = $pid; } // wenn nicht root -> dazu
			}
			$this->aToggleId[] = $this->nToggleId; // + sich selbst auch dazu
		}
		// toggle-icon vars setzen
		$this->aToggleIcon	= array();
		$this->aToggleIcon['hide']	= '<img src="'.$this->aENV['path']['pix']['http'].'btn_hide.gif" alt="'.$this->aMSG['navi']['hide_subnToggleIds'][$this->syslang].'" class="btnsmall">';
		$this->aToggleIcon['show']	= '<img src="'.$this->aENV['path']['pix']['http'].'btn_show.gif" alt="'.$this->aMSG['navi']['show_subnToggleIds'][$this->syslang].'" class="btnsmall">';
		$this->aToggleIcon['here']	= '<img src="'.$this->aENV['path']['pix']['http'].'btn_here.gif" alt="" class="btnsmall">';
	}
	
/** Config-Funktion zum Ueberschreiben der benoetigten Icons zum "toggeln" (Aufklappen von Subnavigationen)
* 
* @access   public
* @param	array	$aToggleIcon	Vars fuer die Toggle-Icons
*/
	function setToggleVars($aToggleIcon) {
		if (is_array($aToggleIcon)) $this->aToggleIcon = $aToggleIcon;
	}
	
/** Ausgabe-Funktion: Gebe das richtige Icon zum "toggeln" (Aufklappen von Subnavigationen) zurueck.
* Beispiel: 
* <pre><code> 
* echo $oCmsNavi->getToggleIcon($aData['id'], $nEntriesSub, "&foo=".$bar); // params: $id,$nEntriesSub[,$sAddGetVars='']
* </code></pre>
* 
* @access   public
* @param	array	$id				Aktueller Datensatz f.d. ein Toggle-Icon ermittelt werden soll
* @param	string	$nEntriesSub	Anzahl Subnavigationspunkte dieses Navigationspunktes
* @param	string	$sAddGetVars	(optional) Zusatzliche GET-Vars, die beim Aufklappen nicht verloren gehen duerfen
* @return	HTML
*/
	function getToggleIcon($id, $nEntriesSub=0, $sAddGetVars='') {
		if ($nEntriesSub < 1) {
			// kein toggle-icon, sondern platzhalter
			return $this->aToggleIcon['here'];
		} else {
			// auf-/zu-klapp link
			$href = $this->aENV['PHP_SELF']."?nToggleId=";
			if (!in_array($id, $this->aToggleId)) {
			// click to open branch
				$href .=  $id;
				$status = 'show';
			} else {
			// click to close branch
				$href .= $this->oNav->getParent($id);
				$status = 'hide';
			}
			if (!empty($sAddGetVars)) { $href .= '&'.str_replace('?', '&', $sAddGetVars); }
			// output
			return '<a href="'.$href.'">'.$this->aToggleIcon[$status].'</a>';
		}
	}
	
/** Ausgabe-Funktion zum Ermitteln, ob die aktuelle Subnavigation aufklappen muss.
* Beispiel: 
* <pre><code> 
* echo ($oCmsNavi->openToggle($aData['id'])) ? "true" : "false"; // params: $currentId
* </code></pre>
* 
* @access   public
* @return	boolean		true, wenn die Subnavi ausgeklappt werden soll
*/
	function openToggle($currentId) {
		// output
		return (!empty($this->nToggleId) && in_array($currentId, $this->aToggleId)) ? true : false; // zeige subnavi oder nicht
	}
	
} // END of class -------------------------------------------------------------------------

// END HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////
?>