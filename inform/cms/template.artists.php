<?php
/**
* template.artists.php
*
* Overviewpage: artists-Template
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen: (0|1)] (optional)
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$navi_id	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	array	$aPageInfo	-> kommt aus der 'inc.cms_navi.php'
*
* ACHTUNG: STANDARD verwaltet den Datensatz nicht ueber '$id', sondern ueber den Fremdschluessel '$navi_id'!!!
*/

// 1. init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once($aENV['path']['sys']['unix'].'inc.sys_login.php');

// 2a. GET-params abholen
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$remoteSave		= (isset($_GET['remoteSave']) && $_GET['remoteSave'] == 1) ? 1 : 0;
	$sGEToptions	= '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['remoteSave']) && $_POST['remoteSave'] == 1)	{ $remoteSave = 1; }
// 2c. Vars:
	$sTemplate		= 'artists';
	$sTable			= 'web_'.$sTemplate;
	$limit			= 20;	// Datensaetze pro Ausgabeseite einstellen
	$sEditPage		= $aENV['SELF_PATH'].'template.'.$sTemplate.'_detail.php';	// fuer EDIT-link
	$sNewPage		= $sEditPage.$sGEToptions;									// fuer NEW-button
	// available language-versions
	$aAvailableLanguages = $aENV['content_language'];
	unset($aAvailableLanguages['default']); // delete "default"
	
	if (!$navi_id) { header('Location: '.$aENV['page']['cms_welcome']); } // NO ACCESS without $navi_id!

// 3. HTML
	require_once ($aENV['path']['sys']['unix'].'inc.sys_header.php');
	require_once ($aENV['path']['cms']['unix'].'inc.cms_navi.php');

// 4. Standard-Block
//	require_once('inc.template.standard_block.php');
?>

<form action="<?php echo $aENV['PHP_SELF'].$sGEToptions; ?>" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aPageInfo['title_'.$weblang]; ?></span> 
			<?php echo $aMSG['std']['orderby'][$syslang]; ?> <?php echo $aMSG['form']['date'][$syslang]; ?></p>
		</td>
		<td align="right"><?php if ($oPerm->hasPriv('create')) { echo get_button('NEW', $syslang, 'smallbut'); } // params: $sType,$sLang[,$sClass="but"] ?></td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th rowspan="2"><small>&nbsp;</small></th>
		<th width="10%" rowspan="2"><b><?php echo $aMSG['form']['prio'][$syslang]; ?></b></th>
		<th width="75%" rowspan="2"><?php echo '<b>'.$aMSG['form']['title'][$syslang].'</b> ('.$aENV['content_language'][$weblang].')'; ?></th>
		<th width="15%"><b><?php echo $aMSG['form']['state'][$syslang]; ?></b></th>
	</tr>
	<tr>
		<th><p><?php echo implode("&nbsp;|&nbsp;", array_keys($aAvailableLanguages)); ?></p></th>
	</tr>
<?php
	require_once ($aENV['path']['global_module']['unix'].'class.cms_content_view.php');
	$oView =& new cms_content_view($oDb, $sTable, $aENV['preview']); // params: $oDb,$sTable[,$bWithPreview=false]
	$oView->setFields(array('title_de', 'title_en', 'flag_online_de', 'flag_online_en', 'navi_id', 'prio'));
	$oView->setLinkFields(array('title_de', 'title_en'));
	$oView->setOrderBy('prio DESC');
	$oView->setLimit($limit);
	$oView->setNaviId($navi_id);
	$oView->setEditPage($sEditPage);
	
	$oView->select(); // params: -
	
	while ($oView->hasNext()) {
?>
	<tr>
		<td class="lang<?php echo $weblang; ?>"><p><small>&nbsp;</small></p></td>
		<td><p><?php
			echo $oView->getPrioButtons(); // params: [$sGEToptions='']
		?></p></td>
		<td><p><?php // title(-link)
			echo $oView->getLink('title_'.$weblang); // params: $sField[,$sGEToptions=''][,$nLength=80][,$bFallback=true]
		?></p></td>
		<td><?php // status-image
			foreach ($aAvailableLanguages as $key => $val) {
				echo $oView->getOnlineStatus('flag_online_'.$key, 'title_'.$key, '&weblang='.$key); // params: $sField,$sDataField[,$sGEToptions]
			} ?></td>
	</tr>
<?php } // END while
	if ($oView->getEntries() == 0) { echo '<tr><td colspan="4" align="center"><p>'.$aMSG['std']['no_entries'][$syslang].'</p></td></tr>'; }
?>
</table>

<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2">
	<tr>
		<td><p><?php // Anzahl gefundene Datensaetze anzeigen
			echo $oView->getDbStatus(); // params: -
			?></p>
		</td>
		<td align="right"><p><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oView->getDbLinks(); // params: [$sGEToptions=''][,$nPageLimit=5]
			?></p>
		</td>
	</tr>
</table>
</form>
<br><br>

<?php require_once ($aENV['path']['sys']['unix'].'inc.sys_footer.php'); ?>