<?php
/**
* search.php
*
* CMS-search-page: //-> 2sprachig - Suchfelder/-tables muessen angepasst werden!!!
*
* @param	string	$sSearchterm	Suchbegriff
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aMSG	-> kommt aus der 'php/array.messages.php'
* @param	array	$aPageInfo	-> kommt aus der 'inc.cms_navi.php' (hier noch nicht benutzt)
*
* @author	Andy Fehn <af@design-aspekt.com>
* @author	Frederic Hoppenstock <fh@design-aspekt.com>
* @version	3.2 / 2006-05-02 (umbau auf class.Search.php)
*/

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.Search.php");

// 2a. GET-params abholen
	$sSearchterm	= (isset($_GET['sSearchterm'])) ? $oFc->make_secure_string($_GET['sSearchterm']) : '';
	$sTemplate		= (isset($_GET['sTemplate'])) ? $oFc->make_secure_string($_GET['sTemplate']) : '';
	$sTable			= (isset($_GET['sTable'])) ? $oFc->make_secure_string($_GET['sTable']) : '';
	if (isset($_GET['sSearchmode']) && $_GET['sSearchmode'] == 'wordpart') {
		$sSearchmode	= 'wordpart';
	}
	if (isset($_GET['sSearchmode']) && $_GET['sSearchmode'] == 'fulltext') {
		$sSearchmode	= 'fulltext';
	}
// 2b. POST-params abholen
// 2c. Vars
	$sGEToptions	= '?sSearchterm='.$sSearchterm;
	#$sTable		= "cms_search";#$aENV['table']['cms_search'];
	// text-ersetzungen
	$str_search = array("{TEMPLATE}", "{SEARCHTERM}");
	$str_replace = array($sTemplate, $sSearchterm);
	// set defaults
	$bHasFulltextResults = false;
	$numrows = 0;
	$start = 0;
	$limit = 30;
// Search-Query Vars
	$orderby = "ref_table ASC";


// 3. DB

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['cms']['unix']."inc.cms_navi.php");

// SEARCH
	$oSearch =& new Search($oDb,$aENV,'cms',$Userdata['id']);
	$oSearch->setLang($weblang);
	###$oSearch->migrateCmsSearchTable();
	###$oSearch->migrateAdrSearchTable();
?>

<form>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td>
			<span class="title"><?php 
			if (!empty($sTemplate)) {
				echo str_replace($str_search, $str_replace, $aMSG['search']['searchresult_detail'][$syslang]);
			} else {
				echo $aMSG['search']['hl_searchresult'][$syslang];
			} 
			?></span>
		</td>
		<td align="right"><?php if (!empty($sTemplate)) {
				echo '<a href="'.$aENV['PHP_SELF'].$sGEToptions.'&sSearchmode='.$sSearchmode.'" title="'.$aMSG['btn']['back'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['btn']['back'][$syslang].'" class="btn"></a></p>';
		} ?></td>
	</tr>
</table>
<?php echo HR; ?>
<p>
<?php 
// kein suchbegriff
if (empty($sSearchterm)) {
	echo '<b>'.$aMSG['search']['no_searchterm'][$syslang].'</b>';
	echo '</p></form><br><br><br></div>';
	require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");
	exit; // seitenausgabe beenden
}
// suchbegriff zu kurz
$sSearchtermLenght = strlen($sSearchterm);
if ($sSearchtermLenght < 4) {
	echo '<b>'.$aMSG['search']['short_searchterm'][$syslang].'</b>';
	echo '</p></form><br><br><br></div>';
	require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php");
	exit; // seitenausgabe beenden
}

	$sDbSearchterm = $oSearch->modifySearchString($sSearchterm);

// wenn suchbegriff ok
if (empty($sTable)) { // ANSICHT overview ##############################################################
	
	$oSearch->select($sDbSearchterm,$start,$limit,$orderby);

	$numrows = $oSearch->getEntries();
	if ($numrows < 1) {
	// no search result at all
		echo str_replace("{SEARCHTERM}", $sSearchterm, $aMSG['search']['no_searchresult'][$syslang]).'<br><br>';
	}

	// ausgabe
	$counter = array();
	$aData = $oSearch->getSearchResult();

	for($i=0;is_array($aData[$i]);$i++) {
		$counter[$aData[$i]['ref_table']] ++;
	}
	foreach ($counter as $table => $anzahl) {
		// template = table ohne prefix
		$templ = str_replace(array("web_", "cms_"), '', $table);
		// generierung des template-Ausgabe-Namens
		$sTemplate = ($templ == "home" || $templ == "standard") 
			? $aMSG['template'][$templ][$syslang]
			: $aENV['template'][$templ][$syslang];
	// ausgabe rubrik-treffer
		echo '<b>'.$anzahl.'</b> | '."\n";
		echo '<a href="'.$aENV['PHP_SELF'].$sGEToptions.'&sTemplate='.$sTemplate.'&sTable='.$table.'&sSearchmode='.$sSearchmode.'">'.$sTemplate.'</a><br><br>'."\n";
	}
	
} else { // ANSICHT detail ##############################################################
	
	$oDb2 =& new dbconnect($aENV['db']);
	// extra constraint
	$oSearch->setFilter("ref_table='".$sTable."'");
	
	// titel + template aus navi-table in tmp-array speichern (performance!)
	$oDb->query("SELECT id, template, title_".$syslang." AS navi_title FROM ".$aENV['table']['cms_navi']);
	while ($aData = $oDb->fetch_array()) {
		$aNaviTmp[$aData['id']]['navi_title'] = $aData['navi_title'];#
		$aNaviTmp[$aData['id']]['template'] = $aData['template'];
	}
	$oSearch->select($sDbSearchterm,$start,$limit,$orderby);
	// DB-search 1: fulltext-search
	if ($oSearch->getNumfulltext() > 0) { // wenn fulltextsearch erfolgreich war dann variable setzen
		$bHasFulltextResults = true;
	}
	// DB-search 2: normal-search
	if ($oSearch->getNumsearchtext() > 0) { // wenn normal-search erfolgreich war dann variable setzen
		$bHasWordpartResults = true;
	}
	
	// mode-switcher
	// ggf. GET-Options erweitern
	if (!empty($sTemplate))	$sGEToptions .= '&sTemplate='.$sTemplate;
	if (!empty($sTable))	$sGEToptions .= '&sTable='.$sTable;
	// entsprechende Nachricht ausgeben
	
	if($sSearchmode != '') {
		// do search
		if ($sSearchmode == 'fulltext') {
			$oSearch->select($sDbSearchterm,$start,$limit,$orderby,true,false);
		} else {
			$oSearch->select($sDbSearchterm,$start,$limit,$orderby,false,true);
		}
	}
	$numrows = $oSearch->getEntries();
	if ($numrows < 1) {
	// no search result at all
		echo str_replace("{SEARCHTERM}", $sSearchterm, $aMSG['search']['no_searchresult'][$syslang]).'<br><br>';
	}
	
	// ausgabe
	echo '<ol start="1">';
	$aData = $oSearch->getSearchResult();
//	echo $oSearch->getEntries(); // debug
	foreach($aData as $k => $v) {

		if(($sSearchmode == '') || ($sSearchmode == 'fulltext' && $v['type'] == 'fulltext') || ($sSearchmode == 'wordpart' && $v['type'] == 'wordpart')) {
		
		// subquery
			$oDb2->query("SELECT navi_id FROM ".$sTable." WHERE id='".$v['id']."'");
			$aData2 = $oDb2->fetch_array();
			// build vars
			$page		= $aNaviTmp[$aData2['navi_id']]['template'];
			$getvars	= '?navi_id='.$aData2['navi_id'];
			$navi_title	= $aNaviTmp[$aData2['navi_id']]['navi_title'];
			$subtitle	= explode (' || ', $v['searchtext']);
			$subtitle	= ($v['title'] == "") ? $aMSG['err']['notitle'][$syslang] : $v['title'];
			if ($sTable != $aENV['table']['cms_standard']) {
				$page		.= '_detail';
				$getvars	.= '&id='.$v['id'];
			} else {
				$getvars	.= '&showEditBlock=1'; // editBlock-Layer einblenden (bei overview-page)
			}
			if (!empty($v['lang'])) {
				$getvars	.= '&weblang='.$v['lang'];
				$subtitle	.= ' ['.$v['lang'].']';
			}
		// link
			echo '<li><a href="'.$aENV['SELF_PATH'].'template.'.$page.'.php'.$getvars.'"><b>'.$navi_title.'</b><br>'.$subtitle.'</a><br></li>'."\n";
		}
	}
	echo "</ol>";
	
} // END ANSICHT ############################################################## 

?>
</p>
</form>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>