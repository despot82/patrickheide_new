<?php
/**
* cms_newsletter_recipient_detail.php
*
* Detailpage: newsletter-recipient-detail
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$show		[Suchkriterium]
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_newsletter_recipient_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	if (isset($_GET['id'])) { $id = $oFc->make_secure($_GET['id']); } else { $id = ''; }
	if (isset($_GET['start'])) { $start = $oFc->make_secure($_GET['start']); } else { $start = ''; }
	if (isset($_GET['show'])) { $show = $oFc->make_secure($_GET['show']); } else { $show = ''; }
	$sGEToptions = "?start=".$start."&show=".$show;
// 2b. POST-params abholen
	if (isset($_POST['aData']))	{ $aData = $oFc->make_secure($_POST['aData']); } else { $aData = array(); }
	if (isset($_POST['btn']))	{ $btn = $oFc->make_secure($_POST['btn']); } else { $btn = array(); }
// 2c. Vars:
	$sTable = "newsletter_recipient";
	$sViewerPage	= $aENV['SELF_PATH']."cms_newsletter_recipient.php".$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;									// fuer NEW-button

// 3a. DB-action: delete
	if (isset($btn['delete']) && $aData['id']) {
		$aData2delete['id'] = $aData['id']; // Delete data from editwiew
		$oDb->make_delete($aData2delete,$sTable);
		header("Location: ".$sViewerPage); exit;
	}
// 3b. DB-action: save or update
	if (isset($btn['save'])) {
		// erst checken ob email = unique
		$oDb->query("SELECT id FROM ".$sTable." WHERE email='".$aData['email']."' AND id!='".$aData['id']."'");
		$entries = floor($oDb->num_rows());
		if ($entries>0) {
			echo js_alert("Diese E-Mail Adresse ist bereits angemeldet!");
			$alert=true;
		}
	}
	if (isset($btn['save']) && !isset($alert)) {
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aUpdate['id'] = $aData['id'];
			$oDb->make_update($aData,$sTable,$aUpdate);
		} else { // Insert eines neuen Eintrags
			$aData['angemeldet_date'] = date("Y-m-d");
			$oDb->make_insert($aData,$sTable);
			$aData['id'] = $oDb->insert_id();
		}
	}
// 3c. DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id,$sTable);
		$mode_key = "edit";
	} else {
		$mode_key = "new";
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>

<script language="JavaScript" type="text/javascript">
<!--
function confirmDelete() {
	var chk = window.confirm("<?php echo $aMSG['form']['confirmDelete'][$syslang]; ?>");
	return(chk);
}
function checkReqFields() {
	var ok = true;
	frm = document.forms["recipient"];
	with(frm) {
		obj = elements['aData[email]'];
		if (obj.value == "" || obj.value.indexOf('@') == -1 || obj.value.indexOf('.') == -1) {
			ok = false;
			m = " <?php echo $aMSG['form']['alertEmail'][$syslang]; ?> ";
			mfocus = obj;
		}		
		if(ok == false) {
			if(m) { alert(m); }
			if(mfocus) { mfocus.focus(); }
			return false;
		} 
		else { return true; }
	}
}
//-->
</script>

<form name="recipient" action="<?php echo $aENV['PHP_SELF'].$sGEToptions; ?>" enctype="multipart/form-data" method="post">
<input type="hidden" name="aData[id]" value="<?php echo $aData['id']; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><p><span class="title">Newsletter <?php echo $aMSG['std']['recipient'][$syslang]; ?></span> <?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p></td>
		<td align="right">
			<?php echo $oForm->button("BACK"); // params: $sType[,$sClass=''] ?>
			<?php echo $oForm->button("NEW"); // params: $sType[,$sClass=''] ?>
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td><p><b>E-Mail *</b><br><small>[max. 50 characters]</small></p></td>
		<td><input type="text" name="aData[email]" value="<?php echo $aData['email']; ?>" size="55" maxlength="50"></td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['comments'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea("bemerkung", 3, 75); //  params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><b>Format *</b></p></td>
		<td><input type="radio" name="aData[format]" value="plain"<?php if ($aData['format']=="plain" || !$aData['format']) { echo " checked"; } ?> class="radiocheckbox"><span class="small"> Plain-Text&nbsp;&nbsp;</span><input type="radio" name="aData[format]" value="html"<?php if ($aData['format']=="html") { echo " checked"; } ?> class="radiocheckbox"><span class="small"> HTML &nbsp;&nbsp;&nbsp;</span></td>
	</tr>
	
<?php if (is_array($aData)) { ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['state'][$syslang]; ?></p></td>
		<td><span class="text">
		<?php // testempfaenger (nur als admin auswahlmoeglichkeit)
		if ($oPerm->hasPriv('admin')) {
			echo $aMSG['form']['testRec'][$syslang].': '; ?>
			</span>
			<?php echo $oForm->radio("flag_testempf"); // params: $sFieldname[,$aValues=''][,$bFirstChecked=false][,$bLinebreaks=false] ?>
			<br><span class="text">
		<?php
		} else {
			if ($aData['flag_testempf']) { echo $aMSG['form']['testRec'][$syslang].'<br>'; }
		}
		// angemeldet / abgemeldet
		if ($aData['angemeldet_date']) { echo $aMSG['form']['signOn'][$syslang].": ".$oDate->date_mysql2trad($aData['angemeldet_date'])."<br>"; }
		if ($aData['abgemeldet_flag']) { echo $aMSG['form']['signOff'][$syslang].": ".$oDate->date_mysql2trad($aData['abgemeldet_date'])."<br>"; }
		// letzter erhaltener newsletter
		if ($aData) {
			if ($aData['letzte_nl_id']>0) {
				$oDb->query("SELECT betreff_datum,flag_sonder_nl FROM newsletter_content WHERE id='".$aData['letzte_nl_id']."'");
				$aData = $oDb->fetch_array();
				if ($aData['flag_sonder_nl']=="1") { $sSubjectTitle = $aMSG['form']['nlExtra'][$syslang]; }
				else { $sSubjectTitle="Newsletter "; }
				echo $aMSG['form']['state1'][$syslang].$sSubjectTitle.$oDate->date_mysql2trad($aData['betreff_datum']);
			} 
			else { echo $aMSG['form']['state2'][$syslang]; }
	  	} ?></span>
		</td>
	</tr>
<?php } ?>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr>
		<td><p>&nbsp;</p></td>
		<td><input type="submit" value="<?php echo $aMSG['form']['btnSave'][$syslang]; ?>" name="btn[save]" class="but" onClick="return checkReqFields()">&nbsp;<?php
		if ($mode_key == "edit") {echo '<input type="submit" value="'.$aMSG['form']['btnDelete'][$syslang].'" name="btn[delete]" class="but" onClick="return confirmDelete()">&nbsp;';} 
		if ($mode_key == "edit") {echo '<input type="button" value="'.$aMSG['form']['btnCancel'][$syslang].'" class="but" onClick="javascript:location.href=\''.$sViewerPage.'\';">';} 
		?></td>
	</tr>
	<tr>
		<td colspan="2" class="off"><p>* <?php echo $aMSG['std']['required'][$syslang]; ?></p></td>
	</tr>
</table>
</form>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
