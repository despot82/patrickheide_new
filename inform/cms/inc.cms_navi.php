<?php
/**
* inc.cms_navi.php
*
* navi-include (nur CMS) //-> 2sprachig und voll kopierbar!
*
* @param	string	$navi_id	-> kommt aus der 'inc.sys_login.php'
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	object	$oNav		-> kommt aus der 'php/_include_all.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.2 / 2004-12-06 (NEU: Navi-Klasse verbessert + 3.Ebene + Service-Navi eingebaut)
* #history	2.1 / 2004-08-02 (NEU: Navi-Klasse eingebaut)
* #history	2.0 / 2004-05-04 (new_intranet)
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.cms_navi.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

?>

<!-- Rahmentabelle linke Zelle -->
<td width="25%" class="contentNaviPad">
<div class="text">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td width="70%" height="24"><span class="title"><b><?php echo $aMSG['cms']['structure'][$syslang]; ?></b></span></td>
		<td width="30%" align="right" nowrap><?php // BUTTONS
		if ($oPerm->hasPriv('admin')) { echo '<a href="'.$aENV['SELF_PATH'].'cms_meta.php" title="'.$aMSG['cms']['meta'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_info.gif" alt="'.$aMSG['cms']['meta'][$syslang].'" title="'.$aMSG['cms']['meta'][$syslang].'" class="btn"></a>'; }
		if ($oPerm->hasPriv('admin')) { echo '<a href="'.$aENV['SELF_PATH'].'cms_navi.php" title="'.$aMSG['cms']['editstructure'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['cms']['editstructure'][$syslang].'" title="'.$aMSG['cms']['editstructure'][$syslang].'" class="btn"></a>'; }
		?></td>
	</tr>
</table>

<?php
// horizontale Linie
	echo HR;

// CMS-NAVI (wird mittels CMS-NAVI-Klasse (+ HtmlFolder-Klasse) generiert)
if (is_object($oNav)) {

// JavaScript-Block: Icon-Preloader
	echo $oNav->getJsIconPreloader();
?>

<form action="<?php echo $aENV['SELF_PATH']; ?>cms_search.php" method="get" name="searchForm">
<input type="text" name="sSearchterm" value="<?php echo (isset($_GET['sSearchterm']) && !empty($_GET['sSearchterm'])) ? $_GET['sSearchterm'] : ''; ?>" <?php echo get_input_size('130'); ?> class="input">&nbsp;<?php echo get_button('SEARCH', $syslang, 'but'); ?>
</form>

<?php
// horizontale Linie
	echo HR;

// NAVI-KLASSE initialisieren
	$oNav->loadNavi($navi_id, $weblang, false); // params: $nNaviId[,$sLang='de'][,$bValidOnly=true]
	
// Infos der aktiven Seite im Array "$aPageInfo" merken
	$aPageInfo = $oNav->getItemArray($navi_id);
	
// wird in der form admin benötigt!
	$aPageInfo['filename']	= $aPageInfo['filename_'.$weblang];
	
// Navigationsbereiche
	foreach($aENV['content_navi'] as $flag_topnavi => $naviBez) { // i.d. "setup.php" definiert!
		
		// Titel dieses Navigationsbereichs
		echo strToUpper($naviBez[$syslang])."\n";
		// spacer + linie (unter dem titel)
		echo $oNav->getDivider('hDivider');
		
		// write NAVI
		$oNav->writeNavi($flag_topnavi);
		// spacer (unter jedem Navigationsbereich)
		echo '<br>'."\n";
		
	} // END foreach (Sprachversion)
	
// Default-Aufklappen
	echo $oNav->getJsOpenDefault();
	
} // END CMS-NAVI --------------------------------------------------------------------------------------------
?>
</div>
<!-- Rahmentabelle linke Zelle schliessen -->
</td>

<!-- Rahmentabelle Main-Content-Zelle -->
<td width="75%" class="contentPadding">
