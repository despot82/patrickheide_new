<?php
// dieses Include ist IMMER eine fuer Kunden angepasste Version!
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['portal']['recent_previews'][$syslang]; ?></span><br></p></td>
		<td></td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"><br>

<?php // PREVIEW DATA (standard template)
	$sQuery = "SELECT t.navi_id, t.last_modified, t.last_mod_by, t.created_by, t.title_".$weblang." AS t_title, n.title_".$weblang." AS navi_title, n.template 
				FROM ".$aENV['table']['cms_standard']." t, ".$aENV['table']['cms_navi']." n 
				WHERE t.preview_ref_id > 0
					AND t.navi_id = n.id
				ORDER BY t.last_modified DESC";
	$sPreviewMailText .= _printRecentTable($oDb, $sQuery, 'standard', $aMSG['template']['standard'][$syslang]);
?>

<?php
// Examples for other templates:

	// PREVIEW DATA (projects template)
	/*
	$sQuery = "SELECT t.id, t.navi_id, t.last_modified, t.last_mod_by, t.created_by, t.title_".$weblang." AS t_title, n.title_".$weblang." AS navi_title, n.template 
				FROM web_project t, ".$aENV['table']['cms_navi']." n 
				WHERE t.preview_ref_id > 0
					AND t.navi_id = n.id
				ORDER BY t.last_modified DESC";
	$sPreviewMailText .= _printRecentTable($oDb, $sQuery, 'project');
	*/
?>

<?php // PREVIEW DATA (press template)
	/*
	$sQuery = "SELECT t.id, t.navi_id, t.last_modified, t.last_mod_by, t.created_by, t.title_".$weblang." AS t_title, n.title_".$weblang." AS navi_title, n.template 
				FROM web_press t, ".$aENV['table']['cms_navi']." n 
				WHERE t.preview_ref_id > 0
					AND t.navi_id = n.id
				ORDER BY t.last_modified DESC";
	$sPreviewMailText .= _printRecentTable($oDb, $sQuery, 'press');
	*/
?>

<?php // PREVIEW DATA (client template)
	/*
	$sQuery = "SELECT t.id, t.navi_id, t.last_modified, t.last_mod_by, t.created_by, t.title_".$weblang." AS t_title, n.title_".$weblang." AS navi_title, n.template 
				FROM web_client t, ".$aENV['table']['cms_navi']." n 
				WHERE t.preview_ref_id > 0
					AND t.navi_id = n.id
				ORDER BY t.last_modified DESC";
	$sPreviewMailText .= _printRecentTable($oDb, $sQuery, 'client');
	*/
?>

<?php echo HR; ?><br>

<?php 
// HILFSFUNKTION //////////////////////////////////////////////////////////////////////////
function _printRecentTable(&$oDb, $sQuery, $sTemplate, $sHeadline='') {
	// vars
	global $aENV, $syslang, $oUser, $oDate;
	$sHeadline = ($sHeadline != '') ? $sHeadline : $aENV['template'][$sTemplate][$syslang];
	// DB
	$oDb->query($sQuery);
	if ($oDb->num_rows() == 0) return; 
	// output
	echo '<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">'."\n";
	echo '<tr valign="top"><th colspan="3"><p><b>'.$sHeadline.'</b></p></th></tr>'."\n";
	while ($aData = $oDb->fetch_array()) {
		// page title
		$page_title	= (isset($aData['t_title']) && !empty($aData['t_title'])) 
			? shorten_text($aData['t_title'], 60) 
			: $aData['navi_title'];
		// page href
		if ($sTemplate == 'standard') {
			$page_link = _getEditPageLink('standard', $aData['navi_id']); // IMMER 'standard'!!!
		} else {
			$page_link = _getEditPageLink($aData['template'], $aData['navi_id'], $aData['id']);
		}
		// HTML
		echo '<tr valign="top">';
		echo '<td width="1%"><p><small>&nbsp;</small></p></td>';
		echo '<td width="70%"><p>';
		echo '	<a href="'.$page_link.'"><b>'.$page_title.'</b><br>';
		echo '	'._getPath($aData['navi_id'], $aData['navi_title']).'</a>';
		echo '</p></td>';
		echo '<td width="25%" class="sub2"><p>';
		if(isset($aData['last_mod_by'])){
			echo 	$oUser->getUserName($aData['last_mod_by']); // params: $userid
		} else {
			echo 	$oUser->getUserName($aData['created_by']); // params: $userid
		}
		echo '	<br><small>';
		echo 	$oDate->date_from_mysql_timestamp($aData['last_modified']);
		echo ' ['.$oDate->time_from_mysql_timestamp($aData['last_modified']).']';
		echo '</small></p></td>';
		echo '</tr>'."\n";
		// links fuer mail sammeln
		$sPreviewMailText = $page_link."\n";
	}
	echo '</table>'."\n";
	echo '<img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="1" height="5" alt="" border="0"><br>'."\n";
	
	// return preview-mailtext
	return $sPreviewMailText;
}
?>