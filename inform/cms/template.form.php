<?php
/**
* template.form.php
*
* Detailpage: form-Template (als Erweiterung der navi-table - gibt es theoretisch fuer jeden navipunkt)
*
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen: (0|1)] (optional)
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$navi_id	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	array	$aPageInfo	-> kommt aus der 'inc.cms_navi.php'
*
* ACHTUNG: STANDARD verwaltet den Datensatz nicht ueber '$id', sondern ueber den Fremdschluessel '$navi_id'!!!
*/

// 1. init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once($aENV['path']['sys']['unix'].'inc.sys_login.php');

// 2a. GET-params abholen
	$remoteSave		= (isset($_GET['remoteSave']) && $_GET['remoteSave'] == 1) ? 1 : 0;
	$sGEToptions	= '';
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$sWeblang		= (isset($_POST['weblang'])) ? $_POST['weblang'] : '';
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['remoteSave']) && $_POST['remoteSave'] == 1)	{ $remoteSave = 1; }
// 2c. Vars
	$sTemplate		= 'form';
	$sTable			= $aENV['table']['cms_standard'];

	if (!$navi_id) { header('Location: '.$aENV['page']['cms_welcome']); } // NO ACCESS without $navi_id!

// SHARED BEGIN ****************************************************************************
	if (file_exists("./inc.cms_functions.php")) { require_once ("./inc.cms_functions.php"); }
// END SHARED *****************************************************************************

// CMS-Content (+ Search) Object
	require_once($aENV['path']['global_module']['unix'].'class.cms_content_edit.php');
	$oCmsContent =& new cms_content_edit($oDb, $sTable, $aENV['preview']); // params: $oDb,$sTable[,$bWithPreview=false]
	// feld, das den flash-editor benutzt, definieren (default: KEIN Flasheditor)
	$oCmsContent->setFlashEditorField('copytext_'.$weblang); // params: $sFieldname
	$oCmsContent->setFlashEditorField('copytext2_'.$weblang); // params: $sFieldname
	$oCmsContent->setFlashEditorField('copytext3_'.$weblang); // params: $sFieldname
	// inhalte fuer search-table definieren
	$oCmsContent->setSearchLang($weblang); // params: $sLang
	$sSearchtitle = getSearchTitle($sTemplate, $aData, $weblang);
	$oCmsContent->setSearchTitle($sSearchtitle);
	$sSearchtext = getSearchText($sTemplate, $aData, $weblang);
	$oCmsContent->setSearchText($sSearchtext); // params: $sSearchtext
	// funktion definieren, die ggf. bei save ausgefuehrt werden soll
	if (isset($aENV['create_xml_structure']) && $aENV['create_xml_structure'] == true) {
		$oCmsContent->setFunctionOnSave('write_xml_structure'); // params: $sName[,args]
	}

// 3. DB
// DB-action: save // Update eines bestehenden Eintrags
	if ((isset($btn['save']) || $remoteSave == 1) && $aData['id'] && !isset($alert)) {
		// make update
		$aData['weblang'] = $sWeblang;
		$aData = $oCmsContent->save($aData);
	}
	if (isset($btn['accept_changes'])) {
		$oCmsContent->bReloadAfterSave = false;
		$aData['weblang'] = $sWeblang;
		$aData = $oCmsContent->save($aData); // params: $aData
		$oCmsContent->bReloadAfterSave = true;
		// Live-Datensatz mit Preview-Datensatz ueberschreiben; Preview-Datensatz loeschen
		$aData = $oCmsContent->accept($aData); // params: $aData
	}
	if (isset($btn['discard_changes'])) {
		// Preview-Datensatz loeschen + ID des Live Datensatzes zurueckgeben
		$aData['id'] = $oCmsContent->restore($aData); // params: $aData
	}
	// DB-action: select
	$aData			= $oCmsContent->selectStandard($navi_id, $aData['id']); // params: $navi_id,$id=NULL
	$mode_key		= $oCmsContent->getModeKey(); // [new|edit]
	$online_status	= $oCmsContent->getStatus(); // [live|preview]
	
	// ggf. edit-recht entziehen!
	if (!$oPerm->hasPriv('publish')) {
		$oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix'].'inc.sys_header.php');
	require_once ($aENV['path']['cms']['unix'].'inc.cms_navi.php');

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->set_cms_vars('standard', $navi_id, $aENV['preview']); // params: $sTemplate[,$navi_id=NULL][,$bWithPreview=true]
	$oForm->check_field('title_'.$weblang, $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	$oForm->check_field('title2_'.$weblang, $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	$oForm->check_field('title3_'.$weblang, $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	#$oForm->check_field('copytext_'.$weblang, $aMSG['form']['text'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
?>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
echo $oForm->default_hidden_fields(); // params: - 
// echo $oForm->hidden('title_'.$weblang, $aPageInfo['title_'.$weblang]);	// für searchtext aktivieren, falls kein title-feld verwendet wird 
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aPageInfo['title_'.$weblang]; ?></span>
			<?php if ($online_status == 'preview') echo '<br>'.$aMSG['mode']['preview_status'][$syslang]; ?></p>
		</td>
		<td align="right">
			<?php if ($aENV['preview'] == true) echo $oForm->button('PREVIEW'); // params: $sType[,$sClass=''] ?>
		</td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<colgroup><col width="25%"><col width="75%"></colgroup>
<?php if (count($aENV['content_language']) > 2) { // more than one language (+default) ?>
	<tr>
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['language'][$syslang]; ?></b></p></td>
		<td>
		<?php echo $oForm->lang_dropdown(); // params: [$size=1] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<?php } ?>

	<tr><td colspan="2" class="off"><p><b><?php echo $aMSG['form']['form1'][$syslang]; ?></b></p></td></tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['title'][$syslang]; ?> *</b><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td>
		<?php echo $oForm->textfield('title_'.$weblang, 250, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['text'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea_flash('copytext_'.$weblang, $sTable, $aData['id'], 'small'); // params: $sFieldname, $sTable, $id ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	<tr><td colspan="2" class="off"><p><b><?php echo $aMSG['form']['form2'][$syslang]; ?></b></p></td></tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['title'][$syslang]; ?> *</b><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td>
		<?php echo $oForm->textfield('title2_'.$weblang, 250, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['text'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea_flash('copytext2_'.$weblang, $sTable, $aData['id'], 'small'); // params: $sFieldname, $sTable, $id ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	<tr><td colspan="2" class="off"><p><b><?php echo $aMSG['form']['form3'][$syslang]; ?></b></p></td></tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['title'][$syslang]; ?> *</b><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td>
		<?php echo $oForm->textfield('title3_'.$weblang, 250, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['text'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea_flash('copytext3_'.$weblang, $sTable, $aData['id'], 'small'); // params: $sFieldname, $sTable, $id ?>
		</td>
	</tr>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<!--
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['picture'][$syslang]; ?> *</b><br><small>[432 x 432 pixel]</small></p></td>
		<td>
		<?php echo $oForm->media_tools('img_id', 'image', 432, 432); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['images'][$syslang]; ?><br><small>[432 x 432 pixel]</small></p></td>
		<td>
		<?php echo $oForm->slideshow_tools('slideshow', $sTable, $aData['id'], 432, 432); // params: $sFieldname,$sTable,$sId[,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	-->
	
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['keywords'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea("keywords", 2,75); //  params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
<!-- NAVI STATUS -->
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>" height="24"><p><b><?php echo $aMSG['form']['use_on_website'][$syslang]; ?></b></p></td>
		<td><p style="float:left;"><?php 
		if (is_array($aPageInfo)) {
			// flag_online
			echo ($aPageInfo['flag_online_'.$weblang] == 1) ? $aMSG['std']['yes'][$syslang] : $aMSG['std']['no'][$syslang];
			// flag_navi
			if ($aPageInfo['flag_navi'] == 0) echo $aMSG['navi']['dont_use_on_navi'][$syslang];
			// reference_id (highlight)
			if ($aPageInfo['reference_id'] > 0) {
				$oDb->query("SELECT title_".$weblang." AS title FROM ".$aENV['table']['cms_navi']." WHERE id='".$aPageInfo['reference_id']."'");
				if ($oDb->num_rows() > 0) {
					echo ' / '.$aMSG['navi']['highlight_subsection'][$syslang].': "';
					echo $oDb->fetch_field('title').'"';
				}
			}
		} ?>
		</p>
<?php if ($oPerm->hasPriv('publish')) { ?>
		<div style="float:right;"><?php 
			echo '<a href="'.$aENV['SELF_PATH'].'cms_navi_detail.php?id='.$navi_id.'" title="'.$aMSG['form']['edit_structure_page'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['form']['edit_structure_page'][$syslang].'" class="btn"></a>';
		?></div>
<?php } ?>
		</td>
	</tr>
	
<?php if (isset($aENV['allow_cug']) && $aENV['allow_cug'] == true) { // CUG ?>
	<tr valign="top">
		<td height="24"><p><b><?php echo $aMSG['form']['usergroup'][$syslang] ?></b></p></td>
		<td><p><?php
		require_once($aENV['path']['global_service']['unix']."class.cug.php");
		$oCug =& new cug($aENV['db'], $syslang);
		$aUgRelated = $oCug->getUsergroup($aData['navi_id'], $aENV['table']['cms_navi']); // params: $sRelId ,$sRelTable
		$sUgNames = implode(', CUG ', array_values($aUgRelated));
		echo (empty($sUgNames)) ? $aMSG['std']['all'][$syslang] : "CUG ".$sUgNames; ?></p>
		</td>
	</tr>
<?php } // END if CUG ?>
	
</table>
<br>
<?php echo $oForm->button('SAVE'); // params: $sType[,$sClass=''] ?>
<?php if ($online_status == 'preview') { 
		echo $oForm->button('ACCEPT_CHANGES');
		echo $oForm->button('DISCARD_CHANGES');
	} ?>
<?php echo $oForm->button('RESET'); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->end_tag(); // params: - ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix'].'inc.sys_footer.php'); ?>