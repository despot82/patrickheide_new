<?php
/**
* browser-detection-page for CMS!
*
* checkt os/browser/version und filtert die fuer das CMS kritischen (NN4 und MacIE4.0) aus (bzw. schickt sie zur disallow-page).
* -> voll kopierbar!
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.0 / 2004-04-27
* #history	1.0
*/
// init
	require_once ("../sys/php/_include_all.php");

// browser-detection
	// nur problematische browser ausfiltern
	if ($oBrowser->isNN4()) { // identify Netscape 4.x
		header("Location: ".$aENV['page']['disallow']."?msg=nn4"); exit;
	}
	if ($oBrowser->isMacIE40()) { // identify MAC IE 4.0
		header("Location: ".$aENV['page']['disallow']."?msg=macie40"); exit;
	}
	// alles ok :-)
	else { header("Location: ".$aENV['page']['cms_welcome']); exit; }
	
	// -> HTTP 1.1 verlangt eigentlich eine ABSOLUTE Adresse!
	#else { header("Location: http://".$_SERVER['HTTP_HOST'].$aENV['path']['cms']['http'].$aENV['page']['cms_welcome']); exit; }
	// was aber trotzdem meist geht ist:
	#else { header("Location: portal.php"); exit; }
	// was manchmal NICHT geht ist:
	#else { header("Location: /cms/portal.php"); exit; }
	
	#@see: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.30
?>