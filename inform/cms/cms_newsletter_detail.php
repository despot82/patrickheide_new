<?php
/**
* cms_newsletter_detail.php
*
* Detailpage: newsletter-detail
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_newsletter_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	if (isset($_GET['id'])) { $id = $oFc->make_secure($_GET['id']); } else { $id = ''; }
	// diese beiden sind nur zum zurueckspringen zur overview-page
	if (isset($_GET['start'])) { $start = $oFc->make_secure($_GET['start']); } else { $start = ''; }
	$sGEToptions	= "?start=".$start;
// 2b. POST-params abholen
	if (isset($_POST['aData']))	{ $aData = $oFc->make_secure($_POST['aData']); } else { $aData = array(); }
	if (isset($_POST['btn']))	{ $btn = $oFc->make_secure($_POST['btn']); } else { $btn = array(); }
// 2c. Vars:
	$sTable			= "newsletter_content";
	$sViewerPage	= $aENV['SELF_PATH']."cms_newsletter".$sGEToptions;	// fuer BACK-link
	$sNewPage		= $aENV['PHP_SELF'];								// fuer NEW-link

// 3a. DB-action: delete
	if (isset($btn['delete']) && $aData['id']) {
		// delete data
		$aData2delete['id'] = $aData['id']; // Delete data from editwiew
		$oDb->make_delete($aData2delete,$sTable);
		header("Location: ".$sViewerPage); exit;
	}
// 3b. DB-action: save or update
	if (isset($btn['save']) && !isset($alert)) {
		
		// merge date-dropdowns into a valid mysql-date
		$aData['betreff_datum'] = $year."-".$month."-00";
		
		if ($aData['id']) { // Update eines bestehenden Eintrags
			$aUpdate['id'] = $aData['id'];
			$oDb->make_update($aData,$sTable,$aUpdate);
			#header("Location: ".$sViewerPage); exit;
		} else { // Insert eines neuen Eintrags
			$oDb->make_insert($aData,$sTable);
			$aData['id'] = $oDb->insert_id();
		}
	}

// Versenden
	if (isset($btn['testsend']) || isset($btn['send']) || isset($btn['nachsend'])) {
	// vars
		$sRecipientTable = "newsletter_recipient";
		$oDb2 =& new dbconnect($aENV['db']);
		// SQL-constraint fuer TESTversand
		if (isset($btn['testsend'])) { $sConstraintTest =  " AND flag_testempf=1"; } else { $sConstraintTest = ""; }
		// merge date-dropdowns into a valid mysql-date
		$aData['betreff_datum'] = $year."-".$month."-00";
		// subject zusamensetzen
		if ($aData['flag_sonder_nl']=="1") {$sSubjectTitle = $aMSG['form']['nlExtra'][$syslang]; } else {$sSubjectTitle = "Newsletter ";}
		$nl_subject = $aMSG['form']['prefix'].$sSubjectTitle.$oDate->date_mysql2trad($aData['betreff_datum']);
		
		// absender
		$nl_sender = $sNlSender; // Name <email> -> kommt aus setup.php
		
	// plain-text
		$oDb->query("SELECT id,email FROM ".$sRecipientTable." WHERE format='plain' AND abgemeldet_flag=0 AND letzte_nl_id<".$aData['id'].$sConstraintTest);
		$entries_plain = floor($oDb->num_rows());
		$plainHeader = "From: ".$nl_sender."\n";
		while ($aDataRecipient = $oDb->fetch_array()) {
			mail($aDataRecipient['email'], $nl_subject, stripslashes($aData['plain_text']), $plainHeader);
			if (!isset($btn['testsend'])) { // nur bei "echtem" versand updaten
				$oDb2->query("UPDATE ".$sRecipientTable." SET letzte_nl_id=".$aData['id']." WHERE id=".$aDataRecipient['id']);
			}
		}
	// html-text
		$oDb->query("SELECT id,email FROM ".$sRecipientTable." WHERE format='html' AND abgemeldet_flag=0 AND letzte_nl_id<".$aData['id'].$sConstraintTest);
		$entries_html = floor($oDb->num_rows());
		// zusaetzliche header informationen bei HTML
		$htmlHeader = "From: ".$nl_sender."\n";
		$htmlHeader .= "Reply-To: ".$nl_sender."\n";
		$htmlHeader .= "X-Sender: ".$nl_sender."\n";
		$htmlHeader .= "Return-Path: ".$nl_sender."\n";
		$htmlHeader .= "MIME-Version: 1.0\n";
		$htmlHeader .= "Content-Type: text/html; charset=iso-8859-1\n";
		$htmlHeader .= "X-Priority: 3\n"; // 1 UrgentMessage, 3 Normal
		$htmlHeader .= "X-Mailer: PHP / ".phpversion()."\n";
		#$htmlHeader .= "X-MSMail-Priority: High\r\n"; // High = UrgentMessage
		#$htmlHeader .= "Cc: ".$other_email."\r\n";
		#$htmlHeader .= "Bcc: ".$another_email."\r\n"; // separate multiple with commas mail@mail.com, mail2@mail.com
		### OLD: "\r\nContent-Type: text/html\r\nContent-Transfer-Encoding: 8bit\r\n" // hatte zur folge, dass "Content-Transfer-Encoding: 8bit" als Anfang des MailContents ankam!
		while ($aDataRecipient = $oDb->fetch_array()) {
			mail($aDataRecipient['email'], $nl_subject, stripslashes($aData['html_text']), $htmlHeader);
			if (!isset($btn['testsend'])) { // nur bei "echtem" versand updaten
				$oDb2->query("UPDATE ".$sRecipientTable." SET letzte_nl_id=".$aData['id']." WHERE id=".$aDataRecipient['id']);
			}
		}
		
	// bei erstmaligem "echtem" versand: newsletter versanddatum eintragen
		if (isset($btn['send'])) {
			$aData['datum_versendet'] = date("Y-m-d");
			$aUpdate['id'] = $aData['id'];
			$oDb->make_update($aData,$sTable,$aUpdate);
		}
		
	// erfolgsmeldung
		if ($entries_plain>0 || $entries_html>0) {
			if($syslang == "de") { echo js_alert("Newsletter erfolgreich an ".$entries_plain." Plain-Text- und ".$entries_html." HTML-Text-Empfänger versendet worden."); }
			else if($syslang == "en") { echo js_alert("The newsletter has successfully been sent to ".$entries_plain." plain-text-recipients and ".$entries_html." html-text recipients."); }
		} else {
			if($syslang == "de") { echo js_alert("Newsletter ist mangels Empfänger nicht versendet worden."); }
			else if($syslang == "en") { echo js_alert("The newsletter could not be sent, as there is no recipient."); }
		}
	}

// 3c. DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; } else { $aData = ""; }
	if (isset($id) && $id) {
		$aData = $oDb->fetch_by_id($id,$sTable);
		$mode_key = "edit"; // do not change!
	} 
	else {
		$mode_key = "new"; // do not change!
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	if (!$oForm->bEditMode) {
		$mode_key = "viewonly"; // do not change!
	}
?>

<script language="JavaScript" type="text/javascript">
<!--
function confirmSend() {
	var ok = window.confirm("<?php echo $aMSG['form']['confirmSend'][$syslang]; ?>");
	return(ok);
}
function confirmDelete() {
	var chk = window.confirm("<?php echo $aMSG['form']['confirmDelete'][$syslang]; ?>");
	return(chk);
}
function checkReqFields() {
	var ok = true;
	frm = document.forms["nl"];
	with(frm) {
		obj = elements['aData[plain_text]'];
		if(obj.value == "") {
			ok = false;
			m= " <?php echo $aMSG['form']['alertPlainText'][$syslang]; ?> ";
			mfocus=obj;
		}		
		obj = elements['aData[html_text]'];
		if(obj.value == "") {
			ok = false;
			m = " <?php echo $aMSG['form']['alertHTMLText'][$syslang]; ?> ";
			mfocus=obj;
		}		
		if(ok == false) {
			if(m) { alert(m); }
			if(mfocus) { mfocus.focus(); }
			return false;
		}
		else { return true; }
	}
}
//-->
</script>

<form name="nl" action="<?php echo $aENV['PHP_SELF'].$sGEToptions; ?>" enctype="multipart/form-data" method="post">
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><p><span class="title">Newsletter</span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td align="right"><?php echo $oForm->button("NEW"); // params: $sType[,$sClass=''] ?></td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="25%"><col width="75%"></colgroup>
	<tr>
		<td><p><b><?php echo $aMSG['form']['issue'][$syslang]; ?><!-- -Date --> *</b></p></td>
		<td><span class="text"><?php echo $aMSG['form']['prefix']; ?></span>
		<select name="aData[flag_sonder_nl]" size="1">
			<option value="0"<?php if ($aData['flag_sonder_nl']=="0") { echo " selected"; } ?>>Newsletter</option>
			<option value="1"<?php if ($aData['flag_sonder_nl']=="1") { echo " selected"; } ?>><?php echo $aMSG['form']['nlExtra'][$syslang]; ?></option>
		</select> 
<?php	// make data-date
		$datatoday = explode("-",$aData['betreff_datum']);
		if ($datatoday[1] != "00") { $datamonth = $datatoday[1]; }
		if ($datatoday[0] != "0000") { $datayear = $datatoday[0]; }
		// make today-date
		$today = time();
		$todaymonth = date("m", $today);
		$todayyear = date("Y", $today);
?>
		<select name="month" size="1">
<?php // build select-field
		for ($i=1;$i<13;$i++) {
			if ($i < 10) { $i = "0".$i; }
			if ($datamonth) {
				if ($i == $datamonth) {$sel = "selected";} else {$sel="";}
			} else {
				if ($i == $todaymonth) {$sel = "selected";} else {$sel="";}
			}
			echo "			<option value=\"$i\" $sel>$i</option>\n";
		} ?>
		</select>
		<select name="year" size="1">
			<option value="<?php echo $todayyear-2; ?>"<?php if ($datayear==($todayyear-2)) { echo " selected"; } ?>><?php echo $todayyear-2; ?></option>
			<option value="<?php echo $todayyear-1; ?>"<?php if ($datayear==($todayyear-1)) { echo " selected"; } ?>><?php echo $todayyear-1; ?></option>
			<option value="<?php echo $todayyear; ?>"<?php if ($datayear==$todayyear || !$datayear) { echo " selected"; } ?>><?php echo $todayyear; ?></option>
			<option value="<?php echo $todayyear+1; ?>"<?php if ($datayear==($todayyear+1)) { echo " selected"; } ?>><?php echo $todayyear+1; ?></option>
		</select>
		</td>
	</tr>
	
	<tr valign="top">
		<td><p><b>Plain-Text *</b></p></td>
		<td>
		<?php echo $oForm->textarea_flash("plain_text",'newsletter_content',$id); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''][,$sClass="smallbut"][,$sLayer="content"] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><b>HTML *</b></p></td>
		<td>
		<?php echo $oForm->textarea_flash("html_text",'newsletter_content',$id); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''][,$sClass="smallbut"][,$sLayer="content"] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
<?php 
if ($oPerm->hasPriv('admin')) { ?>
<?php if (is_array($aData)) { ?>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['send'][$syslang]; ?></b><br><small>[ <?php echo $aMSG['form']['editHint'][$syslang]; ?> ]</small></p></td>
		<td>
		<?php if ($aData['datum_versendet']!="0000-00-00") { ?>
			<span class="text"><b><?php echo $aMSG['form']['forwarding'][$syslang].$oDate->date_mysql2trad($aData['datum_versendet']) ?></b><br><br></span>
			<input type="submit" value="<?php echo $aMSG['form']['btnSendStragglers'][$syslang]; ?>" name="btn[nachsend]" class="but" onClick="return confirmSend()">
		<?php } else { ?>
			<input type="submit" value="<?php echo $aMSG['form']['btnSendTest'][$syslang]; ?>" name="btn[testsend]" class="but" onClick="return confirmSend()">
			<input type="submit" value="<?php echo $aMSG['form']['btnSend'][$syslang]; ?>" name="btn[send]" class="but" onClick="return confirmSend()">
		<?php } ?></td>
	</tr>
<?php } else { ?>
	<tr valign="top">
		<td><p>&nbsp;</p></td>
		<td><p><?php echo $aMSG['form']['newHint'][$syslang]; ?></p>
		</td>
	</tr>
<?php } ?>
<?php 
} // END if admin
if (($oPerm->hasPriv('create') && $mode_key == "new") || $oPerm->hasPriv('edit')) { ?>
	<tr>
		<td><p>&nbsp;</p></td>
		<td><input type="submit" value="<?php echo $aMSG['form']['btnSave'][$syslang]; ?>" name="btn[save]" class="but" onClick="return checkReqFields()">&nbsp;<?php
		if ($mode_key == "edit") {echo '<input type="submit" value="'.$aMSG['form']['btnDelete'][$syslang].'" name="btn[delete]" class="but" onClick="return confirmDelete()">&nbsp;';} 
		if ($mode_key == "edit") {echo '<input type="button" value="'.$aMSG['form']['btnCancel'][$syslang].'" class="but" onClick="javascript:location.href=\''.$sViewerPage.'\';">';} 
		?></td>
	</tr>
<?php 
} // END if edit ?>
	<tr>
		<td colspan="2" class="off"><p>* <?php echo $aMSG['std']['required'][$syslang]; ?></p></td>
	</tr>
</table>
</form>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
