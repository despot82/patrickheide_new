<?php
/**
* cms_preview_topframe.php
*
* ...
* -> 2sprachig und kopierbar
*
* Diese Seite wird aus der Framesetseite "preview.php" aufgerufen
*
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.2 / 2006-03-06 [NEU: "_hasUserPreviewRightOnly" in "$oPerm->hasCmsPreviewRightOnly()" geaendert]
* #history	1.11 / 2006-01-03 [BUGFIX: "$hasRight" in "_hasUserPreviewRightOnly()" eingebaut]
* #history	1.1 / 2005-08-22 [BUGFIX: "session" in "cookie" umgebaut]
* #history	1.01 / 2005-08-22 [BUGFIX: "top" in "topframe" umbenannt]
* #history	1.0 / 2005-06-14
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_preview_topframe.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once($aENV['path']['sys']['unix'].'inc.sys_login.php');

// META-Include
	$sMetaFile = $aENV['path']['cms_data']['unix'].'array.meta.php';
	// read container-FILE (-> stellt das array $aMetaInfo zur Verfuegung)
	if (file_exists($sMetaFile)) { require_once($sMetaFile); }

// 2a. GET-params abholen
// 2b. POST-params abholen
// 2c. Vars
	// vars fuer mail (Link zu der aktuellen Seite in Bearbeitung)
	$sPreviewMailRecipient	= (isset($aMetaInfo['email_webmaster']) && !empty($aMetaInfo['email_webmaster'])) ? $aMetaInfo['email_webmaster'] : 'oh@design-aspekt.com';
	$sPreviewMailSubject	= 'Seite in Bearbeitung';
	$sPreviewMailText		= "\n";
	$id						= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$previewid				= (isset($_GET['previewid'])) ? $oFc->make_secure_int($_GET['previewid']) : $id;
	$liveid					= (isset($_GET['liveid'])) ? $oFc->make_secure_int($_GET['liveid']) : '';

// HILFSFUNKTIONEN ///////////////////////////////////////////////////////////
	// welcher view?
	if (isset($_GET['preview']) && $_GET['preview'] == 'true') {
		// preview-var in Cookie speichern
		setcookie($aENV['client_shortname'].'_preview', 'true', 0, '/');
		$PREVIEW = true;
	}
	if (isset($_GET['preview']) && $_GET['preview'] == 'false') {
		// preview-var in Cookie speichern
		setcookie($aENV['client_shortname'].'_preview', 'false', 0, '/');
		$PREVIEW = false;
	}

	// welche logout prozedur?
	if (isset($_GET['logoutPreview']) && $_GET['logoutPreview'] == 1) {
		// preview-var-Cookie loeschen
		setcookie($aENV['client_shortname'].'_preview', 'false', (time() - 3600), '/');
		if ($oPerm->hasCmsPreviewRightOnly()) {
			// kompletter Logout + weiterleiten zum Login-Fenster
			$js_action = 'loginWin();';
		} else {
			// Fenster schliessen
			$js_action = 'closeWin();';
		}
	}
// END //////////////////////////////////////////////////////////////////////

// 3. HTML
?>
<html>
<head>
	<title><?php echo $aENV['client_name'].': '.$aENV['system_name'][$syslang].' - Preview Topframe'; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $aENV['charset']; ?>">
	<meta http-equiv="content-language" content="<?php echo $syslang; ?>">
	<meta name="robots" content="noindex,nofollow">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	
<link href="<?php echo $aENV['path']['css']['http'].$sStylesheet; ?>styles.css" type="text/css" rel="stylesheet">
<style type="text/css">
<?php require_once($aENV['path']['config']['unix'].$sStylesheet."styles.php"); ?>
</style>

<link href="<?php echo $aENV['path']['css']['http'].$sStylesheet; ?>topnavi.css" type="text/css" rel="stylesheet">
<style type="text/css">
<?php if ($_GET['printview'] != "true") { require_once($aENV['path']['config']['unix']."styles_topnavi.php"); } ?>
</style>

<script type="text/javascript">
	function sendPageLinkEmail() {
		var mailtostring = 'mailto:<?php echo $sPreviewMailRecipient; ?>';
		mailtostring += '?subject=<?php echo $sPreviewMailSubject; ?>';
		linkto = 'http://' + parent.window.frames['content'].location.hostname;
		linkto += parent.window.frames['content'].location.pathname;
		linkto += '\?preview=true';
		<?php
		if (!empty($id)) { echo 'linkto += "\&id='.$id.'";'; }
		?>
		mailbody = '<?php echo urlencode($sPreviewMailText); ?>' + linkto;
		mailtostring = mailtostring + '&body='+ mailbody.replace("&", "%26");
		location.href = mailtostring;
		return; // wichtig!
	}
	function toggleView(view) {
		var getvar = (view == 'preview' ? '?preview=true' : '?preview=false');
		if (view == "preview") {
			<?php
			if (!empty($id)) { echo 'getvar += "&id='.$previewid.'";'; }
			?>
		} else {
			<?php
			if (!empty($id)) { echo 'getvar += "&id='.$liveid.'";'; }
			?>
		}
		<?php
		if (!empty($previewid)) { echo 'getvar += "&previewid='.$previewid.'";'; }
		if (!empty($liveid)) { echo 'getvar += "&liveid='.$liveid.'";'; }
		?>
		var content_location = parent.window.frames['content'].location.pathname;
		parent.window.frames['topframe'].location.href = '<?php echo $aENV['PHP_SELF']; ?>' + getvar;
		parent.window.frames['content'].location.href = content_location + getvar;
		return;
	}
	function logoutPreview() {
		parent.window.frames['topframe'].location.href = '<?php echo $aENV['PHP_SELF']; ?>?logoutPreview=1';
		return;
	}
	function closeWin() {
		parent.window.close();
	}
	function loginWin() {
		parent.window.location.href = '<?php echo $aENV['page']['sys_welcome']; ?>?logout=true';
	}
<?php echo $js_action; ?>
</script>

<style type="text/css">
.bodyPopup	{ background-color: #D3D5DD; }
#previewButtonBk {
	font-family:Verdana,Arial,Helvetica,Sans-Serif; font-size: 11px; line-height: 16px; 
	position:absolute; width:600px; height:21px; top:15px; left:0px; z-index:10;
}
#previewBalkenBk {
	position:absolute; width:1500px; height:15px; top:33px; left:0px; z-index:1; 
	background-color:#FF4E00; border-width:1px; border-style:solid;
	border-top:#FF4E00; border-right:#FF4E00; border-bottom:#000000; border-left:#FF4E00;
	border-width:1px; border-style:solid;
}

.mainnavi {
	font-weight:bold; color:#000000; background-color: #E9EAEF; text-decoration:none;
	border-top:#FFFFFF; border-right:#000000; border-bottom:#000000; border-left:#FFFFFF; 
	border-width:1px; border-style:solid; margin-right:1px; padding:3px 40px 3px 40px;
}
a:link.mainnavi, a:visited.mainnavi {
	color:#000000; background-color: #E9EAEF; text-decoration:none;
}
.mainnavihi {
	font-weight:bold; color:#FFFFFF; background-color:#FF4E00; text-decoration:none;
	border-top:#FFFFFF; border-right:#000000; border-bottom:#FF4E00; border-left:#FFFFFF; 
	border-width:1px; border-style:solid; margin-right:1px; padding:3px 40px 3px 40px;
}
a:link.mainnavihi, a:visited.mainnavihi {
	color:#FFFFFF; background-color: #FF4E00; text-decoration:none;
}
a:hover.mainnavi, a:hover.mainnavihi {
	text-decoration:none;
}
</style>
</head>

<body class="bodyPopup">

<div id="previewButtonBk">
	<a href="javascript:toggleView('preview');" class="mainnavi<?php if ($_GET['preview']=='true') echo 'hi'; ?>" title="<?php echo $aMSG['preview']['preview'][$syslang]; ?>"><?php echo $aMSG['preview']['preview'][$syslang]; ?></a><a 
	href="javascript:toggleView('live');" class="mainnavi<?php if ($_GET['preview']=='false') echo 'hi'; ?>" title="<?php echo $aMSG['preview']['live'][$syslang]; ?>"><?php echo $aMSG['preview']['live'][$syslang]; ?></a><a
	href="javascript:logoutPreview();" class="mainnavi" title="<?php echo $aMSG['preview']['logout_closewin'][$syslang]; ?>"><?php echo $aMSG['preview']['logout_closewin'][$syslang]; ?></a><br>
</div>

<div id="previewBalkenBk"></div>

</body>
</html>