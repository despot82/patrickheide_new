<?php
/**
* template.events_detail.php
*
* Detailpage: events-Template
*
* @param	int		$id			welcher Datensatz	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen: (0|1)] (optional)
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$navi_id	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	array	$aPageInfo	-> kommt aus der 'inc.cms_navi.php'
*/

// 1. init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once($aENV['path']['sys']['unix'].'inc.sys_login.php');

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$start			= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$remoteSave		= (isset($_GET['remoteSave']) && $_GET['remoteSave'] == 1) ? 1 : 0;
	$sGEToptions	= '?start='.$start;
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$sWeblang		= (isset($_POST['weblang'])) ? $_POST['weblang'] : '';
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['remoteSave']) && $_POST['remoteSave'] == 1)	{ $remoteSave = 1; }
// 2c. Vars:
	$sTemplate		= 'events';
	$sTable			= 'web_'.$sTemplate;
	$sViewerPage	= $aENV['SELF_PATH'].'template.'.$sTemplate.'.php'.$sGEToptions;	// fuer BACK-button
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;									// fuer NEW-button
	
	if (!$navi_id) { header("Location: ".$aENV['page']['cms_welcome']); } // NO ACCESS without $navi_id!

// SHARED BEGIN ****************************************************************************
	if (file_exists("./inc.cms_functions.php")) { require_once ("./inc.cms_functions.php"); }
// END SHARED *****************************************************************************

// CMS-Content (+ Search) Object
	require_once($aENV['path']['global_module']['unix'].'class.cms_content_edit.php');
	$oCmsContent =& new cms_content_edit($oDb, $sTable, $aENV['preview']); // params: $oDb,$sTable[,$bWithPreview=false]
	// erzeuge neue maxprio bei insert
	$oCmsContent->setPrio(true); // params: [$sPrio=false]
	// feld, das den flash-editor benutzt, definieren (default: KEIN Flasheditor)
	$oCmsContent->setFlashEditorField('subtitle_'.$weblang); // params: $sFieldname
	$oCmsContent->setFlashEditorField('copytext_'.$weblang); // params: $sFieldname
	// Feld, das die Form-Methode "$oForm->date_dropdown()" benutzt, definieren (default: KEIN solches Feld)
	$oCmsContent->setDateDropdownField('date'); // params: $sFieldname
	$oCmsContent->setDateDropdownField('date2'); // params: $sFieldname
	// inhalte fuer search-table definieren
	$oCmsContent->setSearchLang($weblang); // params: $sLang
	$sSearchtitle = getSearchTitle($sTemplate, $aData, $weblang);
	$oCmsContent->setSearchTitle($sSearchtitle);
	$sSearchtext = getSearchText($sTemplate, $aData, $weblang);
	$oCmsContent->setSearchText($sSearchtext); // params: $sSearchtext
	// funktion definieren, die bei save + delete ausgefuehrt werden soll
	#$oCmsContent->setFunctionOnDelete('write_xml_press', $aData['navi_id']); // params: $sName[,args]
	#$oCmsContent->setFunctionOnSave('write_xml_press', array($navi_id, $aData['navi_id'])); // params: $sName[,args]

// CUG
	if (isset($aENV['allow_cug']) && $aENV['allow_cug'] == true) {
		require_once($aENV['path']['global_service']['unix']."class.cug.php");
		$oCug =& new cug($aENV['db'], $syslang);
		// diese Seite kann fuer CUGs geschuetzt werden (=> dem Content-Object mitteilen!)
		$oCmsContent->setCUG($oCug); // params: [$oCug=NULL]
	}
	
// 3. DB
// DB-action: delete
	if (isset($btn['delete']) && $aData['id']) {
		// Datensatz loeschen
		$oCmsContent->delete($aData['id']); // params: $id
		
		// back to overview
		header('Location: '.$sViewerPage); exit;
	}
// DB-action: save
	if ((isset($btn['save']) || $remoteSave == 1) && !isset($alert)) {
		// Datensatz sichern
		$aData['weblang'] = $sWeblang;
		$aData = $oCmsContent->save($aData); // params: $aData
	}
	if (isset($btn['save_close']) && !isset($alert)) {
		// Datensatz sichern + back to overviewpage
		$aData['weblang'] = $sWeblang;
		$oCmsContent->save_close($aData, $sViewerPage); // params: $aData, $sViewerPage
	}
	if (isset($btn['accept_changes'])) {
		$oCmsContent->bReloadAfterSave = false;
		$aData['weblang'] = $sWeblang;
		$aData = $oCmsContent->save($aData); // params: $aData
		$oCmsContent->bReloadAfterSave = true;
		// Live-Datensatz mit Preview-Datensatz ueberschreiben; Preview-Datensatz loeschen
		$aData = $oCmsContent->accept($aData); // params: $aData
	}
	if (isset($btn['discard_changes'])) {
		// Preview-Datensatz loeschen + ID des Live Datensatzes zurueckgeben
		$aData['id'] = $oCmsContent->restore($aData); // params: $aData
	}
// DB-action: select
	if (isset($aData['id'])) { $id = $aData['id']; }
	$aData			= $oCmsContent->select($id); // params: $id
	$mode_key		= $oCmsContent->getModeKey(); // [new|edit]
	$online_status	= $oCmsContent->getStatus(); // [live|preview]
	
// ggf. edit-recht entziehen!
	if ($mode_key == 'edit' && !$oPerm->hasPriv('publish') && $aData['flag_online_'.$weblang] == 1) {
		$oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix'].'inc.sys_header.php');
	require_once ($aENV['path']['cms']['unix'].'inc.cms_navi.php');

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->set_cms_vars($sTemplate, $navi_id, $aENV['preview']); // params: $sTemplate[,$navi_id=NULL][,$bWithPreview=true]
	$oForm->check_field('title_'.$weblang, $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
?>


<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
echo $oForm->default_hidden_fields(); // params: - 
// echo $oForm->hidden('title_'.$weblang, $aPageInfo['title_'.$weblang]);	// für searchtext aktivieren, falls kein title-feld verwendet wird 
?>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aPageInfo['title_'.$weblang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?>
			<?php if ($online_status == 'preview') echo '<br>'.$aMSG['mode']['preview_status'][$syslang]; ?></p>
		</td>
		<td align="right">
			<?php if ($aENV['preview'] == true) echo $oForm->button('PREVIEW'); // params: $sType[,$sClass=''] ?>
			<?php echo $oForm->button('BACK'); // params: $sType[,$sClass=''] ?>
			<?php echo $oForm->button('NEW'); // params: $sType[,$sClass=''] ?>
		</td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<colgroup><col width="25%"><col width="75%"></colgroup>

<?php if (count($aENV['content_language']) > 2) { // more than one language (+default) ?>
	<tr>
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['language'][$syslang]; ?></b></p></td>
		<td>
		<?php echo $oForm->lang_dropdown(); // params: [$size=1] ?>
		</td>
	</tr>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
<?php } ?>
	
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['date'][$syslang]; ?> *</b></p></td>
		<td><?php 
			echo $oForm->date_dropdown('date', '', 15, 2); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]
			echo ' - ';
			echo $oForm->date_dropdown('date2', '', 15, 2); // params: $sFieldname[,$bDateRequired=true][,$nPastYears=5][,$nFutureYears=1]	
		?></td>
	</tr>

	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['title'][$syslang]; ?> *</b></p></td>
		<td>
		<?php echo $oForm->textfield('title_'.$weblang, 250, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['location'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textfield('subtitle_'.$weblang, 100, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<!--<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['picture'][$syslang]; ?> *</b><br><small>[max. 1280 x 1024 pixel]</small></p></td>
		<td>
		<?php echo $oForm->media_tools('img_id', 'image', 1280, 1024); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>-->
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['images'][$syslang]; ?> *</b></p></td>
		<td>
		<?php echo $oForm->slideshow_tools('slideshow', $sTable, $aData['id']); // params: $sFieldname,$sTable,$sId[,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['text'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea_flash('copytext_'.$weblang, $sTable, $aData['id']); // params: $sFieldname, $sTable, $id ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['intlink'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->linkmanager_tools('int_link'); // params: $sFieldname[,$nSize=45] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['extlink'][$syslang]; ?><br /><small>[ http://... ]</small></p></td>
		<td>
		<?php echo $oForm->textfield('external_link', 100, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['pdf'][$syslang]; ?></p></td>
		<td><?php echo $oForm->media_tools('pdf_'.$weblang, 'pdf');	// params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0]?></td>
	</tr>

	
<?php if ($aPageInfo['layout'] == 'B') { ?>
<?php } ?>
		
	<!--<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['keywords'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea("keywords", 2,75); //  params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>-->
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
<?php if ($oPerm->hasPriv('publish')) { ?>
	<?php
	// CUG //////////////////////////////////////////////////////////
	if (is_object($oCmsContent->oCug)) require_once ("./inc.cug.php"); 
	?>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['use_on_website'][$syslang]; ?></b></p></td>
		<td>
		<?php echo $oForm->radio('flag_online_'.$weblang); // params: $sFieldname[,$aValues=''][,$bFirstChecked=false][,$bLinebreaks=false] ?>
		</td>
	</tr>
<?php } ?>
<?php if ($oPerm->hasPriv('edit')) { ?>
	<tr>
		<td><p><b><?php echo $aMSG['form']['move_to'][$syslang]; ?></b></p></td>
		<td>
		<?php echo $oForm->categories_dropdown(); // params: [$sTrenner='&nbsp;|&nbsp;'] ?>
		</td>
	</tr>
<?php } ?>
</table>
<br>
<?php echo $oForm->button('SAVE'); // params: $sType[,$sClass=''] ?>
<?php if ($online_status == 'preview') { 
		echo $oForm->button('ACCEPT_CHANGES');
		echo $oForm->button('DISCARD_CHANGES');
	} ?>
<?php echo $oForm->button('SAVE_CLOSE'); ?>
<?php if ($mode_key == 'edit') { echo $oForm->button('DELETE'); } ?>
<?php if ($mode_key == 'edit') { echo $oForm->button('CANCEL'); } ?>
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix'].'inc.sys_footer.php'); ?>
