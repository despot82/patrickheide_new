<?php
/**
* cms_newsletter.php
*
* Overviewpage: newsletter-uebersichtsseite
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_newsletter.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start = (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	#$show = (isset($_GET['show'])) ? $oFc->make_secure_string($_GET['show']) : 'all'; }
// 2c. Vars:
	$sEditPage		= $aENV['SELF_PATH']."cms_newsletter_detail.php";	// fuer EDIT-link
	$sNewPage		= $sEditPage;										// fuer NEW-link
	$sSubjectPrefix	= "design aspekt - ";
	$sTable			= "newsletter_content";
	$limit			= 20;	// Datensaetze pro Ausgabeseite einstellen
	// available language-versions
	$aAvailableLanguages = $aENV['content_language'];
	unset($aAvailableLanguages['default']); // delete "default"

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
?>

<?php echo $oForm->start_tag($aENV['PHP_SELF']); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><p><span class="title">Newsletter</span> 
			<?php echo $aMSG['std']['orderby'][$syslang]; ?> <?php echo $aMSG['form']['date'][$syslang]; ?></p>
		</td>
		<td align="right"><?php echo $oForm->button("NEW"); // params: $sType[,$sClass=''] ?></td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="70%"><p><b><?php echo $aMSG['form']['issue'][$syslang]; ?></b></p></th>
		<th width="25%"><p><b><?php echo $aMSG['form']['sent'][$syslang]; ?></b></p></th>
		<th width="5%"><p><b><?php echo $aMSG['form']['state'][$syslang]; ?></b></p></th>
	</tr>
<?php
// 4. DB-action: select
	$sQuery = "SELECT id,betreff_datum,flag_sonder_nl,datum_versendet FROM ".$sTable." ORDER BY betreff_datum DESC";
	
	$oDb->query($sQuery);
	$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)

	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
	if (empty($start)) { $start=0; }
	$oDb->limit($sQuery, $start, $limit);
	
	while ($aData = $oDb->fetch_array()) {
		$msg = ($aData['datum_versendet'] == "0000-00-00") ? "not yet!" : '';
		$sSubjectTitle = ($aData['flag_sonder_nl'] == "1") ? "Sonder-Newsletter " : "Newsletter ";
		$sGEToptions = "?id=".$aData['id']."&start=".$start;
?>
	<tr>
		<td><p><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="editieren"><?php 
			echo $sSubjectPrefix.$sSubjectTitle.$oDate->date_mysql2trad($aData['betreff_datum']); ?></a></p></td>
		<td><p><?php echo $oDate->date_mysql2trad($aData['datum_versendet']).$msg; ?></p></td>
		<td align="right" nowrap><p><?php
			$status = ($aData['datum_versendet'] == "0000-00-00") ? 0 : 1; // status aufbereiten
			// status-image
			#foreach ($aAvailableLanguages as $key => $val) {
			#	echo get_status_img($status, 1, $sEditPage.$sGEToptions."&weblang=".$key); // params: $status,$data,$href[,$extra][,$naviStatus]
			#}
			echo get_status_img($status, 1, $sEditPage.$sGEToptions); // params: $status,$data,$href[,$extra][,$naviStatus]
		?></p></td>
	</tr>
<?php } // END while
	if ($entries == 0) { echo '<tr><td colspan="3" align="center"><p>'.$aMSG['std']['no_entries'][$syslang].'</p></td></tr>'; }
?>
</table>

<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2">
	<tr>
		<td><p><?php // Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></p>
		</td>
		<td align="right"><p><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks(); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions='']
			?></p>
		</td>
	</tr>
</table>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
