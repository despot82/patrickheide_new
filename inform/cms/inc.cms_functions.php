<?php
/*
 * Created on 21.03.2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 function getClient() {
	global $oDb;
	$aParam	= func_get_args();
	$nId	= trim($aParam[0]);
	$sLang	= trim($aParam[1]);
	
	if(empty($nId)) 	{ return false; }
	if(empty($sLang))	{ return false; }
	
	$aClient	= $oDb->fetch_by_id($nId, 'web_client');
	
	return (!empty($aClient['title_'.$sLang]) ? $aClient['title_'.$sLang] : '');
}
?>
