<?php
// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './inc.cug.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// CUG ///////////////////////////////////////////////////////////////////////
if (is_object($oCug) && $oPerm->hasPriv('edit')) { // mindestbedingung
	$checkUg = $oCug->hasUsergroup($navi_id, $aENV['table']['cms_navi']); // params: $sRelId ,$sRelTable
	if (!$checkUg) { // nur wenn die Seite nicht eh schon mit UGs verknuepft ist!
	 ?>
	<tr valign="top">
		<td><p><?php echo $aMSG['std']['access'][$syslang]; ?></p></td>
		<td>
		<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr valign="top">
			<td>
			<input type="checkbox" name="no_cug" value="1" class="radiocheckbox" disabled<?php 
			if ($mode_key == "new" || empty($aData['cug'])) echo ' checked'; ?>><span class="text"><?php echo $aMSG['checkbox']['no_cug'][$syslang]; ?></span><br>
			<?php echo HR; ?>
			<input type="hidden" name="aData[cug]" value="">
			<?php // CUG: alle Verknuepften Usergroups ermitteln...
			$aUgRelated = $oCug->getUsergroup($aData['id'], $sTable); // params: $sRelId ,$sRelTable
			$aUgRelatedId = array_keys($aUgRelated);
			// alle usergroups ausgeben
			$aUG = $oCug->getAllUsergroups(); // params: -
			foreach ($aUG as $ug_id => $ug_title) {
				$checked = (is_array($aUgRelatedId) && in_array($ug_id, $aUgRelatedId)) ? ' checked' : '';
				echo '<input type="checkbox" name="cug" value="'.$ug_id.'" id="cug_'.$ug_id.'" class="radiocheckbox"'.$checked.' onclick="checkUg()">';
				echo '<label for="cug_'.$ug_id.'" class="text">'.$ug_title.'<br></label>'."\n";
			}
			// hinweis wenn noch keine usergroups existieren
			#if (count($aUG) == 0) {
			#	echo '<span class="small"><a href="'.$aENV['SELF_PATH'].'cms_cuguser_group.php">'.$aMSG['form']['no_cugusergroup'][$syslang].'</a><br></span>';
			#} ?>
			<script language="JavaScript" type="text/javascript">
			function checkUg() {
				with (document.forms['editForm']) {
					var str = '';
					var hiddencug = elements['aData[cug]']; // hiddenfield
					var cbx = elements['cug']; // checkbox(en) der UGs
					var nocug = elements['no_cug']; // fake-checkbox fuer "alle"
					if (cbx.length > 0) {
						// mehrere checkboxen (=array)
						if (nocug.checked == true) {nocug.checked = false;}
						for (var i=0; i<cbx.length; i++){
							// registriere ob eine gecheckt ist
							if(cbx[i].checked == true) {
								str += ',' + cbx[i].value; // value = kommagetrennte UG-IDs
							}
						}
						strlen = str.length; // erstes komma entfernen
						if (strlen > 0) {str = str.substr(1, strlen);}
						// fallback: wenn KEINE gecheckt ist, checke 'no_cug'
						if (str == '' && nocug.checked == false) {nocug.checked = true;}
					} else {
						// 1 checkbox (=string)
						nocug.checked = (cbx.checked == true) ? false : true; // ggf. fake-checkbox checken
						if (cbx.checked == true) {str = cbx.value;} // value = UG-ID
					}
					hiddencug.value = str; // schreibe value ins hidden-field
				}
			}
			//window.onload = checkUg;
			checkUg();
			</script>
			</td>
			<?php if ($mode_key == "edit") { ?>
			<td align="right">
			<script language="JavaScript" type="text/javascript">
			function confirmJumpToUG(towhere) {
				var chk = window.confirm("<?php echo $aMSG['err']['is_data_saved'][$syslang]; ?>");
				if(chk==true){
					location.replace('<?php echo $aENV['path']['adr']['http'];?>web_cug.php')
				}
			}
			</script>
			<a href="#" onclick="confirmJumpToUG()" title="<?php echo $aMSG['user']['edit_user_usergroup'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['user']['edit_user_usergroup'][$syslang]; ?>" class="btn"></a>
			</td><?php } // END if ?>
			</tr></table>
		</td>
	</tr>
<?php } else { ?>
	<tr valign="top">
		<td colspan="2">
		<p>Der Navigationspunkt ist einer oder mehrerer CUGs zugeordnet.<?php #echo $aMSG['form']['media_cug'][$syslang]; ?></p>
		</td>
	</tr>
<?php } // END if
} ////////////////////////////////////////////////////////////// END CUG ?>