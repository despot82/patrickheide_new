<?php
/**
* cms_meta.php
*
* Overviewpage: meta //-> 2sprachig und voll kopierbar!
*
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
*
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.01 / 2004-10-21 (BUGFIX: escape-/Anfuehrungszeichen-Problematik beim Container-schreiben)
* #history	2.0 / 2004-05-04 (new_intranet)
* #history	1.0 / 2003-06-03
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_meta.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	
	if (!$oPerm->hasPriv('admin')) { header("Location: ".$aENV['page']['cms_welcome']); } // NO ACCESS without 'admin'-privilege!

// 2a. GET-params abholen
	
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	$sGEToptions	= '';
	
// 2c. Vars
	$sTable			= $aENV['table']['cms_navi'];
	$sContainerPath		= $aENV['path']['cms_data']['unix'];
	$sContainerFilename	= 'array.meta.php';
	$sViewerPage	= $aENV['SELF_PATH']."cms_portal.php";	// fuer BACK-button...
	
// 3a. write new container-FILE
	if (isset($btn['save']) || isset($btn['save_close']) || $remoteSave == 1) {
		// fallback (wenn das erste mal nicht von "admin" gespeichert wird!)
		if (!isset($aData['flag_online']))			{ $aData['flag_online'] = 1; }
		if (!isset($aData['flag_filter_nn4']))		{ $aData['flag_filter_nn4'] = 0; }
		if (!isset($aData['flag_filter_macie40']))	{ $aData['flag_filter_macie40'] = 0; }
		if (!isset($aData['flag_filter_js']))		{ $aData['flag_filter_js'] = 0; }
		if (!isset($aData['flag_filter_flash']))	{ $aData['flag_filter_flash'] = 0; }
		
		
		$aTempM	= $oDb->fetch_by_id(1, 'cms_meta');
		
		if(!empty($aTempM)) {
			$oDb->make_update($aData, 'cms_meta', 1);
		} elseif(empty($aTempM)) {
			$aData['id']	= 1;
			$oDb->make_insert($aData, 'cms_meta');
			unset($aData['id']);
		}
		
		if(empty($aData['counter']) 
		&& $oPerm->isDaService()) { if(file_exists($aENV['path']['root']['unix'].'counter.php')) { unlink($aENV['path']['root']['unix'].'counter.php'); } }
		if(!empty($aData['counter'])
		&& $oPerm->isDaService()) {
			$sCounterString	= stripslashes($aData['counter']);
			
			$oFile->write_str_in_file($aENV['path']['root']['unix'].'counter.php', $sCounterString);
		}
		
		unset($aData['counter']);
		
		if($aData['flag_online']==0) { if (file_exists($sContainerPath.$sContainerFilename)) { unlink($sContainerPath.$sContainerFilename); } }
		if($aData['flag_online']==1) {
			// build string
			$sContainerString = '<?php // this page was created automatically by design aspekt CMS. Do not make any changes!'."\n";
			foreach($aData as $key => $val) {
				$val = str_replace('"', "'", preg_replace('/\r\n|\r|\n/', '', $val));
				$sContainerString .= '$aMetaInfo["'.$key.'"] = "'.stripslashes($val).'";'."\n";
			}
			if (empty($aData['created']) || empty($aData['created_by'])) {
				$sContainerString .= '$aMetaInfo[\'created\'] = "'.$oDate->get_mysql_timestamp().'";'."\n";
				$sContainerString .= '$aMetaInfo[\'created_by\'] = "'.$Userdata['id'].'";'."\n";
			}
			$sContainerString .= '$aMetaInfo[\'last_modified\'] = "'.$oDate->get_mysql_timestamp().'";'."\n";
			$sContainerString .= '$aMetaInfo[\'last_mod_by\'] = "'.$Userdata['id'].'";'."\n";
			$sContainerString .= "?>";
			
			// make file
			$oFile->write_str_in_file($sContainerPath.$sContainerFilename, $sContainerString);
		}
				
		// back to overviewpage
		if (isset($btn['save_close'])) {
			header("Location: ".$sViewerPage); exit;
		}
	}

	// if nFlagRewrite == true && isDaService delete & create all container files	
	if(isset($btn['container_create']) 
	&& $oPerm->isDaService()) {
		$sContainerPath		= '../../';
		
		// include the CmsNaviAdmin class && the CmsContentClass
		require_once ($aENV['path']['global_module']['unix'].'class.CmsNaviAdmin.php');
		require_once($aENV['path']['global_module']['unix']."class.cms_content_edit.php");
					
		// initialise objects
		$oAdmin			=& new CmsNaviAdmin();
		$oCmsContent	=& new cms_content_edit($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]

		// delete and create ALL Containers
		$oAdmin->createAllContainer($sContainerPath);
		
		// update navi file
		$oNav->clearCache();
		
		// if aENV create xml structure == true create xml structure
		if (isset($aENV['create_xml_structure']) && $aENV['create_xml_structure'] == true) {
			write_xml_structure();
		}
		
		// update xml structure for the flasheditor
		if (is_object($oCmsContent)) {
			$oCmsContent->_write_flasheditor_xmlstructure(); // params: -
		}
		
		echo js_alert($aMSG['meta']['alert_create'][$syslang]);
		
		header("Location: ".$_SERVER['PHP_SELF']); exit;
	}
		
// 3.b read container-FILE (-> stellt das array $aMetaInfo zur Verfuegung)
	if (file_exists($sContainerPath.$sContainerFilename)) { require_once($sContainerPath.$sContainerFilename); }

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
//	$aData = $aMetaInfo; // die Formular-Funktionen erwarten die daten im array "aData"...
	$aData	= $oDb->fetch_by_id(1, 'cms_meta');
	if(empty($aData)) { $aData	= $aMetaInfo; }
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	// description + keywords js-check fuer jede sprachversion schreiben
	foreach ($aENV['content_language'] as $key => $val) {
		if ($key == "default") { continue; } // ueberspringe default
		$oForm->check_field("description_".$key,$aMSG['form']['description'][$syslang]." (".$key.")"); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
		$oForm->check_field("keywords_".$key,$aMSG['form']['keywords'][$syslang]." (".$key.")"); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	} // END foreach
	if (isset($aENV['email_im_meta']) && $aENV['email_im_meta'] == true) { // standard email adresse nur wenn konfiguriert
		$oForm->check_field("email", "Standard-".$aMSG['form']['email'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	}
	if ($oPerm->isDaService()) { // design aspekt only
		// sonderfunktion // js-code:
		if (!isset($aMetaInfo['flag_online'])) {$aMetaInfo['flag_online'] = '0';} // '0/NULL'-fallback
		$sSonderJScheck = "obj = elements['aData[flag_online]'];\n";
		$sSonderJScheck .= "if (obj.value != ".$aMetaInfo['flag_online'].") { if (obj.value==1) {m=' online ';} else {m=' offline ';} ";
		if ($syslang == "de")	{$sSonderJScheck .= "var chk = window.confirm('Sind Sie sicher, dass Sie die Website jetzt '+m+' stellen wollen?');";}
		else				{$sSonderJScheck .= "var chk = window.confirm('Are you sure to change the website-state to '+m+'?');";}
		$sSonderJScheck .= "return(chk);}";
		$oForm->add_js($sSonderJScheck); // params: $sJScode
	}

?>


<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="remoteSave" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['cms']['meta'][$syslang]; ?></span></p></td>
		<td align="right">
			<p>
				<?php echo '<a href="'.$aENV['SELF_PATH'].'cms_portal.php" title="'.$aMSG['content']['edit'][$syslang].'"><img src="'.$aENV['path']['pix']['http'].'btn_list.gif" alt="'.$aMSG['content']['edit'][$syslang].'" class="btn"></a>'; ?>
			</p>
		</td>
	</tr>
</table>

<?php echo HR; ?>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	
<?php	// description + keywords for each language
		foreach ($aENV['content_language'] as $key => $val) {
			if ($key == "default") { continue; } // ueberspringe default ?>
	<tr valign="top">
		<td class="lang<?php echo $key; ?>"><p><b><?php echo $aMSG['form']['title'][$syslang]; ?> (<?=$key?>) *</b><br><small>[max. 150 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td class="lang<?php echo $key; ?>">
		<?php echo $oForm->textfield("title_".$key, 250, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $key; ?>"><p><b><?php echo $aMSG['form']['description'][$syslang]." (".$key.")"; ?> *</b><br><small>[max. 500 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td class="lang<?php echo $key; ?>">
		<?php echo $oForm->textarea("description_".$key, 3, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $key; ?>"><p><b><?php echo $aMSG['form']['keywords'][$syslang]." (".$key.")"; ?> *</b><br><small>[max. 20 <?php echo ($syslang=='de') ? $aMSG['form']['keywords'][$syslang] : strToLower($aMSG['form']['keywords'][$syslang]); ?>]</small></p></td>
		<td class="lang<?php echo $key; ?>">
		<?php echo $oForm->textarea("keywords_".$key, 4, 78); // params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr><tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	
<?php	} // END foreach ?>

<?php if (isset($aENV['email_im_meta']) && $aENV['email_im_meta'] == true) { // standard email adresse nur wenn konfiguriert ?>
	<tr valign="top">
		<td><p><b>Standard-<?php echo $aMSG['form']['email'][$syslang]; ?> *</b><br><small>[max. 150 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td>
		<?php echo $oForm->textfield("email", 250, 78); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
<?php	} // END if ?>

<?php
	foreach($aENV['content_language'] as $sKey => $sLang) {
		if($sKey=='default') { continue; }
		?>
		<tr valign="top">
			<td><p><b><?php echo $aMSG['form']['language_state'][$syslang]." ($sLang)"; ?> *</b></p></td>
			<td>
			<?php echo $oForm->radio("flag_online_".$sKey, array('offline', 'online')); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
			</td>
		</tr>		
		<?php
	}
?>
	
<?php if ($oPerm->isDaService()) { // design aspekt only ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['website_state'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->select("flag_online", array(0 => 'under construction', 1 => 'online')); // params: $sFieldname,$aValues[,$sExtra=''][,$nSize=1][,$bMultiple=false] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['filter_browser'][$syslang]; ?></p></td>
		<td><?php echo $oForm->checkbox("flag_filter_nn4"); // params: $sFieldname[,$sDefaultCheckboxValue='1'][,$sDefaultHiddenValue='0'][,$sExtra=''] ?>
			<span class="text">Netscape Navigator 4</span><br>
			<?php echo $oForm->checkbox("flag_filter_macie40"); // params: $sFieldname[,$sDefaultCheckboxValue='1'][,$sDefaultHiddenValue='0'][,$sExtra=''] ?>
			<span class="text">Mac-IE 4.0</span><br>
		</td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['filter_js'][$syslang]; ?></p></td>
		<td><?php echo $oForm->radio("flag_filter_js"); // params: $sFieldname[,$aValues=''][,$bFirstChecked=false][,$bLinebreaks=false] ?></td>
	</tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['filter_flash'][$syslang]; ?></p></td>
		<td><?php echo $oForm->radio("flag_filter_flash"); // params: $sFieldname[,$aValues=''][,$bFirstChecked=false][,$bLinebreaks=false] ?></td>
	</tr>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['meta']['counter'][$syslang]; ?></p></td>
		<td><span class="small"><?php echo $oForm->textarea('counter'); ?></small>
		</td>
	</tr>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['meta']['firm_ip'][$syslang]; ?></p></td>
		<td><span class="small"><?php echo $oForm->textfield('firm_ip'); ?></small>
		</td>
	</tr>

<?php } // END design aspekt only ?>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="1" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><p><?php echo $aMSG['meta']['sitemap_url'][$syslang]; ?></p></td>
		<td><span class="small"><?php echo $aENV['sitemap']['index']['url']; ?></small>
		</td>
	</tr>

</table>
<br>
<?php echo $oForm->button("SAVE"); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button("SAVE_CLOSE"); // params: $sType[,$sClass=''] ?>
<?php
	if($oPerm->isDaService()) { 
		echo $oForm->button("CONTAINER_CREATE"); // params: $sType[,$sClass=''] 
	}
?>
<?php echo $oForm->button("RESET"); // params: $sType[,$sClass=''] ?>
<?php if (!$oPerm->isDaService()) { ?>
<input type="hidden" name="aData[firm_ip]" value="<?php echo $aData['firm_ip']; ?>">
<input type="hidden" name="aData[flag_online]" value="<?php echo $aData['flag_online']; ?>">
<input type="hidden" name="aData[flag_filter_nn4]" value="<?php echo $aData['flag_filter_nn4']; ?>">
<input type="hidden" name="aData[flag_filter_macie40]" value="<?php echo $aData['flag_filter_macie40']; ?>">
<input type="hidden" name="aData[flag_filter_js]" value="<?php echo $aData['flag_filter_js']; ?>">
<input type="hidden" name="aData[flag_filter_flash]" value="<?php echo $aData['flag_filter_flash']; ?>">
<?php } ?>
<input type="hidden" name="aData[created]" value="<?php echo $aData['created']; ?>">
<input type="hidden" name="aData[created_by]" value="<?php echo $aData['created_by']; ?>">
<?php echo $oForm->end_tag(); ?>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
