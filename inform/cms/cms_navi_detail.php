<?php
/**
* cms_navi_detail.php
*
* Detailpage: navi //-> 2sprachig und voll kopierbar!
*
* @param	int		$id			[welcher Datensatz]	// diese seite ist bei GET-uebergabe der $id eine edit-seite, sonst eine neu-seite
* @param	int		$parent_id	[welche parent_id]	// fuer den fall dass noch keine parent_id existiert oder zum korrekten zurueckspringen und highlighten der aufklapp-navi
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$remoteSave	[um das Formular von aussen abschicken zu koennen: (0|1)] (optional)
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*/

// shared functions begin
	
		/**
		 *	save entry in global search table for every entry of the passed table with title, copytext and keywords
		 *
		 * @global array config array
		 * @global string navi structure page to return to after update
		 * 
		 * @param object database object
		 * @param string table to read
		 * @param array languages
		 * @param array userdata
		 */					
		function sqlTable2SearchTable($oDb, $sTable, $aLanguages, $Userdata) {
			global $aENV, $sViewerPage, $Userdata;
			
			// initialise search object
			$oSearch	=& new Search($oDb, $aENV, 'cms', $Userdata['id']);
			$oSearch->setRefTable($sTable);
			
			$oDbUpdate	= new dbconnect($aENV['db']);
			$sSql	= "SELECT s.* FROM ".$aENV['table']['cms_navi']." n JOIN ".$sTable." s ON n.id = s.navi_id";
			$oDbUpdate->query($sSql);
			
			$sKey		= ereg_replace("cms_|web_", "", $sTable);			
			
			// fetch line by line
			while($aData = $oDbUpdate->fetch_array()) {
				
				// make an insert or update for every entry of the ref table
				foreach($aLanguages as $sLang => $null) {
					$sSearchtext	= '';
					$sSearchtitle	= '';
					$sSearchtext	= getSearchText($sKey, $aData, $sLang);
					$sSearchtitle	= getSearchTitle($sKey, $aData, $sLang);

					$oSearch->setText($sSearchtext);
					$oSearch->setTitle($sSearchtitle);
					$oSearch->setLang($sLang);

					$oSearch->save($aData['id']);
					
				} // end foreach
				
			} // end while
			
		} // end function
		
// shared functions end		

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_navi_detail.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once($aENV['path']['sys']['unix'].'inc.sys_login.php');
	require_once ($aENV['path']['global_module']['unix'].'class.CmsNaviAdmin.php');
	require_once ($aENV['path']['global_service']['unix'].'class.Search.php');
	
	if (!$oPerm->hasPriv('admin')) { header('Location: '.$aENV['page']['cms_welcome']); } // NO ACCESS without 'admin'-privilege!

// 2a. GET-params abholen
	$id				= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$parent_id		= (isset($_GET['parent_id'])) ? $oFc->make_secure_int($_GET['parent_id']) : '0';
	$remoteSave		= (isset($_GET['remoteSave']) && $_GET['remoteSave'] == 1) ? 1 : 0;
	$sGEToptions	= '?';
	
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();
	if (isset($_POST['remoteSave']) && $_POST['remoteSave'] == 1)	{ $remoteSave = 1; }
	
// 2c. Vars:
	if (isset($aData['parent_id']) && !empty($aData['parent_id']))	{ $parent_id = $aData['parent_id']; }
	if (isset($aData['parent_id']) && $aData['parent_id'] == '0')	{ $parent_id = '0'; } // 0/NULL fallback
	if (!empty($parent_id))	{ $sGEToptions .= '&nToggleId='.$parent_id; }	// zum highlighten der aufklapp-navi beim zurueckspringen!
	
	$sTable			= $aENV['table']['cms_navi'];
	$sViewerPage	= $aENV['SELF_PATH'].'cms_navi.php'.$sGEToptions;	// fuer BACK-button...
	$sNewPage		= $aENV['PHP_SELF'].$sGEToptions;					// fuer NEW-button
	
	$sContainerPath		= '../../';
	$sContainerFilename	= '';
	
	if (empty($aData['layout'])) { $aData['layout'] = 'A'; } // set default
	
	// available language-versions
	$aAvailableLanguages = $aENV['content_language'];
	unset($aAvailableLanguages['default']); // delete "default"
	$nAnzLanguages = count($aAvailableLanguages);

	// CMS-Content (+ Search) Object
	require_once ($aENV['path']['global_module']['unix'].'class.cms_content_edit.php');
	$oCmsContent	= & new cms_content_edit($oDb, $sTable); // params: $oDb,$sTable[,$bWithPreview=false]
	
	// CUG
	if (isset($aENV['allow_cug']) && $aENV['allow_cug'] == true) {
		require_once ($aENV['path']['global_service']['unix'].'class.cug.php');
		$oCug =& new cug($aENV['db'], $syslang);
	}
	
	// CMS-Navi Administrations-Hilfsklasse
	$oAdmin =& new CmsNaviAdmin();

// 3. DB
// DB-action: delete
	if (isset($btn['delete']) && $aData['id']) {
		// check if this navi-item has sub-items
		$sql	= "SELECT id FROM ".$sTable." WHERE parent_id=".$aData['id'];
		$oDb->query($sql);
		$checkSub	= floor($oDb->num_rows()); // get total entries

		if ($checkSub > 0) {// do not delete but alert
			echo js_alert($aMSG['navi']['alert_del'][$syslang]);
		} else {
			// .php an den Filename aus $aData anhängen
			foreach($aENV['content_language'] as $sKey => $sValue) {
				if($sKey == 'default') continue;
				$aData['filename_'.$sKey]	= str_replace('.php','',$aData['filename_'.$sKey]);
				$aData['filename_'.$sKey]	.= '.php';
			} // end foreach aENV content language	
				
			$aData = $oAdmin->delete($aData,$oDb,$sTable,$sContainerPath,new Search($oDb,$aENV,'cms',$Userdata['id']));

			// aktualisiere navi-Array-FILE
			$oNav->clearCache();
			
			// ggf. aktualisiere XML-Struktur
			if (isset($aENV['create_xml_structure']) && $aENV['create_xml_structure'] == true) {
				write_xml_structure();
			}
			
			// CmsContent: aktualisiere XML-Struktur f.d. Flash-Texteditor
			if (is_object($oCmsContent)) {
				$oCmsContent->_write_flasheditor_xmlstructure($aENV['linkman']['multilang']); // params: -
			}
			
			// CUG: loesche alle verknuepfungen mit diesem user zu usergroups
			if (is_object($oCug)) {
				$oCug->removeAssignments($aData['id'], $sTable); // params: $sRelId, $sRelTable
			}
			
			// back to overview page
			header('Location: '.$sViewerPage); exit;
		}
	}
	
// DB-action: save or update
	if ((isset($btn['save']) || isset($btn['save_close']) || $remoteSave == 1) && !isset($alert)) {

		$nCug	= $aData['cug'];
		unset($aData['cug']);

		// checkbox-handling: verlinke auf ersten unterpunkt
		if (isset($_POST['first_sub'])) { $aData['link'] = '{FIRST_SUB}'; }
		
		// foreach aENV content language and write compare array
		foreach ($aENV['content_language'] as $sKey => $sValue) {
			if($sKey	== 'default')	continue;
			if(empty($aData['title_'.$sKey])) continue;
			if($aData['template'] == 'home') continue;

			$aCompare['filename_'.$sKey]	= $aData['filename_'.$sKey].".php";
		}
		
		$aData = $oAdmin->save($aData, $oDb, $sContainerPath, $sTable, $Userdata, $_POST);

		// CUG: usergroups in verknuepfungstabelle speichern
		if (is_object($oCug)) {
			$oCug->assignUsergroup($aData['id'], $sTable, $nCug); // params: $sRelId, $sRelTable, $usergroup
			unset($aData['cug']); // loesche dieses feld (da es in der navi-table nicht existiert!)
		}

		// foreach aENV content language and check if filenames have changed
		foreach ($aENV['content_language'] as $sKey => $sValue) {
			if($sKey	== 'default')	continue;
			if($aData['template'] == 'home') continue;

			if($aCompare['filename_'.$sKey]	== $aData['filename_'.$sKey])
				unset($aCompare['filename_'.$sKey]);
			else
				$aCompare['filename_'.$sKey]	.= " ($sValue)";	
		}

		// if array aCompare != empty alert dynamic file change
		if(!empty($aCompare) 
		&& isset($_REQUEST['aData']['id']) 
		&& !empty($_REQUEST['aData']['id']))
			echo js_alert($aMSG['navi']['dyn_file_change'][$syslang]." ".implode("\n",$aCompare));
			
		// aktualisiere navi-Array-FILE
		$oNav->clearCache();

		// ggf. aktualisiere XML-Struktur
		if (isset($aENV['create_xml_structure']) && $aENV['create_xml_structure'] == true) {
			write_xml_structure();
		}
				
		// CmsContent: aktualisiere XML-Struktur f.d. Flash-Texteditor
		if (is_object($oCmsContent)) {
			$oCmsContent->_write_flasheditor_xmlstructure(); // params: -
		}
		
		// back to overviewpage
		if (isset($btn['save_close'])) {
			header('Location: '.$sViewerPage); exit;
		}
	}
	
// DB-action: select
	if (isset($aData['id'])) {	$id	= $aData['id']; } else { $aData = ""; }
	if (isset($id) && !empty($id)) {
		$aData		= $oDb->fetch_by_id($id, $sTable);
		$mode_key	= 'edit'; // do not change!

		// checkbox fuer "linkto-first-sub"
		if ($aData['link'] == '{FIRST_SUB}') {
			$aData['link']	= '';
			$first_sub		= 1;
		} else { $first_sub = 0; }
		// set defaults
		if ($aENV['cms_navi_onlinefromto_dateselect'] == 'popup') {
			// wenn die zeitsteuerung mittels "$oForm->date_popup()" gemacht wird: datum formatieren!
			foreach ($aENV['content_language'] as $key => $val) {
				$aData['online_from_'.$key]	= $oDate->date_mysql2trad($aData['online_from_'.$key],false);
				$aData['online_to_'.$key]	= $oDate->date_mysql2trad($aData['online_to_'.$key],false);
			}
		}
	} else {
		$mode_key	= 'new'; // do not change!
		// set defaults
		$default_online_from	= ($aENV['cms_navi_onlinefromto_default_start'] == 'today') 
								  ? date('Y-m-d') 
								  : '0000-00-00';
		
		foreach ($aENV['content_language'] as $key => $val) {
			$aData['online_from_'.$key]	= $default_online_from;
			// wenn die zeitsteuerung mittels "$oForm->date_popup()" gemacht wird: datum formatieren!
			if ($aENV['cms_navi_onlinefromto_dateselect'] == 'popup') {
				$aData['online_from_'.$key]	= $oDate->date_mysql2trad($aData['online_from_'.$key],false);
			}
		}
	}

	// if nFlagRewrite == true && isDaService delete & refresh global search table	
	if(isset($btn['search_refresh']) 
	&& isset($aENV['searchpattern'])
	&& $oPerm->isDaService()) {
		
		// include the CmsNaviAdmin class && the CmsContentClass
		require_once ($aENV['path']['global_service']['unix'].'class.Search.php');

		// require functions from cms for getSearch Functions 
		if(file_exists('./inc.cms_functions.php')) { require_once ('./inc.cms_functions.php'); }
		
		// save all languages in an array (except default key)
		$aLanguages	= $aENV['content_language'];
		unset($aLanguages['default']);

		
		$sTable	= (isset($aENV['template'][$aData['template']][$syslang])) ? "web_".$aData['template']:(($aData['template']=='home' || $aData['template']=='standard' ) ? $aENV['table']['cms_standard']:'web_'.$aData['template']);
		sqlTable2SearchTable($oDb, $sTable, $aLanguages, $Userdata); 

		// redirect to navi structure
		header("Location: ".$sViewerPage); exit;
	}

// 4. HTML
	require_once ($aENV['path']['sys']['unix'].'inc.sys_header.php');
	require_once ($aENV['path']['sys']['unix'].'inc.sys_no_content_navi.php');

	// .php wegschneiden bei Filenames aus der Datenbank
	foreach($aENV['content_language'] as $k => $v) {
		if($k == 'default') continue;
		$aData['filename_'.$k]	= str_replace('.php','',$aData['filename_'.$k]);
	}

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	// title js-check fuer jede sprachversion schreiben
	/*
	foreach ($aENV['content_language'] as $key => $val) {
		if ($key == "default") { continue; } // ueberspringe default
		$oForm->check_field("title_".$key,$aMSG['form']['title'][$syslang]." (".$key.")!"); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
	}*/
	if (!isset($aData['template'])) { // bei NEW
		$sSonderJScheck = "template = elements['aData[template]']; // es wird erwartet, dass template ein ARRAY ist!\n";
		$sSonderJScheck .= "i=0; for (anz=1; anz<template.length; anz++){ i++; }\n";
		$sSonderJScheck .= "if (template[i].value=='no_page') {\n";
		$sSonderJScheck .= "obj = elements['aData[link]'];\n";
		$sSonderJScheck .= "obj2 = elements['first_sub'];\n";
		$sSonderJScheck .= "if (template[i].checked && (!obj.value && !obj2.checked)) {ok=false;m=' ".$aMSG['navi']['alert_nolink'][$syslang]."! ';mfocus=obj;}\n";
		$sSonderJScheck .= "}\n"; // dieser ganze block hatte nur die funktion zu ueberpruefen, ob - wenn 'no_page' selektiert wurde - auch ein link eingegeben wurde!!! :-(
		$oForm->add_js($sSonderJScheck); // params: $sJScode
	} else { // bei EDIT
		$sSonderJScheck = "obj = elements['aData[template]']; // es wird erwartet, dass template EIN HIDDEN-FIELD ist!\n";
		$sSonderJScheck .= "if (obj.value=='no_page') {\n";
		$sSonderJScheck .= "obj2 = elements['aData[link]'];\n";
		$sSonderJScheck .= "obj3 = elements['first_sub'];\n";
		$sSonderJScheck .= "if (!obj2.value && !obj3.checked) {ok=false;m=' ".$aMSG['navi']['alert_nolink'][$syslang]."! ';mfocus=obj2;}\n";
		$sSonderJScheck .= "}\n"; // dieser ganze block hatte nur die funktion zu ueberpruefen, ob - wenn 'no_page' selektiert wurde - auch ein link eingegeben wurde!!! :-(
		$oForm->add_js($sSonderJScheck); // params: $sJScode
	}

	if (!$oForm->bEditMode) {
		$mode_key	= "viewonly"; // do not change!
	}
?>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<input type="hidden" name="aData[id]" value="<?php echo $id; ?>">
<input type="hidden" name="parent_id" value="<?php echo $aData['parent_id']; ?>">
<input type="hidden" name="remoteSave" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['cms']['structure'][$syslang]; ?></span> 
			<?php echo $aMSG['mode'][$mode_key][$syslang]; ?></p>
		</td>
		<td align="right"><?php echo $oForm->button('BACK'); // params: $sType[,$sClass=''] ?></td>
	</tr>
</table>
<br />

<?php if (!isset($aData['template'])) { // NEW-mode ?>
<span class="small"><?php echo $aMSG['navi']['subheadline'][$syslang]; ?><br /><br /></span>
<?php } ?>


<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<?php if (count($aENV['content_navi']) > 1) { // NUR wenn mehrere Navi-Bereiche ?>
	<tr>
		<td width="30%"><p>Navigation</p></td>
		<td width="70%"><?php // parent_id select-field
		echo $oAdmin->getAssignNaviSelect($aData); // params: &$aData[,$maxLevel=2] ?>
		</td>
	</tr>
<?php } else {
	echo $oForm->hidden('flag_topnavi', 0);

} ?>
	<tr>
		<td width="30%"><p><?php echo $aMSG['navi']['section'][$syslang]; ?></p></td>
		<td width="70%"><?php // parent_id select-field
		echo $oAdmin->getAssignParentSelect($aData, $aENV['cms_navi_sublevel']); // params: &$aData[,$maxLevel=2] ?>
		</td>
	</tr>

<?php	// title + flag_online for each language
		foreach ($aENV['content_language'] as $key => $val) {
			if ($key == 'default') { continue; } // ueberspringe default ?>
	<tr valign="top">
		<td class="lang<?php echo $key; ?>"><p><?php echo $aMSG['form']['title'][$syslang]; if ($nAnzLanguages>1) {echo " (".$key.")";} ?><br /><small>[max. 150 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td class="lang<?php echo $key; ?>">
		<div style="float:left"><?php echo $oForm->textfield('title_'.$key,250,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?></div>
		<div style="float:right">
		<?php // Jump-to-Content-Button
			if ($mode_key == 'edit' && $aData['template'] != 'no_page') {
				echo '<a href="'.$aENV['path']['cms']['http'].'template.'.$aData['template'].'.php?navi_id='.$aData['id'].'&weblang='.$key.'" title="'.$aMSG['btn']['edit_content'][$syslang].'">';
				echo '<img src="'.$aENV['path']['pix']['http'].'btn_goto.gif" border="0" alt="'.$aMSG['btn']['edit_content'][$syslang].'" class="btn"></a>';
			}
		?>
		</div>
		</td>
	</tr>
	<? 
	if($aData['id'] != '' && $aData['template'] != 'home') { ?>
	<tr valign="top">
		<td class="lang<?php echo $key; ?>">
			<p><b><?php echo $aMSG['form']['filename'][$syslang]; ?></b><br /><small>[max. 254 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td class="lang<?php echo $key; ?>">
			<p><?php echo $oForm->textfield('filename_'.$key,254,75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
			.php</p>
		</td>
	</tr>
	<? 
	} 
	if($aData['template'] == 'home') {
		$aData2 = $oAdmin->getContainerFilename($aData);
		$oForm->aData['filename_'.$key] = $aData2['filename_'.$key];
		echo $oForm->hidden('filename_'.$key);
	}
	?>
	<tr valign="top">
		<td class="lang<?php echo $key; ?>"><p><b><?php echo $aMSG['form']['use_on_website'][$syslang]; ?></b></p></td>
		<td class="lang<?php echo $key; ?>"><p><?php 
			if ($aData['template'] == 'home' && $aData['flag_online_'.$key] == 1) { // SONDERFALL: HOME ist immer online!
				echo $aMSG['std']['yes'][$syslang].' ';
				echo '<input type="hidden" name="flag_online_'.$key.'" value="1">';
			} else {
				echo $oForm->radio('flag_online_'.$key); // params: $sFieldname[,$aValues=''][,$bFirstChecked=false][,$bLinebreaks=false]
				// ggf. zeitschaltung
				if ($aENV['cms_navi_onlinefromto']) {
					echo '&nbsp; |&nbsp; ';
					echo $oForm->onlineFromTo($key, $aENV['cms_navi_onlinefromto_dateselect']);
				}
			}
		?></p></td>
	</tr>
<?php	} // END foreach ?>
	
<?php
if (isset($aData['template'])) { // EDIT-mode /////////////////////////////////////////////////////////////
// (template wurde schon ausgewaehlt und ist nicht mehr veraenderbar) ---------------------------------- ?>
	
<?php if ($aData['parent_id'] == 0 || ($aData['parent_id'] != 0 && !in_array($oAdmin->getParentTemplate($aData), $aENV['no_sub_templates']))) {
		// "in navi zeigen"-radiobuttons: NICHT anzeigen wenn parent den userdefined-"no-sub-templates" angehoert  ?>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['navi']['use_on_navi'][$syslang]; ?></b></p></td>
		<td><p><?php echo $oForm->radio('flag_navi'); ?></p></td>
	</tr>
<?php	if ($aData['flag_navi'] == 0 && $aData['parent_id'] != 0) {
		// NICHT zeigen: bei einem hauptpunkt + bei nicht fuer navi freigegeben ?>
	<tr>
		<td><p><?php echo $aMSG['navi']['highlight_subsection'][$syslang]; ?>:</p></td>
		<td><?php // reference_id select-field
		echo $oAdmin->getAssignReferenceSelect($aData); // params: &$aData[,$maxLevel=2] ?>
		</td>
	</tr>
<?php	} // END NICHT zeigen ?>
<?php } else { ?>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['navi']['use_on_navi'][$syslang]; ?></b></p></td>
		<td><p><b><?php echo $aMSG['form']['no'][$syslang]; ?></b> <small><br /><input type="hidden" name="aData[flag_navi]" value="0">
			<?php echo $aMSG['navi']['innavi_notallowed'][$syslang]; ?> 
			"<?php echo $aMSG['template']['standard'][$syslang]; ?>", 
			"<?php echo $aMSG['template']['no_page'][$syslang]; ?>" & 
			"<?php echo $aMSG['template']['static_page'][$syslang]; ?>".</small></p></td>
	</tr>
<?php } // end if in_array ?>

	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	
	<tr valign="top">
		<td height="27"><p><b><?php echo $aMSG['navi']['page_type'][$syslang]; ?></b></p></td>
		<td><input type="hidden" name="aData[template]" value="<?php echo $aData['template']; ?>">
			<span class="text"><b><?php
		// template-name ausgabe
			echo $oAdmin->getTemplateName($aData); // params: &$aData
		// specials
			if ($aData['template'] == 'static_page') {
				echo '<span class="small"><br />[ '.$aMSG['navi']['needs_include'][$syslang].' <b>static.'.$aData['filename_'.$aENV['content_language']['default']].'.php</b>]</span>';
			} ?></b></span><br /><?php
			if ($aData['template'] == 'no_page') { ?>
				<span class="small">&nbsp;&nbsp;&nbsp;&nbsp;A) <?php echo $aMSG['navi']['go_to'][$syslang]; ?>&nbsp;&nbsp;</span>
				<input type="text" name="aData[link]" value="<?php echo $aData['link']; ?>" size="35" maxlength="150" title="<?php echo $aMSG['navi']['link_tip'][$syslang]; ?>">
				<span class="small"> <?php echo $aMSG['navi']['or'][$syslang]; ?></span><br />
				<span class="small">&nbsp;&nbsp;&nbsp;&nbsp;B)</span>
				<input type="checkbox" name="first_sub" id="link_first_sub" value="1"<?php if ($first_sub == 1) echo ' checked'; ?> class="radiocheckbox">
				<label for="link_first_sub" class="small">&nbsp;<?php echo $aMSG['navi']['first_sub'][$syslang]; ?></label><br /><?php 
			}
		// LAYOUT: hier ggf. andere unterscheidungen (layout = A oder B ...) [aus der "setup.php"]
			if (isset($aENV['layout'][$aData['template']])) {
				foreach($aENV['layout'][$aData['template']] as $key => $val) { ?>
			<input type="radio" name="aData[layout]" value="<?php echo $key ?>" id="layout_<?php echo $key ?>"<?php if ($aData['layout'] == $key) echo ' checked'; ?> class="radiocheckbox">
			<label for="layout_<?php echo $key ?>" class="small"><b><?php echo (is_array($val)) ? $val[$syslang] : $val; // mehrsprachig = moeglich! ?></b></label><br />
<?php			} // END foreach
			} // END if isset ?>
		</td>
	</tr>
<?php 
} else { // NEW-mode /////////////////////////////////////////////////////////////////////////////////////////
	// (template muss gewaehlt werden) -------------------------------------------------------------------- ?>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['navi']['page_type'][$syslang]; ?></b></p></td>
		<td><input type="radio" name="aData[template]" value="standard" id="template_standard" checked class="radiocheckbox">
			<label for="template_standard" class="small"><b><?php echo $aMSG['template']['standard'][$syslang]; ?></b></label><br />
			<?php 
				foreach ($aENV['template'] as $key => $val) { // hole alle in der "_include_all.php" angegebenen templates ?>
					<input type="radio" name="aData[template]" value="<?php echo $key ?>" id="template_<?php echo $key ?>" class="radiocheckbox">
					<label for="template_<?php echo $key ?>" class="small"><b><?php echo $val[$syslang] ?></b></label><br />
			<?php 
				} // END foreach 
			?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['navi']['special_pages'][$syslang]; ?></b></p></td>
		<td>
<?php	
		// es darf nur EINE "home"-page existieren!!!
		$oDb->query("SELECT id FROM ".$sTable." WHERE template='home'");
		if ($oDb->num_rows() == 0) {  ?>
			<input type="radio" name="aData[template]" value="home" id="template_home" class="radiocheckbox">
			<label for="template_home" class="small"><b><?php echo $aMSG['template']['home'][$syslang]; ?></b></label><br />
			<?php echo HR; ?>
<?php	} // END if  ?>
			
			<input type="radio" name="aData[template]" value="static_page" id="template_static" class="radiocheckbox">
			<label for="template_static" class="small"><b><?php echo $aMSG['template']['static_page'][$syslang]; ?></b></label><br />
			<?php echo HR; ?>
			
			<input type="radio" name="aData[template]" value="no_page" id="template_nopage" class="radiocheckbox">
			<label for="template_nopage" class="small"><b><?php echo $aMSG['template']['no_page'][$syslang]; ?></b></label><br />
				<span class="small">&nbsp;&nbsp;&nbsp;&nbsp;A) <?php echo $aMSG['navi']['go_to'][$syslang]; ?>&nbsp;&nbsp;</span>
				<input type="text" name="aData[link]" value="<?php echo $aData['link']; ?>" size="35" maxlength="150"><span class="small">&nbsp;<?php echo $aMSG['navi']['or'][$syslang]; ?></span><br />
				<span class="small">&nbsp;&nbsp;&nbsp;&nbsp;B)</span><input type="checkbox" name="first_sub" id="link_first_sub" value="1"<?php if ($first_sub==1) echo " checked"; ?> class="radiocheckbox">
				<label for="link_first_sub" class="small">&nbsp;<?php echo $aMSG['navi']['first_sub'][$syslang]; ?></label><br />
		</td>
	</tr>
<?php /////////////////////////////////////////////////////////////////////////////////////////////////
} // END mode -------------------------------------------------------------------------------------- ?>

<?php if ($aENV['pic_on_navi'] == 'all' || ($aENV['pic_on_navi'] == 'da' && $oPerm->isDaService())) { // ein bild einem navigationspunkt zuweisen koennen // design aspekt only ?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['image'][$syslang]; ?></b></p></td>
		<td>
		<?php if ($aENV['pic_on_navi_type'] == 'txt') echo $oForm->textfield('image'); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		<?php if ($aENV['pic_on_navi_type'] == 'mdb') echo $oForm->media_tools('image', 'all'); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['image'][$syslang].' Highlight'; ?></b></p></td>
		<td>
		<?php if ($aENV['pic_on_navi_type'] == 'txt') echo $oForm->textfield('image_hi'); // params: $sFieldname[,$nMaxlength=150][,$nSize=57][,$sDefault=''][,$sExtra=''] ?>
		<?php if ($aENV['pic_on_navi_type'] == 'mdb') echo $oForm->media_tools('image_hi', 'all'); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>
<?php } ?>

<?php // CUG 
if (is_object($oCug)) { 
?>
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>
	<tr valign="top">
		<td><p><b><?php echo $aMSG['form']['navi_cug'][$syslang]; ?></b></p></td>
		<td>
		<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr valign="top">
			<td>
		<?php	if ($aData['template'] == 'home') { // die "home"-page darf nicht zu einer CUG gemacht werden!!! ?>
			<input type="checkbox" name="no_cug" value="1" class="radiocheckbox" disabled checked><span class="text"><?php echo $aMSG['checkbox']['no_cug'][$syslang]; ?></span><br />
		<?php	} else { ?>
			<input type="checkbox" name="no_cug" value="1" class="radiocheckbox" disabled<?php 
			if ($mode_key == 'new' || empty($aData['cug'])) echo ' checked'; ?>><span class="text"><?php echo $aMSG['checkbox']['no_cug'][$syslang]; ?></span><br />
			<?php echo HR; ?>
			<input type="hidden" name="aData[cug]" value="">
			<?php 
				// CUG: alle Verknuepften Usergroups ermitteln...
				$aUgRelated		= $oCug->getUsergroup($aData['id'], $sTable); // params: $sRelId ,$sRelTable
				$aUgRelatedId	= array_keys($aUgRelated);

				// alle usergroups ausgeben
				$aUG = $oCug->getAllUsergroups(); // params: -
				
				foreach ($aUG as $ug_id => $ug_title) {
					$checked = (is_array($aUgRelatedId) && in_array($ug_id, $aUgRelatedId)) ? ' checked' : '';
					echo '<input type="checkbox" name="cug" value="'.$ug_id.'" class="radiocheckbox" '.$checked.' onclick="checkUg()">';
					echo '<span class="text">'.$ug_title.'<br /></span>'."\n";
				}
			?>
			<script language="JavaScript" type="text/javascript">
			function checkUg() {
				var str = '';
				var cug = document.forms['editForm'].elements['aData[cug]'];
				var cbx = document.forms['editForm'].elements['cug'];
				var no = document.forms['editForm'].elements['no_cug'];
				if (cbx.length == undefined) {
					// 1 checkbox (=string)
					no.checked = (cbx.checked == true) ? false : true;
					if (cbx.checked == true) {str = cbx.value;} // value fuers hidden-field
				} else {
					// mehrere checkboxen (=array)
					if (no.checked == true) {no.checked = false;}
					for (var i=0; i<cbx.length; i++){
						// registriere ob eine gecheckt ist
						if (cbx[i].checked == true) {
							str += ',' + cbx[i].value; // value fuers hidden-field
						}
					}
					strlen = str.length; // erstes komma entfernen
					if (strlen > 0) {str = str.substr(1, strlen);}
					// fallback: wenn KEINE gecheckt ist, checke 'no_cug'
					if (str == '' && no.checked == false) {no.checked = true;}
				}
				document.forms['editForm'].elements['aData[cug]'].value = str; // schreibe value(s) ins hidden-field
			}
			checkUg(); // onLoad...
			</script>
	<?php	} // END if != "home" ?>
					</td>
					<td align="right">
					<script language="JavaScript" type="text/javascript">
					function confirmJumpToUG(towhere) {
						var chk = window.confirm("<?php echo $aMSG['err']['is_data_saved'][$syslang]; ?>");
						if(chk==true){
							location.replace('<?php echo $aENV['path']['adr']['http']; ?>web_cug.php')
						}
					}
					</script>
					<a href="#" onclick="confirmJumpToUG()" title="<?php echo $aMSG['user']['edit_user_usergroup'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_edit.gif" alt="<?php echo $aMSG['user']['edit_user_usergroup'][$syslang]; ?>" class="btn"></a>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
<?php 
} // END CUG ?>

</table>
<br />
<?php echo $oForm->button('SAVE'); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->button('SAVE_CLOSE'); ?>
<?php
	if($oPerm->isDaService()) {
		?><input type="submit" name="btn[search_refresh]" value="<?php echo $aMSG['btn']['search_refresh'][$syslang]; ?>" class="but" /><?php
	}
if ($mode_key == 'edit') { echo $oForm->button('DELETE'); } ?>
<?php if ($mode_key == 'edit') { echo $oForm->button('CANCEL'); } ?>
<?php echo $oForm->end_tag(); ?>
<br /><br />

<?php require_once ($aENV['path']['sys']['unix'].'inc.sys_footer.php');?>