<?php // Hilfsfile fuer den flash-texteditor

require_once ("../sys/php/_include_all.php");

// bekommt aus Flash folgende Variablen per GET -> Beispiele...
# $_GET['vTable']	= "cms_standard";
# $_GET['vId']		= "16";
# $_GET['vField']	= "copytext_de";

// GET-vars abholen
	$vTable			= strip_tags($_GET['vTable']);
	$vId			= $_GET['vId'] + 0;
	$vField			= strip_tags($_GET['vField']);

// -> bei fehlenden Parametern: Handbremse ziehen!
	if (empty($vTable) || empty($vField)) exit;

// get Data
	$oDbXML =& new dbconnect($aENV['db']);
	$oDbXML->query("SELECT `".$vField."` FROM `".$vTable."` WHERE `id` = '".$vId."'");
	$aDataXML = $oDbXML->fetch_array();
	
	// Sonderzeichen- und Html-Tag-Behandlung damit Flash unbekannte Tags NICHT entfernt
	$sValue = prepareHtmlTagsForFlashEditor($aDataXML[$vField]);

	if(empty($sValue)
	&& $vId==99999) { 
			$sValue	= $aENV['clientsites']['default']['text'];
	}	

// print XML
?>
<text>
	<textfield txt_content="<?php echo stripForFlash($sValue); ?>" />
</text>