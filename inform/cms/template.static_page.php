<?php
/**
* template.static_page.php
*
* Adminseite fuer das nicht editierbare static_page-template
* -> 2sprachig und voll kopierbar!
*
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.2 / 2003-07-28 ('$navi_id' eingefuehrt + USER-rechte alle nach oben)
* #history	1.1 / 2003-06-17 (2sprachigkeit hinzugefuegt)
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './static_page.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
	$sGEToptions	= '';
// 2b. POST-params abholen
// 2c. Vars
// 2d. Texte
	$MSG = array();
	$MSG['text']['de'] = "Diese Seite ist nicht editierbar.";
	$MSG['text']['en'] = "This page is not editable!";

// 3. DB

// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['cms']['unix']."inc.cms_navi.php");
?>

<form>
<br>
<table width="100%" height="300" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td class="off"><p><?php echo $MSG['text'][$syslang]; ?></p></td>
	</tr>
</table>
</form>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
