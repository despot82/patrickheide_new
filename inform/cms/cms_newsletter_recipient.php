<?php
/**
* cms_newsletter_recipient.php
*
* Overviewpage: newsletter-recipient-uebersichtsseite
*
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	string	$show		[Suchkriterium]
* @param	string	$sTerm		[Suchbegriff]
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_newsletter_recipient.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");
	require_once($aENV['path']['global_service']['unix']."class.DbNavi.php");

// 2a. GET-params abholen
	$start	= (isset($_GET['start'])) ? $oFc->make_secure_int($_GET['start']) : '';
	$show	= (isset($_GET['show'])) ? $oFc->make_secure_string($_GET['show']) : 'all';
	$sTerm	= (isset($_GET['sTerm'])) ? $oFc->make_secure_string($_GET['sTerm']) : '';
// 2c. Vars:
	$sNewPage	= $aENV['SELF_PATH']."cms_newsletter_recipient_detail.php".$sGEToptions;	// fuer NEW-button
	$sEditPage	= $aENV['SELF_PATH']."cms_newsletter_recipient_detail.php";					// fuer EDIT-link
	$sTable		= "newsletter_recipient";
	$limit		= 20;	// Datensaetze pro Ausgabeseite einstellen
	// available language-versions
	$aAvailableLanguages = $aENV['content_language'];
	unset($aAvailableLanguages['default']); // delete "default"

// 3. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['sys']['unix']."inc.sys_no_content_navi.php");

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
?>

<?php echo $oForm->start_tag($aENV['PHP_SELF'], '', 'get'); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra=''] ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><span class="title">Newsletter <?php echo $aMSG['std']['recipient'][$syslang]; ?></span><span class="text"> <?php echo $aMSG['std']['orderby'][$syslang]; ?> E-Mail</span></td>
		<td align="right">
		<?php echo $oForm->button("NEW"); // params: $sType[,$sClass=''] ?>
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr valign="top">
		<td class="self"><span class="text"><b><?php echo $aMSG['form']['search'][$syslang]; ?></b></span>
		<input type="text" name="sTerm" value="<?php echo $sTerm; ?>" size="30">
		<select name="show" size="1" class="smallForm">
		<option value="all"<?php if ($show == "all" || !$show) {echo " selected";} ?>>-- <?php echo $aMSG['form']['selAll'][$syslang]; ?> --</option>
		<option value="aktuell"<?php if ($show == "aktuell" || !$show) {echo " selected";} ?>><?php echo $aMSG['form']['selReg'][$syslang]; ?></option>
		<option value="abgemeldet"<?php if ($show == "abgemeldet") {echo " selected";} ?>><?php echo $aMSG['form']['selUnreg'][$syslang]; ?></option>
		<option value="testempf"<?php if ($show == "testempf") {echo " selected";} ?>><?php echo $aMSG['form']['testRec'][$syslang]; ?></option>
		</select>
		<input type="submit" value="&raquo;" class="but">
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
	<tr>
		<th width="60%"><p><b>E-Mail</b></p></th>
		<th width="15%"><p><b>Format</b></p></th>
		<th width="20%"><p><b><?php echo $aMSG['form']['signOn'][$syslang]; ?></b></p></th>
		<th width="5%"><p>&nbsp;</p></th>
	</tr>
<?php
// 4. DB-action: select
	$sQuery = "SELECT id,email,format,flag_testempf,angemeldet_date,abgemeldet_flag,abgemeldet_date,letzte_nl_id FROM ".$sTable;
	if ($show == "abgemeldet")	{ $sQuery .= " WHERE abgemeldet_flag=1"; }
	if ($show == "aktuell")		{ $sQuery .= " WHERE abgemeldet_flag=0"; }
	if ($show == "testempf")	{ $sQuery .= " WHERE flag_testempf=1"; }
	if ($sTerm != '')			{
		if($show == "all" || $show == "") { $sQuery .= " WHERE email LIKE '%".$sTerm."%'"; }
		else { $sQuery .= " AND email LIKE '%".$sTerm."%'"; }
	}
	$sQuery .= " ORDER BY email ASC";
	
	$oDb->query($sQuery);
	$entries = floor($oDb->num_rows());	// Feststellen der Anzahl der verfuegbaren Datensaetze (noetig fuer DB-Navi!)
	if (empty($start)) {$start=0;}
	$oDb->query($sQuery." LIMIT $start,$limit");
	
	$oDBNavi = new DbNavi($Userdata,$aENV,$entries, $start, $limit);
	while ($aData = $oDb->fetch_array()) {
		$sGEToptions = "?id=".$aData['id']."&start=".$start."&show=".$show;
		$self = ($Userdata['id'] == $aData['id']) ? ' class="self"' : '';
?>
	<tr valign="top">
		<td><p><a href="<?php echo $sEditPage.$sGEToptions; ?>" title="editieren"><?php echo $aData['email']; ?></a><?php
			if ($aData['flag_testempf']) { echo " <small>(Testempfaenger)</small>"; } ?></p>
		</td>
		<td><p><?php echo $aData['format']; ?></p></td>
		<td><p><small><?php echo $oDate->date_mysql2trad($aData['angemeldet_date']);
			if ($aData['abgemeldet_flag']) {
				echo '<br>off: '.$oDate->date_mysql2trad($aData['abgemeldet_date']).'<br>';
			} ?></small></p>
		</td>
		<td align="center"><p><?php
			$status = ($aData['abgemeldet_flag']) ? 0 : 1; // status aufbereiten
			$neu = ($aData['letzte_nl_id'] > 0) ? '1' : ''; // status aufbereiten
			// status-image
			echo get_status_img($status, 1, $sEditPage.$sGEToptions); // params: $status,$data,$href[,$extra][,$naviStatus]
			#foreach ($aAvailableLanguages as $key => $val) {
			#	echo get_status_img($status, 1, $sEditPage.$sGEToptions."&weblang=".$key)."\n"; // params: $status,$data,$href[,$extra][,$naviStatus]
			#}
		?></p></td>
	</tr>
<?php } // END while
	if ($entries == 0) { echo '<tr><td colspan="4" align="center"><p>'.$aMSG['std']['no_entries'][$syslang].'</p></td></tr>'; }
?>
</table>

<br>
<table width="100%" border="0" cellspacing="1" cellpadding="2">
	<tr>
		<td><p><?php // Anzahl gefundene Datensaetze anzeigen
			echo $oDBNavi->getDbnaviStatus(); // params: $nEntries[,$nStart=0][,$nLimit=20]
			?></p>
		</td>
		<td align="right"><p><?php // wenn es mehr gefundene Datensaetze als eingestelltes Limit gibt 
			echo $oDBNavi->getDbnaviLinks(); // params: $nEntries[,$nStart=0][,$nLimit=20][,$sGEToptions='']
			?></p>
		</td>
	</tr>
</table>
</form>
<br><br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
