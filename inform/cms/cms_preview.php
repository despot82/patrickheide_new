<?php
/**
* cms_preview.php
*
* preview-frameset-page for CMS!
* -> voll kopierbar!
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	1.01 / 2005-08-22 [BUGFIX: "top" in "topframe" umbenannt]
* #history	1.0 / 2005-06-14
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_preview.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// AUTH this page!
	if (!$oPerm->hasPriv('preview')) {
		header("Location: ".$aENV['page']['disallow']."?msg=userrights"); exit;
	}
// vars
	$page	= (isset($_GET['page'])) ? $oFc->make_secure_string($_GET['page']) : '';
	if (empty($page)) {
		$page = (isset($aENV['web_home']) && !empty($aENV['web_home'])) ? $aENV['web_home'] : 'home.php';
	}
	$page	.= '?preview=true';
	$sTopFrameParams = '';
	$id		= (isset($_GET['id'])) ? $oFc->make_secure_int($_GET['id']) : '';
	$sTable	= (isset($_GET['table'])) ? $oFc->make_secure_string($_GET['table']) : '';
	if (!empty($id)) {
		$page	.= '&id='.$id;
		if (!empty($sTable)) {
			$oDb->query("SELECT preview_ref_id FROM ".$sTable." WHERE id='".$id."'");
			$liveid = $oDb->fetch_field('preview_ref_id');
			if ($liveid == 0) { $liveid = $id; }
		}
		$sTopFrameParams .= '&id='.$id.'&liveid='.$liveid;
	}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
	<title><?php echo $aENV['client_name'].': '.$aENV['system_name'][$syslang].' - Preview'; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $aENV['charset']; ?>">
	<meta name="robots" content="noindex,nofollow">
	<!-- dieses frameset bekommt von der oeffnenden window.open() funktion den namen 'preview' zugewiesen! -->
</head>

<frameset rows="50,*" framespacing="0" frameborder="0" marginwidth="0" marginheight="0" border="0">

	<frame src="cms_preview_topframe.php?preview=true<?php echo $sTopFrameParams; ?>" name="topframe" id="topframe" frameborder="0" scrolling="No" noresize marginwidth="0" marginheight="0">
	<frame src="../../<?php echo $page; ?>" name="content" id="content" frameborder="0" scrolling="Yes" marginwidth="0" marginheight="0">
	
</frameset>

</html>