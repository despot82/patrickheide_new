<?php
/**
* popup_linkmanager.php
*
* Popup, um Texte mit (internen) Links zu versehen.
* -> 2sprachig und kopierbar 
* (NOTE: welche subsub-pages angezeigt werden, ist in der setup.php definiert).
*
* Diese Seite wird aus der JS-selectLinkmanagerWin(formfield) aufgerufen
*
* @param	int		$formfield	[um zu wissen in welches Feld uebertragen werden soll] (required)
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	3.01 / 2005-04-14 (BUGFIX: $weblang in "_getFormattedXmlData()")
* #history	3.0 / 2004-10-29 (auf XML umgebaut!)
* #history	2.2 / 2004-08-02 (Navi-Klasse eingebaut)
* #history	2.1 / 2004-06-02 (texte ausgelagert)
* #history	2.0 / 2004-05-04 (new_intranet)
* #history	1.2 / 2004-04-07 (JS: forms[0] in forms['editForm'] geaendert)
* #history	1.11 / 2004-01-22 (logo rausgenommen / header verkuerzt)
* #history	1.1 / 2003-07-17 (2sprachigkeit hinzugefuegt, int.-linkman automatisiert und kommentar verbessert)
* #history	1.0
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './popup_linkmanager.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ('../sys/php/_include_all.php');
	// login!
	require_once($aENV['path']['sys']['unix'].'inc.sys_login.php');

// 2a. GET-params abholen
	$formfield	= (isset($_GET['formfield'])) ? $oFc->make_secure_string($_GET['formfield']) : '';
// 2b. POST-params abholen
// 2c. Vars

// 3. HTML
	require_once ($aENV['path']['sys']['unix'].'inc.sys_header.php');
?>

<script language="JavaScript" type="text/javascript">
// puts value into input field of opener page
function put_value(val) {
	<?php if ($formfield) { ?>
	opener.document.forms['editForm']['aData[<?php echo $formfield; ?>]'].value = val;
	opener.focus();
	self.close();
	<?php } else { ?>
	alert('<?php echo $aMSG['linkmanager']['alert'][$syslang]; ?>')
	<?php } ?>
}
// Vorlader-Funktion fuer Icon-tausch
var icons = new Array();
icons["hide"] = new Image();
icons["hide"].src = "<?php echo $aENV['path']['pix']['http']; ?>btn_hide.gif";
icons["show"] = new Image();
icons["show"].src = "<?php echo $aENV['path']['pix']['http']; ?>btn_show.gif";
</script>
<style>
.text { line-height:16px }
</style>

<div id="contentPopup">
<span class="title"><?php echo $aMSG['linkmanager']['internal_link'][$syslang]; ?></span>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="6" alt="" border="0"><br>

<div class="text">
<?php // XML-Struktur
	
// init XML parser
	require_once($aENV['path']['php']['unix'].'PEAR/PEAR.php');
	require_once($aENV['path']['php']['unix'].'PEAR/XML/Parser.php');
	require_once($aENV['path']['php']['unix'].'PEAR/XML/Unserializer.php');
	$options = array('parseAttributes' => true);
	$unserializer = &new XML_Unserializer($options);
// get xml into array
	$xml =& _getFormattedXmlData();
	$status = $unserializer->unserialize($xml);
	if (PEAR::isError($status)) {
		echo 'Error: '.$status->getMessage();
	} else {
		$data = $unserializer->getUnserializedData();
		$data = $data['node']; // eine Ebene tiefer einsteigen (root ueberspringen)!
		#print_r($data);
// HTML OUTPUT
		_print_structure($data);
	}

// HILFSFUNKTIONEN ////////////////////////////////////////////////////////////////////////////
	
// schreibe die komplette struktur rekursiv als html-links mit dhtml und icons
	function _print_structure(&$data, $key=0, $depth=0, $id=0) {
		global $aENV;
		// umschliessender layer ist nicht noetig im level 0 und 1, da immer aufgeklappt
		if ($depth > 1) { // alle tieferen level erst mal geschlossen
			echo '<div id="nLayer_'.$id.'" style="display:none">'."\n";
		}
		// neue id erzeugen
		if ($depth > 0) { 
			$id_rand = uniqid($depth+$key);
		}
		
		// ebene durchlaufen
		if (!is_array($data[0])) { $data = array(0 => $data); } // wenn nur ein eintrag, dann ist das array zu flach!!!
		foreach ($data as $key => $val) {
			// divider fuer level 1 (drueber)
			if ($depth == 1) {
				echo '<div class="hDivider"><img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"></div>'."\n";
			}
			if ($depth == 0) {
				echo '<br>'."\n"; // spacer
				echo shorten_text($val['label'], 50).'<br>'."\n"; // root-nodes -> nur Titel!
			} else {
				$id = $id_rand.$key; // id wirklich "unique" machen
				// Icon
				$mode = (is_array($val['node'])) ? 'folder' : 'file';
				echo _getIcon($depth, $id, $mode);
				// Link
				echo '<a href="javascript:put_value(\''.$val['intLink'].'\')" onFocus="this.blur()">'.shorten_text($val['label'], 50).'</a><br>'."\n";
			}
			// rekursiv weiterhangeln
			if (is_array($val['node'])) { _print_structure($val['node'], $key, ++$depth, $id); --$depth; }
			
			// divider fuer level 0 (drunter)
			if ($depth == 0) {
				echo '<div class="hDivider"><img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="1" height="1" alt="" border="0"></div>'."\n";
			}
		}
		// umschliessenden layer schliessen
		if ($depth > 1) {
			echo '</div>'."\n";
		}
	}
// sub von "_write_structure()": gebe das richtige icon mit der richtigen einrueckung zurueck
	function _getIcon($depth, $id, $mode) {
		global $aENV, $aMSG, $syslang;
		// einrueckung
		$spacer_width = 0;
		for ($i = 1; $i < $depth; $i++) { $spacer_width += 17; }
		$spacer = ($spacer_width > 0) ? '<img src="'.$aENV['path']['pix']['http'].'onepix.gif" width="'.$spacer_width.'" height="1" alt="" border="0">' : '';
		// icon
		$icon['file']	= '<img src="'.$aENV['path']['pix']['http'].'btn_here.gif" alt="" class="btnsmall">';
		$icon['folder']	= '<a href="javascript:sublayerSwitch(\'nLayer_'.$id.'\')" onClick="toggleImage(\'nImg_'.$id.'\',\'internal\');">';
		$icon['folder']	.= '<img src="'.$aENV['path']['pix']['http'].'btn_show.gif" name="nImg_'.$id.'" alt="'.$aMSG['linkmanager']['show_subpages'][$syslang].'" class="btnsmall"></a>';
		// output
		return $spacer.$icon[$mode];
	}
// lese das fuer flash codierte XML aus und gebe es "gesaeubert" als string zurueck
	function _getFormattedXmlData() {
		// vars
		global $aENV, $weblang;
		$file = $aENV['path']['xml']['unix'].'/link_structure_'.$weblang.'.xml';
		// xml file auslesen und zwischenspeichern
		$buffer = '';
		if (!($fp = fopen($file, 'r'))) { die('could not open XML file'); }
		while ($data = fread($fp, 4096)) { $buffer .= $data; }
		fclose($fp);
		// daten sind speziell fuer flash aufbereitet, also reformatieren:
		$buffer = $buffer; 
		$buffer = str_replace('&', '&amp;', $buffer);
		// output
		return $buffer;
	}
?>
</div>
<br>
</div>

</body>
</html>