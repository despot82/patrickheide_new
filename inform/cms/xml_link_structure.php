<?php // Hilfsfile fuer den flash-texteditor

// bekommt aus Flash folgende Variablen per GET -> Beispiele...
# $contentLang = "de";	// [{empty}|de|en]
# $timestamp = "0";		// fake! wird fuer nichts benoetigt, ausser dass der browser das file nicht cached

#require_once ("../sys/php/_include_all.php");

$xml_file = (isset($_GET['contentLang']) && !empty($_GET['contentLang'])) 
	? "../../data/cms/xml/link_structure_".$_GET['contentLang'].".xml" 
	: "../../data/cms/xml/link_structure.xml";

if (file_exists($xml_file)) {
	//  Turn off caching:
	header("Expires: Sun, 12 Sep 1976 12:15:00 CST");
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache");
	// load static xml file
	require_once($xml_file);
}
?>