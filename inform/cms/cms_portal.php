<?php
/**
* portal.php
*
* CMS-welcome-page: //-> 2sprachig und (unter vorbehalt!) kopierbar!
*
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$navi_id	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aMSG		-> kommt aus der 'php/array.messages.php'
* @param	array	$aPageInfo	-> kommt aus der 'inc.cms_navi.php' (hier noch nicht benutzt)
*
* @author	Andy Fehn <af@design-aspekt.com>
* @version	2.0 / 2004-05-04 (new_intranet)
* #history	1.0 / 2003-06-03
*/

// existiert eine fuer Kunden angepasste Version dieser Seite?
	$custompage = './cms_portal.custom.php';
	if (file_exists($custompage)) {
		include($custompage); exit;
	}

// 1. init
	require_once ("../sys/php/_include_all.php");
	// login!
	require_once($aENV['path']['sys']['unix']."inc.sys_login.php");

// 2a. GET-params abholen
// 2b. POST-params abholen
// 2c. Vars
	$oDb2 =& new dbconnect($aENV['db']);
	$limit = 10; // Anzahl  geaenderter Datensaetze, die angezeigt werden sollen
	// reset
	$navi_id = 0;
	$oSess->unset_var('navi_id');
	
// 3. DB
	// titel + template aus navi-table in tmp-array speichern (performance!)
	$aNaviTmp = array();
	$oDb->query("SELECT id, parent_id, title_".$weblang." AS navi_title FROM ".$aENV['table']['cms_navi']);
	while ($aData = $oDb->fetch_array()) {
		$aNaviTmp[$aData['id']]['navi_title'] = $aData['navi_title'];
		$aNaviTmp[$aData['id']]['parent_id'] = $aData['parent_id'];
	}

// USER init
	if (!is_object($oUser)) {
		$oUser =& new user($oDb2); // params: &$oDb[,$aConfig=NULL)
	}
	
// 4. HTML
	require_once ($aENV['path']['sys']['unix']."inc.sys_header.php");
	require_once ($aENV['path']['cms']['unix']."inc.cms_navi.php");

// HILFSFUNKTIONEN /////////////////////////////////////////////////////////
	function _getEditPageLink($sTemplate, $nNaviId, $sId='', $sLang='') {
		global $aENV;
		$page		= $sTemplate;
		$getvars	= '?navi_id='.$nNaviId;
		if ($sTemplate != 'standard' && $sTemplate != 'home' && $sTemplate != 'form' && $sTemplate != 'search' && $sTemplate != 'static_page') {
			$page		.= '_detail';
			$getvars	.= '&id='.$sId;
		} else {
			$getvars	.= '&showEditBlock=1'; // editBlock-Layer einblenden (bei overview-page)
		}
		if (!empty($sLang)) {
			$getvars	.= '&weblang='.$sLang;
		}
		return $aENV['SELF_PATH'].'template.'.$page.'.php'.$getvars;
	}
	function _getPath($nNaviId, $sNaviTitle) {
		global $aENV, $aNaviTmp;
		$path		= $sNaviTitle;
		$parent_id	= $aNaviTmp[$nNaviId]['parent_id'];
		if ($parent_id > 0) {
			$path	= $aNaviTmp[$parent_id]['navi_title'].' / '.$path;
			$grandparent_id	= $aNaviTmp[$parent_id]['parent_id'];
			if ($grandparent_id > 0) {
				$path	= $aNaviTmp[$grandparent_id]['navi_title'].' / '.$path;
			}
		}
		return $path;
	}
	// in process
	if ($aENV['preview'] && file_exists("./inc.template.recent_previews.php")) {
		require_once ($aENV['path']['cms']['unix']."inc.template.recent_previews.php");
	}
	// not online
	if (file_exists("./inc.template.notonline.php")) {
		require_once ($aENV['path']['cms']['unix']."inc.template.notonline.php");
	}
?>

<form>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aMSG['portal']['recent_changes'][$syslang]; ?></span><br></p></td>
		<td></td>
	</tr>
</table>
<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"><br>

<?php // LAST MODIFIED DATA
	echo '<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">'."\n";
	// select: die 10 zuletzt geaenderten datensaetze
	$oDb->query("SELECT ref_id,title, ref_table, last_modified, lang, searchtext 
				FROM ".$aENV['table']['sys_global_search']." 
				WHERE module='cms'
				ORDER BY last_modified DESC 
				LIMIT 0, ".$limit);
	while ($aData = $oDb->fetch_array()) {
	// subquery
		$oDb2->query("SELECT t.navi_id, t.last_modified, t.last_mod_by, t.created_by, n.template, n.title_".$weblang." AS navi_title 
					FROM ".$aData['ref_table']." t, ".$aENV['table']['cms_navi']." n 
					WHERE t.id = '".$aData['ref_id']."'
						AND t.navi_id = n.id");
		if($oDb2->num_rows() == 0) continue;
		$aData2 = $oDb2->fetch_array();
		// page title
		$page_title	= $aData['title'];
		$page_title	= ($page_title == "") ? $aMSG['err']['notitle'][$syslang] : $page_title;
		// last_mod_by / created_by (fallback)
		$aData2['last_mod_by'] = (empty($aData2['last_mod_by'])) ? $aData2['created_by'] : $aData2['last_mod_by'];
		// HTML
		echo '<tr valign="top">';
		echo '<td width="1%" class="lang'.$aData['lang'].'"><p><small>&nbsp;</small></p></td>';
		echo '<td width="60%"><p>';
		echo '	<a href="'._getEditPageLink($aData2['template'], $aData2['navi_id'], $aData['ref_id'], $aData['lang']).'"><b>'.$page_title.'</b><br>';
		echo '	'._getPath($aData2['navi_id'], $aData2['navi_title']).'</a>';
		echo '</p></td>';
		if (count($aENV['content_language']) > 2) {
			echo '<td width="10%"><p>'.$aData['lang'].'</p></td>';
		}
		echo '<td width="25%" class="sub2"><p>';
		echo 	$oUser->getUserName($aData2['last_mod_by']); // params: $userid
		echo '	<br><small>';
		echo 	$oDate->date_from_mysql_timestamp($aData['last_modified']);
		echo ' ['.$oDate->time_from_mysql_timestamp($aData['last_modified']).']';
		echo '</small></p></td>';
		echo '</tr>'."\n";
	}
	echo '</table>'."\n";
?>

</form>
<br>

<?php require_once ($aENV['path']['sys']['unix']."inc.sys_footer.php"); ?>
