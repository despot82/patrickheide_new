<?php
/**
* inc.template.standard_block.php
*
* Include: standard-Datensatz edit-seite fuer Listen-Templates
*
* @param	int		$navi_id	welcher Navigationspunkt + zum richtigen zurueckspringen
* @param	int		$start		[zum richtigen zurueckspringen] (optional)
* @param	array	Formular:	$aData
* @param	array	Formular:	$btn
* @param	string	$weblang	-> kommt aus der 'inc.sys_login.php'
* @param	string	$syslang	-> kommt aus der 'inc.sys_login.php'
* @param	array	$aENV		-> kommt aus der 'config/setup.php'
* @param	string	$aMSG		-> kommt aus der 'php/array.*_messages.php'
* @param	array	$aPageInfo	-> kommt aus der 'inc.cms_navi.php'
*/

// 2a. GET-params abholen
	$showEditBlock	= (isset($_GET['showEditBlock']) && $_GET['showEditBlock'] == 1) ? 1 : 0;
// 2b. POST-params abholen
	$aData			= (isset($_POST['aData'])) ? $_POST['aData'] : array();
	$sWeblang		= (isset($_POST['weblang'])) ? $_POST['weblang'] : '';
	$btn			= (isset($_POST['btn'])) ? $_POST['btn'] : array();

// DB: standard-table

// SHARED BEGIN ****************************************************************************
	if (file_exists("./inc.cms_functions.php")) { require_once ("./inc.cms_functions.php"); }
// END SHARED *****************************************************************************

	// CMS-Content (+ Search) Object
	require_once($aENV['path']['global_module']['unix'].'class.cms_content_edit.php');
	$oCmsContent =& new cms_content_edit($oDb, $aENV['table']['cms_standard'], $aENV['preview']); // params: $oDb,$sTable[,$bWithPreview=false]
	// feld, das den flash-editor benutzt, definieren (default: KEIN Flasheditor)
	$oCmsContent->setFlashEditorField('copytext_'.$weblang); // params: $sFieldname
	// inhalte fuer search-table definieren
	$oCmsContent->setSearchLang($weblang); // params: $sLang
	$sSearchtitle = getSearchTitle('standard', $aData, $weblang);
	$oCmsContent->setSearchTitle($sSearchtitle);
	$sSearchtext = getSearchText('standard', $aData, $weblang);
	$oCmsContent->setSearchText($sSearchtext); // params: $sSearchtext
	// ggf. XML-Struktur updaten
	if (isset($aENV['create_xml_structure']) && $aENV['create_xml_structure'] == true) {
		$oCmsContent->setFunctionOnSave('write_xml_structure'); // params: $sName[,args]
	}
	
	if (isset($btn['save']) || $remoteSave == 1 || isset($btn['accept_changes']) || isset($btn['discard_changes'])) {
		// editBlock-Layer nach "save" eingeblendet lassen
		if ($sGEToptions == '') $sGEToptions = '?';
		$sGEToptions .= '&showEditBlock=1';
	}
	// DB-action: save // Update eines bestehenden Eintrags
	if ( (isset($btn['save']) || $remoteSave == 1) && $aData['id'] && !isset($alert)) {
		// make update
		$aData['weblang'] = $sWeblang;
		$aData = $oCmsContent->save($aData); // params: $aData
	}
	if (isset($btn['accept_changes'])) {
		$oCmsContent->bReloadAfterSave = false;
		$aData['weblang'] = $sWeblang;
		$aData = $oCmsContent->save($aData); // params: $aData
		$oCmsContent->bReloadAfterSave = true;
		// Live-Datensatz mit Preview-Datensatz ueberschreiben; Preview-Datensatz loeschen
		$aData = $oCmsContent->accept($aData); // params: $aData
	}
	if (isset($btn['discard_changes'])) {
		// Preview-Datensatz loeschen + ID des Live Datensatzes zurueckgeben
		$aData['id'] = $oCmsContent->restore($aData); // params: $aData
	}
	// DB-action: select
	$aData			= $oCmsContent->selectStandard($navi_id, $aData['id']); // params: $navi_id,$id=NULL
	$mode_key		= $oCmsContent->getModeKey(); // [new|edit]
	$online_status	= $oCmsContent->getStatus(); // [live|preview]
	
	// ggf. edit-recht entziehen!
	if (!$oPerm->hasPriv('publish')) {
		$oPerm->removePriv('edit'); // params: $sPrivilege[,$sModule='']
	}

// FORM
	$oForm =& new form_admin($aData, $syslang); // params: $aData[,$sLang='de']
	$oForm->set_cms_vars('standard', $navi_id, $aENV['preview']); // params: $sTemplate[,$navi_id=NULL][,$bWithPreview=true]
	#$oForm->check_field('title_'.$weblang, $aMSG['form']['title'][$syslang]); // params: $sFieldname[,$sAlertName=''][,$sCheck='FILLED']
?>

<script language="JavaScript" type="text/javascript">
	var icons = new Array(); // vorlader
	icons["hide"] = new Image();
	icons["hide"].src = "../sys/pix/btn_overvw_close.gif";
	icons["show"] = new Image();
	icons["show"].src = "../sys/pix/btn_overvw_edit.gif";
</script>

<?php	// JavaScripts + oeffnendes Form-Tag
echo $oForm->start_tag($aENV['PHP_SELF'], $sGEToptions); // params: [$sAction=''][,$sUrlParams=''][,$sMethod='post'][,$sName='editForm'][,$sExtra='']
echo $oForm->default_hidden_fields(); // params: - 
#echo $oForm->hidden('title_'.$weblang, $aPageInfo['title_'.$weblang]);	// für searchtext aktivieren, falls kein title-feld verwendet wird
?>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr valign="top">
		<td><p><span class="title"><?php echo $aPageInfo['title_'.$weblang]; ?></span>
			<?php if ($online_status == 'preview') echo '<br>'.$aMSG['mode']['preview_status'][$syslang]; ?></p>
		</td>
		<td align="right">
			<?php if ($aENV['preview'] == true) echo $oForm->button('PREVIEW'); // params: $sType[,$sClass=''] ?><a 
			href="javascript:sublayerSwitch('editBlock')" onClick="toggleImage('btnEditBlock','startpageBlock');" title="<?php echo $aMSG['cms']['editintro'][$syslang]; ?>"><img src="<?php echo $aENV['path']['pix']['http']; ?>btn_overvw_edit.gif" name="btnEditBlock" alt="<?php echo $aMSG['cms']['editintro'][$syslang]; ?>" class="btn"></a>
		</td>
	</tr>
</table>

<img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"><br>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<colgroup><col width="25%"><col width="75%"></colgroup>
	<?php if (count($aENV['content_language']) > 2) { // more than one language (+default) ?>
	<tr>
		<td class="lang<?php echo $weblang; ?>"><p><b><?php echo $aMSG['form']['language'][$syslang]; ?></b></p></td>
		<td>
		<?php echo $oForm->lang_dropdown(); // params: [$size=1] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="3" alt="" border="0"></td></tr>	
	
	<?php } ?>
</table>

<div id="editBlock" style="visibility:visible;display:none">
<table width="100%" border="0" cellspacing="1" cellpadding="2" class="tabelle">
<colgroup><col width="25%"><col width="75%"></colgroup>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['title'][$syslang]; ?><br><small>[max. 250 <?php echo $aMSG['std']['characters'][$syslang]; ?>]</small></p></td>
		<td>
		<?php echo $oForm->textfield('title_'.$weblang, 250, 75); // params: $sFieldname[,$nMaxlength=150][,$nSize=57] ?>
		</td>
	</tr>
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>"><p><?php echo $aMSG['form']['text'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea_flash('copytext_'.$weblang, $aENV['table']['cms_standard'], $aData['id']); // params: $sFieldname, $sTable, $id ?>
		</td>
	</tr>
	<!--
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['picture'][$syslang]; ?><br><small>[432 x 432 pixel]</small></p></td>
		<td>
		<?php echo $oForm->media_tools('img_id', 'image', 432, 432); // params: $sFieldname[,$sMediaType='all'][,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>
	-->
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['images'][$syslang]; ?><br><small>[432 x 432 pixel]</small></p></td>
		<td>
		<?php echo $oForm->slideshow_tools('slideshow', $aENV['table']['cms_standard'], $aData['id'], 432, 432); // params: $sFieldname,$sTable,$sId[,$nWidth=''][,$nHeight=''][,$bMustfit=0] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>	
	
	<tr valign="top">
		<td><p><?php echo $aMSG['form']['keywords'][$syslang]; ?></p></td>
		<td>
		<?php echo $oForm->textarea("keywords", 2,75); //  params: $sFieldname[,$nRows=10][,$nCols=43][,$sDefault=''][,$sExtra=''] ?>
		</td>
	</tr>
	
	<tr><td colspan="2" class="off"><img src="<?php echo $aENV['path']['pix']['http']; ?>onepix.gif" width="1" height="5" alt="" border="0"></td></tr>

<!-- NAVI STATUS -->
	<tr valign="top">
		<td class="lang<?php echo $weblang; ?>" height="24"><p><b><?php echo $aMSG['form']['use_on_website'][$syslang]; ?></b></p></td>
		<td><p style="float:left;"><?php 
		if (is_array($aPageInfo)) {
			// flag_online
			echo ($aPageInfo['flag_online_'.$weblang] == 1) ? $aMSG['std']['yes'][$syslang] : $aMSG['std']['no'][$syslang];
			// flag_navi
			if ($aPageInfo['flag_navi'] == 0) echo $aMSG['navi']['dont_use_on_navi'][$syslang];
			// reference_id (highlight)
			if ($aPageInfo['reference_id'] > 0) {
				$oDb->query("SELECT title_".$weblang." AS title FROM ".$aENV['table']['cms_navi']." WHERE id='".$aPageInfo['reference_id']."'");
				if ($oDb->num_rows() > 0) {
					echo ' '.$aMSG['navi']['highlight_subsection'][$syslang].': "';
					echo $oDb->fetch_field('title').'"';
				}
			}
		} ?>
		</p>
<?php if ($oPerm->hasPriv('publish')) { ?>
		<div style="float:right;"><?php 
			echo '<a href="'.$aENV['SELF_PATH'].'cms_navi_detail.php?id='.$navi_id.'" title="'.$aMSG['form']['edit_structure_page'][$syslang].'">';
			echo '<img src="'.$aENV['path']['pix']['http'].'btn_edit.gif" alt="'.$aMSG['form']['edit_structure_page'][$syslang].'" class="btn"></a>';
		?></div>
<?php } ?>
		</td>
	</tr>
<?php if (isset($aENV['allow_cug']) && $aENV['allow_cug'] == true) { // CUG ?>
	<tr valign="top">
		<td height="24"><p><b><?php echo $aMSG['form']['usergroup'][$syslang] ?></b></p></td>
		<td><p><?php
		require_once($aENV['path']['global_service']['unix']."class.cug.php");
		$oCug =& new cug($aENV['db'], $syslang);
		$aUgRelated = $oCug->getUsergroup($aData['navi_id'], $aENV['table']['cms_navi']); // params: $sRelId ,$sRelTable
		$sUgNames = implode(', CUG ', array_values($aUgRelated));
		echo (empty($sUgNames)) ? $aMSG['std']['all'][$syslang] : "CUG ".$sUgNames; ?></p>
		</td>
	</tr>
<?php } // END if CUG ?>
	
</table><br>
<?php echo $oForm->button('SAVE'); // params: $sType[,$sClass=''] ?>
<?php if ($online_status == 'preview') { 
		echo $oForm->button('ACCEPT_CHANGES');
		echo $oForm->button('DISCARD_CHANGES');
	} ?>
<?php echo $oForm->button('RESET'); // params: $sType[,$sClass=''] ?>
<?php echo $oForm->end_tag(); // params: - ?>
<br><br>
</div>
<?php if ($showEditBlock == 1) { // init ?>
<script language="JavaScript" type="text/javascript">
	sublayerSwitch('editBlock');
	toggleImage('btnEditBlock','startpageBlock');
</script>
<?php } ?>

<?php echo HR; ?>

