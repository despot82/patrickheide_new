<?php
/**
 * 
 * Template Name:Home
 * 
 * The template for page: Home
 *
 * @package impronta-child
 */
    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Patrick Heide - Home</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Patrick Heide" />
	<meta name="keywords" content="Patrick Heide Contemporary Art London Exhibitions Fairs Events" />
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<meta http-equiv="imagetoolbar" content="no" />

<!-- CSS -->
	<style type="text/css" title="currentStyle">/* <![CDATA[ */
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/styles.css";
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/large.css";
	/* ]]> */</style>
	<!--[if lt IE 7.]>
	<style type="text/css" title="currentStyle">
	@import "./web/styles/ie6.css";
	</style>
	<![endif]-->

<!-- JavaScript -->
	<script type="text/javascript" src="../lightwindow/javascript/slideshow.js"></script>
	<script type="text/javascript" src="../lightwindow/javascript/prototype.js"></script>
    <script type="text/javascript" src="../lightwindow/javascript/effects.js"></script>
    <script type="text/javascript" src="../lightwindow/javascript/prototype.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.12.1.min.js"></script>
    
	<script language="javascript" type="text/javascript"><!--//--><![CDATA[//><!--
	var currentPage = '/home_en.php';
	window.onresize = reloadPage;
	//--><!]]></script>

	<script type="text/vbscript"><!--//--><![CDATA[//><!--
	Function VBGetSwfVer(i)
		on error resume next
		Dim swControl, swVersion
		swVersion = 0

		set swControl = CreateObject("ShockwaveFlash.ShockwaveFlash." + CStr(i))
		if (IsObject(swControl)) then
			swVersion = swControl.GetVariable("$version")
		end if
		VBGetSwfVer = swVersion
	End Function
	//--><!]]></script>
</head>

<body>

<script type="text/javascript"><!--//--><![CDATA[//><!--
if (document.body.offsetWidth < 1245) {
	document.write('<link href="../../web/styles/small.css" rel="stylesheet" type="text/css" />\n');
}
//--><!]]></script>

<div id="layout">

	<div id="logo">
		<a href="index.php.html" title="Patrick Heide" onfocus="this.blur()">
			<img src="<?php  echo get_stylesheet_directory_uri();  ?>/images/PatrickHeide.gif" width="220" height="410" alt="Patrick Heide" title="Patrick Heide" />
		</a>
	</div>

<?php 
    include "sidebar-navi.php";	
?>

<div id="leftcolumn">
		
<?php 

    $announcements = get_page_by_title("Announcements");
	$announcements_content_raw = $announcements -> post_content;
	$announcements_content = str_replace("\n", "<br/>", $announcements_content_raw);
	echo $announcements_content;

?>	
		
</div>
	
	<div id="homecontent">
	
		<div id="slideshow">
			<script type="text/javascript"><!--//--><![CDATA[//><!-- 
			function displayHomepageImage(url) {

			var captionDiv = "captionDiv";
			
			// Version check based upon the values entered above in "Globals"
            var oTag = "<object data='<?php echo get_home_url(1); ?>/media/slideshow.swf?sPix=%2F" + url + "&amp;txt=true&amp;sHref=http://patrickheide.com/exhibition-Memories%20of%20an%20unrevealed%20disaster/' type='application/x-shockwave-flash' width='100%' height='100%' id='slideshow_flash'><param name='allowscriptaccess' value='samedomain' /><param name='movie' value='./web/pix/slideshow.swf?sPix=%2Fgp/media/bitter/9.jpg&amp;txt=true&amp;sHref=http://www.patrickheide.com/2016.php?id=217&year=0' /><param name='quality' value='high' /><param name='bgcolor' value='#ffffff' /><param name='scale' value='noscale' /><param name='salign' value='tl' /><param name='menu' value='false' /><param name='wmode' value='window' /></object>";

			function initSlideshow() {		
				//return;																																													
				aSlides = new Array();
				//aSlides[0] = url.substr(4, url.length - 4);

				
				//aSlides[0] = '/media/25_02_2016/without_descriptions/Atltitude.jpg';
				//aSlides[1] = '/media/bitter/10.jpg';
				//aSlides[2] = '/media/bitter/9.jpg';
				
				aSlideTxt = new Array();

                //alert(url);
                
                
				document.writeln('<a href="http://www.patrickheide.com/home"><img src="../' + url + '" id="slide" title="" alt="" /></a>');

				Slideshow = new slideshow(aSlides, "slide", "", 1, "true", "false", aSlideTxt, "captionDiv");	// params: aSlides,imgName[,layerID][,direction=1][,autostart="true"][,imgButtons="true"][,aSlideTxt][,captionDivID]]

			}
			
			if (oTag != "") {
				var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
				// Check to see if the version meets the requirements for playback
				if (hasReqestedVersion) {  // if we've detected an acceptable version
					embedFlash(oTag);   // write the relevant Object-Tag
				}
				else {  // flash is too old or we can't detect the plugin
					// JS-Slideshow Init
					initSlideshow();
				}
			}
			else {  // no Object-Tag defined
				// JS-Slideshow Init
				initSlideshow();
			}
			//--><!]]>
			}
			
			//displayHomepageImage("gp/media/bitter/6.jpg");
			
			<?php 
			
			
			if (substr(get_home_url(1), 0, 16) == "http://localhost") { 
		        $len = strlen(get_home_url(1)) - 2;
			} else { 
			    $len = strlen(get_home_url(1)) + 1;
			} ?>
			
			
			displayHomepageImage("<?php echo substr(wp_get_attachment_url(get_post_thumbnail_id($post->ID)), $len, strlen(wp_get_attachment_url(get_post_thumbnail_id($post->ID)))); ?>");
			
			</script>
			
		</div>		
	
	</div>
   
    <div class="captionDiv">
    
    </div>
    
    <div id="captionDiv">
    
    </div>
    
    
</div>

<script>

    $(".captionDiv").html("<?php echo preg_replace("/\r\n|\r|\n/","<br/>",$post -> post_content); ?>");

    $("#Home").addClass("hi");
        
</script>

</body>
</html>



