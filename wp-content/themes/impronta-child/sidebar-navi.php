<div id="navigation">
		<ul class="navi">
						<li>
				<a id="Home" onfocus="this.blur()" title="" href="../home">Home</a><br>
			</li>
			<li>
				<a id="Profile" onfocus="this.blur()" title="" href="../profile">Profile</a><br>
			</li>
			<li>
				<a id="Artists" onfocus="this.blur()" title="" href="../artists">Artists</a><br>
			</li>
			<li>
				<a id="Exhibitions" onfocus="this.blur()" title="" href="../exhibitions_2017">Exhibitions</a><br>
			</li>
			<li>
				<a id="FairsAndEvents" onfocus="this.blur()" title="" href="../fairsandevents_2017">Fairs &amp; Events</a><br>
			</li>
			<li> 
				<a id="Publications" onfocus="this.blur()" title="" href="../publications">Publications</a><br>
			</li>
			<li>
				<a id="Contact" onfocus="this.blur()" title="" href="../contact.php.html">Contact</a><br>
			</li>

		</ul>
	    <a id="facebook" class="social-media" target="_blank" href="http://www.facebook.com/PATRICKHEIDECONTEMPORARYART"></a><a id="twitter" class="social-media" target="_blank" href="https://twitter.com/PATRICKHEIDECA"></a>
</div>
