<?php 



$sql = "
SELECT menu_item_post.post_title 
FROM wp_terms AS t
LEFT JOIN wp_term_taxonomy AS tt ON(tt.term_id = t.term_id) #hook term to its taxonomy
LEFT JOIN wp_term_relationships AS tr ON(tr.term_taxonomy_id = tt.term_taxonomy_id) 
LEFT JOIN wp_posts AS menu_item_post ON(tr.object_id = menu_item_post.ID AND 
                                         menu_item_post.post_type = 'nav_menu_item')
JOIN wp_postmeta pag ON (pag.post_id = menu_item_post.id AND 
                         pag.meta_key='_menu_item_object_id')
WHERE 1 = 1 and pag.meta_value = " . $post -> ID . "
AND t.name = 'artists_menu' 
AND tt.taxonomy = 'nav_menu'
AND menu_item_post.post_status='publish'; ";

global $wpdb;

$result = $wpdb -> get_results($sql);

$artist_name = $post -> post_title;

//print "<pre>sql is:"; print_r($sql); print "</pre>";

if($section == "representing") {

?>

<ul class="navi">				
    <li>
		<a href="../artists" class="hi">Representing</a><br />
		
		<ul>

			<li><a href="../artists" title="Overview page"><img src="../web/pix/arrow_navi_return.gif" width="11" height="6" alt="Overview page" title="Overview page" /></a><br /></li>
			<li class="hi"><?php echo $artist_name; ?><br /></li>
			<li><a href="<?php echo $url; ?>?section=thumb" <?php if($_GET["section"] == "thumb") echo "class='on'"; ?>>Thumbnails</a><br /></li>
			<li><a href="<?php echo $url; ?>?section=about" <?php if($_GET["section"] == "about") echo "class='on'"; ?>>About this artist</a><br /></li>
			
		</ul>

    </li>
	<li>
		<a href="../artists_collaborating">Collaborating</a><br />
		

	</li>
	<li>
		<a href="../artists_breeder">Breeder</a><br />
		

	</li>
</ul>

<?php 
}

if($section == "collaborating") {

?>
		
<ul class="navi">				
    <li>
		<a href="../artists">Representing</a><br />

	</li>
	<li>
		<a href="../artists_collaborating" class="hi">Collaborating</a><br />
		
		<ul>

			<li><a href="../artists_collaborating" title="Overview page"><img src="../web/pix/arrow_navi_return.gif" width="11" height="6" alt="Overview page" title="Overview page" /></a><br /></li>
			<li class="hi"><?php echo $artist_name; ?><br /></li>
			<li><a href="<?php echo $url; ?>?section=thumb" <?php if($_GET["section"] == "thumb") echo "class='on'"; ?>>Thumbnails</a><br /></li>
			<li><a href="<?php echo $url; ?>?section=about" <?php if($_GET["section"] == "about") echo "class='on'"; ?>>About this artist</a><br /></li>
			
		</ul>

	</li>
	<li>
		<a href="../artists_breeder">Breeder</a><br />
		

	</li>
</ul>

<?php 
}

if($section == "breeder") {


?>

<ul class="navi">				
    <li>
		<a href="../artists">Representing</a><br />
					
    </li>
	<li>
	    <a href="../artists_collaborating">Collaborating</a><br />

    </li>
    <li>
		<a href="../artists_breeder" class="hi">Breeder</a><br />
					
		<ul>
	
			<li><a href="../artists_breeder" title="Overview page"><img src="../web/pix/arrow_navi_return.gif" width="11" height="6" alt="Overview page" title="Overview page" /></a><br /></li>
			<li class="hi"><?php echo $artist_name; ?><br /></li>
			<li><a href="<?php echo $url; ?>?section=thumb" <?php if($_GET["section"] == "thumb") echo "class='on'"; ?>>Thumbnails</a><br /></li>
			<li><a href="<?php echo $url; ?>?section=about" <?php if($_GET["section"] == "about") echo "class='on'"; ?>>About this artist</a><br /></li>
			
		</ul>

    </li>
</ul>


<?php 

}

?>

