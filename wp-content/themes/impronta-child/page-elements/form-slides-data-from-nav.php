<?php 


foreach ($nav_menu_items as $item) {
    	
    	if($item -> menu_item_parent == $top_menu_item -> ID) {
    		
    		$galleries = get_post_galleries($item -> object_id, false);
    		
    		//Extract first image from first gallery
    		$gallery = $galleries[0];
    		
    		$images_posts_ids = explode(",", $gallery["ids"]);
    		
    		//post_title, post_excerpt
    		$id = $images_posts_ids[0];
    		
    		$image_post = get_post($id);
    		
    		//print "<pre>image_post: "; print_r($image_post); print "</pre>";
    		
    		//Form the slideshow array later to be used in forming the oTag variable. If no galleries, add empty picture
    	    if(empty($galleries)) {
    			$slideshow_images_urls[] = "http://localhost/gp/empty.jpg";
    		} else {
    		
    		    $slideshow_images_urls[] = $image_post -> guid;
    		}
    		
    		//Form descriptions array with descriptions of all the images
    	    if(empty($galleries)) {
    			$descriptions[] = "This artist does not have an excerpt yet.";
    		} else {
    		    $descriptions[] = $image_post -> post_excerpt;
    		}
    		
    		//Form the array of ids that are also the id-s of artists' pages
    		$artists_ids[] = $item -> object_id;
    		
    		
    		//echo $item -> object_id;
    		
    		echo "<br/>";
    	}
    }
    
    
?>