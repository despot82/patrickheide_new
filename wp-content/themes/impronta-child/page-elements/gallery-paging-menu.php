<div id="paging">
<?php if (($gallery_index == 0) || ($gallery_index == 1) || ($gallery_index == -1) || empty($gallery_index)) {      ?>

	<img src="../web/pix/skip_back_off.gif" width="6" height="11" alt="" title="" />
<?php } else { ?>	
	<a href="<?php echo $url . "?section=gallery&pag=" . ($gallery_index - 1); ?>" title="previous Page">
	    <img src="../web/pix/skip_back_on.gif" width="6" height="11" alt="previous Page" title="previous Page" />
	</a>
<?php } ?>
	&nbsp;&nbsp;&nbsp;<span style="color:#000000"><?php if($gallery_index == 0) echo 1; else echo $gallery_index; ?></span>
	&nbsp;&nbsp;/&nbsp;&nbsp;<?php echo max(1,count($galleries) - 1); ?>&nbsp;&nbsp;&nbsp;
<?php 

/* 
 * In 3 cases there should be grayed out next button:
 * 1.The index of gallery is given and this index is the last gallery there is, so gallery_index = count(galleries)
 * 2.The gallery_index is zero, so it is a thumb view, and number of galleries is 2 or less, so there is no second gallery
 * 3.The count of galleries is 1, so there is only thumbview, thus - no second gallery
 * 
 */

?>	

<?php if($gallery_index == count($galleries) - 1 || ($gallery_index == 0 && count($galleries) < 3) || (count($galleries) == 1) ) {  //eg. 3 galleries, first is thumb (index [0]), 2 regular galleries ([1],[2]) ?>
    <img src="../web/pix/skip_frw_off.gif" width="6" height="11" alt="next Page" title="next Page" />
<?php } else { ?>	
	<a href="<?php echo $url . "?section=gallery&pag=" . ($gallery_index==0 ? 2 : $gallery_index + 1); ?>" title="next Page">
	    <img src="../web/pix/skip_frw_on.gif" width="6" height="11" alt="next Page" title="next Page" />
	</a>
<?php } ?>
</div>