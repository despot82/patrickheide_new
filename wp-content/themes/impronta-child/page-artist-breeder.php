<?php
/**
 * 
 * Template Name:Artist-Breeder
 * 
 * The template for a artist from the Breeder section of the website
 *
 * @package impronta-child
 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Patrick Heide - Artists / Breeder</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Patrick Heide" />
	<meta name="keywords" content="Patrick Heide Contemporary Art London Exhibitions Fairs Events" />
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<meta http-equiv="imagetoolbar" content="no" />

<!-- CSS -->

<link rel="stylesheet" type="text/css" href="../lightwindow/css/lightwindow.css" />
    
<style type="text/css" title="currentStyle">/* <![CDATA[ */
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/styles.css";
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/large.css";
	/* ]]> */
</style> 
	
	<!--[if lt IE 7.]>
	<style type="text/css" title="currentStyle">
	@import "./web/styles/ie6.css";
	</style>
	<![endif]-->

	<script language="javascript" type="text/javascript"><!--//--><![CDATA[//><!--
	var currentPage = '/artists.php?id=1';
	
	//--><!]]></script>

	<script type="text/vbscript"><!--//--><![CDATA[//><!--
	Function VBGetSwfVer(i)
		on error resume next
		Dim swControl, swVersion
		swVersion = 0

		set swControl = CreateObject("ShockwaveFlash.ShockwaveFlash." + CStr(i))
		if (IsObject(swControl)) then
			swVersion = swControl.GetVariable("$version")
		end if
		VBGetSwfVer = swVersion
	End Function
	//--><!]]></script>
</head>

<body>

<script type="text/javascript"><!--//--><![CDATA[//><!--
if (document.body.offsetWidth < 1245) {
	document.write('<link href="./web/styles/small.css" rel="stylesheet" type="text/css" />\n');
}
//--><!]]></script>

<div id="layout">

	<div id="logo">
	<a href="index.php" title="Patrick Heide" onfocus="this.blur()">
	
		<img src="<?php  echo get_stylesheet_directory_uri();  ?>/images/PatrickHeide.gif" width="220" height="410" alt="Patrick Heide" title="Patrick Heide" />
	</a>
</div>

<?php
    $url = get_permalink($post);

    include "sidebar-navi.php";

    //Get the description content part of the page
    $content = $post -> post_content;
    
    //print "<pre>"; print_r($content); print "</pre>";
    
    $contentExploded = explode("--------", $content);
    
    $info_raw = $contentExploded[1];
    $info = str_replace("\n", "<br/>", $info_raw);
    
?>

<div id="leftcolumn">
	
<?php 

    $section="breeder";
    include (get_stylesheet_directory() . "/page-elements/artist-sidebar-menu.php");
    
    
?>
	
</div>
	
<div id="content">

<?php   

$gallery_index = 1; //If no other setting, then we will display the first gallery

if($_GET["section"] == "thumb") {
	
	//display first gallery images
	$gallery_index = 0;
	
}

if($_GET["section"] == "about") {
	echo "<div class='column two'";
    echo $info;
    echo "</div>";

	$gallery_index = -1;

}

if($_GET["section"] == "gallery" || !isset($_GET["section"])) {
	
	if(isset($_GET["pag"])) {
	    //echo $_GET["pag"];
	    //display images of the number-th gallery
	    
	    $gallery_index = $_GET["pag"];
	}

} 
?>

    <div id="thumbs">
    
<?php  

    include "page-elements/artist-thumbs-gallery.php";

?>
			
  	</div> <!-- END thumbs -->
				
</div>
	
<?php if($_GET["section"] != "about") include "page-elements/gallery-paging-menu.php"; ?>

</div>
<br />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>

<script>


    $("#Artists").addClass("hi");
        
</script>

<script type="text/javascript" src="../lightwindow/javascript/prototype.js"></script>
<script type="text/javascript" src="../lightwindow/javascript/effects.js"></script>
<script type="text/javascript" src="../lightwindow/javascript/lightwindow.js"></script>


</body>
</html>