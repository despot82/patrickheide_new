<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Impronta child
 */

?>

<?php 
    include "sidebar-navi.php";
?> 

<div id="leftcolumn">
		<ul class="navi">				
	        <li>
				<a id="2016" href="../exhibitions_2016">2016</a><br>
				

			</li>
			<li>
				<a id="2015" href="../exhibitions_2015">2015</a><br>
				

			</li>
			<li>
				<a id="2014" href="../exhibitions_2014">2014</a><br>
				

			</li>
			<li>
				<a id="2013" href="../exhibitions_2013">2013</a><br>
				

			</li>
			<li>
				<a id="2012" href="../exhibitions_2012">2012</a><br>
				

			</li>
			<li>
				<a id="2011" href="../exhibitions_2011">2011</a><br>
				

			</li>
			<li>
				<a id="2010" href="../exhibitions_2010">2010</a><br>
				

			</li>
			<li>
				<a id="2009" href="../exhibitions_2009">2009</a><br>
				

			</li>
			<li>
				<a id="2008" href="../exhibitions_2008">2008</a><br>
				

			</li>
			<li>
				<a id="2007" href="../exhibitions_2007">2007</a><br>
				

			</li>
			<li>
				<a id="patrick_heide_art_projects" href="2006.php.html">patrick heide art projects</a><br>
				

			</li>
    </ul>
</div>

<!-- #sidebar -->


