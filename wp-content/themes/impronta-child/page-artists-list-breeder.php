<?php
/**
 * 
 * Template Name:Artists-List-Breeder
 * 
 * The template for list of breeder artists
 *
 * @package impronta-child
 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Patrick Heide - Artists / Breeder</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Patrick Heide" />
	<meta name="keywords" content="Patrick Heide Contemporary Art London Exhibitions Fairs Events" />
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<meta http-equiv="imagetoolbar" content="no" />

	<style type="text/css" title="currentStyle">/* <![CDATA[ */
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/styles.css";
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/large.css";
	/* ]]> */</style>
	<!--[if lt IE 7.]>
	<style type="text/css" title="currentStyle">
	@import "./web/styles/ie6.css";
	</style>
	<![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    
	<!--  Javascript  -->
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . "/js/scripts.js"; ?>"></script>
	<script type="text/javascript" src="../lightwindow/javascript/slideshow.js"></script>
	<script type="text/javascript" src="../lightwindow/javascript/prototype.js"></script>
	<script type="text/javascript" src="../lightwindow/javascript/effects.js"></script>
	
	

	<script language="javascript" type="text/javascript"><!--//--><![CDATA[//><!--
	var currentPage = '/artists.php';
	window.onresize = reloadPage;
	//--><!]]></script>

	<script type="text/vbscript"><!--//--><![CDATA[//><!--
	Function VBGetSwfVer(i)
		on error resume next
		Dim swControl, swVersion
		swVersion = 0

		set swControl = CreateObject("ShockwaveFlash.ShockwaveFlash." + CStr(i))
		if (IsObject(swControl)) then
			swVersion = swControl.GetVariable("$version")
		end if
		VBGetSwfVer = swVersion
	End Function
	//--><!]]></script>
</head>

<body>

<script type="text/javascript"><!--//--><![CDATA[//><!--
if (document.body.offsetWidth < 1245) {
	document.write('<link href="../web/styles/small.css" rel="stylesheet" type="text/css" />\n');
}
//--><!]]></script>

<div id="layout">

	<div id="logo">
		<a href="index.php.html" title="Patrick Heide" onfocus="this.blur()">
			<img src="<?php  echo get_stylesheet_directory_uri();  ?>/images/PatrickHeide.gif" width="220" height="410" alt="Patrick Heide" title="Patrick Heide" />
		</a>
	</div>

	<div id="navigation">
		<ul class="navi">
						<li>
				<a href="../home" title="" onfocus="this.blur()">Home</a><br />
			</li>
			<li>
				<a href="../profile" title="" onfocus="this.blur()">Profile</a><br />
			</li>
			<li>
				<a href="../artists" title="" onfocus="this.blur()" class="hi">Artists</a><br />
			</li>
			<li>
				<a href="../exhibitions_2016" title="" onfocus="this.blur()">Exhibitions</a><br />
			</li>
			<li>
				<a href="../fairsandevents_2016" title="" onfocus="this.blur()">Fairs & Events</a><br />
			</li>
			<li>
				<a href="../publications" title="" onfocus="this.blur()">Publications</a><br />
			</li>
			<li>
				<a href="../contact.php.html" title="" onfocus="this.blur()">Contact</a><br />
			</li>

		</ul>
	    <a href="http://www.facebook.com/PATRICKHEIDECONTEMPORARYART" target="_blank" class="social-media" id="facebook"></a><a href="https://twitter.com/PATRICKHEIDECA" target="_blank" class="social-media" id="twitter"></a>
	</div>

<?php 

    global $wpdb;
    
    $sql_breeder = "select 
	top_menu_item.post_title top_menu_item, 
	#sub_menu_item.id as sub_menu_item_post_id,
	linked_page.id as linked_page_post_id, 
	sub_menu_item.post_title as menu_item_title,
	custom_url.meta_value as custom_url
	from wp_posts as top_menu_item
	join wp_postmeta sub_menu_item_parent_meta on #find all post meta which link other items to top_menu_item as parent
	  (sub_menu_item_parent_meta.meta_key='_menu_item_menu_item_parent' and
	   sub_menu_item_parent_meta.meta_value = top_menu_item.id)
	join wp_posts sub_menu_item on (sub_menu_item.id = sub_menu_item_parent_meta.post_id) #hook all these child menu items
	join wp_postmeta linked_page_meta on
	  (linked_page_meta.post_id = sub_menu_item_parent_meta.post_id and
	   linked_page_meta.meta_key='_menu_item_object_id')
	left join wp_posts as linked_page on(linked_page.id = linked_page_meta.meta_value)
	left join wp_postmeta as custom_url on
	  (custom_url.post_id = sub_menu_item.id and
	   custom_url.meta_key = '_menu_item_url') 
	where 1=1
	and top_menu_item.post_title='Breeder' #just select Breeder artists pages
	and top_menu_item.post_type = 'nav_menu_item' #top menu item is also a nav_menu_item
	order by top_menu_item.post_title desc, sub_menu_item.menu_order asc;";
    
    
    $results_breeder = $wpdb -> get_results ($sql_breeder);
   
    
    //print "<pre>"; print_r($results); print "</pre>";    
    
/*
     Array
(
    [0] => stdClass Object
        (
            [top_menu_item] => Representing
            [linked_page_post_id] => 943
            [menu_item_title] => Isabel Albrecht
            [custom_url] => http://localhost/gp/artists.php-id=1.html
        )
        
*/
    
    

?>

	<div id="leftcolumn">
		<ul class="navi">
		    <li>
					<a href="../artists">Representing</a><br />
					

			</li>
			
			<li>
					<a href="../artists_collaborating">Collaborating</a><br />
					

			</li>
						
		    <li>
					<a href="../artists_breeder" class="hi">Breeder</a><br />
					
					<ul>
					
<?php 


//Form menu with ids
for($i = 0; $i < count($results_breeder); $i ++) {
	$next = $results_breeder[$i];
	$url = "";
	if(empty($next -> custom_url)) {
		$url = get_permalink($next -> linked_page_post_id);
	} else {
		$url = $next -> custom_url;
	}
	
	echo "<li><a href='" . $url . "' id='artist" . $next -> linked_page_post_id . "' title='...more'>" . 
	$next -> menu_item_title . "</a><br /></li>";
	
}



?>
					
		
					</ul>

				</li>
				
				
</ul>
	</div>

	<div id="homecontent">

		<div id="slideshow">
			<script type="text/javascript"><!--//--><![CDATA[//><!-- 
			var captionDiv = "captionDiv";


<?php 

//Form ids for looping over the menu items and highlight them when their turn comes
for($i = 0; $i < count($results_breeder); $i ++) {
	$next = $results_breeder[$i];
	echo "aArtistId[" . $i . "] = " . $next -> linked_page_post_id . ";";  
}


?>
			

			aSlideTxt = new Array();

<?php 

    //Images urls and captions to display on bottom of the page
    require "page-elements/form-slides-data-from-page-gallery.php";
    
    //Form javascript array from above php array
	foreach($descriptions as $key => $description ) {
    	    
		$description = str_replace("\n", "<br/>", $description);
		
	    echo "aSlideTxt[" . $key . "] = '" . $description . "';";
	    
	}				
?>
			
			var nActiveArtistId = 0; 
			
			// Version check based upon the values entered above in "Globals"
    var oTag = '<object data="../web/pix/slideshow.swf?sPix=';

            <?php 

    		foreach($slideshow_images_urls as $id => $url) {
    		    
    		?>

    		oTag += '<?php echo $url; ?>';

    		<?php 

    			if($id != count($slideshow_images_urls) - 1 )  {
    		?>

    		oTag += ',';

    		<?php 	
    			}

    		}

    		?>			

    		oTag += '&amp;txt=true&amp;sHref=/artists.php?artist=1" type="application/x-shockwave-flash" width="100%" height="100%" id="slideshow_flash"><param name="allowscriptaccess" value="samedomain" /><param name="movie" value="../web/pix/slideshow.swf?sPix=%2Fmedia%2Finstallation_shot_.825d4591.jpg%2C%2Fmedia%2Fpatrick_heide_mime.518bc48f.jpg%2C%2Fmedia%2Ftilellw7rl7xh0bsgb.7b4cc223.jpg%2C%2Fmedia%2Falex_hamilton_inst.f6de635e.jpg%2C%2Fmedia%2Fmagnificent_coinci.2aee4477.jpg%2C%2Fmedia%2Fthomas_kilpper_11l.38acbb5c.jpg%2C%2Fmedia%2Fminjung_kim_instal.8cb73c22.jpg%2C%2Fmedia%2F0f0bc3e1e303657990.599d7189.jpg%2C%2Fmedia%2Fbatwomen_on_my_min.e6350ad1.jpg%2C%2Fmedia%2Fwindows_13_lr_.56369a7c.jpg%2C%2Fmedia%2Fthomas_muller_inst.aceef1f4.jpg%2C%2Fmedia%2Fnr.5--0040lr.05dd532d.jpg%2C%2Fmedia%2Ffrancesco_pessina_.787d1ba7.jpg%2C%2Fmedia%2Fhutong_01.b20810c4.jpg%2C%2Fmedia%2Fdirty_linen_04.5f23ae05.jpg%2C%2Fmedia%2Fhere_and_again_09l.9eff2af4.jpg&amp;txt=true&amp;sHref=/artists.php?artist=1" /><param name="quality" value="high" /><param name="bgcolor" value="#ffffff" /><param name="scale" value="noscale" /><param name="salign" value="tl" /><param name="menu" value="false" /><param name="wmode" value="window" /></object>';            
			
			function initSlideshow() {
				aSlides = new Array();

				//aSlides[0] = '/media/installation_shot_.825d4591.jpg';
<?php 
for($i = 0; $i < count($slideshow_images_urls); $i ++) {
	$next = $slideshow_images_urls[$i];
	//echo "alert('" . $next . "');";
	echo "aSlides[" . $i . "] = '" . $next . "';";  
}
?>

				

document.writeln('<img src="/media/installation_shot_.825d4591.jpg" id="slide" title="" alt="" />');
Slideshow = new slideshow(aSlides, "slide", "", 1, "true", "false", aSlideTxt, "captionDiv");	// params: aSlides,imgName[,layerID][,direction=1][,autostart="true"][,imgButtons="true"][,aSlideTxt][,captionDivID]]

			}
			
			if (oTag != "") {
				var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
				// Check to see if the version meets the requirements for playback
				if (hasReqestedVersion) {  // if we've detected an acceptable version
					embedFlash(oTag);   // write the relevant Object-Tag
				}
				else {  // flash is too old or we can't detect the plugin
					// JS-Slideshow Init
					initSlideshow();
					if (aSlideTxt) { window.onload = initCaption; }
				}
			}
			else {  // no Object-Tag defined
				// JS-Slideshow Init
				initSlideshow();
				if (aSlideTxt.length > 0) { window.onload = initCaption; }
			}
			//--><!]]></script>
			
				
			<noscript>
				<div>
					<img src="media/installation_shot_.825d4591.jpg" id="slide_bak" title="" alt="" />
				</div>  	
			</noscript>
		</div>
		
	</div>
	
	<div id="captionDiv">
		Colour Progression- Isabel Albrecht<br />Installation Shot at Patrick Heide CA, London 2008
	</div>
	
	<a href="javascript:hideInfoBreeder();"><div id="infoBreederOverlay" style="display: block;width:100%; height:100%; background: #fff; opacity:0.9;position: absolute; z-index:99;"></div>
<div id="infoBreeder" style="padding-left: 10px;width:100%; height:100%; position: absolute; z-index:99;">
  <div style="background: #eee;height: 20px; display: block; text-decoration: none;color: #000;padding: 0 0;width:480px; margin:200px auto 0 auto;">
    <div style="padding-left: 2px;background: #eee;float:left; font-size: 12px;">Breeder</div>
    <div style="padding-right: 2px;background: #eee;float:right;">
      <a href="javascript:hideInfoBreeder();">close info</a>
    </div>
  </div>
  <div style="text-decoration: none;color: #000;padding: 20px;height:126px; width:420px; margin:0 auto; background: #fff; border:10px solid #eee; border-width: 0 10px 10px 10px;">
   &quot;Breeder&quot; is a new section of the website that is showcasing young
artists and talents that we have worked with in the past and in whose
future we believe strongly.<br />
We will be continually updating the &quot;Breeder&quot; section and also
introduce other emerging artists and recent graduates that we
consider to have potential to develop and become part of the
established gallery program.<br /><br />
So have a look from time to time for new discoveries!<br /><br />
   </div>
   </div>
	
	
</div>
<br />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>

</body>
</html>