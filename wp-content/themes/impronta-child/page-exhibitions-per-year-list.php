<?php
/**
 * 
 * Template Name:Exhibitions-Per-Year-List
 * 
 * The template for Exhibitions listing for a year page
 *
 * @package impronta-child
 */

?>

<head>

<!-- Meta Tags -->
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index, follow" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Patrick Heide" />
<meta name="keywords" content="Patrick Heide Contemporary Art London Exhibitions Fairs Events" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta http-equiv="imagetoolbar" content="no" />

<!-- JavaScript -->
<script type="text/javascript" src="../lightwindow/javascript/prototype.js"></script>
<script type="text/javascript" src="../lightwindow/javascript/effects.js"></script>
<script type="text/javascript" src="../lightwindow/javascript/lightwindow.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.12.1.min.js"></script>

<style type="text/css" title="currentStyle">/* <![CDATA[ */
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/styles.css";
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/large.css";
	/* ]]> */
</style> 

<?php 

    $content = $post -> post_content;
    $contentExploded = explode("--------", $content);
    $script = $contentExploded[1];
    
?>

</head>

<body>

<div id="layout">

<div id="logo">
	<a href="index.php.html" title="Patrick Heide" onfocus="this.blur()">
	
		<img src="<?php  echo get_stylesheet_directory_uri();  ?>/images/PatrickHeide.gif" width="220" height="410" alt="Patrick Heide" title="Patrick Heide" />
	</a>
</div>

<?php 
    include "sidebar-navi.php";	
?>

<div id="leftcolumn">
		
<?php 

    $years_page = get_page_by_title("Exhibitions_Years");
	$years_page_content = $years_page -> post_content;
	echo $years_page_content;

?>	
		
</div>

<div id="content">
	    
	    <script type="text/javascript"><!--//--><![CDATA[//><!--
	    var activeEventId;
		
		function setEvent(eventId) {

			if (activeEventId != null) {
				document.getElementById('img' + activeEventId).className = "previewImgOff";
				document.getElementById('text' + activeEventId).className = "eventTextOff";
			}
			activeEventId = eventId;	 
			document.getElementById('img' + eventId).className = "previewImgOn";
			document.getElementById('text' + eventId).className = "eventTextOn";
		}
		//--><!]]>
	</script>  
      
<?php 

    //print "<pre>";    print_r($post); print "</pre>";
    
//*******************************************************

$gallery = get_post_gallery($post, false);

//print "<pre>gallery: "; print_r($gallery); print "</pre>";

$images_posts_ids = explode(",", $gallery["ids"]);

for($i = 0; $i < count($gallery["src"]); $i ++)
{
	//post_title, post_excerpt
	$id = $images_posts_ids[$i];

	$image_post = get_post($id);

	//print "<pre>image_post: "; print_r($image_post); print "</pre>"; ****** post_content (description), post_excerpt, guid, post_title

	//print "<pre>getimagesize of $id: "; print_r(getimagesize($image_post -> guid)); print "</pre>";

	$size = wp_get_attachment_metadata($id);

?>

<div id="img<?php echo $i; ?>">
	<a href="<?php echo $image_post -> post_content; //post_content is taken from Description field ?>">
	<img style="margin-top: 14px;" 
	     title="<?php echo $image_post -> post_title; ?>" 
	     src="<?php echo $image_post -> guid; //guid is the URL of the image from the admin page ?>" 
	     alt="<?php echo $image_post -> post_title; ?>" 
	     
	     <?php 
	     if ($size["width"] / $size["height"] <= 1.48) { 
	         echo "height='135'";
	     } else if ($size["width"] / $size["height"] > 1.48) { 
	         echo "width='200'";
	     }  
	     ?>
	     
	     border="0" /></a>
</div>
<div id="text<?php echo $i; ?>">
<?php
    
    $excerpt_processed = str_replace("\n", "<br/>", $image_post -> post_excerpt);
    echo $excerpt_processed; //post_excerpt is taken from Caption field from admin page  
?>
</div>


<?php 
    	
    }
    

    //echo $contentExploded[0];
?>  

<script>


    $("#Exhibitions").addClass("hi");
    $("#<?php echo get_post_meta($post ->  ID, 'Year')[0]; ?>").addClass("hi");
    

    //Set onmouseup event for each exhibition tab
    $( "#content" ).children().each(function( index ) {
        id = $(this).attr("id");

        //First check that id exists, if it is "undefined", pass it, otherwise scripts fails and dies
        if (typeof id === "undefined") {
            
        } else {
        	firstThree = id.substring(0, 3);
            firstFour = id.substring(0, 4);
            
            if(firstFour.toLowerCase() == "text") {

                //alert("tolowcase equals text");
                $(this).addClass("eventTextOff");
                
                
                $(this).mouseup(function() {
                	id = $(this).attr("id");
                	
                	setEvent(id.substring(4, 7));

                	//alert( "Handler for .mouseup() called." + id.substring(4, 7));
                });
            }

            if(firstThree.toLowerCase() == "img") {
            	$(this).addClass("previewImgOff");
            }
        }  	    
    });

    setEvent(<?php echo (get_post_meta($post ->  ID, 'Active')[0]) - 1; ?>);
    
</script>


</div>

</div>

</body>
