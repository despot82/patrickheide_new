<?php
/**
 * 
 * Template Name:Fairs-And-Events
 * 
 * The template for fair or event page
 *
 * @package impronta-child
 */

?>

<head>

<!-- Meta Tags -->
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index, follow" />
	
<!-- CSS -->

<link rel="stylesheet" type="text/css" href="../lightwindow/css/default.css" />
<link rel="stylesheet" type="text/css" href="../lightwindow/css/lightwindow.css" />
    
<style type="text/css" title="currentStyle">/* <![CDATA[ */
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/styles.css";
		@import "<?php echo get_stylesheet_directory_uri(); ?>/css/large.css";
	/* ]]> */
</style> 

<?php 

    $content = $post -> post_content;
    $contentExploded = explode("--------", $content);
    
    $fairevent_info_raw = $contentExploded[1];
    $fairevent_info = str_replace("\n", "<br/>", $fairevent_info_raw);
    //$fairevent_info = $fairevent_info_raw;
    
    
?>


</head>

<body>

<div id="layout">

<div id="logo">
	<a href="index.php.html" title="Patrick Heide" onfocus="this.blur()">
	
		<img src="<?php  echo get_stylesheet_directory_uri();  ?>/images/PatrickHeide.gif" width="220" height="410" alt="Patrick Heide" title="Patrick Heide" />
	</a>
</div>

<?php 
    include "sidebar-navi.php";	
?>

<div id="leftcolumn">

<?php 

$years = get_page_by_title("Fairsandevents_Years");
$years_content = $years -> post_content;
echo $years_content;

?>		

</div>
   
<div id="content">  

<div class="imgColumn">  
    
<?php 
    
$gallery = get_post_gallery($post, false);

//print "<pre>gallery: "; print_r($gallery); print "</pre>";
    
$images_posts_ids = explode(",", $gallery["ids"]);
    
for($i = 0; $i < count($gallery["src"]); $i ++)
{
    //post_title, post_excerpt
    $id = $images_posts_ids[$i];
        
    $image_post = get_post($id);
        
    //print "<pre>image_post: "; print_r($image_post); print "</pre>";
        
    $size = wp_get_attachment_metadata($id);
        
    //print "<pre>meta:"; print_r($size); print "</pre>";
        

    include "page-elements/exhibition-image-thumb-lightwindowed.php";  	
    
}

?>  

</div>

<div class="column two">
<?php 
  
    echo $fairevent_info;

?>

</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>

<script>

    $("#FairsAndEvents").addClass("hi");
    $("#<?php echo get_post_meta($post ->  ID, 'Year')[0]; ?>").addClass("hi");

    
</script>


<script type="text/javascript" src="../lightwindow/javascript/prototype.js"></script>
<script type="text/javascript" src="../lightwindow/javascript/effects.js"></script>
<script type="text/javascript" src="../lightwindow/javascript/lightwindow.js"></script>

</body>
 