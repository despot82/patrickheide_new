jQuery(document).ready(function($) {

	//Ajax-ify Home menu link
	$("#Home").click(function(){ 	
		//alert("Home link clicked");
		$.ajax({  
		       url: ajaxurl, 
		       type: "POST",    	   
		       cache: false,
		       data: { 
		    	   action: 'home_page'
		    	
	   	   }
	     })
	     .done(function( html ) {
	         $("#main").html(html);
	  	     //alert("DONE feshe from home script branch!");
	    });
	});

	//Ajax-ify Profile menu link
	$("#Profile").click(function(){ 	
		//alert("Profile link clicked");
		$.ajax({  
		       url: ajaxurl, 
		       type: "POST",    	   
		       cache: false,
		       data: { 
		       	   action: 'profile_page'
		       	  
	   	       }
	     })
	     .done(function( html ) {
	    	 $("#main").html(html);
	  	     //alert("DONE feshe from profile script branch!");
	    });
	});
	
	//Ajax-ify Artists menu link
    $("#Artists").click(function(){ 	
		//alert("Profile link clicked");
		$.ajax({  
		       url: ajaxurl, 
		       type: "POST",    	   
		       cache: false,
		       data: { 
		       	   action: 'artists_page'
		       	  
	   	       }
	     })
	     .done(function( html ) {
	    	 $("#main").html(html);
	  	     //alert("DONE feshe from artists script branch!");
	    });
	});
	
    //Ajax-ify Exhibitions menu link
	$("#Exhibitions").click(function(){ 	
		//alert("Exhibitions link clicked");
		$.ajax({  
		       url: ajaxurl, 
		       type: "POST",    	   
		       cache: false,
		       data: { 
		       	   action: 'exhibitions_page'
		       	  
	   	       }
	     })
	     .done(function( html ) {
	    	 $("#main").html(html);
	  	     //alert("DONE feshe from ex script branch!");
	    });
	});
	
	//Ajax-ify Fairs and Events menu link
	$("#FairsEvents").click(function(){ 	
		//alert("Fairs & Events link clicked");
		$.ajax({  
		       url: ajaxurl, 
		       type: "POST",    	   
		       cache: false,
		       data: { 
		       	   action: 'fairsevents_page'
		       	  
	   	       }
	     })
	     .done(function( html ) {
	    	 $("#main").html(html);
	  	     //alert("DONE feshe from Fairs and Events script branch!");
	    });
	});
	
	//Ajax-ify Publications menu link
	$("#Publications").click(function(){ 	
		//alert("Publications link clicked");
		$.ajax({  
		       url: ajaxurl, 
		       type: "POST",    	   
		       cache: false,
		       data: { 
		       	   action: 'publications_page'
		       	   
	   	       }
	     })
	     .done(function( html ) {
	    	 $("#main").html(html);
	  	     //alert("DONE feshe from publications script branch!");
	    });
	});
	
	//Ajax-ify Contact menu link
	$("#Contact").click(function(){ 	
		//alert("Exhibitions link clicked");
		$.ajax({  
		       url: ajaxurl, 
		       type: "POST",    	   
		       cache: false,
		       data: { 
		       	   action: 'contact_page'
		       	  
	   	       }
	     })
	     .done(function( html ) {
	    	 $("#main").html(html);
	  	     //alert("DONE feshe from contact script branch!");
	  	     
	    });
	});	
	
	
	
});

