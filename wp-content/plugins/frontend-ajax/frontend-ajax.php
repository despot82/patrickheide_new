<?php
/*
 Plugin Name: Frontend Ajax
 Description: Ajax-ify loading of frontend pages
 Version: 1.0
 Author: Vladimir Despotovic
 Author URI: http://despotovicvladimir.com
 */

//Enqueue jquery
wp_enqueue_script("jquery-1.12.1", get_stylesheet_directory_uri() . "/js/jquery-1.12.1.min.js");

//Include the FEA plugin's scripts
wp_enqueue_script("fea-scripts", plugin_dir_url(__FILE__) . "/js/scripts.js");

//Localize the ajaxurl to send ajax requests to
wp_localize_script("fea-scripts", "ajaxurl", admin_url( "admin-ajax.php"));

$ajax_pages = array("home_page", "profile_page", "artists_page", "exhibitions_page", "fairsevents_page",
		            "publications_page", "contact_page"
);

//Go through each page name and trigger function with the same name when that name comes through admin ajax call
foreach($ajax_pages as $page) {
	//Ajax-ify page call  
	$function_name = $page . "_callback";
	
	$$function_name = function()
	{
		$action_exploded = explode("_", $_POST["action"]);
		$page = $action_exploded[0];
		
		$page_post = get_page_by_title($page);
		
		echo $page_post -> post_content;
		
		echo "
		
		<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/scripts/scripts.js'></script>
		<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/scripts/slideshow.js'></script>
		<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/scripts/prototype.js'></script>
		<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/scripts/effects.js'></script>
		<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/scripts/lightwindow.js'></script>
	";
		
		wp_die();
	};
	
	add_action("wp_ajax_nopriv_$page", $$function_name);
	add_action("wp_ajax_$page", $$function_name);
	
	
	
}

?>