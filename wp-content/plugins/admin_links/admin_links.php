<?php

/**
 * @package admin_links
 * @version 1.0
 */
/*
 Plugin Name: Admin Links
 Plugin URI: http://despotovicvladimir.com
 Description: Plugin to remove some of the links in admin 
 Author: Vladimir Despotovic
 Version: 1.0
 Author URI: http://despotovicvladimir.com
 */

//Remove unused submenu items
function remove_menus(){

	//remove_menu_page( 'index.php' );                  //Dashboard
	remove_menu_page( 'edit.php' );                   //Posts
	//remove_menu_page( 'upload.php' );                 //Media
	//remove_menu_page( 'edit.php?post_type=page' );    //Pages
	remove_menu_page( 'edit-comments.php' );          //Comments
	//remove_menu_page( 'themes.php' );                 //Appearance
	remove_menu_page( 'plugins.php' );                //Plugins
	//remove_menu_page( 'users.php' );                  //Users
	remove_menu_page( 'tools.php' );                  //Tools
	//remove_menu_page( 'options-general.php' );        //Settings

	
	//remove_submenu_page("themes.php", "themes.php");
	//remove_submenu_page("themes.php", "widgets.php");
	remove_submenu_page("themes.php", "theme-editor.php");
	
	
	remove_submenu_page("options-general.php", "options-reading.php");
	remove_submenu_page("options-general.php", "options-permalink.php");

	

}
add_action( 'admin_menu', 'remove_menus' );

//Remove Customize sub menu item
add_action('admin_menu', 'as_remove_menus');
function as_remove_menus () {
	global $submenu;
	
	//print "submenu<pre>"; print_r($submenu); print "</pre>"; die();
	
	
	unset($submenu['themes.php'][5]);
	unset($submenu['themes.php'][6]);
	unset($submenu['themes.php'][7]);
	unset($submenu['themes.php'][20]);
    

	//print "submenu<pre>"; print_r($submenu); print "</pre>"; die();
	
}


//Remove theme editor submenu item
add_action('admin_init', 'my_remove_menu_elements', 102);
function my_remove_menu_elements()
{
	remove_submenu_page( 'themes.php', 'theme-editor.php' );
}