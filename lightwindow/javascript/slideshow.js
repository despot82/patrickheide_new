/*******************************************************************************
file:			slideshow.js
author:			oh@design-aspekt.com
date:			15-05-2006

requires:		Array with Image-Paths
				image with distinctive name

input params:	essential: Array with Image-Paths, image-name
				optional: layerID, direction, autostart, buttons, 
							Array with captions, layerID for captions
********************************************************************************/
var Slideshow = null;
var slideObject = null;						// very important: stores the Slideshow-Object, because setTimeout cannot call function with "this"
var activeBtn = null;						// stores the active link-element in jump-navi -> used for swapHighlight
var activeSrc = null;
var dummyThumb = "./web/pix/proj_thumb_hi.gif";
var displayMode = "slide";

var aSlides = new Array();
var aSlideTxt = new Array();
var aSlidesThumb = new Array();

function slideshow() {	// Object constructor
	// object attributes defined by input params
	this.aSlides = arguments[0];			// array with image names to be loaded	
	this.imgName = arguments[1];			// name of the image to be rotated
	
	if (arguments[2]) {
		this.layerID = arguments[2];		// id of the layer in which the image is located: relevant for NS4, otherwise var can be empty		
	}
	else { this.layerID = null; }
	
	if (arguments[3]) {
		this.direction = arguments[3];		// can be 1 or -1; e.g. the direction can be changed via mouse-click etc.
	}
	else { this.direction = 1; }
	
	if (arguments[4]) {
		this.autostart = arguments[4];		// can be "true" or "false"
	}
	else { this.autostart = "true"; }
	
	if (arguments[5]) {
		this.imgButtons = arguments[5];		// can be "true" or "false"
	}
	else { this.imgButtons = "false"; }
	
	if (arguments[6]) {
		this.aSlideTxt = arguments[6];		// array with caption text to be displayed with each image
	}
	else { this.aSlideTxt = new Array(); }
	
	if (arguments[7]) {
		this.captionDivId = arguments[7];	// id of the layer to display captions
	}
	else { this.captionDivId = null; }
	
	// further object attributes
	this.start = "true";					// initial state to prevent counting in rotate() before first rotation
	this.counter = 0;
	this.delay = false;						// Timeout for rotating Images
	this.slide = false;						// Timeout for delaying image rotation during loading process
	this.aPreLoad = new Array();			// Array used to preload images by creating new Image-Objects
	
	this.playStatus = (this.autostart == "true" ? "play" : "stop");		// switch to toggle Play/Stop-Button
	
	// global var to handle timeout
	slideObject = this;						// stores the Object for use in setTimeout()
	
	// object methods
	this.getImage = getImage;					// cross-browser method to retrieve the image
	this.checkImg1 = checkImg1;					// checks if first image is loaded and then starts preLoader and rotation
	this.preLoader = preLoader;					// method creating new Image-Objects and thus forcing the browser to load the images at once
	this.rotateImg = rotateImg;					// method for rotating the image
	this.changeDirection = changeDirection;		// method to change the direction, e.g. triggered by Event-Handler
	this.stopSlide = stopSlide;					// method to to manually start slideshow, e.g. triggered by Event-Handler
	this.startSlide = startSlide;				// method to to manually interrupt slideshow, e.g. triggered by Event-Handler
	this.skipBack = skipBack;					// method to skip one image back, e.g. triggered by Event-Handler
	this.skipForward = skipForward;				// method to skip one image forward, e.g. triggered by Event-Handler
	this.jump2img = jump2img;					// method to jump to particular image, e.g. triggered by Event-Handler
	this.dropItem = dropItem;					// method to delete images from array when loading process is interrupted
	this.swapHighlight = swapHighlight;			// method to highlight image-links in jump-navi
	this.createImgLinks = createImgLinks;		// writes numbered Links to jump to images directly
	this.togglePlayButton = togglePlayButton;	// swaps Play-Button (play/stop)
	this.showCaption = showCaption;				// shows Caption
	
	// init actions
	this.aPreLoad[0] = new Image();			// loads first Image into preLoad-Array
	this.aPreLoad[0].onerror = this.dropItem;	// if loaded image data is corrupt, the image is deleted from the array to prevent interruption during rotation
	this.aPreLoad[0].src = this.aSlides[0];
	if (this.imgButtons == "false") { this.checkImg1(); }
}

function getImage(imgName,layerID) {
	return (document.all || document.getElementById ? window.document.images[imgName] : layerID ? document.layers[layerID].document.images[imgName] : window.document.images[imgName]);
}

function checkImg1() {
	if (this.aPreLoad[0].complete == true) {
		this.preLoader();	// preloads images after first image has been loaded
		if (this.autostart == "true") { this.rotateImg(); }
	}
	else { setTimeout('slideObject.checkImg1()',100); }
}

function preLoader() {
	for (i=1; i<this.aSlides.length; i++) {
		this.aPreLoad[i] = new Image();
		this.aPreLoad[i].onerror = this.dropItem;	// if loaded image data is corrupt, the image is deleted from the array to prevent interruption during rotation
		this.aPreLoad[i].src = this.aSlides[i];	
	}
}

function rotateImg() {
	if (this.aPreLoad[this.counter].complete == true) {	// checks if current image has been loaded
		if (this.direction == 1 && this.start == "false") { (this.counter == (this.aPreLoad.length-1)) ? this.counter=0 : this.counter++; }
		else if (this.direction == -1  && this.start == "false") { (this.counter < 1) ? this.counter=this.aPreLoad.length-1 : this.counter--; }
		this.getImage(this.imgName,this.layerID).src = this.aPreLoad[this.counter].src;
		this.showCaption(this.aSlideTxt[this.counter]);
		self.status = "Picture "+ (this.counter+1) + " of " + this.aPreLoad.length;
		if (this.start == "true") { this.start = "false"; }
		this.swapHighlight(this.counter);
		this.slide = setTimeout('slideObject.rotateImg()',5000);
	}
	else { // if image has still not been loaded, the function is called again with less delay
		self.status = "Picture "+ (this.counter+1) + " is still loading...";
		this.slide = setTimeout('slideObject.rotateImg()',100);
	}
}

function changeDirection() {
	if (this.direction == 1) { this.direction = -1; }
	else if (this.direction == -1) { this.direction = 1; }
}

function startSlide() { this.rotateImg(); }
function stopSlide() {
	if (this.slide != false) { clearTimeout(this.slide); }
}

function skipBack() {
	if (this.slide != false) { clearTimeout(this.slide); }
	if (this.delay != false) { clearTimeout(this.delay); }
	(this.counter < 1) ? this.counter = this.aPreLoad.length-1 : this.counter--;
	this.getImage(this.imgName,this.layerID).src = this.aPreLoad[this.counter].src;
	this.showCaption(this.aSlideTxt[this.counter]);
	self.status = "Picture "+ (this.counter+1) + " of " + this.aPreLoad.length;
	this.delay = setTimeout('slideObject.rotateImg()',5000);
}

function skipForward() {
	if (this.slide != false) { clearTimeout(this.slide); }
	if (this.delay != false) { clearTimeout(this.delay); }
	(this.counter == (this.aPreLoad.length-1)) ? this.counter = 0 : this.counter++;
	this.getImage(this.imgName,this.layerID).src = this.aPreLoad[this.counter].src;
	this.showCaption(this.aSlideTxt[this.counter]);
	self.status = "Picture "+ (this.counter+1) + " of " + this.aPreLoad.length;
	this.delay = setTimeout('slideObject.rotateImg()',5000);
}

function jump2img(counter) {
	if (this.slide != false) { clearTimeout(this.slide); }
	if (this.delay != false) { clearTimeout(this.delay); }
	this.counter = counter;
	this.getImage(this.imgName,this.layerID).src = this.aPreLoad[this.counter].src;
	this.showCaption(this.aSlideTxt[this.counter]);
	this.swapHighlight(this.counter);
	self.status = "Picture "+ (this.counter+1) + " of " + this.aPreLoad.length;
}

function dropItem() { this.del = slideObject.aPreLoad.pop(); }

function swapHighlight(linkId) {
	if (displayMode == "text") { swapDescription(false); }
	
	if (this.imgButtons == "true") {
		imgBtn = eval("document.getElementById('slideBtn" + linkId + "')");		
		if (activeBtn) {
			activeBtn.src = activeSrc;
		}
		if (imgBtn) {
			activeBtn = imgBtn;
			activeSrc = imgBtn.src;
			imgBtn.src = dummyThumb;
		}		
	}
}

function createImgLinks(aSlidesThumb, imgName, layerID) {
	for (i=0; i<aSlidesThumb.length; i++) {
		newSrc = aSlidesThumb[i];
		if (i == 0) {
			document.write("<a href=\"javascript:Slideshow.jump2img(" + i + ");\" onfocus=\"this.blur()\"><img src=\"" + dummyThumb + "\" id=\"slideBtn" + i + "\" alt=\"\" width=\"10\" height=\"10\" /></a>\n");
			activeBtn = eval("document.getElementById('slideBtn" + i + "')");
			activeSrc = newSrc;
		}
		else {
			document.write("<a href=\"javascript:Slideshow.jump2img(" + i + ");\" onfocus=\"this.blur()\"><img src=\"" + newSrc + "\" id=\"slideBtn" + i + "\" alt=\"\" width=\"10\" height=\"10\" /></a>\n");
		}		
	}
	this.checkImg1();
}

function togglePlayButton(imgName, layerID) {
	if (this.playStatus == "play") {
		this.getImage(imgName, layerID).src = aImages[imgName]["on"].src;
		this.stopSlide();
		this.playStatus = "stop";
	}
	else {	
		this.getImage(imgName, layerID).src = aImages[imgName]["off"].src;
		this.startSlide();
		this.playStatus = "play";
	}
}

function showCaption(txt) {
	if (document.getElementById(this.captionDivId)) {
		document.getElementById(this.captionDivId).innerHTML = txt;
	}
}