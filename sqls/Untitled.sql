select top_menu_item.post_title, linked_page.*
from wp_posts as top_menu_item
join wp_postmeta sub_menu_item_parent_meta on 
  (sub_menu_item_parent_meta.meta_key='_menu_item_menu_item_parent' and
   sub_menu_item_parent_meta.meta_value = top_menu_item.id)
join wp_postmeta linked_page_meta on
  (linked_page_meta.post_id = sub_menu_item_parent_meta.post_id and
   linked_page_meta.meta_key='_menu_item_object_id')
join wp_posts as linked_page on(linked_page.id = linked_page_meta.meta_value)
where 1=1
and top_menu_item.post_title = 'Representing' #just select Representing artists pages
and top_menu_item.post_type = 'nav_menu_item' #top menu item is also a nav_menu_item




/* Needed:
-name of parent menu item (representing, collaborating, breeder)
-id of the menu item
-id of the linked page


*/