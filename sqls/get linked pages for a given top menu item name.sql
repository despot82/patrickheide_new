select 
top_menu_item.post_title top_menu_item, 
#sub_menu_item.id as sub_menu_item_post_id,
linked_page.id as linked_page_post_id, 
sub_menu_item.post_title as menu_item_title,
custom_url.meta_value as custom_url
from wp_posts as top_menu_item
join wp_postmeta sub_menu_item_parent_meta on #find all post meta which link other items to top_menu_item as parent
  (sub_menu_item_parent_meta.meta_key='_menu_item_menu_item_parent' and
   sub_menu_item_parent_meta.meta_value = top_menu_item.id)
join wp_posts sub_menu_item on (sub_menu_item.id = sub_menu_item_parent_meta.post_id) #hook all these child menu items
join wp_postmeta linked_page_meta on
  (linked_page_meta.post_id = sub_menu_item_parent_meta.post_id and
   linked_page_meta.meta_key='_menu_item_object_id')
left join wp_posts as linked_page on(linked_page.id = linked_page_meta.meta_value)
left join wp_postmeta as custom_url on
  (custom_url.post_id = sub_menu_item.id and
   custom_url.meta_key = '_menu_item_url') 
where 1=1
and top_menu_item.post_title='Representing' #just select Representing artists pages
and top_menu_item.post_type = 'nav_menu_item' #top menu item is also a nav_menu_item
order by top_menu_item.post_title desc, sub_menu_item.menu_order asc;




/* Needed:
-name of parent menu item (representing, collaborating, breeder)
-id of the menu item
-id of the linked page


*/