SELECT DISTINCT meta_key
			FROM wp_postmeta
			WHERE meta_key NOT BETWEEN '_' AND '_z'
			HAVING meta_key NOT LIKE '\\_%'
			ORDER BY meta_key
			LIMIT 3000;
            
create table wp_postmeta_deleted as SELECT * from wp_postmeta where meta_key in(SELECT DISTINCT meta_key
			FROM wp_postmeta
			WHERE meta_key NOT BETWEEN '_' AND '_z'
			HAVING meta_key NOT LIKE '\\_%'
			ORDER BY meta_key);

delete from wp_postmeta WHERE meta_key in(SELECT meta_key
			FROM wp_postmeta_deleted);

select * from wp_postmeta_deleted;

drop table wp_postmeta_deleted;


