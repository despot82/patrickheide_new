#all menus
SELECT t.*, 'TERMS || TERM_TAXONOMY', tt.*
FROM wp_terms AS t
LEFT JOIN wp_term_taxonomy AS tt ON tt.term_id = t.term_id
WHERE slug='artists_menu';

#get all submenu items
SELECT * #t.name, mip.meta_value AS parent_menu_item, 'PARENT || PAG._menu_item_object_id', pag.meta_value AS page_the_item_is_linked_to,linked_page.guid
FROM wp_terms AS t
LEFT JOIN wp_term_taxonomy AS tt ON(tt.term_id = t.term_id) #hook term to its taxonomy
LEFT JOIN wp_term_relationships AS tr ON(tr.term_taxonomy_id = tt.term_taxonomy_id) #hook to taxonomy's relationships
LEFT JOIN wp_posts AS menu_item_post ON(tr.object_id = menu_item_post.ID AND 
                                         menu_item_post.post_type = 'nav_menu_item')
JOIN wp_postmeta mip ON (mip.post_id = menu_item_post.id AND 
                         mip.meta_key='_menu_item_menu_item_parent')
JOIN wp_postmeta pag ON (pag.post_id = menu_item_post.id AND 
                         pag.meta_key='_menu_item_object_id')
JOIN wp_posts linked_page ON(linked_page.id = pag.meta_value)
WHERE 1 = 1 
AND t.name = 'artists_menu' #find artists_menu term which is created when artists menu is created
AND tt.taxonomy = 'nav_menu' #additional check that it is a nav_menu
AND menu_item_post.post_status='publish'; #only get published posts, not drafts

#Custom order: ORDER BY FIELD(Language,'ENU','JPN','DAN'), ID

/*
need: 
-page name for artist, 
-tab under which it is (representing, collaborating, breeder), 
-

*/


