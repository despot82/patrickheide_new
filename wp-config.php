<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'inform');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oTz>w@vw$k|3fGBbAY]A @{.DN| dYEEww;ROSt6,Ge@DEQ~|q24J5p329+B3EQS');
define('SECURE_AUTH_KEY',  '9O4wJ]ix-ie>Z,|&obzJyM#&6DVWZb-_km2{!R$n,|-:+!&UBF4G8$&#AiqBp|-U');
define('LOGGED_IN_KEY',    'GqS;L+I#8imLuMzKs%x>%Ti-]7mBhkTPDT~K$v0m_@9f9Z2eE:j3>Y}CS4Q5*U(e');
define('NONCE_KEY',        '%nN>pBl^!`+?WyD#oAf9Uro<+&-f^>Psxme}`j`Q$v=O%F}rT/YW9z|OX$m}d M0');
define('AUTH_SALT',        ':Xf(X#KF>:VM?~MI|SkJ|Gg}uZ>!f*)P#W- C.2K-U8c5z_jsS/NjsF$sha|S({9');
define('SECURE_AUTH_SALT', '+K<6|09!u*1R*^bP4&v4]km]Gn~-IV+*(#Tby@?76A4m(&5r9q,XX`gVv4*hQduQ');
define('LOGGED_IN_SALT',   'aqPi1,<?);8w _H4j4!Vqg`/g2{$FjE8fQ86)pYVbZ8dLmpK<O3Eg!{[bX&@iZy6');
define('NONCE_SALT',       'M5OG|z^L}b@dVOiKk2AMIQv{OT2VM+-&;-#;,UU(+R3mZY8MUSy@:+?G:Q~MH{`M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
